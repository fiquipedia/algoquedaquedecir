+++
title = "Sobre el blog"
description = "Sobre el blog algoquedaquedecir"
date = "2024-01-21"
author = "Enrique García"
+++

Revisado 21 enero 2024  

Este blog es la versión en gitlab del blog inicial en blogger [algoquedaquedecir](https://algoquedaquedecir.blogspot.com/) creado en 2017.  
Ya había comenzado a colocar ficheros asociados en [gitlab drive.fiquipedia algoquedaquedecir](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/algoquedaquedecir)   

En enero 2024 decido empezar a poner aquí en gitlab las entradas del blog debido a que blogger me anula entradas, lo que supone no tener el control de lo publicado.  
[twitter FiQuiPedia/status/1746492007072047274](https://twitter.com/FiQuiPedia/status/1746492007072047274)  
![](https://pbs.twimg.com/media/GDzJOExWgAkoFcd?format=jpg)  
Ya [me había pasado algo similar con FiQuiPedia en 2018](https://twitter.com/FiQuiPedia/status/952693468744339457) y por eso unido al cambio de versión que no migraba bien mis contenidos abandoné Google Sites.  


