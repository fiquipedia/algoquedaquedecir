+++
author = "Enrique García"
title = "Posts del blog"
date = "2024-01-21"
toc = false
+++

Revisado 4 enero 2025

En todos los _[posts](https://www.fundeu.es/recomendacion/post-alternativas-en-espanol/)_ sigo manteniendo fecha de revisión: todos están en permanente estado de revisión.  
Los _posts_ en blogger indicaban en la url año y mes de creación: aquí lo elimino de la url pero mantengo la fecha original como dato.  
Muchos _posts_ se referencian mutuamente: intentaré que los _posts_ referencien a blogger cuando citen otros no migrados, o que referencien aquí en gitlab cuando sean _posts_ migrados. Eso implica revisar los _posts_ ya migrados y que puedan quedar inconsistencias.  
Las entradas en blogger las iré eliminando y remitiendo aquí: un tema es que los comentarios de blogger quedarán allí, los pondré dentro del post a nivel informativo y está por ver cómo se implementan comentarios aquí.  
Las imágenes de los posts a menudo están en blogger o en Twitter: inicialmente mantengo su ubicación original y las enlazo, si hubiera problemas intentaría tener una copia en local  
 
Este blog lo inicio migrando solo un _post_ desde Blogger, ver [Sobre el blog](../sobre-el-blog)  
Intentaré ir poniendo aquí entradas aunque cuando haya muchas es más rápido llegar vía buscador / etiquetas  
Aquí las pongo en orden cronológico creciente de migración / elaboración de posts: cada uno indica la fecha de elaboración y revisión  
*  [Primer post del blog: presentación](../post/primer-post-del-blog-presentacion) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2017/08/primer-post-del-blog-presentacion.html)  
*  [Ley Maestra de Libertad de Elección Educativa](../post/ley-maestra-de-libertad-de-eleccion-educativa) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2021/01/ley-maestra-de-libertad-de-ensenanza-de.html)   
*  [Uso laboral de medios particulares](../post/uso-laboral-de-medios-particulares)  
*  [Cheques FP Grado Superior](../post/cheques-fp-grado-superior) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2022/06/becas-fp-grado-superior-madrid.html)  
*  [Becas para el estudio de Programas de Segunda Oportunidad Madrid](../post/becas-para-el-estudio-de-programas-de-segunda-oportunidad) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2021/02/becas-para-el-estudio-de-programas-de-segunda-oportunidad.html)  
*  [Grabar conversación como prueba](../post/grabar-conversacion-como-prueba) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2023/12/grabar-conversacion-como-prueba.html)  
*  [Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia](../post/comisiones-de-servicio-madrid-iv-programas-en-centros-bachillerato-de-excelencia) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2023/02/comisiones-de-servicio-madrid-iv-programas-en-centros-bachillerato-de-excelencia.html)  
*  [Grupo Planeta y educación Madrid](../post/grupo-planeta-y-educacion-madrid) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2023/10/grupo-planeta-y-educacion-madrid.html)  
*  [Grupo Planeta y educación Andalucía](../post/grupo-planeta-y-educacion-andalucia)  
*  [Derechos de autor en educación](../post/derechos-de-autor-en-educacion) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2018/08/derechos-de-autor-en-educacion.html)  
*  [Finalidad evaluación para clasificación centros](../post/finalidad-evaluacion-para-clasificacion-centros) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2018/01/lomce-finalidad-evaluacion-para.html)  
*  [Reclamar a inspección](../post/reclamar-a-inspeccion-educativa)  
*  [#Concertados en diferido y voluntad política](../post//concertados-en-diferido-y-voluntad-politica) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2022/12/concertadosendiferido-y-voluntad.html)  
*  [Xcelence escuelas que inspiran](../post/xcelence-escuelas-que-inspiran) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2022/11/xcelence-escuelas-que-inspiran.html)  
*  [Programa Bachillerato Excelencia Madrid](../post/programa-bachillerato-excelencia-madrid)  
*  [Comisiones de servicio. Normativa y jurisprudencia](../post/comisiones-de-servicio-normativa-y-jurisprudencia)  
*  [Bachillerato Internacional](../post/bachillerato-internacional) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2022/11/bachillerato-internacional.html)  
*  [Promoción y titulación en educación](../post/promocion-y-titulacion-en-educacion)  
*  [Horarios secundaria en distintas CCAA](../post/horarios-secundaria-en-distintas-ccaa) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2018/01/horarios-secundaria-madrid-vs-resto-ccaa.html)  
*  [Bilingüismo Madrid: materias](../post/bilinguismo-madrid-materias)  
*  [Ratios en educación: normativa](../post/ratios-en-educacion-normativa) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2018/12/ratios-en-educacion-normativa.html) donde solo quedan los comentarios  
*  [Ratios en educación: coste](../post/ratios-en-educacion-coste)  
*  [Pruebas evaluación educación obligatoria](../post/pruebas-evaluacion-educacion-obligatoria)
*  [Transparencia oposiciones: enunciados inspección educativa](../post/transparencia-oposiciones-enunciados-inspeccion-educativa) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2022/02/transparencia-enunciados-inspeccion.html)  
*  [Coste dinero público de un privado con concierto](../post/coste-dinero-publico-privado-con-concierto)  
*  [Reducción lectivas Madrid](../post/reduccion-lectivas-madrid)  
*  [!Reclamad, malditos!](../post/reclamad-malditos) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2018/07/reclamad-malditos.html)  
*  [Transparencia algoritmos](../post/transparencia-algoritmos) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2021/03/transparencia-algoritmos.html)  
*  [RGPD y religión en educación pública](../post/rgpd-y-religion-en-educacion-publica)  
*  [Aulas Profesionales Emprendimiento](../post/aulas-profesionales-emprendimiento)  
*  [Privatización educación: FP](../post/privatizacion-educacion-fp) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2021/12/privatizacion-de-la-educacion-fp.html)  
*  [Google en educacion](../post/google-en-educacion) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2021/06/google-en-educacion.html)  
*  [Google en educacion: Madrid](../post/google-en-educacion-madrid)  
*  [Sueldo docentes](../post/sueldo-docentes) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2018/09/sueldo-docentes-publicos.html)  
*  [Subida impuestos rentas altas](../post/subida-impuestos-rentas-altas) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2018/09/sueldo-docentes-publicos.html)  
*  [Profesorado religiosamente pagado con dinero público](../post/profesorado-religiosamente-pagado-con-dinero-publico) migrado desde [blogger](https://algoquedaquedecir.blogspot.com/2020/02/profesorado-religiosamente-pagado-con-dinero-publico.html)  
*  [Asignación gastos funcionamiento centros públicos](../post/asignacion-gastos-funcionamiento-centros-publicos)  
*  [MUFACE](../post/muface)  
*  [MUFACE: coste](../post/muface-coste)  
*  [Moodle: ficheros realimentación calificación](../post/moodle-ficheros-realimentacion-calificacion)  
*  [Jornada escolar](../post/jornada-escolar)  
*  [Primero y segundo ESO en CEIP](../post/primero-y-segundo-eso-en-ceip)  
*  [Ausencias docentes](../post/ausencias-docentes)  
*  [Materias optativas Madrid](../post/materias-optativas-madrid)  
*  [Gestión cheques educación privados Madrid](../post/gestion-cheques-educacion-privados-madrid)  
*  [Competencias docentes](../post/competencias-docentes)  

