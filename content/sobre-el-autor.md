+++
title = "Sobre el autor"
description = "Sobre el autor"
date = "2024-01-21"
author = "Enrique García"
+++

Revisado 21 enero 2024  

Remito al [primer post del blog: presentación](/post/primer-post-del-blog-presentacion) y a la [página de contacto en FiQuiPedia](https://www.fiquipedia.es/contacto/) aparte de volver a poner aquí el texto de mi bio en redes sociales  

> Docente en la pública.  
Algo es infinitamente superior a 0.  
0 es una referencia arbitraria.  
cc-by-sa.  
Transparencia.  
Correlación≠causalidad.  
Memento mori  


