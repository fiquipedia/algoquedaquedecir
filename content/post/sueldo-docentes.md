+++
author = "Enrique García"
title = "Sueldo docentes"
date = "2018-09-01"
tags = [
    "impuestos", "Post migrado desde blogger"
]

description = "Sueldo docentes"
thumbnail = "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhtmjBp69FXnWJga_fb-4XTAL7ST5eUZ8cyvI0SuFHCet1zpFVQrw5MgqT5fKhETBJdgbr1E5nV8sxN2NMMHo9bQdkIEG95MQOk34t6bsLIGV0BcunVTEAeETRxElCKgdnvvoZ8Uly3A1dm/s400/2018-08-30-YaEsMediodia.jpg"
images = ["https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhtmjBp69FXnWJga_fb-4XTAL7ST5eUZ8cyvI0SuFHCet1zpFVQrw5MgqT5fKhETBJdgbr1E5nV8sxN2NMMHo9bQdkIEG95MQOk34t6bsLIGV0BcunVTEAeETRxElCKgdnvvoZ8Uly3A1dm/s400/2018-08-30-YaEsMediodia.jpg"]

toc = true

+++

Revisado 14 octubre 2024

## Resumen

Intento documentar el tema del sueldo de los docentes públicos: somos empleados públicos y nuestro sueldo es público, así que más que de transparencia se trata un tema de querer informarse.  
Lo hago inicialmente en 2018 porque surge esta imagen en la televisión, que es totalmente falsa.  
Además intento poner el contexto en el que se da esa información falsa; lo veo con un mal uso de estadísticas e información mezclado con un intento de manipular para oponerse a la subida de impuestos a las rentas más altas / banca.

Inicialmente trataba en el mismo post el tema de subida de impuestos a rentas altas pero al migrar el post separo temas y creo post separado [Subida impuestos rentas altas](../post/subida-impuestos-rentas-altas)  

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhtmjBp69FXnWJga_fb-4XTAL7ST5eUZ8cyvI0SuFHCet1zpFVQrw5MgqT5fKhETBJdgbr1E5nV8sxN2NMMHo9bQdkIEG95MQOk34t6bsLIGV0BcunVTEAeETRxElCKgdnvvoZ8Uly3A1dm/s400/2018-08-30-YaEsMediodia.jpg)  
[T01 Programa 54, "Ya es mediodía", Telecinco, 30 agosto 2018](https://www.mitele.es/programas-tv/ya-es-mediodia/5b87e982591063c8a28b467a/player)  

Post relacionados:

* [Rompe cadenas](http://algoquedaquedecir.blogspot.com/2018/01/rompe-cadenas.html) (parte asociada a informarse y no creerse ni difundir bulos)
* [RPTs y trasnparencia](https://algoquedaquedecir.blogspot.com/2018/07/rpts-y-transparencia.html) (asociado a que en altos cargos ellos deciden dónde se pone el dinero público, subiendo o bajando por ejemplo salarios de altos cargos o de funcionarios normales)
* [Ocupar puesto pagado con dinero público: libre concurrencia, igualdad, mérito, capacidad, publicidad y transparencia](https://algoquedaquedecir.blogspot.com/2017/12/ocupar-puesto-pagado-con-dinero-publico.html)  (asociado a que los salarios públicos y cómo se llega a cobrarlos debería ser información pública)
* [Muestras de información a medias](http://algoquedaquedecir.blogspot.com/2019/03/muestras-de-informacion-medias.html) (sobre el uso de valores medios, allí cito la idea de salario medio, pero este post lo escribí antes, pendiente de mover y unificar información) 
* [Docentes: su número y variación](https://algoquedaquedecir.blogspot.com/2020/09/docentes-su-numero-y-su-variacion.html) surge el tema de salario de docentes en distintas CCAA asociado a la dificultad para contratar de algunas CCAA

## Detalles

El post lo escribo en septiembre 2018 e inicialmente me centro en la parte de docentes que conozco y puedo comentar.

La parte sobre subida de impuestos la separo en post separado [Subida impuestos rentas altas](../post/subida-impuestos-rentas-altas)  

Se podría crear un post adicional relacionado con ambos que hablase sobre distribución de salarios (no solo educación, y no solo subida de impuestos)

### Agosto 2018: Sueldo docentes públicos y subida impuestos a rentas altas

Este tema lo conozco vía Twitter, donde lo comento.
Hay varias partes: el salario de los docentes públicos, la idea de salario medio y la subida de impuestos a rentas altas. Están mezcladas por cómo surge aunque intento separar y centrarme aquí en sueldo de docentes; puede haber información repetida en ambos posts.  

**30 agosto 2018 19:40**

[twitter PacoFerez/status/1035205550986993666](https://twitter.com/PacoFerez/status/1035205550986993666)  
Veo esto y pienso ![🤔](https://abs.twimg.com/emoji/v2/72x72/1f914.png "Rostro pensativo"): 1.-me están robando un dineral cada mes o el que ha documentado la noticia no tiene ni puñetera idea de lo que gana un profesor 😱  

Intentando parar el bulo, lo primero es informarse. Conozco los detalles de mi caso que cobro aproximadamente unos 30000€, pero desconozco el resto. En el post [Transferencias entre bancos y nóminas funcionarios Madrid](http://algoquedaquedecir.blogspot.com/2017/12/transferencias-entre-bancos-y-nominas.html) comparto el importe de mis últimas 12 nóminas si alguien las quiere ver.

Usando ideas que cito en post [Rompe cadenas](http://algoquedaquedecir.blogspot.com/2018/01/rompe-cadenas.html) (parte asociada a informarse y no creerse ni difundir bulos)  pongo la consulta a @Newtral, que responden rápidamente

> Buenos días. Según los datos de la  ***Encuesta de Estructura Salarial, del Instituto Nacional de Estadística*** , cuyos últimos datos se pueden consultar en este enlace https://www.ine.es/jaxi/Datos.htm?path=/t22/p133/cno11/serie/l0/&file=02001.px, el  ***salario medio en el sector de la Educación es de 22.289,73 euros al año*** , ligeramente por debajo del sueldo medio que se cobra en España (23.156,34 euros al año). Estas cifras son las últimas oficiales, pero corresponden al año 2016, por lo que podrían haber variado ligeramente.  
Además, hace unos meses el sindicato  ***UGT***  realizó un informe, que se puede consultar aquí https://educacion.fespugtclm.es/docentes-imprescindibles-estudio-salarial-de-los-docentes-en-espana-los-docentes-no-ven-reflejado-en-sus-salarios-las-mejoras-economicas-que-pregona-el-gobierno/), en el que indicaba que en 2017 el  ***salario medio de un maestro de primaria oscilaba entre los 1.975 euros al mes de Asturias y los 2.510 euros al mes de Ceuta y Melilla*** , es decir, entre los 27.650 y los 35.140 euros al año.  
Entre los profesores de secundaria, los salarios oscilan entre los 2.232 euros al mes de Asturias y los 3.002 de Ceuta y Melilla, es decir, entre los 31.248 euros al año y los 42.028 euros al año.  
En el caso de la Comunidad de Madrid, los maestros tienen un sueldo medio de 28.434 euros al año, mientras que el de los profesores de secundaria es de 32.144 euros al año.  
De esta forma,  ***ni los datos oficiales ni los informes sectoriales indican que los docentes tengan retribuciones medias cercanas a los 60.000 euros*.**   
Un saludo  

Respondo con copia a Telecinco

**31 agosto 2018 19:36**
[https://twitter.com/FiQuiPedia/status/1035567041904816129](https://twitter.com/FiQuiPedia/status/1035567041904816129)  
esa información es falsa, comparto información de @newtral
https://www.ine.es/jaxi/Datos.htm?path=/t22/p133/cno11/serie/l0/&file=02001.px  
https://educacion.fespugtclm.es/docentes-imprescindibles-estudio-salarial-de-los-docentes-en-espana-los-docentes-no-ven-reflejado-en-sus-salarios-las-mejoras-economicas-que-pregona-el-gobierno/  
@telecincoes debería rectificar  
@PacoFerez @feccoo @UGTMadrid  

También pongo un correo a mediasetcom@mediaset.es pidiendo que rectifiquen.

[twitter JUc3m/status/1035585540983349249](https://twitter.com/JUc3m/status/1035585540983349249)  
DE MEDIA  
El tema de MEDIA (aparece en la pantalla) es brutal: si el valor medio son 60000 € y muchos ganan mucho menos, supondría que habría gente que gana mucho más de 60000 € y es absurdo.  
Los conocimientos de estadística en los medios dejan que desear: el hombre de Telecinco dice *"el sueldo **medio** que cobran en nuestro país en la actualidad **muchos** médicos y profesores"* . Si quiere hablar del sueldo que cobran muchos, sería la moda.  

El tema de estadísticas y medias es un post pendiente, he citado algunas ideas en Twitter

En agosto 2015 surge esto  
[twitter voz_populi/status/627594663583592448](https://twitter.com/voz_populi/status/627594663583592448)  
El salario medio del sector público en 2014 fue un 50% más alto que el privado según la A.E.A.T  

Que respondo con este tuit citándolo y citando algunas ideas
[twitter FiQuiPedia/status/627749532009394176](https://twitter.com/FiQuiPedia/status/627749532009394176)  
Media no da información total  
[La mayoría de los trabajadores (62%) gana menos que la media](http://www.ciencia-explicada.com/2012/10/la-mayoria-de-los-trabajadores-62-gana.html)  

[Manipulación y desprestigo sector público para nuevos recortes: "El salario anual en el sector público fue un 30,8% más alto que el salario medio."](http://www.docentesconeducacion.es/viewtopic.php?f=68&t=1376&p=7137#p7137) @malaprensa  

![](https://pbs.twimg.com/media/CLY23kEWgAA_Gfq.jpg)

![](https://pbs.twimg.com/media/CLY23v4WEAAndaP.jpg)

En 2017 comento otra idea sobre media asociada a ratios, ver post [Ratios en educación](http://algoquedaquedecir.blogspot.com/2017/09/ratios-en-educacion.html)

[twitter FiQuiPedia/status/878587707383062529](https://twitter.com/FiQuiPedia/status/878587707383062529)  
media sin dispersión no da info real: si 9 personas cobran 0€ y otra 10€, la media es 1. Ratio legal primaria es 25 por grupo, no 25 media  
otro ejemplo: 9 personas viven 100 años y una fallece con 0; vida media son 90 años. En educación CRA suelen tener ratios bajos y baja media  

Pienso en el contexto de esa información **¿por qué difundir esa información falsa? ¿Desprestigiar a los docentes diciendo que cobran mucho?**  
Así que me voy a la fuente

El tema comienza en el minuto 49:00 de programa, donde se introduce el tema diciendo literalmente  

> "Un tema que preocupa a todos, que preocupa a la calle ¿Vamos o no vamos a pagar más impuestos?"

Seguidamente se introduce esta otra imagen (es anterior a la otra donde aparecen médicos y profesores)

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhDt0sS6r-GojITL3JRp-Bq0qdbS6G5Oq6s-H97omzrNsrCO93Mxom-lugrrx_pldxmWACEOqY6IgpPu-AEBVWzoN4qX9akHdG2-EO7BnGGa-vp6ufZYkjO5SN3TjJMFrHePPA2kCyfyRKb/s400/2018-08-30-YaEsMediodia2.jpg)

Y el que aparece en la imagen dice  

>"... a partir de rentas de 150000€. Pero unidos podemos pide que la subida afecte a partir de rentas de 60000€. Ojo: la ministra de hacienda, María Jesús Montero, se ha negado en rotundo a esta última opción ¿Y por qué? Pues por una razón. Lo ha dicho la propia ministra. **60000€ es el sueldo medio que cobran en nuestro país en la actualidad muchos médicos y profesores"**

En lugar de poner imágenes de la ministra diciendo eso que citan, ponen otras.  
Así que de nuevo hay que ir a la fuente  

**29 agosto 2018**

[El Gobierno y Podemos negocian subir el IRPF a las rentas de más de 150.000 euros - elpais](https://elpais.com/politica/2018/08/29/actualidad/1535556652_937621.html)  
Hacienda rechaza tajantemente subir a partir de 60.000, como pide Unidos Podemos, pero se abre a tocar a partir de 150.000, como prometió Sánchez en la oposición  
La ministra María Jesús Montero, según fuentes de la negociación, explicó a Podemos en la reunión que subir por encima de los 60.000 implicaría gravar más a algunos profesores de Universidad o médicos, y el Gobierno no tiene intenciones de hacer eso. Prefiere concentrarse en las subidas del impuesto de sociedades a las grandes empresas, por ejemplo.

Ahí dicen "según fuentes de la negociación", luego parece que no es algo que ha dicho oficialmente la ministra.  
Buscando más información sobre médicos (de docentes incluyendo universidad ya he puesto alguna referencia citando tuits)  

**10 junio 2018**

[Hacienda retiene hasta el 50% de las guardias de los médicos en España](https://www.redaccionmedica.com/secciones/sanidad-hoy/hacienda-retiene-hasta-el-50-de-las-guardias-de-los-medicos-en-espana-1761)  
En el caso de un  **médico con 40 años o más**  que es propietario de plaza y puede ganar sobre los  **60.000 euros brutos**  al año sin guardias, el porcentaje que retiene Hacienda de la guardia es aún mayor.  
...  
Los más perjudicados son los  **médicos**   **veteranos** , con  **más de 55 años y propietarios de su plaza.** Ellos pueden alcanzar los  **80.000 euros brutos**  al año sin guardias y al aplicarse el tipo impositivo del IRPF  **el valor de sus guardias cae un 45 por ciento.**   

Parece que se está manipulando información para oponerse a una subida de impuestos a las rentas más altas.  

Me remito al otro post [Subida impuestos rentas altas](../post/subida-impuestos-rentas-altas)  

**9 septiembre 2018**

[No, en España los maestros no ganan más que en Europa ni que en la OCDE. Ganan menos - diariodeuninterino](https://diariodeuninterino.wordpress.com/2018/09/09/no-en-espana-los-maestros-no-ganan-mas-que-en-europa-ni-que-en-la-ocde-ganan-menos/) (ver también [Los maestros españoles tienen un salario inferior a la media europea (corrección y matización del artículo anterior,o fe de erratas)](https://diariodeuninterino.wordpress.com/2018/09/10/los-maestros-espanoles-tienen-un-salario-inferior-a-la-media-europea-correccion-y-matizacion-del-articulo-anterioro-fe-de-erratas/)  

La tabla siguiente aparece en un [informe del ministerio de educación](http://www.mecd.gob.es/dctm/inee/eag/panorama2016okkk.pdf?documentId=0901e72b82236f2b) que muestra información que recoge la [OCDE sobre el salario de los profesionales de la educación en diferentes países](https://www.oecd-ilibrary.org/education/panorama-de-la-educacion_20795793). En esta tabla se indica que en España los maestros cobran en su primer año de trabajo una cantidad de 36.405 euros de media (no se cobra lo mismo en todas las comunidades autónomas). Esa cantidad está por encima de la media de lo que ganan los profesionales de la educación de la OCDE y de la UE. Pero **sucede una cosa, esa cantidad es falsa, es mentira y, sobre esa mentira, se construyen los bulos y ataques contra un profesorado cuyo salario mensual está bastante alejado de esa cantidad.**

![](https://diariodeuninterino.files.wordpress.com/2018/09/tabla-retribuciones.png)

La gráfica es 

>Gráfico y tabla 3.8 (extracto de la Tabla D3.1a y D3.3a):  
Retribución del profesorado (2014) (en $)  
Retribución anual del profesorado en instituciones públicas: inicial, tras 15 años de ejercicio profesional y máxima en la escala, por nivel educativo, en dólares convertidos mediante PPA (paridad del poder adquisitivo)  

en página 61 de http://www.mecd.gob.es/dctm/inee/eag/panorama2016okkk.pdf?documentId=0901e72b82236f2b

Es decir que además de la conversión a dólares, hay una conversión, que habría que revisar en detalle. Pero sin mirar más, la mayoría de la gente se quedará con el mensaje de que los docentes cobran mucho.

Y quizá, es una idea, precisamente ese valor de 57278 (sin unidades), haya sido utilizado por alguien para decir "cobran 60000 €"

Otras referencias al tema de esos 60000€

**31 agosto 2018 1:07**

[twitter Gauguinepicuro/status/1035287947036291074](https://twitter.com/Gauguinepicuro/status/1035287947036291074)  
Espera... espera ¿que los profesores ganamos 60000 euros? JAJAJAJAJAJAJAJAJAJJAAJAJAJJA

Creo que merece la pena algún comentario del hilo

[twitter CrazyFeelingFB/status/1035415716034473987](https://twitter.com/CrazyFeelingFB/status/1035415716034473987)  
Vamos a acabar con la polémica del "Algunos sí y algunos no" simplemente publicando la tabla de salarios de la Universidad.
QUEDARÁ TODO DEFINITIVAMENTE ZANJADO!!!! Ni catedráticos, ni leches!!!! ... 60.000 al año NI EL RECTOR!!!!  
![](https://pbs.twimg.com/media/Dl6JAgEW4AA5zoZ.jpg)  

[twitter mmhn60/status/1035635915329359873](https://twitter.com/mmhn60/status/1035635915329359873)  
Catedrático de bachillerato desde el 83  
Once trienios a las espaldas  
Ya lo publiqué ayer (2017; lo mismo nos tienen guardada una sorpresa en lo que queda de 2018)  
![](https://pbs.twimg.com/media/Dl9RXbMW0AAdd22.jpg)  

**Curiosidad 23 julio 2017**

[Noruega, el país donde nadie puede esconder su salario - bbc](https://www.bbc.com/mundo/noticias-internacional-40691744)  

### Enero 2019: Polémica sueldo docentes públicos

En este hilo aparecen datos de deciles con límites

[twitter amjorge15/status/1089887495708426240](https://twitter.com/amjorge15/status/1089887495708426240)  
Posición de los empleados públicos en la distribución de la renta. En los deciles 8, 9 y 10 suman 63,4%, % similar a si miramos la distribución salarial salvo que, en el 10, pasan de 20,9% a 24,3%. Es decir, casi 1 de cada 4 de los empleados públicos está en el 10% que más gana.  
![](https://pbs.twimg.com/media/DyANoMXX4AAm8hF?format=jpg)  
Como es habitual la fuente es elaboración propia a partir de los microdatos de la ECV, concretamente la variable PL230T2 del fichero de personas.  
...  
[twitter amjorge15/status/1090017707913867267](https://twitter.com/amjorge15/status/1090017707913867267)  
Para ponerlo con más perspectiva incluyo el total (el gráfico del principio del hilo). Ahora darle una vuelta al conflicto salarial que generó de la marea verde  
![](https://pbs.twimg.com/media/DyCFF9wWoAAVqUN?format=jpg)  
Es que me da la risa, que yo hice el instituto con la marea verde, y ahora veo esto. Imaginad cómo esta la cosa que un colectivo distribuido de este modo peto la calle y consiguió que muchísima gente defendiese y compartiese que durante una crisis brutal no se les podía tocar.  
No esperaba para nada que estuvieran TAN arriba, es que es 1/3. En fin, con esto ya si que cierro el hilo, mañana más y mejor.  
Los limites de los deciles son estos
![](https://pbs.twimg.com/media/DyGCWLFXgAA56Ig.jpg)  


[twitter amjorge15/status/1090265331732340736](https://twitter.com/amjorge15/status/1090265331732340736)  
Aquí se estudia la brecha salarial entre empleados públicos y privados tratando de explicarla por varios factores

[Boletín económico 02/2015 Banco de España](https://www.bde.es/f/webbde/SES/Secciones/Publicaciones/InformesBoletinesRevistas/BoletinEconomico/15/Feb/Fich/be1502.pdf)  

Página 20 

> Las diferencias salariales entre los sectores público y privado en España  
*Los datos indican que los salarios-hora en el período de 2005‑2012 fueron en promedio un 36% mayores en el sector público que en el sector privado. Sin embargo, este cuadro muestra también que los empleados del sector público tienen, en promedio, más edad, más cualificación y mayor antigüedad en sus puestos de trabajo, y trabajan a tiempo completo con mayor frecuencia. Estas características diferenciales apuntarían, en principio, a unos salarios más elevados.*  

La polémica surge por cómo se habla del conflicto de los recortes

[twitteroctavio_pr/status/1090618420503154688](https://twitter.com/octavio_pr/status/1090618420503154688)  
Este twiit se basa en ideas falsas (la marea verde no defendía nuestros salarios sino que no despidieran a miles de docentes, no aumentara ratio ni se recortara en educación):  

Me temo que no en tanto que los medios no se redujeron [Recortes en educación: un conflicto laboral - larazon.es](https://www.larazon.es/amp/opinion/columnistas/recortes-en-educacion-un-conflicto-laboral-GE12072271/)  
Vale, pero en ese caso no tiene queja respecto a mi primer tweet y principal conclusión, que es que aumentar salarios públicos aumenta la desigualdad  

Se cita artículo de 28 febrero 2016 de Juan Ramón Rallo

[twitter octavio_pr/status/1090619341492633600](https://twitter.com/octavio_pr/status/1090619341492633600)
Datos: 3000 profesores menos en mi comunidad. 7 menos en mi instituto (aumento de ratio y de horas lectivas - que es una bajada de salario real-). Pero no hubo recortes. Ya.  

[twitter amjorge15/status/1090619751917850624](https://twitter.com/amjorge15/status/1090619751917850624)  
Si, pero no de medios, tiene datos nacionales en el artículo que engloban su caso (si es que es cierto) y el del resto, si tiene dudas, consulte los anuarios estadísticos del ministerio  

Pongo unas frases del artículo de Juan Ramón Rallo

> Hace unos días, el Ministerio de Educación publicó su anuario estadístico de «Las cifras de la educación en España», correspondiente al ejercicio 2013.  
...  
Asimismo, la plantilla de profesores no universitarios también crece desde 466.674 hasta 475.516.  
...  
He ahí la clave de las mediatizadas protestas de los funcionarios: en 2013, el profesorado tenía una mayor carga de trabajo y cobraba menos que en 2007. Lo que se nos ha vendido como un desmantelamiento de la educación pública es, fundamentalmente, un conflicto laboral entre el Ministerio (y las Consejerías) de Educación y los empleados públicos del ramo, los cuales tratan de asociar en el imaginario colectivo sus (algo peores) condiciones laborales con un desmantelamiento de la educación pública. No se ha producido tal cosa: si las mareas verdes quieren reclamar mayores sueldos y menores horas de trabajo a pesar de la crisis, sólo tienen que publicitarlo sin camuflarlo como una destrucción de la educación pública. Claro que entonces probablemente no cosecharían tantas simpatías ni se generaría tanto alarmismo populista a su alrededor.

Vayamos a los datos (publicados días antes que el artículo)

**[Las cifras de la educación en España. Curso 2013-2014 (Edición 2016)](https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/estadisticas/indicadores/cifras-educacion-espana/2013-2014.html)**

Se presenta el anuario estadístico Las cifras de la educación en España. Estadísticas e indicadores correspondientes al curso 2013-2014. Esta publicación de síntesis es elaborada por la Subdirección General de Estadística y Estudios del Ministerio de Educación, Cultura y Deporte y proporciona una panorámica de la situación de la educación española, a través de datos relevantes e indicadores, provenientes de distintas fuentes estadísticas.

Fecha de Publicación: 05 de febrero de 2016

En página 2, profesorado total públicos 617.420

Ahí hay datos de 2013-14, no se compara con 2007-08

[Las cifras de la educación en España. Curso 2007-2008 (Edición 2010)](https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/estadisticas/indicadores/cifras-educacion-espana/2007-2008.html)  

En página 2, profesorado total públicos  603.387

Efectivamente ha aumentado profesorado de 2007 a 2013, 603.387 a  617.420 (no los 466.674 hasta 475.516 que dice artículo)

Al verlo pienso que puede ser un error porque si se está hablando de funcionarios con plaza, y el número no puede bajar más allá de jubilaciones, sin contar los interinos.

Sin embargo veo esto, que con datos sí baja y bastante, luego se deben reflejar los interinos, aunque no es posible si hay sustituciones, y debe estar contabilizando las plantillas de centros incluyendo los interinos.

[twitter octavio_pr/status/1090664092380119042](https://twitter.com/octavio_pr/status/1090664092380119042)  
Datos oficiales de la Junta de Castilla La Mancha, número de profesores: Curso 2010-2011: 35.715 docentes. Curso 2015-2016; 31.425 docentes. Ya la resta le dejo que la haga usted mismo.

Años más tarde escribo el post [Docentes: su número y variación](https://algoquedaquedecir.blogspot.com/2020/09/docentes-su-numero-y-su-variacion.html), pendiente revisar  


### Septiembre 2024: Polémica sueldo docentes públicos con datos OCDE

**10 septiembre 2024**

Los datos de OCDE se publican 10 septiembre 2024

[Education at a Glance 2024](https://www.oecd.org/en/publications/education-at-a-glance-2024_c00cad36-en.html)  

[Education at a Glance 2024 (PDF 498 páginas)](https://www.oecd.org/content/dam/oecd/en/publications/reports/2024/09/education-at-a-glance-2024_5ea68448/c00cad36-en.pdf)  

Surgen artículos con reacciones lógicas de incredulidad  

[Los mitos sobre el profesorado español que desmiente la OCDE: ni trabajan más días ni tienen las peores ratios - abc.es](https://www.abc.es/sociedad/mitos-sobre-profesorado-espanol-desmiente-ocde-horas-20240910200718-nt.html)  

> Lo que no se puede decir es que los docentes españoles estén mal pagados. España se sitúa por encima de la media de la OCDE y también de la Unión Europea. **Los salarios en Primaria van desde los 51.280 a los 73.536, mientras que en la UE esta horquilla está entre los 40.810 y 67.285 euros respectivamente. En la ESO, los salarios de nuestros profesores se encuentran entre los 57.427 y los 82.111, frente a una media de 42.327 y 69.994 en la UE.** Cabrales indica a ABC que el informe hace una comparación entre personas con un nivel de estudios similar y que, además, la variable está relativizada con el nivel de precios del país. "No estamos mal a nivel salarial, pero la progresión de los sueldos se estanca antes que en otros países. Es al inicio de la carrera de maestro donde estamos mejor que muchos".   

[El profesorado español da más horas de clase y tiene más estudiantes en el aula que la media europea - elpais](https://elpais.com/educacion/2024-09-10/el-profesorado-espanol-da-mas-horas-de-clase-y-tiene-mas-estudiantes-en-el-aula-que-la-media-europea.html)  
> La OCDE calcula el salario medio del profesorado en bruto, incluyendo los complementos y las aportaciones que los docentes hacen a la seguridad social y al régimen de pensiones (pero no la parte que desembolsan sus empleadores) al inicio de su carrera y respecto al máximo que podrían alcanzar por etapas. **España se sitúa por encima de la UE en ambos casos en todas las etapas educativas. De 51.280 dólares (el cambio a finales de 2023, que es el año que recoge el informe de la OCDE, era de 0,9 dólares por cada euro; es decir, que en este caso sumarían 46.152 euros) al principio a 73.536 como máximo en Primaria, frente a un promedio comunitario de 40.810 y 67.285 dólares respectivamente. Y, en la ESO, de 57.427 dólares a 82.111, frente a una media de 42.327 y 69.994 en la UE.** Pero para alcanzar dichos máximos, en España son necesarios muchos más años, 39, que en el promedio comunitario, que es de 32. En términos reales (una vez ajustada la inflación), los salarios de los docentes españoles han aumentado un 2% en los últimos ocho años, frente a un 4% de promedio en la OCDE.  


Me recuerda esto de 2018 que ya citaba en el post 

[No, en España los maestros no ganan más que en Europa ni que en la OCDE. Ganan menos - diariodeuninterino](https://diariodeuninterino.wordpress.com/2018/09/09/no-en-espana-los-maestros-no-ganan-mas-que-en-europa-ni-que-en-la-ocde-ganan-menos/) (ver también [Los maestros españoles tienen un salario inferior a la media europea (corrección y matización del artículo anterior,o fe de erratas)](https://diariodeuninterino.wordpress.com/2018/09/10/los-maestros-espanoles-tienen-un-salario-inferior-a-la-media-europea-correccion-y-matizacion-del-articulo-anterioro-fe-de-erratas/)  

Yendo a los datos, [Education at a Glance 2024 página 381 del pdf, 371 de número](https://www.oecd.org/content/dam/oecd/en/publications/reports/2024/09/education-at-a-glance-2024_5ea68448/c00cad36-en.pdf#page=381)  

> Figure D3.2. Lower secondary teachers’ average actual salaries compared to the statutory minimum and maximum salaries (2023)  
Annual salaries of teachers in public institutions, in equivalent USD converted using PPPs for private consumption  

Se usa [Purchasing power parities (PPPs)](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:Purchasing_power_parities_(PPPs))  
> Purchasing power parities, abbreviated as PPPs, are indicators of price level differences across countries. PPPs tell us how many currency units a given quantity of goods and services costs in different countries. Using PPPs to convert expenditure expressed in national currencies into **an artificial common currency, the purchasing power standard (PPS)**, eliminates the effect of price level differences across countries created by fluctuations in currency exchange rates.   

La definición de [Purchasing power standard (PPS)](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:Purchasing_power_standard_(PPS))  
> The purchasing power standard, abbreviated as PPS, is an artificial currency unit. Theoretically, one PPS can buy the same amount of goods and services in each country.  

Habría que mirar detalle

[Eurostat database > Data navigation tree > Detailed datasets > Economy and finance > prices > Purchasing power parities](https://ec.europa.eu/eurostat/web/main/data/database)  



El Ministerio publicó un informe en castellano el mismo día 10

[La escolarización de 0 a 3 años en España sube diez puntos en una década y supera las medias de la OCDE y la UE](https://www.educacionfpydeportes.gob.es/prensa/actualidad/2024/09/20240910-panoramaeducacion2024.html)  

Enlaza [Panorama de la educación Indicadores de la OCDE Informe español Ministerio de Educación, Formación Profesional y Deportes](https://www.educacionfpydeportes.gob.es/dam/jcr:5ce07faf-1447-44c1-af46-26de8a7a9820/panorama-de-la-educaci-n-2024-2.pdf)  

En página 111  
> Gráfico y Tabla 3.7 (extractos de la Tabla D3.1)  
Retribución del profesorado (2023)  
Salario reglamentario anual del profesorado en instituciones públicas: inicial, tras 15 años de ejercicio profesional y máxima en la escala, por nivel educativo, en dólares estadounidenses convertidos mediante PPA (paridad del poder adquisitivo)

Y las cifras que aparecen son las mismas que la de los artículos de abc y elpais  
![](https://pbs.twimg.com/media/GXYiexkWAAA6AuW?format=jpg)  


Mirando [Spain - Teachers' salaries and allowances - eurydice](https://eurydice.eacea.ec.europa.eu/countries/spain/teachers-salaries-and-allowances)  
![](https://pbs.twimg.com/media/GXYl54iXQAA7hoJ?format=jpg)  


## Otros temas

Pendiente un post / información explicando la nómina de un docente público: el nivel de funcionario que se tiene, complementos, diferencias con otros niveles por tema de vacaciones, diferencias entre comunidades autónomas; informarse y documentar enlaza con pedir argumentadamente una equiparación entre funcionarios docentes, como sí han conseguido funcionarios de cuerpos de seguridad en 2018. Otro tema sería comparar nómina docentes públicos con nómina de docentes en privados con concierto.

También fuentes de datos de salarios de docentes como

[Teachers' salaries. Teachers' statutory salaries - eurydice](https://eurydice.eacea.ec.europa.eu/data-and-visuals/teachers-statutory-salaries)   
> Spain: salaries are weighted averages of salaries at the regional level (comunidades autónomas).  


Los docentes son grupo A según EBEP

[Artículo 76. Grupos de clasificación profesional del personal funcionario de carrera.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a76)

Los cuerpos y escalas se clasifican, de acuerdo con la titulación exigida para el acceso a los mismos, en los siguientes grupos:

Grupo A: Dividido en dos Subgrupos, A1 y A2.

Para el acceso a los cuerpos o escalas de este Grupo se exigirá estar en posesión del título universitario de Grado. En aquellos supuestos en los que la ley exija otro título universitario será éste el que se tenga en cuenta.

La clasificación de los cuerpos y escalas en cada Subgrupo estará en función del nivel de responsabilidad de las funciones a desempeñar y de las características de las pruebas de acceso.

[Ley 6/2018, de 3 de julio, de Presupuestos Generales del Estado para el año 2018.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-9268)

[Artículo 18. Bases y coordinación de la planificación general de la actividad económica en materia de gastos de personal al servicio del sector público.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-9268#ar-18)

Cinco. 1. Los funcionarios a los que resulta de aplicación el artículo 76 del Texto Refundido de la Ley del Estatuto Básico del Empleado Público, aprobado por Real Decreto Legislativo 5/2015, de 30 de octubre (en adelante, EBEP), e incluidos en el ámbito de aplicación de la Ley 30/1984, de 2 de agosto, de medidas para la reforma de la Función Pública, en los términos de la disposición final cuarta del EBEP o de las Leyes de Función Pública dictadas en desarrollo de aquél, percibirán, en concepto de sueldo y trienios, en las nóminas ordinarias de enero a diciembre de 2018, las cuantías referidas a doce mensualidades que se recogen a continuación:

|Grupo/Subgrupo EBEP|Sueldo (euros)|Trienios (euros)|
| --- | --- | --- |
|A1|13.780,08|530,16|
|A2|11.915,28|432,24|
|B|10.415,52|379,32|
|C1|8.946,36|327,12|
|C2|7.445,76|222,60|
|E (Ley 30/1984) y Agrupaciones Profesionales (EBEP)|6.814,80|167,52|

2. Los funcionarios a que se refiere el punto anterior percibirán, en cada una de las pagas extraordinarias de los meses de junio y diciembre en el año 2018, en concepto de sueldo y trienios, los importes que se recogen a continuación:

|Grupo/Subgrupo EBEP|Sueldo (euros)|Trienios (euros)|
| --- | --- | --- |
|A1|708,61|27,26|
|A2|724,16|26,26|
|B|750,16|27,33|
|C1|644,35|23,54|
|C2|614,82|18,37|
|E (Ley 30/1984) y Agrupaciones Profesionales (EBEP)|567,90|13,96|

[ANEXO IV Módulos económicos de distribución de Fondos Públicos para sostenimiento de Centros Concertados](https://www.boe.es/buscar/act.php?id=BOE-A-2018-9268#ai-4)

**27 agosto 2019**

[La diferencia de ser profesor en la escuela concertada o en la pública: 52.000 euros](https://amp.elmundo.es/comunidad-valenciana/2019/08/27/5d641529fdddffbf818b4605.html)  

De hecho, según un informe del propio sindicato, el dinero que cada profesor de la concertada con más de 25 años de antigüedad deja de percibir en relación a un docente de la pública asciende a  **52.000 euros** . Se trata de la cifra que resulta de la diferencia entre la paga de antigüedad a la que sí tienen derecho los docentes de la concertada una vez cumplen 25 años de servicio (aunque a día de hoy hay que  **reclamarla por la vía judicial** ) y los sexenios a lo largo de ese tiempo.

Respondo aquí citando este post, y un tema precisamente de trienios que cobran solamente en centros privados con concierto

[twitter FiQuiPedia/status/1166998488665776130](https://twitter.com/FiQuiPedia/status/1166998488665776130)  
Pueden @ndelatower o @FSIE_CV  documentar esa diferencia de 52000€? doy datos de que  supondría duplicar salario en  la pública.  
PD: en Madrid la paga de 25 años con dinero público se "regala" a religiosos de privados con concierto.

Lo que dicen es que esa diferencia es en 25 años,

[FSIE denuncia que los docentes de la concertada han perdido 52.000 euros](https://www.levante-emv.com/comunitat-valenciana/2019/08/27/fsie-denuncia-docentes-concertada-han/1915318.html)  
El sindicato FSIE-Comunitat Valenciana, mayoritario en la enseñanza [concertada](https://www.levante-emv.com/tags/ensenanza-concertada.html), ha elaborado un estudio en el que afirma que la diferencia entre la paga de antigüedad y los sexenios a lo largo de  **25 años**  es de aproximadamente  **52.000 euros** , por lo que «ese es el dinero que cada profesor de la concertada con más de 25 años de antigüedad ha dejado de recibir por la falta de acuerdo».

Pero redacción de el mundo indica diferencia y se interpreta que es anual; se aclara en otra frase.  
En el otro artículo se indica A LO LARGO en la misma frase que los 52000.  
A lo largo de 25 años son 2080€ anuales, 149€ en 14 pagas mensuales.

**6 septiembre 2019**

[¿Dónde cobran más los maestros? - marca.com](https://www.marca.com/tiramillas/actualidad/2019/09/06/5d728b2ae2704ee8878b465c.html)  
Esta infografía de Statista muestra cómo, en el caso de los maestros de primaria, Luxemburgo es el país en el que estos reciben un mayor sueldo bruto, al menos de los 32 Estados comparados por la organización de desarrollo (diez de ellos mostrados en el gráfico).  
![](https://e00-marca.uecdn.es/assets/multimedia/imagenes/2019/09/06/15677874131628.jpg)  
Esto sucede tanto en el caso del sueldo inicial, este de más de 63.500 euros anuales, como tras quince años de experiencia, con casi 92.800 euros. Curiosamente este sueldo es superior al salario del presidente del Gobierno de España, al que según los presupuestos de 2019 (todavía no aprobados) correspondería menos de 83.000 euros en concepto de asignación.

En octubre 2024 veo esto sobre sueldo de médicos de atencioń primaria, y es un ejemplo más de cómo la propia administración miente al dar datos sobre sueldo de sus funcionarios  

[twitter CiudadanO_O/status/1841884120512639481](https://x.com/CiudadanO_O/status/1841884120512639481)  
¿Os acordáis de que los médicos de Madrid "ganan de media 76.000€ y es la segunda comunidad que mejor paga"?   
Pues registré en agosto una solicitud de acceso a información pública vía Transparencia sobre sus fuentes y les pedía DATOS   
Y han contestado...  

Cita esta noticia de agosto 2024 

[La Comunidad de Madrid lanza una campaña para favorecer el retorno de médicos a la región y atraer especialistas extracomunitarios a Atención Primaria - comunidad.madrid](https://www.comunidad.madrid/noticias/2024/08/06/comunidad-madrid-lanza-campana-favorecer-retorno-medicos-region-atraer-especialistas-extracomunitarios-atencion-primaria)  
> el Ejecutivo autonómico recuerda que Madrid es la segunda autonomía de España que mejor retribuye a los facultativos de los centros de salud, con un salario medio al año de alrededor de 76.000 euros.  


[Descifrando la brecha de salarios: ¿cobran mucho los funcionarios o las empresas pagan poco? - elconfidencial](https://www.elconfidencial.com/economia/2024-10-12/brecha-salarios-cobran-mucho-funcionarios-empresas-pagan-poco_3980593/)  
> Un estudio publicado por el BdE ha generado un debate en torno a las retribuciones. Los salarios privados están en línea con la baja productividad que tiene España, pero las retribuciones públicas son inusualmente altas

>En el caso de España, el peso del sector público sobre el total de la economía está entre los más bajos de Europa. Por ejemplo, el gasto supone un 46,5% del PIB, frente a casi el 50% en el conjunto de la eurozona. Sin embargo, el gasto en sueldos públicos es superior al europeo: España destinó, en 2023, el 11% del PIB al pago de salarios públicos, frente al 10% de la eurozona. 

> Si España tiene una plantilla pública reducida, pero, al mismo tiempo, gasta por encima de la media europea en salarios públicos (medidos en porcentaje del PIB), significa que los salarios son más generosos que la media europea. 


[The public-private wage GAP in the euro area a decade after the sovereign debt crisis - bde.es](https://www.bde.es/wbe/es/publicaciones/analisis-economico-investigacion/documentos-trabajo/the-public-private-wage-gap-in-the-euro-area-a-decade-after-the-sovereign-debt-crisis.html)  


