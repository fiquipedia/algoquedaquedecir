+++
author = "Enrique García"
title = "Transparencia oposiciones: enunciados inspección educativa"
date = "2022-02-20"
tags = [
    "educación", "Madrid", "transparencia", "oposiciones", "inspección", "Post migrado desde blogger"
]
toc = true

+++

Revisado 24 julio 2024

### Resumen

En 2022 decido buscar transparencia en enunciados oposiciones inspección, tras  haber buscado [transparencia con los enunciados de oposiciones docentes de 2018](https://algoquedaquedecir.blogspot.com/2018/08/oposiciones-transparencia-enunciados.html), y [transparencia en la valoración oposiciones de 2021](https://algoquedaquedecir.blogspot.com/2021/03/oposiciones-transparencia-valoracion.html), descubriendo en 2021 un punto oscuro y peligroso: se empieza a denegar información de oposiciones docentes y de inspección.  

En 2022 consigo una resolución estimatoria del consejo de transparencia de Madrid que cambia el criterio del consejo de transparencia estatal, y en 2024 Madrid, que había denegado enunciados de inspección, los publica de forma activa ya sin reclamar.

Posts relacionados:

[Comisiones servicio Madrid: inspección educativa](https://algoquedaquedecir.blogspot.com/2022/02/comisiones-servicio-madrid-inspeccion-educativa.html) (cito la idea " los inspectores son los responsables de que se cumpla la legislación educativa: ratios, cuotas en privados con concierto, horarios, ... ")

## Detalle

Tras el "éxito" de 2018 con enunciados, en 2021 no solo no tengo éxito con la añadir transparencia en la valoración, sino que me encuentro un caso de menos transparencia en enunciados: CLM, que habían facilitado enunciados en 2018, no lo hace en 2021. Utiliza una sentencia de 2019 sobre oposiciones Defensa, y aunque reclamo a CTBG, no prospera.

La información de CTBG

[Ministerio de Defensa. Acceso a pruebas y plantillas de resultados de procesos selectivos - consejodetransparencia.es](https://www.consejodetransparencia.es/ct_Home/va/Actividad/recursos_jurisprudencia/Recursos_AGE/2018/117_MDefensa_4.html)  

Todo el tema lo comento comenzando en julio 2021 aquí

[Transparencia oposiciones y sentencia Ministerio Defensa - civio](https://comunidad.civio.es/t/transparencia-oposiciones-y-sentencia-ministerio-defensa/1694)  

[Resolución de CTBG RT0691/2021](https://www.consejodetransparencia.es/ct_Home/dam/jcr:a3f8c0c5-43fe-4157-b977-39e141859369/RT_0691_2021.pdf)  

Y aquí resto de documentación

[Transparencia oposiciones - 2021 CLM](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/TransparenciaOposiciones/2021-CLM)

En esa carpeta se incluye el documento de argumentación usado al reclamar a CTBG, que en mi opinión CTBG ignoró. 

Se da la paradoja que de que la misma administración de CLM sí facilita los enunciados de inspección educativa

[Transparencia oposiciones - Inspección - 2018 CLM](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/TransparenciaOposiciones/Inspecci%C3%B3n/2018-CLM)  

Aprovechando que Madrid, que sí facilita en 2021 y en 2018 (de hecho publica de forma activa) los enunciados de oposiciones docentes, también deniega los enunciados de oposiciones a inspección de 2019 a otra persona, creo que procede dedicar esfuerzo a intentar romper ese giro a la opacidad. Además en este caso de Madrid responderá CTyP y no CTBG.

Lo primer agradecer a la persona que durante 2021 pide enunciados de inspección y los comparte. Los comparto en

[Transparencia oposiciones - Inspección](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/TransparenciaOposiciones/Inspecci%C3%B3n)  

La resolución de CTBG de enero 2022 en la que se deniegan enunciados inspección Madrid es [RT_0754_2021](https://www.consejodetransparencia.es/ct_Home/dam/jcr:db9e9e53-ea3f-4b35-8fb2-47bfc6efb8cf/RT_0754_2021.pdf)  

Y la resolución utiliza el mismo argumento de la sentencia de Defensa y resoluciones CTBG que usan esa sentencia de Defensa.

Creo que conseguir que CTyP emita resolución estimatoria que aclare que no hay que aplicar la sentencia de Defensa puede ser positivo para la transparencia, ya que rebatiría lo dicho por CTBG.

Planteo texto de solicitud de transparencia. No entro en solicitar documentación para objetivar correcciones.

> Solicito copia o enlace a los enunciados de los ejercicios de la oposición al Cuerpo de Inspectores de Educación asociados a la convocatoria publicada 9 octubre 2018 en BOCM http://www.bocm.es/boletin/CM_Orden_BOCM/2018/10/09/BOCM-20181009-12.PDF.  
En caso de que se inadmita la presente solicitud utilizando como argumentación la sentencia nº 120/2019 del Juzgado Central de lo Contencioso nº 5, en el procedimiento ordinario nº 58/2018 que aplica a oposiciones del Ministerio de Defensa, que a su vez se ha usado en distintas resoluciones del Consejo de Transparencia y Buen Gobierno, se solicita copia o enlace, si existe, al análisis jurídico que justifique por qué esos argumentos son válidos para los enunciados de oposiciones de inspección pero no lo son para los enunciados de oposiciones docentes, que la consejería ha publicado de forma activa en 2021, fecha posterior a la sentencia citada, dado que se puede considerar mayor la similitud entre oposiciones dentro de la misma consejería (a inspector y a docente) que la similitud entre oposiciones entre administraciones distintas (Ministerio de Defensa y Comunidad de Madrid)

09/291855.9/22

**25 febrero 2022**

Recibo [resolución inadmitiendo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/TransparenciaOposiciones/Inspecci%C3%B3n/2019-Madrid/2022-02-25-Resolucion09-OPEN-00014.5-2022.pdf)

Citan la sentencia de Defensa como me imaginaba.

En mi opinión creo que la respuesta supone admitir que dicho análisis jurídico no existe, y que el hecho de no facilitar enunciados en este caso cuando sí lo hacen para oposiciones no es una actuación basada en un análisis jurídico sino en un "juicio de valor de la administración pública" que me dan de manera indirecta.

**9 marzo 2022**

Envío reclamación a CTyP

En formulario con texto breve indico

>"La resolución inadmite aplicando artículo 18.1.e sin realizar una adecuada valoración de este caso, como se reconoce al indicar que no existe un informe jurídico.  
La resolución se limita a citar una sentencia del Ministerio de Defensa y a indicar que existe similitud, sin argumentarse adecuadamente.
La falta de similitud de la sentencia utilizada se argumenta en esta reclamación.  
Considerando la importancia para la Transparencia de que los enunciados de procesos selectivos en educación sean en un futuro, como lo han sido hasta ahora, información sobre la que cualquier ciudadano pueda ejercer y obtener el derecho de acceso, esta reclamación realiza una argumentación detallada y extensa en documento separado que incluye un índice, al tiempo que este documento en formato electrónico tiene enlaces internos para referenciar elementos relacionados"

Adjunto [documento de argumentación separado (25 páginas)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/TransparenciaOposiciones/Inspecci%C3%B3n/2019-Madrid/2022-03-09-argumentaci%C3%B3nCTyPEnunciadosOposicionesInspecci%C3%B3n-09-OPEN-00014.5-2022.pdf)

**10 marzo 2022**

Recibo respuesta CTyP con documento firmado día 10, RDACTPCM079/2022

**17 junio 2022**

Recibo [alegaciones enviadas a CTyP día 3 junio 2022, fechadas 2 junio 2022](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/TransparenciaOposiciones/Inspecci%C3%B3n/2019-Madrid/2022-06-03-Alegaciones_79_22.pdf)

Respondo mismo día

>Respondo realizando comentarios.  
En antecedente tercero indican "Acompaña un documento de 25 páginas en el [que] realiza una extensa y sigo reiterativa, cuando no confusa, exposición...sobre la inaplicabilidad"  
Aportan ese juicio de valor, sin aportar detalles sobre qué consideran confuso.  
En la alegación Primera.A no rebaten ninguno de los argumentos de esas 25 páginas sobre la inaplicabilidad de la sentencia, limitándose a enumerar resoluciones de CTBG donde se ha aplicado dicha sentencia y valoraciones de CTBG sobre otros casos. Se limitan a hablar de la idea general de similitud que CTBG usa en resoluciones y a aplicarla de nuevo a este caso sin valorar los detalles de falta de similitud e inaplicabilidad aportados y sin rebatirlos.  
En la alegación Primera.B entiendo que alegan que no existe informe jurídico, lo que a nivel de transparencia al no ser un documento existente bastaría, ya que en mi solicitud indicaba "copia o enlace, SI EXISTE". En mi solicitud intentaba reflejar que por lo que respondieron venían a reconocer que no existía, y queria esa confirmación para así intentar visualizar la necesidad de que el Consejo de Transparencia y Participación valorase "el análisis jurídico" aportado en mi argumentación de 25 páginas.  
En la alegación Segunda entiendo que alegan que no se ha interpuesto contencioso sobre otras resoluciones (alguna no es mía). Considero que interponer o no contencioso, algo que tiene un coste en tiempo y dinero que no cualquiera puede asumir, sobre otras resoluciones no tiene nada que ver con el análisis y valoración de la argumentación de esta reclamación.  
En la alegación Tercera y última se limitan a reproducir íntegramente parte de la sentencia de Defensa, sin rebatir ninguno de los argumentos de mi documento de 25 páginas.  
Mi resumen es que el que CTBG haya tomado un criterio de aplicar sentencia de Defensa a otros casos no implica que sea necesariamente correcto ni que CTyP de Madrid deba aplicarlo sin más a este, ya que cada Consejo es independiente y establece sus criterios no siempre coincidentes. Los consejos se apoyan en las resoluciones y criterios de otros Consejos en función de las argumentaciones realizadas, y creo que este es un caso donde, analizando o rebatiendo mis argumentaciones sobre la sentencia, puede fijar un criterio distinto.  
Considero que la falta de transparencia en procesos selectivos a la administración educativa, reflejada en artículo 55.2.b EBEP, que supone la falta de acceso a los enunciados, cuando hasta la sentencia se facilitaban, supone un retroceso en la transparencia debido a un error de CTBG que CTyP puede corregir.  


**18 junio 2022**

 Envío correo adicional a CTyP

>Como continuación de mi argumentación que muestra la inconsistencia de la negativa de Madrid de facilitar acceso a enunciados, Madrid ha publicado de forma activa hoy mismo, día de las pruebas, los enunciados de oposiciones de educación primaria https://www.comunidad.madrid/servicios/educacion/documentacion-procesos-selectivos-oposiciones-2022-maestros#examenes-junio-2022

**21 julio 2022**

Envío correo adicional a CTyP

>Como continuación de las alegaciones realizadas envío enlaces a ejecuciones recientes de consejerías de de educación / resoluciones de transparencia de consejos de transparencia que argumentan el derecho de acceso a los enunciados de oposiciones y a la documentación manejada por los tribunales para objetivar la corrección  
[País Vasco](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/TransparenciaOposiciones/2021-Pa%C3%ADsVasco/2022-07-15-Resoluci%C3%B3n58-2022Expediente2022-000233.pdf)  
Analizada la petición trasladada, el Departamento de Educación va a proceder a publicar de manera general todas las plantillas de corrección disponibles relativas a las distintas especialidades. Estando el proceso de publicación actualmente en curso, estarán disponibles para su consulta el 15 de julio de 2022, lo que le comunico para su conocimiento y efectos oportunos”.  
En consecuencia, procede declarar el derecho de la persona reclamante a las soluciones, plantillas correctoras u otra documentación manejada por los tribunales para objetivar el ejercicio de su discrecionalidad técnica, en la medida que esta documentación exista efectivamente; en caso de no existir tal documentación, es preciso que así se manifieste formalmente por parte del Departamento de Educación  
[Murcia](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/TransparenciaOposiciones/2021-Murcia/2022-07-21-02%20-%20Orden%20Consejera%20(R106-2021)%20(COPIA).pdf)  
>- En la resolución de fecha 7 de abril de 2022 del Consejo de la Transparencia de la Región de Murcia en el fundamento de derecho quinto se expresa que “En consecuencia, procede declarar el derecho de la persona reclamante a la plantilla correctora u otra
documentación manejada por los tribunales para objetivar el ejercicio de su discrecionalidad
técnica, en la medida que esta documentación exista efectivamente; en caso de no existir tal
documentación, es preciso que así se le manifieste formalmente por parte del Departamento
reclamado”, pongo de manifiesto que no consta más información que la remitida en el enlace
anteriormente referido”.

**7 noviembre 2022**

[twitter FiQuiPedia/status/1589577485871910912](https://twitter.com/FiQuiPedia/status/1589577485871910912)  
RDA120/2022 estimatoria CTyPCM que anula el criterio de @ConsejoTBG de denegar acceso a enunciados oposiciones aplicando erróneamente la sentencia de @Defensagob  
@educacmadrid deberá dar enunciados de inspección

[RDA120/2022 anonimizada](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/TransparenciaOposiciones/Inspecci%C3%B3n/2019-Madrid/2022-11-03-ResolucionCTyPM-RDA120_2022_FDO-anonimizado.pdf)  

Esa sentencia me parece un retroceso en la transparencia: tras conseguir en 2018 acceso a documentos oposiciones de todas las CCAA, ahora lo impide, y me recuerda lo de la limitación temporal, también de @Defensagob

[El Supremo da la razón a Civio y todas las administraciones tendrán que dar acceso a información anterior a la entrada en vigor de la Ley de Transparencia - civio](https://civio.es/novedades/2020/03/03/el-supremo-da-la-razon-a-civio-y-todas-las-administraciones-tendran-que-dar-acceso-a-informacion-anterior-a-la-entrada-en-vigor-de-la-ley-de-transparencia/)

**1 diciembre 2022**

[Ejecución RDA120/2022](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/TransparenciaOposiciones/Inspecci%C3%B3n/2019-Madrid/2022-12-01-Ejecucion_resolucion_CTPCM_reclamacion_RDACTPCM079_2022.pdf)  

La Comunidad de Madrid publica en lugar de enviar copia

**20 abril 2024**

La Comunidad de Madrid publica los enunciados de oposiciones de inspección **de forma activa** y **el mismo día de la prueba**

[Procesos Selectivos. Oposiciones 2023. Inspectores de educación. Documentación. Tablón.](https://www.comunidad.madrid/servicios/educacion/procesos-selectivos-oposiciones-inspectores-educacion#documentacion-tablon)  

[CONCURSO-OPOSICIÓN DE ACCESO AL CUERPO DE INSPECTORES DE EDUCACIÓN MADRID. PRIMERA PARTE DE LA PRUEBA. 20 de abril de 2024](https://www.comunidad.madrid/sites/default/files/doc/educacion/rh06/rh06_0208_2024_prim_par_cuadernillo_casos_.pdf)

[twitter FiQuiPedia/status/1781731207543197745](https://twitter.com/FiQuiPedia/status/1781731207543197745)  
* 2021: [@ConsejoTBG desestima reclamación enunciados inspección Madrid 2018](https://consejodetransparencia.es/ct_Home/dam/jcr:db9e9e53-ea3f-4b35-8fb2-47bfc6efb8cf/RT_0754_2021.pdf)  
* 2022: CTyPCM estima deben facilitarse esos enunciados  
* 2024: [@educacmadrid publica de forma activa enunciados el día de la prueba, hoy](https://comunidad.madrid/sites/default/files/doc/educacion/rh06/rh06_0208_2024_prim_par_cuadernillo_casos_.pdf)  
\#ReclamadMalditos
