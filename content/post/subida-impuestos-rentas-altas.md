+++
author = "Enrique García"
title = "Subida impuestos rentas altas"
date = "2018-09-01"
tags = [
    "impuestos", "Post migrado desde blogger"
]

description = "Subida impuestos rentas altas"
thumbnail = "https://pbs.twimg.com/media/Dl6XxoXXcAAffaZ.jpg"
images = ["https://pbs.twimg.com/media/Dl6XxoXXcAAffaZ.jpg"]

toc = true

+++

Revisado 13 septiembre 2024

## Resumen

Comento la idea de subida de impuestos (sobre rentas del trabajo) a rentas altas.  
Lo hago inicialmente en 2018 porque surge esta imagen en la televisión, que es totalmente falsa.  

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhtmjBp69FXnWJga_fb-4XTAL7ST5eUZ8cyvI0SuFHCet1zpFVQrw5MgqT5fKhETBJdgbr1E5nV8sxN2NMMHo9bQdkIEG95MQOk34t6bsLIGV0BcunVTEAeETRxElCKgdnvvoZ8Uly3A1dm/s400/2018-08-30-YaEsMediodia.jpg)  
[T01 Programa 54, "Ya es mediodía", Telecinco, 30 agosto 2018](https://www.mitele.es/programas-tv/ya-es-mediodia/5b87e982591063c8a28b467a/player)  

Inicialmente trataba en el mismo post el sueldo de docentes pero al migrar el post separo temas y creo post separado [Sueldo docentes](../post/sueldo-docentes)  

Además intento poner el contexto en el que se da esa información falsa; lo veo con un mal uso de estadísticas e información mezclado con un intento de manipular para oponerse a la subida de impuestos a las rentas más altas / banca.

Post relacionados:

* [El dinero que pagas de impuestos no es tuyo](https://algoquedaquedecir.blogspot.com/2022/09/el-dinero-que-pagas-de-impuestos-no-es-tuyo.html)  
* [Rompe cadenas](http://algoquedaquedecir.blogspot.com/2018/01/rompe-cadenas.html) (parte asociada a informarse y no creerse ni difundir bulos)
* [RPTs y trasnparencia](https://algoquedaquedecir.blogspot.com/2018/07/rpts-y-transparencia.html) (asociado a que en altos cargos ellos deciden dónde se pone el dinero público, subiendo o bajando por ejemplo salarios de altos cargos o de funcionarios normales)
* [Ocupar puesto pagado con dinero público: libre concurrencia, igualdad, mérito, capacidad, publicidad y transparencia](https://algoquedaquedecir.blogspot.com/2017/12/ocupar-puesto-pagado-con-dinero-publico.html)  (asociado a que los salarios públicos y cómo se llega a cobrarlos debería ser información pública)
* [Muestras de información a medias](http://algoquedaquedecir.blogspot.com/2019/03/muestras-de-informacion-medias.html) (sobre el uso de valores medios, allí cito la idea de salario medio, pero este post lo escribí antes, pendiente de mover y unificar información) 

## Detalles

El post lo escribo en septiembre 2018 e inicialmente me centro en la parte de docentes que conozco y puedo comentar, pero en otras profesiones de las indicadas parece que es igual de escandalosa la mentira.
Tras esa parte inicial comento otras subidas de impuestos, ya no limitadas a docentes, que pongo en el post  

La parte sobre el sueldo de docentes la separo en post separado [Sueldo docentes](../post/sueldo-docentes)  

Se podría crear un post adicional relacionado con ambos que hablase sobre distribución de salarios (no solo educación, y no solo subida de impuestos)

### Agosto 2018: Sueldo docentes públicos y subida impuestos a rentas altas

Este tema lo conozco vía Twitter, donde lo comento.
Hay varias partes: el salario de los docentes públicos, la idea de salario medio y la subida de impuestos a rentas altas. Están mezcladas por cómo surge aunque intento separar y poner aquí la parte de subida de impuestos; puede haber información repetida en ambos posts.  

Pienso en el contexto de esa información **¿por qué difundir esa información falsa? ¿Desprestigiar a los docentes diciendo que cobran mucho?**  
Así que me voy a la fuente

El tema comienza en el minuto 49:00 de programa, donde se introduce el tema diciendo literalmente  

> "Un tema que preocupa a todos, que preocupa a la calle ¿Vamos o no vamos a pagar más impuestos?"

Seguidamente se introduce esta otra imagen (es anterior a la otra donde aparecen médicos y profesores)

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhDt0sS6r-GojITL3JRp-Bq0qdbS6G5Oq6s-H97omzrNsrCO93Mxom-lugrrx_pldxmWACEOqY6IgpPu-AEBVWzoN4qX9akHdG2-EO7BnGGa-vp6ufZYkjO5SN3TjJMFrHePPA2kCyfyRKb/s400/2018-08-30-YaEsMediodia2.jpg)

Y el que aparece en la imagen dice  

>"... a partir de rentas de 150000€. Pero unidos podemos pide que la subida afecte a partir de rentas de 60000€. Ojo: la ministra de hacienda, María Jesús Montero, se ha negado en rotundo a esta última opción ¿Y por qué? Pues por una razón. Lo ha dicho la propia ministra. **60000€ es el sueldo medio que cobran en nuestro país en la actualidad muchos médicos y profesores"**

En lugar de poner imágenes de la ministra diciendo eso que citan, ponen otras.  
Así que de nuevo hay que ir a la fuente  

**29 agosto 2018**

[El Gobierno y Podemos negocian subir el IRPF a las rentas de más de 150.000 euros - elpais](https://elpais.com/politica/2018/08/29/actualidad/1535556652_937621.html)  
Hacienda rechaza tajantemente subir a partir de 60.000, como pide Unidos Podemos, pero se abre a tocar a partir de 150.000, como prometió Sánchez en la oposición  
La ministra María Jesús Montero, según fuentes de la negociación, explicó a Podemos en la reunión que subir por encima de los 60.000 implicaría gravar más a algunos profesores de Universidad o médicos, y el Gobierno no tiene intenciones de hacer eso. Prefiere concentrarse en las subidas del impuesto de sociedades a las grandes empresas, por ejemplo.

Ahí dicen "según fuentes de la negociación", luego parece que no es algo que ha dicho oficialmente la ministra.  
Buscando más información sobre médicos (de docentes incluyendo universidad ya he puesto alguna referencia citando tuits)  

**10 junio 2018**

[Hacienda retiene hasta el 50% de las guardias de los médicos en España](https://www.redaccionmedica.com/secciones/sanidad-hoy/hacienda-retiene-hasta-el-50-de-las-guardias-de-los-medicos-en-espana-1761)  
En el caso de un  **médico con 40 años o más**  que es propietario de plaza y puede ganar sobre los  **60.000 euros brutos**  al año sin guardias, el porcentaje que retiene Hacienda de la guardia es aún mayor.  
...  
Los más perjudicados son los  **médicos**   **veteranos** , con  **más de 55 años y propietarios de su plaza.** Ellos pueden alcanzar los  **80.000 euros brutos**  al año sin guardias y al aplicarse el tipo impositivo del IRPF  **el valor de sus guardias cae un 45 por ciento.**   

Parece que se está manipulando información para oponerse a una subida de impuestos a las rentas más altas.

Que una persona pague la mitad de lo que cobra vía IRPF se vende como injusto, pero para que eso ocurra debe cobrar varios millones de €, y normalmente con esas cantidades ya se usa ingeniería fiscal y se tributa mucho menos o nada.

Subir impuestos se ve como demoniaco, pero hacerlo a las rentas altas es precisamente justo, y lo dice el [artículo 31.1 de la Constitución](http://www.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=31&tipo=2)  
> 31.1 Todos contribuirán al sostenimiento de los gastos públicos **de acuerdo con su capacidad económica** mediante un sistema tributario justo inspirado en los principios de igualdad y **progresividad** que, en ningún caso, tendrá alcance confiscatorio.

Creo que esto es manipular desde arriba para evitar la subida de impuestos a las rentas muy altas, que afectaría a muy muy pocas personas. 

Enlaza con tuits como este, de ese mismo día 30 agosto

[twitter Albert_Rivera/status/1035160010555367429](https://twitter.com/Albert_Rivera/status/1035160010555367429)  
Sánchez e Iglesias pretenden subir el impuesto de la renta al 52%. Que más de la mitad del sueldo de un profesional se lo pueda llevar Hacienda es confiscatorio y se suma al IVA, IBI o sucesiones que ya pagan la mayoría de familias. Viejas recetas agotadas para un mundo global.

Que está bien explicado aquí 

[twitter indvbio/status/1035431849617702912](https://twitter.com/indvbio/status/1035431849617702912)  
con la cosa de que me tiene bloqueado, me había perdido esta perla de [@albert_rivera](https://twitter.com/Albert_Rivera) y joder.  

![](https://pbs.twimg.com/media/Dl6XxoXXcAAffaZ.jpg)](https://pbs.twimg.com/media/Dl6XxoXXcAAffaZ.jpg)  
1) un tipo marginal (máximo) del 52% no significa que Hacienda se lleve la mitad de tu sueldo. el IRPF coge tu sueldo, declara una parte exenta y el resto lo parte en tramos y de cada uno pagas un % que va creciendo hasta el tipo marginal.  
2) Rivera tiene que saber esto, así que está engañando deliberadamente.  
3) el tipo del 52% se aplicaría a quienes cobren por encima de 120.000 o 150.000 euros anuales. Esta es la Encuesta anual de estructura salarial de 2016, publicada por el INE este año.  
![](https://pbs.twimg.com/media/Dl6auTKW4AE-WwW.jpg)  
4) la subida se aplicaría a PARTE de la cagarruta de la derecha del gráfico, que está a AÑOS LUZ del salario más frecuente, del salario mediano y el medio. Como con el Impuesto de Sucesiones, [@CiudadanosCs](https://twitter.com/CiudadanosCs) intenta vender como problema generalizado algo que afecta a los ricos.   

Más tarde localizo esto, que es de julio 2018

[twitter FiQuiPedia/status/1078584607245824001](https://twitter.com/FiQuiPedia/status/1078584607245824001)  
Cuando hablen de subir impuestos y clase media pensad en esta gráfica. La distribución de ingresos en 1971 en EEUU tiene cierta analogía a la distribución de ingresos en España actual.   
[twitter conradhackett/status/1015704270249889793](https://x.com/conradhackett/status/1015704270249889793)  
Watch the income distribution in America change  

No hay que pensar solo en medias; no dan información real 

Se puede ver una captura aquí

[Demise of the US Middle Class Now Official - nakedcapitalism](https://www.nakedcapitalism.com/2015/12/demise-of-the-us-middle-class-now-official.html)  

![](https://www.nakedcapitalism.com/wp-content/uploads/2015/12/Screen-shot-2015-12-10-at-4.42.45-AM.jpg)  

Otros comentarios

**30 agosto 2018**

[twitter jfalbertos/status/1035268716982874113](https://twitter.com/jfalbertos/status/1035268716982874113)  
Es evidente que Rivera sabe que el tipo marginal y el efectivo no son lo mismo, pero oye, esto es tuiter y hemos venido a divertirnos..

**31 agosto 2018**

[twitter svtorron/status/1035413572279521280](https://twitter.com/svtorron/status/1035413572279521280)  
Para acercarnos a que la mitad de tu sueldo “se lo lleve Hacienda” en IRPF tienes que ganar unos 5 millones de euros al año.

**31 agosto 2018**

[https://twitter.com/nachoalvarez_/status/1035511207900401666](https://twitter.com/nachoalvarez_/status/1035511207900401666)  
Querido @Albert_Rivera confundir los tipos marginales con los tipos medios del IRPF es, o "cuñadismo político" o algo impropio de un dirigente que tiene la responsabilidad que tú tienes. En todo caso, aquí 👇te dejo los datos de la OCDE, por si quieres dejar de decir falsedades  
![](https://pbs.twimg.com/media/Dl7fqX0W0AEga3L.jpg)  

Y me lleva a compartir reflexiones como esta

[twitter gamusino/status/1035419937957969920](https://twitter.com/gamusino/status/1035419937957969920)  
Una cosa digna de estudiar es la cantidad de curritos mileuristas que asumen con resignación recortes en sanidad, transporte o subidas de IVA pero ponen el grito en el cielo por subir el tipo marginal a gente que gana 10 veces más que ellos.  


¿Por qué oponerse a la subida desde el ministerio? Habría que saber los sueldos de políticos respecto a ese límite de 60000€, aunque a priori creo que lo superan. Enlaza con tema [RPTs y altos cargos](http://algoquedaquedecir.blogspot.com/2018/07/rpts-y-transparencia.html): es difícil que alguien legisle bajarse el sueldo a sí mismo aumentando los impuestos y lo que recibe neto.

Se puede mirar 

[Publicidad > Organización > RPTs > MHAC - transparencia.gob.es](http://transparencia.gob.es/transparencia/transparencia_Home/index/PublicidadActiva/OrganizacionYEmpleo/Relaciones-Puestos-Trabajo/RPT-MHAC.html)  

Y ver cómo hay varios puestos de nivel 30 (por ejemplo "SUBDIRECTOR / SUBDIRECTORA GENERAL") con complemento específico cercano a los 30000€ (varios casos de 29.321,46€)   

En [artículo 22 PGE 2018](https://www.boe.es/buscar/act.php?id=BOE-A-2018-9268#ar-22) se pueden ver complementos destino por nivel incluyendo nivel 30  

También se puede ver en [artículo 20 PGE 2018](https://www.boe.es/buscar/act.php?id=BOE-A-2018-9268#ar-20)  

como 

* Presidente del Gobierno  
* Vicepresidente del Gobierno  
* Ministro del Gobierno  
* Presidente del Consejo de Estado  
* Presidente del Consejo Económico y Social  

todos superan los 60000€ y les afectaría. 

Pero si vamos más allá, el tema de subir el IRPF a rentas altas es algo que no afectaría a la gente, y si se intenta difundir quizá es para esconder algo más. Sin ser conspiranoico, vuelvo a buscar datos

30 agosto 2018 12:22 (antes del programa de Telecinco)

[twitter pnique/status/1035095319539867649](https://twitter.com/pnique/status/1035095319539867649)  (tuit no existe en 2024)  
Como estoy leyendo y escuchando mucha manipulación sobre el IRPF, yo que sí estuve en la reunión, os cuento la realidad en este hilo. 👇  
Lo primero y más importante: No llegamos a ningún acuerdo con el Gobierno sobre el IRPF. Si alguien escribe, o dice lo contrario, te está mintiendo.  
De hecho, en una reunión de casi 4 horas, apenas tratamos el tema de una forma exploratoria en unos minutos. Imposible llegar a un acuerdo en unos minutos.  
La mayor parte de la reunión la dedicamos a los impuestos que consideramos más importantes: los que tienen que ver con los privilegios fiscales de las grandes fortunas, de la banca, de los especuladores financieros y de las grandes corporaciones.  
También hablamos mucho sobre la lucha contra el fraude fiscal. Privilegios fiscales y fraudes son los dos agujeros más grandes por los que se escapan los peces gordos y la mayor merma de ingresos que está deteriorando nuestro estado del bienestar.  
El intercambio breve de parceres fue más o menos así:  
Nosotros trasladamos al Gobierno que NO queríamos subir el IRPF a la gente que gana menos de 120.000€ (es decir, 10.000€ brutos mensuales), en concreto, dejar el tipo como está en el tramo de 60.000€ a 120.000€ y a lo mejor plantearnos una subida por encima de 120.000€.  
Además de los 10.000€ al mes, para hacernos una buena idea de qué significa esto, es bueno recordar que sólo (aproximadamente) el 1% de los asalariados gana más de 120.000€ al año.  
El Gobierno nos contestó (también de manera preliminar y breve) que ellos 120.000 lo consideraban una cifra demasiado baja (??)... y ahí se quedó la cosa.  
Ahora pregúntate, con estos hechos en la mano, lo siguiente:  
¿Por qué hay quien habla de un acuerdo donde no ha habido acuerdo?  
¿A quién beneficia llamar "clase media" al 1% de asalariados que gana más de 10.000€ al mes? (Pista: a ti no.)  
¿Por qué, si hablamos 4 horas de acabar con los privilegios fiscales de grandes fortunas, banca y grandes corporaciones y sólo unos minutos a hablar de IRPF, todos los medios de comunicación hablan de IRPF?  
Cómo decían los policías de la serie The Wire: Follow the money. 😉  

Así que se puede decir, creo que objetivamente, que todo esta falsa información sobre subida IRPF desvía la atención sobre la **subida de impuestos a grandes fortunas, banca y grandes corporaciones** .

**9 octubre 2018** parece que se va a limitar a los 140000€

[El IRPF por encima de 140.000 euros subirá dos puntos en 2019](https://www.eldiario.es/economia/IRPF-elevaran-permisos-paternidad-semanas_0_823118075.html)  
Principio de acuerdo entre el Gobierno y Unidos Podemos en varios puntos de sus negociaciones presupuestarias  
El permiso de paternidad pasaría a ser de ocho semanas en 2019, equiparado al de maternidad en 2021, según los negociadores  
El tipo estatal sobre las rentas del capital se incrementará en 4 puntos porcentuales para las superiores a 140.000 euros (del 23 al 27%)  

Sobre  **subida de impuestos a grandes fortunas, banca y grandes corporaciones** e impuestos no sobre renta trabajo sino otros como patrimonio) hay mucho que hablar

Información de días antes de esa reunión

**25 agosto 2018**

[Cero euros - infolibre](https://www.infolibre.es/noticias/opinion/columnas/2018/08/25/cero_euros_86066_1023.html)  
El debate no es si los bancos deben pagar más por el impuesto de sociedades. La cuestión es si van a empezar a pagar algo. **0,00 euros debería ser una cifra escasa incluso para los charlatanes** disfrazados de economistas viejoliberales.

**1 septiembre 2018**

[La mentira de los impuestos a los “ricos” Daniel Lacalle - elespanol.com](https://www.elespanol.com/economia/20180901/mentira-impuestos-ricos/334596539_13.amp.html)  
> El cuento de **demonizar las rentas altas** empieza por el lenguaje. “Rentas altas”. ¿Quién define “altas”? Parte de hacer creer al que lo lee o escucha que es una renta injusta o desproporcionada. No dicen “las rentas más productivas” o “las rentas mejor remuneradas”, no. “Rentas altas”. Para que usted piense que **somos malvados explotadores.**   
...  
Pero lo que es absolutamente intolerable es que digan que no va a afectar a “la clase trabajadora”. **Como si los que ganan 120.000 o 150.000 euros al año, o lo que sea, no fueran trabajadores** . Y además, incansables, como los demás. Como si los médicos, ingenieros, arquitectos o cualquier grupo que se gane su salario con su esfuerzo no fueran trabajadores que se levantan cada mañana para generar **crecimiento, empleo y prosperidad** .

**5 septiembre 2018**

[Las presiones "de los de siempre" fuerzan a Sánchez a renunciar al impuesto a la banca - publico](https://www.publico.es/economia/impuesto-banca-presiones-siempre-fuerzan-sanchez-renunciar-impuesto-banca.html)  


**25 septiembre 2018**

Se publican datos CIS, y hay una pregunta concreta que se comenta aquí

[twitter Arma_pollo/status/1044563098374426625](https://twitter.com/Arma_pollo/status/1044563098374426625)  
Habría que incluir otro peldaño en 10.000€ para calcular cuántos son la "clase media y trabajadora" de Villacis  

![](https://pbs.twimg.com/media/Dn8Ima2WkAA3BpV.jpg)  
[BARÓMETRO DE SEPTIEMBRE 2018 AVANCE DE RESULTADOS Estudio nº 3223 Septiembre 2018](https://www.cis.es/documents/d/cis/es3223mar_a#page=25)  
> Pregunta 41 ¿Y en qué tramo de esa misma escala están comprendidos sus ingresos personales, después de las deducciones de impuestos, es decir, sus ingresos netos?

(se puede ver que 14,6+1,2+7,8+13,1+14,0+12,3+5,7+1,6+0,6+0,2+0,3+28,3=99,7, no 100 quizá por redondeos mal hechos, pero próximo.

En cualquier caso: 14,6+1,2+7,8+13,1+14,0=50,3% ingresan netos menos de 1200 € al mes.

### Abril 2019: Subida de impuestos rentas altas tras 28A

**30 abril 2019**

[Programa de Estabilidad. Sánchez prevé una subida adicional de "la presión fiscal" de más de 26.000 millones en cuatro años - elmundo](https://www.elmundo.es/economia/macroeconomia/2019/04/30/5cc8ae21fc6c83e06f8b45a5.html)  
Por lo pronto, el programa de estabilidad incluye las propuestas fiscales que ya se contemplaban en los fallidos Presupuestos Generales del Estado de 2019, esto es: el incremento de Sociedades; la subida del IRPF a las rentas más altas; el impuesto al diésel o fiscalidad verde; y las tasas *Google* y *Tobin* , Con todas estas actuaciones, la estimación es que se obtenga **un incremento de recaudación inmediato de 5.654 millones** el plan constata que, a pesar de lo afirmado tanto por Sánchez como por la ministra de Hacienda, María Jesús Montero, las rentas medias sí se verán perjudicas.

[El Gobierno promete a Bruselas elevar la recaudación en 20.000 millones en tres años - elconfidencial](https://www.elconfidencial.com/economia/2019-04-30/plan-de-estabilidad-gobierno-espana-2019_1976734/)  
El Programa de Estabilidad remitido a la Comisión Europea incluye una subida de los ingresos públicos de casi dos puntos del PIB y una subida de impuestos en 2020 de 5.654 millones  

El documento parece ser este (pdf de 134 páginas, metadatos indican 30 abril 2019)

http://www.mineco.gob.es/stfls/mineco/comun/pdf/190430_np_progrdest.pdf

En página 57 hay un cuadro "Cuadro4.3.2.Impacto nuevas medidas de ingreso" y son 5.654 millones (en otros sitios se dicen 20 000 M€)

En página 63

B. Incremento de los tipos de gravamen del Impuesto sobre la Renta de las Personas Físicas a las rentas altas

Se incrementan dos puntos los tipos impositivos sobre la base general para los contribuyentes que tengan rentas superiores a 130.000 euros y cuatro puntos para la parte que exceda de 300.000 euros. Asimismo, el tipo estatal sobre la base del ahorro se incrementará en cuatro puntos porcentuales para las rentas del ahorro superiores a 140.000 euros, pasando del 23 por ciento al 27 por ciento.

La adopción de esta medida responde a la justicia social y a la capacidad contributiva de los ciudadanos, de forma que aporten en mayor medida los ciudadanos que dispongan de mayor renta

**1 mayo 2019**

[twitter Albert_Rivera/status/1123498792618164224](https://twitter.com/Albert_Rivera/status/1123498792618164224)  
Sánchez ocultó a los españoles esta subida masiva de impuestos para que no le pasara factura en las urnas. Es un escándalo y un fraude, además de ir en dirección contraria a lo que necesita nuestra economía. Un sablazo intolerable a las clases medias.

[twitter jonbaldw/status/1123654674404585472](https://twitter.com/jonbaldw/status/1123654674404585472)  
Para Albert Rivera subir en dos puntos el tramo del IRPF a quienes ingresan a partir de 130.000€ y cuatro a quienes ingresan a partir de 300.000€ es un ataque a la clase media. En 2016 declaró por encima de 60.000€ el 0'46% de la población. Pura clase media, amigo.  
Son datos de la agencia tributaria, los podéis consultar aquí y ver que no me invento nada:   
https://www.agenciatributaria.es/AEAT/Contenidos_Comunes/La_Agencia_Tributaria/Estadisticas/Publicaciones/sites/irpf/2016/jrubikf48e908ff11bf1ee563d94177f08f1e74f44155f.html  
https://www.agenciatributaria.es/AEAT/Contenidos_Comunes/La_Agencia_Tributaria/Estadisticas/Publicaciones/sites/irpf/2016/jrubikf72ed1a85bb22a3c9f3ba8de0a47a248ee439d1b3.html  

Fe de erratas: quiénes tributaron ppr encima de 60.000€ fueron el 3'7% y quienes tributaron por encima de 150.000€ fueron el 0'46%. De todas maneras el mensaje es el mismo y es hasta insultante hacer creer a la gente que eso es clase media.

[PLAN DE ESTABILIDAD 2019-2022 » ¿Cuánto y a quién subirá el Gobierno de Sánchez los impuestos en 2020? - elpais](https://elpais.com/economia/2019/05/01/actualidad/1556712337_996315.html)  
El Ejecutivo espera que el gran aumento de la recaudación fiscal proceda de la mayor actividad económica

> El [Plan de Estabilidad 2019-2022](https://elpais.com/economia/2019/04/30/actualidad/1556654440_338288.html) que el Gobierno de Pedro Sánchez envió ayer por la noche a la Comisión Europea contiene una subida de impuestos de 5.654 millones de euros para 2020.   
...  
¿A quién afectarán los nuevos impuestos?  
Las subidas de impuestos de Sánchez afectarán sobre todo a las rentas más altas y a las grandes empresas. [Recaerán sobre el 0,5% de los contribuyentes y el 1% de las empresas](https://elpais.com/economia/2018/10/11/actualidad/1539286046_154796.html). Entre las medidas tributarias destaca la subida de dos puntos del tramo máximo del IRPF para los contribuyentes que ganen más de 130.000 euros. Es decir, que las rentas altas, unas 90.000 personas de los más de 19 millones de contribuyentes, pagarán un gravamen máximo del 47% en lugar del 45% al que tributan actualmente. Este tramo máximo aumentará en cuatro puntos para los que declaren más de 300.000 euros al año. Es decir, estos tributarán al 49% en el tramo máximo.  
Aún así, los tipos máximos del IRPF serán inferiores a los que estuvieron vigentes entre 2012 y 2015, cuando en lo peor de la crisis el PP aprobó un gravamen complementario que elevó el IRPF hasta el 52%, el mayor nivel desde los ochenta.

**2 mayo 2019**

[La subida de impuestos que prepara el Gobierno, al detalle](https://cadenaser.com/ser/2019/05/02/economia/1556779819_762753.html)  
En el Plan de Estabilidad 2019-2022 que ha remitido a Bruselas pone cifras a lo que intentó aprobar en los Presupuestos que desembocaron en las elecciones anticipadas: las subidas de impuestos suman 5.654 millones más

> Las grandes nóminas  
La subida más polémica de todas. Sube dos puntos el impuesto de la Renta **a quien cobre más de 130.000 euros al año** . En castellano, quien cobra unos 7.041 euros netos al mes pagará en impuestos 200 euros más cada mes. Ahí está la polémica porque eso son abogados, médicos, pilotos... y esos sectores preguntan si son éstos los ricos de España.
Además, **sube cuatro puntos la Renta a quien cobre más de 300.000 euros al año** . La subida ahí es enorme: quien cobra 16.250 euros netos al mes pasa a pagar 1.000 euros más al mes en impuestos.


**17 octubre 2019**

[Montero descarta ahora subir el IRPF a las rentas altas por ser una propuesta de Podemos - elpais](https://elpais.com/economia/2019/10/17/actualidad/1571325925_702117.html)  
El proyecto presupuestario del PSOE para 2018, sin embargo, ya incluía esta medida

El documento del PSOE  *Un proyecto para España. Alternativa presupuestaria 2018* , de abril de 2018 —presentado cuando los socialistas aún estaban en la oposición y poco antes de la moción de censura que terminó con el Ejecutivo de Mariano Rajoy—, ya contemplaba un alza del impuesto sobre la renta para los contribuyentes con ingresos superiores a los 150.000 euros anuales. 

### Diciembre 2019: Subida de impuestos rentas altas en acuerdo gobierno

**30 diciembre 2019**

http://www.rtve.es/contenidos/documentos/acuerdo_coalicion_psoe_podemos.pdf  
> 10.2.-Aumento de la progresividad del sistema fiscal.  
...  
Impuesto sobre la Renta de las Personas Físicas. Se incrementan dos puntos los tipos impositivos sobre la base general para los contribuyentes que tengan rentas superiores a  130.000  euros  y  cuatro  puntos  para  la  parte  que  exceda  de  300.000  euros.  El  tipo estatal sobre las rentas de capital se incrementará en 4 puntos porcentuales para dichas rentas superiores a 140.000 euros.  

[El PSOE y Podemos ultiman una subida inmediata de impuestos - elmundo](https://www.elmundo.es/espana/2019/12/30/5e08fedb21efa0f9268b460a.html)  

Pero la parte central del acuerdo será la subida de impuestos. En el "preacuerdo" que ambos líderes firmaron el 12 de noviembre se introdujo el concepto de "justicia fiscal" y se hablaba de "una reforma fiscal justa y progresiva". Más allá de los nuevos tributos que puedan crearse, el dilema es dónde situar el umbral de un incremento del IRPF.  
Podemos lo coloca en los 100.000 euros brutos de ingresos anuales, aunque ya en el anterior pacto que firmaron con el PSOE, el presupuestario de [octubre de 2018](https://www.elmundo.es/espana/2018/10/10/5bbe692b22601d10578b456f.html), ese listón se elevó a los 130.000 euros. Lo que parece seguro es que los españoles iniciarán 2020 con una subida de impuestos.

[twitter kikollan/status/1211646459654918145](https://twitter.com/kikollan/status/1211646459654918145)  
1/ ¿Si ganas 130.000€ anuales eres rico? Sin duda. Significa que ingresas más que el 99% de los españoles. Aquí van algunos datos más.  
2/ Según la World Income Database de 2016 (https://wid.world/es/country/es-spain/):  
- Si ganas 41.000€, pertences al 10% de adultos con mayores rentas de España.  
- Si ganas 50.000€, eres 5%.  
- Si ganas 119.000€, eres 1%.  
- Si ganas 132.000€, eres 0,8%.  
3/ Aquí la evolución desde los ochenta:  
![](https://pbs.twimg.com/media/ENCiCd5WsAATedn?format=jpg&name=small)](https://pbs.twimg.com/media/ENCiCd5WsAATedn?format=jpg)  
4/ Renta por hogar dividida por persona equivalente*(Eurostat 2018 https://ec.europa.eu/eurostat/web/income-and-living-conditions/data/database)  
- Eres 10% con 29.800€ (53.600€ para parejas con hijo)  
- Eres 5% con 36.500€ (65.700€)  
- Eres 1% con 54.360€ (98.000€)  
(*El primer adulto cuenta 1; otros 0,5; cada hijo 0,3.)  
5/ Ya que estamos con rentas: ojo a la brecha por hijos.  Los hogares con rentas medianas más altas son los "mayores de 65 años" (16.700€). ¿Las más bajas? Familias con hijos y tres adultos (13.000€), familias numerosas (12.000€) y madres/padres solteras (10.200€).  
6/ Los 1% con más renta de España tienen el 9% de todas las rentas del país. Como en otros páises, es más que hace una década, pero la mitad que en EEUU:  
![](https://pbs.twimg.com/media/ENCpcdPWsAA1sIU?format=jpg&name=small)](https://pbs.twimg.com/media/ENCpcdPWsAA1sIU?format=jpg)  

[twitterariamsita/status/1211662636103995394](https://twitter.com/ariamsita/status/1211662636103995394)  
Recordatorio periódico de cómo se distribuye la renta en España. Efectivamente, casi nadie gana 130k  
(tabla con ingresos medios POR HOGAR)

![](https://pbs.twimg.com/media/ENCwu_NXYAAHXq9?format=jpg) 

Como estamos hablando de IRPF, está bien recordar también que las declaraciones de más de 90k son un 1,1% del total en España:

![](https://pbs.twimg.com/media/ENCx17gXsAsSXLW?format=png)

### Actualización 2024

Intento citar aquí cambios desde 2018 que escribí inicialmente el post, cuando lo migro en 2024 no lo actualizaba desde 2020

[Ley 38/2022, de 27 de diciembre, para el establecimiento de gravámenes temporales energético y de entidades de crédito y establecimientos financieros de crédito y por la que se crea el impuesto temporal de solidaridad de las grandes fortunas, y se modifican determinadas normas tributarias.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-22684)  

[Las grandes empresas de más de 5.000 trabajadores tributan a un tipo efectivo del 3,6%, pero representan el 0,8% del total - newtral](https://www.newtral.es/impuesto-sociedades-grandes-empresas-factcheck/20230620/)  
> Resumen  
Díaz dijo que no es aceptable que “una peluquería tribute al 17,5% y una gran empresa lo haga al 3,9%”. Los datos son correctos: las empresas más pequeñas pagan un 17,5% por el impuesto de sociedades y las más grandes (de más de 5.000 trabajadores), un 3,6%, según los datos de 2020 de la Agencia Tributaria, los últimos disponibles.

**24 abril 2024**

[Las grandes multinacionales españolas pagan menos impuestos pese a que sus beneficios son mayores - cadenaser](https://cadenaser.com/nacional/2024/04/24/las-grandes-multinacionales-solo-pagan-un-144-en-impuesto-de-sociedades-en-espana-cadena-ser/)  

Según datos de Hacienda, las 123 mayores empresas de España pagaron en 2021 un tipo del 14,4% en el Impuesto de Sociedades

### Explicación tramos IRPF

Ver todo el hilo
[twitter contextosin/status/1211715973444784131](https://twitter.com/contextosin/status/1211715973444784131)  
EL IRPF EXPLICADO CON HELADOS  
Imagina que te pagaran el sueldo en helados. ¿Cuánto se llevaría hacienda? Pues depende del número de bolas.  
La primera la deja toda para tí, a la segunda le da un mordisquito, a la tercera un poco más, a la cuarta un mordisco más grande..  
![](https://pbs.twimg.com/media/ENDhPzfXUAArVwa?format=jpg&name=small)](https://pbs.twimg.com/media/ENDhPzfXUAArVwa?format=jpg)

### Opinión población sobre impuestos

Ver post [El dinero que pagas de impuestos no es tuyo](https://algoquedaquedecir.blogspot.com/2022/09/el-dinero-que-pagas-de-impuestos-no-es-tuyo.html)  

**9 enero 2023**

[La mayoría pagaría más impuestos para mejorar la sanidad - elpais](https://elpais.com/espana/2023-01-09/la-mayoria-pagaria-mas-impuestos-para-mejorar-la-sanidad.html)  
