+++
author = "Enrique García"
title = "Grupo Planeta y educación Madrid"
date = "2023-10-13"
tags = [
    "educación", "Madrid", "Privatización", "post migrado desde blogger", "Planeta"
]
toc = true

+++

Revisado 14 mayo 2024

## Resumen

El grupo Planeta, que controla muchos medios de comunicación relevantes en España, ha recibido y recibe dinero y favores asociados a educación en Madrid.

* Dinero: en 2020 con la excusa de covid19 el grupo Planeta recibe, por un contrato de emergencia a dedo por dos años y por unos materiales que ningún docente había pedido y que no se han usado, 10 M€ (14,5 M€ incluyendo IECISA, actual INETUM)  
En 2023 recibe en licitación abierta*, por "lo mismo" en un contrato a 3 años, 4 M€  
* Antes de la licitación se pone por escrito que se va a haber un nuevo contrato de Madrid5e, del que forma parte Planeta, que es la que tiene registrado ese dominio  
* Favores: en 2020 se crea una universidad privada del grupo Planeta, ver post [Universidades privadas Madrid](http://algoquedaquedecir.blogspot.com/2019/02/universidades-privadas-madrid.html)

Ver [Grupo Planeta y Educación Andalucía](../grupo-planeta-y-educacion-madrid)

## Detalle

### Cronología de aulaPlaneta en Madrid

Los materiales de aulaPlaneta se habían ofertado en algunos centros antes de 2020, lo que suponía coste para el centro, y en general con poco uso.  
Recopilo documentos en [drive.fiquipedia aulaplaneta](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/algoquedaquedecir/Madrid5e/aulaplaneta)  

**2016**

[Jornada "Aprendizaje competencial - Diseño de proyectos con Aulaplaneta" (I)](https://mediateca.educa.madrid.org/imagen/w8sw4ly7du3gvxhh)

Subido el 18 de mayo de 2016 por Ctif madridsur    

Profesor participante en la Jornada IN 0004 - Aprendizaje Competencial: diseño de proyectos con Aulaplaneta, explicando las características de los recursos digitales de Aulaplaneta, celebrada el día 4 de mayo de 2016.

### Madrid 2020

En 2020 con la pandemia grupo Planeta ofrece sus materiales de manera gratuita en abril 2020.

**14 abril 2020**

[El Grupo Planeta dotará de recursos a los colegios de Madrid en plena crisis del coronavirus a través de 'aulaPlaneta' - antena3](https://amp.antena3.com/noticias/cultura/el-grupo-planeta-y-la-comunidad-de-madrid-se-alian-para_202004145e95a8608781e90001ad2ba1.html)  
El Grupo Planeta y la Comunidad de Madrid han pactado una alianza para dotar de recursos a profesores y alumnos madrileños. 


[Acordamos con Planeta ofrecer recursos digitales a centros públicos y concertados - comunidad.madrid](https://www.comunidad.madrid/noticias/2020/04/14/acordamos-planeta-ofrecer-recursos-digitales-centros-publicos-concertados)  
La Comunidad de Madrid ha alcanzado un acuerdo con el Grupo editorial y de comunicación Planeta, que permitirá ofrecer recursos digitales a centros públicos y concertados desde 5º de Educación Primaria a 4º de Educación Secundaria Obligatoria. Esta iniciativa tiene como objetivo dar continuidad al curso escolar tras la suspensión de la actividad presencial por el COVID-19, y forma parte de un amplio paquete de medidas para facilitar la educación a distancia de los alumnos madrileños.

**24 abril 2020**

Fecha en metadatos de este documento
[¿Cómo solicitar ACTIVAR LOS RECURSOS de aulaPlaneta para tu centro?](https://www.educa2.madrid.org/web/educamadrid/principal/files/0cf74207-b6c3-42dd-af3c-e9fcf4ab433b/1_Elementos%20aulaPlaneta_EducaMadrid%20VF%2014_04.pdf?t=1589197699119)  
"Para activar el servicio, los centros remitirán un correo electrónico a CAU@educa.madrid.org desde la cuenta institucional del centro, con el siguiente contenido: Deseo activar el acceso a aulaPlaneta en las aulas virtuales de EducaMadrid del centro."

La autora en metadatos es [Cristina Esteve Deu - Directora pedagógica - Aulaplaneta](https://es.linkedin.com/in/cristina-esteve-deu-724a80b1)

**25 agosto 2020**

Se presenta el curso 2020-2021

[La Comunidad de Madrid contratará casi 11.000 profesores, hará test COVID-19 a los docentes y un estudio serológico a alumnos y colectivos de riesgo - comunidad.madrid](https://www.comunidad.madrid/notas-prensa/2020/08/25/comunidad-madrid-contratara-casi-11000-profesores-hara-test-covid-19-docentes-estudio-serologico-alumnos-colectivos-riesgo)  

Se puede ver en página 19 de la presentación enlazada en ese artículo  
[PDF - ESTRATEGIA EDUCACION NUEVO CURSO_25_08_2020](https://www.comunidad.madrid/file/223257/download)  

> "Currículo on line de las materias troncales desde 5º de primaria, secundaria y bachillerato en español y en inglés"

En [Nota de Prensa - Estrategia inicio del curso escolar](https://www.comunidad.madrid/file/223268/download)  
> Por ello, se ha creado una plataforma con el currículo digital online de las materias
troncales para que los profesores puedan organizar la enseñanza a distancia con todo tipo de recursos digitales, en castellano y en inglés, que podría ser utilizada por 690.000 alumnos desde 5º de Primaria hasta Bachillerato.

Orden de emergencia (se publica el 26 octubre tras solicitud de información)  
[ORDEN Nº 1932/2020, DE 25 DE AGOSTO, POR LA QUE SE ACUERDA LA DECLARACIÓN DE EMERGENCIA DEL CONTRATO DE SERVICIOS DE “CURRÍCULO DIGITAL DE LAS MATERIAS TRONCALES PARA LOS CURSOS COMPRENDIDOS ENTRE 5º CURSO DE EDUCACIÓN PRIMARIA Y SEGUNDO CURSO DE BACHILLERATO, DIVIDIO EN TRES LOTES”. EXPTE. A/SER-028102/2020.](https://contratos-publicos.comunidad.madrid/medias/orden-emergencia-plataforma-curriculo-digital/download)

**27 agosto 2020**

[Contrato de lotes 1 y 2 con Grupo Planeta](https://contratos-publicos.comunidad.madrid/medias/contrato-anonimizado-lotes-1-2/download) (se publica 1 marzo 2021)

> El precio de este contrato es de DIEZ MILLONES NOVECIENTOS CATORCE MIL TRESCIENTOS SETENTA Y UN EUROS CON SETENTA Y OCHO CÉNTIMOS (10.914.371,78 euros); 10.494.585,25 euros de base imponible y 419.783,53 euros en concepto de IVA (4%)

**16 septiembre 2020**

[ACUERDOS CONSEJO DE GOBIERNO 16 de septiembre de 2020](https://www.comunidad.madrid/sites/default/files/200916_cg.pdf)   
> Informe por el que se da cuenta de la declaración de emergencia del contrato de servicios del currículo digital de las materias troncales, para los cursos comprendidos entre 5º curso de educación primaria y segundo curso de bachillerato, dividido en tres lotes, suscrito con las empresas “Editorial Planeta, S.A.U.” e “Informática El Corte Inglés, S.A.”, por importe total de 14.544.280,34 euros y un plazo de ejecución máximo del 27 de agosto al 15 de septiembre de 2020.

Realizo solicitud de acceso vía transparencia:

---

1.Copia o enlace al convenio suscrito por la Comunidad con Planeta en abril 2020  
https://www.comunidad.madrid/noticias/2020/04/14/acordamos-planeta-ofrecer-recursos-digitales-centros-publicos-concertados  
2. Copia o enlace a los contratos de emergencia con “Editorial Planeta, S.A.U.”,  “Informática El Corte Inglés, S.A.”,  “ODILO TID, S.L.” que finalizaron 15 septiembre y se citan en consejo de gobierno dia 16.  
https://www.comunidad.madrid/sites/default/files/200916_cg.pdf  
3. Copia o enlace a documentación que justifique la necesidad de “plataforma web de biblioteca escolar de la Comunidad de Madrid" cuando Madrid ya dispone de abiesweb http://abiesweb.es/web/responsables#madrid  

---

49/254486.9/20

**18 septiembre 2023**

Se publica contrato de emergencia en portal de contratación A/SER-028102/2020
[Currículo digital de las materias troncales para los cursos comprendidos entre 5º curso de educación primaria y segundo curso de bachillerato](https://contratos-publicos.comunidad.madrid/contrato-publico/curriculo-digital-materias-troncales-cursos-comprendidos-5o-curso-educacion)

**25 septiembre 2020**

Realizo solicitud

---

Copia o enlace a documentación que indique la relación de que centros que han solicitado uso de Aula Planeta en aulas virtuales, ya que según documento de abril 2020  
https://www.educa2.madrid.org/web/educamadrid/principal/files/0cf74207-b6c3-42dd-af3c-e9fcf4ab433b/1_Elementos%20aulaPlaneta_EducaMadrid%20VF%2014_04.pdf?t=1589197699119  
"Para activar el servicio, los centros remitirán un correo electrónico a CAU@educa.madrid.org desde la cuenta institucional del centro, con el siguiente contenido: Deseo activar el acceso a aulaPlaneta en las aulas virtuales de EducaMadrid del centro."

---

49/331656.9/20

**27 septiembre 2020**

Solicito copia o enlace al Pliego de Prescripciones Técnicas del contrato "Currículo digital de las materias troncales para los cursos comprendidos entre 5º curso de educación primaria y segundo curso de bachillerato"  
Número de expediente  A/SER-028102/2020 que se cita como adjunto en IV. ESPECIFICACIONES TECNICAS de http://www.madrid.org/contratos-publicos/1354842786883/1245472924202/1354846126554.pdf  adjunto. 

49/335447.9/20

_No reclamo porque lo acaban publicando_

**30 septiembre 2020**

Localizo un grupo de telegram [aulaPlaneta en Madrid5e](https://t.me/aulaplanetaeneducamadrid)
En ese momento tiene 158 miembros  
_A 27 enero 2024 tiene 177 miembros_ 

**2 octubre 2020**

[Madrid gasta cerca de 17 millones de euros en materiales digitales para Educamadrid - elpais](https://elpais.com/espana/madrid/2020-10-01/madrid-gasta-cerca-de-17-millones-de-euros-en-materiales-digitales-para-educamadrid.html)  
Los sindicatos y los directores de instituto lo tachan de “despilfarro innecesario” y critican la adjudicación “a dedo”

**14 octubre 2020**

[Los editores recurren el gasto de 17 millones en materiales educativos digitales: “Es un despropósito” - elpais](https://elpais.com/espana/madrid/2020-10-14/los-editores-recurren-el-gasto-de-17-millones-en-materiales-educativos-digitales-para-educamadrid-es-un-desproposito.html)  
La patronal Anele acusa al Gobierno de Madrid de vulnerar “principios tan básicos como la neutralidad, la libre concurrencia, la transparencia e incluso la libertad de cátedra”

**23 octubre**

Notifican [ampliación de plazo 09-OPEN-00123.6/2020 = 49/254486.9/20 (contrato abril)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Madrid5e/aulaplaneta/2020-10-23-AmpliacionPlazo.pdf)

**9 noviembre 2020**

Veo pliego

[twitter FiQuiPedia/status/1325927035055828993](https://twitter.com/FiQuiPedia/status/1325927035055828993)  
Día 23 octubre dar pliegos era algo complejo y necesitaba ampliación plazo.  
Mientras espero:  
Día 26 se publican documentos, incluyendo memoria justificativa  
Día 4 noviembre se publican pliegos

**11 noviembre 2020**

Recibo [resolución](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Madrid5e/aulaplaneta/2020-10-23-AmpliacionPlazo.pdf)

**15 noviembre 2020**

Reclamo a CTBG

---

16 septiembre 2020 realizo solicitud.  
14 octubre 2020 notifican ampliación de plazo.  
No realizo alegaciones sobre puntos 2 y 3 (enlazan información publicada con posterioridad a mi solicitud y a la ampliación de plazo)
Sin embargo sobre punto 1 indican "acuerdo verbal" del que no facilitan información, que es lo que reclamo.
Creo que procede citar resolución estimatoria RT0269/2020 con situación similar de contrato verbal  y también supuestamente cesión gratuita (todavía no se ha ejecutado y no he recibido información) 

---

**16 noviembre 2020**

Se asigna RT 0646/2020.

**28 enero 2021**

[twitterblondieconomics/status/1354757364386291718](https://twitter.com/blondieconomics/status/1354757364386291718)  
Hoy hacemos la presentación pública en la @ComunidadMadrid  “Madrid5e” un proyecto de transformación digital xa mejorar la calidad de educación madrileña con una presentadora @MonicaCarrillo  prescriptores de lujo @inetum_espana @aulaPlaneta  y la Presidenta de la CM @IdiazAyuso

**3 marzo 2021**

Recibo [resolución RT 0646/2020 desestimatoria](https://www.consejodetransparencia.es/ct_Home/eu/dam/jcr:187a1a5f-f676-4a14-9869-88fb60caacbf/RT_0646_2020.pdf)  
> ...Por lo tanto, procede desestimar la reclamación presentada al no existir objeto sobre el que ejercer el derecho de acceso a la información.

**1 abril 2021**

[Ayuso se gana al Grupo Planeta con una universidad privada y un contrato a dedo de 14,5 millones - ctxt](https://ctxt.es/es/20210401/Politica/35712/Ayuso-Grupo-Planeta-universidad-privada-Atresmedia-Jose-Creuheras-Mauricio-Casals.htm)

**28 mayo 2022**

Planteo solicitud

---

En expediente A/SER-028102/2020 de contrataron vía emergencia en 2020 recursos que indican en contratos "Las licencias adquiridas estarán a disposición de la Consejería de Educación y Juventud de la Comunidad de Madrid durante dos cursos escolares"  
Finalizados esos dos cursos, solicito copia o enlace a documentación con la información de la Consejería disponga sobre:  
-Sobre la formación ofrecida y realizada por docentes asociada a dichos recursos. Número de cursos, número de docentes que los han realizado.  
-Sobre el uso por docentes y alumnos de dichos recursos, incluyendo si se dispone del % de docentes y/o alumnos que lo han utilizado. Por ejemplo considero que no es relevante solo el número de aulas virtuales que incluyen recursos, ya que se ha hecho que se incluyan por defecto al crear un nuevo aula, sino el número de aulas virtuales en las que realmente se han incluido más recursos que los de por defecto, y estadísticas sobre los accesos. Los pliegos indican "El acceso a las plataformas del currículo digital de cada lote se realizará a través de la cuentas de EducaMadrid"  
-Sobre encuestas a docentes y/o alumnos sobre dichos recursos.  
-Sobre la adecuación de los materiales actuales a los cursos en los que el currículo se modifica en curso 2022-23 y y en curso 2023-2024.  
-Sobre decisión de renovación, total o parcial, de dichos materiales, o de contratación de otros distintos.  

---

**21 julio 2022**

Recibo [resolución sobre aulaPlaneta](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Madrid5e/aulaplaneta/2022-07-21-ResolucionInformacionPlaneta.pdf)

[twitter FiQuiPedia/status/1550054002196467712](https://twitter.com/FiQuiPedia/status/1550054002196467712)  
Sobre contrato de emergencia de 14,5 M€ para unos materiales por 2 años, datos sobre su inutilidad:  
· 717 de 110000 docentes se han formado \[en Madrid5e]  
· Datos de aulas, docentes y alumnos desvirtuados al incluirlos por defecto.  
· Parece no se renueva  

> "...han certificado un total de 717 docentes y en las que uno de los objetivos fundamentales ha sido la formación en el uso de los mencionados recursos.  
> ...  
> En relación a la adecuación de los materiales actuales a los cursos en los que el currículo se modifica en los cursos 2022-2023 y 2023-2024, cabe señalar que los materiales actuales no tienen la necesidad de ser actualizados pues el uso de estas licencias no tienen continuidad para futuros cursos escolares.  
> En cuanto a la “decisión de renovación, total o parcial, de dichos materiales, o de contratación de otros distintos”, se comunica que en caso que la Administración valore realizar nuevas licitaciones, éstas se adaptarán a las necesidades del nuevo currículo, pudiendo participar todas aquellas empresas que cumplan los requisitos incluidos en los correspondientes pliegos de prescripciones técnicas."

**16 septiembre 2022**

Planteo solicitud

---

En expediente A/SER-028102/2020 se contrataron vía emergencia en 2020 recursos que indican en contratos "Las licencias adquiridas estarán a disposición de la Consejería de Educación y Juventud de la Comunidad de Madrid durante dos cursos escolares"

En resolución 09-OPEN-00083.0/2022 se indicó que no tenían continuidad para futuros cursos.

"En relación a la adecuación de los materiales actuales a los cursos en los que el currículo se modifica en los cursos 2022-2023 y 2023-2024, cabe señalar que los materiales actuales no tienen la necesidad de ser actualizados pues el uso de estas licencias no tienen continuidad para futuros cursos escolares.  
En cuanto a la “decisión de renovación, total o parcial, de dichos materiales, o de contratación de otros distintos”, se comunica que en caso que la Administración valore realizar nuevas licitaciones, éstas se adaptarán a las necesidades del nuevo currículo, pudiendo participar todas aquellas empresas que cumplan los requisitos incluidos en los correspondientes pliegos de prescripciones técnicas."  

Sin embargo, finalizados esos dos cursos, a día 16 septiembre 2022 en las aulas virtuales de EducaMadrid se pueden seguir añadiendo los bloques "aulaPlaneta Repositorio, Autologin de aulaPlaneta, Banco de contenidos de aulaPlaneta, Buscar en aulaPlaneta,  EdebeON+, Matic de aulaPlaneta"  

Solicito copia o enlace a:

- Documentación (convenio, contrato, acuerdo o similar) que indique las condiciones bajo las que la consejería puede seguir usando esos recursos.

- En caso de que la documentación anterior no exista, comunicaciones enviadas a los responsables de administración de EducaMadrid para que eliminen dichos recursos de las aulas virtuales de Educamadrid al carecer de licencia para su uso.

---

**16 noviembre 2022**

¡Tras haberse [autoampliado el plazo para responder](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Madrid5e/2022-10-14-AmpliacionPlazo.pdf)!, ["responden" esto](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Madrid5e/aulaplaneta/2022-07-21-ResolucionInformacionPlaneta.pdf)

> "RESUELVE  
En relación al primer punto de la solicitud, se informa de que no se dispone de la documentación solicitada.  
En cuanto al segundo punto de la solicitud, se informa de que no existen las comunicaciones requeridas por el solicitante."

### Madrid 2023

**9 enero 2023**

Se publica licitación en Diario Oficial de la Unión Europea

[España-Madrid: Servicios TI: consultoría, desarrollo de software, Internet y apoyo 2023/S 006-013534 Anuncio de información previa](https://ted.europa.eu/udl?uri=TED:NOTICE:13534-2023:TEXT:ES:HTML)  

que indica "El presente anuncio tiene por objeto reducir los plazos de recepción de ofertas" 

**18 enero 2023**

Recibo respuesta de DPD (asociada a Edebé ON, que forma parte de Madrid5e: se cita Madrid5e en la respuesta)  

[2023-01-18 Respuesta reclamación DPD Edebé](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Madrid5e/edebeon/2023-01-18-Respuesta+reclamaci%C3%B3n+DPD+-+Edebe.pdf)  

> "Se ha revisado con la Dirección General responsable el Pliego de Prescripciones Técnicas del nuevo contrato de Madrid5E ..."

Cuando Madrid5E está asociada a un adjudicatario concreto, que es grupo Planeta

**19 abril 2023**

Se publica licitación  A/SER-048423/2022 (322O-001-23)
[Consultoría y desarrollo del software de una colección de objetos digitales educativos para la docencia en centros sostenidos con fondos públicos de Educación Infantil y Primaria, Educación Secundaria Obligatoria y Bachillerato](https://contratos-publicos.comunidad.madrid/contrato-publico/consultoria-desarrollo-software-coleccion-objetos-digitales-educativos-docencia-0)

Presupuesto base licitación. Importe total 7.949.697,58 euros  
Duración del contrato 36 Meses

**11 mayo 2023**

ANELE pone recurso, del que desiste día 17

[TCAP Tribunal Administrativo de Contratación Pública Recurso nº 200/2023 Resolución nº 207/2023](https://contratos-publicos.comunidad.madrid/medias/resolucioncensuradopdf-0/download) 

**4 julio 2023**

Se adjudica, lote 2 es para Grupo Planeta A08186249
[Adjudicación](https://contratos-publicos.comunidad.madrid/medias/resolucionpublicacioncensuradopdf-4/download)

> De acuerdo con la oferta económica presentada por EDITORIAL PLANETA S.A para el LOTE 2 el importe total de adjudicación del LOTE 2 , asciende a **4.089.800€** (Base Imponible:3.380.000€,IVA:709.800€).  
> Lote 2: Servicio de desarrollo de software de Objetos digitales Educativos  
Secundaria Obligatoria

Se adjudica de la misma manera que con la adjudicación a dedo de 2020: INETUM se lleva la parte de Bachillerato.

> Lote 3: Servicio de desarrollo de software de Objetos digitales Educativos  
Bachillerato

Sin embargo, lo que en 2020 costó 14,5 M€ para 2 años, ahora es menos de la mitad para 3 años.

En octubre 2023 me indican que "obligan" a personas asociadas a CompDigEdu a formarse en aulaPlaneta. 

La idea es en ver pliegos plazos y entregas, pedir documentos sobre cumplimientos y comprobar si hay algo nuevo realmente.  
[A/SER-048423/2022 PLIEGO DE CLÁUSULAS ADMINISTRATIVAS PARTICULARES QUE HA DE REGIR EN EL CONTRATO DE SERVICIOS DE: “CONSULTORÍA Y DESARROLLO DEL SOFTWARE DE UNA COLECCIÓN DE OBJETOS DIGIATALES EDUCATIVOS PARA LA DOCENCIA EN CENTROS SOSTENIDOS CON FONDOS PÚBLICOS DE EDUCACIÓN INFANTIL Y PRIMARIA, EDUCACIÓN SECUNDARIA OBLIGATORIA Y BACHILLERATO” DIVIDIDO EN TRES LOTES A ADJUDICAR POR PROCEDIMIENTO ABIERTO CON PLURALIDAD DE CRITERIOS. FINANCIADO POR LOS FONDOS DE LA AYUDA A LA RECUPERACIÓN PARA LA COHESIÓN Y LOS TERRITORIOS DE EUROPA “REACT-UE](https://contratos-publicos.comunidad.madrid/medias/pcapcensurado1pdf-2/download)

> Cláusula 23. Plazo de ejecución.  
El plazo total y los parciales de ejecución de los trabajos a que se refiere este pliego serán los que
figuran en el apartado 18 de la cláusula 1 o el que se determine en la adjudicación del contrato, siendo los plazos parciales los que se fijen como tales en la aprobación del programa de trabajo, en su caso.  
Los plazos parciales que se fijen en la aprobación del programa de trabajo, con los efectos que en la aprobación se determinen, se entenderán integrantes del contrato a los efectos legales pertinentes.  
El cómputo del plazo para la ejecución del contrato se iniciará el día siguiente al de la formalización de aquél, salvo que se establezca otra cosa en el apartado 18 de la cláusula 1.  
El contratista está obligado a cumplir el contrato dentro del plazo total fijado para la realización del mismo, así como de los plazos parciales señalados para su ejecución sucesiva.

Se formalizó 14 agosto.

> 17.- Programa de trabajo.  
Procede: No  
>  
> 18.- Plazo de ejecución.  
Total: 36 meses. Desde el 1 de marzo de 2023, o desde la fecha de formalización si esta fuera posterior  
**Recepciones parciales: NO**  
Procede la prórroga del contrato: No  
Duración máxima del contrato incluidas las prórrogas: 36 meses  

> Cláusula 21. Programa de trabajo.  
> En cuanto a la obligación de presentación del programa de trabajo, se estará a lo que determina el apartado 17 de la cláusula 1.

> 24.- Régimen de pagos.  
Forma de pago: Un único pago tras la puesta a disposición por parte de la empresa  
adjudicataria de los objetos digitales desarrollados por el servicio en la plataforma **Madrid Digital.**  

_Pone Madrid Digital y no EducaMadrid!!!_

Al mismo tiempo que pone que no hay recepciones parciales, por lo que aplicarían los 36 meses, se ponen en "21.- Penalidades. Por ejecución defectuosa del contrato! penalizaciones retrasos de días!!!?? citando pliego prescripciones técnicas.

En 2.1.2 (Primaria), 2.2.2 (Secundaria) y  2.3.2 (Bachillerato)  Actuaciones a realizar de Pliego de prescripciones técnicas se indica

> "Tras la firma del contrato el adjudicatario desarrollará el software para la integración de la colección de ODEs objeto del contrato en los entornos de Aula Virtual de la Consejería de Educación, y en un plazo no superior a 30 días para los ODEs del nivel 2 OA (Objetos de Aprendizaje) y 45 días para los ODEs del nivel 3 SD (Secuencia Didáctica) y Nivel 4 PF (Programa de Formación)...
(la entrega solo se considerará válida cumplida con la validación de la dirección del proyecto)"

**13 octubre 2023**

Planteo solicitud de acceso

---

Solicito copia o enlace de la siguiente documentación asociada a expediente A/SER-A/SER-048423/2022 (322O-001-23)

https://contratos-publicos.comunidad.madrid/contrato-publico/consultoria-desarrollo-software-coleccion-objetos-digitales-educativos-docencia-0

1. Documentos que reflejen con fecha de cumplimiento de los actos formales y positivos de recepción según artículo 210 Ley 9/2017

https://www.boe.es/buscar/act.php?id=BOE-A-2017-12902#a2-22  

lo que incluye la validación de la dirección del proyecto ya que en 2.1.2 (Primaria), 2.2.2 (Secundaria) y  2.3.2 (Bachillerato)  Actuaciones a realizar de Pliego de prescripciones técnicas se indica

"Tras la firma del contrato el adjudicatario desarrollará el software para la integración de la colección de ODEs objeto del contrato en los entornos de Aula Virtual de la Consejería de Educación, y en un plazo no superior a 30 días para los ODEs del nivel 2 OA (Objetos de Aprendizaje) y 45 días para los ODEs del nivel 3 SD (Secuencia Didáctica) y Nivel 4 PF (Programa de Formación)...

(la entrega solo se considerará válida cumplida con la validación de la dirección del proyecto)"

y la formalización se firmó por los contratistas el 27 de julio de 2023, publicándose en el portal el 14 de agosto de 2023.

2. Documentos que reflejen que se ha realizado la puesta en funcionamiento de los ODEs desde la plataforma de Aula Virtual de EducaMadrid.

3. Documentos que reflejen si se ha realizado el pago según apartado 24 de clausula 1 del pliego de prescripciones administrativas, indicando en qué plataforma de Madrid Digital se han puesto a disposición (tal y como indica el pliego), además de la plataforma del Aula Virtual de EducaMadrid que no forma parte de MadridDigital.

---

43/306860.9/23

**12 diciembre 2023**

Recibo [resolución](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Madrid5e/aulaplaneta/2023-12-12-ResolucionPlaneta.pdf), les basta un pantallazo para indicar que los materiales están y son correctos


### Información Madrid5e en general

https://madrid5e.com/ 

Dominio registrado el 8 enero 2021

https://es.godaddy.com/whois/results.aspx?itc=dlp_domain_whois&domain=madrid5e.com

No se puede ver el registrador, pero es Planeta mirando que en madrid5e.com el pie

Política de uso de Cookies | Política de privacidad | Condiciones de uso

redirige a datos de aulaPlaneta. 

[Reunión con Aula Planeta](https://www.comunidad.madrid/transparencia/agenda/reunion-aula-planeta)  
Miércoles, 11/11/2020 - 10:00  
Convocante  
Manuel Bautista Monjón  
Unidad Organizativa Responsable  
Dirección General de Educación Concertada, Becas y Ayudas al Estudio  

[(Reunión sobre el ) Proyecto Madrid5e](https://www.comunidad.madrid/transparencia/agenda/proyecto-madrid5e)  
Viernes, 11/12/2020 - 11:00  
Convocante  
Rocío Albert López-Ibor  
Unidad Organizativa Responsable  
Viceconsejería de Política Educativa  

Relación de participantes por parte de la Comunidad de Madrid  
David Cervera Olivares  
José Luis Villena Acedos  
Jacinto Aramendi Ledesma  

Relación de otros participantes por parte de la Comunidad de Madrid  
MERCEDES MARIN GARCIA  

[Díaz Ayuso presenta una nueva plataforma educativa con contenidos curriculares bilingües](https://www.comunidad.madrid/noticias/2021/01/28/diaz-ayuso-presenta-nueva-plataforma-educativa-contenidos-curriculares-bilingues)  
La presidenta de la Comunidad de Madrid, Isabel Díaz Ayuso, ha presentado Madrid 5e: Currículo digital integrado, una herramienta que el Gobierno regional ha puesto este curso al alcance de los alumnos, desde 5º de Primaria hasta 2º de Bachillerato, con materiales digitales para las asignaturas troncales, con más de 300.000 contenidos curriculares tanto en castellano como en inglés.

[Presentación “MADRID 5e: Educación digital en el siglo XXI”](https://www.comunidad.madrid/transparencia/agenda/presentacion-madrid-5e-educacion-digital-siglo-xxi)  
Jueves, 28/01/2021 - 11:45  
Convocante  
Isabel Díaz Ayuso  
Unidad Organizativa Responsable  
Presidenta  
La presidenta de la Comunidad de Madrid, Isabel Díaz Ayuso, presenta Madrid 5e: Currículo digital integrado, herramienta educativa que pone a disposición de los centros docentes de la región recursos digitales para las asignaturas troncales, desde 5º de Educación Primaria hasta 2º de Bachillerato. Participan José Creuheras, presidente de Grupo Planeta, y Carlos Muñoz, director general de Inetum Iberia-Latam. Acompaña a la presidenta el consejero de Educación y Juventud, Enrique Ossorio.

[programas de innovación educativa y formación del profesorado desarrollados en el curso 2020-2021](https://gestiona3.madrid.org/bvirtual/BVCM050435.pdf)

> 1. Programas de nuevos recursos digitales  
> 1.1. Madrid 5e  
> ...  
> Por otra parte, Madrid5e Bachillerato ofrece a todos los docentes y alumnos de 1º y 2º de Bachillerato de la
Comunidad de Madrid una serie de recursos digitales interactivos.

En esta página
[Innovación y formación del profesorado. Recursos educativos](https://innovacionyformacion.educa.madrid.org/recursos-educativos)  

aparecía información (que no está en octubre 2023):

[Madrid5e innovacionyformacion.educa.madrid.org archive.org](https://web.archive.org/web/20230601023150/https://innovacionyformacion.educa.madrid.org/node/28688)

> Es una apuesta de la Comunidad de Madrid para integrar contenidos curriculares digitales en las aulas. Este proyecto cuenta con dos módulos diferentes:  
> * [AulaPlaneta](https://madrid5e.com/), que proporciona contenidos curriculares desde 5º de primera a 4º de la ESO
> * [EdebeOn+ Inetum](https://www.madrid5ebachillerato.com/), proporciona contenidos curriculares para 1º y 2º de bachillerato  
> La Comunidad de Madrid brinda a los colegios el acceso a los contenidos y recursos curriculares de aulaPlaneta, de las asignaturas troncales desde 5º de Primaria a 4º de ESO.  
> Saca el máximo partido a tus clases con aulaPlaneta en Educamadrid.

[Madrid5E. Seminarios web (noviembre 2020)](https://www.educa2.madrid.org/web/revista-digital/convocatorias/-/visor/madrid5e-seminarios-web)

[Madrid5e](https://www.educa2.madrid.org/web/revista-digital/inicio/-/visor/madrid5e2)

[Descubre aulaPlaneta en Madrid5e](https://sites.google.com/view/indice-educamadrid/p%C3%A1gina-principal)

[educación y digitalización después de la pandemia: oportunidades de futuro](http://www.madrid.org/bvirtual/BVCM050466.pdf)  
> Madrid 5e es una apuesta de la Comunidad de Madrid para integrar contenidos curriculares digitales en las aulas. Este proyecto abarca contenidos curriculares digitales interactivos desde Infantil hasta 2º de Bachillerato para las asignaturas troncales y en español e inglés. De esta manera los docentes pueden integrar junto a estos contenidos otros en abierto o propios. 

En octubre 2023 se cita "Responsable del proyecto Madrid5e" a una persona de Planeta  
[Proyecto Madrid5e](https://sites.google.com/view/indice-sites/inicio/digitalizaci%C3%B3n)  

**27 octubre 2023**

Se envía correo a todos los docentes desde

Dirección General de Bilingüismo y Calidad de la Enseñanza <dgmce@educa.madrid.org>

Se incluye este boletín
[Proyecto Madrid 5e. Docentes - 25/10/2023](https://boletines.educa.madrid.org/boletin/a91626a665381751caea96bef26acc14)

Se incluyen enlaces a 3 vídeos con fecha 24 octubre 2023

[C RECURSOS INNOVACIÓN - mediateca](https://mediateca.educa.madrid.org/centro/innovacion)  
[MADRID 5e - Objetos Digitales Educativos de Primaria](https://mediateca.educa.madrid.org/video/dmrew4yycpy655qa)  
[MADRID 5e - Objetos Digitales Educativos de Secundaria](https://mediateca.educa.madrid.org/video/4pakg4iawyg9epwx)  
[MADRID 5e - Objetos Digitales Educativos de Bachillerato](https://mediateca.educa.madrid.org/video/rfws4mg2i9zbe9i2)  

[Madrid 5e: Formación para docentes de Secundaria - 19/12/2023](https://boletines.educa.madrid.org/boletin/27aa121665819d8944a09de8bb39fc21)  

En enero 2024 hay más enlaces  

[MADRID 5e - Formación ODEs Secundaria Nivel 3 y 4](https://mediateca.educa.madrid.org/video/t5ub3sknacaa5exx)

[Seminario web Madrid 5e ODE Secundaria 16/01/2024](https://mediateca.educa.madrid.org/video/9xlsa7ns398at6qi)

31 enero 2024 se envía boletín a los docentes  
[Proyecto Madrid 5e. Docentes - 31/01/2024](https://boletines.educa.madrid.org/boletin/f2463a9065b811b02543b5367f8af3c5)  
 

13 febrero 2024 se envía correo a docentes con curso donde aparece Madrid5e (en este caso al ser bachillerato es Edebé, no Planeta)  

[Integración y uso didáctico de ODEs en tu aula de Bachillerato (I_0021)](https://innovacionyformacion.educa.madrid.org/actividades/integracion-uso-didactico-odes-aula-bachillerato-i0021)  
> Objetivos  
Favorecer y facilitar el uso de los ODES nivel 2, 3 y 4 de Edebé On+ en las aulas virtuales.

29 febrero 2024 se envía correo
[Proyecto Madrid 5e. Docentes - 29/02/2024](https://boletines.educa.madrid.org/boletin/11a05ee465dde891e9c0841f662ed07c)

1 febrero 2024 se envía correo

[Proyecto Madrid 5e. Docentes. Abril - 01/04/2024](https://boletines.educa.madrid.org/boletin/106625a6660248873e998e75ef9716d0)

14 mayo 2024 se envía correo

Estimado docente,

El próximo 25 de mayo se realizará en el CTIF Madrid-Capital la Jornada Madrid5e-Aulaplaneta “Inspírate y conecta con tu aula”, en la que se hablará sobre los retos de la educación actual, compartiendo formas de ver y afrontar estos retos y generando espacios en los que compartir realidades y experiencias de aula de docente a docente.

Está dirigido a docentes de Enseñanza Secundaria y la certificación conlleva un crédito de formación. Para más información e inscripciones puede acceder al siguiente enlace.

Un cordial saludo.

[Jornada Madrid5e-Aulaplaneta “Inspírate y conecta con tu aula”](https://innovacionyformacion.educa.madrid.org/actividades/jornada-madrid5e-aulaplaneta-inspirate-conecta-aula)  


### Información aulaPlaneta

**30 abril 2013**

[Se presenta aulaPlaneta, un innovador servicio educativo digital - antena3](https://www.antena3.com/noticias/cultura/presenta-aulaplaneta-innovador-servicio-educativo-digital_20130430574897636584a8317e0e3a7d.html)

[SIMO Educación 2013 - aulaPlaneta Recopilatorio de entrevistas y noticias sobre el SIMO Educación 2013 relacionadas con aulaPlaneta.](https://www.youtube.com/playlist?list=PLQigX-k8viurWT0TMOti9bfiFGsWAZUbm)

[@aulaplaneta youtube](https://www.youtube.com/@aulaplaneta)

[www.aulaplaneta.com](https://www.aulaplaneta.com/)

En 2023 la nota de prensa más reciente es de 2022

[sala de prensa - aulaplaneta](https://www.aulaplaneta.com/sala-de-prensa)

[sala de prensa - aulaplaneta - archive (2023)](https://web.archive.org/web/20230609171312/https://www.aulaplaneta.com/sala-de-prensa)

En octubre 2023 y enero 2024 en 

[aulaplaneta > Mi Comunidad > Comunidad Autónoma de Madrid](https://docsp.aulaplaneta.com/mi-comunidad/madrid)  

[Captura](https://web.archive.org/web/20231003171013/https://docsp.aulaplaneta.com/mi-comunidad/madrid)

indica normativa LOMCE derogada, al tiempo que indica que es la vigente

> Comunidad Autónoma de Madrid  
> La propuesta didáctica de aulaPlaneta garantiza la completa cobertura del currículo educativo vigente, así como de los aspectos ligados a la realidad propia de Madrid. Todos los materiales se han organizado por curso y área de acuerdo con la legislación en vigor, que establecen los decretos autonómicos correspondientes:  
> DECRETO 89/2014, de 24 de julio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el Currículo de la Educación Primaria.  
> DECRETO 48/2015, de 14 de mayo, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo de la Educación Secundaria Obligatoria.

**15 diciembre 2021**

[Nace el nuevo Aulaplaneta, el compañero digital que facilita el día a día del docente](https://www.aulaplaneta.com/2021/12/15/novedades-aulaplaneta/nace-el-nuevo-aulaplaneta-el-companero-digital-que-facilita-el-dia-a-dia-del-docente)

> El nuevo Aulaplaneta, fruto de la innovación y adaptación permanente, nace con la vocación de ayudar y acompañar al docente en un momento de cambio por la adaptación a la nueva ley orgánica educativa: la LOMLOE.

**5 mayo 2022** 

[La LOMLOE busca la mejora de los resultados y el crecimineto de las oportunidades formativas con una educación de calidad](https://www.aulaplaneta.com/2022/05/05/noticias-sobre-educacion/la-lomloe-busca-la-mejora-de-los-resultados)

Los bloques de aulaPlaneta ("Aulaplaneta repositorio" y "Banco de contenidos de aulaplaneta") se añaden por defecto a las nuevas aulas virtuales de EducaMadrid, y siguen en octubre 2023.

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhC9on91QHR8eJ7V6VjsnDVXjUkq8k9MgB956qlFEA1fVVtiGZ9hTYlrOey695EXv3WsczDLW8jFUjpsm5RvqrvDke9kt52UluQel2O7tAmRpI750oDe-H_M0X3rn-qxm7N0VU7VYk-psAQ1bj_nbDCSHMRJKgb_XLDfFWi9laHwu1EALb7vyOq0umFJgIL/s16000/Screenshot%202023-10-13.png)

**15 octubre 2023**

Miro un ejemplo que conozco: Física y Química 4ESO  
[Comparación currículo Física y Química 4º ESO - fiquipedia](https://fiquipedia.es/home/materias/eso/fisicayquimica-4eso/comparacion-curriculo-fisica-y-quimica-4-eso/)  
Novedad Madrid es añadir ondas y rendimiento, y en lo que hay ahora en Aulas Virtuales de aulaPlaneta no lo localizo ¿venden como LOMLOE lo que tenían de LOMCE? ¿entregan algo "disponible próximamente"?

![](https://pbs.twimg.com/media/F8f4_2nWMAEIjfa?format=jpg)

### Información grupo Planeta en general

**12 febrero 2020**

[Serie_Octuvre (3/4): El franquismo detrás de La Sexta y Antena3 - youtube](https://www.youtube.com/watch?v=JUfWj8BdrOI&t=106s)  
> 1:46 - L'origen del Grupo Planeta  

