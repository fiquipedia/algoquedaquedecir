+++
author = "Enrique García"
title = "Horarios secundaria en distintas CCAA"
date = "2018-01-19"
tags = [
    "Madrid", "educación", "normativa", "Post migrado desde blogger"
]
toc = true

description = "Horarios secundaria en distintas CCAA"
thumbnail = "https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/2024-02-24-HorariosSecundaria.png"
images = ["https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/2024-02-24-HorariosSecundaria.png"]

+++

Revisado 31 agosto 2024

## Resumen

En algunas comunidades [se ha normalizado](http://algoquedaquedecir.blogspot.com/2018/01/2018-objetivo-evitar-normalizacion.html) que tener 30 periodos en el horario semanal de secundaria cuando no pasa en otras CCAA que tienen un número menor de periodos. También se ha normalizado tener [20 periodos lectivos que fijó artículo 3 de Real Decreto-Ley 14/2012](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=6&tn=1&p=20120421#a3), que según artículo [86.1 Constitución Española debería ser provisional](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a86), pero desde 2012 que se amplió a 20 han pasado muchos años sin volver a 18; en 2018 tras la moción de censura surge derogar ese cambio, y al tiempo que se deroga surge la idea de uniformizar horarios entre CCAA y la comento aquí.

Realizo una comparación gráfica de horario semanal en distintas CCAA. Aquí solo imagen, detalles más adelante, incluyo hoja de cálculo con los datos usados. 

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/2024-02-24-HorariosSecundaria.png)

En 2024 actualizo para curso 2023-2024 tras migrar post desde blogger, la revisión está relacionada con la huelga en Madrid asociada a la reducción de horas lectivas, ver post [Reducción lectivas Madrid](../reduccion-lectivas-madrid)  

**Pendiente actualizar para curso 2024-2025 (normativa y gráfica resumen)**, solo revisadas algunas comunidades.  

Este post está relacionado con posts:

* [Horarios docentes secundaria](http://algoquedaquedecir.blogspot.com/2017/09/horarios-docentes-secundaria.html) (se documentan de manera general)
* [Recordar antes de acordar: acuerdo sectorial educación Madrid 2017-2021](http://algoquedaquedecir.blogspot.com/2017/09/recordar-antes-de-acordar-acuerdo.html) se cita "reducción" a 19 periodos lectivos en Madrid para algunos casos 
* [Derogar RDL14/2012](http://algoquedaquedecir.blogspot.com/2018/08/derogar-rdl142012.html) (en 2018 se plantea derogar aumento a 20 periodos que se hizo en 2012)

## Detalle

Los temas generales documentados en post [horarios de docentes secundaria](http://algoquedaquedecir.blogspot.com/2017/09/horarios-docentes-secundaria.html)

Antes de este post había comentado por Twitter (marco la parte que aplica a este post, en otro post trato tema de [horarios secundaria Madrid "ilegales"](http://algoquedaquedecir.blogspot.com/2018/01/horarios-secundaria-madrid-ilegales.html))

[twitter FiQuiPedia/status/950786601487040512](https://twitter.com/FiQuiPedia/status/950786601487040512)  
Bajar de 20 a 18 depende RDL 14/2012 estatal, pero se debería bajar de 30 a 27 períodos en horarios: todos los horarios de secundaria de Madrid a 30 son ilegales, no pasa en otras CCAA.  
[twitter FiQuiPedia/status/950790365728124928](https://twitter.com/FiQuiPedia/status/950790365728124928)  
en otras CCAA han hecho trampas y es ilegal, como es ilegal bajar a 19 en Madrid a tutores, lo detalló en post; lo que es lectivo o no lo marca normativa. Veo tan grave tener 3 periodos de más en horario, sólo Madrid e ilegales, que 20 lectivos "legales" por nefasto RDL 14/2012.

La idea inicial del post era mostrar que Madrid es la única comunidad que tiene horarios a 28 h = 30 períodos, y difundir para que se sepa, y conseguir que se cambie.  
En septiembre 2018 al plantear derogación RDL14/2012 surge la idea de que normativa sera homogénea entre CCAA y tema de ser homogénea con privados con concierto financiados con dinero público.

Al migrar el post en 2024 y actualizarlo veo que pecaba de "madridcentrismo" y cambio el nombre para que en lguar de ser "Madrid vs CCAA" pase a ser "en distintas CCAA"

### Normativa y realidad horarios secundaria por CCAA

En resumen inicial pongo gráfica: algunas cosas documentadas con ejemplos concretos (marcaba inicialmente con asterisco), y sin asterisco lo documentado con normativa (la normativa que no es de Madrid no la conozco bien y puedo interpretar algo mal, abierto a comentarios)

Comienzo por Madrid y luego el resto. 

Como escribo el post inicialmente en 2018 y luego voy revisando, intento poner para cada comunidad primero un resumen para el último curso y enlace a documentación general.

Horas y periodos no son lo mismo: ojo a los detalles, al final para comparar uso periodos de 55 minutos.

Es importante que esto es general: puede haber horarios que no sean exactamente así, porque se fijan unos mínimos y máximos.

#### Comunidad de Madrid

Resumen 2024-2025: 28 horas y 30 periodos permanencia, 20 periodos lectivos. 

Documentado en post sobre horarios secundaria, son 28 horas y 30 periodos.   
Regulado en instrucciones de inicio de curso  
[Ámbito general, comienzo de curso, admisión de alumnos y consejos escolares](http://www.comunidad.madrid/servicios/educacion/ambito-general-comienzo-curso-admision-alumnos-consejos-escolares)

Incluyo mi propio horario curso 2017-2018 como prueba adicional (ese curso soy tutor 3º ESO) donde se puede ver
"Total de periodos de obligada permanencia en el centro: 30"

![](https://2.bp.blogspot.com/-PffSrtYgfUo/WmE7gCs5AGI/AAAAAAAAObY/CDukzXkZ0GgBES_y-1go_UDuBuy0RgDPwCLcBGAs/s1600/IMG-20180118-WA0001.jpg)

Incluye el resumen similar de otro horario de otro docente también tutor 3º ESO  
También se ve "Total de periodos de obligada permanencia en el centro: 30"  
Pero lo peculiar es que la "reducción a 19" se hace de manera distinta (19+1), cuando en mi caso (en la misma Comunidad de Madrid) se hace como 18+2  

![](https://3.bp.blogspot.com/-cOTxjjLrMEc/WmjJ9z8c5NI/AAAAAAAAOd8/dmHclZzLR48iMZlzD23Z-Xdwe-J3NTvdwCLcBGAs/s1600/Horario2.jpg)

En 2019-2020 no varía respecto 2017-2018, más allá de que ciertas horas a tutores se consideren lectivas.

​[INSTRUCCIONES de las Viceconsejerías de Política Educativa y Ciencia y de Organización Educativa, de 5 de julio de 2019, sobre comienzo del curso escolar 2019- 2020 en centros docentes públicos no universitarios de la Comunidad de Madrid.](http://www.comunidad.madrid/sites/default/files/doc/educacion/instrucciones_inicio_curso_2019_2020_firmado.pdf)  
CSV : 1038077242549437344108

>4.3.4.2. Horario regular: lectivo y complementario  
...  
El número de periodos semanales del horario regular, **entre lectivos y complementarios, será de 30**, y el número de **periodos lectivos, con carácter general, de 20 semanales.**  

**Curso 2021-2022**

Las instrucciones siguen igual
[Direcciones de Área / Inicio / Normativa inicio de curso 25/06/2021 - Intrucciones Inicio de curso 2021-2022](https://www.educa2.madrid.org/web/direcciones-de-area/normativa-inicio-de-curso/-/visor/25-06-2021-intrucciones-inicio-de-curso-2021-2022)

[Instrucciones de las Viceconsejerías de Política Educativa y de Organización Educativa, sobre comienzo del curso escolar 2021-2022 en centros docentes públicos no universitarios de la Comunidad de Madrid.](https://www.educa2.madrid.org/web/educamadrid/principal/files/6a25dc54-e0d9-4d0e-b81a-3f3018eebe43/instrucciones_inicio_de_curso_21_22_VOE_24475451.pdf?t=1624614708645)  
CSV 0981587477518839178477

> 6.1.4.2. Horario regular: lectivo y complementario  
> 1. El horario regular de los profesores que imparten enseñanzas en los centros públicos de Educación  Secundaria  se  organizará  por  periodos  lectivos,  que  con  carácter  general tendrán una duración mínima de 55 minutos, para ajustarse a los periodos lectivos de los alumnos.  
> 2. El número de periodos semanales del horario regular, **entre lectivos y complementarios, será de 30**, y el número de **periodos lectivos, con carácter general, de 20 semanales.**  

> 6.1.4.3. Horario lectivo  
Se considerarán periodos lectivos:  
...  
> 11. Los tutores de ESO contabilizarán un segundo periodo lectivo de su horario personal para el
desarrollo de actuaciones recogidas en el Plan de Convivencia del centro a que hace
referencia el Decreto 32/2019, de 9 de abril, por el que se establece el marco regulador de la
convivencia en los centros docentes de la Comunidad de Madrid. Asimismo, los tutores de
los cursos primero y segundo de Bachillerato contabilizarán un periodo lectivo de su horario
personal para el desarrollo de actuaciones recogidas en dicho Plan de Convivencia del
centro.

> 6.1.4.6. Compensación por exceso de horas lectivas  
El régimen de compensación con horas complementarias será como máximo un periodo
complementario por cada periodo lectivo, y sólo a partir de las veinte horas semanales de carácter
lectivo, **respetándose en todo caso los 30 periodos de permanencia semanal en el centro.**

**2023-2024**

[INSTRUCCIONES de las Viceconsejerías de Política Educativa y de Organización Educativa de 13 de junio de 2023 sobre comienzo del curso escolar 2023-2024 en centros docentes públicos no universitarios de la Comunidad de Madrid](https://www.comunidad.madrid/transparencia/sites/default/files/regulation/documents/resolucion_conjunta_instrucciones_inicio_de_curso_23-24.pdf)

Sin cambios 

**2024-2025**

[INSTRUCCIONES de la Viceconsejería de Política y Organización Educativa, de 5 de julio de 2024, sobre comienzo del curso escolar 2024-2025 en centros docentes públicos no universitarios de la Comunidad de Madrid.](https://www.comunidad.madrid/sites/default/files/instrucciones_inicio_curso_2024_2025.pdf)  

Sin cambios

#### Andalucía

Resumen 2023-2024: 28 horas y 25 periodos permanencia, 18 horas lectivas. 

En diciembre 2017 me lo confirman y me envían un pantallazo de Séneca

![](https://2.bp.blogspot.com/-cv9KVv6xOpI/WmCe2aAs6qI/AAAAAAAAOag/7n_C4wB6VzEFMRFMWRyhRmTnhdwAzSNAQCEwYBhgL/s1600/2017-12-02-FotoHorarioAndalucia.jpg)

En enero 2018 me pasan imagen de horario curso 2017-2018

![](https://2.bp.blogspot.com/-8X3YO8IA0JQ/WmE2Jz4Ch6I/AAAAAAAAObI/IhRWIOtKCRgkZ67tXPEWoipCge53zVRHgCLcBGAs/s1600/2018-AndaluciaHorario.jpg)

[Conoce cómo se estructura tu horario laboral teniendo en cuenta el que es regular y no regular, las horas consideradas como lectivas, las de libre disposición y las posibles reducciones horarias aplicables. - feandalucia.ccoo.es](https://www.feandalucia.ccoo.es/docu/p5sd15006.pdf)

[Acuerdo de 17 de julio de 2018, del Consejo de Gobierno, por el que se aprueba el Acuerdo Marco de 13 de julio de 2018, de la Mesa General de Negociación Común del Personal Funcionario, Estatutario y Laboral de la Administración de la Junta de Andalucía, para la mejora de la calidad del empleo público y de las condiciones de trabajo del personal del sector público andaluz.](https://www.juntadeandalucia.es/boja/2018/140/3)  
> Para contribuir a esta consolidación de la calidad del empleo público se llevará a cabo la recuperación progresiva del poder adquisitivo, de derechos económicos, el desarrollo de la carrera profesional, una mayor incorporación de personal al sector público, la ampliación de la estabilización en el empleo público y la reducción de la temporalidad, la definitiva implantación de la jornada de 35 horas y **la recuperación de las 18 horas lectivas del personal docente**, y también otras medidas novedosas y favorecedoras de la conciliación de la vida personal, familiar y laboral, que harán posible progresar en la cultura de la corresponsabilidad entre mujeres y hombres en la atención a las cargas familiares y en el reparto equilibrado de los permisos y licencias.

> Sexto. Jornada de trabajo.  
...  
> 4.º Asimismo, coincidiendo con el inicio del curso escolar 2018-2019, **el horario lectivo del personal docente será de dieciocho horas**, a excepción del que imparte las enseñanzas de educación infantil y primaria y el de los Centros Específicos de Educación Especial, sin que pueda resultar aplicable, a partir de ese momento, la medida del apartado Tercero.1 del Acuerdo de esta Mesa General de 18 de diciembre de 2017.

**Curso 2021-2022**

La normativa general de secundaria remite a normativa 2010
[Inicio > Centros educativos > Organización y funcionamiento - juntadeandalucia.es](https://www.juntadeandalucia.es/organismos/educacionydeporte/areas/centros-educativos/organizacion-funcionamiento.html)

[Orden de 20 de agosto de 2010, por la que se regula la organización y el funcionamiento de los Institutos de Educación Secundaria (IES), así como el horario de los centros, del alumnado y del profesorado. (BOJA 30-08-2010)](https://www.juntadeandalucia.es/boja/2010/169/1)  
> Artículo 13. Horario individual del profesorado.  
...  
> 3. La parte lectiva del horario regular será de un mínimo de 18 horas, pudiendo llegar excepcionalmente a 21 si la distribución horaria del instituto lo exige, y se dedicará a las siguientes actividades:

Las instrucciones para curso 2021-2022 no dicen nada explícito sobre horario

[INSTRUCCIONES DE 13 DE JULIO DE 2021, DE LA VICECONSEJERÍA DE EDUCACIÓN Y DEPORTE, RELATIVAS A LA ORGANIZACIÓN DE LOS CENTROS DOCENTES Y A LA FLEXIBILIZACIÓN CURRICULAR PARA EL CURSO ESCOLAR 2021/22](https://anpeandalucia.es/openFile.php?link=notices/att/1/2021-07-13-instrucciones_inicio_de_curso-2021_2022_t1626261183_1_1.pdf)  
Verificación tFc2e8GFQS9BGFESED5EAUB4ZQ99G3

> el horario individual del profesorado se adaptará a la circunstancia establecida en
el punto 1 de este apartado, sin que se modifiquen las condiciones reguladas en la normativa que resulte de aplicación para cada una   de las enseñanzas. El profesorado no verá incrementado, como consecuencia de esta circunstancia, su horario de obligada permanencia en el centro

Considero que aplica acuerdo de 2018 y son 18 horas (no periodos de 55 minutos)

[CSIF > Andalucía > Educación > GUÍA SOBRE HORARIOS - SECUNDARIA Curso 2023/2024 06 de Septiembre de 2023 ](https://www.csif.es/contenido/andalucia/educacion/304046)  
> 25 h. de horario regular: 18 son horas lectivas (según el Acuerdo 17/07/2018 de Consejo de Gobierno, BOJA Nº 140 del 20/07/2018) y el resto son horas NO lectivas.  

#### Aragón

Resumen 2023-2024: 25 horas de permanencia, 27 periodos permanencia, 18 horas lectivas. 
	
[Instrucciones a centros educativos](https://educa.aragon.es/-/instrucciones-a-centros)

[ORDEN de 18 de mayo de 2015, de la Consejera de Educación, Universidad, Cultura y Deporte por la que se aprueban las Instrucciones que regulan la organización y el funcionamiento de los Institutos de Educación Secundaria de la Comunidad Autónoma de Aragón. ](https://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=858511505757)  
> 73. La suma de la duración de los periodos lectivos y de los periodos
complementarios de obligada permanencia en el Centro, recogidos en el horario
individual de cada profesor, será de **veinticinco horas semanales**. Aun cuando los
periodos lectivos tengan una duración inferior a sesenta minutos, no se podrá alterar,
en ningún caso, el total de horas de dedicación al centro; en el caso de que los periodos
lectivos sean de 55 minutos, **el número de periodos reflejados en el horario individual del profesorado será de 27**.

[ORDEN ECD/779/2016, de 11 de julio, por la que se modifica el anexo de la Orden de 18 de mayo de 2015, de la Consejera de Educación, Universidad, Cultura y Deporte, por la que se aprueban las Instrucciones que regulan la organización y el funcionamiento de los Institutos de Educación Secundaria de la Comunidad Autónoma de Aragón.](https://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=919780964949)  

**5 octubre 2018**
[La DGA y los sindicatos negocian la bajada de horas lectivas para los docentes de Secundaria](https://www.heraldo.es/noticias/aragon/2018/10/05/la-dga-los-sindicatos-negocian-bajada-horas-lectivas-para-los-docentes-secundaria-1270150-300.html)  
El Consejo Escolar ha pedido al Gobierno de España que se unifiquen los tiempos lectivos en 18 horas semanales para Secundaria y 23 en Primaria e Infantil.

**14 mayo 2019**
[Los profesores irán a la huelga el día 23 para volver a las 18 horas lectivas](https://www.elperiodicodearagon.com/noticias/aragon/profesores-iran-huelga-dia-23-volver-18-horas-lectivas_1362176.html)  
Las organizaciones sindicales han criticado este martes en rueda de prensa que el Gobierno del PSOE se comprometió a recuperar el horario lectivo de 18 horas antes de acabar la legislatura

[El Departamento de Educación ofrece a los sindicatos una reorganización de la jornada laboral](https://www.europapress.es/aragon/noticia-departamento-educacion-ofrece-sindicatos-reorganizacion-jornada-laboral-20190514171937.html)  
> El Departamento de Educación, Cultura y Deporte del Gobierno de Aragón ha ofrecido a los sindicatos una reorganización de la jornada laboral basada en una **bajada de la jornada lectiva para los mayores de 55 años a 18 horas semanales en Secundaria** y a 23 en Primaria en el próximo curso 2019-2020.     
Así lo ha explicado el Ejecutivo autonómico, que ha precisado que también ha propuesto que esa **reducción a 18 horas en Secundaria se dé para todo el profesorado en el curso 2020-2021**, y llegue a las 24 horas para los maestros, para bajar definitivamente a 23 en el curso 2021-2022.  

**Curso 2021-2022**

[INSTRUCCIONES DE LA SECRETARIA GENERAL TÉCNICA DEL DEPARTAMENTO DE EDUCACIÓN, CULTURA Y DEPORTE, PARA LOS INSTITUTOS DE EDUCACIÓN SECUNDARIA DE LA COMUNIDAD AUTÓNOMA DE ARAGÓN EN RELACIÓN CON EL CURSO 2021-2022](https://educa.aragon.es/documents/20126/394129/4.+INSTRUCCIONES+SECUNDARIA+para+21-22+.pdf/68059d90-3c1b-48c4-1578-2cbd2caf35d8?t=1627539922087)  
> Orden de 18 de mayo de 2015, de la Consejera de
Educación, Universidad, Cultura y Deporte por la que se aprueban las Instrucciones que
regulan la organización y el funcionamiento de los Institutos de Educación Secundaria de la
Comunidad Autónoma de Aragón, modificada por Orden ECD/779/2016, de 11 de julio, BOA
de 29 de julio (en adelante Instrucciones de Organización y Funcionamiento - II.O.F.)  
...   
> El profesorado de los IES debe permanecer en el centro treinta horas semanales. Cuando
los periodos lectivos tengan una duración inferior a sesenta minutos no se alterará el total de
horas de dedicación al centro; en los casos en los que los periodos lectivos hayan sido
planificados de 55 minutos, el número de periodos reflejados en el horario individual del
profesorado será 27 (Instrucción 73 de las II.O.F).

**Curso 2023-2024**

[ORDEN ECD/961/2023, de 18 de julio, por la que se aprueban las Instrucciones para los Institutos de Educación Secundaria de la Comunidad Autónoma de Aragón en relación con el curso 2023/2024.](https://educa.aragon.es/documents/20126/3652837/Orden+ECD_961_2023+aprueban+Instrucciones+23-24+IES.pdf/f7b1002b-6996-239b-20e7-f47495cb9b30?t=1690442339444)  
> En relación a los horarios, se estará a lo establecido en la Orden de 18 de mayo de 2015, de la
Consejera de Educación, Universidad, Cultura y Deporte por la que se aprueban las Instrucciones
que regulan la organización y el funcionamiento de los Institutos de Educación Secundaria de la
Comunidad Autónoma de Aragón, modificada por Orden ECD/779/2016, de 11 de julio, BOA de 29
de julio (en adelante Instrucciones de Organización y Funcionamiento - II.O.F.)  
...  
> De acuerdo con lo establecido en la Circular de la Dirección General de Personal de 10 de
mayo de 2023 relativa a la programación del cupo de docentes de Educación Secundaria en
Centros Públicos docentes de la Comunidad Autónoma de Aragón para el curso 2023/2024,
teniendo en cuenta el Pacto de Mesa Sectorial de 21 de noviembre de 2022 para la
Recuperación y Mejora de las condiciones laborales del profesorado de Aragón, **el cupo de profesorado se computará a 19 horas lectivas**.

#### Principado de Asturias

Resumen 2023-2024: 25 horas de permanencia, 18 periodos lectivos. 

[Circulares e instrucciones - educastur](https://www.educastur.es/centros/organizacion-de-centros/normativa-y-documentacion/circulares-e-instrucciones)

Visto [Horarios secundaria - ccoo.es](http://docpublicos.ccoo.es/cendoc/045333HorariosSecundaria.pdf) de 2015, cita normativa, y habla de 27 h y 29 periodos.

[CIRCULAR DE INICIO DE CURSO 2017-2018 PARA LOS CENTROS DOCENTES PÚBLICOS. Edición 18 de julio de 2017. TEXTO CONSOLIDADO. Edición 29 de agosto de 2017](https://www.educastur.es/documents/34868/316236/2017_07_CIR_InicioCursoPublicos_20170718.pdf/548602d0-0839-7bb9-958c-b234b4750cc7?version=1.0&t=1630587378348)  
>3.5. Elaboración de horarios  
...  
Horario del profesorado  
...  	
> En los Institutos de Educación Secundaria, se tendrán en cuenta los apartados
102 a 145 inclusive del anexo de la Resolución de 6 de agosto de 2001 por la que se
aprueban las instrucciones que regulan la organización y funcionamiento de los Institutos
de Educación Secundaria del Principado de Asturias, modificada por la Resolución de 5 de
agosto de 2004 (BOPA 17/08/2004), la Resolución de 27 de agosto del 2012 (BOPA
29/08/2012) y por la Resolución de 5 de mayo de 2014 (BOPA 22/05/2014).  
Los apartados 104 y 106 de la precitada Resolución quedarán redactados, en su
modificación prevista, en los siguientes términos:  
> 104. La suma de la duración de los períodos lectivos y complementarios de
**obligada permanencia en el Instituto, recogidas en el horario del profesorado, será de veintiséis horas semanales**.

Me comentan un ejemplo curso 2017-2018 de 28 periodos: sin ser jefe departamento y sin dar bilingüe: horario 20 periodos lectivos y 8 complementarios desglosados en 1 guardia de aula, 1 guardia de biblioteca, 1 reunión de departamento, 1 atención a familias, 1 reunión de tutores, 2 REDES, y 1 de preparación de prácticas.

**28 noviembre 2018**

[Principado y sindicatos acuerdan la vuelta a las 18 horas para los docentes de Secundaria para el próximo curso](https://www.20minutos.es/noticia/3503591/0/principado-sindicatos-acuerdan-vuelta-18-horas-para-docentes-secundaria-para-proximo-curso/)  
La Consejería de Educación y Cultura y los sindicatos Suatea, UGT, ANPE y CCOO han acordado restablecer la jornada lectiva de 18 horas para los profesores de Secundaria a partir del curso 2019/2020. El acuerdo, alcanzado por unanimidad, está condicionada a la derogación del decreto-ley 14/2012, en trámite en el Congreso de los Diputados.

**23 agosto 2019**

[Colegios en los que solo el director es funcionario: Educación publica las vacantes](https://www.lavozdeasturias.es/amp/noticia/asturias/2019/08/23/colegios-solo-director-funcionario-educacion-publica-vacantes/00031566573561965487991.htm)  
La consejería ha colgado a última hora de la mañana el listado completo de la macroconvocatoria del verano. Las circulares de inicio de curso regulan la media jornada de 18 horas

[INSTRUCCIONES. Centros docentes públicos. Curso 2019-2020](https://www.educastur.es/-/instrucciones.-centros-docentes-p%C3%BAblicos.-curso-2019-2020)  
[CIRCULAR POR LA QUE SE DICTAN INSTRUCCIONES PARA EL CURSO ESCOLAR 2019-2020 PARA LOS CENTROS DOCENTES PÚBLICOS Edición 23 de agosto de 2019](https://www.educastur.es/documents/34868/38433/2019-09_circulares_inicio-curso_publicos-mod.pdf/f4e64f95-f17c-e2dd-950b-5f6dfe5197b9?t=1621800881366)

En la circular se cita

[Resolución de 17 de mayo de 2019, de la Consejería de Educación y Cultura, de quinta modificación de la Resolución de 6 de agosto de 2001, de la Consejería de Educación y Cultura, por la que se aprueban las instrucciones que regulan la organización y funcionamiento de los Institutos de Educación Secundaria del Principado de Asturias.](https://sede.asturias.es/bopa/2019/05/27/2019-05277.pdf)  
>“114. El Profesorado de Enseñanza Secundaria, el Profesorado Técnico de Formación profesional, y los Maestros y las Maestras que imparten docencia en el primer y segundo curso de la Educación Secundaria Obligatoria, impartirán, como mínimo, dieciocho períodos lectivos, pudiendo llegar excepcionalmente a veintiuno cuando la distribución horaria del departamento lo exija y siempre dentro del mismo. **Cada período lectivo que exceda de los dieciocho se computará con un período menos de obligada permanencia en el centro”.**

**Curso 2021-2022**

[Circulares inicio de curso 2021-2022. Centros públicos y privados sostenidos con fondos públicos](https://www.educastur.es/-/circulares-inicio-de-curso-2021-2022.-centros-p%C3%BAblicos-y-privados-sostenidos-con-fondos-p%C3%BAblicos)  
[CIRCULAR POR LA QUE SE DICTAN INSTRUCCIONES PARA EL CURSO ESCOLAR 2021-2022 PARA LOS CENTROS DOCENTES PÚBLICOS Julio de 2021](https://www.educastur.es/documents/34868/291792/2021-07-cen-circulares-circular-inicio-21-22-centros-publicos.pdf/31f0815c-c650-c436-b0ed-1920ae2e7a64?t=1627470684508)  
>  8.2 HORARIO DEL PROFESORADO  
...  
En los institutos de Educación Secundaria y centros de Enseñanzas Artísticas, se tendrán en
cuenta los apartados 102 a 145 inclusive del anexo de la Resolución de 6 de agosto de 2001. El
texto consolidado se encuentra en la Resolución de 6 de agosto de 2001.  
La suma de la duración de los periodos lectivos y complementarios de **obligada permanencia en el centro docente, recogidos en el horario de los profesores de secundaria, será de 25 horas semanales.**  

Se citan instrucciones de 2001, para secundaria es a partir página 14

[RESOLUCION de 6 de agosto de 2001, de la Consejería de Educación y Cultura, por la que se aprueban las instrucciones que regulan la organización y funcionamiento de los Institutos de Educación Secundaria del Principado de Asturias](https://sede.asturias.es/bopa/2001/08/13/20010813.pdf#page=14)  
> III.3.1.1 III.3.1.1. Profesorado de Enseñanza Secundaria, Profesorado Técnico de Formación Profesional y Maestros y Maestras con destino en el Instituto  
> 114. El Profesorado de Enseñanza Secundaria, el Profesorado Técnico de Formación Profesional, y los Maestros
y Maestras que imparten docencia en el primer ciclo
de la Educación Secundaria Obligatoria, impartirán,
**como mínimo, dieciocho períodos lectivos**, pudiendo
llegar excepcionalmente a veintiuno cuando la distribución horaria del departamento lo exija y siempre dentro del mismo. Cada período lectivo que exceda de los dieciocho se computará con un período menos de
obligada permanencia en el centro.

[Circulares inicio de curso 2023-2024. Centros públicos y privados sostenidos con fondos públicos](https://www.educastur.es/-/circulares-inicio-de-curso-2023-2024)
[Circular de inicio de curso. Centros públicos (pdf)](https://www.educastur.es/documents/34868/38433/2023-06-circular-inicio-curso-centros-publicos.pdf/5ad0846c-bdee-5c10-34f0-df5fb9bea736?t=1687251650532)

Sin cambios respecto 2021-2022

#### Islas Baleares

Resumen 2023-2024: 26 horas de permanencia, 28 periodos de permanencia, 18 periodos lectivos. 

[ Inici > Conselleria d'Educació i Universitats > Secretaria Autonòmica de Desenvolupament Educatiu > Direcció General de Planificació i Gestió Educatives > Instruccions d'organització i funcionament](https://www.caib.es/sites/instruccions201617/)  
(en la url indica 201617 pero parece enlazar al último curso, y no están disponibles enlaces de cursos anteriores)  

[INSTRUCCIONS PER A L’ORGANITZACIÓ I EL FUNCIONAMENT DELS CENTRES DOCENTS PÚBLICS D’EDUCACIÓ SECUNDÀRIA PER AL CURS 2017-2018](http://www.caib.es/sites/instruccions201617/ca/educacia_secundaria_i_educacia_per_adults/archivopub.do?ctrl=MCRST4905ZI229059&id=229059)
lleva a  
[Instruccions sobre l'horari general dels centres, dels alumnes i dels professors](http://www.caib.es/govern/sac/fitxa.do?codi=2639009&coduo=36&lang=ca)  
donde aparecen Instruccions sobre l'horari general dels centres, dels alumnes i dels professors dels centres d'educació infantil, primària i secundària de les Illes Balears (enlace no operativo en 2024)

> **26 hores dedicades a activitats lectives i complementàries (equivalent a 28 períodes)** amb horari fix en el centre i de compliment obligatori (classes, guàrdies i vigilàncies de pati, tutoria dels alumnes i atenció a les famílies, reunions de departament i coordinacions, manteniment de laboratoris, tallers i aules específiques, activitats de suport i d’ampliació, etc.).  

> Els docents faran un **màxim de 18 períodes de docència directa** (classes, suports i tutoria) a un grup d’alumnes. El professorat que, excepcionalment, imparteixi més de 18 períodes lectius de docència directa a un grup d’alumnes tendrà 1 període menys de permanència al centre per cada període de més (compensació d’hores lectives, CHL, que no són de permanència obligada al centre). 

Instruccions sobre l'horari general dels centres, dels alumnes i dels professors (NOVETAT)
Darrera actualització: 07 març 2019  
Instruccions sobre l’horari general dels centres, dels alumnes i dels professors dels centres d’educació infantil, primària i secundària de les Illes Balears (enlace no disponible en 2024)  

>3.3.1.1 Períodes lectius  
El nombre de períodes lectius (55 minuts) dels professors ha de ser entre 18 i 21. Aquest nombre de períodes setmanals lectius pot prendre’s com a mitjana mínima quan els ensenyaments no presentin una distribució horària uniforme al llarg del curs.  
Permanència al centre:  
- Els períodes de permanència al centre de compliment obligatori del professorat, entre lectius (classes, tutoria, suports, caporalia de departament, coordinacions, equip directiu, FCT i majors de 55) i complementaris, han de ser 28.  

**Curso 2021-2022**

Lo que veo más reciente es de 2020

Darrera actualització: 30 juliol 2020  
[Instruccions sobre l’horari general dels centres, dels alumnes i dels professors dels centres d’educació infantil, primària i secundària de les Illes Balears](http://www.caib.es/govern/sac/fitxa.do?codi=2639009&coduo=36&lang=ca)  
> 3.3.1.1 Períodes lectius  
El nombre de períodes lectius (55 minuts) dels professors ha de ser entre 18 i 21. Aquest nombre de períodes setmanals lectius pot prendre’s com a mitjana mínima quan els ensenyaments no presentin una distribució horària uniforme al llarg del curs. Permanència al centre:  
- Els períodes de permanència al centre de compliment obligatori del professorat, entre lectius (classes, tutoria, suports, caporalia de departament, coordinacions, equip directiu, FCT i majors de 55)  i complementaris, han de ser 28.  

**Curso 2023-2024**

[Instruccions per a l’organització i el funcionament dels centres docents públics de segon cicle d’educació infantil i educació primària, i d’educació secundària per al curs 2023-2024.](https://www.caib.es/sites/instruccions201617/ca/centres_pablics_deducacia_infantil_educacia_primaria_i_educacia_secundaria_2023-2024/archivopub.do?ctrl=MCRST4905ZI436593&id=436593)  

Se enlaza 

[Instruccions sobre l’horari general, l’horari de l’alumnat i l’horari dels docents dels centres educatius de les Illes Balears sostinguts amb fons públics per al curs 2023-2024](https://www.caib.es/govern/rest/arxiu/5864117) (enlace operativo en 2024)  
> 1.3 Horari dels professors dels centres públics de secundària  
...  
**26 hores dedicades a activitats lectives i complementàries (equivalent a 28 períodes de 55 minuts)** amb horari fix en el centre i de compliment obligatori ...  
...  
b. Activitats lectives  
El nombre de períodes lectius (55 minuts) dels professors ha de ser entre 18 i 21.  

#### Canarias

Resumen 2023-2024: 24 horas de permanencia: 18 horas lectivas + 6 horas complementarias 

[Educación  >  Centros  >  Gestión de centros educativos  >  Organización y funcionamiento  >  Calendario escolar e instrucciones generales](https://www.gobiernodecanarias.org/educacion/web/centros/gestion_centros/organizacion-funcionamiento/instrucciones/)

[5076 ORDEN de 9 de octubre de 2013, por la que se desarrolla el Decreto 81/2010, de 8 de julio, por el que se aprueba el Reglamento Orgánico de los centros docentes públicos no universitarios de la Comunidad Autónoma de Canarias, en lo referente a su organización y funcionamiento.](http://www.gobiernodecanarias.org/boc/2013/200/001.html)  
> Artículo 36.- Jornada de trabajo: aspectos generales.  
> 1. La jornada semanal del profesorado de los Institutos de Educación Secundaria es la siguiente:  
Ver anexo en la página 26144 del documento [Descargar](http://sede.gobcan.es/boc/boc-a-2013-200-5076.pdf)

En tabla de anexo indica "26 horas de permanencia en el centro" y "20 horas lectivas y 6 horas complementarias"

**13 mayo 2019**

[El profesorado de Secundaria recuperará la jornada lectiva de 18 horas el próximo curso](https://www3.gobiernodecanarias.org/noticias/el-profesorado-de-secundaria-recuperara-la-jornada-lectiva-de-18-horas-el-proximo-curso/)  
El Consejo de Gobierno de Canarias aprobó hoy un acuerdo por el que se establece, a partir del curso escolar 2019/2020, la parte lectiva de la jornada semanal del personal docente no universitario que imparte las enseñanzas reguladas en la Ley orgánica 2/2006 de 3 de mayo, de Educación, en centros públicos, distintas a educación infantil y primaria, en dieciocho horas.  
Este cambio supondrá la inversión de 63,7 millones de euros, para la contratación de 1.250 docentes más, que se incorporarán a las diferentes enseñanzas afectadas por la medida, como la Educación Secundaria, la Formación Profesional, la de Adultos y las Enseñanzas de Régimen Especial.

[3854 ORDEN de 16 de julio de 2019, por la que se modifica la Orden de 9 de octubre de 2013, que desarrolla el Decreto 81/2010, de 8 de julio, por el que se aprueba el Reglamento Orgánico de los centros docentes públicos no universitarios de la Comunidad Autónoma de Canarias, en lo referente a su organización y funcionamiento.](https://www.gobiernodecanarias.org/boc/2019/145/001.html)  
> Uno. El apartado 1 del artículo 36, que queda redactado del siguiente modo:  
"1.- La jornada semanal del profesorado en los centros en los que se impartan las enseñanzas de educación secundaria y educación superior no universitaria es la siguiente:  
Ver anexo en la página 29600 del documento [Descargar](http://sede.gobcan.es/boc/boc-a-2019-145-3854.pdf)  

En tabla anexo indica 24 horas de permanencia: 18 horas lectivas + 6 horas complementarias

**Curso 2021-2022** 

[Instrucciones de organización y funcionamiento de los centros públicos, curso 2021-2022](https://www3.gobiernodecanarias.org/medusa/edublog/cprofesgrancanarianoroeste/2021/07/02/instrucciones-de-organizacion-y-funcionamiento-de-los-centros-publicos-curso-2021-2022/)

Sin cambios localizados; se considera que aplican ya las 18+6. Viendo tablas de pdf, son 18 horas, no 18 periodos, pero no es muy consistente. Si se consideran 18 horas serían 18/(55/60)=19,6 periodos, y 6 horas serían 6,5 periodos, y 24 h serían 26,1 periodos. Considero 19+7 periodos

**Curso 2023-2024**

[2126 Viceconsejería de Educación, Universidades y Deportes.- Resolución de 16 de junio de 2023, por la que se dictan instrucciones de organización y funcionamiento dirigidas a los centros docentes públicos no universitarios de la Comunidad Autónoma de Canarias para el curso 2023-2024. ](https://www.gobiernodecanarias.org/boc/2023/127/005.html)  
>  ANEXO III  
CENTROS PÚBLICOS DE EDUCACIÓN SECUNDARIA, DE ENSEÑANZAS
DE RÉGIMEN ESPECIAL Y DE EDUCACIÓN DE PERSONAS ADULTAS  
...  
3.2. JORNADA LABORAL. La jornada de trabajo semanal del profesorado es la establecida en los artículos 36 y 37 de la Orden de 9 de octubre de 2013, que se ajustará a lo indicado en la Orden de 16 de julio de 2019. 


#### Cantabria

Resumen 2023-2024: 25 horas de permanencia, 18 periodos lectivos 

[Instrucciones de Inicio y Fin de Curso](https://www.educantabria.es/centros/instrucciones-de-inicio-y-fin-de-curso)  

En instrucciones inicio de curso 2017-2018 (enlace no operativo en 2024) se indicaba  
>G.3. ASPECTOS COMUNES PARA TODO EL PROFESORADO DEL CENTRO  
>1. En  los horarios personales debe constar la permanencia en el centro durante veinticinco horas semanales. Si los períodos tienen una  duración igual o inferior a cincuenta y cinco minutos, se consignarán veintisiete períodos, que equivalen a las veinticinco horas establecidas de permanencia en el centro.  
> 2. El horario del profesorado quedará distribuido como se establece a continuación:  
Períodos Lectivos 20 ; Horas Complementarias 6  

En instrucciones de inicio de curso 2019-2020 (enlace no operativo en 2024) se indicaba  
>2. Los profesores de enseñanza secundaria, profesores técnicos y maestros impartirán, en función de su jornada (media, dos tercios o completa) entre 9 y 18 periodos lectivos, pudiendo llegar excepcionalmente a 21. El cuadro siguiente indica el número de periodos lectivos y horas complementarias correspondientes a los diferentes casos que se pueden dar:  
...  
Periodos lectivos 18 / Horas complementarias 9 / CHL -  
...  

En instrucciones de inicio de curso 2020-2021 (enlace no operativo en 2024) se indicaba  
> 2. Los profesores de enseñanza secundaria, profesores técnicos y maestros impartirán respectivamente, en función de su jornada -media, dos tercios o completa- entre 9 y 10 periodos lectivos, entre 12 y 14 periodos lectivos y 18 periodos lectivos pudiendo llegar excepcionalmente a 21. El cuadro siguiente indica el número de periodos lectivos y horas complementarias de obligado cumplimiento  correspondientes a los diferentes casos que se pueden dar:  
...  
Periodos lectivos 18 / Horas complementarias 9 / CHL -  
...  

**Curso 2023-2024**

[Instrucciones de Inicio de Curso Institutos de Educación Secundaria Curso 2023-2024](https://www.educantabria.es/documents/39930/20861917/INSTRUCCIONES+IES+INICIO+CURSO+23-24+VER+final_signed.pdf/99e0b2a9-5b2c-6640-d9f3-9dc838232ad8?t=1693564794296)  
> C.2. HORARIO DEL PROFESORADO DEL CENTRO  
La distribución y el horario de permanencia obligatoria del profesorado del centro se
atendrá a lo dispuesto en el apartado Horario del profesorado de las Instrucciones
sobre horarios en los Institutos de Educación Secundaria emitidas por la Dirección
General de Personal Docente y Ordenación académica con fecha de 7 de junio de 2023.

[INSTRUCCIONES SOBRE HORARIOS EN LOS INSTITUTOS DE EDUCACIÓN SECUNDARIA - anpecantabria.org](http://anpecantabria.org/Documentos/IIC%2023-24/Instrucciones%20sobre%20horarios%20IES%2023-24.pdf)  
> 3. La suma de la duración de los periodos lectivos y de los periodos complementarios
recogidos en el horario individual de cada profesor/a, será de **veinticinco horas semanales**, aun cuando dichos periodos tengan una duración inferior a sesenta minutos,
no se podrá alterar, en ningún caso, el total de las horas de dedicación al centro.  
...  
b) Horario de permanencia obligatoria:  
> 1. El profesorado destinado en el centro, impartirá, **con carácter general, dieciocho periodos lectivos**, pudiendo llegar hasta veinte periodos lectivos semanales,
	
#### Castilla-La Mancha

Resumen 2024-2025: 26 periodos de permanencia, 19 periodos lectivos 

[Centros Educativos > Organización y Funcionamiento](https://www.educa.jccm.es/es/centros/organizacion-funcionamiento)

[Centros Educativos › Organización y Funcionamiento › Normativa de Organización y Funcionamiento > Institutos de Educación Secundaria, Bachillerato y Formación Profesional](https://www.educa.jccm.es/es/centros/organizacion-funcionamiento/normativa-organizacion-funcionamiento/institutos-educacion-secundaria-bachillerato-formacion-prof)

[Horario semanal del profesorado SECUNDARIA. Lapicerillo con toda la información - ste-clm.com](https://ste-clm.com/horario-semanal-del-profesorado-secundaria-lapicerillo-con-toda-la-informacion/)

[Orden de 02/07/2012, de la Consejería de Educación, Cultura y Deportes (IES) por la que se dictan instrucciones que regulan la organización y funcionamiento de los institutos de educación secundaria en la Comunidad Autónoma de Castilla-La Mancha.](http://www.educa.jccm.es/es/normativa/orden-02-07-2012-consejeria-educacion-cultura-deportes-ies.ficheros/152214-IES_2012_9771.pdf)  
>92. El horario semanal de obligada permanencia en el centro es de veintinueve horas. Para su cumplimiento, se estará a lo dispuesto por la Administración educativa en la regulación del calendario escolar y del horario de los centros docentes.  
>93. Con carácter general, de las **veintinueve horas semanales de permanencia en el centro, veintiuna tendrán la consideración de lectivas** y el resto de complementarias.

Me informa un profesor que está a 26 periodos en curso 2017-2018 y me pasa su horario (son de 55 minutos)

![](https://2.bp.blogspot.com/-VDZsccZKP5w/WmE8X82g8oI/AAAAAAAAObc/4f0_lD-miV0xl3YrmOPwGD3uh5aK8fNmACLcBGAs/s1600/2017-HorarioCLM.jpg)

**Curso 2021-2022**

[Resolución de 16/06/2021, de la Consejería de Educación, Cultura y Deportes, por la que se dictan instrucciones para el curso 2021/2022 en la comunidad autónoma de Castilla-La Mancha.2021/7403](https://www.csif.es/sites/default/files/field/file/Resol%2016%20de%20junio%20inicio%20curso%2021%2022.pdf)

Para horarios se remite a órdenes de organización y funcionamiento

Y ahí en 2021 lo último es de 2012, de modo que se puede asumir se mantenían los 20+6 periodos

En 2024 veo hay nueva normativa de 2022 
[Orden 118/2022, de 14 de junio, de la Consejería de Educación, Cultura y Deporte de regulación de la organización y el funcionamiento de los centros públicos que imparten enseñanzas de Educación Secundaria Obligatoria, Bachillerato y Formación Profesional en la comunidad de Castilla-La Mancha](https://www.educa.jccm.es/es/normativa/orden-118-2022-14-junio-consejeria-educacion-cultura-deport.ficheros/407166-EEMM_2022_5883.pdf)  
> Artículo 15. Horario profesorado.  
> 1. El horario semanal del profesorado de los Institutos de Educación Secundaria incluirá periodos lectivos y
complementarios, hasta sumar un total de **veintinueve horas de obligada permanencia en el centro.** De acuerdo
con lo que las Normas de organización, funcionamiento y convivencia establezcan, **tres periodos complementarios podrán ser de cómputo mensual** para las actividades que no se hayan incluido de modo ordinario en el horario
semanal.

Me confirman que en 2023-2024 siguen a 20 lectivas  
[twitter ProfaDeQuimica/status/1720545086398742990](https://twitter.com/ProfaDeQuimica/status/1720545086398742990)  
También en CLM vamos a 20 lectivas todavía (de hecho, yo este curso voy a 21).

Viendo horario asumo que normativa hace referencia a horas pero quiere decir periodos.

En curso 2024-2025 se pasa a 19 lectivas  

[Orden 140/2024, de 28 de agosto, de la Consejería de Educación, Cultura y Deportes, por la que se dictan instrucciones sobre medidas educativas, organizativas y de gestión para el desarrollo del curso escolar 2024/2025 en la comunidad autónoma de Castilla-La Mancha.](https://docm.jccm.es/docm/verArchivoHtml.do?ruta=2024/08/30/html/2024_6860.html&tipo=rutaDocm)  
> Artículo 5. Horario del profesorado.  
> 1. El profesorado que imparte docencia en Educación Secundaria Obligatoria, Bachillerato, Formación Profesional, Educación de Personas Adultas y Enseñanzas de Régimen Especial tendrá **19 horas lectivas**. El resto de las horas hasta alcanzar el total de horas de obligada permanencia en el centro serán de carácter complementario. El profesorado que excepcionalmente supere las 19 horas lectivas semanales verá compensado su horario con dos horas complementarias por cada hora que se exceda.  

#### Castilla y León

Resumen 2023-2024: 17 periodos lectivos y 8 periodos complementarios, 25 periodos

Me informa un profesor FP que está a 27 periodos  

[El horario del docente - stecyl.net](http://stecyl.net/el-horario-del-docente/)  
De fecha septiembre 2018 con esta imagen donde aparecen 27 periodos semanales

![](http://stecyl.net/wp-content/uploads/2014/02/CARTEL-HORARIO-EEMM-2017-18-orden-692-1024x707.jpg)

Se cita como normativa 
[ORDEN EDU/491/2012, de 27 de junio, por la que se concretan las medidas urgentes de racionalización del gasto público en el ámbito de los centros docentes no universitarios sostenidos con fondos públicos de la Comunidad de Castilla y León](https://www.educa.jcyl.es/es/resumenbocyl/orden-edu-491-2012-27-junio-concretan-medidas-urgentes-raci)  
[ORDEN EDU/692/2017, de 18 de agosto, por la que se modifica la Orden EDU/491/2012, de 27 de junio, por la que se concretan las medidas urgentes de racionalización del gasto público en el ámbito de los centros docentes no universitarios sostenidos con fondos públicos de la Comunidad de Castilla y León.](https://www.educa.jcyl.es/es/resumenbocyl/orden-edu-491-2012-27-junio-concretan-medidas-urgentes-raci.ficheros/1104874-ORDEN%20EDU-692-2017%2Cde%2018%20de%20agosto.pdf)  
[Ley 1/2012, de 28 de febrero, de Medidas Tributarias, Administrativas y Financieras (Castilla y León)](https://www.boe.es/buscar/doc.php?id=BOE-A-2012-4385)  
> Artículo 70. Jornada laboral del personal docente en centros públicos no universitarios.  
Este profesorado impartirá como **mínimo 18 periodos lectivos semanales, pudiendo llegar excepcionalmente a 21** cuando la distribución horaria del departamento lo exija y siempre dentro del mismo. La parte del horario comprendido entre 19 y 21 periodos lectivos se compensará con las horas complementarias establecidas por la Jefatura de estudios, a razón de dos horas complementarias por cada período lectivo.

Para curso 2021-2022 instrucciones no indican nada de horario

[INSTRUCCIÓN DE 15 DE JUNIO DE 2021, DE LA DIRECCIÓN GENERAL DE CENTROS, PLANIFICACIÓN Y ORDENACIÓN EDUCATIVA, POR LA QUE SE UNIFICAN LAS ACTUACIONES  DE  LOS  CENTROS  DOCENTES  QUE  IMPARTEN  ENSEÑANZAS  NO UNIVERSITARIAS EN CASTILLA Y LEÓN CORRESPONDIENTES AL INICIO DEL  CURSO ESCOLAR 2021/2022.](https://www.educa.jcyl.es/es/institucional/crisis-coronavirus/crisis-coronavirus-protocolos-resoluciones-guias/instruccion-15-junio-2021-direccion-general-centros-planifi.ficheros/1492954-INSTRUCCI%C3%93N%20DE%20INICIO%20DE%20CURSO%2021_22.pdf)

[CSIF INFORMA. Horarios, elección de grupos y horas complementarias. Maestros y Secundaria y OC.](https://www.csif.es/contenido/castilla-y-leon/educacion/204397)  
En ese enlace en 2020-2021 para secundaria indica 20 periodos + 7 complementarias  
Mismo enlace en 2023-2024 para secundaria indica **17 periodos + 8 complementarias** (50 minutos por sesión + 5 minutos del cambio = 25 periodos)  
![](https://www.csif.es/sites/default/files/field/file/Horarios%20PES%20y%20O.C.%202023-24%20%283%29_1.png)

#### Cataluña

Resumen 2023-2024: 24 horas, 18 lectivas

[Llei d’Educació de Catalunya (LEC 12/2009) Disposición adicional trigésima primera. Condiciones laborales del personal adscrito al Departamento de Enseñanza.](https://www.boe.es/buscar/act.php?id=BOE-A-2009-13038#da-3)  
> 4. El horario de permanencia del profesorado de secundaria no puede ser de más de veinticuatro horas semanales.

_esa disposición se añade por el art. 172.4 de la Ley 5/2020, no estaba en 2018 cuando escribo inicialmente el post_

En 2018 me pasan una foto de un horario, con 27 periodos (y se ve son de 1 hora)

![](https://1.bp.blogspot.com/-rQy_xmDihVw/WmemdTOtkjI/AAAAAAAAOco/hpVTrMDrcd8djWA8xPYeCZ7e2jjeN1tuACLcBGAs/s1600/20180123-HorarioCatalu%25C3%25B1a.jpg)

14 noviembre 2018 me puntualizan
[twitter memele87/status/1062821519460569091](https://twitter.com/memele87/status/1062821519460569091)  
En el artículo se basan en 1 horario de 27 horas pero diría que eso es porque hay una guardia de patio (tiempo que suele ser de descanso). El horario normal es de 28 horas en el centro (sin contar recreos, que sumaría más de 30 h).

Efectivamente mirando horario veo que aparecen en 3 recreos de media hora, 2 "PATI" y 1 "GUARDIA", sumaría 28 h y 30 min, pero eso rebasaría. Eso llevaría, usando periodos de 55 minutos por comparar, a 31=28,5·(60/55)  
Habría que ver la regulación exacta; quizá se contabilicen los 5 min de cambio de clase en la hora del horario.  

[Documents per a l'organització i la gestió dels centres Personal docent 09/06/2023](https://documents.espai.educacio.gencat.cat/IPCNormativa/DOIGC/ORG_Personal_docent.pdf)  
> 1.2 Professorat amb destinació als instituts, a les escoles d'art i a les escoles
oficials d'idiomes  
a) **24 hores de permanència al centre** en horari fix: **18 hores lectives** (al llarg dels cinc
dies de la setmana, de dilluns a divendres) i 6 hores d'activitats complementàries  

#### Ceuta y Melilla

Resumen 2023-2024: 25 horas de permanencia, 18 lectivas

[Instrucciones de la dirección general de evaluación y cooperación territorial y de la dirección geneal de formación profesional para su aplicación en las ciudades autónomas de Ceuta y Melilla durante el curso escolar 2017-2018](https://www.csif.es/sites/default/files/field/file/instrucciones%20curso%20escolar%202017-2018%20ceuta%20y%20melilla.pdf)  
> 3.5.1. Distribución del horario  
...  
Para el curso 2017-2018 el número de periodos lectivos semanales serán de 20, el número de periodos complementarios serán de 7, ...  

[INSTRUCCIONES DE LA DIRECCIÓN GENERAL DE EVALUACIÓN Y COOPERACIÓN TERRITORIAL Y DE LA DIRECCIÓN GENERAL DE FORMACIÓN PROFESIONAL PARA SU APLICACIÓN EN LAS CIUDADES AUTÓNOMAS DE CEUTA Y MELILLA, DURANTE EL CURSO ESCOLAR 2019-2020.](http://www.educacionyfp.gob.es/dam/jcr:9b37aaf8-97b8-4cf3-9f1b-09ef0f4b2b5f/instrucciones-curso-19-20-ceuta-melilla.pdf)  
> 2.2.2. HORARIO PROFESORADO DE EDUCACIÓN SECUNDARIA, BACHILLERATO Y FORMACIÓN PROFESIONAL Y ARTES PLÁSTICAS Y DISEÑO  
...  
La distribución del horario de los funcionarios docentes con destino en los Institutos de Educación Secundaria, Bachillerato y Formación Profesional será la establecida en el capítulo V, puntos 69 y siguientes de la Orden de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la organización y funcionamiento de los institutos de educación secundaria.

Normativamente parece que es una bajada a 18 que es lo que reflejaba orden de 1994, pero instrucciones también dicen esto, y eso supone se sigue a 20

> En función del cupo global de profesorado que se haya asignado a cada Dirección Provincial dentro de las limitaciones que la prórroga presupuestaria impone, las Direcciones Provinciales deberán atenerse estrictamente a la distribución del cupo del profesorado, comunicada por la Dirección General de Evaluación y Cooperación Territorial, y a las Instrucciones de la Subsecretaría dictadas al efecto para el comienzo del curso 2019-2020, en relación con la adecuada distribución de los recursos humanos disponibles.

Comento que aunque no se baje a 18, sí aplicaría orden 1994 respecto a CHL, y estar a 20 supondría que no tener 4 CHL sería ilegal en curso 19-20

**27 septiembre 2019**

Aparece en prensa que sí aplica el tema de CHL.

[La reversión fallida de recortes deja al profesorado en la "excepcionalidad" y pone a los IES en un brete](http://www.ceutaldia.com/articulo/educacion/reversion-fallida-recortes-deja-profesorado-excepcionalidad-pone-ies-brete/20190927074143207338.amp.html)

Lo comento en Twitter con una imagen pasada por mail de un sindicato que indica que solo pueden tener una guardia.

[twitter FiQuiPedia/status/1177671522909085699](https://twitter.com/FiQuiPedia/status/1177671522909085699)  
Que alguien a 20 periodos tenga 4 complementarias libres y una única guardia creo que es una mejora, aunque no es logro administración sino casual por el caos normativo.  
PD: en Madrid a 20 no hay CHL, ni tenemos 27 periodos, sino 30.  
![](https://pbs.twimg.com/media/EFfuAQcW4AIv5PP?format=jpg)

**28 septiembre 2019**

[Martínez no atisba “problemas” en los institutos aunque pierdan guardias](https://elfarodeceuta.es/martinez-institutos-guardias/amp/)  
> El director provincial de Educación, Javier Martínez, aseguró este viernes que no cree que la necesidad de compensar cada profesor en los institutos con cuatro horas semanales complementarias por las dos de clase que debe impartir sobre las 18 establecidas en la normativa vigente vaya a generar “mayores problemas”. 

**3 octubre 2019**

[twitter RTVCE/status/1179729112547647489](https://twitter.com/RTVCE/status/1179729112547647489)  
Los institutos compensarán las 20 horas semanales de los profesores con menos horas de guardia. Este asunto ha sido tratado en una reunión esta misma mañana entre el director provincial de @educaciongob, @JMartinezAlonso y los directores de los institutos de la ciudad.  
_Incluye vídeo_  

[Inicio > Contenidos > Ciudades autónomas > Melilla > Ciudad de Melilla > Curso académico 2021-2022](https://www.educacionyfp.gob.es/contenidos/ba/ceuta-melilla/melilla/portada/curso-academico21-22.html)

[INSTRUCCIONES DE LA SECRETARÍA DE ESTADO DE EDUCACIÓN Y DE LA SECRETARÍA GENERAL DE FORMACIÓN PROFESIONAL PARA SU APLICACIÓN EN LAS CIUDADES AUTÓNOMAS DE CEUTA Y MELILLA, EN LAS ENSEÑANZAS DE RÉGIMEN GENERAL DURANTE EL CURSO ESCOLAR 2021-2022](https://www.educacionyfp.gob.es/dam/jcr:a004b4d3-95ce-445e-8144-725eb1cd6d7a/instrucciones-inicio-curso21-22.pdf)  
> 5.2.2.  Horario del profesorado de educación secundaria y bachillerato  
...  
La distribución del horario de los funcionarios docentes con destino en los Institutos de Educación Secundaria, Bachillerato y Formación Profesional será la establecida en el capítulo V, puntos 69 y siguientes de la Orden de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la organización y funcionamiento de los Institutos de Educación Secundaria.

Se elimina la mención a cupo que se hacía en 2019-2020, luego están a 18+9.

[RESOLUCIÓN DE LA SECRETARÍA DE ESTADO DE EDUCACIÓN Y DE LA SECRETARÍA GENERAL DE FORMACIÓN PROFESIONAL POR LA QUE SE APRUEBAN LAS INSTRUCCIONES PARA SU APLICACIÓN EN LAS ENSEÑANZAS DE RÉGIMEN GENERAL EN LAS CIUDADES DE CEUTA Y MELILLA DURANTE EL CURSO ESCOLAR 2023-2024.](https://www.educacionyfp.gob.es/dam/jcr:322b444b-4ee8-4e81-a1bf-f24bf32c51f0/instrucciones-curso-23-24.pdf)  
> 4.2.2. Horario del profesorado de Educación Secundaria Obligatoria, Bachillerato y Formación Profesional  
...  
> 2. La suma de la duración de los períodos lectivos y las horas complementarias de obligada
permanencia en el Instituto, recogidas en el horario individual de cada profesor, será de **veinticinco horas semanales**.
...  
4.2.2.1 Horario lectivo  
> 1. Con carácter general, el profesorado de enseñanza secundaria impartirá **18 periodos lectivos semanales**, de acuerdo con lo establecido en la Orden de 29 de junio de 1994 por la que se aprueban
las instrucciones que regulan la organización y funcionamiento de los institutos de educación
secundaria, y tras la aplicación de las recomendaciones establecidas en el artículo único de la Ley
4/2019, de 7 de marzo. Podrán llegar excepcionalmente a 21 horas cuando la distribución horaria del
departamento lo exija y siempre dentro del mismo o las preferencias de elección de materias del
profesorado así lo requiera. La parte del horario comprendido entre las 18 y 21 horas lectivas se
compensará con las horas complementarias establecidas por la Jefatura de estudios, a razón de dos
horas complementarias por cada período lectivo.

#### Extremadura

Resumen 2023-2024: 25 horas y 27 periodos, y 18 periodos lectivos.

[Inici > oSistema educativo > Legislación Educativa > Secretaria General de Educación > Instrucciones SGE](https://educarex.es/legislacion-sge/instrucciones.html)

[Instrucciones de la Dirección General de Política Educativa de 27 de Junio de 2006, por la que se concretan las normas de carácter general a las que deben adecuar su organización y funcionamiento los Institutos de Educación Secundaria y los Institutos de Educación Secundaria Obligatoria de Extremadura.](https://www.sindicatopide.org/Legislacion/2006.06.27-NormasFunto-IES.pdf)  
> 116. La suma de la duración de los períodos lectivos y complementarios de obligada permanencia en el Instituto, recogidos en el horario individual de cada profesor o profesora, será de **veinticinco horas semanales**.  
...  
> 122. Los Catedráticos de Enseñanza Secundaria, los Profesores de Enseñanza Secundaria, los
Profesores Técnicos de Formación Profesional y los Maestros adscritos al Instituto que imparten
docencia en el primer ciclo de la Educación Secundaria Obligatoria impartirán, **como mínimo, dieciocho períodos lectivos**, pudiendo llegar excepcionalmente a veintiuno cuando la distribución
horaria del Departamento lo exija y siempre dentro del mismo.  

Esas instrucciones son 2006, anterior a RDL14/2012 y parece que no refleja la realidad en el momento de elaborar el post en 2018, como refleja prensa más tarde.

En 2013 tuvo una modificación asociada a proyectos internacionales, no afecta al horario en general

[INSTRUCCIÓN de 3 de julio de 2013, de la Secretaría General de Educación, por la que se modifican las Instrucciones de la Dirección General de Política Educativa, de 27 de junio de 2006, que concretan las normas de carácter general a las que deben adecuar su organización y funcionamiento los Institutos de Educación Secundaria y los Institutos de Educación Secundaria Obligatoria de Extremadura](https://www.csif.es/sites/default/files/field/file/MODIFICACION%20INSTRUCCIONES%2027%20JUNIO%202006_sec.pdf)  
que fue derogada por  
[NSTRUCCIÓN Nº 10/2021, DE 19 DE JULIO, DE LA SECRETARÍA GENERAL DE EDUCACIÓN, QUE REGULA LA PARTICIPACIÓN DE CENTROS E INSTITUCIONES EDUCATIVAS DE LA COMUNIDAD AUTÓNOMA DE EXTREMADURA EN PROGRAMAS Y PROYECTOS INTERNACIONALES](https://www.educarex.es/pub/cont/com/0054/documentos/Instrucciones/2021/huella_Instruccion_Programas_y_Proyectos_Internacionales.pdf)

**21 junio 2018**

[La Junta extiende la jornada de 35 horas semanales a sanitarios y profesores](http://www.hoy.es/extremadura/junta-reduce-jornada-20180620134932-nt.html)  
> Establece un cómputo anual máximo para el personal del SES y fija las 18 horas lectivas para los docentes en dos cursos  
...  
De este modo, según el acuerdo firmado, los profesores realizarán a partir de septiembre una jornada de 19 horas lectivas, en lugar de las 20 actuales, y será el siguiente curso, **en 2019-2020, cuando volverán a las 18 horas lectivas.**  

Comentarios parecen indicar que el cambio no es tal, como ocurrió en Madrid en 2017-2018  

[twitter denobisipsis/status/1009812790746468352](https://twitter.com/denobisipsis/status/1009812790746468352)  
En realidad son 19 para el próximo año, y 18 el siguiente. La cuestión es que no tienen pensado contratar más profesorado. Ya veremos.

[twitter jesus_r/status/1010184109614882816](https://twitter.com/jesus_r/status/1010184109614882816)  
Te cuento: al ser 19 horas, sobra una por cada miembro/claustro y aquí lo mejor: en mi centro esas horas suman más de 100. ¡pero solo mandan un profe=19h! ¿resto? Quitar desdobles, subir ratio, y así. Los periodistas no dicen la verdad=realidad  

[twitter margampons/status/1010151393833967616](https://twitter.com/margampons/status/1010151393833967616)  
No es como lo cuentan...primero a 19 y sin aumento de plantilla...cómo se puede hacer? Quitando desdobles, incrementando la ratio...viva la calidad de la enseñanza!! Es una vergüenza

[Instrucción n.º 22/2019, de 17 de julio de 2019, de la Secretaría General de Educación, por la que se unifican las actuaciones correspondientes al inicio y desarrollo del curso escolar 2019-2020 en los centros docentes no universitarios sostenidos con fondos públicos de la Comunidad Autónoma de Extremadura que imparten enseñanzas de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria o Bachillerato.](https://www.educarex.es/pub/cont/com/0054/documentos/Instrucciones/2019/Instrucci%C3%B3n_inicio_curso_2019-2020.pdf)  
> Tercera. Adecuación del horario del profesorado de enseñanza secundaria, profesores técnicos de formación profesional y profesorado de enseñanzas de régimen especial  
En aplicación del Acuerdo Junta de Extremadura Sindicatos con representación en la Mesa General de Negociación de la Junta de Extremadura para la recuperación de derechos, para la extensión de medidas de flexibilización y para la profundización en las políticas de igualdad del conjunto de empleados y empleadas públicas de la Administración autonómica extremeña, de 20 de junio de 2018, y en virtud, asimismo, del acuerdo alcanzado en Mesa Sectorial de Educación, de 28 de junio de 2018, el horario lectivo mínimo del profesorado perteneciente a los cuerpos de catedráticos y profesores de enseñanza secundaria, profesores técnicos de formación profesional, catedráticos y profesores de música y artes escénicas, catedráticos, profesores y maestros de taller de artes plásticas y diseño, y catedráticos y profesores de escuelas oficiales de idiomas queda establecido en **18 horas lectivas semanales, pudiendo llegar excepcionalmente a 21 horas** cuando la distribución horaria del departamento lo exija y siempre dentro del mismo.  
Las horas lectivas que excedan de 18 se compensará con horas complementarias en la siguiente proporción: a 19 horas lectivas le corresponden 5 horas complementarias y 1 CHL; a 20 horas lectivas le corresponden 3 horas complementarias y 2 CHL; a 21 horas lectivas le corresponden 1 hora complementaria y 3 CHL.  

[Instrucción 9/2021, de 28 de junio, de la Secretaría General de Educación, por la que se unifican las actuaciones correspondientes al inicio y desarrollo del curso escolar 2021/2022 en los centros docentes no universitarios sostenidos con fondos públicos de la Comunidad Autónoma de Extremadura que imparten enseñanzas de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria, Bachillerato, Formación Profesional o Enseñanzas de Régimen Especial](https://educarex.es/pub/cont/com/0054/documentos/Instrucciones/2021/Instruccion_N9_2021_inicio_de_curso_21_22.pdf)  
> 1.  El  horario  lectivo  mínimo  del  profesorado  perteneciente  a  los  cuerpos  de  catedráticos  y  profesores  de  enseñanza secundaria,  profesores  técnicos  de formación profesional, catedráticos  y profesores  de música y artes  escénicas,  catedráticos,  profesores  y  maestros  de  taller  de  artes  plásticas  y  diseño,  y  catedráticos  y profesores de escuelas oficiales de idiomas queda establecido en **19 periodos lectivos semanales, pudiendo llegar excepcionalmente a 21 periodos** cuando la  distribución  horaria  del departamento  lo  exija  y  siempre dentro del mismo.  
Los periodos lectivos que excedan de 19 se compensarán con periodos complementarios en la  siguiente  proporción: a 20 periodos lectivos le corresponden 4 complementarios y 1 CHL; a 21 periodos lectivos le corresponden 2 complementarios y 2 CHL.  
...  
sin que comporte, a su vez, reducción del horario de permanencia en el centro, que sigue fijado en  30  periodos  semanales  

[Instrucción nº 18/2023, de la Secretaría General de Educación, por la que se unifican las actuaciones correspondientes al inicio y desarrollo del curso escolar 2023/2024 en los centros docentes no universitarios sostenidos con fondos públicos de la Comunidad Autónoma de Extremadura que imparten enseñanzas de Educación Infantil, Educación Primaria, Educación Especial, Educación Secundaria Obligatoria, Bachillerato, Formación Profesional o Enseñanzas de Régimen Especial.](https://educarex.es/pub/cont/com/0054/documentos/INSTRUCCIO%CC%81N_inicio_de_curso_2023_2024_DEF_29_06_2023%28F%29%5B3715%5D.pdf)  
> Decimoséptima. – Adecuación del horario del profesorado de secundaria y profesorado de enseñanzas de
régimen especial en centros públicos.  
> 1. El horario lectivo mínimo del profesorado perteneciente a los cuerpos de catedráticos y profesores de
enseñanza secundaria, catedráticos y profesores de música y artes escénicas, catedráticos, profesores y
maestros de taller de artes plásticas y diseño, y catedráticos y profesores de escuelas oficiales de idiomas
queda establecido en **18 periodos lectivos semanales, pudiendo llegar excepcionalmente a 21 periodos**
cuando la distribución horaria del departamento lo exija y siempre dentro del mismo. El horario lectivo
máximo correspondiente a media jornada es de 10 periodos lectivos.  
> 2. Los periodos lectivos que excedan de 18 se compensarán con periodos complementarios en la siguiente
proporción: a 19 periodos lectivos le corresponden 5 complementarios y 1 CHL; a 20 periodos lectivos le
corresponden 3 complementarios y 2 CHL; a 21 periodos lectivos le corresponden 1 complementario y 3
CHL.

#### Galicia

Resumen 2024-2025: 20 horas lectivas + 7 complementarias, 30 horas 

(Gracias a [Alberto Fortes](https://twitter.com/fortesnovoa/status/955907246315180032))

[ORDEN de 23 de junio de 2011 por la que se regula la jornada de trabajo del personal funcionario y laboral docentes que imparten las enseñanzas reguladas en la Ley orgánica 2/2006, de 3 de mayo, de educación.](https://www.xunta.gal/dog/Publicados/2011/20110630/AnuncioC3F1-290611-3585_es.html)  
> Artículo 4. Jornada semanal del personal funcionario y personal laboral que imparte las enseñanzas de educación secundaria, enseñanzas artísticas, de idiomas y deportivas.  
> 1. La jornada semanal del personal funcionario y laboral docente que imparte las enseñanzas de educación secundaria obligatoria, programas de calificación profesional inicial, bachillerato, formación profesional específica, enseñanzas artísticas, de idiomas y deportivas será de treinta y siete horas y medias, de las cuales dedicarán treinta a las actividades del centro con presencia en el mismo. De estas 30 horas semanales, **23 serán de horario fijo en el centro educativo**. De estas 23 horas, **18 tendrán carácter lectivo** pudiendo llegar hasta veinte si las necesidades del centro así lo exigen en función del profesorado disponible.  

Me comenta otro profesor que está actualizada en  
[ORDEN de 4 de junio de 2012 por la que se modifica el artículo 4 de la Orden de 23 de junio de 2011 por la que se regula la jornada de trabajo del personal funcionario y laboral docente que imparte las enseñanzas reguladas en la Ley orgánica 2/2006, de 3 de mayo, de educación.](https://www.xunta.gal/dog/Publicados/2012/20120613/AnuncioG0164-070612-0002_es.html)  
> 1. La jornada semanal del personal funcionario y laboral docente que imparte las enseñanzas de educación secundaria obligatoria, programas de cualificación profesional inicial, bachillerato, formación profesional específica, enseñanzas artísticas, de idiomas y deportivas será de treinta y siete horas y media, de las cuales dedicarán treinta a las actividades del centro con presencia en el mismo. De estas 30 horas semanales, 23 serán de horario fijo en el centro educativo. De estas 23 horas, **20 tendrán carácter lectivo** y las tres restantes se distribuirán entre dos guardias y una hora de tutoría de padres y alumnos.  

Se habla de 30 "actividades del centro con presencia en el mismo" y 3 "horario fijo en el centro" (guardias y tutoría) y asumo 30 en total. 

Se habla de horas pero [me indican](https://x.com/jjcanido/status/1829843651901485189) que hace referencia a periodos de 50 minutos, me mencionan ROC y Galicia tiene propio pero no parece citar duración.  

[DECRETO 324/1996, de 26 de julio, por el que se aprueba el Reglamento orgánico de los institutos de educación secundaria.](https://www.xunta.gal/dog/Publicados/1996/19960809/Anuncio90AA_es.html)  

En [ORDEN de 1 de agosto de 1997 por la que se dictan instrucciones para el desarrollo del Decreto 324/1996 por el que se aprueba el Reglamento orgánico de los institutos de educación secundaria y se establece su organización y funcionamiento.](https://www.xunta.gal/dog/Publicados/1997/19970902/AnuncioAD5A_es.html)  
> 71.a) Cada período lectivo tendrá una duración mínima de 50 minutos.  

[Twitter jjcanido/status/1829895601368203701](https://x.com/jjcanido/status/1829895601368203701)  
Aquí por regla general son 50 minutos, pero p.ej., los centros que tienen calidade tienen sesiones de 1 hora.  
Por otro lado, pone que los alumnos no pueden tener más de 7 sesiones diarias, y sistemáticamente los centros de secundaria gallegos, al tener 32 horas semanales, tienen un día con 1 tarde con 2 sesiones, haciendo un total de 8 sesiones 🤷‍♂️  

Si son 30 periodos de 50 minutos, serían 30*0,50/0,60=25 horas de permanencia en centro.  

Aunque normativa de 2012 parezca antigua, es la citada en 2024 [CIG Ensino > Lexislación > Horario do profesorado](https://www.cig-ensino.gal/horario-do-profesorado.html)  

**4 octubre 2018**
[La propuesta de horas lectivas del Consejo Escolar exigiría 3.000 profesores más en Galicia](https://www.lavozdegalicia.es/noticia/galicia/2018/10/04/propuesta-horas-lectivas-consejo-escolar-exigiria-3000-profesores-galicia/0003_201810G4P12993.htm)  
> 1.200 serían de infantil y primaria y unos 1.600 de secundaria  
> Si la Xunta decide aplicar la propuesta del Consejo Escolar del Estado de que  **los maestros estén 23 horas semanales en el aula y los profesores de secundaria 18,**  entonces  **harían falta 3.000 docentes más en la comunidad** , de los que 1.200 serían de infantil y primaria y unos 1.600 de secundaria; son más o menos los mismos empleos que se perdieron con la crisis.

**5 mayo 2023**
[Educación negociará reducir ratios en ESO y rebajar horarios docentes a los maestros](https://www.farodevigo.es/galicia/2023/10/05/educacion-negociara-reducir-ratios-rebajar-92944155.html)  
> La propuesta sindical en Secundaria, donde los profesores cumplen un horario lectivo de 20 horas, es rebajarlo a 18.

Las instrucciones no citan horarios de docentes

[RESOLUCIÓN de 6 de junio de 2023, de la Dirección General de Ordenación e Innovación Educativa, por la que se dictan instrucciones para el desarrollo de las enseñanzas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en el curso académico 2023/24.](https://www.xunta.gal/dog/Publicados/2023/20230619/AnuncioG0655-080623-0002_es.html)  

[RESOLUCIÓN de 23 de mayo de 2024, de la Dirección General de Ordenación e Innovación Educativa, por la que se dictan instrucciones para el desarrollo de las enseñanzas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en el curso académico 2024/25.](https://www.xunta.gal/dog/Publicados/2024/20240604/AnuncioG0761-270524-0001_es.html)  

#### Región de Murcia

Resumen 2024-2025: 18 horas lectivas + 6 horas complementarias, 24 horas

[Inicio > Educación > Ordenación Académica > Instrucciones de inicio de curso](https://www.carm.es/web/pagina?IDCONTENIDO=64251&IDTIPO=100&RASTRO=c1655$m)

[Orden de 29 de junio de 2017, de la Consejería de Educación, Juventud y Deportes, por la que se establecen procedimientos en materia de recursos humanos para el curso 2017-2018.](https://www.borm.es/borm/documento?obj=anu&id=758761)  
>12.2.3. Distribución del horario del profesorado que imparte docencia
en institutos de educación secundaria, ...  
...  
b) La suma de la duración de los períodos lectivos y las horas
complementarias de obligada permanencia en el centro, recogidas en el horario
individual de cada profesor, será de **veintiséis horas semanales.**
...  
ANEXO VII  
PROFESORADO QUE IMPARTE DOCENCIA EN:  
-INSTITUTOS DE EDUCACIÓN SECUNDARIA  
...  
TABLA I. RÉGIMEN DE JORNADA SEMANAL  
PERIODOS LECTIVOS DE 60 MINUTOS, INCLUIDOS CINCO MINUTOS COMO TIEMPO MÁXIMO PARA EFECTUAR LOS CAMBIOS DE CLASE  
20 + 6  

[Orden de 27 de junio de 2018, de la Consejería de Educación, Juventud y Deportes, por la que se establecen procedimientos en materia de recursos humanos para el curso 2018-2019](http://www.carm.es/web/pagina?IDCONTENIDO=147704&IDTIPO=60)  
Sin cambios respecto 2017

[Orden de 11 de julio de 2019, de la Consejería de Educación, Juventud y Deportes, por la que se prorroga para el curso 2019-2020 la Orden de 27 de junio de 2018, de esa misma Consejería, por la que se establecen procedimientos en materia de recursos humanos para el curso 2018-2019](https://www.carm.es/web/pagina?IDCONTENIDO=65772&IDTIPO=100&RASTRO=c798$m4328,5002)

[Resolución de 17 de julio de 2023, de la Dirección General de Recursos Humanos, Planificación Educativa y Evaluación, Dirección General de Centros Educativos e Infraestructuras y Dirección General de Atención a la Diversidad, Innovación y Formación Profesional de la Consejería de Educación, Formación Profesional y Empleo, por la que se dictan Instrucciones de comienzo del curso 2023-2024 para los centros docentes que imparten Educación Secundaria Obligatoria y Bachillerato](https://www.carm.es/web/descarga?ARCHIVO=Instrucciones%20de%20Inicio%20de%20curso%202023-2024%20ESO%20y%20Bachillerato.pdf&ALIAS=ARCH&IDCONTENIDO=185357&IDTIPO=60&RASTRO=c1655$m64251)  

Remite al Decreto 235/2022 que es currículo y habla de horario de alumnado, no de profesorado.

[Orden de 27 de junio de 2023 de la Consejería de Educación, Formación Profesional y Empleo por la que se establecen procedimientos en materia de recursos humanos para el curso 2023-2024](https://www.carm.es/web/descarga?ARCHIVO=2023-4072.pdf&ALIAS=ARCH&IDCONTENIDO=185054&IDTIPO=60&RASTRO=c798$m4328,5002,72660)  
> ANEXO I  
>PROFESORADO QUE IMPARTE DOCENCIA EN:  
>- INSTITUTOS DE EDUCACIÓN SECUNDARIA  
>...  
TABLA I. RÉGIMEN DE JORNADA SEMANAL  
PERIODOS LECTIVOS DE 60 MINUTOS, INCLUIDOS CINCO MINUTOS COMO TIEMPO MÁXIMO PARA EFECTUAR LOS CAMBIOS DE CLASE  
19+6  (Para 20+4 se refleja 1 hora de compensación horaria)  

[Orden de 28 de junio de 2024 de la Consejería de Educación, Formación Profesional y Empleo por la que se establecen procedimientos en materia de recursos humanos para el curso 2024-2025](https://www.carm.es/web/descarga?ARCHIVO=2024-3339.pdf&ALIAS=ARCH&IDCONTENIDO=191677&IDTIPO=60&RASTRO=c798$m4328,5002,73724,73725)  
> ANEXO III  
>PROFESORADO QUE IMPARTE DOCENCIA EN:  
>- INSTITUTOS DE EDUCACIÓN SECUNDARIA  
>...  
TABLA I. RÉGIMEN DE JORNADA SEMANAL  
PERIODOS LECTIVOS DE 60 MINUTOS, INCLUIDOS CINCO MINUTOS COMO TIEMPO MÁXIMO PARA EFECTUAR LOS CAMBIOS DE CLASE  
18+6  (Para 19+4 se refleja 1 hora de compensación horaria)  

#### Comunidad Foral de Navarra

Resumen 2023-2024: 18 horas lectivas, 25 horas permanencia

[Instrucciones de inicio de curso](https://www.educacion.navarra.es/web/dpto/instrucciones-de-inicio-de-curso)

[Distribución del horario para el profesorado de Secundaria curso 2014 - 2015](https://archivo.aso-apia.org/pdf/documentos/horarios_web_indexados.pdf)

Parece desactualizado citar eso de 2015 en 2018, pero se ve que en 2015 se cita normativa y es de 1998, permite localizarla y tiene notas de vigencia actuales
[DECRETO FORAL 225/1998, DE 6 DE JULIO, POR EL QUE SE REGULA LA JORNADA Y EL HORARIO DEL PROFESORADO DE LOS CENTROS DOCENTES PÚBLICOS QUE IMPARTEN LAS ENSEÑANZAS DE EDUCACIÓN INFANTIL, EDUCACIÓN PRIMARIA, EDUCACIÓN SECUNDARIA OBLIGATORIA, BACHILLERATO, FORMACIÓN PROFESIONAL Y PROGRAMAS DE INICIACIÓN PROFESIONAL](http://www.lexnavarra.navarra.es/detalle.asp?r=28853)  
> Artículo 13  
> 1. El horario lectivo del profesorado que tenga jornada completa constará de **20 horas lectivas semanales** y estará formado por las horas de docencia directa, así como por las horas de cómputo lectivo.  
Excepcionalmente, se podrá llegar a un máximo de veintidós horas lectivas cuando la distribución horaria del centro lo exija y siempre dentro del mismo  
...  
> 3. Cuando un profesor deba impartir más de veinte horas lectivas, se tendrá en cuenta que, en todo caso, la suma de las horas lectivas y las horas complementarias de cómputo semanal que deben figurar en el horario individual de cada profesor será de 25 horas semanales,  

[RESOLUCIÓN 389/2019, de 25 de junio, del Director General de Educación, por la que se aprueban las instrucciones que van a regular, durante el curso 2019-2020, la organización y el funcionamiento de los centros públicos que imparten las enseñanzas de segundo ciclo de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria y Bachillerato en el ámbito territorial de la Comunidad Foral de Navarra.](https://www.educacion.navarra.es/documents/27590/41518/RES+389-2019+INSTRUCCIONES+INDICE.doc/50740b69-3c56-8047-0691-76c60a001532)  

En curso 2019-2020 baja a 18, ya que se indica en página 70

> En el siguiente cuadro se presenta un resumen de la jornada laboral del profesorado a tiempo completo:  
**CUADRO RESUMEN DE LA JORNADA DEL PROFESORADO (a tiempo completo): SECUNDARIA**  
JORNADA ANUAL 1592 h.  
   30 h. SEMANALES DE DEDICACIÓN DIRECTA AL CENTRO   
      18 h. a) DOCENCIA DIRECTA         a) Impartir clase o docencia al alumnado.  

      +  
      b) CÓMPUTO LECTIVO (Horas resultantes de reducir las de docencia directa)  b) Para las labores definidas en el artículo 22 del DF 225/1998.  
      2h. CÓMPUTO LECTIVO (Reversión horaria. Pacto 2019) Para las labores dispuestas en el apartado “Jornada Laboral” de la presente Resolución.  
      5h. COMPLEMENTARIAS DE CÓMPUTO SEMANAL - Guardias - Departamento - Tutorías - Reuniones de coordinación.
JORNADA ANUAL: 1592 h.  
   30 h. SEMANALES DE DEDICACIÓN DIRECTA AL CENTRO   
      5h. COMPLEMENTARIAS DE CÓMPUTO MENSUAL - Claustro - Sesiones de evaluación y equipo docente - Familias - Otras actividades complementarias y extraescolares - Instrumentos de planificación instituciona - 35 horas de formación  
   RESTO DE HORAS HASTA COMPLETAR LA JORNADA ANUAL Preparación de las actividades docentes y al perfeccionamiento profesional del profesorado, sin perjuicio de la obligación que tiene el profesorado de participar en comisiones y tribunales de selección o actividades análogas, cuando sean designados al efecto.

[RESOLUCIÓN 250/2021, de 15 de junio, del Director General de Educación, por la que se aprueban las instrucciones que van a regular, durante el curso 2021-2022, la organización y el funcionamiento de los centros públicos que imparten las enseñanzas de segundo ciclo de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria y Bachillerato en el ámbito territorial de la Comunidad Foral de Navarra.](https://bon.navarra.es/es/anuncio/-/texto/2021/194/9)  

[RESOLUCIÓN 207/2023, de 2 de junio, del director general de Educación, por la que se aprueban las instrucciones que van a regular, durante el curso 2023-2024, la organización y el funcionamiento de los centros educativos que imparten las enseñanzas de segundo ciclo de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria, Bachillerato y de Educación Especial en el ámbito territorial de la Comunidad Foral de Navarra.](https://bon.navarra.es/es/anuncio/-/texto/2023/133/4)  

Las instrucciones tienen tabla similar CUADRO RESUMEN DE LA JORNADA DEL PROFESORADO (a tiempo completo): SECUNDARIA, no varían.

Veo por tablas que son 18 h, no 18 periodos, de manera similar a lo visto en pdf de Canarias para 2021-2022, pero en Navarra total 25 h, no 24 h como Canarias.

[Solo Navarra y País Vasco mejoran las recomendaciones de horas lectivas de docentes planteadas por el Ejecutivo](https://eldiariodelaeducacion.com/blog/2019/09/04/solo-navarra-y-pais-vasco-mejoran-las-recomendaciones-de-horas-lectivas-de-docentes-planteadas-por-el-ejecutivo/)  
> La Ley 4/2019 aprobada en marzo por el Gobierno de Pedro Sánchez recomienda 18 horas lectivas como máximo en Secundaria y 23 en Primaria.  
Las dos comunidades del norte, Navarra (que empieza hoy las clases) y País Vasco, mejoran las recomendaciones de la ley. Según datos facilitados por el sindicato STEs, la primera comienza el curso con 18 horas en secundaria y 19,5 en primaria. País Vasco serán 17 en secundaria y 23 en primaria.

#### País Vasco

Resumen 2023-2024: 23 horas permanencia, 18 horas lectivas 

[Organización de centros docentes no universitarios. Organización del curso en centros públicos](https://www.euskadi.eus/organizacion-de-centros-docentes-no-universitarios-organizacion-del-curso-en-centros-publicos/web01-a2hikast/es/) Este enlace tiene las últimas instrucciones, no las anteriores

Documento antiguo (pdf de 2011)[CONTESTACIÓN A LA PREGUNTA PARA SU RESPUESTA POR ESCRITO PRESENTADA POR IÑAKI OYARZABAL DE MIGUEL, PARLAMENTARIO DEL GRUPO POPULAR VASCO RELATIVA A LOS HORARIOS DE LOS PROFESORES DE PRIMARIA Y SECUNDARIA.](https://web.archive.org/web/20181221164725/http://www.legebiltzarra.eus/irud/09/00/025333.pdf)  
> 1. ¿Cuáles son los horarios de los profesores de Primaria y Secundaria?
Detállese las horas lectivas y las horas de permanencia en el centro.  
...  
El profesorado de secundaria realizará una jornada
de 30 horas semanales de dedicación directa al centro y **23 de permanencia habitual, de las cuales como promedio 17 serán lectivas**, y con un máximo de 18.  

[ORGANIZACIÓN DEL CURSO 2017-2018 EN LOS INSTITUTOS DE EDUCACIÓN SECUNDARIA RESOLUCIÓN DE LA VICECONSEJERA DE EDUCACIÓN](https://zelaieta.hezkuntza.net/c/document_library/get_file?uuid=c7021a3b-3d37-4ba9-8d68-e72ca0bfc788&groupId=12305)  
> 3.1.3.a.- Profesorado con dedicación ordinaria  
La dedicación ordinaria directa al centro del profesorado será de 30 horas semanales, de las
cuales 25 serán de dedicación docente, que supondrá el contacto directo con el alumnado ...

cita Acuerdo Regulador de las Condiciones Laborales del personal docente funcionario no universitario de la CAPV que me lleva a
[Condiciones de trabajo. Acuerdos y convenios](https://www.euskadi.eus/personal-acuerdos-convenios/web01-a2hlang/es/)
y de ahí a [Acuerdo Regulador de las condiciones de trabajo del personal funcionario docente no universitario de la CAPV (BOPV 16-07-2010)](http://www.euskadi.net/bopv2/datos/2010/07/1003455a.pdf) donde está el texto citado  
> El profesorado de secundaria realizará una jornada
de 30 horas semanales de dedicación directa al centro y **23 de permanencia habitual, de las cuales como promedio 17 serán lectivas**, y con un máximo de 18. 

[Los profesores de la red pública vasca deberán dar más horas lectivas ](http://www.eitb.eus/es/noticias/sociedad/detalle/4150842/los-profesores-red-publica-vasca-deberan-dar-horas-lectivas/)  
> Según la normativa aprobada por el Ejecutivo español en 2012, las horas lectivas de los profesores deben ser, como mínimo, 20 en Secundaria y Bachillerato y de 25 horas en Enseñanza Infantil y Primaria. Sin embargo, Euskadi no aplicó la norma al entender el Gobierno Vasco que dicho decreto invadía sus competencias y mantuvo las 17 horas en Secundaria y Bachillerato y 23 en Infantil y Primaria.  

[Educación aumentará el horario de los docentes de la manera «menos lesiva»](http://www.diariovasco.com/sociedad/201606/15/educacion-aumentara-horario-docentes-20160615001459-v.html)  
> impartir 20 horas lectivas en Secundaria y 25 en Primaria - frente a las 17 y 23 actuales -  

[Pleno. Sentencia 68/2016, de 14 de abril de 2016. Recurso de inconstitucionalidad 382-2013. Interpuesto por el Consejo de Gobierno de la Comunidad Autónoma del País Vasco en relación con los arts. 3 y 4 del Real Decreto-ley 14/2012, de 20 de abril, de medidas urgentes de racionalización del gasto público en el ámbito educativo. Límites a los decretos-leyes; competencias sobre educación, función pública y ordenación general de la economía: constitucionalidad de los preceptos legales estatales relativos a la jornada lectiva y sustitución del personal docente (STC 26/2016). Voto particular.](https://www.boe.es/buscar/doc.php?id=BOE-A-2016-4852)

A falta de información, siendo 23 horas en total, y como dice que se pasa a 20, serían 20 + 3 horas.

[ORGANIZACIÓN DEL CURSO 2019-2020 EN LOS INSTITUTOS DE EDUCACIÓN SECUNDARIA RESOLUCIÓN DE LA VICECONSEJERA DE EDUCACIÓN](https://www.ehige.eus/wp-content/uploads/2019/09/Resolucion-Secundaria-2019-2020.pdf)  
> 3.3.- JORNADA SEMANAL  
a) Profesorado con dedicación ordinaria   
La dedicación ordinaria directa al centro del profesorado será de 30 horas semanales. 23 horas
semanales serán de permanencia habitual en el centro, entre las que se hallan las de dedicación
docente del profesorado, que implican la intervención directa con el alumnado.  
Estas 23 horas engloban las destinadas a la impartición del currículo, las de tutoría con
alumnado, las de refuerzo educativo, las de actividades dedicadas a proyectos con participación del
alumnado contemplados en el PEC y las de atención educativa (guardias y cuidado de recreos).
Además se tendrán en cuenta las dedicadas a la atención a las madres y padres, a otras labores de
tutoría distintas a las de tutoría grupal, a reuniones del departamento didáctico u otras reuniones de
coordinación pedagógica, a actividades de formación, y toda aquella actividad que contribuya al
desarrollo del PEC.  

[ORGANIZACIÓN DEL CURSO 2023-2024 EN LOS INSTITUTOS DE EDUCACIÓN SECUNDARIA](https://www.euskadi.eus/contenidos/informacion/ctros_ee_organizacion_curso_pb/es_def/adjuntos/organizacion_curso_2023_2024_inst_educ_secundaria.pdf)  
> 4.3.1. Profesorado de dedicación ordinaria  
El profesorado tendrá una dedicación directa ordinaria en el centro de 30 horas semanales.  
Al menos 23 horas semanales corresponderán a la estancia habitual en el centro, entre las que se
encuentran:  
Horas de impartición del currículo y horas de refuerzo educativo **(máximo 18 horas semanales)**  

[Solo Navarra y País Vasco mejoran las recomendaciones de horas lectivas de docentes planteadas por el Ejecutivo](https://eldiariodelaeducacion.com/blog/2019/09/04/solo-navarra-y-pais-vasco-mejoran-las-recomendaciones-de-horas-lectivas-de-docentes-planteadas-por-el-ejecutivo/)  
> La Ley 4/2019 aprobada en marzo por el Gobierno de Pedro Sánchez recomienda 18 horas lectivas como máximo en Secundaria y 23 en Primaria.  
Las dos comunidades del norte, Navarra (que empieza hoy las clases) y País Vasco, mejoran las recomendaciones de la ley. Según datos facilitados por el sindicato STEs, la primera comienza el curso con 18 horas en secundaria y 19,5 en primaria. País Vasco serán 17 en secundaria y 23 en primaria.

Cita 17 horas en 2019 pero no lo veo en instruciones. 

#### La Rioja

Resumen 2023-2024: 18 periodos lectivos, 27 en total

[Instrucciones y circulares](https://www.larioja.org/edu-recursos-humanos/es/normativa-recursos-humanos/instrucciones-circulares)  

[Resolución de 28 de junio de 2017, de la Dirección General de Educación por la que se se apruebas las instrucciones por las que se regulan la organización y funcionamiento de los centros docentes públicos situados en la Comunidad Autónoma de La Rioja para el curso académico 2017/2018. (29/06/2017)](http://www.larioja.org/larioja-client/cm/edu-recursos-humanos/images?idMmedia=913605)
que cita
[Orden 2/2003, de 3 de enero, de la Consejería de Educación, Cultura, Juventud y Deportes, por la que se dictan instrucciones sobre el horario del Personal Docente en los Institutos de Educación Secundaria](https://ias1.larioja.org/boletin/boletin/bor_mostrar_anuncio.jsp?referencia=623541-1-HTML-231686-X)  
> Anexo.- Instrucciones sobre diversos aspectos de la organización y funcionamiento de los Institutos de Educación Secundaria.  
Horarios de los Profesores  
...  
> 3. La suma de la duración de los períodos lectivos y las horas complementarias de obligada permanencia en el Centro, recogidas en el horario individual de cada Profesor, será de **25 horas**. Aún cuando los períodos lectivos tengan una duración inferior a sesenta minutos, no se podrá alterar, en ningún caso, el total de horas de dedicación al centro; en el caso de que los períodos lectivos sean de 55 minutos, el número de períodos reflejados en horario individual del profesorado será de **27, entre lectivos y complementarios.**  
...  
A) Profesores de Enseñanza Secundaria y Profesores Técnicos de Formación Profesional.  
>9. Los Profesores de Enseñanza Secundaria y los Profesores Técnicos de Formación Profesional impartirán como **mínimo 18 períodos lectivos semanales**, pudiendo llegar excepcionalmente a 21 cuando la distribución horaria lo exija y siempre dentro del mismo.  

[INSTRUCCIONES DE 25 DE JUNIO DE 2019, DE LA DIRECCIÓN GENERAL DE EDUCACIÓN POR LAS QUE SE REGULA LA ORGANIZACIÓN Y EL FUNCIONAMIENTO DE LOS CENTROS DOCENTES PÚBLICOS SITUADOS EN LA COMUNIDAD AUTÓNOMA DE LA RIOJA PARA EL CURSO ACADÉMICO 2019/2020](https://www.larioja.org/larioja-client/cm/edu-recursos-humanos/images?idMmedia=1137909)

[Instrucciones de 7 de junio de 2021, de la Dirección General de Gestión Educativa, por las que se regula la organización y el funcionamiento de los Centros Docentes Públicos situados en la Comunidad Autónoma de La Rioja para el curso académico 2021/2022](https://web.larioja.org/bor-portada/boranuncio?n=16925233-4-HTML-539371-X)

[Instrucciones de 28 de julio de 2023, de la Dirección General de Gestión Educativa, por las que se regula la organización y el funcionamiento de los centros docentes públicos situados en la Comunidad Autónoma de La Rioja para el curso académico 2023/2024.](https://www.larioja.org/larioja-client/cm/edu-recursos-humanos/images?idMmedia=1498773)  
> Institutos de Ed. Secundaria y asimilados:  
- Reducción de jornada de 1/3: 12 períodos lectivos; excepcionalmente 13.  
- Reducción de jornada de 1/2: 9 períodos lectivos; excepcionalmente 10.  

Luego se puede entender que jornada completa son 18 periodos lectivos 

#### Comunidad Valenciana

Resumen: 18 horas lectivas y 7 horas complementarias

[Inicio > Educación y Formación Profesional > Contenido Educativo > Ordenación y Centros > Ordenación Académica > Educación Secundaria Obligatoria > Normativa > Organización y funcionamiento de centros > Instrucciones de inicio de curso](http://www.ceice.gva.es/es/web/ordenacion-academica/secundaria/normativa/instrucciones-de-inicio-de-curso)

[RESOLUCIÓN de 20 de julio de 2017, de la Secretaría Autonómica de Educación e Investigación, por la que se aprueban las instrucciones para la organización y el funcionamiento de los centros que imparten Educación Secundaria Obligatoria y Bachillerato durante el curso 2017-2018.](https://www.dogv.gva.es/datos/2017/07/25/pdf/2017_6849.pdf)  
> 3. Durante los periodos lectivos establecidos en el calendario escolar vigente, dedicarán a las actividades del centro 30 horas semanales, de las cuales **25 serán recogidas en el horario individual semanal**, y 5 serán horas complementarias computadas mensualmente.  
...  
> 2. Durante los periodos lectivos establecidos en el calendario escolar vigente, el profesorado dedicará a las actividades del centro 30  horas semanales, de las cuales como **mínimo 20 serán lectivas**, y las restantes se distribuirán entre complementarias recogidas en el horario  individual semanal y complementarias computadas mensualmente.  

**17 octubre 2018**

Se comparten imágenes reales de horarios, con grandes variaciones dentro de un mismo centro

[https://twitter.com/xarxatic/status/1052554274142646272](https://twitter.com/xarxatic/status/1052554274142646272)  
En el IES de mi mujer sí que saben hacer horarios en su Departamento. Uno a 14 de clase, otra a 16 y mi mujer a 21. Con el agravante que los dos primeros tienen optativas de bachillerato con menos de 5 alumnos. Nivelazo.  
![](https://pbs.twimg.com/media/DptsfkjX4AErg2i.jpg)  
![](https://pbs.twimg.com/media/DptsgdeX4AAKLiK.jpg)  
![](https://pbs.twimg.com/media/DptshkpWwAAVzqT.jpg)  

**8 mayo 2019**

[Educación plantea mantener 25 horas lectivas para maestros y reducirlas progresivamente a 18 en secundaria](https://www.europapress.es/comunitat-valenciana/noticia-educacion-plantea-mantener-25-horas-lectivas-maestros-reducirlas-progresivamente-18-secundaria-20190508130032.html)

[RESOLUCIÓN de 5 de julio de 2019, del secretario autonómico de Educación y Formación Profesional, por la que se aprueban las instrucciones para la organización y el funcionamiento de los centros que imparten Educación Secundaria Obligatoria y Bachillerato durante el curso 2019-2020.](http://www.dogv.gva.es/datos/2019/07/18/pdf/2019_7221.pdf)  
> 1. La jornada laboral del profesorado es, con carácter general, de 37 horas y 30 minutos semanales. Durante los periodos lectivos establecidos en el calendario escolar vigente, el profesorado dedicará a las actividades del centro 30 horas semanales, de las cuales como **mínimo 19 serán lectivas**, y las sobrantes se distribuirán entre complementarias recogidas en el horario individual semanal y complementarias computadas mensualmente.  

**2 julio 2020**

[ANPE NO FIRMARÁ EL ACUERDO PROPUESTO POR CONSELLERIA](https://anpecomunidadvalenciana.es/notices+anpe+no+firmar%C3%81+el+acuerdo+propuesto+por+conselleria-id=118146)  
> Respecto del horario del profesorado, pretende aplazar al curso 2021‐2022 las 18 horas lectivas del profesorado de Secundaria y otros cuerpos y reducir a 23 horas lectivas las del profesorado de Educación Infantil, Primaria y Educación Especial también para el curso 2021‐2022.

[acord-sindical-20-21_cast--def-2.07--](https://anpecomunidadvalenciana.es/openFile.php?link=notices/att/4/acord-sindical-20-21_cast--def-2.07--_t1593697898_4_1.doc)  
> Cuarto. En el curso 2021-2022, la parte lectiva de la jornada semanal del personal docente de los centros públicos que imparte las enseñanzas reguladas en la Ley Orgánica 2/2006, de 3 de mayo, de Educación, será de 23 horas en educación infantil, primaria y educación especial y de 18 horas en  el resto de enseñanzas reguladas por la Ley Orgánica 2/2006, de 3 de mayo, de Educación.

**6 julio 2020**

[CSIF firma el acuerdo de reducción de jornada lectiva tras eliminar Educación la cláusula que lo vinculaba al informe de Hacienda](https://www.csif.es/contenido/comunidad-valenciana/educacion/300783)

[Educación acepta rebajar las horas lectivas del profesorado para el curso 2021-2022](https://www.informacion.es/alicante/2020/07/06/educacion-acepta-rebajar-horas-lectivas-8725404.amp.html)  
> El cuerpo de maestros realizará 23 horas semanales y el de profesores de instituto, 18

[DECRETO 58/2021, de 30 de abril, del Consell, sobre jornada lectiva del personal docente y número máximo de alumnado por unidad en centros docentes no universitarios [2021/4855]](https://dogv.gva.es/portal/ficha_docv.jsp?L=1&signatura=2021%2F4855)  
> Artículo 2. Jornada lectiva  
> 1. La parte lectiva de la jornada semanal del personal docente que imparte las enseñanzas reguladas en la Ley Orgánica 2/2006, de 3 de mayo, de Educación, en centros públicos, será de 23 horas en educación infantil y primaria. En las restantes enseñanzas la jornada será de **18 horas**, sin perjuicio de las situaciones de reducción de jornada previstas en la normativa vigente.

[RESOLUCIÓN de 27 de junio de 2023, del secretario autonómico de Educación y Formación Profesional, por la que se aprueban las instrucciones para la organización y el funcionamiento de los centros que imparten Educación Secundaria Obligatoria y Bachillerato durante el curso 2023-2024.](https://dogv.gva.es/datos/2023/06/30/pdf/2023_7359.pdf)  
> 2. Conforme al Decreto 58/2021, de 30 de abril, del Consell, sobre
jornada lectiva del personal docente y número máximo de alumnado por
unidad en centros docentes no universitarios (DOGV 9077 06.05.2021),
la parte lectiva de la jornada semanal del personal docente que imparte
las enseñanzas reguladas en la Ley orgánica 2/2006, de 3 de mayo, de
educación, en centros públicos, será de 18 horas en las enseñanzas diferentes de Educación Infantil y Educación Primaria, sin perjuicio de las situaciones de reducción de jornada previstas en la normativa vigente.
En consecuencia, los horarios lectivos del profesorado son de **18 horas lectivas semanales**, pudiéndose establecer un incremento hasta 20 horas lectivas, distribuidas de lunes a viernes, con un mínimo de dos horas
lectivas diarias y un máximo de cinco. 

[Guía de horarios 2023-2024 ANPECV](https://documentos.anpecomunidadvalenciana.es/GUIAS/Guia_horarios_23-24_ANPECV.pdf)
> **18 horas lectivas + 7 horas complementarias** + 5 h complementarias semanales de cómputo mensual  

### Documentos generales de comparativa

Es importante prestar atención a la fecha de cada uno, cada comparativa tiene datos de unas fechas concretas, y a veces combinan datos de primaria. Se incluyen algunos de primaria porque pueden citar normativa autonómica que cite también secundaria.

Suelen esar realizados por sindicatos. 

[Un estudio de FETE‐UGT revela las desigualdades en los horarios laborales  del profesorado de secundaria  en función de la Comunidad  Autónoma en la que trabaje](http://ugt-andalucia.com/documents/12565/0/Com_carga+lectiva.pdf)  
(fecha interna 2011, y el enlace interno a informe completo no funciona)

Estudio similar de primaria, en web Aragón
[ESTUDIO COMPARATIVO DEL HORARIO SEMANAL DEL PROFESORADO DE EDUCACIÓN INFANTIL Y PRIMARIA EN LAS COMUNIDADES AUTÓNOMAS. Abril 2012](http://bases.cortesaragon.es/bases/ndocumenVIII.nsf/e86600a24e054a61c12576d2002e551c/53cc1e320548ff02c1257d80003b3ca4/$FILE/GABprimaria_horario_final_2012_.pdf.pdf)  

Gracias al título localizo  
[ESTUDIO COMPARATIVO DEL HORARIO SEMANAL DEL PROFESORADO DE EDUCACIÓN SECUNDARIA EN LAS COMUNIDADES AUTÓNOMAS](https://docplayer.es/8741982-Estudio-comparativo-del-horario-semanal-del-profesorado-de-educacion-secundaria-en-las-comunidades-autonomas.html)

[Horario del profesorado de Educación Secundaria en las Comunidades Autónomas. Gabinete Técnico Enseñanza. FeSP-UGT. Enero 2018](https://ugt-sp.es/images/PDF/ensenanza/Estudios%20e%20Informes/horas-secundaria-publica.pdf)

[Condiciones laborales del profesorado. Horario lectivo por etapas. (Normativa estatal y autonómica) Comparativa con Europa. UGT octubre 2023](https://ugt-sp.es/wp-content/uploads/Boletin-condiciones-laborales-del-profesorado-2023.pdf)

### Derogación RDL14/2012 y horarios homogéneos entre CCAA 

Está relacionado con post [Derogar RDL14/2012](http://algoquedaquedecir.blogspot.com/2018/08/derogar-rdl142012.html)

Puede haber información y artículos que citen ese cambio asociados a la visión histórica y cronológica de cada comunidad, de la misma manera que puede haber asociados al aumento de horas lectivas en 2012.

Hay que tener claro que tener 18 periodos lectivos no es una mejora, es simplemente revertir una peora que ha durado años, y en algunas ocasiones se intenta negociar no regresar de 20 a 18, sino gradualmente de 20 a 19 y de 19 a 18.

**20 septiembre 2018**

[Consejo Escolar pide homogeneizar horas lectivas de docentes en toda España](https://www.lavanguardia.com/vida/20180920/451931551383/consejo-escolar-pide-homogeneizar-horas-lectivas-de-docentes-en-toda-espana.html)  
> El Consejo Escolar del Estado ha recomendado "una homogeneización" en toda España de la parte lectiva semanal de los docentes, tanto de centros públicos como concertados, para "evitar desigualdades" entre el profesorado.
El máximo órgano consultivo del Gobierno en materia educativa lo señala así en su informe de ponencia al que ha tenido acceso Efe, sobre el borrador del anteproyecto de ley del Ejecutivo para revertir los recortes que fijó el PP en 2012, entre ellos el aumento hasta un 20 % de las horas de clase de los profesores.  
Se trata del documento, preceptivo que no vinculante para el Gobierno, que el 2 de octubre debe aprobar el Pleno del Consejo Escolar del Estado, en el que está representada toda la comunidad educativa.  
El Consejo recuerda en su informe que el Ejecutivo pretende restablecer la situación existente con la Ley Orgánica de Educación (LOE), pero que para ello se debe establecer "un máximo" de 23 periodos lectivos (horas) en los centros de Infantil, Primaria y Especial y "un máximo" de 18 periodos lectivos en los que impartan el resto de enseñanzas de régimen general.  
El departamento que dirige Isabel Celaá únicamente comenta en su anteproyecto que las administraciones "podrán establecer" la parte lectiva de la jornada semanal del personal docente de los centros públicos, sin especificar "un máximo" como sí propone el Consejo.

El tema de homogeneizar entre empleados públicos entre CCAA tiene todo el sentido el mundo.

**30 septiembre 2018**

[Varapalo del Consejo Escolar a la ley de Celaá: censura que las CCAA fijen las horas de clase de los docentes](http://www.elmundo.es/espana/2018/09/30/5bafca6ae2704ea49f8b45fe.html)

**1 octubre 2018**

Se cita cambio horario en entrevista

[“Antes de Navidad podría estar la modificación de la Lomce en la que estamos trabajando”](http://eldiariodelaeducacion.com/blog/2018/10/01/antes-de-navidad-podria-estar-la-modificacion-de-la-lomce-en-la-que-estamos-trabajando/)  
> El Ministerio ha dado ya algunos pasos para cambiar la Lomce, aunque todavía tímidos. Espera poder llegar a Navidad con una propuesta de modificación.  
...  
*Quedan en manos de las comunidades las horas lectivas. Es posible que haya grandes diferencias entre ellas. ¿Por qué el Ministerio deja en manos de las comunidades esta decisión?*  
El Ministerio cree en el Estado de las autonomías y considera que una competencia de auto-organización, como la de atribuir docencia directa al profesorado, es competencia de la comunidad autónoma. Fue el PP el que en 2012 restó esa competencia. Ahora devolvemos una competencia que tenían desde tiempo inmemorial y que llevó a una práctica: el profesor de secundaria tenía 18 horas de docencia directa (de las 37,5 que tienen los funcionarios) y el de primaria, 21. Nosotros pensamos que una vez que la gran mayoría de las comunidades se pongan a esa atribución horario, para aquella que quede residual, será muy dificil mantenerse en otra. En la Conferencia Sectorial de julio, 12 de las 17 CCAA estaban de acuerdo con la medida y las 5 del PP no, que querían recursos. Que les podrían haber venido por los 6.000 millones, pero ellos mismos se han cerrado la puerta.  
*Claro, ¿Quién pagaría el cambio de las horas lectivas?*  
El Ministerio de Hacienda nunca se negó a no mirarlo, estaba en disposición de ayudar a las CCAA que tuvieran problemas. Tampoco hay que hacerlo de la noche a la mañana. Algunas han dibujado itinerarios en el tiempo para poder incorporarse a la medida.

**2 octubre 2018**

[El Consejo Escolar insta a igualar el número de horas de clase de los docentes](https://www.eldiario.es/sociedad/Consejo-Escolar-igualar-numero-docentes_0_820667954.html)  
> El Consejo Escolar aprueba pedir a Gobierno un máximo de horas lectivas docentes  
El Pleno del Consejo Escolar del Estado ha aprobado hoy por mayoría un informe en el que recomienda al Gobierno que establezca un máximo de 23 horas lectivas para los docentes de los centros de Infantil, Primaria y Educación Especial de toda España y de 18 horas para Secundaria.  

[Consejo Escolar aprueba pedir a Gobierno un máximo de horas lectivas docentes](https://www.cope.es/actualidad/sociedad/noticias/consejo-escolar-aprueba-pedir-gobierno-maximo-horas-lectivas-docentes-20181002_265570)  
> El Pleno del Consejo Escolar del Estado ha aprobado hoy por mayoría un informe en el que recomienda al Gobierno que establezca un máximo de 23 horas lectivas para los docentes de los centros de Infantil, Primaria y Educación Especial de toda España y de 18 horas para Secundaria.

[El Pleno del Consejo Escolar del Estado pide al Gobierno "homogeneizar" las horas lectivas del profesorado](http://www.europapress.es/sociedad/educacion-00468/noticia-pleno-consejo-escolar-estado-pide-gobierno-homogeneizar-horas-lectivas-profesorado-20181002145606.html)  
> El organismo estatal aboga asimismo por establecer un máximo de 23 periodos lectivos en los centros de Educación Infantil, Primaria y Especial, así como un máximo de 18 periodos lectivos en los centros que imparten el resto de enseñanzas de régimen general.

[Dictamen 18/2018 Consejo Escolar de Estado](https://www.educacionyfp.gob.es/dam/jcr:0f6737e6-eaa6-4839-a01c-7c905f5537fc/dictamen-18-2018%20(pleno%2002.10.2018).pdf)

Sobre horarios en centros públicos

> 2. Al artículo Único, apartado 2   
Se recomienda una homogeneización en todo el territorio español para evitar desigualdades entre el profesorado.  
Se sugiere completar el apartado 2 de la forma siguiente:  
”Las Administraciones Públicas con competencias educativas podrán establecer en su respectivo ámbito, la parte lectiva de la jornada semanal del personal docente que imparte enseñanzas reguladas en la Ley Orgánica 2/2006, de 3 de mayo, de Educación, en centros públicos, estableciéndose un máximo de 23 periodos lectivos en los centros de Educación Infantil, Primaria y Especial y un máximo de 18 periodos lectivos en los centros que impartan el resto de enseñanzas de régimen general reguladas por la LOE”.

**5 octubre 2018**

[La DGA y los sindicatos negocian la bajada de horas lectivas para los docentes de Secundaria](https://www.heraldo.es/noticias/aragon/2018/10/05/la-dga-los-sindicatos-negocian-bajada-horas-lectivas-para-los-docentes-secundaria-1270150-300.html)  
> El Consejo Escolar ha pedido al Gobierno de España que se unifiquen los tiempos lectivos en 18 horas semanales para Secundaria y 23 en Primaria e Infantil.

Al haber elecciones sindicales se cita el tema del horario

**15 noviembre 2018**

[twitter feccoo/status/1062974954726219776](https://twitter.com/feccoo/status/1062974954726219776)  
En [@feccoo](https://twitter.com/feccoo) lo tenemos claro: de conseguir la regularización básica de la jornada lectiva del profesorado:  
👉 18 horas lectivas en Secundaria, FP y Régimen Especial.  
👉 23 horas lectivas en Infantil y Primaria.

![](https://pbs.twimg.com/media/DsByFXkWsAEVXiZ.jpg)


**3 septiembre 2019**

[La mayoría de las comunidades tendrán este curso un horario lectivo por encima de lo que recomienda la ley](https://amp.europapress.es/sociedad/educacion-00468/noticia-mayoria-comunidades-tendran-curso-horario-lectivo-encima-recomienda-ley-20190903170459.html)  
> El sindicato STEs denuncia que muchas superan las 18 y 23 horas semanales en Primaria y Secundaria, respectivamente

[JUNTA DE PERSONAL DOCENTE NO UNIVERSITARIO  DAT Madrid Sur RESOLUCIÓN Nº  43/2018-19](https://ugtspmadrid.es/wp-content/uploads/2019/06/REVERSION-DE-LOS-RECORTES-CON-RESPECTO-AL-HORARIO-DE-LOS-PROFESORES-PARA-EL-NUEVO-CURSO-ESCOLAR-1.pdf)  
> Reunida la Comisión Permanente de la Junta de Personal Docente no
Universitaria de la DAT Madrid-Sur el día 21 de mayo de 2019, se aprueba la
siguiente resolución, por unanimidad, sobre el asunto que a continuación se
indica:  
...  
Por ello, esta Junta de Personal solicita que sea una realidad las 23 horas lectivas en
Infantil y Primaria, así como las 18 horas en Secundaria, FP y Régimen Especial, como
compromiso con la calidad educativa y la excelencia de la que tanto se habla, con la
dignidad de los maestros y maestras, de los profesores y profesoras, con las
necesidades del alumnado, y en definitiva, con la escuela pública.  
En Leganés, a 21 de mayo de 2019  

**12 septiembre 2019**

[Solo Navarra ha seguido la recomendación de bajar horas lectivas de docentes](https://www.lavanguardia.com/vida/20190912/47311370209/solo-navarra-ha-seguido-la-recomendacion-de-bajar-horas-lectivas-de-docentes.html)

Citan desglose pero sin aportar normativa (lo intento en este post). Y la normativa no refleja todo, como se ve en Ceuta y Melilla: normativa 2019-20 "baja" a 18 al aplicar orden 1994, pero al tiempo deja claro que sin presupuesto se mantiene a 20.

> UGT ha destacado la "paradoja" de que en Ceuta y Melilla "cuyas competencias dependen del Ministerio de Educación, el horario lectivo del profesorado de Secundaria sea de 21 horas, por encima de la propia recomendación" del departamento que dirige la ministra en funciones Isabel Celaá.  
Según sus datos, Aragón, Castilla-la Mancha, Castilla y León, y Madrid y Murcia no han introducido ninguna modificación en el horario lectivo de su profesorado y mantienen 25 y 20 horas lectivas en Primaria y Secundaria, respectivamente.  
Por su parte, Cantabria y Cataluña han reducido las horas lectivas en Primaria 60 minutos hasta las 24 horas y en Secundaria han sido ocho las comunidades que han fijado el horario en 18 horas lectivas: Andalucía, Asturias, Baleares, Canarias, Cantabria, Extremadura, Galicia, Navarra.  
...  
CCAA INFANTIL PRIMARIA SECUNDARIA, FP, ESPECIAL  
Andalucía 25 18 pudiendo llegar a 20 en Secundaria  
Aragón 25 20  
Asturias 25 18 pudiendo llegar a 20 en Secundaria  
I. Baleares 25 18 pudiendo llegar a 20 en Secundaria  
Canarias 25 18  
Cantabria 24 18  
Castilla-La Mancha 25 20  
Castilla y León 25 20  
Cataluña 24 19  
Ceuta 25 21  
Extremadura 25 18  
Galicia 25 18 pudiendo llegar a 20 en Secundaria  
La Rioja 25 19  
Madrid 25 20  
Melilla 25 21  
Murcia 25 20  
Navarra 19,5 18 pudiendo llegar a 20 en Secundaria  
CAV 25 23 (No hay distinción entre lectivas y otras)  
C. Valenciana 25 19  

[El Consejo Escolar del Estado recomienda una "homogeneización" de las horas lectivas de los docentes de toda España](https://www.europapress.es/sociedad/noticia-consejo-escolar-estado-recomienda-homogeneizacion-horas-lectivas-docentes-toda-espana-20181001190644.html)

**15 febrero 2024**

[Madrid, a las puertas de una huelga de tres días que divide a los sindicatos](https://eldiariodelaeducacion.com/2024/02/15/madrid-a-las-puertas-de-una-huelga-de-tres-dias-que-divide-a-los-sindicatos/)
> Hasta la fecha, la propuesta de la Consejería de Educación es que los profesores de secundaria vuelvan a las 18 horas, desde las 20 actuales, en dos cursos seguidos. Es, dicen diversas fuentes, la fórmula que se ha seguido en otras muchas comunidades autónomas. El problema es que Madrid, recuerdan, fue la primera en instaurar el aumento de horas y todo apunta a que será la penúltima en revertir la medida.

**22 febrero 2024**

[Claves de la huelga de profesorado en la Comunidad de Madrid](https://www.elsaltodiario.com/educacion-publica/claves-huelga-educativa-comunidad-madrid)  
Los próximos 27, 28 y 29 de febrero los sindicatos CGT, STEM y CNT convocan una huelga entre el profesorado no universitario para exigir la reducción en las horas lectivas, volviendo al escenario anterior a la crisis económica de 2011. 

**7 marzo 2024**

[La Comunidad de Madrid retira la oferta de bajar las horas lectivas del profesorado y los sindicatos anuncian movilizaciones](https://www.eldiario.es/madrid/comunidad-madrid-retira-oferta-bajar-horas-lectivas-profesorado-sindicatos-anuncian-movilizaciones_1_10989621.html)

### Derogación RDL14/2012 y horarios homogéneos entre entre públicos y privados con concierto financiados con dinero público

Surge al tiempo en artículo prensa 20 septiembre 2018 al hablar de ponencia 2 octubre 2012  "tanto de centros públicos como concertados"  tiene más que decir.

Lo primero, [no existe ningún centro que sea concertado](http://algoquedaquedecir.blogspot.com/2018/02/no-existe-ningun-centro-que-sea-concertado.html), estamos hablando de centros privados que tienen concierto y reciben dinero público.

Como empresas privadas que son, tienen sus propios criterios de selección y su propia regulación.

Por ejemplo

[Resolución de 30 de julio de 2013, de la Dirección General de Empleo, por la que se registra y publica el VI Convenio colectivo de empresas de enseñanza privada sostenidas total o parcialmente con fondos públicos.](https://www.boe.es/buscar/doc.php?id=BOE-A-2013-9028)  > Artículo 26. Jornada del personal docente.  
Salvo para el personal docente del Primer Ciclo de Educación Infantil que se estará a lo dispuesto en el artículo siguiente, la jornada anual total será de 1.180 horas, de las cuales se dedicarán a actividad lectiva, como máximo, 850 horas, dedicándose el resto a actividades no lectivas.  
El personal interno realizará 40 horas más de jornada anual.  
El tiempo de trabajo dedicado a actividades lectivas, como máximo, será de 25 horas semanales, que se distribuirán de lunes a viernes.

**26 septiembre 2018**

[FSIE DEMANDA QUE LA CARGA LECTIVA DEL PROFESORADO SEA IGUAL EN TODOS LOS CENTROS SOSTENIDOS CON FONDOS PÚBLICOS](https://fsie.es/actualidad/2223-fsie-demanda-que-la-carga-lectiva-del-profesorado-sea-igual-en-todos-los-centros-sostenidos-con-fondos-publicos)  
>El Ministerio de Educación ha presentado el “Anteproyecto de Ley de mejora de las condiciones para el desempeño de la docencia y de la enseñanza en el ámbito de la educación no universitaria”.  
> En el texto, que deroga parte de las reformas introducidas por el “RD 14/2012 de medidas urgentes de racionalización del gasto público en el ámbito educativo”, se contempla que las administraciones públicas puedan reducir la carga lectiva del profesorado pero solo de centros públicos.  
Como ya estableciera el Tribunal Constitucional en su Sentencia del Pleno 26/2016, de fecha 18 de febrero de 2016, la carga lectiva del profesorado de los centros públicos y privados concertados es competencia de la Administración y, al tratarse de centros sostenidos con fondos públicos, debe tener un tratamiento homogéneo.  
FSIE ha denunciado lo que es una clara exclusión y discriminación de los docentes de la enseñanza concertada. Por ello, hemos presentado una enmienda en el Consejo Escolar del Estado para que las administraciones públicas adopten las medidas necesarias que, junto a la negociación colectiva, permitan que la carga lectiva de los docentes de la concertada sea análoga a los docentes de la pública.  
También nos hemos dirigido a la Ministra de Educación y a los grupos parlamentarios de las Cortes para que nuestra propuesta se incorpore en el texto definitivo de la Ley.  
Desde el sindicato defendemos y exigimos que la equiparación con nuestros homólogos de la enseñanza pública no solo ha de ser en las retribuciones salariales sino también en la carga lectiva.  
Somos profesionales y trabajadores que prestamos un servicio de interés público de gran calidad y nos merecemos el mismo reconocimiento y tratamiento.  
FSIE es nuestra fuerza para defender y mejorar tu futuro y el de todos. Cuantos más compañeros nos ayuden y apoyen más objetivos conseguiremos.

**2 octubre 2018**

[Dictamen 18/2018 Consejo Escolar de Estado](https://www.educacionyfp.gob.es/dam/jcr:0f6737e6-eaa6-4839-a01c-7c905f5537fc/dictamen-18-2018%20(pleno%2002.10.2018).pdf)

Sobre horarios en privados con concierto se comenta que depende de una negociación colectiva en centros privados

> Con independencia de lo indicado en la observación anterior, se propone añadir, asimismo, el texto siguiente en el apartado 2 del artículo Único:  
“[…] Las Administraciones Públicas impulsarán y adoptarán las medidas necesarias para*  *que, junto con la negociación colectiva, se posibilite que la carga lectiva semanal de los*  *docentes sea análoga en los centros sostenidos con fondos públicos.” 

**11 diciembre 2018**

[UGT exige homologar condiciones laborales de educación pública y concertada](https://www.lavanguardia.com/vida/20181211/453510910580/ugt-exige-homologar-condiciones-laborales-de-educacion-publica-y-concertada.html)  
> La Unión General de Trabajadores (UGT) ha exigido al Gobierno que elabore un nuevo real decreto que regule los conciertos educativos, para que se homologuen las condiciones laborales de la educación pública y la concertada.  
Así lo ha demandado hoy en rueda de prensa la secretaria del Sector de Enseñanza de UGT, Maribel Loranca, y el responsable del Sindicato de la Privada, Jesús Gualix, después de reunirse este lunes con el secretario de Estado de Educación, Alejandro Tiana, para explicarle la necesidad de que se negocie un nuevo real decreto.

## Comparación gráfica de horario en distintas CCAA

Inicialmente (cuando escribo por primera vez este post enero 2018) lo hago para curso 2017-2018. Lo pongo como imagen

En 2019 actualizo para curso 2019-2020 con cambios: tras Ley 4/2019 o antes en algunas comunidades como Andalucía, Canarias, Extremadura, Valencia, Asturias ... bajan de 20 periodos lectivos.

Incluyo la hoja de cálculo con los datos, encantado de recibir correcciones con enlace a normativa o ejemplos de horarios; en post cito la normativa en la que se basan los datos de la gráfica.  
[Horarios Docentes Secundaria .ods](https://drive.google.com/open?id=1cAGdI5lb_OnksvJCUa9D3QAeNmeaLwKm)

5º (agosto 2024) Gráfica para curso 2023-2024.

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/2024-08-31-HorariosSecundaria.png)

Revisadas erratas localizadas al comenzar a mirar curso 2024-2025: en Galicia son 30 periodos de 50 min en total y no 23 horas, en Murcia son 19 lectivas y no 20

4º (febrero 2024) Gráfica para curso 2023-2024.

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/2024-02-24-HorariosSecundaria.png)

3º (septiembre 2019) Gráfica para curso 2019-2020. Incluyo fecha de revisión y enlace al post (entonces en blogger).  

![](https://1.bp.blogspot.com/-bLh6b_yHzYg/XW0s7MZrYvI/AAAAAAAAPro/76oE0Jm4Kdc57Miid-g_4olJzxE2Bl4hACLcBGAs/s1600/2019-09-02-HorariosDocentesSecundariaMadridvsCCAA.png)

2º. (Enero 2018) Gráfica diferenciando lectivos y complementarios; la suma lectivos y complementarios es igual a los periodos de obligada permanencia en centro de cómputo semanal en horario semanal.

![](https://4.bp.blogspot.com/-W-a4_OcwLyE/WmvCgd4OomI/AAAAAAAAOfE/rRP7uzjlZkQv8R4LxXJGYw7gIb3D5R7KwCLcBGAs/s1600/2018-01-26-HorariosMadridvsCCAA.png)

1º (primera versión que hice, enero 2018) Periodos totales  
-Convirtiendo todo a periodos de 55 minutos para comparar y redondeando a periodos enteros  
-Truncando deliberadamente el eje horizontal (empieza en 22 y no en 0, lo comento más adelante)  

![](https://3.bp.blogspot.com/-Bd9nVjvy_MM/WmfIN3DorbI/AAAAAAAAOdM/LEpQ1r9uFjQnuMtaeUjyrQlwkLhcg5YbQCPcBGAYYCw/s1600/2018-01-23-HorariosMadridvsCCAA.jpg)

### Comentarios en redes sociales a este post y las gráficas

Como pongo en mi post de presentación, no pretendo tener razón, sino informar e informarme, y siempre agradezco comentarios. 
Me gustaría que comentara la administración, pero los primeros no son de ellos

#### Gráfica tiene el eje truncado

[Twitter es así](https://twitter.com/mlalanda/status/901050267059269632), al poco de compartir la gráfica se comenta eso
[https://twitter.com/I027614/status/956066334093332484](https://twitter.com/I027614/status/956066334093332484)  
>Esas gráficas... 😂  
Q te estorba? El corte del eje?  
Sí, algo que es habitualmente objeto de crítica por estos lares. 😂

Respondo en ese mismo hilo

> el objetivo es mostrar la diferencia. Sin truncar eje 18 vs 20 y 27 vs 30 parecen irrelevantes, pero no lo son. Mi siguiente objetivo es comparar los periodos lectivos, ver qué CCAA están a 18 ó a 20; haré otra gráfica y se podría hacer una de complementarias sin truncar eje.  
Con los ejes no truncados no se vería relevante, pero pasar de 18 a 20 supone:  
-Un 10% menos de docentes: 9 asumen lo que antes hacían 10.  
-840 M€ anuales desde 2012, dinero que sale de sobrecargar a docentes por no contratar otros  

Y cito [twitter FiQuiPedia/status/873326864793821184](https://twitter.com/FiQuiPedia/status/873326864793821184)  
Memoria RDL 14/2012, 5 años sin cambios salvo art4 casos sustitución y art6 TC (art2 ratios depende tasa reposición)
[MAIN RDL 14/2012](https://drive.google.com/file/d/0B-t5SY0w2S8iYmh5aVVxOC1BUEU/view)

![](https://pbs.twimg.com/media/DB6seYxXUAEAIl1.jpg)

**Edito 26 enero 2018** : dejo la gráfica truncada con los periodos de permanencia totales y la nueva gráfica con lectivos + complementarios la hago ya sin truncar.

#### Las horas y los periodos no cuadran

Indirectamente me llegan comentarios

"está mal porque las horas/los periodos en (ponga aquí la CCAA que usted quiera distinta de Madrid) son de x minutos y no de 55 minutos"

Dos cosas:

1. Antes de establecer un diálogo, hay que fijar el vocabulario o nos perdemos antes de empezar. Recomiendo leer el post [Horarios docentes secundaria](http://algoquedaquedecir.blogspot.com/2017/09/horarios-docentes-secundaria.html)
donde digo  
> Un tema inicial que creo que considero esencial aclarar es el concepto de hora. Puede parecer que estoy bromeando, pero en jerga educativa una hora no son necesariamente 60 minutos.

2. Para comparar periodos en CCAA que tienen duraciones distintas, hay que fijar un valor.  
En la gráfica inicial indico explícitamente "periodos ...(55 minutos)": si quiero comparar periodos pero la normativa permite que sean de valores distintos (artículo 67 orden 1994 horarios fija como mínimo 50 minutos + 5 minutos cambio clases, pero es un mínimo y pueden ser otros), debo buscar un criterio para comparar. Comparar unificando tiempo hace que no se visualice el número de periodos real dado, que condiciona el número de alumnos que puede llegar a tener un docente, y comparar utilizando número de periodos sin contar su duración haría que no se visualizase el tiempo total. 

**Edito 26 enero 2018** : tras convertir usando los 55 minutos por periodos los casos en los que se definen horarios con horas de 60 minutos, dejo los decimales; antes redondeaba a periodos completos (es la realidad).

#### Periodos lectivos y complementarios

Los periodos lectivos son "efectivos/de docencia directa", porque mientras RDL 14/2012 esté vigente, son 20 periodos lectivos para todos, solamente que algunas CCAA se inventan/ redefinen qué cosas son lectivas. Enlaza con tema normativa básica si la definición de qué es lectivo y qué no (en orden BOE 1994) se puede cambiar por CCAA vía ley o vía simplemente unas instrucciones (como hace Madrid en 2017)

Considero número de lectivos de manera general; debo poner un único valor y no puedo estar reflejando excepciones. Puede haber variantes como reducción de lectivos por jefatura departamento, ser tutor (en Madrid en 2017-2018 solamente los tutores de 1º, 2º y 3º ESO tienen una reducción de 1 periodo, pero no resto de tutores), ... y aumentos "excepcionales" (es habitual que sea posible llegar a 21 periodos en Madrid en 2017-2018 aunque de manera general se esté a 20)


