+++
author = "Enrique García"
title = "Ratios en educación: normativa"
date = "2018-12-18"
tags = [
    "educación", "normativa", "Post migrado desde blogger", "ratio", "covid19"
]
toc = true

description = "Ratios en educación: normativa"
thumbnail = "https://1.bp.blogspot.com/-RbtgT8l8G8M/XDBu_67Td0I/AAAAAAAAPZI/G1yj_mJJoEEmogBjgJpAfAH-ONY4EGKNACLcBGAs/s640/2000-RatiosAcnee-Aragon.jpg"
images = ["https://1.bp.blogspot.com/-RbtgT8l8G8M/XDBu_67Td0I/AAAAAAAAPZI/G1yj_mJJoEEmogBjgJpAfAH-ONY4EGKNACLcBGAs/s640/2000-RatiosAcnee-Aragon.jpg"]

# Otra opción imagen, pero es más historia que normativa https://philadelphiaencyclopedia.org/wp-content/uploads/2013/07/StDonato-575x341.jpg
# Otra opción imagen, se cita en post https://web.archive.org/web/20200908101237im_/https://pbs.twimg.com/media/EhYpbQZXYAE_93s.jpg
# Inicialmente tenía como imagen post la captura de normativa de Aragón
+++

Revisado 26 octubre 2024

## Resumen
 
Las ratios en educación son importantes y considero que son poco entendidas y cumplidas. Se intenta documentar aquí solo normativa. Espero que permita identificar el incumplimiento, anime a denunciar y a reclamar su incumplimiento.  
Este post trata exclusivamente normativa, lo creo separado de un post general [Ratios en educación](https://algoquedaquedecir.blogspot.com/2017/09/ratios-en-educacion.html) por descargarlo debido a su volumen.

**Este es un resumen de los valores máximos de número de alumnos por aula en principales etapas educativas según normativa en el inicio de curso 2017/18, 2018/19 y 2019/20, 2020/21 (*excepción por covid*), 2021/2022, 2022/2023 y 2023/2024. Si alguien dice otra cosa no es correcto (salvo excepciones como FPB, Diversificación ...) o que alguna administración las haya modificado a la baja**

|**Primaria**|**25**|
|:-:|:-:|
|**Secundaria**|**30**|
|**Bachillerato**|**35**|

El aumento de 10% de esos valores es para escolarización extraordinaria según artículo 87 LOE modificada por LOMCE y LOMLOE. En secundaria el curso debe empezar a 30 y puede llegar a 33, nunca superando ese valor por aula.

Posts relacionados:

* [Derogar RDL14/2012](http://algoquedaquedecir.blogspot.com/2018/08/derogar-rdl142012.html) (asociado a iniciativa legislativa 2018 para derogar entre otros artículo 2 asociado que supuso en 2012 aumento 20% ratios). En marzo 2019 se deroga artículo 2 de RDL14/2012 relativo a ratios.
* [Posible bajada de ratios](http://algoquedaquedecir.blogspot.com/2018/07/posible-bajada-de-ratios.html) (asociado a planteamiento de si bajarán y por qué. En breve pienso que lo harán cuando la privada con concierto lo necesiten, y se comenta iniciativa de 2018 que llegó al Congreso de bajar ratios a 20 en primaria y ESO que no fue aprobada)
* [Lo básico sobre normativa básica](https://algoquedaquedecir.blogspot.com/2019/01/lo-basico-de-normativa-basica.html) (asociado a que la normativa sobre ratios es básica, y no puede modificarse por las CCAA) 
* [Ratios en educación: estadísticas y realidad](https://algoquedaquedecir.blogspot.com/2019/04/ratios-en-educacion-estadisticas-realidad.html) (post centrado en datos y en incumplimiento documentado de la normativa)
* [Ratios en educación concertada](https://algoquedaquedecir.blogspot.com/2020/01/ratios-en-educacion-concertada.html)
* [Ratios ilegales en educación en Castilla-La Mancha](http://algoquedaquedecir.blogspot.com/2017/11/ratios-en-educacion-ilegales-en.html)

## Detalle

### **Número de alumnos por aula: normativa** 

Es importante tener claro que en este post se habla de **número de alumnos _máximos y mínimos_ por grupo fijados en normativa**, no de valores medios de ratios por grupo o por profesor, que es algo que tiene post separado [Ratios en educación: estadísticas y realidad](https://algoquedaquedecir.blogspot.com/2019/04/ratios-en-educacion-estadisticas-realidad.html) 

Este post es voluminoso porque se intenta agrupar toda la normativa relacionada a ratios, y eso incluye la estatal y las de las CCAA, además de la asociada a ciertas etapas educativas y a ciertas situaciones como COVID-19.

Intento acortar los títulos ahorrando la palabra normativa que se asume siempre.

Hay que tener en cuenta la diferencia entre normativa de número de alumnos máximo por grupo y regulación que indica vacantes ofertadas en en el proceso de admisión. Si en el proceso de admisión se limita el número pero no se limita a la hora de formar grupos, no es una limitación real. 

### Tabla resumen normativa 

El número de alumnos en el aula tiene que tener un máximo: llega un momento en el que un docente no puede atender correctamente a todos los alumnos, o en el que los alumnos no pueden estar en un aula.  
*(Otros conceptos de ratio se tratan por separado y no tienen normativa: por ejemplo no hay límite en el número de alumnos que puede tener un docente en el mismo curso: si está en secundaria y tiene una materia de 2 sesiones semanales y un horario con 20 sesiones, impartirá 10 grupos y supondrá unos 300 alumnos, pero eso no está regulado)*  
Estos máximos los fija normativa, se intenta resumir y dar una visión histórica poniendo enlaces: por abreviar no se pone nombre de norma en cada enlace, pero se puede consultar pinchando en él.  
Hay que tener presente que LOMCE y LOMLOE son una modificación masiva de LOE, así que a veces se habla de LOE haciendo referencia a LOMCE (LOE modificada por LOMCE) o haciendo referencia a LOMLOE (LOE modificada por LOMLOE): para consultar a efectos prácticos la LOMLOE en BOE se consulta la LOE consolidada (para consultar LOMCE ya derogada hay que consultar la versión LOE de una fecha concreta).  
En 2019 empiezo a poner nombres CCAA para indicar valores que varían según CCAA. También puede haber regulación sobre ratios en instrucciones, que son anuales.  

| |**1970-1990 <br> LGE**|**1990-2006 <br> LOGSE** |**2006-actualidad <br> "LOE"**|
|:-:|:-:|:-:|:-:|
|Normativa general | [LGE](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24172), [Art 56](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1970-852) <br> [LOECE](https://www.boe.es/buscar/doc.php?id=BOE-A-1980-13661), [Art 19.h](https://www.boe.es/buscar/doc.php?id=BOE-A-1980-13661) <br> [LODE](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978), [Art 14](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978#acatorce) | [LOGSE](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24172), [Art 34.4](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1990-24172) <br> [LOCE](https://www.boe.es/buscar/doc.php?id=BOE-A-2002-25037) (anulada) |  [LOE/LOMCE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899) <br> [RD 1631/2006](https://www.boe.es/buscar/act.php?id=BOE-A-2007-238) <br> [Art 13 Diver](https://www.boe.es/buscar/act.php?id=BOE-A-2007-238#a13), [Art 14 PCPI](https://www.boe.es/buscar/act.php?id=BOE-A-2007-238#a14) <br> (2006-actualidad)  <br>  - _[RDL 14/2012](http://boe.es/buscar/act.php?id=BOE-A-2012-5337)_ (2012-2019)<br> _(+20%  **efecto condicionado a tasa reposición, [derogado por Ley 4/2019](https://boe.es/buscar/act.php?id=BOE-A-2019-3307)** )_  |
| Norma específica estatal / autonómica | - [Orden 1978](https://www.boe.es/buscar/doc.php?id=BOE-A-1978-14054) <br> - [Orden 14 junio 1983](https://www.boe.es/buscar/doc.php?id=BOE-A-1983-17045) | - [RD 1004/1991](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419) <br> - [RD 1537/2003](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599) | - [Orden 4265/2007](http://www.bocm.es/boletin/CM_Boletin_BOCM/20070821_B/19800.pdf) <br> - [Orden 1797/2008](http://www.madrid.org/dat_capital/loe/pdf/Orden_PCPI_08.pdf) <br> - [RD 132/2010](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132) <br> - [RD 1147/2011](https://www.boe.es/buscar/act.php?id=BOE-A-2011-13118) <br> - [RD 127/2014](https://www.boe.es/buscar/act.php?id=BOE-A-2014-2360) <br> - [Decreto 107/2014 Madrid](https://gestiona.comunidad.madrid/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=8679&eli=true#no-back-button) |
| Infantil (1er ciclo 0-3) | Sin máximo. <br> ([Art 2 cita y permite 40](https://www.boe.es/buscar/doc.php?id=BOE-A-1978-14054)) | 8 (0 a 1, [Art 13](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 13 (1 a 2, [Art 13](https://https//www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 20 (2 a 3, [Art 13](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) | |
|Infantil (2º ciclo 3-6) | Sin máximo. <br> ([Art 2 cita y permite 40](https://www.boe.es/buscar/doc.php?id=BOE-A-1978-14054)) | 25 (3 a 6, [Art 13](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 25 ([Art 10](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599)) | 25 ([Art 7](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a7)) <br> _30_ ([Art 2](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2))
| Primaria Sin máximo. <br> [Caso documentado de 53](https://web.archive.org/web/20180214074043/http://ricardosanzytur.es/blog/index.php/numero-maximo-de-alumnos-por-aula-junio-de-2012/) | 25 ([Disp.adic 3](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24172)) <br> 25 ([Art 21](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 25 ([Art 14](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599)) | 25 ([Art 11](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a11), [Art 157](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a157)) <br> _30_ ([Art 2](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2))|
|Secundaria | Sin máximo. | 30 ([Disp.adic 3](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24172)) <br> 30 ([Art 27](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 30 ([Art 20](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599)) <br> 30 ([Art 16](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a16), [Art 157](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a157)) <br> _36_ ([Art 2](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2)) |
| Diversificación/PMAR | 15 ([Art 5 Madrid](http://www.bocm.es/boletin/CM_Boletin_BOCM/20070821_B/19800.pdf)) <br> 15 ([Art 4 ORDEN 3295/2016 Madrid](https://gestiona.comunidad.madrid/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=9537&eli=true#no-back-button)) | 18 ([Art 2](http://boe.es/buscar/act.php?id=BOE-A-2012-5337#a2)) | |
| Bachillerato | Sin máximo. | 35 ([Art 27](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 35 ([Art 20](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599)) | 35 ([Art 16](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a16)) <br> 35 ([Art 37 ORDEN 2582/2016 Madrid](https://gestiona.comunidad.madrid/wleg_pub/secure/normativas/listadoNormativas.jsf?opcion=VerHtml&nmnorma=9476&cdestado=P&redir=false#no-back-button)) <br> 32 ([Art 2.1 Orden ECD/97/2015 Cantabria](https://boc.cantabria.es/boces/verAnuncioAction.do?idAnuBlob=290638)) <br> _42_ ([Art 2](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2))|
|GS/PCPI/FPB | 20 ([Art 9 Orden 1797/2008 Madrid](https://www.bocm.es/boletin/bocm-20080424-97)) <br> 30 ([Art 22](https://www.boe.es/buscar/act.php?id=BOE-A-2014-2360#a22)) <br> 30 ([Art 9 Decreto 107/2014 Madrid](https://gestiona.comunidad.madrid/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=8679&eli=true#no-back-button)) <br> 20 ([Art 6 ORDEN 1409/2015 Madrid](https://gestiona.comunidad.madrid/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=8954&eli=true#no-back-button)) <br> 25 ([3.3.1 Ceuta](http://www.educacionyfp.gob.es/dam/jcr:9b37aaf8-97b8-4cf3-9f1b-09ef0f4b2b5f/instrucciones-curso-19-20-ceuta-melilla.pdf)) <br> 16 ([Art 8 Navarra](http://www.lexnavarra.navarra.es/detalle.asp?r=37553)) | 36 ([Art 2](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2)) | |
|FP GM | Sin máximo. | 30 ([Art 35](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 30 ([Art 27](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599)) | 30 ([Art 46](https://www.boe.es/buscar/act.php?id=BOE-A-2011-13118#a46)) <br> _36_ ([Art 2](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2)) |
|FP GS | Sin máximo. | 30 ([Art 41](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419)) <br> 30 ([Art 27](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599)) | 30 ([Art 46](https://www.boe.es/buscar/act.php?id=BOE-A-2011-13118#a46)) <br> 36 ([Art 2](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2)) |
| Educación especial | Ver [Art 8](https://www.boe.es/buscar/doc.php?id=BOE-A-1983-17045) | Ver [Disp.adic 3](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419) | |

Como hasta LODE 1985 no había máximo, se pueden ver casos puntuales y estadísticas (se comentan por separado estadísticas con evolución, y también la historia comentando la frase "pues éramos 40 y no pasaba nada" en otros posts relacionados sobre ratio).

### + 10% por incorporación tardía
Hay que tener en cuenta que LOMCE y LOMLOE lo que hacen es modificar la LOE, donde aparece un tema importante ligado al valor máximo
[Artículo 87. Equilibrio en la admisión de alumnos.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a87)  
> Asimismo, podrán autorizar un **incremento de hasta un diez por ciento del número máximo de alumnos y alumnas por aula** en los centros públicos y privados concertados de una misma área de escolarización, bien para atender necesidades inmediatas de escolarización del **alumnado de incorporación tardía**, bien por necesidades que vengan motivadas por traslado de la unidad familiar en período de escolarización extraordinaria, debido a la movilidad forzosa de cualquiera de los padres, madres o tutores legales, o debido al inicio de una medida de acogimiento familiar en el alumno o la alumna.

Es decir, ese 10 % está asociado a una escolarización extraordinaria de alumnos ya iniciado el curso, por lo que no se indica en la tabla.

Nota: el concepto de "alumnado de incorporación tardía" se entiende cualitativamente en base a lo que se indica en ese artículo, sin que haya una definición oficial en normativa, que sí cita "integración tardía" en lugar de "incorporación tardía".

La realidad es que con la eliminación de la convocatoria extraordinaria en septiembre cuando antes se asociaba tardía a septiembre, ahora parece de facto que tardía se asocia a después de terminado el proceso de admisión en mayo, por lo que las administraciones realmente en junio y julio planifican las ratios máximos contabilizando ese 10% desde el inicio del curso real. 

[Integración tardía en el sistema educativo español - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/integracion-tardia-sistema-educativo-espanol)  
> El Decreto 23/2023, de 22 de marzo, por el que se regula la atención educativa a las diferencias individuales del alumnado de la Comunidad de Madrid, establece, en su artículo 17, que el alumnado con integración tardía se identifica con el que se escolariza en la enseñanza básica del sistema educativo español con posterioridad a la fecha de inicio del curso académico en el que debe comenzar la etapa de Educación Primaria.

A nivel del ministerio se asocia a posibles necesidades especiales para integración tardía en el sistema educativo español, enlazando con idioma (aulas de enlace) y con nivel curricular

[Anuario estadístico. Las cifras de la educación en España](https://www.educacionyfp.gob.es/servicios-al-ciudadano/estadisticas/indicadores/cifras-educacion-espana.html)  
[D6. El alumnado con necesidades específicas de apoyo educativo. Las cifras de la educación en España. Curso 2021-2022 (Edición 2024)](https://www.educacionyfp.gob.es/dam/jcr:6923c645-9877-4036-8243-4589702d58bf/d6.pdf)  
> Alumnado con integración tardía en el sistema educativo español. El procedente de otros países
que en el curso escolar de referencia se ha incorporado a las enseñanzas obligatorias del sistema
educativo español y se encuentra en, al menos, una de las siguientes situaciones: a) está
escolarizado en uno o dos cursos inferiores al que le corresponde por su edad; b) recibe atención
educativa específica transitoria dirigida a facilitar su inclusión escolar, la recuperación del desfase
curricular detectado, o el dominio de la lengua vehicular del proceso de enseñanza  

[Alumnado con necesidad específica de apoyo educativo](https://educagob.educacionyfp.gob.es/equidad/alumnado-neae.html)  
> Alumnado con integración tardía en el sistema educativo español  
Corresponde a las Administraciones públicas favorecer la incorporación al sistema educativo de los alumnos que, por proceder de otros países o por cualquier otro motivo, se incorporen de forma tardía al sistema educativo español.  
Dicha incorporación se garantizará, en todo caso, en la edad de escolarización obligatoria, atendiendo a sus circunstancias, conocimientos, edad e historial académico, de modo que se pueda incorporar al curso más adecuado a sus características y conocimientos previos, con los apoyos oportunos, y de esta forma continuar con aprovechamiento su educación.

### + 20 % máximo [RDL14/2012](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337)

Se puede ver que el cambio más relevante ha sido el [Real Decreto-Ley 14/2012](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337) que incrementó de golpe todos los valores máximos en un 20 %, aparte del 10 % extraordinario. Cita explícitamente artículo 147.1 LOE (luego modificada por LOMCE) pero al tiempo dice que aplica a todas la enseñanzas de la LOE, que vienen fijadas en [artículo 3 LOE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a3).

En este caso el cambio sí fue condicionado a la tasa de reposición, por lo que sí es temporal, al depender tasa reposición (aunque llama la atención que resto de medidas de mismo Real Decreto-Ley 14/2012 no son condicionadas, pero esto es para otros posts)

Aunque estaba en vigor, no aplicaba ya en curso 2017-2018 ni 2018-2019. En marzo 2019 se deroga ese aumento del 20% con [Ley 4/2019](https://boe.es/buscar/act.php?id=BOE-A-2019-3307)

Si miramos [Artículo 2. Ratios de alumnos por aula.](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337&b=5&tn=1&p=20120421#a2)  
> .. la Ley de Presupuestos Generales del Estado ... establezca, con carácter básico, una tasa de reposición de efectivos inferior al 50 por 100 ...  

así que hay que mirar la Ley de Presupuestos Generales.

Ley 3/2017, de 27 de junio, de Presupuestos Generales del Estado para el año 2017: [Artículo 19](https://www.boe.es/buscar/act.php?id=BOE-A-2017-7387#a1-11) fija un 100 % en educación, pero de manera general, "con carácter básico" como dice Real Decreto-Ley 14/2012 fija un 50%, como indica el [capítulo IV del preámbulo](https://www.boe.es/buscar/act.php?id=BOE-A-2017-7387#pr) se establece una tasa de reposición del 50 por ciento, con carácter general  
Al ser un 50% no inferior al 50 %, implica que el incremento del 20 % de artículo 2 Real Decreto-Ley 14/2012 no aplica en 2017 y por lo tanto no en curso 2017-2018.  
Se puede ver el análisis de [Tasa de reposición sube al 50% en 2015 / 100% en 2016. Mayor número de plazas en oposiciones, y posible efecto en ratios](http://www.docentesconeducacion.es/viewtopic.php?f=134&t=2902) para 2015 y 2016 también debería haber ocurrido, pero como la ley de presupuestos es para año normal y no para curso escolar, se mezclan cosas y en 2015 no se aplicó.  

[twitter FiQuiPedia/status/634357572699602944](https://twitter.com/FiQuiPedia/status/634357572699602944)  
\#PPopulistaEs volver a ratios previas a 2012, venderlo como mérito presupuesto 2016, cuando su ley les obliga en 2015
![](https://pbs.twimg.com/media/CM2w2avUwAA0x2x?format=jpg)

En RDL14/2012 se indicó, con carácter básico "PODRÁN ampliar hasta un 20% el ratio máxima por aula": lo hicieron casi todas las CCAA  
(tema separado es que aunque en Ley 4/2019 se indicó "PODRÁN establecer a parte lectiva de la jornada semanal ... recomendándose ... un máximo de 23 horas ... y un máximo de 18 horas", no lo ha hecho casi ninguna comunidad)  
Algunas CCAA no subieron ese 20% las ratios máximas que permitía RDL14/2012: 

**24 agosto 2015**

[Las aulas de Castilla y León siguen lejos del nuevo límite de alumnos del Ministerio](https://www.diariodeburgos.es/noticia/z9af1cd27-a43a-f226-553f9b079a0b1c0c/201508/las-aulas-de-castilla-y-leon-siguen-lejos-del-nuevo-limite-de-alumnos-del-ministerio)  
> La rebaja el número máximo de niños en cada clase no afectará en la Comunidad, donde el problema es que cuesta llegar al mínimo  
...  
Desde la Consejería de Educación, no obstante, recuerdan que Castilla y León no se acogió a la decisión de poder ampliar el número máximo de alumnos en 2012, que ahora el Ministerio decidió rebajar y en ningún caso se permitió superar los límites que ahora vuelven a estar vigentes en toda España.

Es un tema que enlaza con entornos rurales y ratios de CRA

### Máximo y mínimo estatal vs CCAA

Como se vio con RDL14/2012 hay que tener en cuenta que lo que se fija a nivel estatal son **valores máximos y son normativa básica** (ver post [Lo básico de normativa básica](https://algoquedaquedecir.blogspot.com/2019/01/lo-basico-de-normativa-basica.html)): puede haber variaciones  
* entre CCAA, porque pueden fijar valores inferiores o límites inferiores, en normativa autonómica o a nivel de órdenes o instrucciones.
* entre etapas/materias/tipos de agrupaciones, porque hay ciertas situaciones donde según las características del grupo también puede variar. 

Antes de que existiera un máximo por normativa los valores eran muy altos (ver en estadística que para 1971 el valor medio en primaria era superior a 30, cuando en 2015 el valor medio es 13), y la tendencia ha sido disminuir hasta que se aumentó en 2012.

Hay valores especiales en ciertos casos que se comentan en el post, por ejemplo aulas de enlace y alumnos en el exterior

Intento comentar por separado administraciones, comenzando por Madrid, en cuanto a máximos y mínimos, y por separado etapas (primaria, secundaria ...) aunque a veces puede estar mezclado, porque hablar de normativa de ratios en una administración implica hacerlo para una etapa y hacerlo de una etapa implica hacerlo de una administración, salvo los casos de normativa básica estatal. Además hay situaciones donde se realizan modificaciones conjuntas en varias administraciones y comunidades, como ocurrió con RDL14/2012

Ver en [Ratios según CCAA](#ratios-según-ccaa) 

Por ejemplo en [Ratios Aragón](#ratios-aragón) en 2000/2001 unos máximos menores de los estatales, mencionando un número de ACNEEs máximo y el número de alumnos total en esos casos (ver apartado sobre normativa ratios ACNEEs)

Otro ejemplo: Castilla-La Mancha puso unos máximos mayores de los estatales (creo post aparte [Ratios ilegales en educación en Castilla-La Mancha](http://algoquedaquedecir.blogspot.com/2017/11/ratios-en-educacion-ilegales-en.html))

Artículo de 2018 sobre Navarra  
[Nuevo ratio de alumnos en Infantil y Primaria en Navarra](https://web.archive.org/web/20200722053308/http://sociedadcivilnavarra.com/nuevo-ratio-alumnos-infantil-primaria-navarra/)  
> El Gobierno de Navarra con la consejera del ramo, Maria Solana, y el director general de Educación, Roberto Pérez, a la cabeza han tenido a bien implementar el ratio de 25 alumnos en los centros concentrados para los cursos de Infantil y Primaria. De este modo se equipararía el ratio a lo establecido en la red pública.  
> El problema de esta medida es que aplica igualmente a las matriculaciones de primer curso de Infantil como a las matriculaciones en el resto de cursos de los niveles de Infantil y Primaria que se producen normalmente por cambio de ciudad o de centro educativo. De este modo, las bajas que pudieran producirse en aulas con un número de alumnos superior a 25 no se van a cubrir. Ello tiene como consecuencia que nunca puedan darse cambios de centro educativo a partir de Segundo curso de Infantil y en adelante. Para ser más preciso, cambios a centros educativos de la red concertada porque es donde realmente se da el problema de los ratios.

Que muestra que en los privados con concierto el ratio era distinto y antes estaban a más de 25. 

Artículo de 2018 sobre Ibiza  
[ESPAÑA: EDUCACIÓN BAJA LA RATIO A 20 NIÑOS EN EL SEGUNDO CICLO DE INFANTIL](http://otrasvoceseneducacion.org/archivos/275513)  
> El Govern presentó ayer los criterios para la confección de unidades y de la cuota de profesorado en los centros docentes públicos de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria, Bachillerato, Formación Profesional y Estudios de Personas Adultas y de régimen especial para el curso 2018/19.  
Los criterios de este año prevén la reducción de ratios en el último curso de Educación Infantil (cinco años), fijada en 20 alumnos y un máximo de 23. Las ratios del cuarto y quinto curso (tres y cuatro años) se mantienen también en 20 alumnos. Se desdoblará el grupo a partir de 22 alumnos. Excepcionalmente, en los casos en que no sea posible desdoblar físicamente el grupo, se dotará de recursos adicionales de profesorado. Por otro lado, también bajan las ratios de FP Básica de 20 a 18 alumnos.  

Artículo de 2018 sobre Comunidad Valenciana 
[La Generalitat recurrirá la sentencia del TSJCV que anula la rebaja de la ratio de Infantil a 23 alumnos](https://valenciaplaza.com/la-generalitat-recurrira-la-sentencia-del-tsjcv-que-anula-la-rebaja-de-la-ratio-de-infantil-a-23-alumnos)  
> La Conselleria de Educación ha avanzado que la Abogacía de la Generalitat recurrirá la sentencia del TSJCV que anula su orden que rebajaba de 25 a 23 el número máximo de alumnos por aula en Educación Infantil. En cualquier caso, ha subrayado que no es firme y no afecta "de ninguna manera" al funcionamiento de los centros educativos.  
En el fallo, la Sala de lo Contencioso Administrativo considera que la norma autonómica es extemporánea e invade competencias estatales al modificar la ratio en 30 localidades. La anulación estima el recurso presentado por la Federación Española de Religiosos de la Enseñanza - Centros Católicos de la Comunidad Valenciana.  
Tras conocer la resolución, el departamento que dirige Vicent Marzà ha remarcado en primer lugar que "la orden 18/2017 no está en vigor, ya que la sustituye la 22/2018, del 30 de mayo" y ha subrayado además que "la normativa estatal marca máximos de ratios, no mínimos".  
Desde Educación recuerdan así que "la competencia exclusiva en materia de planificación educativa es la autonómica" y defienden que "todas las medidas que se emprenden para garantizar menos alumnado al aula van a favor de garantizar una mejor calidad y atención educativas al alumnado valenciano".  
Por tanto, la Conselleria ha garantizado que "se adoptarán todas las medidas que sean necesarias para velar por este derecho de los niños y niñas valencianos".  

En las elecciones autonómicas de Andalucía 2018 surge bajar ratios

[Así son los programas electorales de PSOE, PP, Ciudadanos y Adelante Andalucía para las elecciones andaluzas](https://sevilla.abc.es/elecciones/andalucia/sevi-programas-electorales-psoe-pp-ciudadanos-y-adelante-andalucia-para-elecciones-andaluzas-201811160726_noticia.html)  
> **-Adelante Andalucía:**  Becas-salario para los universitarios con menos ingresos. Destinar el 5% a las políticas educativas. Nuevos colegios, retirada de amianto y eficiencia energética. **Reducir la ratio por alumno a 20.** Reforma asamblearia en pos de una escuela laica, pública y gratis.  

[La educación en campaña: de la retirada de competencias a la ratio de alumnado](https://www.eldiario.es/andalucia/Elecciones_en_Andalucia_2018-programas-educacion_0_840966715.html)  
> La presidenta de la Junta repite en cada mitin que Andalucía está creciendo en número de maestros mientras la natalidad baja, aunque Podemos e IU le reprochan que ese dato es engañoso: el desequilibrio entre más docentes y menos niños no se ha traducido, en muchos casos, en una bajada de la ratio escolar, porque la Consejería ha redistribuido a los chavales y cerrado muchas unidades.  
...  
Adelante Andalucía propone becas-salario para los universitarios con menos ingresos, la creación de nuevos colegios y la reducción de la ratio a 20 alumnos por clase (ahora la legislación básica fija el mínimo en 25 y el máximo en 28 en Primaria).  

[Servicios Públicos: derechos, no privilegios](https://web.archive.org/web/20200721195007/https://adelanteandalucia.org/epigrafe/04/)  
> La situación del colectivo docente pasa por una creciente precarización de sus condiciones laborales, algo que merma la calidad de nuestra educación pública, con consecuencias directas para nuestro alumnado. Burocratización, **ratios elevadas**, bajas que no se cubren, horas de docencia sin dedicación para otras tareas necesarias como la preparación de material o correcciones, altas tasas de interinidad que en muchos centros merma las posibilidades de tener un proyecto educativo de calidad y garantista, etc.  

Se puede ver como en la tramitación de la derogación parcial de RDL14/2012 (ver post [Derogar RDL14/2012](https://algoquedaquedecir.blogspot.com/2018/08/derogar-rdl142012.html)) hubo votaciones para bajar ratios, que no salieron adelante ni en congreso ni en senado.

### Ratios según enseñanza

Los máximos y mínimos pueden variar según enseñanza, ver [Artículo 3. Las enseñanzas. (LOE)](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a3)

A veces la normativa regula varias enseñanzas conjuntamente

#### Ratios educación infantil

[La relación numérica profesor/alumno por unidad en el primer ciclo de Educación Infantil por comunidad autónoma - Referencias legislativas](https://www.educacionyfp.gob.es/educacion/mc/redie-eurydice/sistemas-educativos/organizacion-y-administracion/ratio-primer-ciclo/legislacion.html)

[La relación numérica alumno/profesor por unidad en el primer ciclo de Educación Infantil en las administraciones educativas](https://www.educacionyfp.gob.es/mc/redie-eurydice/sistemas-educativos/organizacion-y-administracion/ratio-primer-ciclo.html)  
> Ratio: Número máximo de niños por profesional o grupo durante las horas obligatorias de un día de trabajo. (Tomado de Key Data on Early Childhood Education and Care - 2019, estudio en elaboración).

[Ratio alumno/profesor en el primer ciclo de Educación Infantil](https://www.educacionyfp.gob.es/dam/jcr:889c0725-04ac-43bf-865c-bf6605614211/2-4-2-4-1-1-ratio-alumno-profesor-primer-ciclo-ei-ca-b02-28022019.pdf)

<!-- Elimino imagen: no está certifiado ok y no compila tls: failed to verify certificate: x509: certificate signed by unknown authority ![](https://www.educacionyfp.gob.es/.imaging/mte/mefp-theme/contenido-cim-md/dam/mefp/mc/redie-eurydice/redipedia/e-infantil/ratio-primer-ciclo/ratio-alumnado-primer-ciclo-2023/jcr:content/ratio-alumnado-primer-ciclo-2023.png.png)-->

#### Ratios Primaria

[(RD132/2010) Artículo 11. Relación de alumnos por unidad.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a11)  
> Los centros de educación primaria tendrán, como máximo, 25 alumnos por unidad escolar

[(LOE) Artículo 157. Recursos para la mejora de los aprendizajes y apoyo al profesorado.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a157)  
> a) Un número máximo de alumnos por aula que en la enseñanza obligatoria será de 25 para la educación primaria ...

#### Ratios Secundaria

[(RD132/2010) Artículo 16. Relación de alumnos por unidad.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a11)  
> Los centros de educación secundaria tendrán, como máximo, 30 alumnos por unidad escolar en educación secundaria obligatoria ...

[(LOE) Artículo 157. Recursos para la mejora de los aprendizajes y apoyo al profesorado.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a157)  
> a) Un número máximo de alumnos por aula que en la enseñanza obligatoria será ... de 30 para la educación secundaria obligatoria

#### Ratios Bachillerato

[(RD132/2010) Artículo 16. Relación de alumnos por unidad.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a11)  
> Los centros de educación secundaria tendrán, como máximo, ... 35 en bachillerato.

#### Ratios FPB

En Madrid el límite era 20  
[ORDEN 1409/2015, de 18 de mayo, de la Consejería de Educación, Juventud y Deporte, por la que se regulan aspectos específicos de la Formación Profesional Básica en la Comunidad de Madrid.](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=8954)  
> Artículo 6.- Autorización y formación de grupos  
> 2. El **número máximo de alumnos por grupo en la oferta obligatoria de cada ciclo de Formación Profesional Básica será de 20**, sin perjuicio de lo establecido en el artículo 87.2 de la LOE.

Es derogada por [Orden 893/2022, de 21 de abril, de la Consejería de Educación, Universidades, Ciencia y Portavocía, por la que se regulan los procedimientos relacionados con la organización, la matrícula, la evaluación y acreditación académica de las enseñanzas de formación profesional del sistema educativo en la Comunidad de Madrid. ](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=12648) que no cita nada sobre número de alumnos.

En Navarra el valor por defecto es 10, con máximo de 16  
[ORDEN FORAL 66/2016, DE 6 DE JUNIO, DEL CONSEJERO DE EDUCACIÓN, POR LA QUE SE REGULA LA ORDENACIÓN Y EL DESARROLLO DE LA FORMACIÓN PROFESIONAL BÁSICA EN EL ÁMBITO DE LA COMUNIDAD FORAL DE NAVARRA](http://www.lexnavarra.navarra.es/detalle.asp?r=37553)  
> Artículo 7. Formación de grupos.  
Con carácter general, **la ratio será de diez alumnos y/o alumnas mínimo, máximo de catorce alumnos y/o alumnas y excepcionalmente dieciséis**, siempre incluido en dicha ratio el alumnado repetidor.

[INSTRUCCIONES DE LA DIRECCIÓN GENERAL DE EVALUACIÓN Y COOPERACIÓN TERRITORIAL Y DE LA DIRECCIÓN GENERAL DE FORMACIÓN PROFESIONAL PARA SU APLICACIÓN EN LAS CIUDADES AUTÓNOMAS DE CEUTA Y MELILLA, DURANTE EL CURSO ESCOLAR 2019-2020.](http://www.educacionyfp.gob.es/dam/jcr:9b37aaf8-97b8-4cf3-9f1b-09ef0f4b2b5f/instrucciones-curso-19-20-ceuta-melilla.pdf)  
> 3.3.1. Organización de los grupos en régimen presencial.  
En la Formación Profesional Básica el número de alumnos y alumnas por grupo será de **25 con carácter general. Se podrán establecer grupos con 20 estudiantes** previo informe de la inspección educativa con autorización de la Dirección Provincial.

#### Ratios universidad

No localizada normativa histórica aunque se citan ideas asociadas a clases no presenciales.

La idea es recordar clases abarrotadas con alumnos en los laterales. Aplicaría normativa seguridad 

En 2020 me lo recuerda esta imagen

[twitter RaulCanay/status/1303274699426861056 - archive](https://web.archive.org/web/20200908101237/https://twitter.com/RaulCanay/status/1303274699426861056)  
De 500 a 149.  
[![](https://web.archive.org/web/20200908101237im_/https://pbs.twimg.com/media/EhYpbQZXYAE_93s.jpg)](https://web.archive.org/web/20200908101237im_/https://pbs.twimg.com/media/EhYpbQZXYAE_93s.jpg)

### Ratios según materias 

#### Ratios en religión 

Es habitual es el tratamiento distinto de las ratios en religión, especialmene los mínimos, como se ve en este artículo de 2019 

[15 alumnos para impartir Geografía o Física, sin límite para Religión](https://cadenaser.com/emisora/2019/09/06/radio_madrid/1567753033_256619.html)  
> La Comunidad de Madrid establece una cuota mínima de estudiantes para que un centro pueda impartir una materia optativa o de libre configuración en Bachillerato mientras que la ley estatal le obliga a dar Religión aunque hay un solo alumno  

> Las instrucciones de la Comunidad de Madrid para este inicio de curso son claras: para la importación de las materias troncales de opción, específicas y de libre configuración autonómica de Bachillerato se exigirá un mínimo de 15 alumnos matriculados. En el caso de una Segunda Lengua Extranjera, Griego o Latín un mínimo de 10 pero no hay límite ninguno para Religión. En las instrucciones de la Comunidad se señala que en el caso de la Religión se atenderá a lo que marque la normativa específica y lo que marca la ley estatal es que debe ser de oferta obligada, lo que implica que no se puede fijar un cupo mínimo de alumnos.

En instrucciones 2019-2020 y 2023-2024 aparece esta frase de "normativa específica" citada por el artículo de 2019   
> En la materia de Religión, para la formación de grupos se estará a lo establecido en su normativa específica.

En instrucciones anteriores no aparecía esa frase. En 2011-2012 Madrid no se fijaba mínimo en general  
> Los centros deberán organizar los grupos que impartan estas enseñanzas de modo que se procure una correcta utilización de los recursos disponibles. Para ello, si el número de alumnos que opta por estas enseñanzas en cada grupo fuera pequeño, se agruparán con los alumnos de otros grupos del mismo curso, siempre y cuando estas agrupaciones no superen la ratio prevista. 

En 2017-2018 Madrid se decía lo mismo con otras palabras  
> Los centros deberán organizar los grupos de alumnos de enseñanzas de Religión de modo que la asignación horaria realizada por la Dirección General de Recursos Humanos sea utilizada para la impartición de estas enseñanzas. Se llevarán a cabo agrupaciones de alumnos del mismo curso cuando el número de horas asignadas no sea suficiente para impartir docencia a todos los grupos, siempre y cuando estas agrupaciones no superen la ratio prevista para cada etapa

Se permiten seminarios de religión en FP GM con 10 alumnos (común a instrucciones 2011-2012 Madrid)  
> en los ciclos formativos de grado medio de formación profesional específica, sin detrimento del horario general y siempre que las circunstancias del centro lo permitan y el número de solicitantes sea **al menos de 10 alumnos**, podrá ofrecerse un Seminario de Religión de un periodo lectivo semanal.

El mismo valor mínimo para seminarios de religión en FP sigue con otro texto en instrucciones 2019-2020  
> En los Ciclos Formativos de Grado Medio de Formación Profesional podrá ofrecerse un seminario de Religión de un periodo lectivo semanal, siempre que las circunstancias del centro lo permitan, el número de solicitantes sea **al menos de 10 alumnos** y sin detrimento del horario del ciclo formativo y, en su caso, del grupo.

#### Ratios en enseñanzas complementarias de Lengua y Cultura españolas para alumnos españoles residentes en el exterior

[Orden EDU/3122/2010, de 23 de noviembre, por la que se regulan las enseñanzas complementarias de Lengua y Cultura españolas para alumnos españoles residentes en el exterior y se establece el currículo de las mismas.](https://www.boe.es/buscar/doc.php?id=BOE-A-2010-18555)  
> Artículo 18. Composición de los grupos.  
1. El número máximo de alumnos por grupo será de 23 para los grupos del mismo nivel y de 18 para los grupos con dos o más niveles. Excepcionalmente, y por razones justificadas, podría autorizarse un incremento de hasta 5 alumnos por encima de las limitaciones señaladas.

### Ratios según CCAA

#### Ratios Madrid

[ORDEN 2398/2016, de 22 de julio, de la Consejería de Educación, Juventud y Deporte de la Comunidad de Madrid, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=9459) _derogada_  
> Artículo 44 .-  Número de alumnos por aula  
Con carácter general, el número máximo de alumnos por aula en cada uno de los cursos de la Educación Secundaria Obligatoria será de treinta, salvo en los supuestos de incremento de ratios que se establezcan de conformidad con el artículo 87.2 de la Ley Orgánica 2/2006, de 3 de mayo.  
> Artículo 45 .-  Ratio de alumnos por grupo para impartir las materias en centros sostenidos con fondos públicos  
> 1. La impartición de las materias del bloque de asignaturas troncales, del bloque de asignaturas específicas y del bloque de asignaturas de libre configuración autonómica que sean obligatorias y no estén sujetas a opción se regirá por lo dispuesto en el artículo 44 de esta Orden.  
> 2. La impartición de las materias sujetas a opción del bloque de asignaturas troncales, del bloque de asignaturas específicas, y del bloque de asignaturas de libre configuración autonómica, quedará vinculada a que exista un número **mínimo de quince alumnos por grupo**. 

[ORDEN 1712/2023, de 19 de mayo, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/31/BOCM-20230531-17.PDF)  
> Artículo 7 Ratio general y condiciones para conformar grupos de materia dentro
del grupo de referencia  
> 1. Los grupos de referencia tendrán una **ratio máxima de treinta alumnos**, sin perjuicio de los supuestos de incremento de ratio que pudieran producirse en aplicación de lo dispuesto en el artículo 87.2 de la Ley Orgánica 2/2006, de 3 de mayo, de Educación.  
> 2. En los centros sostenidos con fondos públicos, los grupos de materias de opción
del cuarto curso de la Educación Secundaria Obligatoria se conformarán con un **mínimo de quince alumnos**.

[ORDEN 2582/2016, de 17 de agosto, de la Consejería de Educación, Juventud y Deporte de la Comunidad de Madrid, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en el Bachillerato.](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=9476) _derogada_  
> Artículo 37 .-  Número de alumnos por aula  
El número máximo de alumnos por aula en cada uno de grupos de la etapa será de **treinta y cinco**, salvo en el supuesto de incremento de ratio que se establezca de conformidad con el artículo 87.2 de la Ley Orgánica 2/2006, de 3 de mayo, y sin menoscabo de las ratios que se establezcan para otros regímenes de Bachillerato destinados a personas adultas.  
Artículo 38 .-  Impartición de materias  
>1. Los centros deberán ofrecer todas las materias generales y de opción del bloque de asignaturas troncales de las modalidades que se imparten en el centro, así como la materia específica obligatoria Educación Física y las materias opcionales del bloque de asignaturas específicas que sean de oferta obligada para todas las modalidades, y Tecnología Industrial I y II, de oferta obligada en los centros que impartan la modalidad de Ciencias.  
> 2. En los centros sostenidos con fondos públicos la impartición de las materias troncales de opción, específicas y, en su caso, de libre configuración autonómica quedará vinculada a que exista un número **mínimo de quince alumnos**.  
> 3. Excepcionalmente, la Dirección de Área Territorial podrá autorizar, previa solicitud justificada e informe del Servicio de Inspección Educativa, el funcionamiento de grupos por debajo de la ratio indicada en el apartado 2, siempre que no suponga la obligación de incrementar los efectivos de profesorado de los centros públicos ni el incremento de la ratio de profesorado establecida para esta etapa en los centros privados concertados. En este último caso, la Dirección General de Innovación, Becas y Ayudas a la Educación será debidamente informada sobre el particular por la Dirección de Área Territorial correspondiente.

[ORDEN 2067/2023, de 11 de junio, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en el Bachillerato.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/06/21/BOCM-20230621-14.PDF)  
> Artículo 7 Ratio general y condiciones para conformar grupos de materia
dentro del grupo de referencia en régimen ordinario  
1. Los grupos de referencia tendrán una **ratio máxima de treinta y cinco alumnos**, sin
perjuicio de los supuestos de incremento de ratio que pudieran producirse en aplicación de
lo dispuesto en el artículo 87.2 de la Ley Orgánica 2/2006, de 3 de mayo, de Educación.  
2. En los centros sostenidos con fondos públicos, los grupos de materias específicas de
modalidad se conformarán con un **mínimo de quince alumnos**.  

Las instrucciones anuales de Madrid también realizan regulación sobre ratios. Se citan ideas aquí para las últimas instrucciones y algunas de las anteriores, aunque algunos aspectos se vuelvan a citar en el apartado del post que los trata

En [INSTRUCCIONES 2023-2024 MADRID](https://www.comunidad.madrid/transparencia/sites/default/files/regulation/documents/resolucion_conjunta_instrucciones_inicio_de_curso_23-24.pdf)  

> 4.8.1. Organización de las actuaciones de compensación educativa  
El número de alumnos de estos grupos **no debe ser inferior a ocho ni superior a doce**.

> 6.1.12. Ratio de la etapa  
Los grupos de referencia tendrán una **ratio máxima de treinta alumnos**, sin perjuicio de los supuestos
de incremento de ratio que pudieran producirse en aplicación de lo dispuesto en el artículo 87.2 de
la Ley Orgánica 2/2006, de 3 de mayo, de Educación.  
En el caso del **primer curso de Educación Secundaria Obligatoria, la ratio será, con carácter general, de 25 alumnos por grupo**. De conformidad con el apartado 3 de la instrucción séptima de la
Resolución conjunta de las Viceconsejerías de Política Educativa y de Organización Educativa de
25 de noviembre de 2022, modificada por la resolución conjunta de las Viceconsejerías de Política
Educativa y de Organización Educativa de 16 de marzo de 2023, en el proceso de admisión de
alumnos para el curso 2023/2024, en los casos de centros que cuenten con una demanda superior
de plazas escolares, centros únicos de localidad, así como centros que no cuenten con la posibilidad
de incrementar el número de unidades, las Direcciones de Área Territorial podrán autorizar, antes
de la adjudicación de plaza escolar, que la citada oferta inicial de plazas a que se hace referencia
en dicho apartado se incremente hasta un 10% en el número de alumnos por grupo.

>6.1.15. Programa de Diversificación Curricular  
El Programa de Diversificación se impartirá conforme a lo dispuesto en la Orden 190/2023 ... . De conformidad con el artículo 5 de la citada orden para la implantación de los ámbitos de este programa, el número de alumnos por grupo **no podrá ser superior a 15 ni inferior a 10**.

En algunas anteriores como [INSTRUCCIONES 2017-2018 MADRID](http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application/pdf&blobheadername1=Content-Disposition&blobheadervalue1=filename=InstruccionesInicioCurso1718.pdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1352934784947&ssbinary=true) e [INSTRUCCIONES 2019-2020 MADRID](http://www.comunidad.madrid/sites/default/files/doc/educacion/instrucciones_inicio_curso_2019_2020_firmado.pdf) se indicaba lo mismo para los ámbitos del Programa de Mejora del Aprendizaje y del Rendimiento.

Es relevante ver como se suelen fija mínimos en cuanto a optativas en 4ºESO y Bachillerato, rebajando esos mínimos en ciertas materias. 

En 2023-2024  
>6.1.13. Organización y opciones en 4º de ESO  
Los grupos de materia de opción del 4º curso de ESO se conformarán con un **mínimo de quince alumnos**. Excepcionalmente, el director del centro podrá conformar grupos de materias de opción
por debajo de dicha ratio. ... En todo caso, las materias Segunda Lengua Extranjera y Latín podrán impartirse siempre que cuenten con
un número **mínimo de 10 alumnos** y ello no implique un incremento en la dotación del profesorado.  
En el caso de que la misma materia se imparta en más de un grupo, el número de alumnos en cada
uno de ellos **no será inferior a 17**, pudiendo llegar a la ratio establecida con carácter general.

>6.1.14. Cálculo de grupos para materias optativas  
En la etapa de Educación Secundaria Obligatoria, los grupos de materias optativas se conformarán
con un **mínimo de 15 alumnos**. Excepcionalmente, el director del centro podrá conformar grupos de
materias optativas por debajo de dicha ratio. Esta decisión deberá comunicarla a la Dirección de
Área Territorial de forma motivada, informará de la misma a los órganos colegiados del centro
docente y será supervisada por el Servicio de Inspección Educativa. El funcionamiento de grupos
de materias optativas por debajo de quince alumnos se llevará a cabo con el cupo de profesorado
asignado al centro. En todo caso, las materias Filosofía y Cultura Clásica podrán impartirse siempre
que cuenten con un número **mínimo de 10 alumnos** y ello no implique un incremento en la dotación
del profesorado.

En algunas anteriores se citaban otras materias
En 2019-2020  
> 4.3.12. Cálculo de grupos para asignaturas específicas opcionales y de libre configuración autonómica  
En Educación Secundaria Obligatoria, los grupos de materias específicas opcionales y de libre
configuración autonómica se conformarán con un **mínimo de 15 alumnos**, excepción hecha de
las materias Recuperación de Lengua y Recuperación de Matemáticas. ...  
No obstante, las Direcciones de Área Territorial podrán autorizar, siempre que exista 
disponibilidad horaria de los profesores, la impartición de las materias Segunda Lengua
Extranjera, Cultura Clásica, Religión y Valores Éticos a un número menor de alumnos.

En 2023-2024  
>6.3.3. Materias de Bachillerato  
La impartición de las materias específicas de modalidad y, en su caso, optativas, exigirá un **mínimo de 15 alumnos**.  
Excepcionalmente, el director del centro podrá conformar grupos de materias específicas de
modalidad y materias optativas por debajo de dicha ratio. ... En todo caso, las materias Segunda Lengua Extranjera, Dibujo
Técnico, Latín y Griego podrán impartirse siempre que cuenten con un número **mínimo de 10 alumnos** y ello no implique un incremento en la dotación del profesorado.

En algunas anteriores como se fijaban otros mínimos y para otras materias  
En [INSTRUCCIONES 2011-2012 MADRID](https://drive.google.com/open?id=0B-t5SY0w2S8ieUwxeUNURGxlN3M) 5 alumnos y solo Latín y Griego  
>No obstante, en las materias de Latín y Griego de Bachillerato se autorizarán, con carácter excepcional, para el curso 2011-2012 grupos con **al menos 5 alumnos**, siempre que se disponga de profesorado con destino definitivo en las especialidades de Latín o Griego y no suponga aumento de cupo 

En instrucciones 2019-2020 10 alumnos y solo Latín y Griego  
>4.5.3. Materias de Bachillerato  
Materias troncales de opción, específicas y de libre configuración autonómica:  
En las materias troncales de opción, específicas y, en su caso, de libre configuración, su 
impartición exigirá un **mínimo de 15 alumnos matriculados**. Esta limitación numérica no será de 
aplicación a la impartición de las materias Segunda Lengua Extranjera y Griego, ni a la materia 
troncal general Latín, siempre que esto no implique un incremento en la dotación del profesorado 
y se cuente con un número **mínimo de 10 alumnos**. 

#### Ratios Aragón

[ORDEN de 17 de marzo de 2000, del Departamento de Educación y Ciencia, por la que se determina el número máximo de alumnos por aula en los centros sostenidos con fondos públicos de la Comunidad Autónoma de Aragón desde el curso escolar 2000/2001.](http://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=429414803231)  
> Primero: El número máximo de alumnos que podrán matricular los centros sostenidos con fondos públicos en cada
unidad y para cada uno de los cursos, será el que se establece en el anexo incorporado a esta Orden.

2º Ciclo Ed. Infantil, 1º 20  
2º Ciclo Ed. Infantil, 2º y 3º 22  
Educación Primaria, 1º a 6º 22  
Ed. Secundaria Obligato., 1º 27  
Bachillerato, 1º 30  

#### Ratios País Vasco

[ANEXO I CRITERIOS Y RATIOS DE CONCERTACIÓN PARA LOS CURSOS ESCOLARES CORRESPONDIENTES AL PERÍODO 2016-2022](https://www.euskadi.eus/contenidos/ayuda_subvencion/concier_ed_taldeak_2016_2017/es_def/Anexo%20I.pdf)  

Aquí hay tres tablas: 

1- NÚMERO DE ALUMNADO NECESARIO PARA FORMAR GRUPOS AL INICIO DE CADA ETAPA DE ENSEÑANZA

2- NÚMERO DE ALUMNADO NECESARIO PARA MANTENER GRUPOS DEL CURSO ANTERIOR POR ETAPA DE ENSEÑANZA

3- NÚMERO DE ALUMNADO NECESARIO PARA MANTENER LOS GRUPOS CONFORMADOS EN CURSOS POSTERIORES

A los centros privados con concierto les interesa tener aunque sea un año una matriculación alta y conseguir el máximo de aulas posibles al inicio de cada etapa. Si al comienzo de etapa en primaria necesitas 59 matriculas para obtener 3 aulas concertadas, al año siguiente es suficiente que se matriculen 45 para mantener las 3 aulas.

Esto enlaza con el tema de que es difícil cerrar líneas en privados con concierto (se les da concierto de ciertas unidades varios años) pero fácil en públicos (lo deciden curso a curso), que es un post pendiente.

### Ratios en atención a la diversidad 

Al hablar de diversidad se hace referencia a situaciones especiales: ACNEE, TGD, refuerzos, compensatoria, aulas enlace, aulas hospitalarias ...

Aquí intento ver normativa sobre ACNEEs en centros que no sean de educación especial, para los que hay normativa separada, aunque a veces se trata educación de ACNEEs en centros ordinarios y de educación especial en la misma normativa. Esta normativa debe fijar los apoyos (PT, AL) y al mismo tiempo si se tiene en cuenta la reducción de alumnos al tener ACNEEs.

He escuchado varias veces la idea de de que cada ACNEE se contabiliza para ratio como 2 alumnos, pero no he localizado referencias; es algo oficioso que no se aplica siempre y que no está respaldado por normativa.

La normativa en BOE localizada:

No derogada pero de 1983 (la cito en la tabla inicial)

[Orden de 14 de junio de 1983 por la que se desarrolla el Real Decreto 2639/1982, de 15 de octubre, de ordenación de la Educación Especial en lo que se refiere a los niveles de Preescolar y Educación Básica.](https://www.boe.es/buscar/doc.php?id=BOE-A-1983-17045)  
> 8. 1. La relación máxima profesor/alumno será de 1:25 en las unidades ordinarias de Integración Completa y/o de Integración Combinada.  
> 2. En las unidades de Educación Especial de Integración Combinada se formarán grupos de alumnos de acuerdo con el tipo y grado de dificultad, que recibirán atención individualizada a tiempo parcial. La composición de estos grupos oscilará de uno a cinco alumnos.
> 3. En las unidades de Educación Especial con alumnos de integración Parcial y en las de centros específicos, en función del grado y tipo de deficiencia, la relación profesor/alumnos se mantendrá dentro de los siguientes límites:  
Disminuidos psíquicos: 10-12 alumnos/unidad.  
Disminuidos sensoriales: 10-12 alumnos/unidad.  
Autistas: 3-5 alumnos unidad.  
Disminuidos Físicos (plurideficientes): 8/12 alumnos unidad.  
> 4. El número de alumnos disminuidos integrados en aulas ordinarias no será superior a tres.  
> 5. En todo caso, estas cuantificaciones se harán teniendo en cuenta las peculiaridades de los alumnos, de acuerdo con los informes que al respecto se emitan por la Inspección Técnica de Educación Básica.  
> 9. 1. La atención educativa al disminuido se iniciará tan pronto como sea posible, en el marco del sistema ordinario de Educación Preescolar, mediante la modalidad de Integración Completa.  
> 2. La admisión de alumnos disminuidos se llevará a cabo en el nivel de Preescolar, salvo casos extremos, en las mismas condiciones y con los mismos requisitos que los establecidos para los demás alumnos de este nivel.  
> 3. Los alumnos disminuidos que presenten en este nivel dificultades recibirán los apoyos específicos y de rehabilitación que requiera cada caso.  
> 4. En las unidades de Educación Preescolar el número de alumnos disminuidos no será superior a tres.  
> 5. En todo caso, estas cuantificaciones se harán teniendo en cuenta las peculiaridades de los alumnos de acuerdo con los informes que al respecto se emitan por la Inspección Técnica de Educación Básica.

De 1990 pero derogada (sin embargo estudio de sindicato en 2019 la cita)

**4 noviembre 2019**  
[Faltan 626 especialistas en pedagogía terapéutica y en audición y lenguaje en primaria y secundaria](http://madrid.ccoo.es/noticia:405756--Faltan_626_especialistas_en_pedagogia_terapeutica_y_en_audicion_y_lenguaje_en_primaria_y_secundaria)  
> El informe presentado hoy por CCOO recoge datos del 84% de los centros de infantil y primaria y secundaria de la región. El 72% del alumnado con necesidades educativas especiales está afectado por una ratio excesiva. Faltan 626 profesionales, 421 en primaria y 188 en secundaria.  
Si se tiene en cuenta que, según marca la Orden de 18 de septiembre de 1990, cada especialista en pedagogía terapéutica debería atender a 12 alumnos y cada especialista en audición y lenguaje debería atender a 25 alumnos, faltan 626 profesionales para este alumnado. De ellos, 385 son especialistas en pedagogía terapéutica (PT), y 241 lo son en audición y lenguaje (AL). En educación primaria, la etapa donde se hace el diagnóstico temprano que garantiza la progresión educativa adecuada, faltan 421 profesionales (un 67%); y en educación secundaria faltan 188.

[Orden de 18 de septiembre de 1990 por la que se establecen las proporciones de profesionales/alumnos en la atención educativa de los alumnos con necesidades especiales.](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24063)  
> 2.2 ALUMNOS QUE REQUIEREN ADAPTACIONES SIGNIFICATIVAS DE LOS ELEMENTOS DEL CURRICULUM ORDINARIO, LAS CUALES CONLLEVAN LA PRESENCIA DE MEDIOS PERSONALES Y MATERIALES COMPLEMENTARIOS; DICHAS ADAPTACIONES AFECTAN POR LO GENERAL, POR UN LADO, A LA ORGANIZACION DE LOS CENTROS Y A LA METODOLOGIA, Y, POR OTRO, A LA PRIORIZACION DE ALGUNAS AREAS Y CONTENIDOS.  
PARA ESTOS ALUMNOS, LAS PROPORCIONES SERAN:  
2.2.1 PROFESORES DE APOYO PARA LA EDUCACION ESPECIAL EN CENTROS ORDINARIOS Y PROFESORES TUTORES EN UNIDADES DE EDUCACION ESPECIAL, TANTO EN CENTROS ORDINARIOS COMO EN CENTROS DE EDUCACION ESPECIAL  
ALUMNOS / EDUCACION BASICA / F.P. (APRENDIZAJE TAREAS)  
CON DEFICIENCIA MOTORICA 1/8-12 1/8-12  
CON DEFICIENCIA PSIQUICA 1/9-12 1/9-12  
CON PROBLEMAS EMOCIONALES DE CARACTER GRAVE 1/6-8 1/6-8  
CON DEFICIENCIAS AUDITIVAS O VISUALES 1/9-12 1/9-12  
2.2.2 FISIOTERAPEUTAS  
ALUMNOS / EDUCACION BASICA / F.P. (APRENDIZAJE TAREAS)  
CON DEFICIENCIA MOTORICA 1/15-20 1/15-20  
CON DEFICIENCIA PSIQUICA 1/70-75 1/70-75  
2.2.3 LOGOPEDAS  
ALUMNOS / EDUCACION BASICA / F.P. (APRENDIZAJE TAREAS)  
CON DEFICIENCIA MOTORICA 1/20-25 1/35-40  
CON DEFICIENCIA PSIQUICA 1/30-35 1/70-75  
CON PROBLEMAS EMOCIONALES DE CARACTER GRAVE 1/20-25 1/35-40  
CON DEFICIENCIAS AUDITIVAS 1/15-20 1/30-35  
2.2.4 AUXILIARES TECNICOS EDUCATIVOS  
ALUMNOS / EDUCACION BASICA / F.P. (APRENDIZAJE TAREAS)  
CON DEFICIENCIA MOTORICA 1/15-20 1/15-20  
CON PROBLEMAS EMOCIONALES DE CARACTER GRAVE 1/15-20 1/15-20  

Aparece esto que es una referencia a haber 2 ACNEE y reducción, pero no indica de manera explícita que 2 sea el máximo ni que suponga reducción.

> CUARTO. EN LAS AULAS DE LOS CENTROS ORDINARIOS EN LOS QUE HAYA ESCOLARIZADOS DOS ALUMNOS CON NECESIDADES ESPECIALES, CONTEMPLADOS EN LOS APARTADOS 2.1 Y 2.2 DE ESTA ORDEN, DURANTE LA MAYOR PARTE DEL HORARIO ESCOLAR Y PARTICIPANDO EN LA MAYORIA DE LAS ACTIVIDADES EDUCATIVAS QUE SE DESARROLLAN EN ESTAS AULAS, EL NUMERO TOTAL DE ALUMNOS SE ESTABLECE ENTRE 20 Y 25.

Luego hay, o debería haber, normativa propia de cada comunidad

Pendiente tratar el tema de normativa básica y subsidiaria, que es post aparte y enlazar aquí.

He escuchado que un alumno ACNEE cuenta como dos de cara a ratios por grupo, y que hay un máximo de 2 ó 3 por grupo pero no he localizado normativa estatal vigente que fije esa reducción de ratio asociada a ACNEEs. La normativa de 1983 que fija un límite en 3 está asociada al tiempo a un máximo de 25 alumnos por aula en integración completa y combinada que se asocian a educación ordinaria, no especial

> 2. La Educación Básica del disminuido será obligatoria y gratuita.  
> 3. 1. La Educación del disminuido se impartirá en alguna de las formas y atendiendo a las orientaciones que se establecen en los números cuarto al séptimo de la presente Orden.  
> 2. El criterio integrador que deben tener las distintas formas de integración será flexible, por lo que no se entenderán con carácter permanente, sino que tendrán un carácter dinámico en función de la evolución del alumno.  
> 4. Integración completa en unidades ordinarias.- 1 Estará dirigida a aquellos alumnos que presenten una dificultad ligera de carácter transitorio en determinado momento de su proceso educativo, o que por la naturaleza de su disminución necesiten un apoyo específico.  
> 2. El programa en esta forma de integración será el ordinario, con las orientaciones pedagógicas individualizadas, de acuerdo con la dificultad que presente el alumno. Será aplicado por el profesor del aula, de acuerdo con las indicaciones que reciba de los Grupos de Apoyo.  
> 5. Integración combinada en centros ordinarios.- 1. Atenderá a alumnos con dificultades de carácter previsiblemente transitorio en algún área del programa que exija un tratamiento especializado.

Buscando he encontrado normativa autonómica de Aragón que lo limita, es de año 2000

[ORDEN de 17 de marzo de 2000, del Departamento de Educación y Ciencia, por la que se determina el número máximo de alumnos por aula en los centros sostenidos con fondos públicos de la Comunidad Autónoma de Aragón desde el curso escolar 2000/2001.](http://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=429414803231)  
> Segundo: En las aulas en las que se escolaricen Alumnos con Necesidades Educativas Especiales debidamente diagnosticados, el número máximo de alumnos por aula será el que se establece en el citado anexo.

Y anexo indica un máximo de 2 ACNEEs por aula, y la reducción de ratio es de 2 alumnos no ACNEE por cada alumno ACNEE

![](https://1.bp.blogspot.com/-RbtgT8l8G8M/XDBu_67Td0I/AAAAAAAAPZI/G1yj_mJJoEEmogBjgJpAfAH-ONY4EGKNACLcBGAs/s640/2000-RatiosAcnee-Aragon.jpg)

En Madrid no he visto nada similar, pero sí que he visto esto, de 2015 (enlace no operativo en 2019), que a mi me indica que no hay normativa que lo fije

http://www.fetemadrid.es/areas/consejoEscolar/2015-05-13_votoUGT_3.pdf  
> ...La realidad es que en las aulas ordinarias no hay reducción de ratio...

Por procesar y documentar este documento  
[NORMATIVA EN LA COMUNIDAD DE MADRID QUE RIGE LOS MEDIOS Y RECURSOS NECESARIOS PARA ALCANZAR EL MÁXIMO DESARROLLO PERSONAL DE LOS ALUMNOS CON NEE (NECESIDADES EDUCATIVAS ESPECIALES) ASOCIADAS A LA DISCAPACIDAD. Abril 2010 Comisión Educativa CERMI Comunidad de Madrid](https://autismomadrid.es/wp-content/uploads/2011/08/NORMATIVA-NEE-Medios-y-Recursos-15-Mayo-2010.pdf)

Para Ceuta y Melilla, vigente, no cita ACNEE pero sí refuerzos, apoyos, aulas enlace

Pendiente tratar el tema de normativa básica y subsidiaria, que es post aparte y enlazar aquí, pero eso en principio haría que por increíble que parezca es normativa de Ceuta y Melilla aplique a Madrid al no tener normativa propia.

[Orden EDU/849/2010, de 18 de marzo, por la que se regula la ordenación de la educación del alumnado con necesidad de apoyo educativo y se regulan los servicios de orientación educativa en el ámbito de gestión del Ministerio de Educación, en las ciudades de Ceuta y Melilla.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-5493)

[Artículo 37. Organización de la atención al alumnado que se encuentra en situación de desventaja socioeducativa en educación primaria.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-5493#a3-9)  
> 4. El número de alumnos en estos grupos **no debe ser superior a doce** y su adscripción a los mismos se revisará periódicamente en función de sus progresos de aprendizaje, coincidiendo con el calendario de evaluación que el centro tenga establecido con carácter general.

[Artículo 40. Apoyo educativo en pequeño grupo fuera del aula de referencia.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-5493#a4-2)  
> 3. El número de alumnos atendidos en estos grupos **no será superior a doce por grupo** y su adscripción a los mismos se revisará periódicamente, en función de sus progresos de aprendizaje, coincidiendo con el calendario de evaluaciones que el centro tenga establecido con carácter general.

[Artículo 41. Atención en grupos específicos.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-5493#a4-3)  
> 5. Los centros podrán establecer grupos específicos de alumnos que se encuentren en situación de desventaja socioeducativa con un **mínimo de diez y un máximo de quince alumnos por grupo**. Los alumnos de grupos específicos mantendrán su grupo de referencia y su adscripción deberá revisarse periódicamente, en función de sus progresos de aprendizaje, coincidiendo con el calendario general de evaluaciones que el centro tenga establecido.

[Artículo 43. Atención al alumnado con carencias en el uso de la lengua castellana.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-5493#a4-5)  
> 4. La atención en pequeño grupo descrita en el apartado anterior se realizará mediante desdoblamiento en las horas coincidentes con la impartición del área o materia, según la etapa de que se trate, de Lengua castellana y literatura. El número de alumnos de esos grupos **no deberá ser superior a ocho** y su adscripción a los mismos se revisará periódicamente, en función de la evolución de sus aprendizajes.

[CAPÍTULO VIII Atención al alumnado que no puede asistir de modo regular a los centros docentes](https://www.boe.es/buscar/act.php?id=BOE-A-2010-5493#cv-4)  

[Artículo 44. Escolarización.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-5493#a4-6)  
> 3. El Ministerio de Educación, previo acuerdo con la Administración sanitaria competente, podrá habilitar o crear unidades escolares en los centros hospitalarios sostenidos con fondos públicos que mantengan regularmente hospitalizado un **mínimo de cinco alumnos** en edad de escolarización obligatoria. Asimismo, y en las mismas condiciones, se podrá concertar el funcionamiento de unidades escolares en instituciones hospitalarias de titularidad privada.

[Mantenimiento de la ratio de 5 alumnos/as por aula TGD](https://web.archive.org/web/20190609000908/http://www.madrid.org/es/transparencia/compromiso/mantenimiento-ratio-5-alumnosas-aula-tgd)  
> Hemos cumplido nuestro compromiso de **ratio máxima de 5 alumnos por aula TGD**, que mantendremos a lo largo de toda la legislatura.
...  
Ofrecimos a todas las familias aulas con una ratio inferior a 5 alumnos. Algunas familias, sin embargo, algunas prefirieron permanecer en las aulas donde ya estaban escolarizados; para ellos, garantizamos la libertad de elección con profesores de apoyo adicionales.  
En el curso 2015-2016 se pusieron en marcha las siguientes medidas:  
15 nuevos centros preferentes TGD  
Apoyos y refuerzos de profesorado en los centros preferentes de ESO que tenían 6 o 7 alumnos por grupo TGD y en los centros preferentes de E. Primaria que tenían 7 alumnos.  
Durante el 2015-2016 la Comunidad de Madrid contó con 187 centros de escolarización preferente y 204 “Aulas TEA”: 1 Escuela Infantil, 110 Colegios Públicos de Educación Infantil y Primaria, 32 Institutos de Educación Secundaria y 44 Centros concertados. La Comunidad contaba con 15 nuevos centros de este tipo y 17 nuevas aulas, lo que supuso 85 nuevas plazas y reforzó la presencia de profesores de PT y AL en las aulas con **más de 5 alumnos**, tanto en los centros públicos como en los centros concertados. Estas medidas supusieron la incorporación de 21 profesores en la escuela pública y 12 en la escuela concertada, con una inversión de más de 1.200.000 euros al año.

Información de Cantabria (gracias a Luis Ruiz)

[Orden ECD/96/2015, de 10 de agosto, por la que se dictan instrucciones para la implantación de la Educación Secundaria Obligatoria en la Comunidad Autónoma de Cantabria.](https://boc.cantabria.es/boces/verAnuncioAction.do?idAnuBlob=290641)  
> Artículo 2.2: En los grupos en que se escolaricen alumnos con necesidades educativas especiales, el número máximo de alumnos por grupo será de veinticinco en aquellas unidades en las que se los escolarice.

[Orden ECD/97/2015, de 10 de agosto, por la que se dictan instrucciones para la implantación del Bachillerato en la Comunidad Autónoma de Cantabria.](https://boc.cantabria.es/boces/verAnuncioAction.do?idAnuBlob=290638)  
> Artículo 2.1: El número máximo de alumnos en cada uno de los grupos en Bachillerato será de treinta y dos, garantizándose, en todo momento, la continuidad de los alumnos matriculados en el centro.  
Este lı́mite podrá aumentarse hasta treinta y cinco si las necesidades de escolarización ası́ lo requieren. Se tendrá en cuenta, además, que el número de alumnos por grupo al que se refiere este apartado se reducirá en uno por cada alumno con necesidades educativas especiales.

[Normativa de aulas de enlace en Madrid](https://www.educa2.madrid.org/web/direcciones-de-area/aulas-de-enlace)

[Instrucciones Aulas de enlace 2008-2009 Madrid](https://www.educa2.madrid.org/web/educamadrid/principal/files/6774b6ef-551a-47bf-b32b-2306a378cc57/Instrucciones%202008-2009.pdf?t=1539863501341)  
> Decimotercera. Número de alumnos por Aula de Enlace.  
Cada Aula de Enlace contará con un máximo de doce alumnos y un mínimo de cinco.  

### Ratios en orientación

Un orientador en un centro puede tener todos los alumnos del centro, que pueden ser cercano a los 1000 alumnos

**29 marzo 2011**  
[La ratio orientador-alumno, lejos de las recomendaciones internacionales](https://www.magisnet.com/2011/03/la-ratio-orientador-alumno-lejos-de-las-recomendaciones-internacionales/)

**6 junio 2015**
[Comunicado sobre la recomendación de ratio de alumnado por profesional de la orientación COPOE (CONFEDERACIÓN DE ORGANIZACIONES DE PSICOPEDAGOGÍA Y ORIENTACIÓN DE ESPAÑA.)](https://copoe.org/noticias/item/263-comunicado-de-copoe-sobre-la-recomendacion-de-ratio-de-alumnado-por-profesional-de-la-orientacion)  
> La Asociación Americana de Orientadores Escolares (American School Counselor Association, ASCA) recomienda una ratio de **1 orientador para cada 250 alumnos** en su informe "Comprehensive School Counseling Programs" de 2012. La misma recomendación figura desde 1988 y en todas las revisiones de dicho informe (1993, 1997 y 2005):  *"To achieve maximum program effectiveness, the American School Counselor Association recommends a professional school-counselor-to-student ratio of 1:250 and that professional school counselors spend 80 percent or more of their time in direct and indirect services to students".*  Traducción:  *"Para alcanzar una efectividad máxima en los programas, ASCA recomienda una ratio de un orientador escolar por cada 250 alumnos y que los profesionales de la orientación escolar dediquen al menos un 80% de su tiempo a la atención directa e indirecta a los alumnos”.*  
* La Asociación Internacional para la Orientación Educativa y Profesional (AIOEP) a la que pertenece COPOE, organizó el Symposio Internacional de la IAEVG en Vancouver en 2001 donde se hizo pública la necesidad de que la **ratio de orientador por alumnado sea de 1:250**. En la Conferencia organizada por la misma entidad en Ciudad del Cabo en 2011, vuelve a plantearse este mismo objetivo.  
* En el informe de Harris, Belinda (2013),  *International school-based counselling scoping report* . Counselling Minded, August 2013, realizado desde Reino Unido, se compara la ratio en todo el mundo, entre otros factores. Al parecer, **la mejor sería la de Finlandia, 1:245**.  
* En el informe del Ministerio de Educación editado en 2011, denominado “Los procesos de cambio de las políticas públicas sobre orientación y apoyo a la escuela: Análisis comparado de sistemas vigentes y emergentes. Estudio múltiple de casos en una muestra de Comunidades Autónomas”, se recoge en tres ocasiones que la UNESCO recomienda una ratio de un orientador para cada 250 alumnos.

El documento citado se puede consultar en  
[Los procesos de cambio de las políticas públicas sobre orientación y apoyo a la escuela: análisis comparado de sistemas vigentes y emergentes. Estudio múltiple de casos en una muestra de comunidades autónomas ](https://www.libreria.educacion.gob.es/libro/los-procesos-de-cambio-de-las-politicas-publicas-sobre-orientacion-y-apoyo-a-la-escuela-analisis-comparado-de-sistemas-vigentes-y-emergentes-estudio-multiple-de-casos-en-una-muestra-de-comunidades-autonomas_176722/)  
[Los procesos de cambio de las políticas públicas sobre orientación y apoyo a la escuela: análisis comparado de sistemas vigentes y emergentes. Estudio múltiple de casos en una muestra de comunidades autónomas](https://www.researchgate.net/publication/260390417_Los_procesos_de_cambio_de_las_politicas_publicas_sobre_orientacion_y_apoyo_a_la_escuela_analisis_comparado_de_sistemas_vigentes_y_emergentes_Estudio_multiple_de_casos_en_una_muestra_de_comunidades_aut)

**23 julio 2018**  
[Unidos Podemos quiere un orientador educativo por cada 250 alumnos de centros públicos](https://www.europapress.es/sociedad/educacion-00468/noticia-unidos-podemos-quiere-orientador-educativo-cada-250-alumnos-centros-publicos-20180723170356.html)

**20 febrero 2019**  
[Los orientadores de los institutos atienden cuatro veces más alumnos de lo recomendado por la Unesco](https://elpais.com/sociedad/2019/02/20/actualidad/1550677178_441380.html)  
> Los profesionales critican los recortes de la Administración y aseguran estar "desbordados" por la cantidad de tareas que deben asumir, además de la orientación académica 

### Ratios en codocencia

Las ratios se fijan en normativa por unidad asumiendo implícitamente un docente por unidad y aula.  
Anque se implante codocencia, con varios docentes al tiempo en la misma unidad y aula, la normativa no indica que la ratio por unidad pueda aumentar. De hecho eso es lo que ocurre en los desdobles, que hay dos docentes en la misma sesión para la misma unidad.  
Si la administración quiere alegar que sí puede aumentar la ratio en codocencia al ser dos unidades juntas en el mismo aula, entonces no es codocencia, sino unión de espacios para dos unidades.

No he localizado normativa sobre codocencia. Los ejemplos que se pueden buscar de codocencia se mencionan como "fusionar clases"  
[Cómo llevar a la práctica la codocencia](https://www.escuela21.org/blog/como-llevar-a-la-practica-la-codocencia/)  
> Diego y Ana docentes en primaria en el colegio Público Ramiro Solans de Zaragoza, nos comparten en este vídeo producido por Escuela21, su experiencia única, **fusionando clases** y uniendo fuerzas para proporcionar a los estudiantes una educación más rica y equitativa.   

### Ratios en admisión

Lo comento inicialmene: hay que tener en cuenta la diferencia entre normativa de número de alumnos máximo por grupo y regulación que indica vacantes ofertadas en en el proceso de admisión. Si en el proceso de admisión se limita el número pero no se limita a la hora de formar grupos, no es una limitación real.  
En Madrid se vende que bajan las ratios pero realmente bajan la ratio en el proceso de admisión, sin modificar la ratio máxima real en cada aula.  

[RESOLUCIÓN CONJUNTA DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA POR LA QUE SE MODIFICA LA RESOLUCIÓN DE 25 DE NOVIEMBRE DE 2022 POR LA QUE SE DICTAN INSTRUCCIONES SOBRE LA PARTICIPACIÓN EN EL PROCESO DE ADMISIÓN DE ALUMNOS EN CENTROS DOCENTES SOSTENIDOS CON FONDOS PÚBLICOS QUE IMPARTEN SEGUNDO CICLO DE EDUCACIÓN INFANTIL, EDUCACIÓN PRIMARIA, EDUCACIÓN ESPECIAL, EDUCACIÓN SECUNDARIA OBLIGATORIA Y BACHILLERATO DE LA COMUNIDAD DE MADRID PARA EL CURSO 2023/2024](https://www.comunidad.madrid/transparencia/sites/default/files/regulation/documents/modificacion_resolucion_conjunta_admision_regimen_general_2023-2024.pdf)  

> En el caso de los niveles de Educación Infantil de 3 y de 4 años, la **oferta inicial**
de plazas escolares se calculará aplicando la ratio de 20 alumnos por grupo.  

> En el caso del primer curso de Educación Secundaria Obligatoria, la **oferta inicial**
de plazas escolares se calculará aplicando la ratio de 25 alumnos por grupo.

[twitter FiQuiPedia/status/1808455433327165711](https://x.com/FiQuiPedia/status/1808455433327165711)  
Miente @educacmadrid al decir que baja ratio de 30 a 25 en 1ESO y 2ESO: baja oferta inicial de plazas en admisión a 25 pero no baja máximo que normativa sigue fijando en 30. Un contraejemplo basta ver la falsedad: en mi centro todos 1ESO EMPIEZAN a 27.

[RESOLUCIÓN de 22 de noviembre de 2024, de la Viceconsejería de Política y Organización Educativa, por la que se dictan instrucciones sobre la participación en el proceso de admisión de alumnos en centros docentes sostenidos con fondos públicos que imparten segundo ciclo de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria, Bachillerato, así como las etapas de Educación Especial en la Comunidad de Madrid para el curso 2025-2026](https://www.bocm.es/boletin/CM_Orden_BOCM/2024/12/13/BOCM-20241213-26.PDF)  
> En el primer, segundo y tercer curso de Educación Secundaria Obligatoria, la **oferta inicial** de plazas escolares se calculará aplicando la ratio de 25 alumnos por grupo. 

### Ratios en CRA

El entorno rural tiene normativa específica en LOE

[Artículo 82. Igualdad de oportunidades en el ámbito rural.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a82)  
> 1. Las Administraciones educativas prestarán especial atención a los centros educativos en el ámbito rural, considerando las peculiaridades de su entorno educativo y la necesidad de favorecer la permanencia en el sistema educativo del alumnado de las zonas rurales más allá de la enseñanza básica. A tal efecto, **las Administraciones educativas tendrán en cuenta el carácter específico de la escuela rural proporcionándole los medios y sistemas organizativos necesarios para atender a sus necesidades particulares y garantizar la igualdad de oportunidades**.
> 2. En la educación primaria, las Administraciones educativas garantizarán a todos los alumnos un puesto escolar gratuito en su propio municipio o zona de escolarización establecida.  
Sin perjuicio de lo dispuesto en el párrafo anterior, en la educación básica, en aquellas zonas rurales en que se considere aconsejable, se podrá escolarizar a los niños en un municipio próximo al de su residencia para garantizar la calidad de la enseñanza. En este supuesto las Administraciones educativas prestarán de forma gratuita los servicios escolares de transporte y, en su caso, comedor e internado.

Aparte de la ratio, está el tema de cuándo se abre o se cierra un CRA, y qué recursos asociados tienen.

Los Colegios Rurales Agrupados están asociados a infantil y primaria, y a veces hay normativa bajo ese epígrafe sin ser solamente de CRA, sino otros agrupamientos.

[NORMATIVA DE LAS ESCUELAS INFANTILES - sereducadorashoy](http://sereducadorashoy.blogspot.com/2013/01/normativa-de-las-escuelas-infantiles.html)

**Andalucía**  
[DECRETO 149/2009, de 12 de mayo, por el que se regulan los centros que imparten el primer ciclo de la educación infantil.](http://www.waece.org/contenido/Requisitos%20Minimos/andalucia%20decreto%20149-2009.pdf)  
>Artículo 14. Ratio.  
>1. Los centros educativos que impartan el primer ciclo de la educación infantil tendrán como máximo el siguiente número de niños y niñas por unidad:  
a) Unidades para niños y niñas menores de un año: 1/8.  
b) Unidades para niños y niñas de uno a dos años: 1/13.  
c) Unidades para niños y niñas de dos a tres años: 1/20.  
>2. La Consejería competente en materia de educación determinará el número máximo de alumnos y alumnas para las unidades que integren niños y niñas con necesidades específicas de apoyo educativo o trastorno del desarrollo.  
...  
Disposición adicional primera. Centros educativos que atiendan a poblaciones de especiales características.  
...  
3. De conformidad con lo establecido en el apartado anterior, podrán crearse o autorizarse centros de primer ciclo de educación infantil con un número de unidades adecuado a la población que deba escolarizarse en este ciclo, teniendo en cuenta lo dispuesto sobre relación máxima alumnado por unidad escolar en el artículo 14. Estas unidades podrán agrupar niños y niñas de este ciclo de edades diferentes, en cuyo caso **el número máximo de niños y niñas por unidad escolar será de 15**.  

Aquí citan además art157 LOE y art87, citan instrucción 18 febrero 2019 sobre CRA

[https://twitter.com/GranadaUstea/status/1179422476666884097](https://twitter.com/GranadaUstea/status/1179422476666884097)  
Todo lo que hay que saber sobre ratios en Andalucia #StopRatiosIlegales

[Instrucciones de 18 de febrero de 2019, de la Viceconsejería, sobre los procedimientos de admisión y matriculación del alumnado en los centros docentes sostenidos con fondos públicos para el curso escolar 2019/20.](https://www.juntadeandalucia.es/educacion/portals/web/ced/normativa/-/normativas/detalle/instrucciones-de-18-de-febrero-de-2019-de-la-viceconsejeria-sobre-los-procedimientos-de-admision-y-matriculacion-del)  
> Por razones de organización y de conformidad con lo que establece el artículo 4.3 del Decreto
40/2011, de 22 de febrero, se podrá atender en una misma unidad alumnado de diferentes cursos de
un mismo ciclo o etapa educativa, en cuyo caso, el **número máximo de alumnos y alumnas por unidad será de quince**. En el supuesto de alumnado de ciclos o etapas distintos en una misma unidad, dicho **número se reducirá a doce**.

**25 septiembre 2019**  
['La Junta suprimirá a corto plazo todos los colegios rurales en Andalucía', denuncian las familias](https://cadenaser.com/emisora/2019/09/25/radio_granada/1569404274_562103.html)  
> El delegado de Educación niega la mayor y desmiente 'tajantemente' que su departamento haya trasladado esos planes a las familias afectadas  
También niega que exista ningún problema con los colegios en la Alpujarra ya que se cumplen las ratios de mezcla de alumnos de distintos niveles por clase  
Los padres se quedan de que la situación es insostenible, pero la administración considera que el número de alumnos por aula en los colegios rurales sigue por debajo de los 14, máximo establecido por la norma en vigor.

**Castilla-La Mancha**  
[Nube JCCM, documentación CRA](https://nube.castillalamancha.es/index.php/s/4c0s2pfsRJ5o4ql#pdfviewer)  

Orden de 25 de marzo de 2009,de la Consejería de Educación y Ciencia, por la que se regulan los procesos de constitución, modificación y supresión de Colegios Rurales Agrupados de Educación Infantil y Primaria, y las adscripciones y desplazamientos de los maestros y maestras con destino definitivo en estos Centros que se produzcan como consecuencia de dichos procesos.

>Artículo 4.  
El expediente para la constitución de nuevos Colegios Rurales Agrupados se iniciará a propuesta de la correspondiente Delegación Provincial de la Consejería competente en materia de educación o a instancia del Consejo Escolar de uno o varios de los colegios públicos preexistentes, siempre y cuando concurran las siguientes circunstancias:  
>1. Que el número de localidades implicadas sea tres como mínimo y ocho como máximo.  
>2. Que el número de unidades resultante en cada CRA sea de al menos seis para que puedan contar con equipo directivo completo.  
>3. Que la distancia máxima de una localidad respecto del domicilio del CRA no sea superior a 30 kms.  
>4. Los centros completos de nueve o más unidades no podrán formar parte de la composición de un CRA.  
...  
Artículo 7.  
El expediente deberá contener, además, datos relativos a:  
a)Número de unidades y número de alumnos de cada una de las unidades a agrupar.  
...  
Artículo 11.  
La Consejería competente en materia de educación, previo informe motivado por parte de la Delegación Provincial, podrá acordar la supresión de un CRA, cuando el número de alumnos escolarizados aconsejen su funcionamiento como centros independientes o cuando concurran otras circunstancias que impidan la consecución de sus objetivos. Igualmente, podrá acordar la segregación de alguna de sus secciones para integrarse en un CRA nuevo o en otro preexistente  

**Comunidad Valenciana**  
[ORDEN de 15 de mayo de 1997, de la Conselleria de Cultura, Educación y Ciencia, por la que se regula la constitución de colegios rurales agrupados de Educación Infantil y Primaria en la Comunidad Valenciana.](http://www.dogv.gva.es/datos/1997/07/04/pdf/1997_7878.pdf)  
>Noveno  
l. Para iniciar un expediente de constitución de un CRA será necesario que concurran las siguientes circunstancias:  
a) Que el número máximo de localidades implicadas no sea superior a seis.  
b) Que el número máximo de unidades a integrar no sea superior a dieciocho ni inferior a cinco.  
c) Que la distancia máxima entre las localidades más alejadas no sea superior a 30 km.  
d) Que la distancia máxima de una localidad respecto del domicilio del CRA no sea superior a 20 km.  
>2. Excepcionalmente podrá iniciarse un expediente de constitución de un CRA, aunque no concurran las circunstancias señaladas en el punto anterior, cuando se considere necesario para que los centros puedan superar su situación de aislamiento o para conseguir los objetivos previstos en la LOGSE.  

**Galicia**  
[Decreto 329/2005, de 28 de julio, por el que se regulan los centros de menores y los centros de atención a la infancia](http://www.waece.org/contenido/Requisitos%20Minimos/Galicia%20decreto329.pdf)  
>2. Ratios y requisitos específicos de personal.  
a) Ratios.  
La proporción adulto/niño/a será, como máximo, la siguiente:  
-Unidades para niños/as menores de 1 años: 1/8.  
-Unidades para niños/as de1a2 años: 1/13.  
-Unidades para niños/as de2a3 años: 1/20.  
En el caso de no existir demanda suficiente para formar uno o varios grupos del mismo nivel de edad,
los/as niños/as podrán ser agrupados conforme a la siguiente proporción:  
-Grupos formados por niños/as de hasta 2 años de edad: 1/10.  
-Grupos formados por niños/as de edades comprendidas enel tramo 0-3: 1/15.  
b) Titulacióndel personal.  
El personal dedicado a funciones educativas y de atención directa a los/as niños/as deberá estar en posesión de las siguientes titulaciones:  
b1) Personal de atención:  
-Licenciado/a en pedagogía o psicopedagogía.  
-Maestro/a especialista eneducacióninfantil o equivalente.  
-Técnico superior en educación infantil o equivalente.  
La proporción de personal cualificado con la que deberán contar los centros es de un número igual
al de unidades en funcionamiento más uno.  
La dirección pedagógica del centro recaerá en alguno de los miembros del personal con la titulación
de licenciado/a o maestro/a especialista en educación infantil o equivalente.  
b2) Personal de apoyo:  
En todos los centros deberá haber, por lo menos, una persona de apoyo a la atención y cuidado delos/as niños/as la cual, además de las titulaciones anteriores, podrá estar en posesión de alguna de las* *siguientes titulaciones: técnico en atención sociosanitaria, técnico superior en animación sociocultural, técnico en cuidados auxiliares de enfermería, diplomado/a en puericultura reconocido por la Consellería de Sanidad o aquellas otras reconocidas como apropiadas por el órgano competente en la autorización del centro.*

**Propuesta de ratios en CRA de CCOO**  
[Programas. Escuela Rural](http://www.feccoo-madrid.org/a44434d5c8f269ea6a68db2e306700e9000063.pdf)

### Ratios en actividades complementarias y extraescolares

#### Ratios de docentes acompañantes

Es normal plantearse cuántos profesores deben ir en función del número de alumnos si hay una salida de actividad complementaria o se organizan actividades extraescolares por las tardes que no son propiamente lectivas.

La idea esencial es que (hasta donde sé) no es normativa general, sino que forma parte de la normativa de cada centro, ya que lo fija el Consejo Escolar y/o el Departamento de Extrescolares.

Para centros privados, en LODE

[Artículo cincuenta y siete.](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978#acincuentaysiete)  
> Corresponde al Consejo Escolar del centro, en el marco de los principios establecidos en esta Ley:  
...  
j) Informar los criterios sobre la participación del centro en actividades culturales, deportivas y recreativas, así como en aquellas acciones asistenciales a las que el centro pudiera prestar su colaboración.  

En [Artículo 127. Competencias del Consejo Escolar.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a127) se indica  
> b) Aprobar y evaluar la **programación general anual** del centro, sin perjuicio de las competencias del Claustro del profesorado en relación con la planificación y organización docente.  

En [Artículo 50. Programación general anual.](https://www.boe.es/buscar/act.php?id=BOE-A-1996-3689#a5-2)  
>2. La programación general anual incluirá:  
...  
d) El programa anual de actividades extraescolares y servicios complementarios" y si una actividad no está debe aprobarse por Consejo Escolar

[Programa anual de actividades complementarias y extraescolares (Orden de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la organización y funcionamiento de los institutos de Educación Secundaria.)](https://www.boe.es/buscar/act.php?id=BOE-A-1994-15565#programaanualdeactividadescomplementariasyextraescolares)

[Artículo 40. Órganos de coordinación.(Real Decreto 83/1996, de 26 de enero, por el que se aprueba el Reglamento orgánico de los institutos de educación secundaria.)](https://www.boe.es/buscar/act.php?id=BOE-A-1996-3834#a40)  
> En los institutos de educación secundaria existirán los siguientes órganos de coordinación docente:  
a) Departamento de orientación y departamento de actividades complementarias y extraescolares.

Lo que se fija a nivel de centro son criterios como por ejemplo qué % de cada clase o qué % de cada nivel debe participar para que se autorice la actividad evitando que se realicen participando pocos y queden muchos alumnos sin participar.

Mirando ejemplos (por ver si hay alguna referencia más concreta en normativa general)

[INSTRUCCIÓN nº 23/2014, DE LA SECRETARÍA GENERAL DE EDUCACIÓN, SOBRE ACTIVIDADES COMPLEMENTARIAS Y EXTRAESCOLARES ORGANIZADAS POR LOS CENTROS DOCENTES SOSTENIDOS CON FONDOS PÚBLICOS DE LA COMUNIDAD AUTÓNOMA DE EXTREMADURA.](https://www.educarex.es/pub/cont/com/0047/documentos/Instruccion_Actividades_Complementarias_y_Extraescolares_23_09_2014.pdf)  
> Las diferentes características de la actividad a realizar y del grupo al que esté dirigida determinarán, para cada caso concreto, la relación adecuada entre docentes y alumnado. A este respecto se fija con carácter general una distribución aproximada de **un docente por cada 10 alumnos o alumnas en Educación Infantil, 15 en Educación Primaria y 20 en Educación Secundaria Obligatoria. Para la educación secundaria postobligatoria, se establece una ratio aproximada de un docente por cada 25 alumnos o alumnas**. No obstante lo anterior, cuando las circunstancias lo aconsejen por la naturaleza de la actividad o por la condición de los participantes, la Dirección del centro podrá señalar ratios inferiores previa autorización del Servicio de Inspección Educativa.

[NORMATIVA PARA LA REALIZACIÓN DE ACTIVIDADES COMPLEMENTARIAS Y EXTRAESCOLARES (IES Cañada Real)](https://www.educa2.madrid.org/web/centro.ies.canadareal.galapagar/complementarias-y-extraescolares)  
> 10. Se ruega el mínimo número de profesores acompañantes para perjudicar lo menos posible la actividad lectiva diaria; de forma generalizada, se establecerá una **media de 20 alumnos por profesor (depende también de la edad del alumnado)**.

Ejemplo en IES Salvador Dalí Leganés (2017-2018, enlace original ya no disponible)  
>El número de profesores acompañantes dependerá del número de alumnos que participen y de las características de la actividad: por cada veinte alumnos irá un profesor.  
...  
Alumnos de Bachillerato: un profesor por cada veinte alumnos o fracción. 

Ejemplo e IES Las Américas (2017-2018, enlace original ya no operativo)  
> Con carácter general se establece que la ratio para realizar las salidas extraescolares o complementarias debe ser de un profesor/a acompañante del centro por cada 20 alumnos/as, con un mínimo de dos acompañantes, incluyendo aquellos que aporten los centros o instituciones con quienes se colabore en la realización de la actividad. ; salvo que sean alumnos de primer ciclo y viajen en transporte público, o la actividad tenga unas condiciones especiales que entonces se podrá rebajar el número de alumnos por profesor a 15, previa autorización de Jefatura de Estudios.  
En aquellas actividades extraescolares especiales que impliquen pernocta, sin acompañamiento de otros centros educativos y con participación de al menos 25 alumnos, habrá un tercer acompañante (profesorado, guía, monitor, padres,...), con cargo al presupuesto de la actividad.

#### Ratios que permiten las actividades

Se comentó con el aumento de 20% de ratio de RDL14/2012. En las actividades suelen tener unos ratios de alumnos, por ejemplo limitar a grupos de 25, de 30. Cuando se aumentó esa ratio en educación, las actividades de museos no modificaron ese límite. Hay situaciones en las que no es posible ir con el grupo entero.

Por ejemplo, curso 2022-2023, en talleres MUNCYT

[Actividades, oferta escolar](https://www.muncyt.es/sites/default/files/2023-02/Oferta-Escolar2022-2023-Alcobendas_r.pdf)

> Bachillerato – CFGM Todos los cursos Big Data (máximo 25 participantes)

> Taller... (Máximo 35 participantes, incluyendo responsables de grupo)
> Planetario ...(Máximo 35 participantes, incluyendo responsables de grupo)

Aunque se oferta para Bachillerato, no puede ir cualquier grupo de Bachillerato.

### Ratios en comedores escolares

No es docente-alumno sino "monitor/cuidador"-alumno

En Madrid regulado desde 2002 (se puede ver que la regulación tiene modificaciones pero no en la ratio)

[Orden 917/2002, de 14 de marzo, de la Consejería de Educación, por la que se regulan los comedores colectivos escolares en los centros docentes públicos no universitarios de la Comunidad de Madrid](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=3218&cdestado=P#no-back-button)  
> Artículo 11.- Dotación de personal de atención educativa, apoyo y vigilancia  
Las dotaciones de personal para atención, apoyo y vigilancia a los alumnos en el servicio de comedor se atendrán a los siguientes módulos:  
Una persona por cada:  
a) Treinta alumnos o fracción superior a quince en Educación Primaria y en Educación Secundaria, en su caso.  
b) Veinte alumnos o fracción superior a diez en Educación Infantil de cuatro y cinco años y en Educación Especial.  
c) Quince alumnos o fracción superior a ocho en Educación Infantil de tres años.  
d) En los centros de Educación Especial se podrá adecuar, por el Director de Área Territorial, estos módulos en atención a los factores específicos que puedan concurrir en dichos centros.  
En cualquier caso la dotación del personal de atención educativa, apoyo y vigilancia deberá garantizar la adecuada atención del alumnado.

En Castilla-La Mancha regulado en [Decreto 138/2012, de 11/10/2012, por el que se regula la organización y funcionamiento del servicio de comedor escolar de los centros docentes públicos de enseñanza no universitaria dependientes de la Consejería deEducación, Cultura y Deportes de Castilla-La Mancha.](http://www.educa.jccm.es/es/normativa/decreto-138-2012-11-10-2012)  
>Artículo 15. Personal docente y no docente encargado de la atención al alumnado  
...  
>4. Las dotaciones de personal de vigilancia se determinarán en función del número de alumnado usuario del servicio
de comedor escolar autorizado, de acuerdo con la siguiente ratio:  
a) Un vigilante por cada 35 alumnos o alumnas o fracción superior a 15, en Educación Secundaria Obligatoria.  
b) Un vigilante cuidador por cada 30 alumnos o alumnas o fracción superior a 15 en Educación Primaria.  
c) Un vigilante por cada 15 alumnos o alumnas o fracción superior a 10, en los cursos de 2º y 3º de Educación Infantil.  
d) Un vigilante por cada 10 alumnos o fracción superior a 7, en el primer curso de Educación Infantil.  
En el caso del alumnado con necesidades educativas especiales, así como en aquellos otros casos en que motivadamente se justifique, la Dirección General competente en materia de comedores escolares adecuará la ratio en función de las necesidades.  

### Ratios en educación a distancia/no presencial

La idea es es ver hay normativa sobre límite de alumnos por tutor en cursos online (UNED, cursos de CTIF y CRIF online).
Habría que ver en cada caso si es normativa o si son datos estadísticos, y si son casos totalmente a distancia o semipresenciales, con tutorías presenciales
Con el tema de COVID-19 este tema tiene mayor importancia, pero no se había planteado educación no presencial en educación obligatoria.
Sin ser normativa, se citan ideas también en post [Ratios en educación: estadísticas y realidad](https://algoquedaquedecir.blogspot.com/2019/04/ratios-en-educacion-estadisticas-realidad.html)

[Real Decreto 420/2015, de 29 de mayo, de creación, reconocimiento, autorización y acreditación de universidades y centros universitarios.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-6708#a7)
> 2. El número total de miembros del personal docente e investigador en cada universidad no podrá ser inferior al que resulte de aplicar la relación 1/25 respecto al número total de alumnos matriculados en enseñanzas universitarias de carácter oficial. Esta ratio se entenderá referida a personal docente e investigador computado en régimen de dedicación a tiempo completo o su equivalente a tiempo parcial.  
La ratio podrá modularse cuando la universidad imparta enseñanzas en la modalidad no presencial, pudiendo oscilar entre 1/50 y 1/100 en función del nivel de experimentalidad de las titulaciones y de la mayor o menor semipresencialidad.

**24 abril 2015**  
[La UNED reestructura sus Campus académicos para conseguir una ordenación más equilibrada y eficiente](http://portal.uned.es/portal/page?_pageid=93,48681276&_dad=portal&_schema=PORTAL)  
> Ratios tutores-estudiantes desiguales  
Académicamente, el modelo también presentaba carencias significativas, sobre todo en lo que a ratio tutor-alumno se refiere. Había campus que contaban con 9.800 estudiantes y 450 tutores (una ratio de 21'77 alumnos por tutor) y campus con 30.000 estudiantes y 800 tutores (un tutor para cada 37’5 alumnos). La nueva organización se ha establecido considerando también el número de estudiantes y tutores en cada campus e intentando equilibrar las cifras.

### Normativa requisitos centros y seguridad (superficie por alumno)

Hay otra condición para un valor máximo: la seguridad en cuanto a incendios y estructural del edificio; a nadie se le ocurriría meter en una clase de un edificio de madera a 50 alumnos si el suelo cruje cuando entran 10, y si no hay un plan de evacuación que contemple más de 10 alumnos.

La normativa lo refleja, es algo que miré en su momento [Inventario metros cuadrados por alumno en aulas: Idea para ilegalizar el aumento de ratios mostrando que el hacinamiento tiene riesgos e incumple otras leyes de seguridad (incendios...)](http://www.docentesconeducacion.es/viewtopic.php?f=42&t=781)

[Real Decreto 132/2010, de 12 de febrero, por el que se establecen los requisitos mínimos de los centros que impartan las enseñanzas del segundo ciclo de la educación infantil, la educación primaria y la educación secundaria.](http://www.boe.es/buscar/act.php?id=BOE-A-2010-4132)  
> En este sentido, se remite la regulación de los aspectos técnicos relacionados con los requisitos que deben reunir las instalaciones docentes a lo establecido en el Real Decreto 314/2006, de 17 de marzo, por el que se aprueba el Código Técnico de Edificación, donde se establecen las normas técnicas relativas a la seguridad estructural, la seguridad de utilización, la salubridad, la protección frente al ruido, el ahorro de energía y la seguridad en caso de incendio, fijando, entre otros requisitos, una ocupación de 2 metros cuadrados por persona en aulas infantiles, 1,5 metros cuadrados por persona en el resto de las aulas, y 5 metros cuadrados por persona en los espacios diferentes a las aulas como laboratorios, talleres, gimnasios, salas de dibujo, etc.  

[TÍTULO III De los centros de educación primaria.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#tiii)  
[Artículo 10. Instalaciones y condiciones materiales de los centros.](http://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a10)  
> a) Un aula por cada unidad con una superficie adecuada al número de alumnos escolarizados autorizados y en todo caso, con un mínimo de 1,5 metros cuadrados por puesto escolar.  
...  
[Artículo 14. Instalaciones y condiciones materiales de los centros que imparten educación secundaria obligatoria.](http://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a14)  
> a) Un aula por cada unidad con una superficie adecuada al número de alumnos escolarizados autorizados y en todo caso, con un mínimo de 1,5 metros cuadrados por puesto escolar.  
...  
[Artículo 15. Instalaciones y condiciones materiales de los centros que imparten bachillerato.](http://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a15)  
> 1. Los centros en los que se imparta bachillerato deberán disponer, como mínimo, de un aula por cada unidad con una superficie adecuada al número de alumnos scolarizados y en todo caso, con un mínimo de 1,5 metros cuadrados por puesto escolar.

[RD 314/2006-Código Técnico de Edificación](http://www.boe.es/buscar/act.php?id=BOE-A-2006-5515) que citan tiene un [anexo de 952 páginas](http://www.boe.es/boe/dias/2006/03/28/pdfs/C00001-00952.pdf), y en página 855  
[Documento Básico SI Seguridad en caso de incendio, 2 Cálculo de la ocupación](https://www.boe.es/boe/dias/2006/03/28/pdfs/C00001-00952.pdf#page=852)  
> 1 Para calcular la ocupación deben tomarse los valores de densidad de ocupación que se indican en la tabla 2.1 en función de la superficie útil de cada zona, **salvo cuando sea previsible una ocupación mayor o bien cuando sea exigible una ocupación menor en aplicación de alguna disposición legal de obligado cumplimiento**, como puede ser en el caso de establecimientos hoteleros, docentes, hospitales, etc. En aquellos recintos o zonas no incluidos en la tabla se deben aplicar los valores correspondientes a los que sean más asimilables.  
...  
Tabla 2.1 Densidades de ocupación, para uso Docente
|Uso previsto | Zona, tipo de actividad | Ocupación (m²/persona) |
|:-:|:-:|:-:|
| Docente | Conjunto de la planta o del edificio| 10 |
| Docente | Locales diferentes de aulas, como  **laboratorios** , talleres, gimnasios, salas de dibujo, etc. | 5 |
| Docente | Aulas (excepto de escuelas infantiles) | 1,5| 
| Docente | Aulas de escuelas infantiles y salas de lectura de bibliotecas | 2 |

Un cálculo sencillo de superficies de aula mínimas asociadas a máximos de alumnos por aula  
* Primaria: el máximo de 25 alumnos por aula a 1,5 m² por alumno implica que el aula debe tener por lo menos 37,5 m².
* Secundaria: el máximo de 30 alumnos por aula a 1,5 m² por alumno implica que el aula debe tener por lo menos 45 m².
* Bachillerato: el máximo de 35 alumnos por aula a 1,5 m² por alumno implica que el aula debe tener por lo menos 52,5 m².
* Talleres, laboratorios, gimnasios: ... para 25 alumnos suponen 125 m², para 35 alumnos 175 m²... 

La frase que marcada en negrita me temo que permite, según el código de edificación, que se legisle una ocupación mayor a la que indica esa tabla: vamos, que en el Real Decreto-Ley 14/2012 podrían haberlo aumentado, pero no lo hicieron, no solamente porque no cayeron en ello, sino por el hecho de que, para hacer recortes, se legisle una modificación de la ocupación para saltarse los valores que lleva indicando tiempo el código técnico de edificación, empezaría a cantar más de la cuenta y mostraría hacinamiento.

Intento mostrar también una visión histórica similar a la anterior, por citar referencias, porque no hay variación con el tiempo

| Normativa general | [LGE](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24172) <br> (1970-1990) art 56 | [LOGSE](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24172) <br> (1990-2006)  Art 34 | [LOE/LOMCE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899) <br> (2006-actualidad)|
|:-:|:-:|:-:|
|Normativa específica estatal / autonómica | -[RD 1004/1991](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16419) | -[RD 777/1998 (Anexo V, REQUISITOS MÍNIMOS DE ESPACIOS FORMATIVOS](https://www.boe.es/buscar/doc.php?id=BOE-A-1998-10720)) <br> -[RD 1537/2003](https://www.boe.es/buscar/doc.php?id=BOE-A-2003-22599) <br> [RD 314/2006](http://www.boe.es/buscar/act.php?id=BOE-A-2006-5515) (Tabla 2.1 densidad ocupación para uso docente) |
| Infantil (1er ciclo 0-3) | 2 m² (Art 10) | 2 m² (Art 9) | 2 m² |
| Infantil (2º ciclo 3-6) | 2 m² (Art 11) | 2 m² (Art 9) | 2 m² |
| Primaria | 1,5 m² (Art 20) | 1,5 m² (Art 13) | 1,5 m² |
| Secundaria | 1,5 m² (Art 17) | 1,5 m² <br> 5 m² laboratorio | | 
|Diversificación / PMAR | 1,5 m² | | |
| Bachillerato | 1,5 m² (Art 18) | 1,5 m² | | 
| GS/PCPI/FPB| | | |
| FP GM | Ver anexo V | 1,5 m² | |
| FP GS | Ver anexo V | 1,5 m² | |
| Educación especial | | |

A nivel histórico también se puede ver normativa en BOE donde se proponían valores modelo para centros en época de proyectos y nuevas construcciones.

Ejemplos:

[Orden de 26 de marzo de 1981, por la que se aprueban los programas de necesidades para la redacción de los proyectos de construcción y adaptación de Centros de Educación Especial.](https://www.boe.es/boe/dias/1981/04/06/pdfs/A07406-07412.pdf)  
Se indican 40 m² por aula

[Orden de 22 de mayo de 1978 sobre fijación de Programas de Necesidades de Centros no estatales de Educación Preescolar y General Básica.](https://www.boe.es/buscar/doc.php?id=BOE-A-1978-14054)  
Se indican 60 m² por aula para 40 alumnos y 30 m² por aula para 20 alumnos

[Orden de 14 de agosto de 1975 por la que su aprueban los programas de necesidades para la redacción de proyectos de Centros de Educación General Básica y de Bachillerato](https://www.boe.es/boe/dias/1975/08/27/pdfs/A18123-18129.pdf)  
Se indican 56 m² por aula

[ORDEN de 14 de agosto de 1975 por la que se aprueba el programa de necesidades para la redacción de proyectos de Centros de Formación Profesional de primero y segundo grados](https://www.boe.es/boe/dias/1975/08/26/pdfs/A18027-18033.pdf)  
Se indican 56 m² por aula

[Orden de 10 de febrero de 1971 (Educación y Ciencia) por la que se aprueba el programa de necesidades docentes para la redacción de proyectos de Centros de Educación General Basica y de Bachillerato.](https://www.boe.es/boe/dias/1971/02/20/pdfs/A02833-02841.pdf)  
Se indican 50 m² por aula

Y hablando de superficie por alumno, no se puede dejar de comparar la regulación existente para granjas de animales [La granja de Wert: alumnos 1,5 m2, menos que los cerdos en granjas](http://www.docentesconeducacion.es/viewtopic.php?f=57&t=943)

Tienen en algunos casos más superficie que los alumnos

[Real Decreto 1135/2002, de 31 de octubre, relativo a las normas mínimas para la protección de cerdos.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-22544)  
>2. La superficie total de suelo libre de la que deberá disponer cada cerda... será, al menos, de 2,25 metros cuadrados...

[Real Decreto 1135/2002, de 31 de octubre, relativo a las normas mínimas para la protección de cerdos.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-22544)  
>...la zona de suelo a disposición de un verraco adulto deberá ser, como mínimo, de 10 metros cuadrados...

En otros casos tienen menos superficie que esos 1,5 m² por alumno

[Real Decreto 3/2002, de 11 de enero, por el que se establecen las normas mínimas de protección de las gallinas ponedoras.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-831)  
> a) De, al menos, 750 centímetros cuadrados de superficie de la jaula por gallina

Comparando, 1,5 m² = 15000 cm², la superficie de un alumno equivale a la de 20 gallinas.

#### Superficies mínimas fuera del aula

Hay situaciones en las que se decide "aprovechar el espacio" y se reutilizan superficies para otros usos, lo que no siempre es correcto.

Ejemplos:

* Hacer un pabellón nuevo con aulas en el patio del centro (por ejemplo, IES El Olivo en Parla en 2019) no es válido si incumple superficie mínima del patio  
* Desmontar un espacio específico (laboratorio, aula de artes, aula de música) para hacer aula convencional no es válido si con ello no se tiene el número de espacios específicos.

Se citan algunas cosas que fija Real Decreto 132/2010:

[Artículo 3. Requisitos de instalaciones comunes a todos los centros.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a3)  
> 3. Los centros docentes que impartan la educación primaria, la educación secundaria obligatoria y/o el bachillerato deberán contar, además con:  
Un patio de recreo, parcialmente cubierto, susceptible de ser utilizado como pista polideportiva, con una superficie adecuada al número de puestos escolares. En ningún caso será inferior 900 metros cuadrados.  
Biblioteca, con una superficie, como mínimo, de 45 metros cuadrados en los centros que impartan la educación primaria, y 75 metros cuadrados en los centros que impartan la educación secundaria obligatoria o el bachillerato.

[Artículo 14. Instalaciones y condiciones materiales de los centros que imparten educación secundaria obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a14)  
Los centros en los que se imparta educación secundaria obligatoria dispondrán, como mínimo, de las siguientes instalaciones:  
a) Un aula por cada unidad con una superficie adecuada al número de alumnos escolarizados autorizados y en todo caso, con un mínimo de 1,5 metros cuadrados por puesto escolar.  
b) Por cada 12 unidades o fracción, un aula taller para tecnologías y dos aulas para las actividades relacionadas con las materias de música y educación plástica y visual respectivamente.  
c) Al menos un laboratorio de Ciencias Experimentales por cada 12 unidades o fracción.  
d) Un espacio por cada ocho unidades para desdoblamiento de grupos y otro para actividades de apoyo y refuerzo pedagógico.

[Artículo 15. Instalaciones y condiciones materiales de los centros que imparten bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a15)  
> 1. Los centros en los que se imparta bachillerato deberán disponer, como mínimo, de un aula por cada unidad con una superficie adecuada al número de alumnos escolarizados y en todo caso, con un mínimo de 1,5 metros cuadrados por puesto escolar.  
> 2. Un espacio por cada cuatro unidades para desdoblamiento de grupos y otro para actividades de apoyo y refuerzo pedagógico.  
> 3. En función de las modalidades del bachillerato impartidas, los centros deberán disponer, asimismo, de las instalaciones siguientes:  
a) Para la modalidad de artes:  
Dos aulas diferenciadas dotadas de las instalaciones adecuadas para la enseñanza de las materias de modalidad cuando se imparta la vía de artes plásticas, imagen y diseño.  
Un aula de música cuando se imparta la vía de artes escénicas, música y danza.  
b) Para la modalidad de Ciencias y Tecnología:  
Tres laboratorios diferenciados de Física, Química y Ciencias.  
Un aula de dibujo.  
Un aula de Tecnología.  

[Artículo 20. Flexibilización de los requisitos de instalaciones para los centros docentes que impartan distintas enseñanzas en el mismo edificio o recinto escolar.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a20)  

### Ratios por COVID-19

Intento reflejar aquí normativa sobre distanciamiento social e implicaciones en ratio si se mantiene presencialidad.

La pandemia supuso combinar distanciamiento, espacio disponible y educación a distancia. 

Con la evolución de información sobre COVID-19 la información y los valores de distancia han ido variando. Ouede haber información sobre propuestas de ratios que no sean normativP oficial, pero sí citan valores de ratios concretos. De momento lo pongo aquí, quizá con tiempo separe en otro post. A veces más que "normativa" son indicaciones sanitarias, que se revisan. De hecho existe el concepto de "ratio" en contagios; puede haber parte en el post [Ratios en educación](http://algoquedaquedecir.blogspot.com/2017/09/ratios-en-educacion.html)

Se citan artículos aunque este post sea sobre normativa porque visualiza como el valor lo define normativa, ellos pueden conseguir que sea legal o no la ratio que quieran. Hay quien dice que para COVID-19 no se pueden modificar ratios por tema competencias o por tema de estar en orgánica, y eso es falso: RDL14/2012 las modificó.

[twitter FiQuiPedia/status/1272260975631179776](https://twitter.com/FiQuiPedia/status/1272260975631179776)  
-Gobierno puede modificar ratios, ver [art2 RDL14/2012](https://boe.es/buscar/act.php?id=BOE-A-2012-5337&b=4&tn=1&p=20120421#a2) ([RDL en art86CE](https://app.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=86&tipo=2))  
-Básica ([art149CE](https://app.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=149&tipo=2)) es obligado cumplimiento CCAA. Ejemplo  
[Artículo 157. Recursos para la mejora de los aprendizajes y apoyo al profesorado.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a157)  
[Disposición final quinta. Título competencial.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta)  
-Orgánica implica %votos ([art81CE](https://app.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=81&tipo=2)), ratio no es.  
[Disposición final séptima. Carácter de Ley Orgánica de la presente Ley.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dfseptima)

Algunas ideas asociadas a prevención de riesgos laborales las cito inicialmente en post sobre [Climatización aulas y la viga de Ohanes](http://algoquedaquedecir.blogspot.com/2018/02/climatizacion-aulas-y-la-viga-de-ohanes.html)

Se puede ver este hilo iniciado el 6 marzo 2020

[twitter FiQuiPedia/status/1236011023116767233](https://twitter.com/FiQuiPedia/status/1236011023116767233)  
Alguien de @SaludMadrid podría aclarar si, "con el conocimiento actual", son "lugares con aglomeración de personas"  
¿Centros educativos con cientos de alumnos?  
¿Aulas con un alumno cada 1,5 m²?  

[Orden SND/370/2020, de 25 de abril, sobre las condiciones en las que deben desarrollarse los desplazamientos por parte de la población infantil durante la situación de crisis sanitaria ocasionada por el COVID-19.](https://boe.es/buscar/act.php?id=BOE-A-2020-4665#a3)  
> 2. Durante el paseo diario deberá mantenerse una distancia interpersonal con terceros de al menos dos metros.

[Orden SND/386/2020, de 3 de mayo, por la que se flexibilizan determinadas restricciones sociales y se determinan las condiciones de desarrollo de la actividad de comercio minorista y de prestación de servicios, así como de las actividades de hostelería y restauración en los territorios menos afectados por la crisis sanitaria ocasionada por el COVID-19.](https://boe.es/buscar/act.php?id=BOE-A-2020-4791#a3)  
> 2. En todo caso, durante estos contactos sociales con terceros no convivientes deberán respetarse las medidas de seguridad e higiene establecidas por las autoridades sanitarias para la prevención del COVID-19, relativas al mantenimiento de una distancia mínima de seguridad de dos metros o estableciendo medidas alternativas de protección física, de higiene de manos y etiqueta respiratoria

**5 mayo 2020**  
[La quimera del colegio de los días alternos: soluciones a la apertura de los centros para la mitad de los alumnos](https://www.elmundo.es/espana/2020/05/05/5eb1bbf321efa0107b8b4573.html)  
El anuncio de Isabel Celaá de que si no hay vacuna sólo la mitad de los alumnos españoles volverá al colegio en septiembre ha provocado estupor en la comunidad educativa.

**7 mayo 2020**  
[El Departamento de Educación comparte con los agentes educativos el retorno a las aulas de mayo](https://www.irekia.euskadi.eus/es/news/61648-departamento-educacion-comparte-con-los-agentes-educativos-retorno-las-aulas-mayo)  
> **Espacios** . Se guardará una distancia de 1,5 metros entre cada puesto escolar.

La consejería de salud indicaba 2 metros

[Dos metros, la distancia que nos une. OsakidetzaEJGV](https://www.youtube.com/watch?v=OYC97Pl1LDo)

[Recomendaciones para prevenir la transmisión del coronavirus en Euskadi](https://www.euskadi.eus/recomendaciones-para-prevenir-la-transmision-del-coronavirus/web01-a2korona/es/)

**10 mayo 2020**  
[Consideraciones para las medidas de salud pública relativas a las escuelas en el contexto de la COVID-19 Anexo del documento Consideraciones relativas a los ajustes de las medidas de salud pública y sociales en el contexto de la COVID-19](https://apps.who.int/iris/bitstream/handle/10665/332084/WHO-2019-nCoV-Adjusting_PH_measures-Workplaces-2020.1-spa.pdf)

**16 mayo 2020**  
[MEDIDAS DE PREVENCIÓN E HIGIENE FRENTE A COVID-19 PARA LA REAPERTURA PARCIAL DE CENTROS EDUCATIVOS EN EL CURSO 2019-2020](https://drive.google.com/file/d/1PZ7EcfnObxGS3R0Z3Z5GMKlP4Co8tkaP/view)  
>IV. Distancias de seguridad  
>20. Ocupación de las aulas y otros espacios. La distancia mínima interpersonal será de 2 metros. El centro podrá optimizar aulas y otros espacios para dar cabida a los estudiantes pero en todo caso aplicando siempre la distancia interpersonal de 2 metros. Se priorizarán en la medida de lo posible los espacios al aire libre.  
>21. En todos los niveles educativos se organizarán los espacios y la distribución de las personas para lograr una distancia de seguridad interpersonal de 2 metros. La organización de la circulación de personas, la distribución de espacios, y la disposición de estudiantes se organizará para mantener las distancias de seguridad interpersonal exigidas en cada momento por el Ministerio de Sanidad.  

se indica varias veces más 2 metros, pero también esto 

>36. En el caso de que no se pueda asegurar una distancia interpersonal de 2 metros se deberá utilizar mascarilla higiénica, a poder ser reutilizable, por parte del personal de los centros educativos, así como por parte del alumnado a partir de la Etapa de Educación Primaria, en la medida de lo posible, en todos los espacios de la escuela siempre que se haga un uso correcto de la misma.

**19 mayo 2020**  
[Cómo reactivar los colegios, la pregunta que atormenta a toda Europa](https://www.elconfidencial.com/amp/mundo/europa/2020-05-19/como-reactivar-colegios-pregunta-atormenta-europa_2599631/)  
> Actualmente, la ratio de contagios (R) en el Reino Unido está entre 0,7 y 1. La única manera de ir avanzando en la estrategia de desescalada es mantenerla por debajo de 1  

**20 mayo 2020**
[Orden SND/422/2020, de 19 de mayo, por la que se regulan las condiciones para el uso obligatorio de mascarilla durante la situación de crisis sanitaria ocasionada por el COVID-19.](https://boe.es/buscar/act.php?id=BOE-A-2020-5142#a3)  
>El uso de mascarilla será obligatorio en la vía pública, en espacios al aire libre y en cualquier espacio cerrado de uso público o que se encuentre abierto al público, siempre que no sea posible mantener una distancia de seguridad interpersonal de al menos dos metros.

[Educació dice que obligará a las escuelas a abrir en fase 2](https://www.lavanguardia.com/vida/20200520/481292123345/educacio-dice-que-obligara-a-las-escuelas-a-abrir-en-fase-2.html)  
> Ratios de 5 en infantil y de 13 a 15 en primaria y ESO  
...  
La Generalitat contempla la vuelta a clase en grupos reducidos que podrían ser de 5 alumnos para escuelas infantiles, 13 para primaria y 15 para secundaria.

**21mayo 2020**  
[Celaá dice que en septiembre los colegios tendrán que convertir en aulas los gimnasios, salones de actos o bibliotecas](https://amp.elmundo.es/espana/2020/05/21/5ec644b5fc6c8345148b4583.html)  
> La intención es que, si no hay una vacuna y la crisis sanitaria permanece en iguales circunstancias que ahora, se reduzca la ratio de las aulas para que en ellas reciba clases sólo la mitad de los alumnos.

**22 mayo 2020**  
[Instrucciones para la reapertura de centros escolares en junio y el próximo curso 2020-2021](https://www.lavanguardia.com/vida/20200522/481324920609/instrucciones-educacion-curso-2020-2021-coronavirus-reapertura-centros.html)  
> Bajan de nuevo el número de niños por aula en las clases de infantil  
La Conselleria d’Educación ha vuelto a cambiar el número de alumnos máximo por profesor en la etapa de infantil. En las instrucciones de la reapertura de centros prevista en junio, si se alcanza la fase 2, y para el próximo curso 2020-2021 se establecen grupos de 5 niños por educador hasta 3 años, 8 alumnos en P-3 y hasta un máximo de 10 en P-4 y P-5. Los bebés menores de 12 meses no podrán ir a las llars d-infants dada su vulnerabilidad.  
En un primer documento elaborado por la Conselleria a inicios de esta semana, la ratio de las escuelas infantiles, en las dos etapas (de 0-3 y de 3-6 años) era de 5 alumnos por profesor. El conseller Josep Bargalló anunció l miércoles que los alumnos a partir de 3 años se distribuirían en grupos de 13, como los de primaria. Sin embargo, en las instrucciones oficiales recibidas en los colegios, la ratio es de 5 para los pequeños, 8 para los de 3 años y 10 alumnos para los de 4 y 5 años.

**27 mayo 2020**
[RESOLUCIÓN CONJUNTA DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA POR LA QUE SE DICTAN INSTRUCCIONES COMPLEMENTARIAS DE LAS INSTRUCCIONES PARA EL DESARROLLO DEL TERCER TRIMESTRE Y FINAL DEL CURSO 2019-2020 EN LA COMUNIDAD DE MADRID COMO CONSECUENCIA DEL ESTADO DE ALARMA PROVOCADO POR CORONAVIRUS (COVID-19)](https://drive.google.com/file/d/1SCvi1mVuDuOb9QLYJLk_h2NB70Leds2T/view)  
> La ratio, por analogía con los criterios expuestos por el Gobierno de la Nación, será como máximo del 50% por cada grupo de edad. En el caso de superar la ratio máxima establecida y completar dicho aforo, el centro deberá adoptar las medidas organizativas dentro de su autonomía pedagógica y de gestión.

**31 mayo 2020**  
[El misterio de la vuelta al cole en Madrid](https://elpais.com/espana/madrid/2020-05-31/el-misterio-de-la-vuelta-al-cole-en-madrid.html)  
> Sindicatos, asociaciones de padres y directores están a la espera de que la Comunidad diseñe un plan y los protocolos para el regreso a las aulas  
El nudo gordiano es la ratio por aula. De momento, lo único que parece claro es que Madrid descarta reducirla a la mitad, [una medida apuntada por la ministra, Isabel Celáa](https://elpais.com/sociedad/2020-05-04/el-gobierno-planea-limitar-a-15-el-numero-de-alumnos-por-aula-el-curso-que-viene.html), que el consejero de Educación y Juventud, Enrique [Ossorio, considera “imposible de llevar a la práctica”](https://elpais.com/espana/madrid/2020-05-05/la-comunidad-de-madrid-ve-imposible-establecer-clases-con-la-mitad-de-alumnos.html). CC OO estima una horquilla de reducción de entre el 30% y el 50% según la etapa educativa —en Madrid se llega a 28 alumnos en Primaria y a 35 en ESO y Bachillerato— y la Giner de los Ríos contempla la cifra de Celaá de partida, aunque matiza que es necesario hacer una evaluación previa de cada centro porque el número final lo determinará el tamaño de las aulas y de los espacios comunes y de lo masificado o no que esté el centro. Teresa Jusdado, secretaria de Enseñanza de la FeSP UGT Madrid, está de acuerdo: “Estará en el 25%, el 30% o el 50% menos, dependiendo del curso y del centro”.

**6 junio 2020**  
[INSTRUCCIONES Y MEDIDAS DE DESARROLLO Y ADAPTACIÓN A LA INCORPORACIÓN DEL PERSONAL DOCENTE Y PERSONAL FUNCIONARIO Y LABORAL DE ADMINISTRACIÓN Y SERVICIOS EN LOS CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID CON MOTIVO DE COVID-19](https://drive.google.com/file/d/1gkpmRkqp8iY1_KiEpduxW_mhzh1256Dq/view)  
> 5. AULAS
En todos los niveles educativos se organizarán los espacios y la distribución de las personas para lograr dicha distancia de seguridad interpersonal de dos metros. El centro podrá optimizar aulas y otros espacios para dar cabida a los estudiantes, pero en todo caso aplicando siempre la distancia interpersonal de dos metros. En caso de que no sea posible mantener una distancia de seguridad interpersonal de al menos dos metros será obligatorio el uso de mascarilla, salvo en los supuestos contemplados en la normativa vigente.

**9 junio 2020**  
[Las escuelas necesitan triplicar el espacio de la clase](https://www.lavanguardia.com/vida/20200609/481656569808/escuelas-coronavirus-espacio-clases-distancia-seguridad.html)  
El centro tipo en España carece de superficie suficiente para adaptarse a las necesidades del regreso tras la pandemia ni aun usando todas sus instalaciones

Información muy visual, cita valores de normativa

[![](https://rawcdn.githack.com/VangData/2020/cade8c335c2d31a4adea45c0ccb8851bcab81629/Escoles2/Escuelasbona-01.png)](https://rawcdn.githack.com/VangData/2020/cade8c335c2d31a4adea45c0ccb8851bcab81629/Escoles2/Escuelasbona-01.png)

**10 junio 2020**  
[Real Decreto-ley 21/2020, de 9 de junio, de medidas urgentes de prevención, contención y coordinación para hacer frente a la crisis sanitaria ocasionada por el COVID-19.](https://boe.es/buscar/act.php?id=BOE-A-2020-5895#a9)
>Artículo 9. Centros docentes.  
Las administraciones educativas deberán asegurar el cumplimiento por los titulares de los centros docentes, públicos o privados, que impartan las enseñanzas contempladas en el artículo 3 de la Ley Orgánica 2/2006, de 3 de mayo, de Educación, de las normas de desinfección, prevención y acondicionamiento de los citados centros que aquellas establezcan.  
En cualquier caso, deberá asegurarse la adopción de las medidas organizativas que resulten necesarias para evitar aglomeraciones y garantizar que se mantenga una distancia de seguridad de, al menos, 1,5 metros. Cuando no sea posible mantener dicha distancia de seguridad, se observarán las medidas de higiene adecuadas para prevenir los riesgos de contagio.

**11 junio 2020**
Surge hashtag [#20AlumnosPorClase](https://twitter.com/hashtag/20AlumnosPorClase)  
Permite recordar que en normativa podría estar tener 20 alumnos por clase  
[twitter jrfercuen/status/1271189703291699200](https://twitter.com/jrfercuen/status/1271189703291699200)  
Hoy es un buen día para recordar quiénes votaron a favor y quiénes en contra de la reducción de ratio a #20AlumnosPorClase . He tenido que buscar un tuit de @FiQuiPedia porque los medios, esos que “tanto” se preocupan de la educación, curiosamente apenas se hicieron eco

**13 junio 2020**  
[El rompecabezas para conseguir que todos los alumnos vuelvan a clase en septiembre](https://elpais.com/educacion/2020-06-12/el-rompecabezas-para-conseguir-que-todos-los-alumnos-vuelvan-a-clase-en-septiembre.html)  
Las autonomías buscan cómo retomar la enseñanza presencial mientras el Gobierno sopesa ampliar a 25 el número de escolares por clase hasta los 10 años

[La guerra de las ratios: profesores contra autonomías en la vuelta al colegio de septiembre](https://amp.elmundo.es/espana/2020/06/13/5ee3d5f221efa01b098b458f.html)  
>Todos los sindicatos están molestos con Celaá porque no obliga a los consejeros autonómicos a bajar el número de alumnos por aula  
Los sindicatos cuentan que han asistido «estupefactos» esta semana a la [relajación](https://www.elmundo.es/espana/2020/06/10/5ee0b1f8fdddffd88d8b45bf.html) de Celaá en cuanto a distancia interpersonal (de dos a **1,5 metros** ); alumnos por profesor (de **15 a 20** ); uso de mascarillas (excluidos los menores de **10 años** ), y asistencia de todos los críos al colegio (ya no habrá semipresencialidad). Acusan a la ministra de «haber cedido a las presiones autonómicas», y no garantizar un nuevo curso «en plenas condiciones de seguridad».  
Sobre todo porque la reducción de alumnos por aula ha quedado en el aire. La propia Celaá dijo el jueves, tras la **Conferencia Sectorial** , que las ratios van a seguir siendo las mismas porque son las que marca la ley: «25, 30 y 35 alumnos(según las etapas)». Aseguró que lo que plantea -recalcando mucho que es una recomendación que no obliga a nada a las CCAA- son «grupos estables de convivencia» de un máximo de 20 alumnos a cargo de un adulto. Los «grupos» son un eufemismo para evitar mencionar las «ratios», una palabra tabú que causa rechazo a las CCAA. 

**25 agosto 2020**  

Madrid anuncia que baja ratio en primaria y en 1º y 2º ESO

[La Comunidad de Madrid contratará casi 11.000 profesores, hará test COVID-19 a los docentes y un estudio serológico a alumnos y colectivos de riesgo](https://www.comunidad.madrid/notas-prensa/2020/08/25/comunidad-madrid-contratara-casi-11000-profesores-hara-test-covid-19-docentes-estudio-serologico-alumnos-colectivos-riesgo)  
> Las clases serán presenciales en Educación Infantil, Primaria, Especial, 1º y 2º de ESO, con **grupos máximos de 20 alumnos o respetando la distancia de 1,5 metros**

Incluye

[PDF - ESTRATEGIA EDUCACION NUEVO CURSO_25_08_2020](https://www.comunidad.madrid/file/223257/download?token=_zPv-2Fj)

En diapositiva 5, Educación Infantil Segundo Ciclo y Educación Primaria, se indica "Reducción de ratio de 25 a 20 alumnos por clases"

En diapositiva 6, 1º y 2º de la ESO (11-13 años), se indica "Reducción de grupos de 30 alumnos a 23"

**18 mayo 2021**  
Se publica primera versión de "Medidas de prevención, higiene y promoción de la salud frente a Covid-19 para centros educativos en el curso 2021-2022. Documento aprobado en la Comisión de Salud Pública el 18.05.2021 y acordado en la reunión conjunta de la Conferencia Sectorial de Educación y del Consejo Interterritorial del Sistema Nacional de Salud celebrada el 19 de mayo de 2021."

La información se publica por el ministerio en [Cuidado y bienestar de alumnado, profesorado y familias - educacionyfp.gob.es - archive.org](https://web.archive.org/web/20211227204536/https://www.educacionyfp.gob.es/destacados/covid19/cuidado-y-bienestar.html), página ya no disponible.

Ejemplo más reciente [MEDIDAS DE PREVENCIÓN, HIGIENE Y PROMOCIÓN DE LA SALUD FRENTE A COVID-19 PARA CENTROS EDUCATIVOS EN EL CURSO 2021-2022 Versión 29/06/2021 Aprobado en Comisión de Salud Pública el 29 de junio de 2021](https://web.archive.org/web/20210721141235/https://www.educacionyfp.gob.es/dam/jcr:705bde58-a667-4a3e-977d-377a4e6aa847/medidas-centros-educativos-curso-2021-2022.pdf)

En pie de tabla indica 

> 1 Los niveles de alerta para asignar escenarios responden a indicadores de situación epidemiológica según el documento de Actuaciones de respuesta coordinada para el control de la transmisión de COVID-19 . La indicación de cambio de escenario la establecerá la autoridad sanitaria de la Comunidad Autónoma para cada provincia o para el nivel territorial que decida, y dicho cambio se mantendrá durante 2 semanas hasta reevaluación.  
2 En nueva normalidad se permitirá la interacción entre grupos de un mismo curso, sobretodo en actividades al aire libre. No se permitirá la interacción entre grupos en el resto de escenarios de riesgo. El tamaño máximo de las clases será acorde a las ratios establecidas en la normativa aplicable.

Se enlaza [Actuaciones de respuesta coordinada para el control de la transmisión de COVID-19 Actualización de 26 de marzo de 2021. Este documento ha sido elaborado por la Ponencia de Alertas y Planes de Preparación y Respuesta, aprobado por la Comisión de Salud Pública del Consejo Interterritorial del Sistema Nacional de Salud](https://www.sanidad.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov/documentos/Actuaciones_respuesta_COVID_26.03.2021.pdf)

El documento más reciente no archivado ya no habla de escenarios [MEDIDAS DE PREVENCIÓN, HIGIENE Y PROMOCIÓN DE LA SALUD FRENTE A COVID-19 PARA CENTROS EDUCATIVOS Versión 03/05/2022 Aprobado en Comisión de Salud Pública el 3 de mayo de 2022](https://www.educacionyfp.gob.es/dam/jcr:210cdcf4-7e6c-4a93-96c1-e0f90a498c21/2022-05-03-medidas-c-educ-covid.pdf)  
> 3.1.2 Otros aspectos:  
>- El tamaño máximo de las clases será acorde a las ratios establecidas en la normativa aplicable 


