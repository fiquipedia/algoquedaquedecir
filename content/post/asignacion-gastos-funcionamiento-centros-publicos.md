+++
author = "Enrique García"
title = "Asignación gastos de funcionamiento centros públicos"
date = "2024-09-27"
tags = [
    "educación", "coste", "centros públicos"
]
toc = true

+++

Revisado 23 octubre 2024

## Resumen

Al hablar de coste de educación se suele citar la "infrafinaciación" de los privados con concierto, que afecta a sus costes de funcionamiento (el coste de las nóminas de los docentes se paga con dinero público), pero no se suele hablar de qué dinero reciben para costes de funciamiento los centros públicos.  

Se intenta detallar aquí con datos concretos qué coste (asignación de dinero público para gastos de funcionamiento) tienen los centros públicos y cómo se actualiza esa asignación frente a cómo se actualizan los módulos de concierto.   
Con la revisión de datos de 2018 a 2024, en los centros públicos la asignación de gastos de funcionamiento se ha reducido o mantenido, mientras que los módulos de privados con concierto se han revisado al alza.  

Posts relacionados:  
* [Datos gasto en educación](http://algoquedaquedecir.blogspot.com/2020/07/datos-gasto-en-educacion.html)  
* [Datos módulos de conciertos educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-modulos-de-conciertos-educacion.html)  
* [Madrid crea centros privados con concierto en diferido](https://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html)  

## Detalle

Esta información la compartía inicialmente como dato en "(D-CEP) Datos centros educativos públicos." "(D-CEP-1) Asignación anual a centros educativos públicos" en el post [Datos gasto en educación](http://algoquedaquedecir.blogspot.com/2020/07/datos-gasto-en-educacion.html), pero lo separo para ampliar y detallar.  

No soy experto en economía: quizá alguien cuestione la comparación entre la asignación para funcionamiento de centros públicos y los módulos de conciertos para centros privados, diferencias de coste por pago de construcción y terreno ... aunque ahí entrarían la cesión de suelo público a privados para que hagan centros privados garantizándoles el concierto de antemano y permitiéndoles hipotecar el suelo cedido para financiar obras. Ver [Madrid crea centros privados con concierto en diferido](https://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html)  


Comparto documento anonimizado de 2019 que fue como conocí el tema: es la comunicación de la consejería al centro indicando el dinero que se le asigna

[2019-PresupuestoIESanonimizado.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2019-PresupuestoIESanonimizado.pdf)  

En junio 2020 planteo petición presupuestos de algunos IES. Uno de cada DAT grande, más el Ramiro y el San Mateo como ejemplos conocidos de grandes centros con ciertas peculiaridades.  

>Solicito copia o enlace a la siguiente documentación:   
>1. Comunicación realizada según artículo 6.1 de decreto 149/2000 por las Direcciones Generales competentes del importe de los recursos que se les asignarán para sus gastos de funcionamiento y, en su caso, para la reposición de inversiones y equipamiento a los siguientes centros públicos:  
IES Antonio Machado, código 28000522  
IES Virgen de la Paloma, código 28020341  
IES Vírgen de la Paz, código 28038070  
IES Jaime Ferrán, código 28002415  
IES Prado de Santo Domingo, código 28030241  
IES Ramiro de Maeztu, código  28028672  
IES San Mateo, código 28030939  
Se solicita para esos centros de los ejercicios 2018, 2019 y 2020, desglosando al menos Gastos de mantenimiento, Gastos de suministros, Gastos de funcionamiento, Complementos de Programas y Dotación básica.  
>2. Documentación del sistema de cálculo de la asignación correspondiente a cada centro teniendo en cuenta el valor de todos los criterios objetivos utilizados para valorar las necesidades generales de gasto de cada centro, para los ejercicios 2018, 2019 y 2020.  
>3. Normas de elaboración del presupuesto de centro dictadas por la Dirección General de Presupuestos de la Consejería de Presidencia y Hacienda y las instrucciones de las Direcciones Generales de la Consejería de Educación responsables de los centros docentes, de acuerdo con la estructura y clasificación de los Presupuestos Generales de la Comunidad de Madrid, para los ejercicios 2018, 2019 y 2020.

**26 junio 2020**
Me envían notificación 09-OPEN-00056.3/2020 CSV 0907868040924578275529  
> En  relación  a  su  solicitud  de  información  presentada  con  fecha  22/05/2020,  le  informamos que  dada  la  complejidad  de  la  información  solicitada  se  procede  a  ampliar  el  plazo  de resolución por otros veinte días más, de acuerdo con lo establecido en el Artículo 42 de la Ley 10/2019, de 10 de abril, de Transparencia y de Participación de la Comunidad.

**24 julio 2020**
Recibo [resolución "estimatoria"](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2020-07-24-ResolucionAsignacionCentrosPublicos.pdf)  

No facilitan la información solicitada. Indican tres puntos en la respuesta asociada a los tres puntos de la solicitud.

Punto 1.  
Elaboran un resumen, y no me dan las comunicaciones.  
Son documentos que existen y que solicito explícitamente  

[Decreto 149/2000, de 22 de junio, por el que se regula el régimen jurídico de la autonomía de gestión de los centros docentes públicos no universitarios](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=311)  
>Artículo 6. Elaboración y aprobación del presupuesto  
>1. Una vez aprobado el proyecto de los Presupuestos Generales de la Comunidad de Madrid para cada ejercicio presupuestario, por las Direcciones Generales competentes, se comunicará a los centros docentes el importe de los recursos que se les asignarán para sus gastos de funcionamiento y, en su caso, para la reposición de inversiones y equipamiento que, de acuerdo con los programas de inversiones no centralizados, puedan ser contratados por los centros docentes en el marco de su autonomía de gestión económica.

Punto 2.  
Indican  
>Por consiguiente, no existe una documentación concreta del sistema de cálculo de la asignación correspondiente a cada centro teniendo en cuenta el valor de todos los criterios objetivos utilizados para valorar las necesidades generales de gasto de cada centro, según lo expuesto anteriormente.

Hay una inconsistencia importante: se afirma que *"no existe una documentación concreta del sistema de cálculo de la asignación correspondiente a cada centro teniendo en cuenta el valor de todos los criterios objetivos utilizados para valorar las necesidades generales de gasto de cada centro"* cuando en otro documento se afirma *"Para este ejercicio 2019, se ha mantenido el sistema de cálculo de la asignación correspondiente a cada centro teniendo en cuenta el valor de todos los criterios objetivos utilizados para valorar las necesidades generales de gasto de cada centro."*

Según eso, el sistema de cálculo de asignación no está documentado, y se habla de "presentan un dinamismo muy lejos de una estabilidad regulada", pero es que a inicio del año, según normativa, se realiza una asignación con unos criterios. No estoy pidiendo criterios para realizar modificaciones.

Punto 3.  
Nada que objetar. Enlazan dos documentos  

[NORMATIVA PRESUPUESTARIA Y DE RECURSOS HUMANOS DE LA COMUNIDAD DE MADRID. Tomo 1. Edición: 6/2016. Imprime: BOCM (pdf 259 páginas)](http://www.madrid.org/presupuestos/attachments/category/61/Normativa%20presupuestaria%20de%20la%20CM.pdf)  

MANUAL DE GESTIÓN ECONÓMICA DE LOS CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID
CONSEJERÍA DE EDUCACIÓN
DIRECCIÓN GENERAL DE CENTROS DOCENTES
Coordinación del Área de los Servicios de Régimen Jurídico y Gestión Económico-Administrativa
Elaborado por:
Julián Voces Espinosa
Montserrat Boto Sanz
Dirigido por:
Jesús Seco Muñoz
VERSIÓN SEPTIEMBRE 2003
(actualizado enero 2005)

http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-Disposition&blobheadervalue1=filename%3Dman_gecd_e05.pdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1181216410795&ssbinary=true

**25 julio 2020**

Planteo reclamación a CTBG:

>En la solicitud se pedían 3 cosas, y reclamo puntos 1 y 2 ya que información no satisface la solicitud.  
Punto 1: no se facilita copia de documentos que se piden explícitamente y no se niegan que existan "Comunicación realizada según artículo 6.1 de decreto 149/2000"  
En lugar de facilitar copia, facilitan unos datos agregados que han supuesto elaboración.  
Punto 2: hay una inconsistencia importante, ya que se afirma que  
"no existe una documentación concreta del sistema de cálculo de la asignación correspondiente a cada centro teniendo en cuenta el valor de todos los criterios objetivos utilizados para valorar las necesidades generales de gasto de cada centro"
cuando en otro documento similar a los que solicito en punto 1 (aporto un ejemplo anonimizado) se afirma  
"Para este ejercicio 2019, se ha mantenido el sistema de cálculo de la asignación correspondiente a cada centro teniendo en cuenta el valor de todos los criterios objetivos utilizados para valorar las necesidades generales de gasto de cada centro."  
Según eso, el sistema de cálculo de asignación no está documentado, y se habla de "presentan un dinamismo muy lejos de una estabilidad regulada", pero considero que a inicio del año, según normativa, se realiza una asignación con unos criterios.  
No estoy pidiendo criterios para realizar modificaciones.  
Si no existe documentación, pero se facilitan documentos del punto 1, se aportaría transparencia para poder visualizar con qué criterios se ha calculado  la asignación correspondiente a cada centro según datos y criterios objetivos.  

**11 noviembre 2020**

Recibo resolución estimatoria CTBG [RT_0379_2020.pdf](https://www.consejodetransparencia.es/ct_Home/dam/jcr:663bd7fc-7222-4664-9e47-7661c0a40e32/RT_0379_2020.pdf)

**18 diciembre 2020**

Recibo ejecución resolución.

* [Documento ejecución](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2020-12-18-Ejecuci%C3%B3n-09-2798-00012.0-2020.pdf)  
* [Documento criterios](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2020-12-18-DocumentoCriterios.pdf)  
* [Adjunto hoja de cálculo con datos de los 7 centros](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2020-11-18-Adjunto1.xlsx)  

La ejecución tiene varias cosas llamativas:

1. No dan comunicaciones.

CTBG indicó "En caso de que no existan dichas Comunicaciones ...la información extraída de la aplicación GECD (que equivale a dicha Comunicación) deberá aportarse desglosada por centro..."

Dado que dan información de GECD, se puede asumir que dicen que no existen esas comunicaciones, cuando existen para otros centros (y en mis solicitud puse centros de las 5 DATs). Un planteamiento sería solicitarlas directamente a los centros.

2. No dan criterios con cantidades.

La resolución de CTBG indicaba "Documentos sobre el sistema de cálculo de la asignación correspondiente a cada centro teniendo y sobre los distintos criterios objetivos utilizados para valorar las necesidades generales de gasto de cada centro." Pero no dan ninguna cantidad: dan algo muy vago.

Por experiencia pueden decir que han cumplido porque han dado criterios (podían haber dicho que cumplían aunque hubieran dado una estampita). Mi único recurso para tener ese detalle en teoría sería un contencioso... o no: lo que puedo hacer es una solicitud nueva explicitando más lo que quiero. Creo que poder comparar el importe por criterio que dan a públicos con los módulos de concierto es importante para aportar datos al tema de la "infrafinanciación" de los privados con concierto


**19 diciembre 2020**  
Planteo nueva solicitud

> Solicito copia o enlace a documentación que permita el cálculo de cuantía asociada todos y cada uno de los criterios de cálculo de asignación de gastos de funcionamiento a los centros públicos de la subdirección general de centros de educación secundaria para los años 2018 2019 y 2020, desglosando valor por criterio y tramo.  
Tras 09-2798-00012.0/2020, RT0379/2020, se facilitó un documento titulado "CRITERIOS APLICADOS EN EL CÁLCULO DE ASIGNACIÓN DE GASTOS DE FUNCIONAMIENTO A LOS CENTROS DE LA SUBDIRECCIÓN GENERAL DE CENTROS DE EDUCACIÓN SECUNDARIA PARA LOS AÑOS 2018, 2019 Y 2020."  
En dicho documento se reflejan criterios, pero no se indica ninguna cantidad económica ni valor (solo se concretan algunos tramos) que permiten realizar el cálculo de cuantía, que considero que es una información pública sobre la que ejerzo mi derecho de acceso. En concreto  
1 (párrafo 1) "una serie de coeficientes": la documentación debe incluir los valores de esos coeficientes.  
2 (párrafos 1 y 11) "se asigna a todos los centros una Dotación básica", "la Dotación básica se aplica a todos los centros y se calcula sumando a una cantidad fija, un porcentaje de los gastos totales calculados.": la documentación debe incluir el valor económico de esa cantidad fija y el valor del porcentaje de gastos totales calculados, así como la obtención de esos gastos totales calculados.  
3 (párrafo 2). "La asignación (económica) se realiza asignando un valor a cada uno de los datos facilitados": la documentación debe incluir el valor en euros de esa asignación económica.  
4 (párrafos 3 y 4) "La superficie de cada centro le ubica en un determinado tramo y tal ubicación le hace corresponder una determinada asignación (económica). Lo mismo ocurre en los apartados jardines, antigüedad y turnos del centro": la documentación debe incluir la asignación económica a cada tramo (sí se detallan todos los tramos antigüedad, y turnos, pero no se detallan todos los tramos en superficie construida y superficie de jardines (solo tramo inicial y final), ni la asignación de ningún tramo).  
5 (párrafo 8) "Entre los criterios en los que no se consideran tramos, sino que les corresponde un valor fijo que se aplica directamente sobre el valor del dato, se encuentran la asignación por número de calderas, por número de edificios, por número de ascensores, por número de aulas de informática y por número de alumnos.": la documentación debe incluir la asignación económica según esos criterios.  
6 (párrafo 9) "En relación con el apartado Gastos suministros, los criterios calefacción, luz y agua se calculan cada año mediante un prorrateo que depende de la superficie útil total de todos los centros, y el gasto total en calefacción, luz y agua declarado por los centros.": la documentación debe incluir el detalle de ese prorrateo para poder calcular la asignación económica según esos criterios.  
7 (párrafo 10) "En Complemento programas hay asignación de valores fijos en función del tipo de determinados programas en el centro: deportivo, tecnológico, ...": la documentación debe incluir la asignación según el tipo de programa.  
En párrafo 12 se indica "Aplicando el sistema descrito se obtiene la asignación correspondiente a cada centro en función de unos criterios comunes para todos los centros." Dado que los criterios que determinan asignación económica son comunes para todos los centros, la documentación de esos criterios es única, y aplicarlos permitirá obtener los valores de asignación ya facilitados para los 7 centros consultados.  
Quiero hacer constar que la transparencia que solicito en el cálculo de cuantía asociada a todos y cada uno de los criterios de cálculo de asignación de gastos de funcionamiento a los centros públicos es una transparencia similar a la existente en los criterios de cálculo de asignación de gastos de funcionamiento a los centros privados con concierto educativo que son sostenidos con fondos públicos, criterios regulados y documentados con importes en los módulos de conciertos, artículo 117 LOE (https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a117) y Real Decreto-ley 2/2020, ANEXO 1 Módulos económicos de distribución de fondos públicos para sostenimiento de centros concertados https://www.boe.es/buscar/act.php?id=BOE-A-2020-909  
Repito aquí texto de RT0379/2020 "El hecho de que un ciudadano quiera conocer dichos criterios objetivos satisface la finalidad de la LTAIBG prevista en su preámbulo para que “los ciudadanos pueden conocer cómo se toman las decisiones que les afectan, cómo se manejan los fondos públicos o bajo qué criterios actúan nuestras instituciones podremos hablar del inicio de un proceso en el que los poderes públicos comienzan a responder a una sociedad que es crítica, exigente y que demanda participación de los poderes públicos”.

Registro 59/000509.9/20

**17 febrero 2021**

Recibo [resolución estimatoria](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2021-02-17-Resoluci%C3%B3nCriteriosPresupuestoCentrosP%C3%BAblicos.pdf)  

**4 agosto 2024**

Vuelvo a hacer nueva solicitud: pasado el tiempo creo puede ser interesante ver los datos de varios años para reflejar la variación

> Solicito copia o enlace a documentación que permita el cálculo de cuantía asociada todos y cada uno de los criterios de cálculo de asignación de gastos de funcionamiento a los centros públicos para los años 2021, 2022, 2023 y 2024, desglosando valor por criterio y tramo.  
Dicha información fue facilitada por la Consejería para ejercicios 2018, 2019 y 2020 tras RT0379/2020 https://www.
consejodetransparencia.es/ct_Home/dam/jcr:663bd7fc-7222-4664-9e47-7661c0a40e32/RT_0379_2020_Censurado.pdf y solicitud 09-
OPEN-00179.4/2020 en documento con CSV 1056163699752698468977

**26 septiembre 2024**

Recibo [resolución estimatoria](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2024-09-26-ResolucionAcceso-09-OPEN-00159.6-2024.pdf)  

Incluyen datos de Bachillerato Internacional: lo menciono en ese post [Bachillerato Internacinoal](../post/bachillerato-internacional)


### Análisis asignación gastos funcionamiento a centros públicos Madrid 2018 a 2024

Intento resumir los documentos  

1. Hasta 2024 aplican los criterios ya comunicados en 2020: 2021, 2022 y 2023, y al prorrogarse presupuestos  
> prórroga de los gastos de funcionamiento asignados a los centros docentes públicos
no universitarios, en las cuantías consolidadas asignadas para su funcionamiento en el ejercicio
anterior.

Es decir, en 3 años los centro públicos no han recibido más dinero. Sin embargo los módulos de concierto se han actualizado en los presupuestos anualmente en los presupuestos del Estado y en normativa de Madrid en 2021, 2022 y 2023.  

Ejemplos  
[Ley 31/2022, de 23 de diciembre, de Presupuestos Generales del Estado para el año 2023. ANEXO IV Módulos económicos de distribución de fondos públicos para sostenimiento de centros concertados](https://www.boe.es/buscar/act.php?id=BOE-A-2022-22128#ai-4)  

[ORDEN 622/2023, de 27 de febrero, del Vicepresidente, Consejero de Educación y Universidades, relativa a la modificación de los módulos económicos para la financiación de centros docentes privados sostenidos con fondos públicos en el ejercicio 2023](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/03/09/BOCM-20230309-21.PDF)  

[ORDEN 4595/2024, de 11 de octubre, del Consejero de Educación, Ciencia y Universidades, relativa a la modificación de los módulos económicos para la financiación de los centros docentes privados sostenidos con fondos públicos en el ejercicio 2024.](https://www.bocm.es/boletin/CM_Orden_BOCM/2024/10/23/BOCM-20241023-11.PDF)  

2. 2024: se actualizan y reorganizan criterios.   

2.1 Valor del punto  

Se incluye inicialmente esta fórmula  

> De acuerdo con la puntuación obtenida aplicando los valores correspondientes que figuran en la
tabla, se obtienen los puntos del centro para la asignación de los recursos económicos.  
A continuación, se calcula el valor del PUNTO (p) mediante el cociente:  
**PUNTO = (CRÉDITO ASIGNADO EJERCICIO)/(TOTAL PUNTOS CENTROS)**  
Por último, los puntos obtenidos por cada centro se multiplican por el PUNTO y se obtiene el crédito
asignado a cada centro para cada ejercicio económico.  

Eso supone que el valor del punto depende del crédito asignado: aunque un centro tenga los mismos puntos, podría recibir menos si el crédito asignado es menor.  
En el documento de 2021 [2021-02-17-ResoluciónCriteriosPresupuestoCentrosPúblicos.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2021-02-17-Resoluci%C3%B3nCriteriosPresupuestoCentrosP%C3%BAblicos.pdf) el valor del punto se indicaba de manera fija  

>Valor fijo 2018: 4,35 €  
Valor fijo 2019: 4,3 €  
Valor fijo 2020: 4,3 €  

Esto parece trivial, pero implica que de 2018 a 2019 teniendo los mismos puntos se perdió 0,05/4,35=0,015: **1,15% menos**  

En el mismo documento de 2024 (página 11 de 15) se indica valor fijo 4,3 €: **¿es inconsistente con la fórmula inicial?**  

2.2 Variaciones 2024 respecto lo anterior

En 2024 se indica que "PorcBasico" es 0,03 (3%), cuando en documento 2020 era 5%: **2% menos**

Entre 2018, 2019 y 2020 y 2024 hay pocas variaciones de puntos:  

*  "agua, SuperUtil*ValorFijo" pasa de 0,84 a 0,77. En 2024 pasa a 0,71: **todavía más recorte**  
*  "luz" pasa de 3,21 a 3,3. En 2024 pasa a 3,35. **Asociable a incremento coste energía**  
*  "calefacción" pasa de 2,1 a 2,3. En 2020 pasa a 2,45. **Asociable a incremento coste energía**  
En 2024 se contempla en las fórmulas un incremento por turno vespertino que no figuraba en documento 2020.  

La fórmula de 2024  
>  (**) AlumnosACNEE: nº al. motóricos*13 + (nº al. auditivos + nº al. visuales)*6,5 + (resto al. CNEE)*11,5 + nº fisios*13 + nº DUE*14 + nº TIII*29 + nº integrador social*14

No es idéntica a la del documento de 2020: se han restado 28 puntos que se sumaban de manera fija: **28 puntos menos**

Se aclara que no se recibe menos: seguramente eso generaría protestas de los centros que no lo entenderían  

>  No obstante, y en aras a compensar los posibles decrementos respecto al año anterior, en el ejercicio 2024 se ha aplicado el criterio de **compensar hasta igualar la cantidad recibida en el ejercicio anterior, a aquellos centros a los que la cantidad final que se obtenía aplicando los criterios anteriores era inferior a la asignada en el ejercicio anterior.**



