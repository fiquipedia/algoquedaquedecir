+++
author = "Enrique García"
title = "Pruebas evaluación educación obligatoria"
date = "2024-04-08"
tags = [
    "educación", "Madrid", "normativa", "evaluación"
]
toc = true

+++

Revisado 14 septiembre 2024

## Resumen

Con LOMLOE se establecen a nivel estatal pruebas de diagnóstico en educación obligatoria en 4º Educación Primaria (EP) y 2º ESO. Ha habido variaciones respecto a LOE y LOMCE, donde había pruebas de evaluación en 6º EP y 4ºESO, que en el caso de 4º ESO se plantearon con efectos académicos ya que condicionaban titular: eran las que se llamaban "reválidas"

Enlaza con posts:
* [Finalidad evaluación para clasificación centros](https://fiquipedia.gitlab.io/algoquedaquedecir/post/finalidad-evaluacion-para-clasificacion-centros/)  
* [LOMCE: evaluaciones finales 4º ESO](http://algoquedaquedecir.blogspot.com/2017/11/lomce-evaluaciones-finales-4-eso.html)
* [Ley maestra de liberad de enseñanza](../ley-maestra-de-libertad-de-eleccion-educativa) que cita las evaluaciones

En este post no se tratan las evaluaciones en educación no obligatoria, como fueron las reválidas de Bachillerato de LOMCE, se puede ver [Evaluaciones Finales, “reválidas” (surgen con LOMCE y desaparecen con LOMLOE)](https://www.fiquipedia.es/home/legislacion-educativa/lomce/evaluaciones-finales/)


## Detalles 

Las pruebas están en normativa, algunas de diagnóstico y otras con efectos académicos

### Pruebas evaluación LOE 2006

**6º EP y 2º ESO, diagnóstico**

[Artículo 21. Evaluación de diagnóstico.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=35&tn=1&p=20060504#a21)  
>**Al finalizar el segundo ciclo de la educación primaria** todos los centros realizarán una evaluación de diagnóstico de las competencias básicas alcanzadas por sus alumnos. Esta evaluación, competencia de las Administraciones educativas, tendrá carácter formativo y orientador para los centros e informativo para las familias y para el conjunto de la comunidad educativa. Estas evaluaciones tendrán como marco de referencia las evaluaciones generales de diagnóstico que se establecen en el artícu­lo 144.1 de esta Ley.  

[Artículo 29. Evaluación de diagnóstico.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=35&tn=1&p=20060504#a29)  
>**Al finalizar el segundo curso de la educación secundaria obligatoria** todos los centros realizarán una evaluación de diagnóstico de las competencias básicas alcanzadas por sus alumnos. Esta evaluación será competencia de las Administraciones educativas y tendrá carácter formativo y orientador para los centros e informativo para las familias y para el conjunto de la comunidad educativa. Estas evaluaciones tendrán como marco de referencia las evaluaciones generales de diagnóstico que se establecen en el artículo 144.1 de esta Ley.

[Artículo 144. Evaluaciones generales de diagnóstico.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=205&tn=1&p=20060504#a144)  
> 1. El Instituto de Evaluación y los organismos correspondientes de las Administraciones educativas, en el marco de la evaluación general del sistema educativo que les compete, colaborarán en la realización de evaluaciones generales de diagnóstico, que permitan obtener datos representativos, tanto del alumnado y de los centros de las Comunidades Autónomas como del conjunto del Estado. Estas evaluaciones versarán sobre las competencias básicas del currículo, se realizarán en la enseñanza primaria y secundaria e incluirán, en todo caso, las previstas en los artículos 21 y 29. La Conferencia Sectorial de Educación velará para que estas evaluaciones se realicen con criterios de homogeneidad.  
> 2. En el marco de sus respectivas competencias, corresponde a las Administraciones educativas desarrollar y controlar las evaluaciones de diagnóstico en las que participen los centros de ellas dependientes y proporcionar los modelos y apoyos pertinentes a fin de que todos los centros puedan realizar de modo adecuado estas evaluaciones, que tendrán carácter formativo e interno.  
> 3. Corresponde a las Administraciones educativas regular la forma en que los resultados de estas evaluaciones de diagnóstico que realizan los centros, así como los planes de actuación que se deriven de las mismas, deban ser puestos en conocimiento de la comunidad educativa. En ningún caso, los resultados de estas evaluaciones podrán ser utilizados para el establecimiento de clasificaciones de los centros.

[INEE Nacional Evaluaciones anteriores Evaluaciones LOE 2009-2010](https://www.educacionfpydeportes.gob.es/inee/evaluaciones-nacionales/evaluaciones-anteriores/2009-2010.html)  

### Pruebas evaluación LOMCE 2013

**3º EP y 6º EP diagnóstico, y en 4º ESO condiciona titular ESO "reválida"**

[Artículo 20. Evaluación durante la etapa.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=205&tn=1&p=20131210#a20)  
> ...  
>3. Los centros docentes realizarán una evaluación individualizada a todos los alumnos y alumnas **al finalizar el tercer curso de Educación Primaria**, según dispongan las Administraciones educativas, en la que se comprobará el grado de dominio de las destrezas, capacidades y habilidades en expresión y comprensión oral y escrita, cálculo y resolución de problemas en relación con el grado de adquisición de la competencia en comunicación lingüística y de la competencia matemática. De resultar desfavorable esta evaluación, el equipo docente deberá adoptar las medidas ordinarias o extraordinarias más adecuadas.

[Artículo 21. Evaluación final de Educación Primaria.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=35&tn=1&p=20200930#a21)  
>**1. Al finalizar el sexto curso de Educación Primaria**, se realizará una evaluación individualizada a todos los alumnos y alumnas,en la que se comprobará el grado de adquisición de la competencia en comunicación lingüística, de la competencia matemática y de las competencias básicas en ciencia y tecnología, así como el logro de los objetivos de la etapa.  
>2. El Gobierno, previa consulta a las Comunidades Autónomas, establecerá los criterios de evaluación y las características generales de las pruebas para todo el Sistema Educativo Español con el fin de asegurar unos criterios y características de evaluación comunes a todo el territorio.  
>3. El resultado de la evaluación se expresará en niveles. El nivel obtenido por cada alumno o alumna se hará constar en un informe, que será entregado a los padres, madres o tutores legales y que tendrá carácter informativo y orientador para los centros en los que los alumnos y alumnas hayan cursado sexto curso de Educación Primaria y para aquellos en los que cursen el siguiente curso escolar, así como para los equipos docentes, los padres, madres o tutores legales y los alumnos y alumnas.  
Las Administraciones educativas podrán establecer planes específicos de mejora en aquellos centros públicos cuyos resultados sean inferiores a los valores que, a tal objeto, hayan establecido.  
En relación con los centros concertados se estará a la normativa reguladora del concierto correspondiente.

[Artículo 29. Evaluación final de Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=45&tn=1&p=20131210#a29)  
>**1. Al finalizar el cuarto curso**, los alumnos y alumnas realizarán una evaluación individualizada por la opción de enseñanzas académicas o por la de enseñanzas aplicadas, en la que se comprobará el logro de los objetivos de la etapa y el grado de adquisición de las competencias correspondientes en relación con las siguientes materias:  
a) Todas las materias generales cursadas en el bloque de asignaturas troncales, salvo Biología y Geología y Física y Química, de las que el alumno o alumna será evaluado si las escoge entre las materias de opción, según se indica en el párrafo siguiente.  
b) Dos de las materias de opción cursadas en el bloque de asignaturas troncales, en cuarto curso.  
c) Una materia del bloque de asignaturas específicas cursada en cualquiera de los cursos, que no sea Educación Física, Religión, o Valores Éticos.  
>2. Los alumnos y alumnas podrán realizar la evaluación por cualquiera de las dos opciones de enseñanzas académicas o de enseñanzas aplicadas, con independencia de la opción cursada en cuarto curso de Educación Secundaria Obligatoria, o por ambas opciones en la misma ocasión.  
3. Podrán presentarse a esta evaluación aquellos alumnos y alumnas que hayan obtenido bien evaluación positiva en todas las materias, o bien negativa en un máximo de dos materias siempre que no sean simultáneamente Lengua Castellana y Literatura, y Matemáticas. A estos efectos, la materia Lengua Cooficial y Literatura tendrá la misma consideración que la materia Lengua Castellana y Literatura en aquellas Comunidades Autónomas que posean lengua cooficial.  
A los efectos de este apartado, sólo se computarán las materias que como mínimo el alumno o alumna debe cursar en cada uno de los bloques. Además, en relación con aquellos alumnos y alumnas que cursen Lengua Cooficial y Literatura, sólo se computará una materia en el bloque de asignaturas de libre configuración autonómica, con independencia de que dichos alumnos y alumnas puedan cursar más materias de dicho bloque. Las materias con la misma denominación en diferentes cursos de Educación Secundaria Obligatoria se considerarán como materias distintas.  
4. El Ministerio de Educación, Cultura y Deporte establecerá para todo el Sistema Educativo Español los criterios de evaluación y las características de las pruebas, y las diseñará y establecerá su contenido para cada convocatoria.  
5. La superación de esta evaluación requerirá una calificación igual o superior a 5 puntos sobre 10.  
6. Los alumnos y alumnas que no hayan superado la evaluación por la opción escogida, o que deseen elevar su calificación final de Educación Secundaria Obligatoria, podrán repetir la evaluación en convocatorias sucesivas, previa solicitud.  
Los alumnos y alumnas que hayan superado esta evaluación por una opción podrán presentarse de nuevo a evaluación por la otra opción si lo desean, y de no superarla en primera convocatoria podrán repetirla en convocatorias sucesivas, previa solicitud.  
Se tomará en consideración la calificación más alta de las obtenidas en las convocatorias que el alumno o alumna haya superado.  
Se celebrarán al menos dos convocatorias anuales, una ordinaria y otra extraordinaria.  

[Artículo 31. Título de Graduado en Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=45&tn=1&p=20131210#a31)  
> **1. Para obtener el título de Graduado en Educación Secundaria Obligatoria será necesaria la superación de la evaluación final**, así como una calificación final de dicha etapa igual o superior a 5 puntos sobre 10. ...  

[Artículo 144. Evaluaciones individualizadas.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=205&tn=1&p=20131210#a144)  
>1. Los criterios de evaluación correspondientes a las evaluaciones individualizadas indicadas en los artículos 20.3, 21, 29 y 36 bis de esta Ley Orgánica serán comunes para el conjunto del Estado.  
En concreto, las pruebas y los procedimientos de las evaluaciones indicadas en los artículos 29 y 36 bis se diseñarán por el Ministerio de Educación, Cultura y Deporte, a través del Instituto Nacional de Evaluación Educativa. Dichas pruebas serán estandarizadas y se diseñarán de modo que permitan establecer valoraciones precisas y comparaciones equitativas, así como el seguimiento de la evolución a lo largo del tiempo de los resultados obtenidos.  
La realización material de las pruebas corresponde a las Administraciones educativas competentes. Las pruebas serán aplicadas y calificadas por profesorado del Sistema Educativo Español externo al centro.  
Reglamentariamente se regulará el procedimiento de revisión de los resultados de las evaluaciones.  
> 2. Las Administraciones educativas podrán establecer otras evaluaciones con fines de diagnóstico.  
> 3. Las autoridades educativas establecerán las medidas más adecuadas para que las condiciones de realización de las evaluaciones individualizadas se adapten a las necesidades del alumnado con necesidades educativas especiales.


[INEE Nacional Evaluaciones anteriores Evaluaciones LOMCE (2014-2021)](https://www.educacionfpydeportes.gob.es/inee/evaluaciones-nacionales/evaluaciones-anteriores/2014-2021.html)  

### Pruebas evaluación LOMLOE 2020

**4º EP y 2º ESO, diagnóstico** 

[Artículo 21. Evaluación de diagnóstico.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a21)  
> **En el cuarto curso de educación primaria** todos los centros realizarán una evaluación de diagnóstico de las competencias adquiridas por su alumnado. Esta evaluación, que será responsabilidad de las Administraciones educativas, tendrá carácter informativo, formativo y orientador para los centros, para el profesorado, para el alumnado y sus familias y para el conjunto de la comunidad educativa. Estas evaluaciones, de carácter censal, tendrán como marco de referencia el establecido en el artículo 144.1 de esta Ley.  
En el marco de los planes de mejora a los que se refiere el artículo 121 y a partir del análisis de los resultados de la evaluación de diagnóstico, las Administraciones educativas promoverán que los centros elaboren propuestas de actuación que contribuyan a que el alumnado alcance las competencias establecidas, permitan adoptar medidas de mejora de la calidad y la equidad de la educación y orienten la práctica docente.

[Artículo 29. Evaluación de diagnóstico.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a29)  
> **En el segundo curso de educación secundaria obligatoria** todos los centros realizarán una evaluación de diagnóstico de las competencias alcanzadas por su alumnado. Esta evaluación, que será responsabilidad de las Administraciones educativas, tendrá carácter informativo, formativo y orientador para los centros, para el profesorado, para el alumnado y sus familias y para el conjunto de la comunidad educativa. Estas evaluaciones, de carácter censal, tendrán como marco de referencia el establecido en el artículo 144.1 de esta Ley.  
En el marco de los planes de mejora a los que se refiere el artículo 121 y a partir del análisis de los resultados de la evaluación de diagnóstico, las Administraciones educativas promoverán que los centros elaboren propuestas de actuación que contribuyan a que el alumnado alcance las competencias establecidas, permitan adoptar medidas de mejora de la calidad y la equidad de la educación y orienten la práctica docente.
 
[Artículo 144. Evaluaciones de diagnóstico.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a144)  
>1. El Instituto Nacional de Evaluación Educativa y los organismos correspondientes de las Administraciones educativas colaborarán en la realización de un marco común de evaluación que sirva como referencia de las evaluaciones de diagnóstico contempladas en los artículos 21 y 29 de esta Ley. Los centros docentes realizarán una evaluación a todos sus alumnos y alumnas en cuarto curso de educación primaria y en segundo curso de educación secundaria obligatoria, según dispongan las Administraciones educativas. La finalidad de esta evaluación será diagnóstica y en ella se comprobará al menos el grado de dominio de la competencia en comunicación lingüística y de la competencia matemática. Los centros educativos tendrán en cuenta los resultados de estas evaluaciones en el diseño de sus planes de mejora.  
>2. En el marco de sus respectivas competencias, corresponde a las Administraciones educativas desarrollar y controlar las evaluaciones de diagnóstico en las que participen los centros de ellas dependientes y proporcionar los modelos y apoyos pertinentes a fin de que todos los centros puedan realizar de modo adecuado estas evaluaciones, que tendrán carácter formativo e interno.  
>3. Corresponde a las Administraciones educativas regular la forma en que los resultados de estas evaluaciones de diagnóstico que realizan los centros, así como los planes de actuación que se deriven de las mismas, deban ser puestos en conocimiento de la comunidad educativa. En ningún caso, los resultados de estas evaluaciones podrán ser utilizados para el establecimiento de clasificaciones de los centros.  
> 4. Estas evaluaciones, así como las reguladas en el artículo anterior, tendrán en cuenta al alumnado con necesidades educativas especiales derivadas de discapacidad, incluyendo, en las condiciones de realización de dichas evaluaciones, las adaptaciones y recursos que hubiera tenido.


[INEE Nacional Evaluaciones LOMLOE](https://www.educacionfpydeportes.gob.es/inee/evaluaciones-nacionales/evaluaciones-lomloe.html)  


### Pruebas Madrid LOMCE

[Regulación de Pruebas diagnósticas](https://www.comunidad.madrid/servicios/educacion/regulacion-pruebas-diagnosticas)  

Se citan 3 

[Evaluación 3º Primaria](https://www.comunidad.madrid/servicios/educacion/evaluacion-3o-primaria)  
> Evaluación 3º de Educación Primaria en la Comunidad de Madrid. Última realizada Abril 2019  
En aplicación de la Ley Orgánica 8/2013 para la Mejora de la Calidad Educativa se establece la evaluación individualizada a todos los alumnos de 3º curso de Educación Primaria y de 6º curso de Educación Primaria.

[Evaluación 6º Primaria](https://www.comunidad.madrid/servicios/educacion/evaluacion-6o-primaria)  
> Evaluación 6º de Educación Primaria en la Comunidad de Madrid. Última realizada Abril 2019  
En aplicación de la Ley Orgánica 8/2013 para la Mejora de la Calidad Educativa se establece la evaluación individualizada a todos los alumnos de 3º curso de Educación Primaria y de 6º curso de Educación Primaria.

[Evaluación 4º ESO](https://www.comunidad.madrid/servicios/educacion/evaluacion-4o-eso)  
> Evaluación 4º ESO en la Comunidad de Madrid. Última realizada Abril 2019  
La prueba de Evaluación Final de Educación Secundaria es una prueba censal a todos los alumnos de 4º de Educación Secundaria Obligatoria en todos los centros de la Comunidad de Madrid. Se realizó por primera vez en el curso 2016-2017. 

[evaluaciones de educación primaria 2017 tercer curso sexto curso secundaria 2017 cuarto curso INFORME DE LA COMUNIDAD DE MADRID RESULTADOS Y CONTEXTO 2018](https://www.comunidad.madrid/sites/default/files/doc/educacion/sgea_eval_2017_publicacioninforme.pdf)  

En 2017 sí se pagó a aplicadores y correctores

[Resolución 12 diciembre 2017](https://drive.google.com/file/d/1NrjyplokLEbHWkklgx2OQ7yRJHh-PiGn/view)  
> “Coste asociado a aplicadores y correctores para la evaluación final ESO 2017”  
280.516 euros

### Pruebas Madrid LOMLOE 

[La Comunidad de Madrid evaluará los conocimientos de 280.000 alumnos para reforzar la calidad de su enseñanza](https://www.comunidad.madrid/noticias/2024/03/15/comunidad-madrid-evaluara-conocimientos-280000-alumnos-reforzar-calidad-su-ensenanza)  
La Comunidad de Madrid evaluará anualmente los conocimientos de más de 280.000 alumnos para reforzar la calidad de la enseñanza en la región. Las pruebas comenzarán ya este curso y, además de las establecidas en la ley estatal para 4º de Educación Primaria y 2º de Secundaria, el Gobierno autonómico hará también evaluaciones anuales a los escolares de los últimos cursos de ambas etapas (6º de Primaria y 4º de la ESO). Todas ellas incorporarán también las asignaturas de Inglés y Geografía e historia o Ciencias sociales junto a las habituales de Lengua y Matemáticas.

En 2023 se realizan online en marzo con e-valuM para 4º ESO

En 2024 se realizan presenciales en mayo para 2º y 4º ESO

**5 marzo 2024**

[TED 130658-2024](https://ted.europa.eu/es/notice/-/detail/130658-2024)  

[Apoyo técnico para corrección y análisis de los resultados de la evaluación para los alumnos de cuarto y sexto curso de Educación Primaria, y para los alumnos de segundo y cuarto curso de Educación Secundaria Obligatoria de la Comunidad de Madrid en el año 2024 ](https://contratos-publicos.comunidad.madrid/contrato-publico/apoyo-tecnico-correccion-analisis-resultados-evaluacion-alumnos-cuarto-sexto-curso)  
Número de expediente 322O-001-24

La regulación está en dos resoluciones, ambas de misma fecha 15 marzo 2024

La asociada a 4ºEP y 2ºESO (las que obliga LOMLOE)

>RESOLUCIÓN CONJUNTA DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA POR LA QUE SE DICTAN INSTRUCCIONES PARA LA CELEBRACIÓN DE LAS PRUEBAS DIAGNÓSTICAS DE LOS ALUMNOS DE CUARTO CURSO DE EDUCACIÓN PRIMARIA Y SEGUNDO CURSO DE EDUCACIÓN SECUNDARIA OBLIGATORIA DE LA COMUNIDAD DE MADRID PARA EL CURSO ESCOLAR 2023-2024.  
CSV 1222476858813204298948

La asociada a 6ºEP y 4ºESO (las añade Madrid)

>RESOLUCIÓN CONJUNTA DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA POR LA QUE SE DICTAN INSTRUCCIONES PARA LA CELEBRACIÓN DE LAS PRUEBAS DE LOS ALUMNOS DE SEXTO CURSO DE EDUCACIÓN PRIMARIA Y CUARTO CURSO DE EDUCACIÓN SECUNDARIA OBLIGATORIA DE LA COMUNIDAD DE MADRID PARA EL CURSO ESCOLAR 2023-2024.  
CSV 0889204556721578998904

Estas que añade Madrid están asociadas a [Ley 1/2022, de 10 de febrero, Maestra de Libertad de Elección Educativa de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-6768)  
[Disposición adicional segunda. Evaluación del sistema educativo.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-6768#da-2)  
>1. La Consejería competente en la materia de educación no universitaria, además de participar en las evaluaciones previstas en la Ley Orgánica 2/2006, de 3 de mayo, coordinadas por la Administración General del Estado, llevará a cabo aquellas evaluaciones externas, que se consideren necesarias, dirigidas a la mejora de la calidad, de la equidad y de la excelencia de la educación.  
>2. La Comunidad de Madrid podrá participar en las evaluaciones internacionales que sean coordinadas por la Administración General del Estado.

Unos detalles comunes:

* Los aplicadores son del centro  
> Séptima. Responsables de la aplicación de las pruebas.  
>5. Los aplicadores de las pruebas serán docentes del propio centro y, preferentemente, que
no impartan docencia en el grupo en el que se aplica. Habrá un aplicador por aula,
seleccionado por el director, coordinado por el jefe de estudios y supervisado por los
inspectores de educación que estén a cargo de los diferentes centros.

* Los cuadernillos los imprime el centro  
> Décima. Instrumentos para la aplicación de las pruebas.  
El director del centro, de forma telemática, recibirá los archivos con los diferentes
cuadernillos de todas las pruebas de esta evaluación y el resto de materiales necesarios
para realizar la evaluación. Todo este material será fotocopiado por el propio centro.

* El centro aporta los sobres  
> Décimo cuarta. Periodo de descanso y finalización de las pruebas  
> El director introducirá las hojas de respuesta en un sobre para la devolución de las hojas de respuesta. 

* La correción es externa   
> Décimo quinta. Corrección de las pruebas.  
>1. La corrección de las pruebas se realizará de manera externa siendo la gestión y
supervisión de la misma responsabilidad de la Dirección General de Bilingüismo y Calidad
de la Enseñanza.  
>2. Las pruebas serán corregidas por expertos en las áreas o materias realizadas.

A esta frase no le veo sentido: los cuadernillos no se usan luego ¿qué es un uso eficiente?  
> Igualmente, se procurará no escribir anotaciones o borradores en los cuadernillos,
con la finalidad de hacer un uso eficiente de los mismos.

[RESOLUCIÓN DE 16 DE ABRIL DE 2024, DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y ORGANIZACIÓN EDUCATIVA, POR LA QUE SE DICTAN NUEVAS INSTRUCCIONES RELATIVAS A LAS PRUEBAS PARA LA EVALUACIÓN DE DIAGNÓSTICO DE LOS ALUMNOS DE CUARTO CURSO DE EDUCACIÓN PRIMARIA Y SEGUNDO CURSO DE EDUCACIÓN SECUNDARIA OBLIGATORIA Y DE LAS PRUEBAS PARA LA EVALUACIÓN DE FIN DE ETAPA DE LOS ALUMNOS DE SEXTO CURSO DE EDUCACIÓN PRIMARIA Y CUARTO CURSO DE EDUCACIÓN SECUNDARIA OBLIGATORIA EN LA COMUNIDAD DE MADRID PARA EL CURSO ESCOLAR 2023-2024.](https://www.educa2.madrid.org/web/educamadrid/principal/files/secondary/ad13fd46-4e09-4e5e-a16c-7bae7d340c8a/Resolucio%CC%81n%2016%20de%20abril%20de%202024%20Pruebas%20Diagno%CC%81sticas%20y%20Pruebas%20de%20Fin%20de%20Etapa.pdf?t=1713531782192)  

> Disposición derogatoria.  
Queda expresamente derogada la Resolución conjunta de 15 de marzo
de 2024, de las Viceconsejerías de Política Educativa y de Organización
Educativa por la que se dictan instrucciones para la celebración de las pruebas
diagnósticas de los alumnos de cuarto curso de Educación Primaria y segundo
curso de Educación Secundaria Obligatoria de la Comunidad de Madrid para el
curso escolar 2023-2024.  
Queda expresamente derogada la Resolución conjunta de 15 de marzo
de 2024, de las Viceconsejerías de Política Educativa y de Organización
Educativa por las que se dictan instrucciones para la celebración de las pruebas
de los alumnos de sexto curso de Educación Primaria y cuarto curso de
Educación Secundaria Obligatoria de la Comunidad de Madrid para el curso
escolar 2023-2024.

Al menos cambian eliminar este párrafo

> Asimismo, tal y como establece el artículo 140.2 de la Ley Orgánica 2/2006, de 3 de
mayo, de Educación, los resultados de las evaluaciones del sistema educativo,
independientemente del ámbito territorial estatal o autonómico en el que se apliquen, no
podrán ser utilizados para valoraciones individuales del alumnado o para establecer
clasificaciones de los centros.

Pasa a haber pruebas muestrales y censales. 

> Tercera. Alumnos que han de realizar la evaluación.  
>1. Las pruebas deberán ser realizadas por todos los alumnos de cuarto y sexto
curso de Educación Primaria, así como de segundo y cuarto curso de Educación
Secundaria Obligatoria.  
>2. Estas pruebas serán censales y tanto su aplicación como su corrección y el
tratamiento de los resultados tendrán carácter interno. Simultáneamente, se
realizará un control muestral en un número determinado de aulas. En este
supuesto, tanto la aplicación como la corrección serán externas al centro. Los
centros en los que alguna de sus aulas tenga consideración de muestral tendrán
conocimiento de ello con la debida antelación.

Las pruebas censales se corregirán por el propio centro

> Decimosexta. Corrección de las pruebas.  
>1. La corrección de las pruebas que tengan la consideración de muestrales se
realizará de manera externa, siendo la gestión y supervisión de la misma
responsabilidad de la Dirección General de Bilingüismo y Calidad de la
Enseñanza. Las pruebas serán corregidas por expertos en las áreas o materias
realizadas.  
>2. La corrección del resto de pruebas se realizará de manera interna por los
docentes del centro que designe el director, en función de las necesidades del
servicio, el perfil del docente y el área o materia objeto de evaluación. Las
herramientas para la evaluación efectiva de las pruebas serán facilitadas
debidamente a los centros. El director distribuirá las pruebas para ser corregidas
y grabadas por los tutores y docentes correspondientes que introducirán las
respuestas en la plataforma digital destinada tal fin.  

**6 mayo 2024**  
[La Comunidad de Madrid inicia hoy las pruebas para evaluar los conocimientos de más de 280.000 alumnos - comunidad.madrid](https://www.comunidad.madrid/noticias/2024/05/06/comunidad-madrid-inicia-hoy-pruebas-evaluar-conocimientos-280000-alumnos)  

[Dos días de pruebas para evaluar los conocimientos de 280.000 alumnos de Primaria y ESO - madridiario.es](https://www.madridiario.es/dos-dias-pruebas-evaluar-conocimientos-280-000-alumnos-madrilenos-primaria-eso)  
> La Federación de Enseñanza de CC.OO Madrid ha criticado la "falta de transparencia" con la que, a su juicio, se están celebrando las pruebas de evaluación de conocimientos en Primaria y Secundaria en la Comunidad y ha acusado a la Consejería de Educación, Ciencia y Universidades de "falsear los resultados" al excluir de las mismas al alumnado con necesidades especiales.  
De hecho, el sindicato ha impugnado la resolución de 16 de abril del 2024 que anulaba otra anterior del 24 de marzo de la Consejería de Educación, Ciencia y Universidades de la Comunidad de Madrid, relativa a estas pruebas. El motivo es precisamente la exclusión que, según el sindicato, hace la Administración autonómica en estas pruebas al alumnado con necesidades educativas especiales (ACNEE), una decisión que "contraviene la normativa básica estatal", en concreto el artículo 144.4 de la LOE que establece "que las pruebas deben adaptarse a este alumnado, pero en ningún caso apartarlo", ha recordado en un comunicado la secretaria general de esta Federación, Isabel Galvín.  
Tras esta impugnación, sostiene el sindicato, la Consejería indicó que se pueden solicitar las pruebas por el sistema braille para el alumnado que tenga discapacidad visual, pero el sindicato alerta de que no se han procurado "medios para quien tenga otro tipo de discapacidad".  
Sin embargo, fuentes de la Consejería han precisado a Europa Press que "en ningún caso" se ha dado instrucción para excluir a estos alumnos, sino que se dio libertad a los directores de los centros para que participaran o no "en función del bienestar emocional del alumno", una petición, sostienen, realizada por los propios centros.  
Por otro lado, CC.OO ha recurrido las instrucciones del 24 de abril complementada por un correo que se remitió a los centros el 29 de abril por "falta de negociación colectiva" a pesar de que la aplicación de estas pruebas "afecta directamente a las condiciones laborales de docentes y equipos directivos", sostiene.  
En este sentido, ha defendido que para "adaptar, aplicar y corregir estas pruebas" es preciso "modificar el horario y aumentar su trabajo sin que esta ampliación de jornada y de actividad le sea reconocida de ninguna manera", por lo que el sindicato ha ofrecido cobertura "a todo el profesorado que quiera realizar reclamación individual para exigir compensación extraordinaria por esta tarea sobrevenida".

[Evaluaciones externas: CCOO las ha denunciado en los Tribunales y señala que se realizan en unas condiciones que desvirtúan los resultado](https://feccoo-madrid.org/noticia:693008--Evaluaciones_externas_CCOO_las_ha_denunciado_en_los_Tribunales_y_senala_que_se_realizan_en_unas_condiciones_que_desvirtuan_los_resultado&opc_id=9f666eca08d47ba26c6a09927d7abbbb)

**8 mayo 2024**

[Polémica por una prueba escolar de la Comunidad de Madrid: “Está llena de conceptos racistas” ](https://www.eldiario.es/sociedad/polemica-prueba-escolar-comunidad-madrid-llena-conceptos-racistas_1_11348499.html)  
La Consejería de Educación introduce en la prueba diagnóstica de 2º de la ESO preguntas que algunos profesores tachan de “supremacistas” y de planteamiento “eurocéntrico”: “Hablan del mestizaje, pero ¿qué es el mestizaje? La violación sistemática de mujeres indígenas”, critica un profesor 


**9 mayo 2024**

Planteo solicitud de transparecia

Solicito copia o enlace a documentación en la que se refleje, para las pruebas de evaluación de diagnóstico realizadas los días 6 y 7 de mayo de 2024  
- Contenido y fecha de la comunicación realizada a los centros en los que alguna de sus aulas ha tenido consideración de muestral.  
- Número de aulas en las que se ha realizado un control muestral, desglosado por nivel (4º Primaria, 6º Primaria, 2º ESO, 4ºESO) y titularidad del centro.  
- Gestiones realizadas para la aplicación externa de las pruebas muestrales, incluyendo el enlace al contrato si existe con una entidad externa en el que refleje el coste.  
- Gestiones realizadas para la corrección externa de las pruebas muestrales por expertos, incluyendo el enlace al contrato si existe con una entidad externa en el que refleje el coste.  
Existe como antecedente 09-OPEN-00151.0/2017 en la que se indicó que el coste asociado a aplicadores y correctores para la evaluación final ESO 2017 fue de 280516 €  

03/590293.9/24

**16 mayo 2024**
Planteo solicitud transparencia pliegos 03/626321.9/24 tras ver anuncio licitación marzo 2024

Solicito copia o enlace a los pliegos que se citan en este anuncio de licitación https://ted.europa.eu/es/notice/-
/detail/130658-2024 que indica Identificador interno: 322O-001-24 y título Apoyo técnico para corrección y
análisis de los resultados de la evaluación para los alumnos de cuarto y sexto curso de Educación Primaria, y
para los alumnos de segundo y cuarto curso de Educación Secundaria Obligatoria de la Comunidad de Madrid
en el año 2024

**22 mayo 2024**

Recibo [resolución solicitud 16 mayo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/PruebasEvaluaci%C3%B3nEduaci%C3%B3nObligatoria/2024-05-22-Resolucion_de_acceso-OPEN-00090.2-2024.pdf

>Ambas tareas, la aplicación y la corrección externa, las llevó a cabo personal docente
adscrito a la Dirección General de Bilingüismo y Calidad de la Enseñanza. 

**26 mayo 2024**

Se comenta en redes el anuncio de licitación de 1,2 M€

[twitter FiQuiPedia/status/1794773328860713240](https://x.com/FiQuiPedia/status/1794773328860713240)  
Es solo anuncio, seguramente cambiaron de idea y no han adjudicado, pero muestra el coste que ellos estimaban y que ahora recae gratis en los docentes.

**29 mayo 2024**

En portal de contratación indica que se ha actualizado y añaden

ESTE CONTRATO NO SERÁ OBJETO DE LICITACIÓN DADO QUE NO SE VA A LLEVAR A CABO.

Se publica modificción anuncio

[Este anuncio modifica la versión anterior 130658-2024  - europa.eu](https://ted.europa.eu/es/notice/-/detail/314001-2024)  

>  10. Modificación  
Versión del anuncio anterior que debe modificarse: 130658-2024  
Principal motivo de la modificación: Intención de cancelación  
Descripción: No celebración del contrato   

Mismo día firman [resolución que me notifican día 30](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/PruebasEvaluaci%C3%B3nEduaci%C3%B3nObligatoria/2024-05-30-Resolucion_de_accesod09-OPEN-00099.2-2024.pdf) en la que no responden sobre el pliegos.

**30 mayo 2024**

[El Gobierno de Ayuso fracasa en su plan de privatizar la corrección de exámenes](https://www.elplural.com/autonomias/madrid/gobierno-ayuso-plan-privatizar-correccion-examenes_330867102)  

**31 mayo 2024**

Reclamo a CTyPCM pliegos y pongo nueva solicitud

Solicito copia o enlace a la siguiente documentación asociada al Número de expediente 322O-001-24 publicado en el perfil del contratante en https://contratos-publicos.comunidad.madrid/perfil-contratante/publicidad-contrataciones
- Informe de insuficiencia de medios, según artículo 116.4.f de ley 9/2017 previo a su publicación en marzo 2024
- Informe de suficiencia de medios que ha llevado a publicar la intención de cancelación y no celebración de contrato https://ted.europa.eu/es/notice/-/detail/314001-2024 en mayo 2024

 03/700852.9/24

**5 agosto 2024**

Consejo de Transparencia y Protección de Datos (CTPD) le asigna 009/2024 CTPD

**14 septiembre 2024**

Recibo alegaciones de la consejería vía CTPD. Dicen que los pliegos no existen.  

[Alegaciones](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/PruebasEvaluaci%C3%B3nEduaci%C3%B3nObligatoria/2024-09-14-CTPD-Alegaciones+C+Educaci%C3%B3n+19.08.2024.pdf)  

> En el caso actual, lo que se planteaba era adelantar el conocimiento de las distintas
empresas ante una posible licitación posterior, que iba supeditada a una modificación
presupuestaria con el fin de obtener la suficiente cobertura económica en el contrato, por lo
que nunca se aprobaron pliegos de ningún tipo, al no haberse considerado adecuada la
citada modificación presupuestaria.  
Por tanto, por mucho que se desconozcan las características del anuncio de información
previa, considerando que supone la existencia de unos pliegos, lo cierto es que, cuando se
publica el anuncio, habitualmente, no existen los mismos, dado que se puede publican con
una antelación de 12 meses.  
En el caso concreto, no existen tales pliegos, que nunca se aprobaron, y que el anuncio no
ha conllevado una posterior publicación de la licitación, dado que las actuaciones
contempladas en el anuncio las llevaron a cabo personal docente adscrito a la Dirección
General de Bilingüismo y Calidad de la Enseñanza, por lo que no hubo necesidad de licitar
contrato ni de aprobar pliegos.  
No se pueden, consecuentemente, facilitar unos pliegos que no han sido aprobados, dado
que no llegaron a existir.  


Veo normativa permite estimar sin aclarar cómo  
[Artículo 101 Valor estimado Ley 9/2017](https://www.boe.es/buscar/act.php?id=BOE-A-2017-12902#a1-13)  

CTPD indica

> De conformidad con lo establecido en el artículo 82 de la Ley 39/2015, de 1 de
octubre, del Procedimiento Administrativo Común de las Administraciones Públicas (LPAC),
se le da traslado del citado escrito y se le concede un plazo máximo de QUINCE DÍAS para
que formule las alegaciones que considere oportunas, que podrá presentar en cualquiera
de los lugares previstos en el artículo 16.4 LPAC.


Escribo a CTPD con [Formulario de solicitud genérica](https://sede.comunidad.madrid/prestacion-social/formulario-solicitud-generica/tramitar); se trata de probar cómo responder, antes con CTyPCM era vía mail  


Asunto: Respuesta TAUD RECLAMANTE 009/2024 CTPD

Expone: Con fecha 14 septiembre 2024 recibo notificación TAUD RECLAMANTE 009/2024 CTPD con documento  	Alegaciones_C_Educacion_19.08.2024.pdf y se me indica que tengo QUINCE DÍAS para formular alegaciones que puedo presentar en cualquiera de los lugares previstos en el artículo 16.4 LPAC.
A la vista de las alegaciones y de que la administración ahora sí indica explícitamente que no existe en este caso documento de pliegos, renuncio a mi reclamación sobre pliegos. Entiendo que sí debe existir algún tipo de documentación para que la administración indicase el valor estimado de  1 212 631,74 EUR en https://ted.europa.eu/es/notice/-/detail/130658-2024, pero al no ser esa documentación "pliegos" y usar yo ese término en mi solicitud y reclamación, eso sería objeto de otra solicitud  

Solicita: Que se den por recibidas mis alegaciones

14 septiembre 2024 43/233692.9/24 


### Pruebas LOMLOE otras CCAA

#### Andalucía

[INSTRUCCIONES DE 4 DE ABRIL DE 2024, DE LA DIRECCIÓN GENERAL DE ORDENACIÓN, INCLUSIÓN, PARTICIPACIÓN Y EVALUACIÓN EDUCATIVA, POR LAS QUE SE CONCRETAN DETERMINADOS ASPECTOS RELACIONADOS CON LA REALIZACIÓN DE LA EVALUACIÓN DE DIAGNÓSTICO PARA EL CURSO ACADÉMICO 2023/2024.](https://www.juntadeandalucia.es/educacion/portals/delegate/content/48c2c2df-d704-4cf0-abf1-1e6263a04c5e)  
> sin que, en ningún caso, los resultados puedan ser
utilizados para el establecimiento de clasificaciones de los centros.

#### Aragón

[Resolución de la Secretaría General Técnica del Departamento de Educación, Ciencia y Universidades. EVALUACIÓN DE DIAGNÓSTICO.](https://educa.aragon.es/cefyca/-/asset_publisher/tHot7IKFyAW5/content/resoluci%25C3%25B3n_ed_2024?_com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_tHot7IKFyAW5_redirect=https%3A%2F%2Feduca.aragon.es%2Fcefyca%3Fp_p_id%3Dcom_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_tHot7IKFyAW5%26p_p_lifecycle%3D0%26p_p_state%3Dnormal%26p_p_mode%3Dview%26_com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_tHot7IKFyAW5_cur%3D0%26p_r_p_resetCur%3Dfalse%26_com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_tHot7IKFyAW5_assetEntryId%3D4211794)

[Resolución de la Secretaría General Técnica del Departamento de Educación, Ciencia y Universidades del Gobierno de Aragón por la que se establecen las bases que regularán la aplicación de las evaluaciones de diagnóstico (ED) para el alumnado de 4º de Educación Primaria y 2º de enseñanza secundaria en los centros educativos de Aragón que impartan estas enseñanzas durante el curso 2023-2024.](https://cefyca.catedu.es/wp-content/uploads/sites/203/2024/04/CSVHP5P25M3GD1A01PFI-RESOLUCION-SGT-APLICACION-EVALUACION-DE-DIAGNOSTICO-2024-EN-CENTROS-DE-ARAGON.pdf)  
> a) 20 horas de créditos de formación por la superación del curso de formación para personal coordinador de la evaluación de diagnóstico.  
b) Si el alumnado que participa en las pruebas es superior a 20, 1 hora de formación a
añadir por cada fracción de 10 alumnos objetivo de la etapa que coordine.  
c) 1 hora de ponencia a añadir por cada fracción de 25 alumnos objetivo de la etapa que
coordine.

#### Asturias
[ResoluCión de 14 de marzo de 2024, de la Consejería de educación, por la que se convoca la evaluación de diagnóstico de las competencias específicas adquiridas por el alumnado del 4.º curso de educación Primaria y del 2.º curso de educación secundaria obligatoria en el Principado de Asturias, correspondiente al año académico 2023-2024.](https://sede.asturias.es/bopa/2024/03/26/2024-02432.pdf)  
> Segundo.5. ... en ningún caso los resultados de esta evaluación de diagnóstico podrán ser utilizados para el establecimiento de clasificaciones.

#### Cantabria

[Resolución de 21 de marzo de 2024, por la que se dictan instrucciones para el desarrollo de la evaluación de diagnóstico de competen-
cias adquiridas por el alumnado en cuarto curso de Educación Primaria y segundo curso de Educación Secundaria Obligatoria, en el curso
2023-2024, en la Comunidad Autónoma de Cantabria.](https://boc.cantabria.es/boces/verAnuncioAction.do?idAnuBlob=401735)  
>  Los resultados de estas evaluaciones no podrán ser utilizados para el
establecimiento de cualquier tipo de clasificación de los centros.

#### Castilla La Mancha

[26/04/2024 El DOCM publicará el próximo lunes la resolución que regula el procedimiento para la aplicación de la evaluación de diagnóstico](https://www.castillalamancha.es/actualidad/galeriaimagenes/el-docm-publicar%C3%A1-el-pr%C3%B3ximo-lunes-la-resoluci%C3%B3n-que-regula-el-procedimiento-para-la-aplicaci%C3%B3n-de)  

[Resolución de 23/04/2024, de la Dirección General de Innovación Educativa y Centros, por la que se aprueban, para el curso 2023-2024, las instrucciones para la realización de la evaluación de diagnóstico del alumnado de cuarto curso de Educación Primaria y de segundo de Educación Secundaria Obligatoria, en la comunidad autónoma de Castilla-La Mancha. ](https://docm.jccm.es/docm/descargarArchivo.do?ruta=2024/04/29/pdf/2024_3281.pdf&tipo=rutaDocm)  


#### Castilla y León

[ORDEN EDU/17/2024, de 15 de enero, por la que se regula la evaluación de diagnóstico en el cuarto curso de educación primaria y en el segundo curso de educación secundaria obligatoria, y su aplicación al alumnado de los centros docentes de Castilla y León.](https://bocyl.jcyl.es/boletines/2024/01/23/pdf/BOCYL-D-23012024-1.pdf)   
> Artículo 9. Reconocimientos.  
A los docentes que participen en la corrección, codificación y grabación de las
pruebas de evaluación diagnóstica, se les reconocerá su labor mediante 1,5 créditos
de formación, de acuerdo con la Orden EDU/1057/2014, de 4 de diciembre, por la que
se regulan las modalidades, convocatoria, reconocimiento, certificación y registro de las
actividades de formación permanente del profesorado de enseñanzas no universitarias
que presta sus servicios en centros docentes sostenidos con fondos públicos en la
Comunidad de Castilla y León organizadas por la Red de formación y se establecen las
condiciones de reconocimiento de las actividades de formación organizadas por otras
entidades.

#### Comunidad Valenciana

[RESOLUCIÓN de 22 de abril de 2024, de la directora general de Innovación e Inclusión Educativa, por la que se regula la evaluación de diagnóstico del alumnado de los centros educativos sostenidos con fondos públicos de la Comunitat Valenciana para el curso 2023-2024. ](https://dogv.gva.es/datos/2024/04/26/pdf/2024_3525.pdf)  
> Quinto. Proceso y modalidad de evaluación  
>1. El alumnado realizará telemáticamente las pruebas de evaluación
de diagnóstico.  
>2. El proceso se efectuará de manera no simultanea debido a las
limitaciones técnicas que impiden conectar a todo el alumnado al mismo
tiempo.

#### Extremadura

[INSTRUCCIÓN Nº 4/2024, DE LA SECRETARÍA GENERAL DE EDUCACIÓN Y FORMACIÓN PROFESIONAL, POR LA QUE SE REGULA LA EVALUACIÓN DE DIAGNÓSTICO EN LOS CENTROS DE EDUCACIÓN PRIMARIA DE LA COMUNIDAD AUTÓNOMA DE EXTREMADURA PARA EL CURSO 2023/2024](https://www.educarex.es/pub/cont/com/0054/documentos/Instrucci%C3%B3n_4_2024_de_la_SGEFP_Evaluaci%C3%B3n_de_diagn%C3%B3stico_4_EP%28F%29.pdf)  
> La Ley 4/2011, de Educación de Extremadura, en su Título VII, Evaluación del Sistema
Educativo, así como la Ley Orgánica 3/2020, de 29 de diciembre en su artículo 140.2,
establecen que las finalidades de la evaluación del sistema educativo no podrán amparar
que los resultados de estas puedan ser utilizados para realizar valoraciones individuales del
alumnado ni podrán servir de base para establecer clasificaciones de los centros.

> 8.4. La Consejería de Educación, Ciencia y Formación Profesional reconocerá esta actividad
con 15 horas de formación (1,5 créditos) al personal corrector de la prueba, que se
inscribirá de oficio en el Registro General de Formación Permanente del Profesorado,
siempre que hayan sido registrados en el perfil “Corrector de la Evaluación” al que se
refiere el apartado 8.1 de esta instrucción.

#### Galicia

[RESOLUCIÓN de 14 de febrero de 2024, de la Dirección General de Ordenación e Innovación Educativa, por la que se dictan instrucciones para el desarrollo de la evaluación de diagnóstico de educación primaria y educación secundaria obligatoria en los centros docentes de la Comunidad Autónoma de Galicia para el curso escolar 2023/24.](https://www.xunta.gal/dog/Publicados/2024/20240223/AnuncioG0655-150224-0004_es.html)  
> Tanto al personal docente que forme parte de las comisiones de coordinación de la eva-
luación de diagnóstico en los centros educativos como al personal docente seleccionado
que auxilie dicha comisión y que participe activamente en la coordinación, aplicación, co-
rrección y/o grabación de los datos de las pruebas de evaluación de diagnóstico se le reco-
nocerán 20 horas de formación, de acuerdo con la Orden de 14 de mayo de 2013 por la que
se regulan la convocatoria, el reconocimiento, la certificación y el registro de las actividades
de formación permanente del profesorado en Galicia (DOG núm. 96, de 22 de mayo).

#### La Rioja

[Resolución 10/2024, de 22 de marzo, de la Dirección General de Innovación y Ordenación Educativa, por la que se regulan las características generales de la evaluación de diagnóstico del sistema educativo en cuarto curso de Educación Primaria y segundo curso de Educación Secundaria Obligatoria en la Comunidad Autónoma de La Rioja](http://ias1.larioja.org/boletin/Bor_Boletin_visor_Servlet?referencia=28907963-1-PDF-560550)  

#### Navarra

[Evaluaciones Externas Estandarizadas de Navarra](https://www.educacion.navarra.es/web/dpto/evaluacion-y-calidad/evaluacion/evaluacion-externa/evaluacion-de-navarra)  

[Evaluaciones Externas Estandarizadas de Navarra EVALUACIÓN DIAGNÓSTICA CENSAL 4º de Educación Primaria Curso académico 2023-2024 Protocolo de actuaciones](https://www.educacion.navarra.es/documents/27590/3047489/2.+Protocolo_4EP_23_24_castellano.pdf/b5430f05-edcb-0030-b185-b3297dd9547a)  


