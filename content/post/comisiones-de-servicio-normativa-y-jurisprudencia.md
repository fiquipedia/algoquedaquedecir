+++
author = "Enrique García"
title = "Comisiones de servicio. Normativa y jurisprudencia"
date = "2024-02-05"
tags = [
    "educación", "normativa", "Comisiones de servicio", "jurisprudencia"
]
toc = true

+++

Revisado 11 febrero 2024

## Resumen

Las comisiones de servicio son un tema poco regulado a nivel de normativa. Se intenta centralizar aquí la normativa y jurisprudencia que se citaba en los distintos posts asociados a comisiones de servicio.  

Posts relacionados:  
* [Comisiones de servicio en Madrid y opacidad](https://algoquedaquedecir.blogspot.com/2017/08/comisiones-de-servicio-en-madrid-y.html)  
* [Lo básico de normativa básica](https://algoquedaquedecir.blogspot.com/2019/01/lo-basico-de-normativa-basica.html)  
* [Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia](../comisiones-de-servicio-madrid-iv-programas-en-centros-bachillerato-de-excelencia)  
* [Ocupar puesto pagado con dinero público: libre concurrencia, igualdad, mérito, capacidad, publicidad y transparencia](https://algoquedaquedecir.blogspot.com/2017/12/ocupar-puesto-pagado-con-dinero-publico.html)  

Nota: marcadas ToDo cosas por hacer

## Detalle 

A nivel general hay que tener en cuenta que aplican las ideas generales de [Ocupar puesto pagado con dinero público: libre concurrencia, igualdad, mérito, capacidad, publicidad y transparencia](https://algoquedaquedecir.blogspot.com/2017/12/ocupar-puesto-pagado-con-dinero-publico.html), pero aquí me centro en lo que trata explícitamente comisiones de servicio.

ToDo: ;irar la regulación de las CCAA sobre comisiones de servicio, y qué normativa citan en el texto preliminar de las disposiciones que lo regulan.

ToDo: separar "normativa" según provisión / gestión nóminas si es posible (mantengo de gestión nóminas ya que puede aportar información)

### Definición legal de comisión de servicio

En normativa se define poco, pero se puede ver [Resolución de 14 de diciembre de 1992, de la Secretaría de Estado para la Administración Pública, por la que se dispone la publicación del Manual de Procedimientos de Gestión de Recursos Humanos en materia de vacaciones, permisos y licencias, comisiones de servicios y reingresos al servicio activo.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1993-1258)
> La comisión de servicios consiste en la cobertura temporal y normalmente voluntaria, por razones de urgente e inaplazable necesidad, de un puesto de trabajo que queda vacante, por un funcionario adscrito a otro destino que reúne los requisitos establecidos para su desempeño que figuran en la relación de puestos de trabajo.  

Una definición muy breve en [Jerga educativa](https://algoquedaquedecir.blogspot.com/2017/09/jerga-educativa.html)  
> Situación en la que un funcionario está realizando excepcionalmente un trabajo distinto / en un destino distinto al que tendría asignado normalmente.

Es importante tener en cuenta que no aplica a interinos  

### Normativa Estatal

[Orden de 30 de julio de 1992 sobre instrucciones para la confección de nóminas. - boe](https://www.boe.es/buscar/act.php?id=BOE-A-1992-19493)  
> 2.4 Funcionarios en comisión de servicios: Se unirán los siguientes documentos:  
a) Acuerdo de nombramiento para puesto de trabajo, según modelo F.1, y formalización de la toma de posesión en el puesto de trabajo, según modelo F.2R, o cuando la competencia para acordar el cese y el nombramiento corresponda a la misma autoridad, únicamente se acompañará el modelo F.5R.  
b) Certificado de baja en nómina, conforme al anexo III.a).  
2.5 Reincorporación de funcionarios procedentes de comisiones de servicios: Se unirá a la documentación referida en el punto 2.4 el acuerdo de comisión de servicios, según el modelo F7.  
...  
3. Bajas en nómina.  
Se considerará como baja la exclusión de la nómina de un perceptor que figuraba en la del mes anterior. Las bajas en nómina se justificarán con la documentación que en cada caso se indica.  
3.1 Por cambio de puesto de trabajo: Se unirán los documentos siguientes:  
...  
b) Cuando se trate de desempeñar otro puesto en comisión de servicios, se acompañará el modelo F.7

[Anexos (pdf de 190 páginas)](https://www.boe.es/boe/dias/1992/08/13/pdfs/C00001-00190.pdf)  


[Resolución de 14 de diciembre de 1992, de la Secretaría de Estado para la Administración Pública, por la que se dispone la publicación del Manual de Procedimientos de Gestión de Recursos Humanos en materia de vacaciones, permisos y licencias, comisiones de servicios y reingresos al servicio activo.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1993-1258)
> CAPITULO II  
**Comisiones de servicios**  
>1. Introducción.  
La comisión de servicios consiste en la cobertura temporal y normalmente voluntaria, por razones de urgente e inaplazable necesidad, de un puesto de trabajo que queda vacante, por un funcionario adscrito a otro destino que reúne los requisitos establecidos para su desempeño que figuran en la relación de puestos de trabajo.  
Así definida reglamentariamente, la comisión de servicios es una fórmula de práctica común en la realidad administrativa cuya justificación no se encuentra en todos los casos en la causa de urgente o inaplazable necesidad en la provisión de una vacante. Al lado de circunstancias que motivan plenamente la utilización de este tipo de comisiones de servicios para resolver problemas coyunturales de la organización, se encuentran otros en los que **la comisión de servicios es la solución más fácil y cómoda para sortear las lentitudes y rigideces de los concursos.**  
**Los excesos en la utilización de esta fórmula han generado una opinión negativa que afecta a la credibilidad de la política de selección y movilidad de los funcionarios públicos**. Con independencia de que sea necesario mejorar otros elementos de esta política, como son los mecanismos de selección del personal, la mayor frecuencia y periodicidad de los concursos o la utilización de otros procedimientos previstos en la legislación y aún no plenamente utilizados como la redistribución de efectivos, los gestores de recursos humanos cuidarán de utilizar las comisiones de servicios con criterios de adecuación a su causa justificativa, exclusivamente para la provisión de aquellos puestos de trabajo vacantes que sea necesario cubrir temporalmente por auténticas razones de urgente e inaplazable necesidad. 

[Real Decreto 1732/1994, de 29 de julio, sobre provisión de puestos de trabajo reservados a funcionarios de Administración local con habilitación de carácter nacional. (derogado por Real Decreto 128/2018)](https://www.boe.es/buscar/act.php?id=BOE-A-1994-18621)  
[Artículo 30. Nombramientos provisionales.](https://www.boe.es/buscar/act.php?id=BOE-A-1994-18621#a30)  
> 1. De acuerdo con las corporaciones afectadas, en el caso de existencia de más de un posible candidato, y, en todo caso, con la autorización de la corporación en que se cesa y previa conformidad de los interesados, el órgano competente de la comunidad autónoma respectiva podrá efectuar nombramientos provisionales, con carácter prioritario sobre las formas de provisión previstas en los artículos 31, 32, 33 y 34, en puestos vacantes o en aquellos otros que no estén desempeñados efectivamente por sus titulares por encontrarse en alguna de las circunstancias siguientes:  
a) **Comisión de servicios.**  

[Artículo 31. Acumulaciones.](https://www.boe.es/buscar/act.php?id=BOE-A-1994-18621#a31)  
> 1. El órgano competente de la comunidad autónoma respectiva, en el ámbito de su territorio, podrá autorizar a los funcionarios con habilitación de carácter nacional que se encuentren ocupando un puesto de trabajo a ellos reservado a desempeñar asimismo en una entidad local próxima las funciones reservadas, en los supuestos contemplados en el apartado 1 del artículo anterior y por el tiempo de su duración, cuando, previa solicitud del ayuntamiento, no hubiese sido posible efectuar nombramiento provisional o **comisión de servicios**, imposibilidad que ha debido quedar lo suficientemente acreditada en el expediente.

[Artículo 32. Comisiones de servicio.](https://www.boe.es/buscar/act.php?id=BOE-A-1994-18621#a32)  


[Real Decreto 1777/1994, de 5 de agosto, de adecuación de las normas reguladoras de los procedimientos de gestión de personal a la Ley 30/1992, de 26 de noviembre, de Régimen Jurídico de las Administraciones Públicas y del Procedimiento Administrativo Común.](https://www.boe.es/buscar/act.php?id=BOE-A-1994-19272)  
[Artículo 2. Supuestos de eficacia desestimatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-1994-19272#a2)  
> Las solicitudes formuladas en los siguientes procedimientos administrativos de gestión de personal se podrán entender desestimadas una vez transcurridos, sin que se hubiera dictado resolución expresa, los plazos máximos de resolución señalados a continuación:  
...  
c) Autorización de **comisiones de servicios**: Tres meses.  

[Real Decreto 364/1995, de 10 de marzo, por el que se aprueba el Reglamento General de Ingreso del Personal al servicio de la Administración general del Estado y de Provisión de Puestos de Trabajo y Promoción Profesional de los Funcionarios Civiles de la Administración general del Estado.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8729)  
[Artículo 36. Formas de provisión.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8729#a36)  
> 1. Los puestos de trabajo adscritos a funcionarios se proveerán de acuerdo con los procedimientos de **concurso, que es el sistema normal de provisión**, o de libre designación, de conformidad con lo que determinen las relaciones de puestos de trabajo en atención a la naturaleza de sus funciones.  
> 2. Cuando las necesidades del servicio lo exijan, los puestos de trabajo podrán cubrirse mediante redistribución de efectivos o por reasignación de efectivos como consecuencia de un Plan de Empleo.  
> 3. Temporalmente podrán ser cubiertos mediante **comisión de servicios** y adscripción provisional, en los supuestos previstos en este Reglamento.

[Artículo 64. Comisiones de servicios.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8729#a64)  
> 1. Cuando un puesto de trabajo quede vacante podrá ser cubierto, en caso de urgente e inaplazable necesidad, en **comisión de servicios** de carácter voluntario, con un funcionario que reúna los requisitos establecidos para su desempeño en la relación de puestos de trabajo.  
> 2. Podrán acordarse también **comisiones de servicios** de carácter forzoso. Cuando, celebrado concurso para la provisión de una vacante, ésta se declare desierta y sea urgente para el servicio su provisión podrá destinarse con carácter forzoso al funcionario que preste servicios en el mismo Departamento, incluidos sus Organismos autónomos, o Entidad Gestora de la Seguridad Social, en el municipio más próximo o con mejores facilidades de desplazamiento y que tenga menores cargas familiares y, en igualdad de condiciones, al de menor antigüedad.  
> 3. Las citadas **comisiones de servicios** tendrán una duración máxima de un año prorrogable por otro en caso de no haberse cubierto el puesto con carácter definitivo y se acordarán por los órganos siguientes:  
a) La Secretaría de Estado para la Administración Pública, cuando la **comisión** suponga cambio de Departamento ministerial y se efectúe en el ámbito de los servicios centrales, o en el de los servicios periféricos si se produce fuera del ámbito territorial de una Comunidad Autónoma y, en ambos casos, previo informe del Departamento de procedencia.  
b) Los Subsecretarios, en el ámbito de su correspondiente Departamento ministerial, así como entre el Departamento y sus Organismos autónomos y, en su caso, Entidades Gestoras.  
c) Los Presidentes o Directores de los Organismos autónomos y de las Entidades Gestoras y Servicios Comunes de la Seguridad Social, respecto de los funcionarios destinados en ellos.  
d) Los Delegados del Gobierno y Gobernadores civiles, en el ámbito de sus respectivas competencias, cuando se produzcan entre servicios de distintos Departamentos, previo informe del Departamento de procedencia.  
> 4. Si la **comisión** no implica cambio de residencia del funcionario, el cese y la toma de posesión deberán producirse en el plazo de tres días desde la notificación del acuerdo de **comisión de servicios**; si implica cambio de residencia, el plazo será de ocho días en las comisiones de carácter voluntario y de treinta en las de carácter forzoso.  
> 5. El puesto de trabajo cubierto temporalmente, de conformidad con lo dispuesto en los apartados 1 y 2 del presente artículo, **será incluido, en su caso, en la siguiente convocatoria de provisión por el sistema que corresponda.**  
> 6. A los funcionarios en **comisión de servicios** se les reservará el puesto de trabajo y percibirán la totalidad de sus retribuciones con cargo a los créditos incluidos en los programas en que figuren dotados los puestos de trabajo que realmente desempeñan

[Disposición adicional tercera. Comisiones de servicios a funcionarios docentes para la provisión de puestos en las Administraciones educativas.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8729#datercera)  
> Las **comisiones de servicios** a funcionarios docentes en las Administraciones educativas se concederán por el Subsecretario de Educación y Ciencia en el caso de los funcionarios docentes a que se refiere la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo, o por el Rector de la Universidad para los funcionarios docentes dependientes de las Universidades, en las condiciones que en cada caso se establezcan por los mencionados órganos en función de las necesidades de las distintas unidades, programas y actividades educativas o de apoyo.

[Real Decreto 365/1995, de 10 de marzo, por el que se aprueba el Reglamento de Situaciones Administrativas de los Funcionarios Civiles de la Administración General del Estado.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8730)  
[Artículo 3. Servicio activo.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8730#a3)  
> Los funcionarios se hallan en situación de servicio activo:  
...  
c) Cuando se encuentren en **comisión de servicios**.  

[Resolución de 2 de septiembre de 2002, de la Secretaría de Estado para la Administración Pública, por la que se establecen los nuevos modelos de documentos para la inscripción y anotación en el Registro Central de Personal de diversos actos administrativos de personal y nuevos modelos de títulos administrativos y hojas de servicio del personal funcionario y laboral al servicio de la Administración General del Estado.](https://www.boe.es/buscar/doc.php?id=BOE-A-2002-18328)  
Documento F.2.R: Se utilizará para las tomas de posesión por nuevo ingreso, reingreso, concurso, comisión de servicios, etc.

[Anexos (pdf de 85 páginas)](https://www.boe.es/boe/dias/2002/09/21/pdfs/A33760-33844.pdf)


[Real Decreto 1138/2002, de 31 de octubre, por el que se regula la Administración del Ministerio de Educación, Cultura y Deporte en el exterior.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-21183)  
CAPÍTULO II  
Estructura y régimen de personal de las Consejerías de Educación  
[Artículo 10. Los asesores técnicos.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-21183#a10)  
> 4. Los nombramientos se efectuarán por un primer periodo de un curso escolar en régimen de **comisión de servicios**, prorrogable por un segundo periodo de dos cursos escolares, y por un tercer periodo de dos cursos más hasta alcanzar el **límite máximo de cinco cursos escolares**, salvo que el interesado solicite su retorno a España en las condiciones que se establezcan por el Ministerio de Educación, Cultura y Deporte, desaparezca la necesidad que dio origen a la provisión del puesto de trabajo o sea objeto de evaluación desfavorable, de acuerdo con lo dispuesto en el apartado siguiente.

[Artículo 11. Puestos vacantes.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-21183#a11)  
> 1. Los puestos vacantes que no puedan ser cubiertos de acuerdo con lo previsto en el artículo anterior se cubrirán, hasta su provisión reglamentaria, mediante **comisiones de servicio**, por el plazo de un año, entre funcionarios que cumplan los mismos requisitos que se establezcan para participar en el sistema ordinario de provisión.  
...  
> 3. Cuando en el desarrollo de actividades propias de la acción educativa en el exterior se ponga en marcha alguna experiencia de carácter innovador, se podrá destinar a los funcionarios necesarios en **comisión de servicios** por un año. Una vez institucionalizada la experiencia, los puestos deberán ser cubiertos por el sistema ordinario.

CAPÍTULO IV  
Régimen del personal docente destinado en centros y programas en el exterior  
[Artículo 15. Puestos vacantes.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-21183#a15)  
> 1. Las vacantes que no puedan ser cubiertas por el procedimiento establecido en el artículo anterior se cubrirán, hasta su provisión reglamentaria, mediante **comisiones de servicios**, por el plazo de un año, entre funcionarios docentes que cumplan los mismos requisitos que se establezcan para participar en el mencionado procedimiento.  
...  
> 3. Cuando en el desarrollo de actividades propias de la acción educativa en el exterior se ponga en marcha alguna experiencia de carácter innovador, se podrá destinar a los funcionarios docentes necesarios en **comisión de servicios** por un año. Una vez institucionalizada la experiencia, las plazas deberán ser cubiertas por el sistema ordinario.

[Ley Orgánica 2/2006, de 3 de mayo, de Educación.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899)  
[Artículo 122 bis. Acciones destinadas a fomentar la calidad de los centros docentes. Añadido en LOMCE, no en LOMLOE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=177&tn=1&p=20131210#a122bis)  
> 4. Para la realización de las acciones de calidad, el director del centro dispondrá de autonomía para adaptar, durante el período de realización de estas acciones, los recursos humanos a las necesidades derivadas de los mismos. Las decisiones del director deberán fundamentarse en los principios de mérito y capacidad y deberán ser autorizadas por la Administración educativa correspondiente, que se encargará de que se cumpla la normativa aplicable en materia de recursos humanos. La gestión de los recursos humanos será objeto de evaluación específica en la rendición de cuentas. El director dispondrá de las siguientes facultades:  
...  
c) Cuando el puesto se encuentre vacante, sin estar cubierto de manera definitiva por funcionario de carrera docente, y exista financiación adecuada y suficiente, proponer, de forma motivada, la prórroga en la **comisión de servicios** del funcionario de carrera docente que hubiera venido ocupando el puesto de forma provisional o, en su caso, el nombramiento de nuevo en el mismo puesto del funcionario interino docente que lo venía desempeñando, cuando, en ambos supuestos, habiendo trabajado en los proyectos de calidad, sean necesarios para la continuidad de los mismos. En todo caso, en la propuesta deberá quedar debidamente justificada la evaluación positiva del funcionario en el desarrollo de su actividad dentro del correspondiente proyecto de calidad, así como la procedencia e importancia de su continuidad en el puesto que venía desarrollando dentro del proyecto para asegurar la calidad y la consecución de objetivos.

Curiosamente se menciona cómo renovar, pero no cómo se obtienen inicialmente 

[Real Decreto Legislativo 5/2015, de 30 de octubre, por el que se aprueba el texto refundido de la Ley del Estatuto Básico del Empleado Público.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719)  
[Artículo 81. Movilidad del personal funcionario de carrera.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a81)  
> 3. En caso de urgente e inaplazable necesidad, los puestos de trabajo podrán proveerse con carácter provisional debiendo procederse a su convocatoria pública dentro del plazo que señalen las normas que sean de aplicación.

No se cita explícitamente comisión


[Real Decreto 128/2018, de 16 de marzo, por el que se regula el régimen jurídico de los funcionarios de Administración Local con habilitación de carácter nacional.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-3760)  
[Artículo 48. Otras formas de cobertura de puestos reservados.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-3760#a4-10)  
> 1. Con independencia de la provisión de puestos de trabajo por concurso y libre designación y de la asignación de puestos mediante nombramientos de primer destino, las Comunidades Autónomas podrán efectuar la cobertura de los puestos reservados a funcionarios con habilitación de carácter nacional, mediante los siguientes tipos de nombramiento:  
a) Nombramientos provisionales.  
b) **Comisiones de servicios.**  
...  
> 2. La cobertura de un puesto mediante nombramiento provisional, **comisión de servicios** y acumulación, implicará el cese automático, en su caso, del funcionario interino o accidental, que lo estuviera desempeñando

[Artículo 49. Nombramientos provisionales.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-3760#a4-11)  
>...  
Se incluyen, a efectos de estos nombramientos, los puestos ocupados con nombramientos accidentales e interinos, y aquellos otros que no estén desempeñados efectivamente por sus titulares por encontrarse en alguna de las circunstancias siguientes:  
a) **Comisión de servicios**.  

[Artículo 50. Acumulaciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-3760#a5-2)  
>1. El órgano competente de la Comunidad Autónoma respectiva, en el ámbito de su territorio, podrá autorizar a los funcionarios de Administración Local con habilitación de carácter nacional que se encuentren ocupando un puesto de trabajo a ellos reservado, a desempeñar asimismo en otra Entidad Local las funciones reservadas a la misma u otra subescala o categoría, en los supuestos contemplados en el apartado 1 del artículo anterior y por el tiempo de su duración, cuando, previa solicitud del Alcalde o Presidente, no hubiese sido posible efectuar nombramiento provisional o **comisión de servicios**, imposibilidad que ha debido quedar lo suficientemente acreditada en el expediente.  

[Artículo 51. Comisiones de servicios.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-3760#a5-3)  
> 4. En todos los supuestos anteriores, la **comisión de servicios** se efectuará a petición de la Administración interesada y previo informe favorable de la Entidad Local donde el funcionario preste sus servicios.  
> 5. El tiempo transcurrido en esta situación será tenido en cuenta a efectos de consolidación del grado personal correspondiente al nivel del puesto desde el que se produce la comisión, salvo que se obtuviese destino definitivo en el puesto de trabajo desempeñado en **comisión de servicios** o en otro del mismo o inferior nivel, en cuyo caso, a instancia del funcionario, podrá ser tenido en cuenta a efectos de consolidación del grado correspondiente a este último.  
El nombramiento en **comisión de servicios** implicará la pérdida de la valoración de la permanencia en el puesto definitivo desde el que se efectúa la comisión, a efectos de concursos.

[Artículo 52. Nombramientos accidentales.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-3760#a5-4)  
> ...  
> 2. Para que se pueda efectuar un nombramiento accidental, el puesto deberá estar vacante, o no encontrarse desempeñado efectivamente por su titular, por encontrarse en alguna de las circunstancias siguientes:  
a) **Comisión de servicios**.

[Artículo 54. Efectos de la provisión o reincorporación.]((https://www.boe.es/buscar/act.php?id=BOE-A-2018-3760#a5-6)  
> La provisión del puesto de forma definitiva, la reincorporación del titular en los supuestos contemplados en este capítulo, o el nombramiento provisional, en **comisión de servicios** o acumulación, en el caso de que el puesto se estuviera desempeñando por funcionario accidental o interino, determinará, automáticamente, el cese de quien viniera desempeñándolo.

**2023**

[twitter FiQuiPedia/status/1611335029254230016](https://twitter.com/FiQuiPedia/status/1611335029254230016)  
![](https://pbs.twimg.com/media/Flycq5FXwAANPY8?format=jpg)  

Trámite de audiencia Anteproyecto de ley de la Función Pública de la Administración General del Estado
https://hacienda.gob.es/es-ES/Normativa%20y%20doctrina/NormasEnTramitacion/Paginas/AudienciaAbiertas.aspx

[DOCUMENTO SOMETIDO A TRÁMITE DE AUDIENCIA E INFORMACIÓN PÚBLICA EL 3 DE ENERO DE 2023 ANTEPROYECTO DE LEY DE LA FUNCIÓN PÚBLICA DE LA ADMINISTRACIÓN DEL ESTADO](https://www.hacienda.gob.es/Documentacion/Publico/NormativaDoctrina/Proyectos/TEXTO-LeyGeneralFuncionPublica-APL-LFPAE-AUDIENCIA.pdf)

> Entre las novedades destaca una nueva figura, la provisión temporal de puestos, que sustituye a la actual comisión de servicios, que tendrá una duración de un año, en el que deberá proveerse con carácter definitivo.  

Pongo correo

---

Se indica "Entre las novedades destaca una nueva figura, la provisión temporal de puestos, que sustituye a la actual comisión de servicios, que tendrá una duración de un año, en el que deberá proveerse con carácter definitivo."  
En artículo 60 se indica "La duración máxima de esta provisión temporal será de un año, en el que deberá proveerse con carácter definitivo. Los requisitos y procedimiento para la provisión temporal se determinarán reglamentariamente."  
Mis observaciones son:  
- Sustituir la figura de comisión de servicio implica revisar toda la norma que la mencione, o aclarar explícitamente que a partir de la entrada en vigor de esta ley, toda mención a comisión de servicio en normativa previa hará mención a provisión temporal de puestos.  
- Dado que hay sentencia TS de 2019 que obliga a que las convocatorias de comisión de servicio sean públicas, [Roj: STS 2091/2019 - ECLI:ES:TS:2019:2091](https://www.poderjudicial.es/search/openCDocument/47c54a4d73e1a196d4b27000bc7b5614a6bbd551b89e9fda), considero que atendiendo al principio de transparencia deben ser públicas:  
--- Las vacantes cubiertas mediante comisión de servicio  
--- Las fechas en que dichas vacantes han sido cubiertas mediante comisión de servicio, para conocer su duración  
- A nivel de ley se debe fijar de manera objetiva la temporalidad de esta figura (fijar un máximo de 3 o 5 años, pero fijar explícitamente un valor), o se puede convertir en lo mismo que las actuales comisiones de servicio que sin un regulación clara, se encadenan de manera arbitraria.  
Por ejemplo en [Artículo 10. Los asesores técnicos de Real Decreto 1138/2002](https://www.boe.es/buscar/act.php?id=BOE-A-2002-21183#a10) se indica explícitamente hasta 5 años.  
Sé que esta norma no tiene carácter básico, pero los avances en transparencia en la provisión de comisiones de servicio de una administración son un ejemplo a seguir por otras administraciones, al igual que el inmovilismo en la opacidad y los retrocesos.

--- 

#### Normativa comisiones y concurso de traslados

[Real Decreto 1364/2010, de 29 de octubre, por el que se regula el concurso de traslados de ámbito estatal entre personal funcionario de los cuerpos docentes contemplados en la Ley Orgánica 2/2006, de 3 de mayo, de Educación y otros procedimientos de provisión de plazas a cubrir por los mismos.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-16551)  
[Artículo 2. Concurso.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-16551#a2)  
> 1. El concurso es el procedimiento normal de provisión de las plazas o puestos vacantes dependientes de las Administraciones educativas, a cubrir por el personal docente.

Al citar "normal" está reiterando que la comisión es excepcional 

En mayo 2018 veo que la convocatoria de concurso de traslados cita comisiones varias veces

[Resolución de 23 de octubre de 2017, de la Dirección General de Recursos Humanos, por la que se convoca concurso de traslados de ámbito autonómico de los Cuerpos de Maestros, Catedráticos y Profesores de Enseñanza Secundaria, Profesores Técnicos de Formación Profesional, Catedráticos y Profesores de Escuelas Oficiales de Idiomas, Catedráticos y Profesores de Música y Artes Escénicas, y Catedráticos, Profesores y Maestros de Taller de Artes Plásticas y Diseño.](https://www.bocm.es/boletin/CM_Orden_BOCM/2017/10/30/BOCM-20171030-9.PDF)  
> Base tercera  
Participantes  
...  
e)  Los funcionarios que tengan concedida una **Comisión de Servicios** de carácter humanitario, deberán participar en el concurso de traslados al objeto de valorar, la concesión, en su caso, de una nueva comisión.  

> 5.4.1.1.1.  Funcionarios que no hayan participado en los concursos de traslados para los cursos 2013-2014, 2014-15, 2015-2016 y 2016-17 en la Comunidad de Madrid.  
...  
b)  Para la justificación del subapartado 1.1.3 “Servicios en centros de especial dificultad”: Certificación expedida por la Dirección de Área Territorial, acreditativa de los servicios efectivos prestados como funcionario de carrera en el centro desde el que participa, cuando este centro esté calificado de especial dificultad. A los efectos previstos en el subapartado 1.1.3 del baremo (anexo 2 de la presente Resolución), no se computará el tiempo de permanencia fuera del centro en **comisión de servicios**, en servicios especiales o en licencia por estudios o supuestos análogos.

>CONCURSO DE TRASLADOS PARA FUNCIONARIOS DE LOS CUERPOS  DE MAESTROS, CATEDRÁTICOS Y PROFESORES DE ENSEÑANZA SECUNDARIA, PROFESORES TÉCNICOS DE FORMACIÓN PROFESIONAL Y CUERPOS QUE  IMPARTEN ENSEÑANZAS ARTÍSTICAS E IDIOMAS 2017/2018  INSTRUCCIONES PARA CUMPLIMENTAR LA SOLICITUD  
...  
4.- Datos del destino:  
 Todos los participantes deberán hacer constar el centro de destino. Aquellos que se encuentren en **comisión de servicios**, deberán consignar el centro en el que tienen su destino definitivo. Si carece de destino definitivo, deberá  hacer  constar  el  destino  provisional  en  el  presente  curso.

>ANEXO 2  
BAREMO  
No obstante, no se computará a estos efectos el tiempo que se haya permanecido fuera del centro en situación de servicios especiales, en **comisión de servicios**, con licencias por estudios o en supuestos análogos que supongan que no hay desempeño efectivo del puesto  de trabajo.

> DISPOSICIONES COMPLEMENTARIAS  
SEGUNDA.- Antigüedad  
Procederá  asignar  puntuación  por  el  subapartado  1.1.3  a  los  participantes  en  el  concurso  que  se  encuentren  en  las  siguientes situaciones:  
...  
-Los  participantes  de  los  subapartados  1.1.1  y  1.1.2  que  tengan  concedida  una  **comisión  de  servicio**  en  otra  plaza, puesto o centro que tenga la calificación de especial dificultad.

### Normativa Madrid

En 2022 localizo este enlace: no cita normativa pero sí tipos y plazos  
[Provisión de Puestos de Trabajo de Personal Funcionario. Comisiones de servicio](https://www.comunidad.madrid/gobierno/espacios-profesionales/provision-puestos-trabajo-personal-funcionario#comisiones-servicio)

En 2018 estaba operativo este enlace que sí citaba normativa  
[La provisión de puestos de trabajo. D. G. de Presupuestos y Recursos Humanos - archive.org](http://www.madrid.org/cs/Satellite?c=CM_InfPractica_FA&cid=1132041093201&idConsejeria=1109266187242&idListConsj=1109265444710&idOrganismo=1109266227393&language=es&pagename=ComunidadMadrid%2FEstructura&pv=1132041099555&sm=1109266100977)

Citaba  
• Ley 1/1986, de 10 de abril, de la Función Pública de la Comunidad de Madrid.  
• Ley 4/1989, de 6 de abril, de provisión de puestos de trabajo reservados a personal funcionario de la Comunidad de Madrid.  
• Decreto 203/2000, de 14 de septiembre, por el que se dictan las reglas aplicables a los procedimientos de asignación de puestos de trabajo reservados a personal funcionario de la Comunidad de Madrid en los supuestos de pérdida del que se viniera desempeñando y de reingreso al servicio activo.  
• Orden 923/1989, de 20 de abril, de la Consejería de Hacienda, por la que se aprueban las bases generales que han de regir en las convocatorias para provisión de puestos por los sistemas de concurso de méritos y libre designación  
• Orden 2094/1990, de 31 de agosto, del Consejero de Hacienda, por la que se regula el sistema general que ha de regir en las diferentes convocatorias que se publiquen para los concursos de provisión de puestos de trabajo.  

[Ley 30/1984, de 2 de agosto, de medidas para la reforma de la Función Pública.](https://www.boe.es/buscar/act.php?id=BOE-A-1984-17387)  
[Disposición transitoria octava](https://www.boe.es/buscar/act.php?id=BOE-A-1984-17387#dtoctava)  
> 1. Durante el período de transferencias de medios personales a las Comunidades Autónomas, queda prohibida la concesión de **comisión de servicios** al personal destinado en los servicios periféricos a los servicios centrales afectados por dichas transferencias.  
A efectos de transferencias de funcionarios, el personal que actualmente se encuentre en **comisión de servicios** se considerará destinado en su puesto de origen.  
...  
> 4. Previa petición de las Comunidades Autónomas, la Administración del Estado podrá conceder a su personal de los servicios centrales **comisiones de servicio** de hasta dos años de duración con el fin de cooperar, o prestar asistencia técnica, a la Administración de las Comunidades Autónomas.  

[Ley 1/1986, de 10 de abril, de la Función Pública de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734)  
[Sección tercera. De la provisión de puestos de trabajo](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#stercera-3)  

[Artículo 49](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#a49)  
> 1. Los puestos de trabajo adscritos a funcionarios se proveerán por el procedimiento de concurso como sistema normal o el de libre designación como sistema excepcional, de conformidad con lo que establezcan las relaciones de puestos de trabajo.  
> 2. Las convocatorias para la provisión de puestos de trabajo, tanto por concurso como por libre designación, se aprobarán por el Consejero respectivo. Asimismo, corresponderá al titular de cada Consejería la resolución de las mismas, previo informe de la Consejería de Hacienda.  
> 3. Las convocatorias, así como sus respectivas resoluciones, se publicarán en el ‟Boletín Oficial de la Comunidad de Madrid".

Ese texto lo interpreto como que en Madrid solo hay dos tipos de provisión: vía concurso o vía libre designación y **AMBOS DEBEN PUBLICARSE EN BOCM**  

[Artículo 53](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#a53)  
> 1. Cuando un puesto de trabajo quede vacante podrá acordarse para su cobertura, en caso de urgente e inaplazable necesidad, una **comisión de servicios** de carácter voluntario, a favor de un funcionario que reúna los requisitos establecidos para su desempeño en la relación de puestos de trabajo, por un plazo máximo de un año, prorrogable por otro.  
> 2. Asimismo, cuando un puesto haya sido declarado desierto en un concurso y sea urgente para el servicio su provisión, podrá destinarse en **comisión de servicios** de carácter forzoso a un funcionario que preste servicios en la misma Consejería, por un plazo máximo improrrogable de seis meses.  
> 3. A petición de otras Administraciones Públicas podrán autorizarse **comisiones de servicio** a favor de los funcionarios propios de la Comunidad de Madrid en los mismos términos del apartado 1.  
> 4. Las **comisiones de servicios** señaladas en los apartados anteriores, conllevarán derecho a reserva de puesto de trabajo y serán autorizadas por el Consejero de Hacienda, salvo en los supuestos en que las mismas no supongan cambio de Consejería, en cuyo caso serán autorizadas por el Consejero respectivo.  
> 5. Los funcionarios en **comisión de servicios** percibirán las retribuciones del puesto de trabajo realmente desempeñado.  
> 6. Los funcionarios de otras Administraciones Públicas podrán prestar sus servicios en la Comunidad de Madrid mediante **comisión de servicios**, de acuerdo con lo que establezcan las relaciones de puestos de trabajo, por un plazo máximo de un año, prorrogable por otro, previa autorización del órgano competente de la Administración de procedencia del funcionario

Pero el tema es que esa ley y esos artículos no aplican para comisiones servicio docentes Madrid. Son de una sección de funcionarios de la comunidad de Madrid; no para docentes.  
De hecho, esos artículos 49 y 53 están junto a otros que hablan de carrera profesional, y los docentes no tenemos de eso.  

Se ve en [Artículo 3 Ley 1/986](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#a3)  
> 2. En aplicación de la Ley podrán dictarse normas específicas para adecuarla a las peculiaridades del personal sanitario, **docente** e investigador  

En principio indica que puede haber algo específico para docentes, pero aplicaría como supletoria  

[Ley 4/1989, de 6 de abril, de Provisión de Puestos de Trabajo Reservados a Personal Funcionario de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-1989-12125)  
[Disposición derogatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-1989-12125#dd)  
> Quedan derogadas las normas de igual o inferior rango, dictadas con anterioridad, que se opongan a lo previsto en la presente, excepto las relativas al personal docente y sanitario

El ámbito de aplicación es funcionario de la comunidad.  
Se habla de concurso pero parece que se excluye docente y sanitario que tiene otra regulación  

[ORDEN de 17 de enero de 2019, de la Consejería de Economía, Empleo y Hacienda, por la que se dictan Instrucciones para la Gestión de las Nóminas del Personal de la Comunidad de Madrid para 2019.](https://www.bocm.es/boletin/CM_Orden_BOCM/2019/01/22/BOCM-20190122-14.PDF)  
> Artículo 25 **Comisión de Servicios**  
> 1. A los funcionarios a los que se les conceda una **comisión de servicios** para desempeñar puestos de trabajo en otras Administraciones, les serán liquidadas por días sus retribuciones de carácter fijo y periodicidad mensual, tanto básicas como complementarias, incluidas las pagas extraordinarias.  
> 2. Los funcionarios en **comisión de servicios**, procedentes de otras Administraciones, serán considerados como de “nuevo ingreso”, a efectos de percepción de retribuciones; éstas se abonarán por días, de conformidad con lo dispuesto en el artículo 14.2 b) de la presente Orden.

> Artículo 32 Devengo de retribuciones, pagas extraordinarias y pagas adicionales del personal que percibe las retribuciones conforme al Real Decreto Ley 3/1987  
> 1. Retribuciones:  
1.1. Las retribuciones básicas y complementarias que se devenguen con carácter fijo y periodicidad mensual se harán efectivas por mensualidades completas, salvo los siguientes supuestos en que se liquidarán por días:  
a) En el supuesto de que el trabajador se traslade de un Centro a otro, como consecuencia de un concurso de traslado, adscripción funcional a otros centros, **concesión de comisiones de servicios**, etc., los centros de origen deberán efectuar, con cargo a sus respectivos presupuestos, la correspondiente liquidación de haberes, en las que se incluirá la parte proporcional de la paga extraordinaria y de la paga adicional a la que el personal cesante tenga derecho. Asimismo, emitirán certificación de haberes a efectos de su alta en nómina en los nuevos Centros, en la que se reseñará la cuantía y tiempo de servicios liquidado, detallando la liquidación de la paga extra, si se ha disfrutado algún tipo de permiso, vacaciones, días de libre disposición, o cualquier otra consideración que se entienda necesaria.

[Decreto 15/2024, de 7 de febrero, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2024/02/09/BOCM-20240209-1.PDF)

No menciona explícitamente comisiones de servicio, pero en el IES San Mateo es el único método de provisión, ver [Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia](../comisiones-de-servicio-madrid-iv-programas-en-centros-bachillerato-de-excelencia)  

### Normativa Castilla y León

[ORDEN ADM/756/2009, de 9 de marzo, por la que se aprueban los nuevos modelos de documentos para la inscripción y anotación en el Registro General de Personal de diversos actos administrativos de gestión de personal al servicio de la Administración de la Comunidad de Castilla y León.](https://empleopublico.jcyl.es/web/jcyl/binarios/156/19/Orden_ADM_756_2009.pdf?blobheader=application%2Fpdf%3Bcharset%3DUTF-8&blobheadername1=Cache-Control&blobheadername2=Expires&blobheadername3=Site&blobheadervalue1=no-store%2Cno-cache%2Cmust-revalidate&blobheadervalue2=0&blobheadervalue3=JCYL_FuncionPublica&blobnocache=true)  
> F.7 Acuerdo de comisión de servicios.  
F.7.R Acuerdo de comisión de servicios.

### Normativa Galicia

[DECRETO 49/2009, do 26 de febreiro, sobre o exercicio das competencias da Comunidade Autónoma de Galicia respecto dos/as funcionarios/as con habilitación de carácter estatal.](https://www.xunta.gal/dog/Publicados/2009/20090316/AnuncioEA86_gl.html)  
> Artigo 39º.-Sistemas de provisión non definitivos.  
> 1. Os postos reservados que por calquera razón non estivesen desempeñados por funcionarios/as con habilitación de carácter estatal, poderán cubrirse mediante nomeamento provisional, **comisión de servizos**, acumulación, funcionario/a accidental ou nomeamento de funcionario/a interino/a, por esta orde de preferencia coas excepcións indicadas nos artigos seguintes.  
> ...
> Artigo 43º.-**Comisión de servizos.**   
>1. A consellería competente en materia de réxime local poderá conferir **comisións de servizos** nos supostos previstos na normativa a funcionarios/as con habilitación de carácter estatal con destino definitivo na Comunidade Autónoma galega para ocuparen postos reservados en entidades locais galegas, cando non for posible efectuar un nomeamento provisional a favor de funcionario/a da mesma subescala e categoría a que está reservado o posto, imposibilidade que deberá quedar o suficientemente acreditada no expediente.  
Terá preferencia a **comisión de servizos** a favor do funcionario/a con habilitación de carácter estatal da mesma subescala e categoría do posto; no seu defecto, da mesma subescala e distinta categoría, respecto dun nomeamento provisional en favor de habilitado/a de diferente subescala.  
>2. A **comisión de servizos** efectuarase por petición da entidade interesada e coa conformidade do funcionario e da entidade onde preste servizos e/ou destino.  
>3. A concesión destas **comisións de servizos** terá lugar polo prazo máximo dun ano, sendo prorrogable por outro ano máis.  
>4. Se a comisión non implica cambio de residencia do/a funcionario/a, o cesamento e a toma de posesión deberán producirse no prazo de tres días desde a notificación da autorización da comisión. Se implica cambio de residencia, o prazo será de cinco días. 

### Normativa Rioja

**6 marzo 2017**

[Aprobado un nuevo procedimiento para la provisión de puestos de trabajo de funcionarios mediante comisiones de servicio](https://www.europapress.es/la-rioja/noticia-aprobado-nuevo-procedimiento-provision-puestos-trabajo-funcionarios-comisiones-servicio-20170306133620.html)

[Resolución de 28 de junio de 2017, de la Dirección General de Educación por la que se se apruebas las instrucciones por las que se regulan la organización y funcionamiento de los centros docentes públicos situados en la Comunidad Autónoma de La Rioja para el curso académico 2017/2018.](http://www.larioja.org/larioja-client/cm/edu-recursos-humanos/images?idMmedia=913605)  
>2. COMISIONES DE SERVICIO  
En tanto se apruebe la normativa específica reguladora de la provisión de puestos docentes por funcionarios en comisión de servicios, la solicitud de comisiones de servicio del personal docente para el curso siguiente se realizará entre el 1 y el 30 de abril, salvo en casos excepcionales que serán debidamente justificados.

### Jurisprudencia y análisis jurídicos

Lo asociado al Defensor del Pueblo lo separo. De momento aquí recopilo, luego intentaré hacer aquí un resumen rápido, por ejemplo:  
* 2019: STS que obliga a que haya convocatoria  


Los análisis jurídicos los pongo al principio si no tiene una fecha concreta

[Provisión de puestos de trabajo - guiasjuridicas](https://guiasjuridicas.laley.es/Content/Documento.aspx?params=H4sIAAAAAAAEAE1QwU6EMBD9m142MRA9eOkF8LCJGoNovA4wC82WDnYG3P69w-ImNmk673Xem9d-LxhTgxexHU2OHQXkQ48Hxri6zpHhFCikyTZxQSPQss0MdLKAr6iz-Va7FRtolafYYyySVkICvka2eWZ4pJ9XWN0AovYFxN3L9b19-sq2ld8_PuRmxbjNt59uwCBoRjeMz7pl7wdmxyUFieSPKm2Jzk1V7nyTZrQvKLDDGsKAGoMRYje-gQJNu0zqS3fA82Ub5k5JyavyBJ7R-HBW4v2q-cvIH8FtucD_p3fbYhHRuK2E_c50Xs8KBEvwGPrbQ2GefarJ629c8Ux8C3MMJURaGL3NfgH5uJBligEAAA==WKE)



**22 octubre 2012**

[STS, a 22 de octubre de 2012 - ROJ: STS 6834/2012](https://www.poderjudicial.es/search/AN/openDocument/19e2a75b09e96a66/20121112)  
RESUMEN: Reglamento de selección y formación de Policía Vasca. Valoración como mérito del trabajo desarrollado en comisión de servicios y adscripción provisional. Conformidad a derecho. No vulneración del principio de reserva de ley ni del de igualdad. 

> Asimismo, tampoco puede aceptarse la tesis que propugna el Sindicato recurrente negando toda virtualidad a los servicios prestados en los puestos desempeñados con carácter provisional sobre la base de la arbitrariedad con que, a su juicio, se viene haciendo uso por la Administración de estos sistemas de provisión. Más allá de que efectivamente **en la asignación de estos puestos con carácter provisional no están presentes los principios de mérito y capacidad** en igual medida que en los casos de cobertura mediante el concurso y la libre designación, lo cierto es que estas formas provisionales de provisión de puestos de trabajo nacen, en principio, para atender necesidades organizativas perentorias de la Administración correspondiente, siendo innegable que durante su desempeño el funcionario adquiere una experiencia y un bagaje de conocimientos con indudable reflejo en su perfil profesional, de manera que **si el Sindicato recurrente tuviera noticia de una utilización ilegítima por parte de la Administración de estos mecanismos de provisión de puestos lo que debería hacer es combatir caso por caso esas adscripciones provisionales o comisiones de servicio en principio ilegítimas**, no pudiéndose admitir, por el contrario, que ese supuesto uso abusivo que, a juicio del Sindicato recurrente, se está haciendo de dichos mecanismos de provisión de puestos de trabajo -que no olvidemos están previstos y contemplados legalmente- se pueda traducir en la negación de toda trascendencia al tiempo en que un funcionario ha venido desempeñando efectivamente aunque provisionalmente un puesto de trabajo, por cuanto a juicio de esta Sala nada impide que estos servicios sean considerados como un mérito a los efectos previstos en la modificación operada por el Decreto 120/2010.

**16 diciembre 2015** 

[Roj: STSJ GAL 9702/2015 - ECLI:ES:TSJGAL:2015:9702](https://www.poderjudicial.es/search/AN/openDocument/40b9231eb7ad1663/20160121)

>TERCERO .- **Hemos de subrayar que las comisiones de servicio son excepcionales en su adopción y limitadas temporalmente**, circunstancia que dados los antecedentes del caso y la tutela de la sentencia a ejecutar,
conducen a la carga de la Administración de robustecer las razones por las que se opta por la comisión
de servicio ante tan inaudita prolongación de la misma a la luz del elocuente dato de que se vuelve con la
misma figura de provisión temporal y respecto de la misma persona. Recordemos pues que las situaciones
administrativas de los funcionarios locales se rigen por la normativa básica del Estado y por la legislación
autonómica, resultando supletoria la normativa general no básica del Estado ( art.140 del Texto Refundido).
Así, **el carácter temporal de las comisiones de servicio es subrayado por toda la legislación del ramo**. De un
lado, el art.30 bis del Decreto Legislativo 1/2008, de 13 de Marzo por el que se aprueba la Ley de Función
Pública de Galicia, hoy derogado pero aplicable en las coordenadas enjuiciadas (añadido por el art.22 de la
Ley 15/2010 , de 39 de Diciembre): "Esta comisión de servicios tendrá una duración de seis meses, prorrogable
excepcionalmente por otros seis, en el caso de que se mantengan las necesidades de servicio o funcionales.
Los puestos de trabajo vacantes, cubiertos temporalmente por esta comisión de servicios, se incluirán en los
correspondientes concursos de traslados."  
De otro, el Real Decreto 364/1995, de 10 de marzo, por el que se aprueba el Reglamento General de ingreso del
personal al servicio de la Administración General del Estado y de provisión de puestos de trabajo y promoción
profesional de los funcionarios civiles de la Administración General del Estado, cuyo artículo 64 dispone: "3.
Las citadas comisiones de servicios tendrán una duración máxima de un año prorrogable por otro en caso de
no haberse cubierto el puesto con carácter definitivo (...)5. El puesto de trabajo cubierto temporalmente, de
conformidad con lo dispuesto en los apartados 1 y 2 del presente artículo, será incluido, en su caso, en la siguiente
convocatoria de provisión por el sistema que corresponda."  
De ahí que la correcta ejecución de la sentencia se salvaguardaba con una imperiosa, convincente, razonada
y detallada motivación de acudir a la comisión de servicios y a la misma persona, exigencias que no se han
cumplido.  
En efecto, **lleva cubierta en comisión de servicios desde el 16 de Julio de 2008 lo que plantea incluso las posibles responsabilidades disciplinarias y en su caso penales de quienes propician tan anómala situación mas allá de la duración máxima prorrogada de tal figura, y que encierra una prolongación ilegítima de funciones a sabiendas de su irregularidad**.  

**14 enero 2016**

[STSJ Castilla-La Mancha, a 14 de enero de 2016 - ROJ: STSJ CLM 140/2016](https://www.poderjudicial.es/search/AN/openDocument/bf1e69c3210cc8af/20160222)

> Se pretende valorar los méritos obtenidos en los puestos provistos por dicho sistema en los últimos cuatro años, pese a que **dicha situación no puede prolongarse más allá del plazo máximo de dos años**, es decir, con infracción del límite previsto en la normativa de aplicación para esta forma de provisión de los puestos de trabajo, lo que implica una clara situación de ventaja a favor de los funcionarios que en su día accedieron a los puestos convocados en comisión de servicios y que han continuado ocupándolos pese a agotarse el límite máximo de permanencia en dicha situación. Sin embargo, esto no solo no es así sino que, además, los méritos van referidos a los cuatro años anteriores a la fecha de publicación de la convocatoria, lo que hace aún más difícil que un funcionario que no haya ocupado uno de los puestos convocados en ese período temporal, pueda obtenerlo en el concurso objeto de impugnación. Todo lo que implica que la Administración ha actuado, en la convocatoria, no solo de forma arbitraria al restringir, sin justificación aparente, el período temporal del cómputo de determinados méritos, tanto generales como específicos, sino con desviación de poder, en la medida en que se utiliza una fórmula aparentemente legal para tratar de consolidar la situación en que se encuentran los funcionarios que desempeñan los puestos ofertados en comisión de servicios.

**2017**

[Las comisiones de servicios reiteradas como práctica administrativa fraudulenta](https://dialnet.unirioja.es/servlet/articulo?codigo=6201009)  
> Autores: [Octavio Manuel Fernández Hernández](https://dialnet.unirioja.es/servlet/autor?codigo=3163678)
> Localización: [Consultor de los ayuntamientos y de los juzgados: Revista técnica especializada en administración local y justicia municipal](https://dialnet.unirioja.es/servlet/revista?codigo=2575), ISSN 0210-2161, [Nº. 22, 2017](https://dialnet.unirioja.es/ejemplar/473492) (Ejemplar dedicado a: Las comisiones de servicios reiteradas), págs. 2669-2676

**22 febrero 2017**

[Roj: SAN 478/2017 - ECLI:ES:AN:2017:478](https://www.poderjudicial.es/search/AN/openDocument/150cb92fe5ce1e3c/20170307)

>3. Y, ciertamente la cuestión controvertida se reduce a la interpretación que haya de realizarse del artículo
64, párrafos 1 y 5 del Real Decreto 364/1995, de 10 de marzo , por el que se aprueba el Reglamento General
de Ingreso del Personal al Servicio de la Administración General del Estado y de Provisión de Puestos de
Trabajo y Promoción Profesional de los Funcionarios Civiles de la Administración General del Estado, incisos
que han sido ya analizados por esta Sala que se ha pronunciado sobre la interpretación de dicho precepto
y, más concretamente, sobre el significado que haya de atribuirse al inciso "en su caso" del apartado 5
del artículo 64. Y hemos reiterado que **el precepto indicado no establece una obligación incondicional de incorporar al concurso siguiente las plazas que se encuentren cubiertas mediante Comisión de Servicio**, pero
sin desconocer que la expresión "en su caso" no viene sino a modular la obligación que previamente el precepto
acaba de imponer a la Administración, de manera que tal expresión hace exigible que las razones por las que
las vacantes que se encuentren cubiertas en Comisión de Servicio no sean incluidas en la inmediatamente
siguiente convocatoria. Ello exige, en definitiva, razones que justifiquen el ejercicio de las potestades de
autoorganización invocadas por el Abogado del Estado para precisamente excluir del concurso convocado
dichas plazas.

**15 marzo 2017**

[De las comisiones de servicio que pasan a ser vicio](https://delajusticia.com/2017/03/15/de-las-comisiones-de-servicio-que-pasan-a-ser-vicio/)

**17 marzo 2017**

[Comisiones de servicios, ¿igualdad, mérito, capacidad y publicidad?](https://administracionespublicas.wordpress.com/2017/03/17/comisiones-de-servicios-igualdad-merito-capacidad-y-publicidad/)


**7 noviembre 2018**

[Prórroga de comisión de servicios en puesto vacante de habilitado de carácter nacional. STJ Galicia 7 noviembre de 2018.  - contenciosos.com](https://contenciosos.com/funcion-publica/prorroga-de-comision-de-servicios-en-puesto-vacante-de-habilitado-de-caracter-nacional-stj-galicia-7-noviembre-de-2018/)

**28 mayo 2019**

[STS, a 28 de mayo de 2019 - ROJ: STS 1675/2019](https://www.poderjudicial.es/search/AN/openDocument/ac200b2160f327cf/20190604)  
RESUMEN: Función pública. Petición plaza en comisión de servicio. El tipo de silencio administrativo es negativo por ser un expediente iniciado de oficio.  

**24 junio 2019**

[STS, a 24 de junio de 2019 - ROJ: STS 2091/2019](https://www.poderjudicial.es/search/AN/openDocument/c01b8213f1053762/20190701)  
RESUMEN: Función Pública. Cobertura de puestos de trabajo en comisión de servicios por funcionario de la TGSS. Requisitos para convocar la comisión de servicios.

>TERCERO.- El artículo 81.3 del EBEP dice que "en caso de urgente e inaplazable necesidad los puestos de trabajo podrán proveerse con carácter provisional debiendo procederse a su convocatoria pública dentro del plazo que señalen las normas que sean de aplicación". Pues bien, el auto de 3 de julio marzo de 2017 identificó que **la cuestión que tiene interés casacional objetivo para la formación de jurisprudencia es la siguiente: si a la vista de la redacción de los citados preceptos "es o no imprescindible que la cobertura de puestos de trabajo en comisión de servicios deba ir precedida de una convocatoria pública**, habida cuenta que -teniendo en cuenta el tenor literal del primero de aquellos preceptos- no consta la existencia de normas de aplicación que señalen un plazo específico para proceder a tal convocatoria ".  
...  
>4º Por tanto, cuando la causa que justifica la comisión de servicios es que haya una plaza vacante cuya cobertura es urgente e inaplazable, si como medida transitoria se acuerda cubrirla en comisión de servicios -obviamente voluntaria-, la comisión de servicios debe ofertarse mediante convocatoria pública y hacerlo, en su caso, dentro del plazo que prevea el ordenamiento funcionarial respectivo.  
>5º Si en la normativa de desarrollo no se prevé un plazo concreto para ofertarla, tal silencio podrá percutir en la atención a esas necesidades urgentes, esto es, a cuándo debe acordarse la comisión de servicios y cuánto tiempo puede mantenerse la plaza sin ser servida hasta que se oferte en comisión de servicios, pero no a cómo debe acordarse su cobertura para lo cual **es exigible ex lege que sea mediante convocatoria y que sea pública**. Tal exigencia es coherente con el principio de igualdad en el acceso al desempeño de cargos y funciones públicas, para así evitar tratos preferentes en beneficio de la carrera profesional del funcionario comisionado.  
> 6º A la exigencia de convocatoria pública hay que añadir otras garantías y así es como cobra sentido en el ámbito de la Administración General del Estado las deducibles del artículo 64 del RGIPPT: partiendo del presupuesto general -que exista vacante y que sea urgente e inaplazable cubrirla- se regula su duración máxima y prorrogabilidad, que el designado cuente con las exigencias previstas en la relación de puestos de trabajo para ocupar la plaza en cuestión, la competencia para acordarla, que en su caso se oferte la plaza en la siguiente convocatoria para la provisión por el sistema que corresponda, más aspectos relacionados con la condiciones de trabajo del adjudicatario.  
> 7º **La convocatoria pública a la que se refiere el artículo 81.3 del EBEP no implica** -máxime si concurren necesidades urgentes e inaplazables- **aplicar las exigencias y formalidades procedimentales propias de los sistemas de provisión ordinarios**, en especial el concurso, en el que se presentan y valoran méritos, se constituyen órganos de evaluación, etc.: **bastará el anuncio de la oferta de la plaza en comisión de servicios, la constatación de que el eventual adjudicatario cuenta con los requisitos para ocuparla según la relación de puestos de trabajo y su idoneidad para desempeñar la plaza vacante.**  

**3 julio 2019**

[Supremo fin al vicio de la falta de publicidad de las comisiones de servicio - delajusticia](https://delajusticia.com/2019/07/03/supremo-fin-al-vicio-de-la-falta-de-publicidad-de-las-comisiones-de-servicio/)

**11 noviembre 2019**

[La comisión de servicios: reflexiones prácticas sobre la novedosa jurisprudencia del Tribunal Supremo.](https://elconsultor.laley.es/Content/Documento.aspx?params=H4sIAAAAAAAEAMtMSbF1CTEAAiNLczMTM7Wy1KLizPw8WyMDQ0sDcyNjkEBmWqVLfnJIZUGqbVpiTnEqANpRUCA1AAAAWKE)  
Ana Mª BARRACHINA ANDRÉS  
Letrado-Asesor Jurídico del Ayuntamiento de Alicante  
El Consultor de los Ayuntamientos, Nº 11, Sección Empleo público local, Noviembre 2019, pág. 55, Wolters Kluwer  
Diario La Ley, Nº 9511, 11 de Noviembre de 2019, Wolters Kluwer  

> Resumen  
Recientemente hemos conocido dos importantes Sentencias dictadas por el Tribunal Supremo en relación con la publicidad y a los efectos del silencio administrativo en las comisiones de servicios, que sin duda provocan un nuevo panorama en el uso de esta figura a la hora de proveer puestos de trabajo vacantes en la Administración.  
El presente trabajo aborda, de forma práctica, las repercusiones de estos pronunciamientos judiciales y analiza de forma completa la regulación de esta figura a fin de procurar el respeto a las garantías fijadas por el Tribunal Supremo  

Las sentencias que cita son de 28 mayo 2019 y 24 junio 2019 

**24 mayo 2021**

[STS, a 24 de mayo de 2021 - ROJ: STS 2044/2021](https://www.poderjudicial.es/search/openDocument/d091f38f966acaef)  
RESUMEN: FUNCIÓN PÚBLICA Y PERSONAL. Deber de motivación exigible en las resoluciones administrativas, que acuerdan el cese de funcionarios públicos en puestos de libre designación.

**2 junio 2023**

[El TSXG anula, solo en los casos que afectan a los recurrentes, criterios de la Xunta para cubrir puestos con comisiones de servicios en la Administración de Justicia](https://www.poderjudicial.es/cgpj/es/Poder-Judicial/Noticias-Judiciales/El-TSXG-anula--solo-en-los-casos-que-afectan-a-los-recurrentes--criterios-de-la-Xunta-para-cubrir-puestos-con-comisiones-de-servicios-en-la-Administracion-de-Justicia-)

**29 junio 2023**

[Comunicado JJpD sobre las Comisiones de Servicios](http://www.juecesdemocracia.es/2023/06/29/comunicado-jjpd-sobre-las-comisiones-de-servicios/)

Desde Juezas y Jueces para la democracia queremos expresar nuestra honda preocupación y rechazo por el uso abusivo del sistema de comisiones de servicio y refuerzos por parte del Consejo General del Poder Judicial, previa propuesta de las Salas de Gobierno, para la provisión de órganos judiciales.

### Defensor del Pueblo

**16 julio 2018**  

[Informe anual Defensor del Pueblo 2018: Anexos](https://www.defensordelpueblo.es/wp-content/uploads/2019/06/E-1-Recomendaciones.pdf)  
Se cita Recomendación de fecha de salida 16/07/2018  
Con texto asociado a queja nº 16017048  
[Duración máxima de las comisiones de servicio de los funcionarios - defensordelpueblo](https://www.defensordelpueblo.es/resoluciones/duracion-maxima-de-las-comisiones-de-servicio/)  

**2 agosto 2018**

[Carácter excepcional y provisional de las comisiones de servicio en la provisión de puestos de trabajo](https://www.defensordelpueblo.es/resoluciones/respetar-el-caracter-excepcional-y-provisional-de-las-comisiones-de-servicio-en-la-provision-de-puestos-de-trabajo/)

**11 julio 2019**

[Comisiones de servicio en el Servicio Madrileño de Salud.](https://www.defensordelpueblo.es/resoluciones/servicio-madrileno-de-salud-comisiones-de-servicio/)


[DEFENSOR DEL PUEBLO Informe anual 2021 y debates en las Cortes Generales Volumen I. Informe](https://www.defensordelpueblo.es/wp-content/uploads/2022/03/Informe_anual_2021.pdf)  
> Comisiones de servicio  
Las comisiones de servicio indebidamente prorrogadas han sido objeto, un año más, de
la intervención de esta institución. Estas prácticas se traducen en un severo
incumplimiento de la normativa aplicable, pues se marca como máximo un plazo de dos
años (uno prorrogable por otro), lo cual implica un perjuicio en los intereses legítimos del
colectivo de funcionarios de carrera que aspiran a ocupar esas plazas.  
La Administración no está obligada a ofertar en los concursos todas las plazas
vacantes, pues ello no se deduce del texto refundido de la Ley del Estatuto Básico del Empleado Público, ya que está dotada de potestad para decidir qué vacantes han de
ofertarse.  
A juicio del Defensor del Pueblo, el equilibrio que es preciso mantener entre las
necesidades a cubrir y la política de ajustes presupuestarios en materia de personal no
exime a las administraciones de cumplir con sus obligaciones legales, ni justifica la
necesidad de prorrogar en exceso el sistema de comisión de servicio y adscripción
provisional para cubrir los puestos vacantes. La decisión de convocar o no los citados
puestos es de la propia Administración, que, teniendo la habilitación legal y
reglamentaria para ello, ha decidido utilizar un sistema extraordinario, como es el de la
comisión de servicio y prolongar su uso por períodos más que excesivos.  
Por ello, se ha recomendado a las administraciones que se respete el carácter
excepcional y provisional de las comisiones de servicio y, por otra parte, que en las
sucesivas convocatorias se motiven las razones y criterios que avalen la falta de
cobertura a través de concurso ordinario, tanto de plazas vacantes como de plazas que
estén cubiertas en comisión de servicio, a efectos de conciliar el interés de los
funcionarios que se materializa en las legítimas expectativas de su cobertura.

[DEFENSOR DEL PUEBLO. Informe anual 2022. Volumen I](https://www.defensordelpueblo.es/wp-content/uploads/2023/03/Defensor-del-Pueblo-Informe-anual-2022.pdf)  
> Comisiones de servicio  
La cobertura de puestos mediante comisiones de servicio no significa que estas puedan
adjudicarse sin la debida motivación y al margen de los principios de igualdad, mérito y
capacidad que rigen la provisión de puestos de trabajo en la función pública. Ningún
precepto excluye la aplicación de los citados principios a las comisiones de servicio,
antes al contrario, la aplicación de estos principios en este ámbito evita la opacidad y
permite el control de la actuación pública, reduciendo la arbitrariedad.  
En este sentido, la desestimación de la solicitud de acceso a los documentos
incluidos en los expedientes instruidos para la cobertura de comisiones de servicio en la
Guardia Civil ha sido motivo de varias quejas presentadas por funcionarios de este
cuerpo y objeto de actuaciones del Defensor del Pueblo.  
Es criterio de esta institución que ante las solicitudes para conocer los méritos
valorados y los criterios tenidos en cuenta por la Administración, para la concesión de
una comisión de servicios, se facilite al peticionario que no ha sido elegido la información
sobre los elementos valorativos y las puntuaciones que han conducido a la elección de la
persona escogida entre todos los candidatos. Las razones por las que un solicitante
resulta adjudicatario son, precisamente, el presupuesto básico de su concesión. No
obstante, la Dirección General de la Guardia Civil ha discrepado de este criterio y no ha
aceptado las Resoluciones del Defensor del Pueblo en este sentido.

**9 mayo 2022**

[Información sobre los elementos valorativos en una comisión de servicio.](https://www.defensordelpueblo.es/resoluciones/informacion-sobre-los-elementos-valorativos-en-una-comision-de-servicio/)

### Aspectos legales en las comisiones de servicio de educación en Madrid

ToDo 

No tiene que tener todos los requisitos de un concurso, pero ¿se cuando no hay "urgencia" sino es "programado" como el caso de educación puede exigir publicar más elementos que la conovocatoria, como el baremo?

Tener claro qué parte de la provisión general vía comisiones aplica o no a docentes y aplica o no a CCAA. Hay normativa anterior a cesión de competencias a las CCAA 

#### ¿Hay normativa básica sobre comisiones que deba cumplir Madrid?

[Ley 1/1986, de 10 de abril, de la Función Pública de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734)  
[Artículo 4](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#a4)  
> La Legislación estatal será **supletoria** en las materias no reguladas por la presente Ley y demás disposiciones de la Comunidad de Madrid sobre su personal.

Esto viene a decir que la normativa estatal no es básica

#### ¿Cuál es la duración máxima de una comisión de servicio?

El propio informe del Defensor del Pueblo de 2021 indica que límite son 2 años:

[DEFENSOR DEL PUEBLO Informe anual 2021 y debates en las Cortes Generales Volumen I. Informe](https://www.defensordelpueblo.es/wp-content/uploads/2022/03/Informe_anual_2021.pdf)  
> Comisiones de servicio  
...**se marca como máximo un plazo de dos años (uno prorrogable por otro)**...

Lo dicen sentencias  
[STSJ Castilla-La Mancha, a 14 de enero de 2016 - ROJ: STSJ CLM 140/2016](https://www.poderjudicial.es/search/AN/openDocument/bf1e69c3210cc8af/20160222)
> Se pretende valorar los méritos obtenidos en los puestos provistos por dicho sistema en los últimos cuatro años, pese a que **dicha situación no puede prolongarse más allá del plazo máximo de dos años**, es decir, con infracción del límite previsto en la normativa de aplicación para esta forma de provisión de los puestos de trabajo

En sentencia 2015 sobre comisión desde 2008 se está indicando que no pueden ser 7 años 

[Roj: STSJ GAL 9702/2015 - ECLI:ES:TSJGAL:2015:9702](https://www.poderjudicial.es/search/AN/openDocument/40b9231eb7ad1663/20160121)  
> En efecto, **lleva cubierta en comisión de servicios desde el 16 de Julio de 2008** lo que plantea incluso las posibles responsabilidades disciplinarias y en su caso penales de quienes propician tan **anómala situación mas allá de la duración máxima prorrogada de tal figura, y que encierra una prolongación ilegítima de funciones a sabiendas de su irregularidad**.  

Lo indica la ley estatal, aunque no es básica  
[Real Decreto 364/1995, de 10 de marzo, por el que se aprueba el Reglamento General de Ingreso del Personal al servicio de la Administración general del Estado y de Provisión de Puestos de Trabajo y Promoción Profesional de los Funcionarios Civiles de la Administración general del Estado.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8729)  
[Artículo 64. Comisiones de servicios.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-8729#a64)  
> 3. Las citadas comisiones de servicios tendrán **una duración máxima de un año prorrogable por otro** en caso de no haberse cubierto el puesto con carácter definitivo y se acordarán por los órganos siguientes:  

Lo indica la normativa de Madrid  
[Ley 1/1986, de 10 de abril, de la Función Pública de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734)  
[Artículo 53](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#a53)  
> 1. Cuando un puesto de trabajo quede vacante podrá acordarse para su cobertura, en caso de urgente e inaplazable necesidad, una comisión de servicios de carácter voluntario, a favor de un funcionario que reúna los requisitos establecidos para su desempeño en la relación de puestos de trabajo, por **un plazo máximo de un año, prorrogable por otro.**  

Sin embargo Madrid dice que son 6 años para educación  

[2021-07-05 Respuesta Madrid Comisiones Servicio Defensor Pueblo 20028606](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2021-07-05-RespuestaMadridComisionesServicioDefensorPueblo2_20028606.pdf)  
> ...las comisiones de servicios podrán ser
renovadas por decisión de la Dirección General de Recursos Humanos y de la
dirección general competente en el programa, a petición del centro educativo,
en tanto se consolida la plaza para ser ofertada dentro de los concursos de
traslados, de acuerdo con lo previsto en la planificación general educativa y, en
todo caso, **por un periodo máximo de cuatro años y excepcionalmente dos años más.**

Decreto 15/2024, de 7 de febrero, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2024/02/09/BOCM-20240209-1.PDF)  
No menciona explícitamente comisiones de servicio, pero en el IES San Mateo es el único método de provisión, ver [Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia](../comisiones-de-servicio-madrid-iv-programas-en-centros-bachillerato-de-excelencia)  
> Artículo 5  
El profesorado asignado a docencia en centros y aulas de excelencia podrá permanecer en las correspondientes plazas por un **período continuado máximo de seis cursos académicos.**   

El ampliar a 6 años (sin ofertar la plaza como vacante para su provisión sin comisión) se puede hacer si se justifica según comenta jurisprudencia  

[Roj: SAN 478/2017 - ECLI:ES:AN:2017:478](https://www.poderjudicial.es/search/AN/openDocument/150cb92fe5ce1e3c/20170307)  
> Y hemos reiterado que **el precepto indicado no establece una obligación incondicional de incorporar al concurso siguiente las plazas que se encuentren cubiertas mediante Comisión de Servicio**, pero
sin desconocer que la expresión "en su caso" no viene sino a modular la obligación que previamente el precepto
acaba de imponer a la Administración, de manera que tal expresión hace exigible que las razones por las que
las vacantes que se encuentren cubiertas en Comisión de Servicio no sean incluidas en la inmediatamente
siguiente convocatoria. Ello exige, en definitiva, razones que justifiquen el ejercicio de las potestades de
autoorganización invocadas por el Abogado del Estado para precisamente excluir del concurso convocado
dichas plazas.

Pero Madrid lo está haciendo por ley, lo que lo veo irregular.  
Madrid argumenta que el programa de excelencia es algo especial y por eso rechazó la alegación sobre incumplir artículo 53.1 de Ley 1/1986  

* [ MAIN de 12 de diciembre de 2023 tras audiencia e informe jurídico.](https://www.comunidad.madrid/transparencia/sites/default/files/main_decreto_excelencia_2023-12-12.pdf)  
> 3. Permanencia del profesorado PEB: la permanencia «de tres y seis años, a la que hace
mención el […] artículo 5 del Proyecto de Decreto, vulnera lo preceptuado en el
mencionado art. 53.1 de la Ley 1/1986, de 10 de abril, de la Función Pública de la
Comunidad de Madrid».  
De nuevo debe recordarse aquí lo explicado para las alegaciones primera y segunda del
Sr. ……………………  

La ley dice que las comisiones son para **urgente e inaplazable necesidad**, y pone por defecto un límite en 1+1 años, y Madrid está haciendo por ley que una "urgente e inaplazable necesidad" dure 6 años.

#### ¿Es una comisión de servicios un caso de libre designación?

Una libre designación de un funcionario (la libre designación también puede aplicar a no funcionarios) supone una comisión, pero una comisión no supone necesariamente libre designación

15 septiembre 2022 veo que Madrid, en alegaciones a RDACTPCM220/2022 sobre 09-OPEN-00064.6/2022, indica 

> A este respecto, cabe reseñar que **el proceso selectivo del personal para cubrir la Oferta de 180 plazas de asesores técnico docentes** tipo A en la Dirección General de Bilingüismo y Calidad de la Enseñanza para funcionarios de carrera de cuerpos docentes de enseñanzas no universitarias **se realizó mediante el procedimiento de libre designación.**"  

Se está diciendo que se usan comisiones de servicio para provisión por libre designación (la convocatoria indicaba que eran comisiones de servicio). Eso es grieta en la opacidad de las comisiones de servicio en ese caso, porque las libres designaciones implican por [Artículo 49.3](https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#a49) publicar resolución, y en resolución se publican nombres en BOCM.

Se puede poner este ejemplo reciente, que publica nombre y NIF ofuscado  
[ORDEN 2665/2022, de 7 de septiembre, del Vicepresidente, Consejero de Educación y Universidades, por la que se resuelve la convocatoria aprobada por Orden 2290/2022, de 9 de agosto, del Vicepresidente, Consejero de Educación y Universidades, corregida por Orden 2321/2022, de 17 de agosto, del Vicepresidente, Consejero de Educación y Universidades, para la provisión del puesto de Coordinador/a de las Unidades Técnicas del Instituto Superior Madrileño de Innovación Educativa por el procedimiento de Libre Designación.](https://bocm.es/boletin/CM_Orden_BOCM/2022/09/14/BOCM-20220914-1.PDF)



