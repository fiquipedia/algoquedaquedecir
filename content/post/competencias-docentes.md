+++
author = "Enrique García"
title = "Competencias docentes"
date = "2025-01-04"
tags = [
    "educación", "docentes", "competencias"
]
toc = true

+++

Revisado 17 enero 2025

## Resumen

En enero 2025 se habla de un borrador de Marco de Competencias Profesionales Docentes (MCPD). Está relacionado con el estatuto docente

Posts relacionados:  
* [Acceso docente: MIR](http://algoquedaquedecir.blogspot.com/2018/01/acceso-docente-mir.html)  
* [Competencia digital docente](http://algoquedaquedecir.blogspot.com/2021/11/competencia-digital-docente.html)  

## Detalle

**27 enero 2022**  
[24 medidas para cambiar la profesión docente  - eldiariodelaeducacion](https://eldiariodelaeducacion.com/2022/01/27/24-medidas-para-cambiar-la-profesion-docente/)  

[Documento para debate. 24 propuestas de reforma para la mejora de la profesión docente. Enero 2022](https://drive.google.com/file/d/1Kmipe1R4bJpj-nNBUz-yqdjx8YdF-jlv/view)  
pdf 36 páginas

**7 febrero 2022**  
[Algunas consideraciones sobre el documento de 24 propuestas de reforma para la mejora de la profesión docente - eldiariodelaeducacion](https://eldiariodelaeducacion.com/porotrapoliticaeducativa/2022/02/07/algunas-consideraciones-sobre-el-documento-de-24-propuestas-de-reforma-para-la-mejora-de-la-profesion-docente/)  

**7 abril 2022**
[Ser docente en la complejidad. Cualidades humanas y competencias profesionales. Ideas para el debate sobre un marco de competencias profesionales docentes (MCPD) - porotrapoliticaeducativa](https://porotrapoliticaeducativa.org/2022/04/06/ser-docente-en-la-complejidad-cualidades-humanas-y-competencias-profesionales-ideas-para-el-debate-sobre-un-marco-de-competencias-profesionales-docentes-mcpd/)  

**30 diciembre 2024**  
[Un grupo de expertos fija en 12 las competencias docentes - eldiariodelaeducacion](https://eldiariodelaeducacion.com/2024/12/30/un-grupo-de-expertos-fija-en-12-las-competencias-docentes/)  
Decanos y decanas y técnicos de las autonomías y del ministerio han pasado meses trabajando en una propuesta de profesión que los sindicatos conocerán a mediados de enero  

[Una reflexión crítica sobre el Informe final con la Propuesta de Marco de Competencias Profesionales Docentes - eldiariodelaeducacion](https://eldiariodelaeducacion.com/porotrapoliticaeducativa/2024/12/30/una-reflexion-critica-sobre-el-informe-final-con-la-propuesta-de-marco-de-competencias-profesionales-docentes/)  
> La LOMLOE en su disposición adicional séptima obliga al Gobierno, una vez consultadas las comunidades autónomas y los representantes del profesorado, a presentar en el plazo de un año una propuesta normativa que regule la formación inicial y permanente, el acceso y el desarrollo profesional docente. A lo largo de su articulado, la ley establece la necesidad de que esta formación permita afrontar los nuevos retos de una sociedad compleja y promover la investigación educativa y la innovación pedagógica.  
Desde la aprobación de la ley, en diciembre de 2020, se han venido publicando en España diversos documentos y normativas orientados a mejorar la formación inicial del profesorado1. Si bien, el reciente [“Informe final con la Propuesta de Marco de Competencias Profesionales docentes”](https://drive.google.com/file/d/1BXGi-gl6MFjNqmU2Yjfxc27o_CrLinaK/view), elaborado por el Grupo de Trabajo de Formación del Profesorado del MEFPD, no parece haberlos incorporado o, al menos, no hace mención explícita.  
De la lectura de esta última propuesta se deduce, efectivamente, el propósito de actualizar y mejorar la formación del profesorado y de adaptarla a las necesidades cambiantes del sistema educativo y de la sociedad, como han hecho diversos países. Su publicación abre, al tiempo, un plazo de consulta, hasta el próximo 15 de enero, para [recoger la opinión de diversos actores involucrados](https://drive.google.com/file/d/19DvB_xzGAPCsJavAiV0hK77yk7lqKnbA/view) (profesores, sindicatos, asociaciones de estudiantes, universidades, expertos en educación, etc.). El objetivo es asegurar que el marco final sea lo más adecuado y funcional posible. ¿Lo conseguirá?  

La mencionada disposición adicional séptima de LOMLOE   
[Disposición adicional séptima. Normativa sobre el desarrollo de la profesión docente. - Ley Orgánica 3/2020, de 29 de diciembre, por la que se modifica la Ley Orgánica 2/2006, de 3 de mayo, de Educación.](https://www.boe.es/buscar/act.php?id=BOE-A-2020-17264#da-7)  
> A fin de que el sistema educativo pueda afrontar en mejores condiciones los nuevos retos demandados por la sociedad e impulsar el desarrollo de la profesión docente, el Gobierno, consultadas las comunidades autónomas y los representantes del profesorado, presentará, en el plazo de un año a partir de la entrada en vigor de esta Ley, una propuesta normativa que regule, entre otros aspectos, la formación inicial y permanente, el acceso y el desarrollo profesional docente  


**4 enero 2025**  
* Cuando escribo esto veo que se habla a 30 diciembre 2024 que se ha publicado pero no lo veo publicado oficialmente en ningún sitio.  
* Se habla de que "Su publicación abre, al tiempo, un plazo de consulta, hasta el próximo 15 de enero" pero no veo publicadado oficialmente ningún lugar donde enviar opiniones más allá del correo en el formulario direccion.intef@educacion.gob.es
* Documento tiene apartado con miembros de la ponencia y detalla de CCAA, pero no del ministerio 

Para Madrid hay 7 personas

Dña. María Gregoria Casares Andrés  
[jefa de la Unidad Técnica de Sistemas de Información de la Subdirección General de Programas de Innovación y Formación del Profesorado - 2023](https://www.avocesdecarabanchel.es/carabanchel/ies-calderon-barca-celebro-xv-semana-ciencia-tecnologia)   
[Departamento de Informática Personal Profesor Asociado  uc3m](https://www.uc3m.es/ss/Satellite/DeptInformatica/es/DetallePersonalDept/1371330172070/Detalle_Personal_Departamento/idu-1781)  

Dña. Ana Avelina González Hernández  
[ESPECIALIDAD: 205 INSTALACION Y MANTENIMIENTO DE EQUIPOS TERMICOS Y DE FLUIDOS](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2023-18009)  

D. David Mohedano Mohedano  
[Asesor técnico docente abr 2022 - actualidad 2 años 10 meses](https://es.linkedin.com/in/david-mohedano-a7b04777)  

Dña. Celia Moreno Rodríguez  
[Jefa de Servicio de Ordenación Académica de Educación Infantil y Primaria - 2024](https://www.comunidad.madrid/transparencia/agenda/reunion-aceim-5)  

D. Faustino Ramiro Fortea  
[Jefe de Servicio de Programas Educativos en Consejería de Educación, Ciencia y Universidades](https://es.linkedin.com/in/ramirofortea)  

Dña. Nélida Rodriguez Peña  
[ATD Subdirección General de Ordenación Académica Educación Infantil y Primaria. ](https://es.linkedin.com/in/nelidarodriguezpena)  

Dña. Magdalena Rubio Fabián  
[Responsable de Área de Registro, Innovación y Formación](https://www.comunidad.madrid/transparencia/unidad-organizativa-responsable/area-registro-innovacion-y-formacion)  


Realizo solicitud

---

Solicito copia o enlace a la siguiente información sobre el documento "INFORME FINAL CON LA PROPUESTA DE MARCO DE COMPETENCIAS PROFESIONALES DOCENTES, Ponencia dependiente del Grupo de Trabajo de Formación del Profesorado, 19 de noviembre de 2024" publicado en https://drive.google.com/file/d/1BXGi-gl6MFjNqmU2Yjfxc27o_CrLinaK/view que en el apartado MIEMBROS DE LA PONENCIA tiene nombres y apellidos de los miembros de las distintas CCAA, por lo que se entiende que no hay un problema de protección de datos  

- Miembros de la ponencia del Ministerio de Ciencia, Innovación y Universidades (no se indican)  
- Miembros de la ponencia del de Ministerio de Educación y Formación Profesional, a través del Instituto Nacional de Tecnologías del Aprendizaje y de Formación del Profesorado (INTEF) (no se indican)  
- Documentación sobre el proceso realizado para la elección de los miembros de la ponencia, que reflejen en caso de haber existido: convocatoria, baremo, retribución económica adicional por ser miembro, criterios de decisión sobre número de miembros por cada una de las CCAA y entidades que la forman, decisión sobre delegación de selección de miembros en cada una de las CCAA y entidades, ...  
- Lugar y fecha de publicación oficial del dicho documento 
- Mecanismo y plazo para envío oficial de propuestas de mejora a dicho documento 

---

Nº Expediente 00001-00099848

**14 enero 2024**

[NEGOCIACIÓN DEL NUEVO ESTATUTO DOCENTE ESTATAL - stes.es](https://www.stes.es/negociacion-del-nuevo-estatuto-docente-estatal/)  

> Sesión de 14 de enero, martes, de 2025, a las 10:30 horas  
MESA DE NEGOCIACIÓN DEL PERSONAL DOCENTE NO UNIVERSITARIO  
Ministerio de Educación, Formación Profesional y Deportes  
Calle Alcalá, 36. 28071 Madrid  
ORDEN DEL DÍA   
...  
3. Propuesta abierta de constitución de Grupos de Trabajo en relación con la profesión 
docente:  
3.1. Marco de Competencias Profesionales Docentes  

[Informe de la Mesa de Negociación con el Ministerio, del 14 de enero de 2025 - anpeandalucia](https://anpeandalucia.es/notices/191564/Informe-de-la-Mesa-de-Negociaci%C3%B3n-con-el-Ministerio,-del-14-de-enero-de-2025)  


