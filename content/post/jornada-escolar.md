+++
author = "Enrique García"
title = "Jornada escolar"
date = "2024-10-24"
tags = [
    "educación", "jornada"
]
toc = true

+++

Revisado 23 diciembre 2024

## Resumen

En octubre 2024 Madrid propone hacer obligatoria la jornada partida; el debate sobre jornada escolar continua o partida es recurrente, relacionado con una "conciliación" a menudo entendida como no realizar ningún cambio laboral pero hacer que los alumnos estén más tiempo en el centro.  

Se intentan aportar datos y argumentos sobre los tipos de jornada y la elección.  

Nota: en inglés se referencia la jornada continua y partida como continuous school day y split school day   
[Spain 5.1 Organisation of Primary Education - eurydice](https://eurydice.eacea.ec.europa.eu/national-education-systems/spain/organisation-primary-education)  

### Estudios y opiniones

De momento no separo estudios formales de artículos y opiniones; en artículos y opiniones se citan algunos  

**24 enero 2007**  
[Planning middle school schedules for improved attention and achievement Joseph Klein](https://www.tandfonline.com/doi/full/10.1080/0031383042000245825?src=recsys)  

**3 mayo 2013**  
[La jornada escolar continua se impone por motivos no académicos](https://www.aceprensa.com/educacion/la-jornada-escolar-continua-se-impone-por-motivos-no-academicos/)  
> Según informaba la Cadena SER el pasado 24 de abril, la jornada continua ya ha sido adoptada por un 62% de los colegios públicos españoles de infantil y primaria, lo que supone un aumento de 15 puntos respecto al 2009. Si se confirman estos datos, sería la primera vez que este tipo de jornada es mayoritaria en las primeras etapas educativas.

**Marzo 2015**  
[How the Time of Day Affects Productivity: Evidence from School Schedules Nolan G. Pope](https://www.researchgate.net/publication/276905561_How_the_Time_of_Day_Affects_Productivity_Evidence_from_School_Schedules)  

**2017**

[Guía sobre tiempos escolares. Daniel Gabaldón Estevan, Sandra Obiol Francés](https://ojs.uv.es/index.php/creativity/article/download/12062/11705)  
Creativity and Educational Innovation Review Nº 1 • 2017 • ISSN 2603-6061 • DOI 10.7203/CREATIVITY.1.12062

>4. Estudios existentes sobre tiempo escolar y rendimiento

> Gráfico 12: Distribución de las desviaciones estándar del rendimiento académico
según la hora en que comenzó la lección. Fuente: Klein (2004). 

> Gráfico 13: Variaciones diarias en el rendimiento de los alumnos de 10-11 años después de 3 eventos. Fuente: Testu (1994), INSERM, 2001, p. 54, citado en Suchaut, B. (2009, May).

[La ciencia del horario escolar - heraldo.es](https://www.heraldo.es/noticias/sociedad/2017/03/17/la-ciencia-del-horario-escolar-1164693-310.html)  

**21 mayo 2020**  
[¿Cómo organizamos la vuelta a la escuela? ](https://theconversation.com/como-organizamos-la-vuelta-a-la-escuela-139064)  
> Y, por cierto, la jornada escolar matinal intensiva, compactada, o mal llamada continua, que se ha impuesto por doquier a alumnos y familias aunque les perjudicase, la quisieran o no, para la enseñanza primaria y la secundaria obligatoria en gran parte de la escuela pública (no así en la privada y concertada, donde las familias pueden votar con sus pies), debería ser simplemente ignorada en este contexto, si es que no pulverizada para siempre.  

**2021**  
[Feito, R. (2021). El Debate sobre la Jornada Escolar en España. Reflexiones y Datos tras Más de Tres Décadas de un Debate
Inconcluso. International Journal of Sociology of Education, 10(3), 271-293.](https://dialnet.unirioja.es/servlet/articulo?codigo=8187196)  


**2022**  
[Jornada escolar continua: Cómo la pandemia está acelerando un modelo social y educativo regresivo - esade](http://itemsweb.esade.es/wi/Prensa/EsadeEcPol__Jornada%20Escolar.pdf)  
Marta Ferrero, Lucas Gortazar, Ángel Martínez Jorge  

**11 mayo 2022**  
[La jornada continua en los colegios: peor rendimiento escolar, mala para la economía y la sufren más las madres - eldiario](https://www.eldiario.es/sociedad/jornada-continua-colegios-peor-rendimiento-escolar-mala-economia-sufren-madres_1_8980658.html)  

> Dicen las encuestas, a falta de estadística oficial, que el 50,9% de los centros que decían tener jornada continua en el curso 2019-2020, previo a la pandemia, se convirtieron en un 72,3% al año siguiente para retroceder apenas imperceptiblemente hasta el 71,4% en este curso, aunque esta circunstancia podría ser transitoria.   

**mayo 2023**  
[La Jornada Escolar. Reflexiones y Datos - consejoescolar.educacion.navarra](https://consejoescolar.educacion.navarra.es/web1/wp-content/uploads/2023/05/CEN_Mono_Jornada_cast_WEB.pdf)  

**13 junio 2023**  
[La OCDE propone eliminar el horario intensivo y reducir la repetición para luchar contra el abandono escolar - eldiario](https://www.eldiario.es/sociedad/ocde-propone-quitar-horario-intensivo-reducir-repeticion-luchar-abandono-escolar_1_10291751.html)  

**29 agosto 2023**  
[Por qué la jornada intensiva sólo triunfa en los 'coles' públicos: el 67% frente al 4,3% de la privada - elespanol](https://www.elespanol.com/madrid/comunidad/20230829/jornada-intensiva-solo-triunfa-coles-publicos-frente-privada/790171279_0.html)  

**18 septiembre 2023**  
[Jornada escolar continua o partida: datos, estudios y evidencias - sciencemediacentre.es](https://sciencemediacentre.es/jornada-escolar-continua-o-partida-datos-estudios-y-evidencias)  

> En general, las investigaciones señalan que la jornada partida se asocia con mejores resultados académicos. [Un estudio ](https://www.academia.edu/37191278/A_xornada_escolar_de_sesi%C3%B3n_%C3%BAnica_en_Galicia_estudio_avaliativo_conclusi%C3%B3ns_xerais_e_criterios_de_actuaci%C3%B3n)realizado en los años 90 por la Consejería de Educación de Galicia mostró que había entre un 10 y un 20 % más de fracaso escolar en los centros con jornada continua. En Andalucía, [un trabajo de 2002](https://idus.us.es/handle/11441/77859) observó un mayor rendimiento en los alumnos que seguían un horario de jornada partida: en estos centros, el porcentaje de alumnos sin problemas en ninguna materia era un 8 % superior al de los alumnos en centros con jornada continua. Un estudio realizado por la Consejería de Educación de Madrid con alumnos de 6º de primaria en 2014 encontró que los alumnos con horario partido [tenían mejores resultados](https://dialnet.unirioja.es/servlet/articulo?codigo=8187196) (6,9 frente a 6,7 en las pruebas de conocimientos y destrezas indispensables CDI, resultados que ya se habían observado [en 2008](https://dialnet.unirioja.es/servlet/articulo?codigo=8187196) y[ 2010](https://fundaciobofill.cat/uploads/docs/z/p/r/0/m/j/w/w/g/565-doc.pdf)). En cuanto a los informes PISA, y como se recoge [en un informe de la Fundación Bofill](https://fundaciobofill.cat/uploads/docs/z/p/r/0/m/j/w/w/g/565-doc.pdf) (y como se ha apreciado en [algunos estudios exploratorios](https://ojs.uv.es/index.php/creativity/article/view/12062/0)), “la compactación horaria no parece haber dado una respuesta satisfactoria a las expectativas iniciales de mejora del rendimiento escolar en las comunidades autónomas en las que se ha implantado la jornada continua”. 

Uno de los problemas de estos estudios es que podrían estar sesgados por el nivel socioeconómico de las familias, ya que se ha observado que a mayor nivel, más tendencia a la escolarización en centros con jornada partida. [Un trabajo de 2019](https://www.sociedadyeducacion.org/blog/que-sabemos-sobre-el-efecto-del-tipo-de-jornada-escolar-en-el-rendimiento-academico/) realizado en Madrid intentó desentrañar esta relación a partir de las notas medias en matemáticas de alumnos de 3º de primaria. Los alumnos de centros con jornada partida tenían una nota media de 7,16, frente a 6,77 de aquellos en jornada continua. Si se analiza por nivel socioeconómico, la nota sigue siendo más alta para alumnos de jornada partida de niveles bajo, medio-bajo y medio. En cambio, es levemente más baja en los de niveles socioeconómicos medio-alto y alto.

**27 septiembre 2023**  
[Jornada escolar continua o partida: datos, estudios y evidencias - cece](https://actualidaddocente.cece.es/a-fondo/jornada-escolar-continua-o-partida-datos-estudios-y-evidencias/)  
> P. ¿Qué dicen los estudios sobre el efecto de cada tipo de jornada en los resultados académicos?   
R. En general, no existen demasiados trabajos y los métodos empleados no permiten sacar conclusiones sólidas. Marta Ferrero, profesora y vicedecana de investigación y transferencia en la facultad de Formación de Profesorado y Educación de la Universidad Autónoma de Madrid y que ha sido maestra y orientadora con anterioridad, afirma al SMC España: “Hay que ser prudentes, porque no hay mucha investigación a nivel nacional ni internacional y los estudios suelen ser de tipo correlacional, no causal. De hecho, se está perdiendo una oportunidad magnífica en estos momentos: tenemos un laboratorio natural con colegios muy similares que están optando por jornadas diferentes, pero no se está aprovechando para realizar estudios que permitan analizar sus resultados. Eso sí, los datos de los que disponemos no apuntan en ningún momento a que haya un beneficio con la jornada continua”. 

**13 noviembre 2023**  
[twitter lucas_gortazar/status/1723998834634686873](https://x.com/lucas_gortazar/status/1723998834634686873)  
La expansión de la jornada continua se ha paralizado en Navarra. Para mí es una buena noticia. Pero aquí van algunas reflexiones para compartir sobre todo con los docentes que la siguen defendiendo🧵  
Espero que estemos de acuerdo en que la escuela a tiempo parcial (25h) es mayoritaria entre niños de 3-12 años es un recorte de derechos para el alumnado (esp. vulnerable). Insisto, estamos hablando de permanencia (sí, la mitad van como mucho 5h a la escuela) no horas lectivas.  
![](https://pbs.twimg.com/media/F-zcnBnXMAAfLnP?format=jpg)  
Espero que estemos también de acuerdo en que la investigación ha probado: (i) en niños más pequeños (3-7 años), es necesario un parón al mediodía para responder mejor a sus picos de atención (media mañana y 1h30h después de comer); (ii) comer tarde (14h30) es peor para su salud;  
(iii) más tiempo no lectivo va a facilitar su socialización y mejorar sus habilidades socio-emocionales; (iv) más tiempo en la escuela favorece empleo/conciliación especialmente de las madres.  
Espero que estemos de acuerdo en que, a tiempo lectivo constante, no sabemos qué jornada (matinal o partida) es mejor para los resultados académicos, aunque los puntos anteriores van todos de la mano de la segunda. Pero no hay investigación seria al respecto.  
Espero que estemos de acuerdo, por tanto, en la necesidad  una oferta escolar más amplia que la actual, con los recursos de comedor, transporte y extraescolares que sean necesarios. Ni 9/10h en la escuela, pero tampoco 5h (que es donde la mayoría está ahora mismo).  
Asumo que hay desacuerdo en el cómo: creo que es mejor una jornada partida de 6h30/7h con más tiempo de permanencia docente (compensada) y asumo que los pro-continua y pro-derechos de alumnos prefieren extraescolares una vez terminada la jornada, prestadas por no docentes.  
Necesitamos tomar conciencia del tremendo daño que estamos haciéndonos apostando por una educación a tiempo parcial. A niños, familias y, en mi opinión, también a los propios docentes.  
Para más debate, no puedo sino seguir recomendando nuestro informe de 2022 sobre la jornada escolar, sus efectos y con la mayor encuesta a docentes que se ha hecho al respecto. Firmada con @amjorge15 y @ferrero_mar  

**13 septiembre 2024**  
[¿Jornada partida o continua en los colegios? Lo que dicen los datos y la evidencia científica sobre el debate - eldiario](https://www.eldiario.es/sociedad/datos-evidencia-cientifica-debate-jornada-partida-continua_1_11650627.html)  
Isabel Díaz Ayuso promete imponer la jornada partida en los colegios de Madrid, y aunque las investigaciones no han concluido que partir la jornada sea mejor (ni peor) para el rendimiento del alumnado, sí se sabe que permanecer más horas en el centro es positivo 

**27 septiembre 2024**  
["En el Informe PISA no había una correlación entre la jornada continua o jornada partida y éxito escolar"](https://www.tribunavalladolid.com/noticias/380432/en-informe-pisa-no-habia-una-correlacion-entre-la-jornada-continua-o-jornada-partida-y-exito-escolar)  

>  Desde la Federación consideran que "no es la misma situación" en las ciudades que en los medios rurales y por lo tanto "una flexibilidad sí que sería interesante".

> "En una escuela rural, qué van a hacer los niños, se van a ir en autocar al pueblo y van a volver, comer en el comedor... entonces no estamos acortando la jornada escolar".

> No es normal que un niño de tres años tenga el mismo horario que uno de 18, como pasa aquí", explica López.

> Incluso el Informe PISA sacó que no había una correlación entre la jornada continua o jornada partida y éxito escolar  

**25 octubre 2024**  
Tal y como funciona la consulta pública en Madrid, las aportaciones se pueden ver en modo blog
[Proyecto de Decreto de Consejo de Gobierno por el que se regula la jornada escolar en los centros docentes de Infantil y Primaria, así como en colegios públicos autorizados para impartir ESO y en centros que impartan Educación Especial.](https://participa.comunidad.madrid/content/proyecto-decreto-consejo-gobierno-se-regula-jornada-escolar-los-centros-docentes-infantil) 

> Primeramente, me gustaría dejar claro que el horario de salida de los niños del centro es el mismo tanto en jornada partida como en continua (16 horas). La diferencia es que en la partida se come de 12:30 a 14:30 horas y en la continua se come de 14 a 16 horas. Las extraescolares, en ambos casos, son a las 16 horas.   
Soy padre de 3 hijos y he vivido ambas jornadas. Por mi experiencia, puedo confirmar que la jornada partida es perjudicial para los alumnos por varias razones:  
1-Las clases que se imparten después de comer no son productivas. Los niños tienes ganas de todo, menos de dar clase. Mis hijos me dicen que es horrible dar clase tras el comedor (tiene 12, 7 y 4 respectivamente).  
2-Los profesores pasan 30' tras el comedor resolviendo conflictos surgidos durante el parón. Los monitores de los comedores no están capacitados para resolver los continuos problemas que surgen y los tiene que resolver el tutor...  
3-Los niños de Infantil (3 años) se tienen que echar siesta, por lo que las sesiones de tarde se las pasan en gran medida, durmiendo (lo que conlleva que nuestros hijos se duerman a las mil...)  

> Al plantear la jornada se dice continuamente que la jornada continua (que en muchas ocasiones confunden con la intensiva) perjudica a las familias más desfavorecidas porque se ven obligadas a pagar extraescolares porque sus hijos salen antes. Esto es totalmente falso. Con las 2 jornadas el tiempo de permanencia en el centro es exactamente el mismo, solo cambia en que franja horaria se ponen las 2 horas de la comida. En la jornada continua se come de 14 a 16h y en la continua de 12,30 a 14,30. Teniendo en cuenta estos horarios, en el caso de la jornada partida, las familias que no hacen uso del comedor tendrían que recoger a sus hijos y volverlos a llevar 2 horas después, para volverlos a recoger 1h y media después. No siempre se vive al lado del colegio, menos aún con las zonas únicas, con lo que son un montón de trayectos con los peligros y la contaminación que ellos conlleva. Por otro lado, es más probable que en los trabajos de las familias se salga para comer a las 14h que a las 12,30h.  
En conclusión, lejos de ser la jornada continua la que perjudica a familias con dificultades económicas, la jornada partida obliga a muchas familias a pagar por un servicio de comedor que no es nada barato.

**23 diciembre 2024**  
[Emilio Viciana, consejero de Educación: "Hay acuerdo entre CCAA del PP para contenidos comunes en selectividad - elespanol](https://www.elespanol.com/madrid/comunidad/20241223/emilio-viciana-consejero-educacion-acuerdo-ccaa-pp-contenidos-comunes-selectividad/909659157_0.html)  
> P: Respecto a la jornada partida en los colegios y el modelo EGB, ¿tienen cifras sobre cuántos centros podrían adoptar estas medidas?  
R: Estas iniciativas, aunque complementarias, requieren diferentes abordajes. ...  
Respecto a la jornada partida, trabajamos en una normativa que favorezca su implementación aunque no será obligatoria. Responde a una demanda creciente de las familias, respaldada por estudios nacionales e internacionales. Sabemos que el trámite para revertir la jornada escolar es complejo y somos partidarios de agilizarlo y simplificarlo.

### Jornada y resultados

El resultado educativo no solo son resultados académicos, aunque se suele usar PISA y similares como valor para realizar asociaciones, a veces combinando erróneamente correlación y causalidad, ver artículos.  

Es complicado disociar la variable de tipo de jornada de la titularidad del centro que va asociada a segregación, y asociar causalidad: decir que las CCAA con mayor implantación de jornada continua obtienen resultados en PISA peores en la enseñanza pública respecto a los que se obtienen en esas mismas CCAA en la enseñanza privada concertada y que al alumnado se le hace repetir en mayor medida no es hablar solo de jornada continua, y es asumir causalidad jornada > resultados.  

### Jornada y conciliación

[Conciliar](https://dle.rae.es/conciliar) es _2. tr. Hacer compatibles dos o más cosas. Conciliar la vida laboral y la vida familiar._  

Hacer compatible la vida laboral y la vida familiar implica poder  
- tener vida laboral: estar tiempo trabajando, al tiempo que hay alguien ocupándose de los hijos mientras se trabaja  
- tener vida familiar: estar tiempo con la familia, al tiempo que el trabajo nos ha permitido los recursos suficientes  

Las condiciones laborales a veces implican horarios muy amplios, que suponen buscar quién se ocupa de los hijos: si no es posible con apoyo en la familia, se busca ampliar horario (desayuno en el centro, comida en el centro, extraescolares ...)

A veces se utiliza el término "tiempo de calidad con la familia" para enmascarar que es poco tiempo.  
 
Si se visualiza conciliar como tener alternativas para poder trabajar más, no se trata de conciliación sino de soporte a la vida laboral de la familia. Enlaza con propuestas de abrir centros educativos durante julio para que los padres puedan seguir dejando a sus hijos como hacen con campamentos, haciendo solo función de custodia, no educativa.  
Si se visualiza conciliar como tener alternativas para que otros se ocupen de los hijos, no se trata de conciliación sino de soporte no tener vida familiar.  

Como se visualiza el centro educativo como un recurso de conciliación esencial, se suele confundir tiempo de centro educativo como tiempo de educación, y no es lo mismo. El tiempo total lectivo es el mismo, y si los alumnos están más tiempo en el centro lo realizan otras personas: cuidadores en comedor, monitores de extraescolares ...  

Hablar de conciliación suele disociar "vida familiar estando con los hijos" o "trabajo estando los hijos en la escuela con docentes", olvidando que hay un trabajo que es el de docente, por lo que el tipo de jornada combinada de hijo y sus padres docentes influye en la conciliación.

### Tiempo lectivo y tiempo de custodia o cuidado

Es un hecho que "la escuela tiene también una función de custodia o cuidado fundamental" [Jornada escolar continua: Cómo la pandemia está acelerando un modelo social y educativo regresivo - esade](http://itemsweb.esade.es/wi/Prensa/EsadeEcPol__Jornada%20Escolar.pdf), pero creo es esencial "también", ya que su función esencial es la educación.  

La función de custodia y cuidado de los centros educativos asociada a conciliación es algo distinto de la educación. 

Para alumnos vulnerables la permanencia en el centro educativo más tiempo es beneficioso: alimentación en el comedor, apoyos por las tardes con programas como PROA .... pero es esencial ver que es más tiempo de permanencia, no más tiempo lectivo. 

Se comparan tipos de jornada escolar entre países sin comparar al tiempo el número de horas lectivas totales  

[Nuevo informe Eurydice: Número de horas lectivas anuales recomendadas en la educación obligatoria a tiempo completo en Europa 2022/2023](https://inee.educacion.es/2023/06/20/nuevo-informe-eurydice-numero-de-horas-lectivas-anuales-recomendadas-en-la-educacion-obligatoria-a-tiempo-completo-en-europa-20222023/)  
> Tiempo mínimo de instrucción recomendado para la primera etapa de educación secundaria  

[The organisation of school time in Europe. Primary and general secondary education : 2023/2024](https://op.europa.eu/en/publication-detail/-/publication/caea97f5-6194-11ee-9220-01aa75ed71a1/language-en)  

Sobre las horas lecivas, no cambian con el tipo de jornada, cambia la hora a la que comen

### Normativa

[Real Decreto 732/1995, de 5 de mayo, por el que se establecen los derechos y deberes de los alumnos y las normas de convivencia en los centros.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-13291#ar-11)  
> 3. El pleno desarrollo de la personalidad del alumno exige una jornada de trabajo escolar acomodada a su edad y una planificación equilibrada de sus actividades de estudio.

### Normativa Madrid

[Normativa actualizada de aplicación en Jornada y Comedor Escolar - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/regulacion-jornada-comedor-escolar)  

[ORDEN 11994/2012, de 21 de diciembre, por la que se regula la jornada escolar en los centros docentes que imparten segundo ciclo de Educación Infantil y Educación Primaria en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/01/15/BOCM-20130115-11.PDF)  
[ORDEN 502/2013, de 25 de febrero, por la que se regula el procedimiento a seguir para solicitar el cambio de jornada escolar en los centros públicos que imparten segundo ciclo de Educación Infantil y Educación Primaria en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/03/05/BOCM-20130305-2.PDF)  
[ORDEN 121/2024, de 27 de enero, de la Consejería de Educación, Ciencia y Universidades, por la que se modifica la Orden 502/2013, de 25 de febrero, de la Consejería de Educación, Juventud y Deporte, por la que se regula el procedimiento a seguir para solicitar el cambio de jornada escolar en los centros públicos que imparten segundo ciclo de Educación Infantil y Educación Primaria en la Comunidad de Madrid con la finalidad de facilitar el desarrollo y la participación en la consulta a las familias.](https://www.comunidad.madrid/sites/default/files/bocm-20240207-21.pdf)  

### Procedimiento cambio de jornada

El cambio de jornada se decide primero en Consejo Escolar, y luego vota claustro de profesores y padres y madres   

[RESUMEN DEL PROCEDIMIENTO A SEGUIR POR LOS COLEGIOS PÚBLICOS DE EDUCACIÓN INFANTIL Y PRIMARIA QUE DESEEN TRAMITAR EL CAMBIO DE JORNADA ESCOLAR](https://www.comunidad.madrid/sites/default/files/doc/educacion/p1206_resumen_del_procedimiento_para_tramitar_el_cambio_de_jornada_escolar.pdf)  

Si el tipo de jornada se decide y se puede cambiar en un proceso en el que participan familias y docentes, la idea de imponer por normativa resulta extraño.  

### Cambio de normativa Madrid 2024

**12 septiembre 2024**

[Díaz Ayuso anuncia que los nuevos colegios públicos también impartirán 1º y 2º de ESO y tendrán jornada partida](https://www.comunidad.madrid/noticias/2024/09/12/diaz-ayuso-anuncia-nuevos-colegios-publicos-tambien-impartiran-1o-2o-eso-tendran-jornada-partida)  
> Además, la Consejería de Educación, Ciencia y Universidades va a dictar las instrucciones necesarias para que los colegios públicos que actualmente tienen jornada partida **no puedan cambiar** a horario intensivo desde el próximo curso 2025/26.

[Ayuso avanza que los nuevos colegios públicos de Madrid tendrán la jornada partida obligatoria](https://www.eldiario.es/madrid/politica/ayuso-avanza-nuevos-colegios-publicos-tendran-jornada-partida-obligatoria_1_11648278.html)  

[Madrid prohibirá la jornada continua en los nuevos colegios públicos de Infantil y Primaria, que tendrán primero y segundo de la ESO](https://elpais.com/espana/madrid/2024-09-12/madrid-prohibira-la-jornada-continua-en-los-nuevos-colegios-publicos-de-infantil-y-primaria-que-tendran-primero-y-segundo-de-la-eso.html)  

**23 octubre 2024**

[La Comunidad de Madrid inicia los trámites para aprobar el Decreto que establece la jornada partida en los colegios ](https://www.comunidad.madrid/notas-prensa/2024/10/23/comunidad-madrid-inicia-tramites-aprobar-decreto-establece-jornada-partida-colegios)  


**24 octubre 2024**

[Proyecto de Decreto por el que se regula la jornada escolar en los centros docentes de Infantil y Primaria, así como en colegios públicos autorizados para impartir Educación Secundaria Obligatoria y en centros que impartan Educación Especial.](https://www.comunidad.madrid/transparencia/proyecto-decreto-que-se-regula-jornada-escolar-centros-docentes-infantil-y-primaria-asi-como)  

De 25/10/2024 hasta 15/11/2024

> La jornada escolar en los centros docentes que imparten el segundo ciclo de Educación Infantil y Educación Primaria en la Comunidad de Madrid está regulada por la Orden 11994/2012, de 21 de diciembre, de la Consejería de Educación, Juventud y Deporte, por la que se regula la jornada escolar en los centros docentes que imparten segundo ciclo de Educación Infantil y Educación Primaria en la Comunidad de Madrid y la Orden 502/2013, de 25 de febrero, de la Consejería de Educación, Juventud y Deporte, por la que se regula el procedimiento a seguir para solicitar el cambio de jornada escolar en los centros públicos que imparten segundo ciclo de Educación Infantil y Educación Primaria en la Comunidad de Madrid. El tiempo transcurrido desde la entrada en vigor de dichas disposiciones, **el predominio de la jornada escolar continuada en la mayor parte de los colegios públicos de la región, que reduce las alternativas de las familias madrileñas para el ejercicio del principio de elección de centro educativo**, y el incremento de colegios públicos autorizados para impartir la Educación Secundaria Obligatoria, ha mostrado la conveniencia de actualizar la regulación normativa de la jornada escolar **con el objetivo de incrementar la capacidad de elección de las familias madrileñas y facilitar la conciliación de la vida laboral y familiar**. 

En [memoria](https://www.comunidad.madrid/transparencia/sites/default/files/memoria_12.pdf) se indica  

> Desde la entrada en vigor de dichas disposiciones, se ha observado una **creciente demanda de las familias** en las que se pone de manifiesto, la necesidad de aumentar la conciliación con la vida laboral y familiar.  
Asimismo, **diversos estudios científicos** han puesto de manifiesto que desarrollar la
jornada escolar de un modo pausado, alternando los períodos lectivos con los tiempos
de ocio, alimentación y descanso tiene un efecto positivo sobre el rendimiento
académico y el bienestar de los alumnos. Igualmente, algunos estudios han
demostrado la necesidad de tener en consideración el respeto por el adecuado
desarrollo evolutivo y equilibrio social y emocional de los menores, así como sus
biorritmos a la hora de abordar las actividades de enseñanza y aprendizaje en los
centros escolares.  
Para ello, entre otras medidas, la Comunidad de Madrid quiere fomentar la jornada
partida en los colegios públicos, así como incrementar el número de nuevos colegios
públicos de Educación Infantil y Primaria autorizados para impartir Educación
Secundaria Obligatoria. En dichos centros, los alumnos que cursen 1º y 2º de
Educación Secundaria Obligatoria tendrán jornada escolar partida.  
Todo lo anterior, así como la consideración del interés superior del menor, tal como se
recoge en la Ley 4/2023, de 22 de marzo, de Derechos, Garantías y Protección
Integral de la Infancia y la Adolescencia de la Comunidad de Madrid, impulsa a la
Consejería competente en materia educativa a revisar la actual normativa y ajustar la
regulación de la jornada escolar en los centros educativos de educación infantil y
primaria así como en aquellos colegios públicos que también están autorizados para
impartir Educación Secundaria Obligatoria y en los centros específicos de Educación
Especial.  

**19 noviembre 2024**

[La Comunidad de Madrid apuesta por la jornada partida en los colegios para mejorar el rendimiento académico de los alumnos](https://www.comunidad.madrid/notas-prensa/2024/11/19/comunidad-madrid-apuesta-jornada-partida-colegios-mejorar-rendimiento-academico-alumnos)  

> La Comunidad de Madrid apuesta por recuperar la jornada partida en los centros públicos, iniciativa que se refleja en el retraso de dos años en la transición de los alumnos de su colegio al instituto. Con esta medida, el Ejecutivo autonómico persigue mejorar el rendimiento académico de los estudiantes, su descanso y hábitos de alimentación, así como luchar contra la soledad, adicciones, la posible influencia de las bandas juveniles o el abandono escolar. Por ello, todos los colegios nuevos tendrán horario partida de manera obligatoria a partir del curso 2025/26 e incluirán 1º y 2º de la ESO, un modelo que también se pondrá en marcha en los que ya funcionan y así lo soliciten.  
Así lo ha trasladado hoy el consejero de Educación, Ciencia y Universidades, Emilio Viciana, en un encuentro sobre esta materia organizado por el Consejo Escolar de la Comunidad de Madrid. Durante esta sesión de trabajo, a la que han asistido diferentes representantes de la comunidad educativa y expertos del sector, se han abordado las posibles mejoras en el horario y el modelo de los que incorporen cursos de Secundaria, desde la perspectiva e impacto de estos cambios en docentes, familias y, sobre todo, alumnos.

**20 noviembre 2024**

[Consejo Escolar de Madrid: sesión sobre la jornada escolar Y los CEIPSO - feuso](https://feuso.es/comunidad-de-madrid/noticias/19111-consejo-escolar-de-madrid-sesion-sobre-la-jornada-escolar-y-los-ceipso)  
> Finalmente, se expusieron los pros y contras sobre la incorporación de los dos primeros cursos de la ESO a los colegios públicos. En concreto, la Comunidad de Madrid tiene la intención de reinstaurar la jornada partida en todos los colegios públicos y mantener a los alumnos de los primeros cursos de secundaria en los colegios. Se argumenta que esto busca el bienestar del menor, considerándolo menos vulnerable a estas edades en los colegios donde han cursado la primaria.


### Ideas alegaciones

En consulta pública pueden ser muy genéricas, no se puede concretar hasta el trámite de audiencia

- La jornada partida se elige y se debe poder seguir eligiendo, luego no procede hablar de "fomentar" y al mismo tiempo decir que "tendrán jornada escolar partida" lo que sugiere obligar a un tipo de jornada concreta sin posibilidad de cambio.  
- Poner jornada partida implica que más alumnos tendrán que utilizar el comedor escolar, lo que sí tiene un coste
--  presupuestario: debería haber mayor número de becas de comedor, más infraestructuras de comedor, más personal de comedor, más salario para docentes al haber mayor jornada presencial   
--  coste para las familias que quizá no puedan asumir: 
---  coste de transporte para llevarlos a casa y volverlos a traer, si es que es viable y permite conciliar con la distancia a trabajo, domicilio y centro educativo dentro de la zona única
---   coste del servicio de comedor  
- Memoria habla de "diversos estudios científicos" que no se aportan.  
- Además de que por la tarde pueda aumentar la atención de los alumnos, se debe valorar si aumenta la disposición a recibir clase en base a la experiencia de los docentes.   






