+++
author = "Enrique García"
title = "Profesorado religiosamente pagado con dinero público"
date = "2020-02-02"
tags = [
    "educación", "religión", "Post migrado desde blogger"
]

description = "Subida impuestos rentas altas"
thumbnail = "https://pbs.twimg.com/media/GYQTYSrXMAEaJrM?format=jpg"
images = ["https://pbs.twimg.com/media/GYQTYSrXMAEaJrM?format=jpg"]

toc = true

+++

Revisado 24 septiembre 2024

## Resumen

En Madrid hay firmado y está vigente un acuerdo por el que TODOS los profesores religiosos, solo por el hecho de serlos y solo si trabajan en centros privados con concierto (es decir, solo si se paga con dinero público):  
* Cobran TODOS 8 trienios (24 años antigüedad), sea cual sea su antigüedad.  
* Les pude permitir cobrar una paga extraordinaria anual con "25 años antigüedad" (con menos antigüedad real). La empresa privada fija una paga adicional que no paga la empresa privada.  
* El acuerdo se firmó en 2013, en plenos recortes, y desde entonces ha supuesto unos 10 M€ de dinero público que ha ido solamente a trabajadores de centros privados por su condición de religioso, unos 3500€ anuales adicionales de complemento por cada profesor religioso en privado con concierto.

Este tema surge en prensa en febrero 2020, por Ángel Munárriz
[Madrid paga a la concertada católica 1,73 millones al año por un plus de ocho trienios por cada cura que da clases - infolibre](https://www.infolibre.es/noticias/politica/2020/01/30/madrid_paga_concertada_millones_ano_por_plus_ocho_trienios_por_cada_cura_monja_que_clases_102982_1012.html)  

En 2020 consigo datos actualizados y se mantiene aproximadamente 1 M€ anual.  

Posts relacionados

* [Profesorado tratado como Dios manda](https://algoquedaquedecir.blogspot.com/2020/01/profesorado-tratado-como-dios-manda.html)
* [Madrid crea centros privados con concierto en diferido](http://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html) (se cita la cesión de suelo público por Lucía Figar)

## Detalle

### El acuerdo de profesores religiosos de Madrid

Es un tema que, tras descubrirlo en 2018, lo comento en Twitter

Inicialmente lo interpreto mal, asociándolo profesorado de religión, pero es para el religioso (lo defino más adelante)

**19 octubre 2018**

[twitter FiQuiPedia/status/1053323882751557637](https://twitter.com/FiQuiPedia/status/1053323882751557637)  
¿40%? Figar firmó 2013 acuerdo, vigente, con [@ecatolicmadrid](https://twitter.com/ecatolicmadrid)  
II.2.c "Complemento...Se computará, a TODOS los profesores religiosos, el equivalente a 8 trienios de antigüedad"  
[2013-12-27-AcuerdoConsejería Educación-FERE CECA MADRID profesorado religioso-SinFirmasTrasNotificacion23ene2020.pdf](https://drive.google.com/file/d/1V6FAL4G6kos6IQyyBDe5Q0jeTUgSBaIQ/view)  
Con 25 años reciben una paga extra  
[Resolución de 30 de julio de 2013, de la Dirección General de Empleo, por la que se registra y publica el VI Convenio colectivo de empresas de enseñanza privada sostenidas total o parcialmente con fondos públicos.](https://www.boe.es/buscar/doc.php?id=BOE-A-2013-9028) art 62  
[@R_vanGrieken](https://twitter.com/R_vanGrieken)

[twitter FiQuiPedia/status/1053547882203279360](https://x.com/FiQuiPedia/status/1053547882203279360)  
Rectifico gracias a [@Gatoloco_loco](https://twitter.com/Gatoloco_loco): donde dije "de religión" es "religioso". Resto lo veo tal cual; prebendas asociadas a religión (este caso a ser miembros de órdenes religiosas) y no por su trabajo, pagadas con dinero público (este caso aplica a privados con concierto)  

Ahí cito un acuerdo de 27 diciembre 2013, acuerdo, limitado a docentes religiosos de privados con concierto: eso quiere decir que solo aplica si el dinero con el que se paga es público.

(copia local, documento escaneado con sello y firma)

[2013-12-27-AcuerdoConsejería Educación-FERE CECA MADRID profesorado religioso-SinFirmasTrasNotificacion23ene2020.pdf](https://drive.google.com/file/d/1V6FAL4G6kos6IQyyBDe5Q0jeTUgSBaIQ/view)  

(inicialmente localizado en web de escuelas católicas, luego en registros de convenios de Madrid con mejor resolución)

y al tiempo cito artículo 62 del convenio de ese mismo año que supone una paga adicional (Lo comento en apartado Paga adicional a profesorado de privados con concierto)

Es decir, no solo a cualquier empleado religioso se le reconocen 8 trienios (24 años) sino que a priori con 1 año más se suman 25 y se tiene una paga extraordinaria. (ver más adelante, es con 2 años, no con uno)

**6 febrero 2014**

Resumen del acuerdo realizado por Escuelas Católicas

[NUEVO ACUERDO ENTRE LA COMUNIDAD DE MADRID Y FERE-CECA MADRID SOBRE PROFESORADO RELIGIOSO EN CENTROS CONCERTADOS - ecmadrid.org](https://www.ecmadrid.org/es/pdfs-revistas/doc_download/1604-06-02-2014-nuevo-acuerdo-con-la-consejeria-educacion-sobre-profesorado-religioso)  

([Copia local, anonimizada sin firma manuscrita](https://drive.google.com/open?id=1Ne61oBqesW6By-8M7-ARFHOxEjj14uyH))

**14 enero 2015**

[Resolución de la Dirección General de Becas y Ayudas a la Educación, por la que se dictan instrucciones sobre el abono por la Administración educativa de la paga extraordinaria de antigüedad en la empresa, regulada en el VI Convenio colectivo de empresas de enseñanza privada sostenidas total o parcialmente con fondo públicos, para el personal religioso que presta servicios docentes en los centros concertados sin relación laboral contractual con la empresa. Ref: 09/025498.9/15, CSV 1257928631029125339755 - ecmadrid.org](https://www.ecmadrid.org/es/pdfs-revistas/doc_download/2796-instrucciones-14-enero-2015-pea-religiosos-vi-convenio-ensenanza-concertada)  

([copia local](https://drive.google.com/open?id=16QskgF3pNacWx5jRkpzMfSD_-s4TnE69))

Solamente con 2 años se reconocen 25 años y una paga extraordinaria; supone una paga extraordinaria con tener dos años de antigüedad real (en religiosos se limita a caso de jubilación)

>PRIMERA: Profesorado beneficiario de esta paga extraordinaria.  
a) Para generar el derecho a este abono, será requisito necesario que, durante la vigencia del VI Convenio colectivo, de empresas de enseñanza privada sostenidas total o parcialmente con fondos públicos, el profesor religioso haya cesado en el ejercicio de la docencia habiendo cumplido 65 años de edad o más o, que habiendo cumplido 55 años, el cese tenga su causa en la Invalidez Permanente Total, Absoluta o Gran Invalidez.  
b) Asimismo, será preciso que el profesor haya impartido al menos dos años de docencia en los niveles concertados del centro; en este caso se le reconocerá una antigüedad de 25 años para calcular el número de quinquenios a incluir en el monto equivalente derivado de la paga de antigüedad contemplada en el artículo 62 bis del VI Convenio colectivo.  

Una idea relacionada, [artículo 117 LOE modificada por LOMCE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a117), a lo que se le puede ver relación

> c) Las cantidades pertinentes para atender el pago de los conceptos de antigüedad del personal docente de los centros privados concertados y consiguiente repercusión en las cuotas de la Seguridad Social; pago de las sustituciones del profesorado y los derivados del ejercicio de la función directiva docente; pago de las obligaciones derivadas del ejercicio de las garantías reconocidas a los representantes legales de los trabajadores según lo establecido en el artículo 68 del Estatuto de los Trabajadores. Tales cantidades se recogerán en un fondo general que se distribuirá de forma individualizada entre el personal docente de los centros privados concertados, de acuerdo con las circunstancias que concurran en cada profesor y aplicando criterios análogos a los fijados para el profesorado de los centros públicos.

Más tarde encuentro el buscador de convenios (al migrar el post en 2024 pongo la nueva url en dominio .madrid en lugar de la anterior en .org)   

[Registro de convenios y protocolos. Buscador de convenios - comunidad.madrid](https://gestiona.comunidad.madrid/rcnv_app/secure/buscador/buscador.jsf)  

y ahí aparece una adenda realizada en 2016

ADENDA AL ACUERDO DE 27-12-2013 SOBRE PROFESORADO RELIGIOSO EN CENTROS CONCERTADOS (RETRIBUCIONES Y CONDICIONES LABORALES) 01/04/2016  
([copia local, anonimizada sin firmas manuscritas](https://drive.google.com/open?id=1dYAF9X22l7IsP-NDuArW9QX79Tw23yVp))

También se puede ver el convenio previo, y las 7 adendas que tuvo

ACUERDO SOBRE PROFESORADO RELIGIOSO EN CENTROS CONCERTADOS (RETRIBUCIONES Y CONDICIONES LABORALES) 21/02/2001

Algunos convenios falla la descarga.

La séptima adenda habla de financiación de jornadas destinadas a la formación.
([Copia local, anonimizada sin firmas manuscritas](https://drive.google.com/open?id=17Z1kazHe5_YroswozZ2SxRDrA73Gy93K))

### Qué es un profesor religioso

[twitter FiQuiPedia/status/1053560875452428288](https://twitter.com/FiQuiPedia/status/1053560875452428288)  
Mi duda es la condición de religioso, "ser cura o monja", por ejemplo por cursar seminario, votos orden. No es lo mismo religioso que profesor de religión.  

**23 febrero 2007**  

[Los profesores de religión denuncian que el Constitucional les confunde "con curas o monjas" La docente que recurrió su despido se muestra "indignada" con la sentencia del Constitucional, que avaló ayer la postura del episcopado - elpais](https://elpais.com/sociedad/2007/02/23/actualidad/1172185204_850215.html)  
> Para Guridi, el Constitucional "se ha equivocado totalmente" porque confunde a los profesores de religión "con sacerdotes o monjas", cuando estos docentes no tienen voto de obediencia a los obispos y además son "mayoritariamente laicos o seglares". Según Guridi, la misión de los profesores de religión no es dar catequesis o adoctrinar a los alumnos sino "decir lo que la Iglesia piensa".

### Quién es FERE-CECA, firmante del acuerdo

FERE-CECA es parte de [@ecatolicas](https://twitter.com/ecatolicas)

[escuelascatolicas/historia](https://www.escuelascatolicas.es/historia/)  

>1989  
El 24 junio se constituye la Confederación de Centros Educación y Gestión para representar a los centros educativos en su vertiente empresarial. Hoy es la patronal mayoritaria en la enseñanza privada concertada.  
Por otra parte, FERE se incorpora al Consejo General de la Educación Católica y a la Coordinadora de ONGD de España (CONGDE).  
2005  
El 30 de mayo se realiza la unión funcional de FERE-CECA y la patronal EyG bajo la marca “Escuelas Católicas” (EC). Desde entonces EC está llamada a unir y a servir a todos los titulares de centros católicos con una vocación abierta e integradora.  

### Quién es Lucía Figar, firmante del acuerdo

Es una cielina: se llaman cielinos a los seguidores de Comunión y Liberación

**14 octubre 2010**

[Lucía Figar explica a “Comunión y Liberación” sus planes privatizadores en la Comunidad de Madrid - leeyopina](https://leeyopina.wordpress.com/2010/10/14/lucia-figar-explica-a-comunion-y-liberacion-sus-planes-privatizadores-en-la-comunidad-de-madrid/)  

>**Todos  lo sabíamos, pero escucharlo de sus propios labios, aunque sea en italiano o inglés, da verdadero miedo.**  
Durante el mes de agosto de 2010 nuestra Consejera de Educación, Lucía Figar, ha participado en el “Meeting de Rimini”, un encuentro anual que organiza el grupo ultra católico Comunión y Liberación.  
Lucía Figar: “Yo quisiera solo subrayar algunos aspectos importantes. Quisiera subrayar el factor común de todas las iniciativas, las reformas que hemos llevado a cabo, es decir, intentar ampliar los espacios de libertad o de aumentar las libertades educativas en nuestra región.  
En primer lugar, hemos elegido mejorar el panorama de la educación en centros de gestión privada con fondos públicos, ampliar la oferta de plazas en las estructuras de este tipo dentro de la Comunidad de Madrid. Estos centros están sostenidos con el dinero público, con fondos de los gobiernos regionales, pero tienen gestión privada y por tanto tienen titularidad privada y son gestionados de modo privado. Nosotros hemos aumentado las plazas escolares ofreciendo estas plazas a centros que funcionaban como centros privados al 100%, en los que los padres tenían que afrontar el 100% de los gastos; hemos acogido también a nuevos centros que tenían algunas dificultades dentro del programa de centros concertados y desde hace 7 años hemos ofrecido ámbitos públicos, regionales o de ayuntamiento, lugares públicos para que cooperativas, centros religiosos,… puedan llevar adelante sus propios proyectos educativos con la ayuda y el soporte de los fondos públicos.  
El problema también del lugar, del terreno, es un problema grave en España. Era un gran problema y nosotros hemos ofrecido estos lugares gratuitamente a organizaciones religiosas, cooperativas y otras estructuras para que crearan centros sostenidos con fondos públicos pero con gestión privada. Esto indica que en los últimos 6 años hemos abierto 79 centros nuevos de este tipo en la Comunidad de Madrid y hoy en día la proporción entre escuela pública, concertada y privada es esta: la escuela pública representa el 52% en nuestra región, la Comunidad de Madrid, la escuela concertada el 33% y la escuela al 100% completamente privada cerca del 15%.  
Dentro de estos centros, entre comillas, “concertados”, más de 2/3 ofrecen educación católica. Comunión y Liberación gestiona dos centros; C.L. gestiona 2 centros, por lo tanto yo pienso que estos dos centros pueden ser más en futuro obviamente, no sólo quedarse en dos “(aplausos).  
(Lucía Figar)”La escuela católica ha sido ejemplar en el responder a muchas demandas de los padres y, además, la escuela católica ha sido ejemplar a la hora de afrontar la disminución del número de sacerdotes en las escuelas. Es ya realidad, no solo una promesa, poder observar la eficacia con la que profesores laicos están asumiendo proyectos educativos de la escuela católica. La Iglesia en España ha hecho grandes esfuerzos con el fin e formar docentes laicos que asumen su encargo identificándose plenamente con el carisma y el ideal de las instituciones religiosas respectivas. Son responsables del proyecto educativo. Se ha visto un esfuerzo material, espiritual y personal de muchas religiosas y religiosos de la Iglesia católica, de las escuelas católicas en general; esto nos ha permitido en esta fase de expansión de centros concertados en España que muchos de los proyectos nuevos estén en manos de religiosos laicos y que muchos más niños puedan recibir la educación católica en nuestra región.”

**2 septiembre 2010**  

Experiences that educate - meetingdirimini  
[![](https://img.youtube.com/vi/1WfyCfTTcRI/0.jpg)]](https://www.youtube.com/watch?v=1WfyCfTTcRI "Experiences that educate - meetingdirimini")   

0:58:20 

[Traducción de la Intervención de Lucía Figar en el “MEETING DE RIMINI” - leeyopina](https://leeyopina.files.wordpress.com/2010/10/traduccion-de-la-intervencion-de-lucia-figar-en-rimini.pdf)  

**5 octubre 2015**

[Lucía Figar y los cielinos - cuartopoder](https://www.cuartopoder.es/espana/2011/10/05/lucia-figar-y-los-cielinos/)  

**28 marzo 2015**

[El costoso legado de Lucía Figar - eldiario](https://www.eldiario.es/zonacritica/costoso-legado-Lucia-Figar_6_371372863.html)  
"¿Qué nos deja Figar tras sus ocho años al frente de la Consejería de Educación de la Comunidad de Madrid? A grandes rasgos, nos deja un sistema educativo más privatizado y más segregado", afirman los autores

**2 junio 2015**

[El juez imputa a Salvador Victoria, Lucía Figar y a otros 3 cargos del PP por la 'Operación Púnica' - elmundo](https://www.elmundo.es/madrid/2015/06/02/556d5eb9e2704e3c338b456f.html)  

[21 cosas que no sabías de Lucía Figar - huffingtonpost](https://www.huffingtonpost.es/2015/06/02/cosas-lucia-figar_n_7492270.html)  

### Acuerdos profesores religiosos en España

Los acuerdos asociados a trienios se originan en 1989, se puede ver en (1), donde se indica

>b) Antigüedad. Se computará a todos los profesores religiosos el equivalente a tres trienios actualizándose conforme al siguiente calendario:  
1 de Enero de 1.990 4 trienios.  
1 de Enero de 1.993 5 trienios.  

### Búsqueda de transparencia

Tras conocer el tema de 8 trienios en Madrid, planteo solicitud de transparencia

**20 octubre 2018**

---

*El 27 diciembre 2013 se firmó "Acuerdo entre la Comunidad de Madrid (Consejería de Educación, Juventud y Deporte) y la Federación Española de Religiosos de Enseñanza-Titulares de Centros Católicos de la Comunidad de Madrid, sobre profesorado religioso en centros concertados"*

*Según II. Retribuciones. 2.c para complemento de antigüedad se computará, a TODOS los profesores religiosos, el equivalente a 8 trienios de antigüedad.*

*Según VIII. Vigencia y seguimiento, 2. Seguimiento y desarrollo de dicho acuerdo, existe una Comisión que se reúne, al menos, una vez al año, reuniones de las que se levanta acta en las que consten los acuerdos.*

*1. Solicito obtener copia de las actas de las reuniones de la comisión citadas*

*2. Solicito información acerca de número de profesores de centros privados con concierto que ha recibido dicho complemento asociado a 8 trienios*

*3. Solicito datos en relación con el coste que ha supuesto anualmente este complemento de 8 trienos, que según el acuerdo es para todo ese profesorado independientemente de su antigüedad real, desde la firma del acuerdo. La información sobre coste debe incluir si esos 8 trienios se han contabilizado y han supuesto alguna paga extraordinaria según artículos 62 y 62bis del VI Convenio colectivo de empresas de enseñanza privada sostenidas total o parcialmente con fondos públicos.*

---

Registrado como 49/513321.9/18

Se asigna 09-OPEN-00194.8/2018

Comento ideas

[https://twitter.com/FiQuiPedia/status/1053553158633459712](https://twitter.com/FiQuiPedia/status/1053553158633459712)  
Más ideas: En un centro "de monjas o de curas" sería posible que TODO el profesorado fuese religioso y todos tuvieran esas prebendas. ¿Cómo se tiene la condición legal de religioso? ¿Si yo digo que lo soy y me apunto hoy me vale para tener 8 trienios?

**3 noviembre 2019**

Preguntando a responsables de privados con concierto católicos me indican que se quedan cortos 8 trienios

[twitter FiQuiPedia/status/1058630909191176192](https://twitter.com/FiQuiPedia/status/1058630909191176192)  
Sobre los 8 trienios a TODO el profesorado religioso de los privados concertados de Madrid:  
"En cualquier caso, y centrándonos en el profesorado religioso, debo reconocerle a usted que, muy posiblemente, esos 8 trienios de media se queden un tanto cortos."  
![|225x320](https://pbs.twimg.com/media/DrEDMgZWoAAnS-w?format=png)  

[twitter emilio_daz/status/1058502542601191424](https://twitter.com/emilio_daz/status/1058502542601191424)  
Veo nuevo giro en tu argumentario, relativo a los trienios del profesorado religioso. Algunas notas: disp. Adicional 4ª del R. Dto 2377/85, sobre Reglamento Ns. Básicas de conciertos, que habla del monto equivalente del profesorado sin relación laboral (relig y cooperativistas)   
[twitter emilio_daz/status/1058503217355599877](https://twitter.com/emilio_daz/status/1058503217355599877)  
En su desarrollo, desde 1990 se establece el monto equivalente con una media de trienios, dada la imposibilidad de computarlos en este tipo de figuras al no mediar relación laboral. Dicho monto lo han ido actualizando las distintas CCAA, Madrid entre ellas https://twitter.com/emilio_daz/status/1058504032120176640 En cualquier caso, y centrándonos en el profesorado religioso, debo reconocerle a usted que, muy posiblemente, esos 8 trienios de media se queden un tanto cortos.

**20 noviembre 2018**

Recibo [resolución inadmitiendo](https://drive.google.com/open?id=17RODmwh9XKn9bXpdDWARO788NwMFylTc)  

**21 noviembre 2018**

Reclamo a CTBG con esta alegación

---

*Solicito unas actas asociadas al seguimiento de un acuerdo entre un organismo público y entidades privadas que supone gasto de dinero público, y considero que un acta, documento existente, no es información auxiliar ni de apoyo.*

*Solicito información acerca de número de profesores y coste asociado a un acuerdo que supone gasto de dinero público, y considero que es una información disponible, es información relevante, y no supone una "reelaboración", sino simplemente consultar ese dato ya que una gestión correcta del gasto del dinero público debe tener desglosado ese importe o permitir desglosarlo fácilmente. Es un acuerdo que tiene ya varios años y entiendo que un gestor público debe conocer ese importe además de incluirlo la partida en los presupuestos para cubrirlo con dinero público, luego considero que no se trata de una reelaboración.*

---

**6 marzo 2019**

Resolución [estimatoria CTBG RT520/2018](https://www.consejodetransparencia.es/ct_Home/dam/jcr:d58abcc6-3aa7-4586-94ea-a6805e0cd0bc/RT_0520_2018.pdf)  

**6 junio 2019**

Planteo nueva solicitud pasado tiempo sin cumplir resolución CTBG

---

*Solicito:*

*-Una copia del registro de entrada de la resolución estimatoria RT 0520_2018 del Consejo de Transparencia y Buen Gobierno de 6 marzo 2019 en la que se da un plazo de 20 días hábiles para facilitar información.*

*-Documentación de las gestiones realizadas por la Comunidad de Madrid para cumplir dicha resolución, o documentación del Contencioso Administrativo iniciado para no cumplirla.*

---

Se asigna 09-OPEN-00169.4/2019

**5 julio 2019**

Recibo nueva respuesta sin dar información

[twitter FiQuiPedia/status/1147132610184978432](https://twitter.com/FiQuiPedia/status/1147132610184978432)  
Sin vergüenza @sanz_ismael @R_vanGrieken incumplen RT520/2018 @ConsejoTBG  
-No facilitan información ni indican que hayan hecho nada para cumplir.  
-No ponen contencioso  
Habrá sanciones ley 10/2019 transparencia antes de que cumplan, si @ppmadrid no la para  
[2019-07-05-Resolucion.pdf](https://drive.google.com/file/d/1-wCO9re0rmC8-KvrU6iOM6Z0G0m751Uu/view)  

**7 diciembre 2019**

Uso nueva estrategia de usar artículo 53.1 de Ley 39/2015 (probada con tema veranos interinos pero allí sin éxito ya que sí que habían emitido respuesta a la resolución CTBG)

----

*Solicito, amparándome en artículo 53.1 de Ley 39/2015*

*-El estado de la tramitación del procedimiento asociado al cumplimiento de RT 0520_2018 del Consejo de Transparencia y Buen Gobierno, resolución de fecha 6 marzo 2019, para la que en Exp.: 09-OPEN-00169.4/2019 a fecha 5 julio 2019 la Comunidad confirmó que había recibido la resolución y que no había iniciado un contencioso administrativo para no cumplirla.*

*-Identificar las autoridades y personal bajo cuya responsabilidad se ha tramitado o se tramita el procedimiento asociado a RT 0520_2018.*

----

Registro 03/891386.9/19

Se asigna 09-OPEN-00315.5/2019

**20 diciembre 2019**

Recibo [respuesta en la que dan datos parciales](https://drive.google.com/open?id=1H000PWcm09QgEJDgr67LrHrJFGdSSBTN)  

Tras ver los datos, hago unos [pequeños cálculos en hoja de cálculo compartida](https://drive.google.com/open?id=1bAzusyhbIPy2TwAQ800EjjJqRe0F972yu0dQTjn0taw)  

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEivCIbJTNnGfsAYWxp1jzA61dq2r23p1v0FdTmctqJXKZFRlJq3Sn8tf-e9n1ZshyXFSuD25Ocl9vPfmctBLA2VHQmVcHrXnJJp3WQgWbqMfyo_fWmPgS1DBgsFIw5bnf08NyQTVOhs0PYt/s1600/Screenshot_2020-02-02+Madrid+pagos+trienos+a+religiosos+conciertos+.png)

El acuerdo se firmó en 2013, en plenos recortes, y desde entonces ha supuesto unos 10 M€ de dinero público que ha ido solamente a trabajadores de centros privados por su condición de religioso, unos 3500€ anuales adicionales de complemento por cada profesor religioso en privado con concierto.

Casi al mismo tiempo, surge esta noticia.

**19 diciembre 2019**

[Los docentes de Madrid, en el grupo de los que menos cobran Las diferencias salariales en distintas comunidades superan los 600 euros - elpais](https://elpais.com/ccaa/2019/12/19/madrid/1576782693_664064.html)  

**21 diciembre 2019**

Reclamo a CTBG

El punto 2 no lo reclamo, pero es peculiar

Indican "utilizando como dato más fiable el del promedio anual del número de profesores religiosos, dadas las constantes variaciones a causa de las altas y bajas de profesorado en la nómina de pago delegado." Usar un número promedio para dar datos de a cuántas personas se ha dado un complemento hablando de complemento en nóminas lo veo peculiar.

---

*Tras RT 0520/2018 Madrid no cumple resolución, y en respuesta a 09-OPEN-00169.4/2019 a fecha 5 julio 2019 indica que no ha puesto contencioso contra CTBG. En diciembre 2019 pongo nueva solicitud amparándome en artículo 53.1 de Ley 39/2015 para forzar cumplimiento y recibo esta respuesta*

*Considero que la información no satisface la solicitud tal y como refleja resolución RT 0520/2018, ya que no responden a todo*

*Me citan "escrito del Consejo de Transparencia y Buen Gobierno, de 14 de agosto de 2019" que desconozco y "en lo relativo a los dos únicas cuestiones pendientes de informar", sin que sepa que hayan informado de nada.*

*"Copia de las actas de las reuniones de la comisión de seguimiento del Acuerdo de 27 de diciembre de 2013.": no lo facilitan*

*"Información acerca de número de profesores de centros privados con concierto que ha  recibido dicho complemento asociado a 8 trienios.": sí facilitan datos de número promedio de profesores.*

*"Coste anual de este complemento de 8 trienios, desde la firma del acuerdo. La información sobre coste debe incluir  si esos 8 trienios se han contabilizado y han supuesto alguna paga extraordinaria según artículos 62 y 62bis del VI Convenio colectivo de empresas de enseñanza privada sostenidas total o parcialmente con fondos": facilitan coste total complementos, pero no información de la segunda parte sobre si ha habido pagas extraordinarias.*

---

**8 enero 2020**

Recibo [comunicación de CTBG](https://drive.google.com/open?id=1ULI9XZBw_PzETiv5idwNkEpvbc0p_Mrq)  

Respondo

---

En la respuesta se indica "resulta obligado informar que con posterioridad a dictar la Resolución RT-0520-2018, la Consejería de Educación e Investigación de la Comunidad de Madrid se dirigió a este Consejo con escrito de 7 de julio de 2019, que se adjunta, exponiendo un conjunto de consideraciones al tiempo de solicitar aclaración sobre los términos de la Resolución para atender adecuadamente al cumplimiento de lamisma. Dicho escrito fue objeto de contestación mediante escrito de este Consejo de 14 de agosto de 2019, que asimismo se acompaña. A la vista de su contenido, a juicio de este Consejo, la resolución dictada por Consejería de Educación e Investigación de la Comunidad de Madrid el 20 de diciembre de 2019 da cumplimiento, aunque con una demora injustificada, a la RT_0520_2018, de 6 de marzo de 2019." Me gustaría recibir documentación citada de la que no dispongo el escrito de 7 de julio de 2019 y la respuesta de 14 agosto 2019, que no se adjuntan al correo, y que a juicio de CTBG supusieron cumplimiento de RT 520/2018.

---

Recibo documentación

---

Buenos días, se adjuntan los escritos que por error no se han adjuntado en el anterior correo. Le aclaro que el escrito de 7 de julio, realmente es de fecha 10 de julio. Disculpe las molestias.

---

10 julio 2019: [escrito Madrid a CTBG](https://drive.google.com/open?id=0B-t5SY0w2S8iaU1LcGUxQkN1SUxXWUhhelZidWtFTGNSVzZj)  

14 agosto 2019: [escrito CTBG a Madrid](https://drive.google.com/open?id=0B-t5SY0w2S8iSHZxMkRFQXMyMGxGT2JKeWZPY096WjhKNk1R)  

**15 enero 2020**

Descubro que se cobra en más CCAA, planteo consulta a Ministerio vía sede

---

Desearía obtener una copia o enlace para consultar dos documentos:

-Acuerdo/convenio marco entre el Ministerio de Educación y Ciencia y la Federación Española de Religiosos de la Enseñanza de fecha 30 de octubre del 1989

-Instrucciones de 31 de marzo de 1992, de la Subdirección General de Régimen de Conciertos Educativos, del Ministerio de Educación Y Ciencia

---

**27 enero 2020**

Recibo respuesta

---

En respuesta a su consulta le indicamos que, en el fichero adjunto, enviamos el Acuerdo entre el Ministerio de Educación y Ciencia y la Federación Española de Religiosos de la Enseñanza de fecha 30 de octubre del 1989 publicado en el Boletín de la FERE, 1989, n. 329, p. 1-49

También hemos incluido el artículo Comentarios al acuerdo MEC-FERE publicado a continuación del Acuerdo en el mismo Boletín.

Le informamos que lamentablemente, con los datos aportados no ha sido posible encontrar las Instrucciones de 31 de marzo de 1992, de la Subdirección General de Régimen de Conciertos Educativos, del Ministerio de Educación Y Ciencia no las hemos localizado, debido, en parte, a la amplitud del campo "instrucciones". Le recordamos que este tipo de documentos pertenecen al régimen interno de aquel ministerio y las instrucciones concretas de aquel momento, por lo que pudieron no ser publicadas y registradas de manera oficial para ser recogidas en el Archivo General. No obstante, se va a continuar la búsqueda del documento por vías que requieren una mayor demanda de tiempo, por lo que, en caso de encontrar alguna novedad al respecto, nos pondremos en contacto con usted.

---

El anexo lo cito como referencia (1)

**4 febrero 2020**

Planteo la consulta de trienos a nivel nacional, no limitada a Madrid

No localizo convenio en
[Convenios y encomiendas - transparencia.gob.es](https://transparencia.gob.es/transparencia/transparencia_Home/index/PublicidadActiva/Contratos/Convenios-encomiendas.html)  

---

*El 30 de octubre del 1989 se firmó acuerdo entre el Ministerio de Educación y Ciencia y la Federación Española de Religiosos de la Enseñanza sobre los religiosos que prestan sus servicios como profesores en Centros concertados con el entonces Ministerio de Educación y Ciencia, fijándose un pago de 5 trienios a todos los religiosos a partir del 1 enero 1993.*

*1. Solicito obtener copia o enlace al convenio actual vigente que lo haya sustituido, en caso de existir y estar vigente.*

*2. Solicito información del número de trabajadores de centros privados que han recibido pago de trienios, indicando coste total anual de dinero público desglosado por año y territorio en el que el ministerio haya abonado dichos importes, desde el año 1993.*

---

Solicitud 001-040582

**5 junio 2020**
Recibo [resolución, junto con anexo (lo pongo en (1))](https://drive.google.com/file/d/1ZsgiKnMFUgs2kXdXRG_sAobAg4fVP9c3/view?usp=sharing)  

**28 agosto 2024**

Me planteo pedir de nuevo los datos actualizados hasta 2024

*El 27 diciembre 2013 se firmó "Acuerdo entre la Comunidad de Madrid (Consejería de Educación, Juventud y Deporte) y la Federación Española de Religiosos de Enseñanza-Titulares de Centros Católicos de la Comunidad de Madrid, sobre profesorado religioso en centros concertados"*

*Según II. Retribuciones. 2.c para complemento de antigüedad se computará, a TODOS los profesores religiosos, el equivalente a 8 trienios de antigüedad.*

*Solicito copia o enlace a la información anual desde 2019, inclusive, hasta 2024:* 

*1. Número de profesores de centros privados con concierto que ha recibido dicho complemento asociado a 8 trienios*

*2. Coste que ha supuesto anualmente este complemento de 8 trienos, que según el acuerdo es para todo ese profesorado independientemente de su antigüedad real, desde la firma del acuerdo. La información sobre coste debe incluir si esos 8 trienios se han contabilizado y han supuesto alguna paga extraordinaria según artículos 62 y 62bis del VI Convenio colectivo de empresas de enseñanza privada sostenidas total o parcialmente con fondos públicos.*

*La información fue facilitada desde 2014 a 2018 (con datos parciales de 2019) en* 09-OPEN-00194.8/2018 reclamada con resolución estimatoria RT_0520_2018.pdf y ejecutada con 09-OPEN-00315.5/2019 

43/153807.9/24

**24 septiembre 2024**

Recibo [resolución con datos hasta 2024](https://drive.google.com/file/d/1epfyQmEzmYoxw1s2X5oaA-Usyy0h9DZl/view?usp=drive_link)  

Actualizo la [hoja de cálculo](https://docs.google.com/spreadsheets/d/1bAzusyhbIPy2TwAQ800EjjJqRe0F972yu0dQTjn0taw/edit?usp=drive_link)  


### Cantidad de centros con titular congregaciones/instituto religioso y de profesorado religioso 

Dentro de [escuelascatolicas.es estadística](https://www.escuelascatolicas.es/estadistica/)  

[Datos y cifras de la Educación Católica. Estadística Curso 2016-2017](https://www.escuelascatolicas.es/wp-content/uploads/2018/02/Estadistica20162017.pdf)  

En página 13 se indica

Porcentaje de Religiosos (6,1%) y Seglares (93,9%)

1.2.2.B. Centros de Escuelas Católicas por tipo de titular y concierto.

Centros de EC: 1378/2000=68,9%

1.4.1. Personal por categoría, sexo y condición eclesial.

Religiosos 2.442 hombres y 5.309 mujeres

1.4.1.B. Personal de Escuelas Católicas por categoría, sexo y condición eclesial.

Religiosos 2.145 hombres y 4.690 mujeres

2.1.3.1. Profesores en centros católicos por sexo y condición eclesial.

Madrid 245 hombres y 471 mujeres

Religiosos 4,5% y seglares 95,5%

2.1.3.2. Personal no docente en centros católicos por sexo y condición eclesial.

Religiosos 22,9% y seglares 77,1%

**2.2.3.1. Profesores en centros de Escuelas Católicas por sexo y condición eclesial.**

**Madrid 231 hombres y 450 mujeres**

**Religiosos 5% y seglares 95%** 

>En 2016 Madrid indicado 491 profesorado religioso beneficiado y es un valor parecido (implicaría que no aplica a todos, pero esta estadística habla de centros de EC, y no todos están concertados)

### Paga adicional a profesorado de privados con concierto

Es un tema asociado a profesorado de privados con concierto, no limitado a religiosos, pero lo conozco asociado a este caso de trienios de religiosos.

Se cita en el acuerdo

[Resolución de 30 de julio de 2013, de la Dirección General de Empleo, por la que se registra y publica el VI Convenio colectivo de empresas de enseñanza privada sostenidas total o parcialmente con fondos públicos.](https://www.boe.es/buscar/doc.php?id=BOE-A-2013-9028)  
>Artículo 62. Paga Extraordinaria por antigüedad en la empresa.  
Los trabajadores que cumplan 25 años de antigüedad en la empresa, tendrán derecho a una única paga cuyo importe será equivalente al de una mensualidad extraordinaria por cada quinquenio cumplido.  

Esta paga extraordinaria se puede ver asociada en general a profesorado de privados con concierto, no solo religiosos. Habla de "única paga", no indica que sea anual, pero es una mensualidad por quinquenio, luego con 25 años serían 5 mensualidades.

**30 julio 2004**

[La DGA asume al final la paga de la concertada Unos 700 docentes esperan esta extra por antigüedad desde hace cuatro años -elperiodicodearagon](https://www.elperiodicodearagon.com/amp/noticias/aragon/dga-asume-final-paga-concertada_132149.html)  
>Finalmente, tras un tira y afloja en la negociación, el Gobierno de Aragón abonará esta controvertida gratificación a unos 700 profesores aragoneses con al menos 25 años de antigüedad que llevan cuatro esperando cobrar este plus, que está reflejado en el convenio colectivo que firmaron las patronales y los sindicatos nacionales en el 2000. La media de lo que percibirá cada trabajador está en torno a los 9.000 euros, dependiendo del número de quinquenios y de la categoría profesional.  
La DGA se negó a abonar este complemento desde el principio, ya que consideraba injusto que patronales y sindicatos hubieran acordado un plus que después debía asumir la Administración autonómica, que es la que tiene la obligación de pagar al profesorado. El pasado mes de junio, el Tribunal Supremo admitió a trámite un recurso del Gobierno aragonés y las posturas volvieron a acercarse hasta la firma de ayer del acuerdo.

Se aprobó en 2000  
[RESOLUCIÓN de 2 de octubre de 2000, de la Dirección General de Trabajo, por la que se dispone la inscripción en el Registro y publicación del IV Convenio Colectivo de Empresas de Enseñanza Privada sostenidas total o parcialmente con Fondos Públicos.](https://www.boe.es/boe/dias/2000/10/17/pdfs/A35576-35597.pdf)  
> Artículo 61.Paga extraordinaria por antigüedad en la empresa.  
Los trabajadores que cumplan veinticinco años de antigüedad en la empresa, tendrán derecho a un paga cuyo importe será equivalente al de una mensualidad extraordinaria por cada quinquenio cumplido.

No parece estar en [versión anterior de convenio de 1997](https://boe.es/boe/dias/1997/11/13/pdfs/A33261-33262.pdf)  

**9 enero 2020**

Planteo solicitud transparencia al ministerio

---

En el año 2000 se aprobó IV Convenio Colectivo de Empresas de Enseñanza Privada sostenidas total o parcialmente con Fondos Públicos y desde entonces existe una Paga extraordinaria por antigüedad en la empresa (en IV convenio es en artículo 61, en actual VI Convenio es en artículo 62):  
Los trabajadores que cumplan veinticinco años de antigüedad en la empresa, tendrán derecho a un paga cuyo importe será equivalente al de una mensualidad extraordinaria por cada quinquenio cumplido.

Solicito copia o enlace a información del número de trabajadores de centros privados que han recibido dicha paga extraordinaria adicional por dicho motivo, indicando coste total anual de dinero público desglosado por año y territorio en el que el ministerio haya abonado dichos importes, desde el año 2015.

---

**12 febrero 2020**

Recibo [datos de Ceuta y Melilla](https://drive.google.com/open?id=1tM-WYsax-bao2_bmywOv_vbfUp77yyqV)  
[Anexo con datos](https://drive.google.com/open?id=1tLAAvXDMfd5dlNXacR1A12t1tD1897ll)  

![](https://pbs.twimg.com/media/EQkid3_WkAAagcD?format=jpg)  

### Normativa y situación laboral de los religiosos

A los religiosos se les reconoció como cotizado a la Seguridad Social periodos de tiempo, pero lo que hace Madrid de reconocer trienios de antigüedad y solamente en caso de que sean pagados con dinero público en privados con concierto es distinto.

Intento recopilar normativa

[Real Decreto 3325/1981, de 29 de diciembre, por el que se incorpora al Régimen Especial de la Seguridad Social de los Trabajadores por Cuenta propia o Autónomos a los religiosos y religiosas de la Iglesia Católica.](https://www.boe.es/buscar/doc.php?id=BOE-A-1982-1407)  

[Real Decreto 2377/1985, de 18 de diciembre, por el que se aprueba el Reglamento de Normas Básicas sobre Conciertos Educativos.](https://www.boe.es/buscar/act.php?id=BOE-A-1985-26788#cuarta)  
> Disposición adicional cuarta.  
>1. Las retribuciones de los profesores que presten servicios en centros concertados sin tener relación contractual de carácter laboral con la entidad titular del centro, serán abonadas directamente a ésta por la Administración, previa declaración por la entidad titular, y conformidad expresa del profesor, acerca de la inexistencia de la citada relación contractual. A tales efectos, la entidad titular remitirá a la Administración la relación individualizada de dicho profesorado.  
La Administración, al abonar las retribuciones de este profesorado, que tendrán un monto equivalente al que la Administración satisface por el concepto de salarios del personal docente, efectuará e ingresará en el Tesoro las retenciones correspondientes al impuesto sobre la renta de las personas físicas. Asimismo, realizará las oportunas cotizaciones a la Seguridad Social.  
> 2. A efectos de lo dispuesto en el artículo 60.6 de la ley orgánica 8/1985, de 3 de julio, la terminación de la actividad docente del profesorado a que se refiere la presente disposición no tendrá el carácter de despido. Las vacantes así producidas serán provistas en todo caso de acuerdo con el procedimiento establecido en el artículo 60 de la citada ley, procediéndose a la formalización del correspondiente contrato de trabajo, salvo que se produzca de nuevo la situación regulada en el apartado primero de la presente disposición.  
>3. Al personal a que hace referencia esta disposición le será aplicable por analogía la edad de jubilación que se establezca en la normativa laboral aplicable. Asimismo, y también por analogía, le será aplicable la excepción en el procedimiento de provisión prevista en el artículo 26.3 del presente reglamento.  
>4. Lo establecido en esta disposición se entenderá sin perjuicio de lo dispuesto en la legislación vigente respecto al profesorado cuya relación con la titularidad del centro no tenga el carácter de contrato laboral.

Se cita [artículo 60.6 de Ley Orgánica 8/1985 (LODE)](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978&b=71&tn=1&p=20131210#asesenta) que se derogó por LOMCE y se recuperó en LOMLOE

[Real Decreto 2665/1998, de 11 de diciembre, por el que se completa el Real Decreto 487/1998, de 27 de marzo, sobre reconocimiento, como cotizados a la Seguridad Social, de los períodos de actividad sacerdotal o religiosa a los sacerdotes y religiosos o religiosas de la Iglesia Católica secularizados.](https://www.boe.es/buscar/act.php?id=BOE-A-1999-345)  

[Real Decreto 1512/2009, de 2 de octubre, por el que se modifican el Real Decreto 487/1998, de 27 de marzo, sobre reconocimiento, como cotizados a la Seguridad Social, de períodos de actividad sacerdotal o religiosa de los sacerdotes y religiosos o religiosas de la Iglesia Católica secularizados, y el Real Decreto 2665/1998, de 11 de diciembre, por el que se completa el anterior real decreto.](https://www.boe.es/buscar/doc.php?id=BOE-A-2009-16770)  

**17 febrero 2011**

[La gran mayoría de los 60.000 religiosos que hay en España son autónomos Hasta la llegada de democracia, los curas y las monjas no tenían seguridad social en España - cadenaser](https://cadenaser.com/ser/2011/02/17/sociedad/1297912405_850215.html)

**31 mayo 2015**

[Jubilación de sacerdotes o religiosos secularizados - iberley](https://www.iberley.es/temas/jubilacion-sacerdotes-religiosos-secularizados-12131)  

**17 marzo 2016**

[La jubilación de los religiosos y las religiosas - vidanuevadigital](http://www.vidanuevadigital.com/blog/la-jubilacion-de-los-religiosos-y-las-religiosas-alter-consultores-legales/)  

**27 febrero 2017**

[Sacerdotes y religiosos, los autónomos desconocidos - autonomosyemprendedor](https://www.autonomosyemprendedor.es/articulo/tu-historia/sacerdotes-religiosos-autonomos-desconocidos/20170227170449009247.html) 
> En España hay 42.921 religiosos según la Conferencia Española de Religiosos, de los que al menos la mitad están afiliados al RETA. Aunque no desempeñen muchos de ellos actividad por cuenta propia, se decidió que cotizaran por la base mínima para que tuvieran una pensión.  
...  
Esto quiere decir que serán autónomos todos aquellos religiosos que realicen una “actividad” en favor de la Congregación a la que pertenezca o en una entidad que tenga un convenio con dicha institución. Por ejemplo, si eres un religioso que pertenece a la Congregación de los Jesuitas y empiezas a dar clase como profesor en un centro que no tiene un convenio de colaboración con ésta, el contrato será de asalariado y se integrará en el Régimen General. Pero si el centro donde desarrolla la actividad dispone de dicho convenio con la Congregación pasará a formar parte del Régimen de Autónomos.

**10 mayo 2018**

[Sacerdotes y relaciones laborales - expansion](https://www.expansion.com/blogs/sagardoy/2018/05/10/sacerdotes-y-relaciones-laborales.html)  
El pasado 13 de abril el Tribunal Superior de Justicia de Madrid dictaba una sentencia curiosa por los actores implicados, además con interés desde el punto de vista laboral por las consideraciones que en ellas se hace.  
El litigio surge entre un religioso, el Arzobispado de Madrid y la Archidiócesis de Getafe, después de que estos le retiraran el estado clerical y le desvincularan del sacerdocio, al considerar probado que había cometido una serie de actos delictivos. Ante esta situación el afectado demandaba señalando que la relación que le había vinculado con la Iglesia debía considerarse de carácter laboral. 

### Formación religiosos

Es algo que sale en la [séptima adenda del convenio de 2013](https://drive.google.com/open?id=17Z1kazHe5_YroswozZ2SxRDrA73Gy93K)  

### "Liberados sindicales" religiosos

Es algo que sale en [adenda convenio de 2013](https://drive.google.com/open?id=1dYAF9X22l7IsP-NDuArW9QX79Tw23yVp)  

FERE-CECA MADRID podrá seleccionar profesores incluidos en la nómina de pago delegado (religiosos o no religiosos) de centros privados concertados, liberándolos de la tarea docente, para la realización de labores de representación, con dedicación plena o parcial a estas tareas.

### Jubilación de religiosos

En general históricamente se les reconoce como cotizados años realizadas labores religiosas para que puedan cobrar pensión.

El acuerdo de Madrid indica la paga extraordinaria asociada a profesorado de concertados con 25 años de antigüedad en caso de religiosos que se jubilen con 2 años.

[Derecho eclesiástico español. José M. González del Valle](https://books.google.es/books?id=6R0gsvxvka8C&pg=PA277&lpg=PA277#v=onepage&q&f=false)  

Convenios colectivos (nota 32)  

Nota 32: suelen contemplar cláusulas especiales par los religiosos profesos en materia de jubilación

En (1) se indica  
>Premio de jubilación. Los profesores religiosos que se acojan a la jubilación percibirán por este concepto las cantidades aplicables, por analogía a los profesores con relación laboral contractual que cuenten con la antigüedad mínima para tener derecho a dicho premio.  
...  
C) Premio de jubilación  
Por los profe ore religiosos que se jubilen el titular percibirá el monto equivalente de la retribución de los profesores contratados laborales que cuenten con la antigüedad mínima para tener derecho a dicho premio.
Con la actual redacción del convenio Colectivo (art. 71) por todos los religiosos que se jubilen se percibiría, como premio de jubilación el importe de tres mensualidades extraordinarias.  

### Nómina, monto equivalente y pago delegado

Un tema importante es entender cómo llega ese dinero público a esos profesores

**20 octubre 2018**

Comento idea

[twitterFiQuiPedia/status/1053429524904534017](https://twitter.com/FiQuiPedia/status/1053429524904534017)  
Me surge una pregunta a mi: si religiosos son contratados como profesores de religión (a priori es la materia que veo más probable) por un centro privado ¿influye en algo ese régimen Seg Social vs cómo son contratados resto profesores religión no religiosos?

Una respuesta cita monto equivalente

**3 noviembre 2018**

[twitterRaquelPippi/status/1058646235001839616](https://twitter.com/RaquelPippi/status/1058646235001839616)  
El texto se refiere a profes religiosos, curas y monjas que son profes de centros privados concertados, de cualquier asignatura. No tienen nómina, la C de Madrid le paga a su congregación lo que se denomina "monto equivalente", lo que cobrarían si tuvieran nómina.

Tras ver la respuesta de 20 diciembre 2019, a mi me extraña mucho el tema de "promedio anual del número de profesores"  
 **¿promedio?**  
 arece ser que los religiosos no pueden cobrar nómina, se paga a la congregación En las [instrucciones de pago de Madrid](https://drive.google.com/file/d/16QskgF3pNacWx5jRkpzMfSD_-s4TnE69) yo interpreto que depende de los centros definir un monto equivalente a recibir, y eso abre la puerta a la arbitrariedad absoluta  
 >SEGUNDA: Cálculo del monto equivalente.  
 TERCERA: Procedimiento de abono.  
 a) La Administración educativa solicitará a los titulares la documentación relativa al cumplimiento de los requisitos necesarios para el abono de esta paga, quedando éstos sujetos a las obligaciones tributarias y de Seguridad Social que pudieran corresponder como consecuencia de la ejecución del concierto.  

El pago delegado yo lo entiendo como que directamente la administración paga al trabajador del privado con concierto sin pasar el dinero por el propio centro. El monto equivalente se pagaría a la congregación directamente sin pasar por el centro, pero su importe lo fija el centro, y parece que se paga al centro y eld centro lo pasa a la congregación. Si se paga directamente al centro, no veo el concepto de pago delegado, que sí citan en información 20 diciembre 2019

**"dadas las constantes variaciones a causa de las altas y bajas de profesorado en la nómina de pago delegado."**

### Fraudes en pago a religiosos

**14 julio 2016**

[Fraude en centros concertados católicos de Valencia - laicismo.org](https://laicismo.org/fraude-en-centros-concertados-catolicos-de-valencia/)  
> Marzà admite que había docentes en la concertada que cobraban sin trabajar  
Desvela que la conselleria «no tenía ningún control sobre las horas que pagaba» – Anuncia una aplicación para «cruzar la nómina con el horario»  
...  
Entre los colegios investigados existen dos tipos de centros. Los centros educativos privados financiados con fondos públicos a los que la Conselleria de Educación paga la nómina a los «empresarios autónomos». Y los autónomos religiosos que imparten clase en centros regentados por congregaciones católicas (que son mayoría entre los investigados) que cobran en sus cuentas las nóminas de estos profesores. A ninguno se les practica retención del impuesto de la renta de las personas físicas (IRPF) porque no tributan por este concepto, acogiéndose al concordato vigente de la Santa Sede.  
...  
Sin embargo, algunas de las congregaciones religiosas siguen cobrando las nóminas como si estuvieran en activo, cuando no lo están.  

**15 julio 2016**

[Las escuelas católicas atribuyen el escándalo de las nóminas a un problema de Conselleria - valenciaplaza](https://valenciaplaza.com/los-centros-catolicos-atribuyen-el-escandalo-de-las-nominas-a-un-fallo-de-conselleria)  
El colectivo recuerda que los centros subvencionados presentan ante la Administración un registro completo de su actividad, pero depende de ellos contrastar los datos  
*La diputada especificó que quizá esas personas “ni lo saben, porque quien cobra ese dinero son las congregaciones religiosas para las que trabajan”.*

### Otro personal religioso pagado con dinero público

[Real Decreto 1145/1990, de 7 de septiembre, por el que se crea el Servicio de Asistencia Religiosa en las Fuerzas Armadas y se dictan normas sobre su funcionamiento.](https://www.boe.es/buscar/act.php?id=BOE-A-1990-23337)  
> Art. 12.  
> 1. El personal de carácter permanente del Arzobispado Castrense percibirá las siguientes retribuciones:  
a) Las básicas serán las correspondientes a los funcionarios del grupo A.  
b) El complemento de empleo se percibirá en las siguientes cuantías:  
1.º Para el personal con más de veinticinco años de servicio, el correspondiente al nivel 29.  
2.º Para el personal con más de quince años de servicio, el correspondiente al nivel 28.  
3.º Para el resto del personal, el correspondiente al nivel 27.  
c) El complemento específico será igual al importe fijado, para el componente general del complemento específico, en las disposiciones vigentes sobre retribuciones del personal de las Fuerzas Armadas, para empleos militares de igual nivel de complemento de empleo.  
Con criterios similares a los utilizados para la asignación de las características retributivas de la relación de puestos militares, se podrá fijar para los puestos de este colectivo complementos específicos más elevados, incompatibles con los anteriores. Dicha asignación será aprobada por la Comisión Superior de Retribuciones Militares, necesitando el informe previo favorable del Ministerio de Economía y Hacienda, cuando supongan incremento de gasto.  
>2. El personal temporal percibirá el sueldo correspondiente a los funcionarios del grupo A y no devengará trienios. El complemento de empleo será el correspondiente al nivel 26, y el complemento específico será de igual importe que el del componente general del complemento específico correspondiente al empleo militar de igual nivel de complemento de empleo, fijado en las disposiciones vigentes sobre retribuciones del personal de las Fuerzas Armadas.  
>3. También podrán percibir indemnización por razón del servicio.

## Referencias

(1) 27 enero 2020 (ver búsqueda de transparencia) recibo un pdf de 18 páginas que es "Boletín de la FERE, Noviembre 1989, Nº329, ACUERDOS SOBRE RELIGIOSOS EN CENTROS CONCERTADOS"

No publico aquí porque indica *"Las obras del repertorio de CEDRO reproducidas están protegidas por el derecho de autor, y su reproducción y comunicación pública se han realizado con su autorización. Cualquier reproducción, distribución, transformación y comunicación pública, en cualquier medio o cualquier forma, que no esté amparada por la ley o por esta licencia requiere autorización expresa. Diríjase a CEDRO si necesita autorización (www.conlicencia.com)."*

El 5 junio 2020 recibo [pdf, sin ser publicación Cedro y lo puedo compartir](https://drive.google.com/file/d/1B_03XKdkGHijWWUrZyZQIA1BhpN2iRBb/view?usp=sharing)  

