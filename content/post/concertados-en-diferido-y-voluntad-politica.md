+++
author = "Enrique García"
title = "#ConcertadosEnDiferido y voluntad política"
date = "2022-12-13"
tags = [
    "educación", "normativa", "Post migrado desde blogger", "Privatización", "Conciertos", "Privados con concierto"
]
toc = true

+++

Revisado 20 agosto 2024

## Resumen

En 2022 en Madrid van a crearse nuevos #ConcertadosEnDiferido: intento comentar qué voluntades política ha habido y hay para que eso siga pasando o deje de pasar.

Ideas de este post se citan en artículo 24 diciembre 2022, al tiempo que se comenta legalmente 

[La nueva ley de educación no consigue frenar la cesión de suelo público para colegios concertados - eldiario](https://www.eldiario.es/sociedad/nueva-ley-educacion-no-frenar-cesion-suelo-publico-colegios-concertados_1_9786300.html)

También relacionado con este artículo de Agustín Moreno de 10 enero 2023
[La LOMLOE o las cosas a medias - eldiario](https://www.eldiario.es/opinion/tribuna-abierta/lomloe-cosas-medias_129_9852812.html)

## Detalle

En diciembre 2022 surgen en prensa nuevas cesiones de suelo público realizadas por la Comunidad de Madrid para privados con concierto: Villalbilla, Valdebebas, Vallecas y Vicálvaro, reactivando lo que se denominan #ConcertadosEnDiferido: situación en la que la administración cede suelo público a una empresa privada y le garantiza el concierto antes de que exista el centro privado. El término surge en 2018 con un caso en Torrejón de Ardoz en el que las actas del ayuntamiento muestran que la administración solo contemplaba un centro privado concertado, pero que tras la movilización, se consiguió que se crease un centro público, el actual CEE Íker Casillas.

[Madrid crea centros privados con concierto en diferido](http://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html)

[La Comunidad de Madrid no construye centros públicos, y lo pone por escrito](http://algoquedaquedecir.blogspot.com/2018/02/la-comunidad-de-madrid-no-construye-centros-publicos.html)

En 2020 se consiguió un inventario de bienes cedidos y se documentaron casos

[Centros privados con concierto en diferido: inventario Madrid](https://algoquedaquedecir.blogspot.com/2021/03/centros-privados-con-concierto-en-diferido-inventario-madrid.html)

Globalmente muestra un modus operandi de la Comunidad de Madrid, y el número de casos es suficientemente significativo para que sea citado en el Senado en julio 2020 por la ministra de educación.

Procede plantearse dos preguntas:

* Qué encaje legal tienen este tipo de prácticas  
* Qué voluntad política ha habido asociada a permitir o evitar estas prácticas, tanto a nivel de Madrid (que lo realiza) como del Ministerio (que lo cita y conoce)  

“La cesión de suelo público a una empresa privada garantizando el concierto antes de que exista el centro privado” se puede analizar legalmente por partes:

1. Normativa asociada a concesiones de suelo público  
2. Normativa asociada a concertar un centro privado  
3. Normativa asociada a concertar en diferido un centro inexistente  

No hay problema en punto 1 ([Artículo 93. Concesiones demaniales, Ley 33/2003, de 3 de noviembre, del Patrimonio de las Administraciones Públicas](https://www.boe.es/buscar/act.php?id=BOE-A-2003-20254#a93), [Artículo 37. Concesión de servicio público y demanial, Ley 3/2001 Patrimonio Comunidad Madrid](https://www.boe.es/buscar/act.php?id=BOE-A-2001-14644#a37)), ni punto 2 

([artículo 28 en Real Decreto 2377/1985 Reglamento de Normas Básicas sobre Conciertos Educativos](https://www.boe.es/buscar/act.php?id=BOE-A-1985-26788#a28)), pero el detalle está en el punto 3.

Una cosa es solicitar concierto para un centro “de nueva creación”, pero que tiene entidad como centro, y otra es un centro totalmente inexistente, y que se le denomine concertado antes de existir: 

* Concursos indicaban "El adjudicatario asumirá la obligación de construir el centro docente en los plazos previstos en el pliego, solicitar el régimen de concierto, cumpliendo todos los requisitos y condicionantes impuestos por la autoridad educativa, equiparlo y gestionarlo durante el plazo…"  
* Concursos obligaban a poner un cartel de obra de 3x2,7 m llamando concertado a centro inexistente.  

Esta idea se cita en análisis legales 

[Revista catalana de derecho público Núm. 51, Diciembre 2015 La cesión de suelo público dotacional para la apertura de centros docentes concertados: una nueva manifestación del estado garante](https://libros-revistas-derecho.vlex.es/vid/cesion-suelo-publico-dotacional-594075354)  
> En definitiva, al no admitir la legislación básica[53](https://libros-revistas-derecho.vlex.es/vid/cesion-suelo-publico-dotacional-594075354#footnote_53)el otorgamiento directo de conciertos, ni siquiera a quienes compitiendo con otros licitadores hubieran resultado adjudicatarios de suelo público, las CC. AA. carecían de margen para concertar automáticamente las solicitudes de los cesionarios de suelo público.[54](https://libros-revistas-derecho.vlex.es/vid/cesion-suelo-publico-dotacional-594075354#footnote_54)A estos efectos era irrelevante que en la licitación se hubieran valorado los aspectos educativos del proyecto o tenido en cuenta los criterios de preferencia del[artículo 116.2 LOE](https://legislacion.vlex.es/vid/ley-organica-educacion-296525) para el otorgamiento de conciertos.

>[[53]](https://vlex.es/vid/cesion-suelo-publico-dotacional-594075354#footnote_reference_53) Fundamentalmente la LOE, LODE y el [Real Decreto 2377/1985, de 18 de diciembre](https://vlex.es/vid/real-decreto-aprueba-reglamento-940184272), por el que se aprueba el Reglamento de Normas Básicas sobre Conciertos Educativos.  
[[54]](https://vlex.es/vid/cesion-suelo-publico-dotacional-594075354#footnote_reference_54) De acuerdo con el [artículo 116.3](https://vlex.es/vid/ley-organica-educacion-296525) [LOE](https://vlex.es/vid/ley-organica-educacion-296525), compete al Gobierno regular los aspectos básicos del concierto, entre los que se encuentran la tramitación de las solicitudes. Según el reglamento de desarrollo de la LODE, estas deben presentarse en el mes de enero. Las resoluciones de aprobación o denegación tendrán que haber recaído antes del 15 de abril. Si no existen fondos suficientes, habrá que acudir a los criterios de preferencia del [artículo 116.2](https://vlex.es/vid/ley-organica-educacion-296525) [LOE](https://vlex.es/vid/ley-organica-educacion-296525). Por este motivo es dudosa la legitimidad de la adjudicación de conciertos educativos directamente y al margen de los criterios de preferencia de la LOE.

Además hay otro tema relevante: los conciertos de unidades suponen necesidades de escolarización y una asignación presupuestaria; si ambas no existen el concierto puede ser modificado o rescindido. Garantizar el concierto durante décadas, sin tener garantías de esa información, supone garantizar millones de euros durante décadas hipotecando unos recursos públicos que se desconocen para unas posibles necesidades también desconocidas. La bajada de natalidad sugiere que dentro de poco tiempo las necesidades de escolarización serán menores, pero una empresa privada tiene garantizado el concierto. Pocas veces se realizan actuaciones políticas más allá de una legislatura, cuando no se sabe qué voluntad política tendrá el posible nuevo gobierno, si no es por voluntad política concreta, como son infraestructuras públicas para todos o intereses privados afines.

La muestra de que la legalidad no estaba totalmente clara es que LOMCE añadió en 2013 116.8 en LOE, que no estaba antes, y que indica

[Artículo 116. Conciertos (modificación publicada el 10/12/2013)](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=167&tn=1&p=20131210#a116)  
> 8. Las Administraciones educativas podrán convocar concursos públicos para la construcción y gestión de centros concertados sobre suelo público dotacional  

Se puede ver como ese punto no estaba en LOE original y que es eliminado por LOMLOE, por lo que estuvo en vigor entre 30/12/2013 y 19/01/2020.

Viendo el inventario de casos de Madrid, Madrid lo estuvo haciendo ANTES de que apareciese ese cambio.

Si eso ya es llamativo, no lo es menos que la eliminación de dicho punto por LOMLOE en 2020 fue cuestionada, y no solo por PP,  
[twitter begonavillacis/status/1340689367992594435](https://twitter.com/begonavillacis/status/1340689367992594435)  
La #LeyCelaá pretende acabar con la educación concertada prohibiendo a los Aytos ceder suelo para la construcción de escuelas.  
Nos adelantamos a su aprobación y cedemos una parcela en el Cañaveral.  
Madrid será el dique de contención del gobierno autoritario de PSOE y Podemos.  

De modo que Madrid impulsó su [Ley 1/2022, de 10 de febrero, Maestra de Libertad de Elección Educativa de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-6768)  

para introducirlo de nuevo en [artículo 7.3](https://www.boe.es/buscar/act.php?id=BOE-A-2022-6768#a7)  
> 3. La Comunidad de Madrid podrá convocar concursos públicos para la construcción y gestión de centros privados sostenidos con fondos públicos sobre suelo público dotacional.

La pregunta obligada y planteada varias veces sin respuesta es ¿para qué hacía falta introducirlo en normativa en 2022, si Madrid lo hacía antes de que estuviese en normativa en 2013?  

**2022**   
[https://twitter.com/FiQuiPedia/status/1496874496095309828](https://twitter.com/FiQuiPedia/status/1496874496095309828)  
Sigo esperando a que @eossoriocrespo @ppmadrid expliquen para que lo ponen en ley si lo hacían antes de que la ley lo permitiese  
![](https://pbs.twimg.com/media/FMX3a9wWYAoXKAN?format=jpg)  
 
**2021**  
[https://twitter.com/FiQuiPedia/status/1403431079546064906](https://twitter.com/FiQuiPedia/status/1403431079546064906)  
Por cierto, @eossoriocrespo ¿para qué hace falta poner la cesión de suelo público para #ConcertadosEnDiferido en ley de Madrid, si @ppmadrid ya lo hacía antes?  

Se puede considerar objetivo decir que existe voluntad política inequívoca de PP en la defensa de los privados (al introducir en 2013 116.8 en LOMCE y al introducir en 2022 7.3 en Ley Maestra), y que no habido la misma voluntad por el gobierno al elaborar la LOMLOE de no permitir estas prácticas en normativa básica de obligado cumplimiento por las CCAA. Verlo como ingenuidad o connivencia depende de cada uno, pero es evidente que PP es especialista en aprovechar cualquier agujero en la normativa, y si existe una voluntad política real, hay que plasmarla cuando se tiene la posibilidad de legislar, como no duda en hacer el PP.  

**15 diciembre 2022**
[DIARIO DE SESIONES DE LA ASAMBLEA DE MADRID / NÚM. 382 / 15 DE DICIEMBRE DE 2022](https://www.asambleamadrid.es/static/doc/publicaciones/XII-DS-382.pdf#page=33)  
Habla Ossorio  
> Si ustedes, señor Fernández Rubiño, no querían que los ayuntamientos pudieran ceder parcelas a los concertados, se lo podían haber dicho a sus compañeros de bancada y que lo hubiesen dicho en la LOMLOE, pero no lo han dicho y por eso nuestros procedimientos son escrupulosamente legales. 

En LOMLOE en 2020 además de eliminar 116.8 se hace otro cambio al añadir la palabra “público” en Disposición adicional decimoquinta.4 de LOE, palabra que no estaba en LOE ni LOMCE.

[Disposición adicional decimoquinta. Municipios, corporaciones o entidades locales.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dadecimoquinta)  
> 4. Los municipios cooperarán con las Administraciones educativas correspondientes en la obtención de los solares necesarios para la construcción de nuevos centros docentes **públicos**. 

De nuevo el cambio es insuficiente, la redacción no impide que también “cooperen” para centros privados, y no es normativa básica según [disposición final quinta](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta), por lo que las CCAA pueden cambiarlo.

Además del PP de Madrid, se puede ver como PP de Comunidad Valenciana, firmado por Albert Fabra, hizo un movimiento similar en 2011, en [Artículo 116. La iniciativa social en la promoción de centros educativos de Ley 9/2011, de 26 de diciembre, de Medidas Fiscales, de Gestión Administrativa y Financiera, y de Organización de la Generalitat.](https://www.boe.es/buscar/doc.php?id=BOE-A-2012-1253)  
> La Generalitat podrá colaborar con los particulares en la promoción de centros educativos de iniciativa social, con el fin de garantizar la libertad de enseñanza. A tal efecto, podrá otorgar concesiones sobre bienes demaniales de su titularidad, así como los que le hayan sido transmitidos por otras administraciones, para la construcción de centros educativos de titularidad privada que reúnan los requisitos para ser sostenidos con fondos públicos.

**20 junio 2014**  
[El nuevo colegio concertado de Calp tendrá 3 líneas y deberá pagar un mínimo anual a la Generalitat de 8.500 euros](https://calpdigital.es/art/2842/el-nuevo-colegio-concertado-de-calp-tendra-3-lineas-y-debera-pagar-un-minimo-anual-a-la-generalitat-de-8500-euros)  
![](https://calpdigital.es/upload/img/periodico/img_6317.jpg)  

Se ve un cartel similar a los carteles de Madrid

[twitter FiQuiPedia/status/1378726788784852994](https://twitter.com/FiQuiPedia/status/1378726788784852994)  
Aparte de ayuntamientos, me limito a @educacmadrid vía concursos que obligaban a poner un cartel de obra de 3x2,7 m llamando concertado a centro inexistente.  
Hago un repaso difundiendo sus mismos carteles, completándolos con más datos, contrastables.  
![](https://pbs.twimg.com/media/EyI4rRdXEAARR4X?format=png)  

Se ha “vendido” desde el gobierno que la LOMLOE prohibía esas prácticas, pero no lo ha prohibido y deja abierto a que Madrid lo haga.

**19 noviembre 2020**  

[twitterPSOE/status/1329323590202908672](https://twitter.com/PSOE/status/1329323590202908672)  
¿También has oído eso de que "retirar el concepto de demanda social asfixia a la concertada"?  
❌ES FALSO  
➜Ese concepto se usaba para ceder suelo público gratis garantizando conciertos a empresas para crear 🏫 con ánimo de lucro.  
Que no te engañen🤷🏻‍♀️  

**6 noviembre 2020**  
[twitter PodemosEduca/status/1324653916479434754](https://twitter.com/PodemosEduca/status/1324653916479434754)  
Superado el primer trámite para evitar la cesión de suelo público a centros concertados y la financiación pública a los centros que segregan por sexo.  

Se cita artículo 5 noviembre 2020

[El Congreso aprueba cambios en la ley de Educación: prohíbe ceder suelo a la concertada y subvencionar los centros que segreguen por sexo](https://www.eldiario.es/sociedad/congreso-aprueba-cambios-lomloe-prohibe-ceder-suelo-escuela-concertada-subvencionar-centros-segreguen-sexo_1_6389325.html )  

Que se cite al tiempo la prohibición de subvencionar los centros que segregan por sexo es relevante: en eso LOMLOE sí deja clara la prohibición explícita en artículo 84.3 “en ningún caso” y es normativa básica por [disposición final quinta](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta), por lo que LOMLOE en eso sí tuvo voluntad política (aunque también hay voluntad política de controlar TC y CGPJ para que haya sentencias como STC 31/2018 acordes a ciertas voluntades políticas), pero que LOMLOE no pusiese en la ley la prohibición explícita como normativa básica de cesiones para #ConcertadosEnDiferido muestra falta la falta de voluntad real. 

Cuando se hagan realidad los nuevos casos, algunas personas pueden reflexionar si con mayor voluntad, tanta como tiene PP en mantenerlos, se podrían haber evitado.

**17 febrero 2023**  
[Unidas Podemos presenta una propuesta para evitar que la Comunidad de Madrid y el Ayuntamiento cedan suelo público a colegios privados y concertados - eldiario](https://www.eldiario.es/politica/ultima-hora-actualidad-politica-directo_6_9961185_1097637.html)  
> El Grupo Parlamentario Confederal de Unidas Podemos –En Comú Podem– Galicia en Común ha registrado una Proposición No de Ley (PNL) en el Congreso de los Diputados en la que pide al Gobierno abordar la manera de evitar que Ayuntamiento y Comunidad de Madrid cedan suelo público para la creación de centros educativos privados o concertados. Algo que, aseguran, están haciendo estas administraciones “de forma totalmente gratuita”.  
La formación se centra en Valdebebas, Vallecas y Vicálvaro donde, expresan, hay un valor total de suelo público de 29 millones y Ayuntamiento y Comunidad están llevando a cabo “maniobras jurídicas” para ceder “suelo público a empresas privadas” y crear colegios concertados. “El Ayuntamiento y la Comunidad de Madrid desoyen así las demandas de sus propios vecinos y vecinas, que llevan años reclamando más centros públicos en sus barrios, y que ven cómo impera la falta de inversión en infraestructuras públicas”, denuncia el grupo confederal, que añade que en Valdebebas solo hay dos colegios públicos “que se encuentran ya saturados”.  
Unidas Podemos –En Comú Podem– Galicia en Común pone el acento en que las dos administraciones madrileñas “burlan la ley”. Así, expresan que la LOMLOE establece que “la cesión de suelo de manera gratuita solo podrá hacerse si el centro educativo es público, y no para régimen de concierto ni privado”.  
Por eso, expresan, el Ayuntamiento “ha firmado un convenio por el que cede los terrenos a la Comunidad de Madrid”, que promueve la construcción de los centros, tratando de ampararse “en la Ley Maestra de Libertad Educativa”, aprobada en la Asamblea, que le confiere, poder para “convocar concursos públicos para la construcción y gestión de centros privados sostenidos con fondos públicos sobre suelo público dotacional”. “En definitiva, se escapa por completo del sentido ético y político que aúna todo el territorio en la defensa de la educación pública y de calidad”, denuncia el grupo.

**19 agosto 2024**

[Ayuso se abre a cobrar a los colegios concertados que engorden su negocio en parcelas públicas con cursos de etapas no obligatorias - elpais](https://elpais.com/espana/madrid/2024-08-19/ayuso-se-abre-a-cobrar-a-los-colegios-concertados-que-engorden-su-negocio-en-parcelas-publicas-con-cursos-de-etapas-no-obligatorias.html)  
La Administración recoge una tasa por uso en sus tres últimos concursos que se aplica cuando los centros amplían su oferta con enseñanzas en régimen privado   

Aquí se puede ver como lo que se hace con suelo público en teoría para centros sostenidos con fondos públicos realmente contemplan educación privada sin concierto.   

