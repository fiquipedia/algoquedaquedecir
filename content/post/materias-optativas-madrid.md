+++
author = "Enrique García"
title = "Materias optativas Madrid"
date = "2024-12-01"
tags = [
    "educación", "Madrid"
]
toc = true

+++

Revisado 1 diciembre 2024

## Resumen

Con el cambio de LOMCE a LOMLOE cambia la regulación de materias optativas. En Madrid se gestiona especialmente mal;  
- el primer curso 2022-2023 de manera transitoria Madrid permite impartir en cursos LOMLOE materias optativas con currículo LOMCE ya que no se había regulado nada. Por ejemplo Cultura Científica, que en LOMCE era de 2 sesiones semanales, se imparte con 4 sesiones semanales y con mismo currículo; se imparte curso LOMLOE con una materia con currículo LOMCE con estándares.   
- en curso 2023-2024 no se pueden impartir optativas en Bachillerato ya que la regulación no llega a tiempo; se publica después de 30 de enero cuando la normativa permite realizar propuestas para el curso siguiente hasta el 30 de enero.  
- en curso 2024-2025 se fijan unas optativas vía circular en mayo 2024, tarde para los plazos del proceso de admisión y oferta educativa. Esas optativas se proponen como orden en noviembre 2024, ya iniciado el curso.

## Detalle

**2 enero 2023**  
[twitter FiQuiPedia/status/1609853560346345473](https://x.com/FiQuiPedia/status/1609853560346345473) 
Trámite de audiencia Orden concreta el procedimiento para el ejercicio de la autonomía de los centros docentes que impartan ESO y Bachillerato en la C.M.  
https://comunidad.madrid/transparencia/proyecto-orden-vicepresidencia-consejeria-educacion-y-universidades-que-se-concreta-procedimiento  
Alegaciones de  02/01 hasta 23/01/2023  
Cita mecanismo para fijar currículo optativas  

**13 enero 2023**  
Presento alegaciones  
![](https://pbs.twimg.com/media/FmXST9GXwAUph3q?format=png)  

**19 enero 2023**  
Presento alegaciones adicionales  
![](https://pbs.twimg.com/media/Fm2ELzbXoAEKe36?format=png)  

**15 febrero 2023**  
Proyecto indicaba un plazo hasta 30 enero.  
En febrero "se publicará en breve"🤦‍♂️ y lo regulan vía CIRCULAR DG ED. SECUNDARIA, FP Y RÉGIMEN ESPECIAL SOBRE LA OFERTA DE OPTATIVAS PARA CURSO 2023-2024, EN LOS CENTROS DOCENTES QUE IMPARTAN ESO Y BACHILLERATO  
CSV 0963212513962592432431  

**27 febrero 2023**  
[ORDEN 457/2023, de 17 de febrero, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se concreta el procedimiento para el ejercicio de la autonomía de los centros docentes que impartan la Educación Secundaria Obligatoria y el Bachillerato en la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/02/27/BOCM-20230227-18.PDF)  
Orden 457/2023. No cambian art12.1 fija límite 30 enero para optativas ni disposición transitoria 2ª que amplía plazo solo para art11 horario= no se pueden presentar propuesta de optativas para 23-24, sin efecto las anteriores.
Solo las de circular

**31 marzo 2023**
Se publica proyecto  
[Orden 1736/2023 de la Vicepresidencia, Consejería de Educación y Universidades, por la que se establecen los catálogos de materias optativas que los centros podrán incorporar a su oferta educativa en la ESO y en el Bachillerato en la Comunidad de Madrid](https://www.comunidad.madrid/transparencia/orden-17362023-vicepresidencia-consejeria-educacion-y-universidades-que-se-establecen-catalogos)  
Plazo alegaciones de 03/04/2023 a 25/04/2023  



Para curso 2023-2024 al no recibir indicación de lo contrario se aprueban las optativas de proyecto en ESO de mi centro  

[Proyecto en cultura científica 4 ESO ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/materias/eso/PropuestaOptativa4ESOCulturaCient%C3%ADfica.pdf)  

[Proyecto en Investigación Científica e Innovación Tecnológica. Técnicas de laboratorio de Física y Química 4 ESO](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/materias/eso/PropuestaOptativa4ESOProyectoEnT%C3%A9cnicasDeLaboratorioDeF%C3%ADsicaQu%C3%ADmica.pdf)  

(En curso 2023-2024 se imparte un grupo de la optativa de laboratorio, pero no de cultura científica en 4 ESO)

**29 enero 2024**  
Versión definitiva presentada de optativa en 1º Bachillerato de Cultura Científica  

[Cultura Científica 1 Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/materias/bachillerato/PropuestaOptativa1BachilleratoCulturaCient%C3%ADfica.pdf)  

**22 mayo 2024**  
[Circular de la Dirección General de Educación Secundaria, Formación Profesional y régimen Especial sobre la oferta de materias optativas para el curso 2024-2025, en los centros docentes que impartan Educación Secundaria Obligatoria (ESO) y Bachillerato de 22 de mayo de 2024.](https://www.comunidad.madrid/sites/default/files/doc/educacion/2024-05-22_dgesfpre_circular_optativas_2024-25_44234284.pdf)  

No aparece la optativa de Cultura Científica  
 
 
**29 mayo 2024**   
Realizo solicitud vía transparencia

> Copia o enlace a la siguiente documentación sobre las propuestas de optativas asociadas a ORDEN
457/2023  
> 1. Propuestas presentadas según artículo 12 de ORDEN 457/2023. Si la documentación completa fuera
voluminosa, al menos la relación de propuestas de optativas con su nombre.  
> 2. Valoraciones de las propuestas realizadas según artículo 12.5 de ORDEN 457/2023  
> 3. Criterios para la inclusión de optativas en la CIRCULAR DE LA DIRECCIÓN GENERAL DE
EDUCACIÓN SECUNDARIA,FORMACIÓN PROFESIONAL Y RÉGIMEN ESPECIAL SOBRE LA
OFERTA DE MATERIAS OPTATIVAS PARA EL CURSO 2024-2025, EN LOS CENTROS DOCENTES
QUE IMPARTAN ESO Y BACHILLERATO fechado el 22 mayo 2024 previos al trámite de audiencia del
decreto asociado, incluyendo si procede la relación con la valoración de propuestas recibidas.
Toda la información solicitada es relevante porque los centros, para poder realizar el ejercicio de su
autonomía, necesitan conocerla para la futura presentación de propuestas que se realiza anualmente
según artículo 12.1 de ORDEN 457/2023

**19 junio 2024**  
Recibo [respuesta a solicitud](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Normativa/Madrid/optativas/2024-06-20-ResolucionDeAccesoOptativas.pdf)  
No indican el detalle pedido pero resumiendo:  
[twitter FiQuiPedia/status/1803838298042876044](https://x.com/FiQuiPedia/status/1803838298042876044)  
Se presentaron 241 propuestas optativas Madrid:  
18+19 ampliación lengua extranjera✓  
36 deporte✓  
31 anatomía✓  
7 ajedrez✗  
8 cultura✗>cultura inglés✓  
84+10+10 "contenidas en materias existentes"✗  
18 Proyecto✗  
No veo encaje a rechazo Cultura Científica  

**26 noviembre 2024**  
Envío la propuesta de optativa al equipo directivo:  
[Ciencia y Sociedad Crítica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/materias/bachillerato/PropuestaOptativa1BachilleratoCienciaySociedadCr%C3%ADtica.pdf)  

Intento blindar que no puedan rechazarla alegando que contenidos se solapan con otras materias   
[bsky.app fiquipedia.bsky.social/post/3lbumwwt3xs2i](https://bsky.app/profile/fiquipedia.bsky.social/post/3lbumwwt3xs2i)  
Propuesta Cultura Científica 1Bachillerato Madrid fue rechazada curso pasado, supuestamente porque "una materia optativa no puede recoger los contenidos que ya se imparten en otra materia", argumento que no impidió que existiese en 2022-2023 ni que sí exista en otras CCAA  
Así que he creado una nueva propuesta de optativa para 1Bachillerato: Ciencia y Sociedad Crítica (CSC)  
A ver qué alega Madrid esta vez para no aceptarla.   
Si la incorpora al catálogo la podría impartir cualquier centro como optativa.  
Comentarios bienvenidos  

Mismo día se publica trámite de audiencia  
[Proyecto de orden, de la Consejería de Educación, Ciencia y Universidades, que modifica la Orden 1736/2023, por la que se establecen los catálogos de materias optativas que los centros podrán incorporar a su oferta educativa en la ESO y Bachillerato C.M.](https://www.comunidad.madrid/transparencia/proyecto-orden-consejeria-educacion-ciencia-y-universidades-que-modifica-orden-17362023-que-se)  
Plazo para formular alegaciones (ambos incluidos): 
De 27/11/2024 hasta 18/12/2024

Mirando MAIN comento

>  En noviembre 2024 sacan, publicarán BOCM en 2025, un proyecto de orden que dicen es para "Ampliar los catálogos de materias optativas para enriquecer la oferta formativa que los
centros podrán configurar PARA EL CURSO 2024-2025", un curso ya iniciado en septiembre 2024 🤦‍♂️

**1 diciembre 2024**  
Presento alegaciones:  

---

En MAIN https://www.comunidad.madrid/transparencia/sites/default/files/02._main_proy._orden_modif._catalogo_optativas_0.pdf se indica "Objetivos que se persiguen: Ampliar los catálogos de materias optativas para enriquecer la oferta formativa que los centros podrán configurar para el curso 2024-2025."  
Ese objetivo no es correcto por varios motivos:  
1: La propuesta de orden no permite configurar curso 2024-2025 que se inició en septiembre 2024 y cuya oferta educativa los centros realizaron en el periodo de matrícula, en junio 2024.  
2: La propuesta no amplía los catálogos dado que reproduce los que ya se recogen como circular desde mayo 2024 https://www.comunidad.madrid/sites/default/files/doc/educacion/2024-05-22_dgesfpre_circular_optativas_2024-25_44234284.pdf La prueba es que la materia de Anatomía y Fisiología Humanas ya se imparte en curso 2024-2025.  

Sobre punto 1, es previsible que esta orden se publique en BOCM en el comienzo de 2025, y aplicará por primera vez a curso 2025-2026. Solamente aporta convertir en orden lo que hasta ahora era una circular.  

Sobre punto 2, el proyecto de orden no cita la circular mencionada cuando su contenido es esencialmente idéntico, y tampoco cita la posibilidad de ampliar catálogo para curso 2025-2026. Según el artículo 12 de ORDEN 457/2023 los centros pueden realizar propuestas de optativas hasta el 30 de enero de 2025 para el curso 2025-2026, la publicación de esta orden tal y como se ha redactado en el trámite de audiencia con la oferta actual de optativas para 2023-2024 podría implicar por plazos que no se tengan en cuenta dichas propuestas para 2025-2026, ya que haría falta modificar de nuevo la orden para incorporar las propuestas realizadas en enero 2025 que se aceptasen de modo que estuvieran publicadas antes de elaborar la oferta educativa de los centros antes de junio de 2025.   

La realidad es que si los centros pueden realizar propuestas para curso 2025-2026 hasta 30 enero 2025 pero no se van a tener en cuenta, los centros no tienen autonomía real en las optativas de Bachillerato.  
A esto se añade que si bien en ESO se permiten optativas de proyecto dejando libertad a cada centro para su configuración sin que exista un catálogo único, en el caso de Bachillerato solo se permiten las que la Consejería decida añadir a un catálogo único para todos los centros. Se está evitando que un centro ejerza su autonomía a pesar de que en MAIN se indica "atiende las demandas de los centros docentes en este ámbito", no tiene cargas administrativas, no tiene impacto presupuestario, no modifica carga lectiva, y se comentan las ventajas de ampliar las posibilidades formativas.  

En definitiva, la alegación es que la modificación de la orden por la que se establecen los catálogos de materias optativas que los centros podrán incorporar a su oferta educativa en la ESO y Bachillerato realizada en diciembre 2024 va a aplicar por primera vez a curso 2025-2026, y se deberían tener en cuenta las propuestas que los centros realicen en enero 2025 permitiendo que en Bachillerato exista autonomía real al no suponer cargas para la administración.  

---

Nº REGISTRO: 69/215471.9/24 

