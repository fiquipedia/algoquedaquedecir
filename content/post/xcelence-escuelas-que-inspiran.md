+++
author = "Enrique García"
title = "Xcelence escuelas que inspiran"
date = "2022-11-24"
tags = [
    "educación", "Madrid", "Privatización", "post migrado desde blogger", "comisiones de servicio", "Empieza por educar"
]
toc = true

description = "Bachillerato internacional"
thumbnail = "https://dgbilinguismoycalidad.educa.madrid.org/xeqi/img/logo_activo.png"
images = ["https://dgbilinguismoycalidad.educa.madrid.org/xeqi/img/logo_activo.png"]

# Imagen tomada de [dgbilinguismoycalidad.educa.madrid.org xeqi](https://dgbilinguismoycalidad.educa.madrid.org/xeqi/)

+++

Revisado 9 marzo 2024

## Resumen

En noviembre 2022 recibo un correo que  publicita el programa "Xcelence Escuelas que inspiran" e intento aportar información para ver lo que hay detrás.

Enlaza con Empieza por Educar (ExE) y privatización de FP, y también con comisiones de servicio y opacidad.

Posts relacionados:

* [Privatización de la FP](http://algoquedaquedecir.blogspot.com/2021/12/privatizacion-de-la-educacion-fp.html)
* [Comisiones de servicio en Madrid y opacidad](https://algoquedaquedecir.blogspot.com/2017/08/comisiones-de-servicio-en-madrid-y.html)

## Detalle

### Información general 

**18 noviembre 2022**

Recibo un correo de  Dirección General de Bilingüismo y Calidad de la Enseñanza <dgmce@educa.madrid.org> 

> Querido director/a:   
¡Hemos puesto en marcha la tercera edición del proyecto de orientación académico profesional Xcelence - Escuelas Que Inspiran!  
Ya son [100 centros educativos](https://www.espaciobertelsmann.es/eventos/una-mirada-internacional-a-la-orientacion/) los que se han sumado a esta nueva edición y debido al interés de los centros educativos, hemos abierto un nuevo grupo con 30 plazas disponibles que comenzarán la formación entre diciembre y enero.  
[Xcelence - Escuelas Que Inspiran](https://www.xeqi.es/madrid) es un proyecto impulsado por la Fundación Empieza Por Educar y Fundación Bertelsmann con el apoyo de la Consejería de Educación de Madrid. Acompañamos a centros de la Comunidad de Madrid en el diseño e implementación de una estrategia de Orientación Académico Profesional que les permita sistematizar todas aquellas acciones que se van a llevar a cabo en materia de orientación; con ese fin, ofrecemos la dotación de herramientas y recursos didácticos clave para hacer de ésta un elemento estratégico de centro. Todo ello con la misión de  mejorar la experiencia e información del alumnado de cara a su toma de decisiones.  
Nuestro programa incluye:  
* Un marco de referencia en orientación académica y profesional.  
* Una estrategia construida por toda la comunidad educativa.  
* Formación y acompañamiento personalizado durante todo el proceso.  
* Colaboración con el mundo profesional y centros de educación superior.  
* Trabajo en red entre centros educativos que participan en el proyecto.  
Los docentes que participen en este proyecto recibirán una acreditación de 4,8 créditos por formación y 2 créditos de especial dedicación. También la posibilidad de extender su comisión de servicio hasta 2 años.  
[ **¡Apúntate!** ](https://dgbilinguismoycalidad.educa.madrid.org/xeqi/index.php/login)

Los enlaces incluidos son tres

[Una mirada internacional a la orientación - espaciobertelsmann](https://www.espaciobertelsmann.es/eventos/una-mirada-internacional-a-la-orientacion/)  
[Centros de Madrid - xeqi](https://www.xeqi.es/madrid)  
[Inscripción https://dgbilinguismoycalidad.educa.madrid.org xeqi](https://dgbilinguismoycalidad.educa.madrid.org/xeqi/index.php/login) 

Luego miro más información: separado apartados para Empieza por educar y para fundación Bertelsmann, pongo ideas por orden cronológico

**abril 2019**

[Informe inicial del proyecto Xcelence. Adaptación de un modelo de orientación académico-profesional internacional para su aplicación en España - comunidad.madrid](https://www.comunidad.madrid/sites/default/files/doc/educacion/univ/informe_xcelence_.pdf)

**30 septiembre 2022**

[50 centros educativos participan en un innovador programa de orientación académica y profesional - comunidad.madrid](https://www.comunidad.madrid/noticias/2020/09/30/50-centros-educativos-participan-innovador-programa-orientacion-academica-profesional)

**1 octubre 2020**

[El proyecto Xcelence-Escuelas Que Inspiran llega a 100 escuelas para optimizar la orientación académico-profesional - fundacionbertelsmann.org](https://www.fundacionbertelsmann.org/el-proyecto-xcelence-escuelas-que-inspiran-llega-a-100-escuelas-para-optimizar-la-orientacion-academico-profesional/)  
> La Fundación Bertelsmann y Empieza Por Educar impulsan el proyecto piloto de Xcelence – Escuelas Que Inspiran en un centenar de colegios de Madrid y Cataluña.

**28 septiembre 2021**

[El proyecto Xcelence-Escuelas Que Inspiran se consolida en los centros educativos y 50 nuevos centros se sumarán al proyecto en Madrid - fundacionbertelsmann.org](https://www.fundacionbertelsmann.org/el-proyecto-xcelence-escuelas-que-inspiran-se-consolida-en-los-centros-educativos-y-50-nuevos-centros-se-sumaran-al-proyecto-en-madrid/)  
> La Fundación Bertelsmann y Empieza Por Educar, con la colaboración de la Consejería de Educación, Universidades, Ciencia y Portavocía de la Comunidad de Madrid y el apoyo de J.P. Morgan, han presentado las conclusiones del primer año del proyecto piloto Xcelence-Escuelas Que Inspiran en un evento semipresencial en el Espacio Bertelsmann.  
...  
Durante el curso 2020/2021, 45 centros (públicos y concertados) de Madrid y Cataluña han participado en el proyecto piloto Xcelence-Escuelas Que Inspiran, con el objetivo de fortalecer y mejorar sus sistemas de orientación en base al marco de calidad Xcelence y formando a la primera generación de un nuevo rol profesional: el Coordinador/a de Estrategias Académico-Profesionales (CEAPs).

**14 septiembre 2022**

[‘Escuelas que Inspiran’: motivación y orientación para la comunidad escolar de Madrid - telemadrid](https://www.telemadrid.es/programas/madrid-directo-om/Escuelas-que-Inspiran-motivacion-y-orientacion-para-la-comunidad-escolar-de-Madrid-9-2487441247--20220914083115.html)  
El programa promovido por el gobierno regional ayuda a los alumnos a conseguir sus objetivos en el sistema educativo

Esto apareció en BOCM en octubre, y la consejería paga a TeleMadrid por ese reportaje

**28 noviembre 2022**

[Convenio de 9 de septiembre de 2022, entre la Comunidad de Madrid y Radio Televisión Madrid, S. A. U., para impulsar una visión completa y actual de la educación madrileña en los medios de comunicación - bocm.es](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/10/28/BOCM-20221028-27.PDF)  
> 2. Programa “Madrid Directo”:  
...  
— Cómo conseguir tener a los alumnos motivados durante el curso con la profesora y pedagoga, Carmen Guaita. Entrevista con un centro del programa de la Comunidad de Madrid Escuelas que inspiran.  

**31 octubre 2022**

[La Comunidad de Madrid apuesta por una orientación de calidad basada en estándares internacionales - magisnet](https://www.magisnet.com/2022/10/la-comunidad-de-madrid-apuesta-por-una-orientacion-de-calidad-basada-en-estandares-internacionales/)  
El proyecto "Xcelence-Escuelas Que Inspiran" tiene como objetivo garantizar una guía académica integral y de calidad al alumnado.

**18 noviembre 2022**

[Orientar a los jóvenes, fundamental para los retos laborales del futuro - elespanol](https://www.elespanol.com/sociedad/educacion/20221118/orientar-jovenes-fundamental-retos-laborales-futuro/718678292_0.html)
La Fundación Bertelsmann y la Fundación Empieza por Educar inciden en el valor de un modelo de orientación internacional en el evento ‘Xcelence-Escuelas Que Inspiran, una mirada internacional a la orientación’.

**20 junio 2023**

[Educación refuerza la orientación profesional en ocho centros de Secundaria de Aragón](https://www.aragonhoy.es/educacion-cultura-y-deporte/educacion-refuerza-orientacion-profesional-ocho-centros-secundaria-aragon-92442)  
El Gobierno de Aragón, Fundación Bertelsmann, Empieza por Educar y Fundación Ibercaja colaboran en el programa Xcelence-Escuelas que Inspiran

### Convenio Madrid

El convenio indica Proyecto Xcelence-Escuelas Que Inspiran pero es con Fundación Bertelsmann y la Fundación Empieza por Educar

**20 diciembre 2022**  
[CONVENIO de colaboración de 2 de diciembre de 2022, entre la Comunidad de Madrid (Vicepresidencia, Consejería de Educación y Universidades), la Fundación Bertelsmann y la Fundación Empieza por Educar para la intensificación e implementación del desarrollo del programa “Proyecto Xcelence-Escuelas Que Inspiran”.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/12/20/BOCM-20221220-36.PDF)

> El proyecto que se recoge en el presente convenio se enmarca dentro de un proyecto más amplio denominado Xcelence-Escuelas Que Inspiran.
En la siguiente tabla se especifica el presupuesto del proyecto Xcelence-Escuelas Que Inspiran en la Comunidad de Madrid, que asciende a un total de un millón ciento trece mil ochocientas treinta y siete (1.113.837 euros), cuya implantación y funcionamiento ha sido prevista para el curso académico 2022/2023. Queda desglosado a continuación

**7 diciembre 2023**  
[CONVENIO de colaboración de 22 de noviembre de 2023, entre la Comunidad de Madrid (Consejería de Educación, Ciencia y Universidades), la Fundación Bertelsmann y la Fundación Empieza Por Educar, relativo al programa “Proyecto Xcelence-Escuelas Que Inspiran”.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/12/07/BOCM-20231207-29.PDF)  
> Financiación  
La celebración del presente convenio no tiene contenido económico ni supone gasto
adicional al previsto en los presupuestos de la Comunidad de Madrid, por lo que no derivan
de su suscripción gastos para la Administración Regional de ninguna clase. Las adhesiones
de nuevos centros al programa no supondrán, en ningún caso, coste adicional alguno

### Empieza por educar

Empieza por educar lo cito en post [Acceso docente: MIR](http://algoquedaquedecir.blogspot.com/2018/01/acceso-docente-mir.html)

Lo cito en 2018

[twitter FiQuiPedia/status/1023242758683852800](https://twitter.com/FiQuiPedia/status/1023242758683852800)  
25 julio 2018, Madrid, según calendario oficial curso 17-18 ha terminado y no ha empezado 18-19 en ningún centro sostenido con fondos públicos. Luego es centro privado: clases para septiembre o alumnos de atrezzo en clases no reales.

Comentando esto

[twitter sanz_ismael/status/1022217180124000257](https://twitter.com/sanz_ismael/status/1022217180124000257)  
Futuros Docentes impartiendo clase hoy en Madrid mientras un compañero les observa y les graba para ver posibles mejoras. Programa de la @EmpiezaxEducar

[twitter dePlaymobil/status/1023252150288375808](https://twitter.com/dePlaymobil/status/1023252150288375808)  
Son chavales de la concertada. Los cogen de conejillos de indias por cinco euros. Los presuntos docentes después del cursillo de verano van a ir también a aulas de la concertada (donde la mano de obra barata siempre es bienvenida) y al plan Refuerza, que ya está privatizado.

![](https://pbs.twimg.com/media/DjNSZajWwAE0geC?format=jpg)

El centro de la imagen es Colegio Salesianos San Miguel Arcángel
[twitter FiQuiPedia/status/1023264287597502464](https://twitter.com/FiQuiPedia/status/1023264287597502464)  
ese centro es privado concertado, luego el curso 18-19 de sus alumnos ha terminado ¿qué tipo de clase real se imparte? ¿qué  tipo de compañero observa? ¿Pagan 5€ por ir alumnos de privados no concertados que sí tienen septiembre o padres por 4 semanas de guardería?

Es algo que viene de lejos, se cita en este hilo

[twitter MarisaTra/status/1595119523916439553](https://twitter.com/MarisaTra/status/1595119523916439553)  
Una masiva movilización ha conseguido que se elimine la teleasistencia. Igual ocurrió en 2011 cuando la movilización de la marea verde consiguió paralizar la entrada de determinada fundación en los centros educativos.

**26 septiembre 2011**

[La secta neoliberal «Empieza por educar» YA se ha colado en los institutos madrileños](https://profesorgeohistoria.wordpress.com/2011/09/26/la-secta-neoliberal-empieza-por-educar-ya-se-ha-colado-en-los-institutos-madrilenos/)  

El texto es extenso pero lo reproduzco aquí íntegro:

---

Hola a todos/as!:

Reproduzco el correo que una compañera de profesión, llamada Diana, está difundiendo para que nos demos cuenta de lo que se avecina en la educación pública madrileña:

Ayer recibí un correo, con información extraída del foro de Educación del Sur, en el que se contaba lo siguiente (si os interesa, leedlo hasta el final, porque he podido conocer de primera mano la veracidad de la información y añado algún comentario):

1-Cuidado. Nueva forma de privatización cacique y de no contratar interinos. Esto ha pasado este año en el IES Villarejo.

(E mail en respuesta a la petición de información de Soy Pública sobre el tema de que fundaciones privadas den clase en la escuela pública)

Os cuento cómo fueron las cosas de verdad. Pero va a ser largo, así que sentaos.

La fundación (ellos se presentaron como ONG) «Empieza por educar» (www.empiezaporeducar.org) vino a la CCP en junio porque el año siguiente querían implantar como experiencia piloto en 10 institutos de Madrid su programa (y en otros 10 en Barcelona).

En principio no iban a venir a nuestro centro, pero la Directora provincial de Madrid Este, personalmente, les señaló el IES Villarejo, ya que ella es de Villarejo.

Así que vinieron.

Recibimos a las dos chicas responsables en la CCP y nos informaron de que:

1.- Es un programa destinado a centros donde hay dificultades con ciertos alumnos de cualquier tipo (públicos; según ellos, estos problemas no los hay en concertados ni privados) exclusivamente en las asignaturas de Lengua y Matemáticas.

2.- Están subvencionados por empresa privada aunque, como ONG, recibe también subvención pública. Pero mayoritariamente privada. La fundación la lleva la esposa de Botín y está adscrita al grupo Santander.

3.- Ofrecían personal de apoyo en las clases y a entera disposición de la dirección. La persona que viniera tendría horario completo (18-20h lectivas).

4.- Esa persona la eligen ellos y la pagan ellos. Se supone que los de Mate son de Matemáticas, ADE, arquitectos, ingenieros… y los de Lengua, filólogos y blablablá (lo tenéis en su web).

5.- Esta persona la eligen por excelencia en el currículum, tras entrevistas personales y, casi cito «porque son personas muy motivadas, muy preparadas y ansiosas de trabajar». Debe de ser que interpretan que nosotros no lo estamos.

6.- Si llegaran al centro, podrían estar dentro de la propia aula, dando clase por separado en grupos pequeños o en grupos de desdoble. Solos. Pero vamos, como la dirección quisiese.

7.- Incluso él podría evaluar, pero sería el profesor titular funcionario el que firmara las notas.

8.- Su filosofía, según su web, es que la educación está en crisis y que ya vienen ellos a solucionarlo. Insisten mucho en la idea «líderes del mañana» y que cualquiera puede serlo (ideología muy opusiana, como es la fundación y la esposa de Botín). La venden, eso sí, como mecanismos de ayuda y apoyo al profesorado y que así el alumnado estará mejor atendido.

La CCP, por supuesto, quedó desconcertada.

1.- Les dijimos que no sabíamos hasta qué punto era legal que personas que no han pasado los procesos selectivos para acceder a la función pública podían estar dando clases a alumnos en periodo lectivo.

2.- Les dijimos que si ocurriera algo, qué grado de responsabilidad tendrían. No contestaron. También dijimos que no poníamos en duda la formación e interés de sus trabajadores, pero que no eran menores los de los interinos y que además, éstos sí habían demostrado lo que sabían y no, no como los suyos. Y que su palabra no nos bastaba.

3.- Le dijimos a la jefa de estudios que si la dirección provincial permitía abrir desdobles con esta gente, por qué no pedíamos más interinos y personal funcionario y que nos abrieran esos desdobles. La jefa de estudios dijo que no mandarían más.

4.- Les dijimos, por supuesto, que esto no podíamos aprobarlo en CCP, que es un órgano de propuestas y consultivo, y que el poder de decisión están en el claustro.

5.- Enseguida dijeron que no hacía falta la aprobación del claustro y que la decisión debía ser inmediata porque si no, este programa lo ofrecerían a otros centros. También nos quedamos sorprendidos. Dijimos que los jefes de departamento lo llevaríamos a cada departamento y llevaríamos la votación de los miembros al jefe de estudios en un par de días.

6.- Terminó la CCP y, 20′ después, volvió a llamar la directora provincial en persona diciendo que no hacía falta la aprobación en claustro del programa. Que bastaba con la aprobación de los departamentos afectados: Lengua y Matemáticas.

7.- Los departamentos votaron en contra… y no por unanimidad.

8.- Obviamente, algunos profesores ya teníamos pensado que si algún departamento lo aprobaba, solicitaríamos un claustro extraordinario para tratar este tema.

9.- Igualmente, la comunicación de la Directora provincial fue estrictamente telefónica. Hasta donde sabemos, ninguna circular firmada y/o compulsada llegó al centro.

10.- En la siguiente CCP, en tono serio (pero jocoso), la profesora de Música dijo que ella tenía unos amigos de violín muy buenos y que había pensado que vinieran a dar clases a los chicos. Los chicos recibirían unas clases magníficas, además de que desdoblarían el grupo, luego mejor para el alumno, y que como esa decisión no debía tomarla ni el claustro ni la CCP, sino solo el departamento, pues que habían decidido que sí y que nos lo comunicaba nada más. Era broma, claro, pero refleja muy bien el sustrato de caciquismo de la situación.

11.- Todo esto quedó recogido en el acta de la CCP, así como las quejas de que había sido ninguneada por la dirección provincial, tanto como el claustro de profesores. Ya que este programa afecta al PEC del centro, o así lo pensamos, creemos que se debe aprobar en claustro. También quedó recogido el malestar por la presunta ilegalidad en que nos podía haber metido la dirección provincial.

12.- La conclusión de los profesores es que aunque se presenten como ONG o fundación, basta con asomarse a su web para observar que más parecen una empresa de trabajo temporal de élite que busca alumnos universitarios ya licenciados (con o sin Máster de Educación) que tengan dotes de dirección y sean capaces de hacer proyectos que formen los líderes del mañana (y que ellos mismos lo sean), sea en educación o en cualquier ámbito; y apestaba bastante, dicho sea de paso, a que quizá en el futuro quieran privtaizar las direcciones de los centros y nuestros directores terminen saliendo de lugares como éste.

Ahora ya podéis cerrar la boca del asombro, dejar de temblar y de tiraros de los pelos.

Espero que ningún instituto público apruebe esto, porque entonces sería para empezar a dar tortazos a dos manos a algunos de nuestros compañeros.

Recordad que aunque la dirección (porque sea afín) quiera el programa, el profesor puede negarse al no existir ningún convenio oficial con esta gente (hasta donde sabemos) y que el claustro puede rechazarlo, bastaría con proponer uno extraordinario recogiendo las firmas necesarias para discutirlo, y el director no puede tomar una decisión en contra del criterio del claustro ni del consejo escolar.
El director tiene sus competencias, pero también sus límites.

En nuestras manos está pararlo.

2-Contestación de otro forero (del Foro de Educación del Sur):

Me lo habían comentado pero me parecía tan surrealista y fuerte que no me lo podía creer. La persona que me lo contó (uno de los titulados entrevistados para trabajar como profesor para esta fundación) me describió lo mismo que has contado tú y hacía alusión a esos 1o institutos y a una supuesta experiencia piloto. En principio pensé que tenía que estar pensado para colegios concertados, pero ahora me parece que su descripción cuadra del todo con la tuya.

Según me contó, firman un contrato de dos años. En el primero de ellos, esta «fundación» les paga el máster de secundaria en el caso de que no lo tengan y en el segundo hacen una especie de máster de «dirección».

Si no llego a leer tu post, seguiría convencido de que se refería a centros concertados (a pesar de que me dijo claramente que era un instituto público, pero yo pensé que tal vez no lo tenía muy claro, pues acaba de aterrizar en el mundo de la educación). Debo de ser muy ingenuo porque me parecía imposible de creer dado que vulnera claramente la legalidad y los derechos tanto de los alumnos como de todos los docentes (funcionarios o interinos) que vemos cómo sistemáticamente se llevan a cabo recortes en personal y recursos públicos.

Es algo gravísimo y sólo el comienzo de lo que se avecina. O nos empezamos a mover TODOS o la muerte de la educación pública es cuestión de muy poquitos años.

Estoy flipando, de verdad.

Saludos.

3-En el IES Mariano José de Larra (Aluche) estos mismos han preparado una escuela de verano y después a partir de Septiembre ya los tienen en el Instituto. Yo me negué a colaborar y ser «cuerda de transmisión» de semejante despropósito.

Yo pertenezco a un IES de la zona del Larra y expuse en cuanto lo ofrecieron las verdaderas intenciones de esta gente pues me molesté en leer su página web. A nosotros nos lo impusieron, se comentó en reunión de tutores y palabras textuales de la jefa de estudios:»Nosotros somos los gestores y no os vamos a plantear si aceptais ese recurso o no».

Nada de claustro, nada de CCP. Caramelito de escuela de verano e imposición de personas elegidas a dedo recién licenciadas sin máster todavía para apoyos durante el curso escolar. Soy interina, tengo mis exámenes de oposición aprobados y no saqué plaza porque me faltaban méritos. No voy a apoyar esto ni voy a permitir que me atropellen así.

Fui la única que no entregué los papeles de inscripción a los alumnos/as. Yo no patrocino ni academias ni recursos de empresas privadas. Todos mis compañeros/as aunque protestando terminaron entrando en el juego.

Soy de nuevo la que os envía el correo (Diana). He llamado hace un ratito al IES MJ de Larra para corroborar la información que pone en este correo. Sabéis que no me gusta reenviar información que no sea de interés y esté «certificada».

Pues bien, la información que me ha sido dada corrobora el hecho de que la fundación «Empieza por educar» está haciendo un curso de verano en el IES y que, en efecto, se quedarán durante el curso «para echar una mano». El Jefe de Estudios me dice que eso no va a quitar trabajo a los interinos, ya que sólo van a sacar a «alumnos difíciles» de las aulas, uno o dos, y que ni van a firmar actas ni nada.

Yo me pregunto: ¿este trabajo no ha estado siendo realizado por personas del equipo de Orientación? Y ahora lo van a hacer personas con una licenciatura recién acabada. Me han confirmado que durante este primer año, a estos «profesores» les van a pagar el Máster. Y se quedan un segundo año, ya que se trata de un programa a dos años «para después evaluar cómo funciona».

Además, esta persona con la que he hablado me ha dicho que le habían llamado los sindicatos también para que se lo explicaran. Parecía enfadado con ellos, así como con las personas que están enviando esta información, porque dice que es falsa. Lo que no me queda claro es qué es lo que es falso.

Le he agradecido su atención y, la verdad, ahora estoy mucho más preocupada que ayer cuando recibí este correo (gracias, Rafa).

Os pido que hagáis conocer esta información entre los profesores. Nos esperan unos años duros. Y quien piense que por ser ya funcionario/a esto no le afecta, que recuerde el poema de Bertold Brecht: «Primero vinieron a por los judíos, como yo no era judío nada me importó…»</i>

---

**28 octubre 2011**

[La marea verde evita fichar a docentes de una entidad privada - publico](https://www.publico.es/espana/marea-verde-evita-fichar-docentes.html)  
Figar intentó colocar profesores de una fundación

[twitter FiQuiPedia/status/1595129934510690304](https://twitter.com/FiQuiPedia/status/1595129934510690304)  
Buscador de convenios y Empieza Por Educar

[Buscador de convenios](https://gestiona.comunidad.madrid/rcnv_app/secure/buscador/buscador.jsf)  

![](https://pbs.twimg.com/media/FiMKOMBX0AADl5O?format=jpg)

### Funcación Bertelsmann

Sale en artículos

[twitter FiQuiPedia/status/1595131618108518401](https://twitter.com/FiQuiPedia/status/1595131618108518401)  
Mismo buscador, Bertelsmann  
![](https://pbs.twimg.com/media/FiMLwJPXwAcG1S9?format=jpg) 

### Ejemplos de centros

[PROGRAMACIÓN GENERAL ANUAL 2021-22 IES LA SERNA.](http://www.ieslaserna.com/2021-22/pdf/programacion-general-anual.pdf)  
> Puesta en marcha del proyecto Xcelence-Escuelas que Inspiran

[ PROYECTO XCELENCE-ESCUELAS QUE INSPIRAN - IES Dámaso Alonso](https://www.educa2.madrid.org/web/centro.ies.damasoalonso.madrid/proyecto-xcelence-escuelas-que-inspira)

[Xcelence - Escuelas que inspiran - IES Isabel La Católica](https://www.educa2.madrid.org/web/centro.ies.isabellacatolica.boadilla/xcelence)

[Proyecto Xcelence - IES Gran Capitán](https://www.educa2.madrid.org/web/centro.ies.grancapitan.madrid/proyecto-xcelence)

[Proyecto Xcelence - IES José Saramago](https://www.educa2.madrid.org/web/centro.ies.josesaramago.majadahonda/xcelence)

[Proyecto de Orientación Académico-Profesional XCELENCE - IES Adolfo Suárez](https://site.educa.madrid.org/ies.adolfosuarez.paracuellos/index.php/2022/03/23/proyecto-de-orientacion-academico-profesional-xcelence/) 

[Xcelence - IES San Isidro](https://www.educa2.madrid.org/web/centro.ies.sanisidoro.madrid/el-centro1/-/visor/xcelence)

[Xcelence - IES Julio Verne](https://site.educa.madrid.org/ies.julioverne.leganes/index.php/category/proyectos/xcelence/)

[PROYECTO XCELENCE EN EL SIGLO XXI - IES Siglo XXI](https://site.educa.madrid.org/ies.sigloveintiuno.leganes/index.php/proyectos-2/)

La primera sesión se ha llevado a cabo el viernes, 17 de diciembre, y os compartimos las conclusiones de los distintos perfiles participantes.

[INFORMACIÓN A FAMILIAS PROYECTO XCELENCE](https://drive.google.com/file/d/1nynkqkQZNAMTyhOYoywW6am8aPeC8jTN/view?usp=sharing)

PRIMERA SESIÓN 17 DE DICIEMBRE 2021

[CONCLUSIONES PROFESORADO](https://drive.google.com/file/d/1Y6z7b5ZyL60IfA02UvPay6ybh1rgQG0w/view?usp=sharing)

[CONCLUSIONES ALUMNADO](https://drive.google.com/file/d/1hi6yc1CJcjwROwj4cb4kOBo4_WTCYqCw/view?usp=sharing)

### Comisiones de servicio

**18 noviembre 2022** 

[twitter FiQuiPedia/status/1593946303125405697](https://twitter.com/FiQuiPedia/status/1593946303125405697)  
Es esto: xcelence, escuelas que inspiran
[dgbilinguismoycalidad.educa.madrid.org xeqi](http://dgbilinguismoycalidad.educa.madrid.org/xeqi/index.php)  

[twitter Isabel_Gonzal/status/1593645938471571459](https://twitter.com/Isabel_Gonzal/status/1593645938471571459)  
Estamos de rebajas. Debe ser el Black Friday. Las comisiones de servicio opacas son un gran incentivo  
![](https://pbs.twimg.com/media/Fh3EiJiXoAIgHbZ?format=jpg)  
Imagen indica "... posibilidad de extener su comisión de servicio hasta 2 años"  


La comisiones de servicio deben tener convocatoria por sentencia TS, pero no aparecen aquí    
[comunidad.madrid educacion docentes #ccss](https://www.comunidad.madrid/servicios/educacion/docentes#ccss)  

### Transparencia comisiones xcelence

Pongo solicitud transparencia

---

- Solicito copia o enlace a las convocatorias de comisiones de servicio existentes, que reflejen las vacantes, asociadas al programa "Xcelence Escuelas que inspiran" que según correo enviado por  Dirección General de Bilingüismo y Calidad de la Enseñanza dgmce@educa.madrid.org el 18 noviembre 2022 indica "Los docentes que participen en este proyecto ... la posibilidad de extender su comisión de servicio hasta 2 años."  
- Solicito copia o enlace a la convocatoria en caso de que por dicho proyecto se puedan crear nuevas comisiones, ya que no aparecen en https://www.comunidad.madrid/servicios/educacion/docentes#ccss

---

59/398893.9/22

No lo cito, pero viendo información se ve que se hizo piloto en 2020-2021, luego en 2021-2022 habría nuevas comisiones, y Madrid sabía que por sentencia TS era obligatoria convocatoria (ya sacó en 2021 las de programas en centros)

**20 diciembre 2022**

Respuesta

[twitter FiQuiPedia/status/1605182357870370818](https://twitter.com/FiQuiPedia/status/1605182357870370818)  
![](https://pbs.twimg.com/media/FkbAtmdXEAADzB5?format=jpg)  
[2022-12-20 Resolución Convocatorias Comisiones Xcelence](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2022-12-20-ResolucionConvocatoriasComisionesXcelence.pdf) 
Dice @educacmadrid  

>"...no existen las convocatorias de comisiones de servicio a las que se hace referencia en su solicitud..."

Sí existe un nombre para incumplir deliberadamente una sentencia TS

Planteo solicitud para ir a fiscalía: confirmación de que hay comisiones de ese tipo

---

Copia o enlace a la documentación que indice el número de comisiones de servicio asociadas al programa Xcelence - escuelas que inspiran, para cada uno de los cursos 2020-2021, 2021-2022 y 2022-2023.

Dicho programa se inició en centros de Madrid en 2020 según https://www.comunidad.madrid/noticias/2020/09/30/50-centros-educativos-participan-innovador-programa-orientacion-academica-profesional

---

59/672879.9/22

**20 enero 2023**

Me notifican ampliación de plazo

**16 febrero 2023**

Recibo resolución

[2023-02-16 Resolución Convocatoria Comisiones Xcelence](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2023-02-16-ResolucionConvocatoriaComisionesXcelence.pdf)  

Enlazan tres documentos de mayo 2022, asociados a programas en centros en Sur, Capital y Este:

[ANEXO I – EXCELENCE CONVOCATORIA COBERTURA EN COMISIÓN DE SERVICIOS DE PLAZAS VACANTES EN PROGRAMAS EDUCATIVOS EN LA DAT MADRID-SUR (CURSO 2022/2023)](https://www.educa2.madrid.org/web/educamadrid/principal/files/006d035e-6ed9-4fc4-9b20-44df185e4074/ANEXO%20I%20-%20EXCELENCE%20%20-%20.pdf?t=1652864258937)

[ANEXO I CONVOCATORIA COBERTURA EN COMISIÓN DE SERVICIOS DE PLAZAS VACANTES EN PROGRAMAS EDUCATIVOS EN LA DAT MADRID-CAPITAL](https://www.educa2.madrid.org/web/educamadrid/principal/files/e34d4ce4-8b3f-43cf-8d2e-025e7ad923d4/PROYECTOS%20DE%20INNOVACI%C3%93N%20%28STEMADRID%2C%20EXCELENCE%2C%20COMPDIGEDU%2C%20PIE.%29.pdf?t=1652809484729)  
Indica de manera general "ROYECTOS DE INNOVACIÓN (STEMADRID, EXCELENCE, COMPDIGEDU, PIE…)"

[Este (enlace no operativo en 2024)](https://www.educa2.madrid.org/web/educamadrid/principal/files/b19a3d80-6837-458f-a456-d2bd5d2b71a7/DAT%20Este/Comision%20de%20Servicios%202022/Anexo%20I_PROYECTOS%20DE%20INNOVACI%C3%93N%20%28STEMADRID%2C%20EXCELENCE%2C%20COMPDIGEDU%2C%20PIE%2C%20%E2%80%A6%29.pdf?t=1652859617894)

**8 mayo 2023**

Se realiza convocatoria comisiones 2023-2024 y se desglosan las de Xcelence


