+++
author = "Enrique García"
title = "Primer post del blog: presentación"
date = "2017-08-22"
tags = [
    "presentación", "post migrado desde blogger"
]
toc = false

+++

Revisado 21 enero 2024

Durante un tiempo me han planteado y me he planteado escribir un blog: desde 2013 uso Twitter ([@FiQuiPedia](https://twitter.com/fiquipedia)) porque es rápido, pero se queda corto para contar algunas cosas.

Escribir posts supone una disciplina y un tiempo que no estoy seguro de tener, aunque desde 2011 participo en el foro  [www.docentesconeducacion.es](http://www.docentesconeducacion.es/) como usuario hunk y ahí he reflejado muchas ideas, con varios posts al día.

Creo un blog supone un lugar donde poder escribir que permita ir editando y complementando entradas, que en twitter y en un foro no es posible.

También tengo la página web [www.fiquipedia.es](http://www.fiquipedia.es/) pero ahí me centro en recursos de docencia y Física y Química, y es difícil opinar o poner información.

Mis intereses son variopintos, será sobre todo de educación, legislación, política, cosas de Física y Química, ciencia, escepticismo, tecnología, cosas frikis, ... ya se irá viendo.

El nombre del blog enlaza con una frase de mi bio en Twitter que repito mucho:

"algo es infinitamente superior a cero"

Si tienes 100 y pasas a 101, has aumentado un 1%  
Si tienes 1 y pasas a 2, has aumentado la misma cantidad, pero es un 100%  
Si tienes 0 y pasas a 1, has aumentado la misma cantidad, pero es un infinito%  

Elegiré un tema y diré algo y seguro que "algo queda que decir", a otros y a mi.  
El nombre del blog intenta reflejar que lo que digo sé que es incompleto, siempre se puede añadir algo revisando, ampliando con comentarios, ...  La primera palabra de cada post siempre es la misma: "Revisado", y de hecho este mismo post de presentación (e información general) lo iré revisando.

En los posts intentaré poner fecha de revisión (en lugar de posts nuevos sobre un tema intentaré revisarlos), y poner muchos enlaces a referencias (datos, fuentes de información). Necesariamente expresaré opiniones, pero siempre intentaré que sean argumentadas.  
Suelo escribir parrafadas tremendas y detallar mucho, así que saldrán posts tremendos: intentaré hacer el ejercicio de poner inicialmente una versión resumida ([TL;DR](https://en.wikipedia.org/wiki/TL;DR), [BLUF](https://en.wikipedia.org/wiki/BLUF_(communication))... ) que no supere unas pocas líneas. (Inicialmente ponía "versión TL;DR" pero me dicen varias personas que parece que asusta como primera línea del post y desanima a leer el resto, así que pondré simplemente "resumen", aunque veo ejemplos de uso real)

Sobre licenciamiento, intentaré que enlazar la fuente si cito algo breve (por ejemplo prensa), y en general el comentario es el mismo que el de FiQuiPedia [licenciamiento FiQuiPedia](http://www.fiquipedia.es/licenciamiento)  
Ante cualquier indicación para retirar algo por licenciamiento, manifiesto mi disposición a retirarlo si se me argumenta.

No tengo publicidad ni pretendo ponerla, aunque dependo de lo que haga la plataforma Blogger si decidiese añadirla.

Al igual que intento tener cuidado sobre licenciamiento, lo intento poner sobre protección de datos. Uso la ley de transparencia e intento compartir información sobre temas en los que muchas veces aparecen datos. En 2018 surge nueva ley orgánica (ahora se usan siglas "LOPDGDD")
[Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales](https://www.boe.es/buscar/act.php?id=BOE-A-2018-16673) que básicamente se trata de una nueva ley que sustituye a la anterior "LOPD" ([Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal](https://www.boe.es/buscar/act.php?id=BOE-A-1999-23750) y a [Real Decreto-ley 5/2018, de 27 de julio, de medidas urgentes para la adaptación del Derecho español a la normativa de la Unión Europea en materia de protección de datos](https://www.boe.es/buscar/act.php?id=BOE-A-2018-10751) y todo ello para adaptarse a [Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos y por el que se deroga la Directiva 95/46/CE (Reglamento general de protección de datos)](https://www.boe.es/buscar/doc.php?id=DOUE-L-2016-80807).  
Ante cualquier indicación para retirar algo por protección de datos, manifiesto mi disposición a retirarlo si se me argumenta.  
(Nota: haber puesto esto puede parecer exagerado, pero era premonitorio, en enero 2020 la Comunidad de Madrid me notifica por escrito que lee el blog y me indica que incumplo normativa de protección de datos, y me pide borrar información: firmas manuscritas)

En FiQuiPedia puedes encontrar mi nombre y mail de contacto; uso alias hunk pero no me escondo tras el anonimato.

No pretendo tener razón, pretendo informar e informarme comentando cosas de manera argumentada, y siempre son bienvenidos comentarios argumentados, que pueden hacer replantearme cosas.

He visto a gente compartir solamente la parte de esta imagen que en la versión que pongo aquí está tachada "Sólo porque tú tengas razón, no significa que yo esté equivocado", pero sin tachar, y creo que define el hecho de no querer informarse sino tener razón.
(Citaba un tuit original en 2017, pero deja de estar disponible en 2019)
https://twitter.com/ChiiRosenrot/status/876897899690381315
También tiene un error: elegir "un seis o un nueve", igual la realidad no es exactamente lo que dice ninguno de los dos.
Ejemplos que no son ni seis ni nueve:
Ꝯ https://unicode-table.com/es/A76E/
Ⳋ https://unicode-table.com/es/2CCA/
୧ https://unicode-table.com/es/0B67/
![](https://2.bp.blogspot.com/-sWfKAVDcwIQ/XFdt6-xciAI/AAAAAAAAPcQ/Sg8l2Zcki8s6XT9vqohYxuQJeFrlgCadwCLcBGAs/s400/TenerRazon.png)  

Otra idea similar es esta  
Me gusta también porque se suele compartir la primera parte, pero no la segunda, donde se muestra que "algo queda que decir", ni la tercera
[This thing about truth.. is so true!](https://aswaniv.wordpress.com/2018/11/02/this-picture-about-truth-is-so-true/)  
![](https://1.bp.blogspot.com/-1FdrgtAXqcc/Xsqfkdu5yjI/AAAAAAAATnE/hHPaNJDnP_MDMGSx63RZ3suZHf1arorfACLcBGAsYHQ/s1600/TrueTruh3.jpeg)  

Me parece que debo citar este post de [@javierfpanadero](https://twitter.com/javierfpanadero)  
[Perdóneme pero… yo no respeto opiniones](https://lacienciaparatodos.wordpress.com/2009/10/03/perdoneme-pero-yo-no-respeto-opiniones/)  
Misma idea en  
[5 razones de Savater por las que tu opinión no importa](http://pasajero.utero.pe/2015/12/02/5-razones-de-savater-por-las-que-tu-opinion-no-importa/)  
"Lo que debe ser respetado en todo caso son las personas (y sus derechos civiles), no sus opiniones ni su fe." Política para Amador, Fernando Savater.


Permito comentarios en el blog de manera anónima, pero siempre con respeto, y vuelvo a citar a [@javierfpanadero](https://twitter.com/javierfpanadero) 
[Sobre los comentarios](https://lacienciaparatodos.wordpress.com/2011/09/27/sobre-los-comentarios/)  

Cito esta canción de Franco Battiato  
> "Busco un centro de gravedad permanente  
> que no varíe lo que ahora pienso  
> de las cosas, de la gente, ..."  

y pienso que es triste no querer variar lo que uno piensa. Yo busco que no varíe el querer informarme y atreverme a variar lo que pienso sobre cualquier cosa. La misma idea se puede plantear desde el humor [Un hombre alcanza la edad en la que ya nunca volverá a cambiar de opinión - elmundotoday](https://www.elmundotoday.com/2023/05/un-hombre-alcanza-la-edad-en-la-que-ya-nunca-volvera-a-cambiar-de-opinion/)  

También una canción de Robbie Williams que modifico; tengo muchas ideas dando vueltas en la cabeza que me gustaría escribir. A veces escribir es una manera de descargar la cabeza.  

> "Cause I got too much thoughts  
> Running through my brain
> Going to waste"

En este post de presentación voy añadiendo citas según me parezcan interesantes:

En abril 2018 escribo el post ["¡Compartid, malditos!"](http://algoquedaquedecir.blogspot.com/2018/04/compartid-malditos.html), es retuiteado por Francisco Villatoro, y cito este post suyo que creo que refleja ideas de por qué escribir. Aplica a divulgación que él hace, pero lo veo también para los materiales de fiquipedia y para escribir en este blog: informo para informarme y aprender.  
[A veces me pregunto si vale la pena…](http://francis.naukas.com/2017/11/15/a-veces-me-pregunto-si-vale/)  
> ¿Me importa si me leen muchos o me leen pocos? Todos tenemos nuestro ego, pero la verdad es que no es algo relevante para mí. No gano dinero con el blog, ni lo pretendo. ¿Por qué divulgo sobre las cosas que ignoro? Porque enseñar es la mejor manera de aprender. ¿Por qué no divulgo solo sobre los temas que domino? Porque haciéndolo aprendo muy poco. ¿Qué pasa si meto la pata al divulgar sobre un tema que no domino? No pasa nada, así aprendo más. ¿Pero hay lectores que creen que eres experto en todo lo que divulgas? Lo siento, pero espero que sean pocos. No quiero engañar a nadie. No divulgo para enseñar. Divulgo para aprender. 

En junio 2018 escucho esta frase que no conocía hasta ese momento, de Antonio Machado. Un compañero me dice que la frase define FiQuiPedia, pero la reflejo aquí tras buscar fuente original  
[Antonio Machado. Sobre la defensa y la difusión de la cultura. Discurso pronunciado en Valencia en la sesión de clausura del Congreso Internacional de Escritores](http://www.filosofia.org/hem/193/hde/hde08011.htm)  
> Para nosotros, la cultura ni proviene de energía que se degrada al propagarse, ni es caudal que se aminore al repartirse; su defensa, obra será de actividad generosa que lleva implícitas las dos más hondas paradojas de la ética: sólo se pierde lo que se guarda, sólo se gana lo que se da. \[18]  
 
En enero 2019 conozco esta cita de Edmund Burke que me encanta y decido incluir  
[Top 10 Edmund Burke Quotes - brainyquote](https://www.brainyquote.com/lists/authors/top_10_edmund_burke_quotes)  
> Nobody made a greater mistake than he who did nothing because he could do only a little.  

> Nadie comete un  error más grande que aquel que no hace nada porque sólo podría hacer un poco.  

Con el post intento hacer "algo", es poco y algo queda que decir, pero creo que merece la pena.

En 2019 descubro esta cita de Alejandra Pizarnik  

> "Hay que luchar todos los días, como Sísifo. Esto es lo que no comprendo. Que la vida contiene días, muchos días, y nada se conquista definitivamente. Por todo hay que luchar siempre y siempre. Hasta por lo que ya tenemos y creemos seguro. No hay treguas. No hay paz."

En marzo 2020 leo estas dos citas y las añado, me encanta el resumen.  
Con este blog intento hacer algo: como ya he puesto varias veces en redes #CompartidMalditos #ReclamadMalditos
[Double Quote: The Man or The Uniform](https://medium.com/the-agoge/double-quote-the-man-or-the-uniform-ec22127df930)  
Double Quote: The Man or The Uniform  
1. Brett McKay on John Boyd  
2.  Richard Feynman recounts that at a very early age his father taught him the difference between the uniform and the man  
...  
Synthesis: Never look to the uniform to find the man. The man makes the uniform, not the other way around. A uniform confers no knowledge, no excellence, it is the person underneath that adds value to the trappings. Honors or uniforms — they are a costume designed to broadcast a message, regardless of whether that message is true or not. A man’s actions, his contributions define him. This is an exhortation to never forget that who you are doesn’t matter, only what you do matters. So chase the beauty of accomplishment, do something rather than try to be something, and never evaluate a man by his epaulets.  
