+++
author = "Enrique García"
title = "Transparencia algoritmos"
date = "2021-03-24"
tags = [
    "educación", "Madrid", "transparencia", "algoritmos", "Post migrado desde blogger"
]
toc = true
description = "Transparencia algoritmos"
thumbnail = "https://pbs.twimg.com/media/D-da0DpWwAE5et4?format=jpg"
images = ["https://pbs.twimg.com/media/D-da0DpWwAE5et4?format=jpg"]


+++

Revisado 1 marzo 2025

## Resumen

La transparencia en la administración supone permitir supervisar el cumplimiento de normativa, y ello implica poder supervisar los algoritmos que implementan decisiones de la administración. El uso de código abierto también enlaza con el uso de software libre en la administración: que se use software libre realizado por otros y que lo que hace la administración sea software libre.

Post relacionado:

* [Oposiciones: tribunales](https://algoquedaquedecir.blogspot.com/2018/03/oposiciones-tribunales.html)

## Detalle

### Plantamiento inicial: BOSCO y Civio

El tema lo conocía a través de CIVIO

[Etiqueta transparencia algorítmica - civio](https://civio.es/tag/transparencia-algoritmica/)  

**2 julio 2019**

[twitter civio/status/1145984819375083525](https://twitter.com/civio/status/1145984819375083525)  
Queremos que el código fuente y los algoritmos que hacen las #AAPP y que nos regulan dejen de ser secretos. Por eso, con ayuda de @jdelacueva, vamos a los tribunales para exigir el código de la aplicación del #bonosocial del Gobierno:  
["Que se nos regule mediante código fuente o algoritmos secretos es algo que jamás debe permitirse en un Estado social, democrático y de Derecho"](https://civio.es/novedades/2019/07/02/que-se-nos-regule-mediante-codigo-fuente-o-algoritmos-secretos-es-algo-que-jamas-debe-permitirse-en-un-estado-social-democratico-y-de-derecho/)  
El día 20 de junio presentamos nuestro recurso contencioso-administrativo ante la negativa del Consejo de Transparencia de obligar a hacer público el código de un programa que decide quién resulta beneficiario del bono social eléctrico.

[¿Regulados por programas y algoritmos que permanecen en secreto? Cuéntanos tu caso - comunidad.civio.es](https://comunidad.civio.es/t/regulados-por-programas-y-algoritmos-que-permanecen-en-secreto-cuentanos-tu-caso/)  

Noticia asociada en febrero 2022  

[La Justicia impide la apertura del código fuente de la aplicación que concede el bono social](https://civio.es/novedades/2022/02/10/la-justicia-impide-la-apertura-del-codigo-fuente-de-la-aplicacion-que-concede-el-bono-social/)  

En febrero 2022 se comparte toda la documentación del proceso

[Que las ayudas públicas lleguen a quienes más las necesitan - civio.es](https://civio.es/acceso-a-bono-social/)  

Por ejemplo [Recurso de apelación de Civio](https://civio.box.com/s/85l4had075ntq3o7tymfx8fq0ijcxgmf) de fecha 3 febrero 2022

En página 24 de 25 se cita [RT0253/2021 código fuente aplicación informática sorteo de tribunales procesos selectivos](https://www.consejodetransparencia.es/ct_Home/dam/jcr:ddb7deb0-76fd-46cd-bf14-3b14f3f3dcc7/RT_0253_2021.pdf)   

**1 marzo 2022**

Bono social: Por qué es necesario auditar los sistemas informáticos que deciden nuestras vidas - Fundación Civio  
[![](https://img.youtube.com/vi/4D5zqNS7Ja4/0.jpg)](https://www.youtube.com/watch?v=4D5zqNS7Ja4 "Bono social: Por qué es necesario auditar los sistemas informáticos que deciden nuestras vidas - Fundación Civio")  

> Dentro de las actividades por el 10º aniversario de Civio, te invitamos a un encuentro online sobre la opacidad en los programas informáticos, que cada vez más, determinan derechos y deberes. Con el abogado y patrono de Civio, Javier de la Cueva y nuestros codirectores, David Cabo y Eva Belmonte hablaremos del litigio que tenemos abierto para conocer el código fuente de BOSCO, la aplicación que determina quién recibe el bono social. Los tribunales nos han denegado en primera instancia el acceso a dicho código, y condenado a pagar las costas. Por supuesto, hemos recurrido.

**22 abril 2022**

[Un algoritmo opaco al que han detectado errores decide quién recibe ayudas públicas del bono social](https://elpais.com/tecnologia/2022-04-22/un-algoritmo-opaco-al-que-han-detectado-errores-decide-quien-recibe-ayudas-publicas-del-bono-social.html)  
El Gobierno rechaza abrir el código fuente del sistema que asigna descuentos en la factura de la luz a familias vulnerables, lo que contribuiría a investigar más fallos

**24 junio 2024**  
[Primer paso para llevar al Supremo la sentencia que rechaza abrir el código fuente de BOSCO - civio](https://civio.es/novedades/2024/06/24/primer-paso-para-llevar-al-supremo-el-caso-bosco/)  


#### Junio 2022 proyecto gestión, concesión y pago del Bono Social Térmico en la Comunidad de Madrid.

[Proyecto de Orden de la Consejera de Familia, Juventud y Política Social por la que se regula el procedimiento para la gestión, concesión y pago del Bono Social Térmico en la Comunidad de Madrid.](https://www.comunidad.madrid/transparencia/proyecto-orden-consejera-familia-juventud-y-politica-social-que-se-regula-procedimiento-gestion)

5 junio realizo alegación

>1. COSTE  
En MAIN se indica  que "Implica un gasto" y que "Afecta a los Presupuestos de otras Administraciones Territoriales." pero en impacto presupuestario no se menciona ningún gasto propio.  
En proyecto se indica  
"4. Los ficheros facilitados por los comercializadores de referencia serán automatizados en la aplicación informática diseñada al efecto, "  
"Los datos proporcionados por los interesados, tras el requerimiento de documentación, serán incorporados en la aplicación informática del bono social térmico con el fin de proceder al pago de la ayuda."  
No se aclara si "diseñada al efecto" supone o no un desarrollo (que supondría coste) o es utilizar una aplicación existente.  
> 2. ALGORITMO  
El título del proyecto incluye la palabra "concesión", luego sugiere que la Comunidad de Madrid realiza la concesión, aplicando unos criterios. La aplicación de concesión debe implementar mediante un algoritmo las reglas que fija normativa, y  la Comunidad como administración debería velar por la transparencia haciendo público el código  
[RT 0748/2021](https://www.consejodetransparencia.es/ct_Home/dam/jcr:971c8cfe-0ef5-4cdf-afdd-191e9c8733b4/RT_0748_2021.pdf)  
En Fundamento Jurídico 5  
"5. Mientras no se instauren otros mecanismos que permitan alcanzar los fines señalados con garantías equivalentes –como podrían ser, por ejemplo, auditorías independientes u órganos de supervisión–, el único recurso eficaz a tales efectos es el acceso al algoritmo propiamente dicho, a su código, para su fiscalización, tanto por quienes se puedan sentir perjudicados por sus resultados como por la ciudadanía en general, en aras de la observancia de principios éticos y de justicia."

48/160089.9/22


**15 septiembre 2022**

[Carta al Gobierno sobre la futura supervisión de algoritmos e inteligencias artificiales](https://civio.es/novedades/2022/09/15/carta-al-gobierno-sobre-la-futura-supervision-de-algoritmos-e-ias/)  
Más de 50 organizaciones pedimos la participación de la sociedad civil en el desarrollo de políticas relacionadas con las IAs para garantizar el respeto a los derechos, la transparencia y la inclusividad.

### TVR (Tabla de Variables de Riesgo) y Civio

**26 febrero 2025**  
[Las prisiones españolas usan un algoritmo sin actualizar desde 1993 para decidir sobre permisos de salida - civio](https://civio.es/justicia/2025/02/26/las-prisiones-espanolas-usan-un-algoritmo-sin-actualizar-desde-1993-para-decidir-sobre-permisos-de-salida/)

### Normativa sobre transparencia algoritmos

Surge en prensa

**11 marzo 2021**

[Glovo, Deliveroo y Uber tendrán que hacer públicos sus algoritmos](https://www.elsaltodiario.com/falsos-autonomos/glovo-deliveroo-uber-ley-rider-hacer-publicos-algoritmos)  
La Ministra de Trabajo y Economía Social, Yolanda Díaz, anuncia que las empresas de reparto y otras que utilizan algoritmos tendrán que explicar las fórmulas matemáticas que organizan la actividad de sus empleados.

**12 mayo 2021**

[Real Decreto-ley 9/2021, de 11 de mayo, por el que se modifica el texto refundido de la Ley del Estatuto de los Trabajadores, aprobado por el Real Decreto Legislativo 2/2015, de 23 de octubre, para garantizar los derechos laborales de las personas dedicadas al reparto en el ámbito de plataformas digitales.](https://boe.es/buscar/act.php?id=BOE-A-2021-7840)  

**24 mayo 2022**

[Digital Services Act: agreement for a transparent and safe online environment](https://www.europarl.europa.eu/news/es/press-room/20220412IPR27111/digital-services-act-agreement-for-a-transparent-and-safe-online-environment)  
>Access to platforms’ algorithms now possible   
...  
Under the new rules, intermediary services, namely online platforms - such as social media and marketplaces - will have to take measures to protect their users from illegal content, goods and services.  
**Algorithmic accountability:** the European Commission as well as the member states will have access to the algorithms of very large online platforms;  

### Transparencia algoritmos en educación 

#### Algoritmo cálculo asignación gastos de funcionamiento centros públicos en Madrid

En noviembre 2020 consigo una resolución relacionada con algoritmos: no pido código fuente, pero sí información sobre "algoritmo" de cálculo de asignación de gastos de funcionamiento a los centros públicos de Madrid, y CTBG estima que se debe facilitar aunque tengan que sacarse de la aplicación informática.

[twitter FiQuiPedia/status/1326570990667771907](https://twitter.com/FiQuiPedia/status/1326570990667771907)  
Resolución estimatoria [RT0379/2020 @ConsejoTBG](https://www.consejodetransparencia.es/ct_Home/dam/jcr:663bd7fc-7222-4664-9e47-7661c0a40e32/RT_0379_2020_Censurado.pdf)  
\#TransparenciaAlgoritmos  
"Independientemente de su ubicación o soporte, ya se encuentren en un solo documento o en varios, O TENGAN QUE EXTRAERSE DE LA APLICACIÓN INFORMÁTICA"  

Pongo parte del texto, no solo la frase del tuit:

>La Dirección General reconoce que los criterios están dispersos y que afectan a múltiples partidas de gasto de la Consejería, sin embargo, al mismo tiempo afirma que la interacción con los centros docentes es “muchas veces en tiempo real y que la respuesta a las necesidades surgidas requiera respuestas de carácter inmediato”. De hecho, la existencia de una aplicación como la mencionada sugiere que al menos una parte de esos criterios objetivos se encuentran incorporados a la misma.  
De este modo, la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial, dispone de documentación relativa al sistema de cálculo de la asignación, independientemente de su soporte, o de que se encuentre dispersa en varios documentos. De hecho, debe presumirse que para una adecuada gestión de los miles de centros docentes que existen en la Comunidad, dichos criterios se encuentran identificados por la Consejería y son de uso habitual incluso automatizado. El hecho de que un ciudadano quiera conocer dichos criterios objetivos satisface la finalidad de la LTAIBG prevista en su preámbulo para que “los ciudadanos pueden conocer cómo se toman las decisiones que les afectan, cómo se manejan los fondos públicos o bajo qué criterios actúan nuestras instituciones podremos hablar del inicio de un proceso en el que los poderes públicos comienzan a responder a una sociedad que es crítica,  exigente y que demanda participación de los poderes públicos”.  

**4 agosto 2024**  
Tras conseguir la información en 2020 para cursos 2018, 2019 y 2020, creo que 2024 es el momento de pedir la información de años 2021, 2022, 2023 y 2024, y poder comparar la evolución / crecimiento de asignación de gastos a centros públicos frente a asignación de gastos a centros privados con concierto vía módulo de concierto.


---

“Solicito copia o enlace a documentación que permita el cálculo de cuantía asociada todos y cada uno de los criterios de cálculo de asignación de gastos de funcionamiento a los centros públicos para los años 2021, 2022, 2023 y 2024, desglosando valor por criterio y tramo.
Dicha información fue facilitada por la Consejería para ejercicios 2018, 2019 y 2020 tras RT0379/2020 https://www.consejodetransparencia.es/ct_Home/dam/jcr:663bd7fc-7222-4664-9e47-7661c0a40e32/RT_0379_2020_Censurado.pdf y solicitud 09-OPEN-00179.4/2020 en documento con CSV 1056163699752698468977

---

Puesta 4 agosto 2024 43/083522.9/24


#### Algoritmo cálculo cupo profesorado en Madrid

Para ESO y Bachillerato la administración informa a los equipos directivos en julio de cada curso del "cupo", que es la cantidad de docentes que van a tener el curso siguiente. Durante junio los centros realizan la matrícula y saben qué cantidad de grupos y optativas podrían tener, pero no saben si los van a tener realmente.  
Alguna vez algún equipo directivo me ha dicho que "las DATs tienen ciertas fórmulas para calcular el cupo"  
También se podría relacionar con el hecho de que la asignación informática de destinos para docentes se hace tras conocer el cupo, salvo para el cupo de religión.  
Las instrucciones de inicio de curso que se publican en julio citan el cupo varias veces   
[INSTRUCCIONES DE LA VICECONSEJERÍA DE POLÍTICA Y ORGANIZACIÓN EDUCATIVA, SOBRE COMIENZO DEL CURSO ESCOLAR 2024-2025 EN CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID](https://www.comunidad.madrid/sites/default/files/instrucciones_inicio_curso_2024_2025.pdf)  
> ajustándose al cupo máximo autorizado  
> ajustándose al cupo máximo autorizado  
> con el cupo aprobado  
> con el cupo de profesores asignado  
> con el cupo de profesores asignado  
> con el cupo de profesores asignado  

La idea es pedir información utilizando los argumentos con los que dieron la fórmula del cálculo de la asignación económica.  


Solicito copia o enlace a:  
- La lista de parámetros objetivos que utiliza la administración educativa para asignar el cupo a los centros educativos.   
- Documentación del sistema/algoritmo de cálculo del cupo de profesores asignado a cada centro teniendo en cuenta el valor de todos los parámetros objetivos utilizados.
- En caso de que la información no sea la misma en los últimos cursos, se solicita separada para cursos 2022-2023, 2023-2024 y 2024-2025.
Procede citar la resolución de CTBG RT0379/2020 que para información similar indica "Independientemente de su ubicación o soporte, ya se encuentren en un solo documento o en varios, o tengan que extraerse de la aplicación informática" 
https://www.consejodetransparencia.es/ct_Home/dam/jcr:663bd7fc-7222-4664-9e47-7661c0a40e32/RT_0379_2020_Censurado.pdf

Presentada 3 agosto 2024 43/082145.9/24

### Transparencia sorteo tribunales oposiciones

En marzo 2021 surge un tema que me lleva a hacer el ejercicio de buscar transparencia en un algoritmo

[twitter FiQuiPedia/status/1367144376963629057](https://twitter.com/FiQuiPedia/status/1367144376963629057)  
El sorteo de tribunales en Madrid ya no es por letra  
[NOTA INFORMATIVA. ANUNCIO DEL SORTEO PÚBLICO ALEATORIO PARA LA DESIGNACIÓN DE VOCALES DEL TRIBUNAL DE SELECCIÓN DE LOS PROCEDIMIENTOS SELECTIVOS PARA INGRESO Y ACCESOS A LOS CUERPOS DE PROFESORES DE ENSEÑANZA SECUNDARIA, PROFESORES DE ESCUELAS OFICIALES DE IDIOMAS, PROFESORES TÉCNICOS DE FORMACIÓN PROFESIONAL, PROFESORES DE MÚSICA Y ARTES ESCÉNICAS, PROFESORES DE ARTES PLÁSTICAS Y DISEÑO Y DE MAESTROS DE TALLER DE ARTES PLÁSTICAS Y DISEÑO Y PROCEDIMIENTO PARA LA ADQUISICIÓN DE NUEVAS ESPECIALIDADES POR LOS FUNCIONARIOS DE LOS CITADOS CUERPOS, (BOCM DE 24 DE MARZO DE 2020)](https://comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210302_nota_informativa_sorteo_trib.pdf)  
Sobre "mediante una aplicación informática, numerados y ordenados aleatoriamente": #TransparenciaAlgoritmos

El tema, para mi muy llamativo, es el cambio de criterio entre 2018 y 2021

[twitter FiQuiPedia/status/1372630934223212546](https://twitter.com/FiQuiPedia/status/1372630934223212546)  
Listados de de funcionarios para formar tribunales  
2018: @educacmadrid no los facilita tras solicitarlos (solo apellidos)  
2021: @educacmadrid los publica e incluye nombre y NIF ofuscado  
\#Transparencia
\#ReclamadMalditos 

3 años después lo facilita de forma activa, con más detalle del que en 2018 argumentaba que no debía facilitar tras pedirlo.

A pesar del cambio en 3 años en cuanto a listados, creo que sin saber cómo es el sorteo sigue sin haber transparencia 

**2 marzo 2021**

[NOTA INFORMATIVA. ANUNCIO DEL SORTEO PÚBLICO ALEATORIO PARA LA DESIGNACIÓN DE VOCALES DEL TRIBUNAL DE SELECCIÓN DE LOS PROCEDIMIENTOS SELECTIVOS PARA INGRESO Y ACCESOS A LOS CUERPOS DE PROFESORES DE ENSEÑANZA SECUNDARIA, PROFESORES DE ESCUELAS OFICIALES DE IDIOMAS, PROFESORES TÉCNICOS DE FORMACIÓN PROFESIONAL, PROFESORES DE MÚSICA Y ARTES ESCÉNICAS, PROFESORES DE ARTES PLÁSTICAS Y DISEÑO Y DE MAESTROS DE TALLER DE ARTES PLÁSTICAS Y DISEÑO Y PROCEDIMIENTO PARA LA ADQUISICIÓN DE NUEVAS ESPECIALIDADES POR LOS FUNCIONARIOS DE LOS CITADOS CUERPOS, (BOCM DE 24 DE MARZO DE 2020)](https://comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210302_nota_informativa_sorteo_trib.pdf)  
> A este fin, y teniendo en cuenta la fecha de finalización de presentación de solicitudes, se anuncia que el sorteo se realizará el martes 9 de marzo de 2021, a las 12.30 horas, en la sede de la Dirección General de Recursos Humanos, calle S anta Hortensia número 30, planta baja.  
Se contará con la participación de un Notario designado por el Ilustre Colegio Notarial de Madrid, que dará fe del sorteo público aleatorio.  
Con carácter previo a la fecha de celebración del sorteo, se remitirán al Notario designado los listados del personal funcionario susceptible de poder participar en los tribunales, una vez excluidos los indicados en la base 6.1.5 de la convocatoria, ordenados alfabéticamente, por cada una de las especialidades convocadas  
El proceso de sorteo se desarrollará de la siguiente manera:  
- En primer lugar, los listados del personal funcionario elegible, para cada especialidad, ordenados alfabéticamente, serán, mediante una aplicación informática, numerados y ordenados aleatoriamente.  
- A continuación, y también informáticamente, se sorteará un número aleatorio (comprendido entre 0 y 9999).  
- En cada especialidad, al número obtenido se le aplicará la siguiente operación: (número obtenido/10000) x número de profesores integrantes en la lista de la especialidad.  
- A la parte entera de dicho resultado se le sumará 1.  
- El resultado obtenido de la función aleatoria aplicada, determinará el número a partir del cual se designarán los vocales titulares y suplentes necesarios para la formación de los tribunales de cada especialidad, según el orden numérico consecutivo asignado en el listado aleatorio.  

Hago el ejercicio de pedir el algoritmo de ordenación con los tribunales; si se consigue que den un código fuente es ese, y darlo será una grieta / precedente para no ver imposible pedir y conseguir transparencia en otras cosas, que pidiendo inicialmente no darían (el algoritmo de asignación de puestos escolares)

**8 marzo 2021**

Solicito copia del código fuente de la aplicación informática utilizada para el sorteo de tribunales asociado a procesos selectivos según nota informativa de 2 marzo 2021  
https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210302_nota_informativa_sorteo_trib.pdf  

Según dicha nota "\[los listados remitidos al notario] serán, mediante una aplicación informática, numerados y ordenados aleatoriamente ...también  informáticamente, se sorteará un número aleatorio (comprendido entre 0 y 9999)"

Considero que, manteniendo el notario como garantía del contenido de los listados, una transparencia completa supone que no exista opacidad en el código utilizado sobre ellos. El propio notario podría verificar que al listado que se ha facilitado se le aplica un algoritmo implementado con un código público.

Considero que la aplicación informática debe ser suficientemente genérica y sencilla para que sea utilizada en más sorteos y permita garantizar la transparencia.

**18 marzo 2021**

[RESOLUCIÓN DE 18 DE MARZO DE 2021, DE LA DIRECCIÓN GENERAL DE RECURSOS HUMANOS, POR LA QUE SE PUBLICA EL RESULTADO DEL SORTEO PÚBLICO ALEATORIO PARA LA DESIGNACIÓN DE VOCALES DEL TRIBUNAL DE SELECCIÓN DE LOS PROCEDIMIENTOS SELECTIVOS PARA INGRESO Y ACCESOS A LOS CUERPOS DE PROFESORES DE ENSEÑANZA SECUNDARIA, PROFESORES DE ESCUELAS OFICIALES DE IDIOMAS, PROFESORES TÉCNICOS DE FORMACIÓN PROFESIONAL, PROFESORES DE MÚSICA Y ARTES ESCÉNICAS, PROFESORES DE ARTES PLÁSTICAS Y DISEÑO Y DE MAESTROS DE TALLER DE ARTES PLÁSTICAS Y DISEÑO Y PROCEDIMIENTO PARA LA ADQUISICIÓN DE NUEVAS ESPECIALIDADES POR LOS FUNCIONARIOS DE LOS CITADOS CUERPOS, CELEBRADO EL DÍA 9 DE MARZO DE 2021.](https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210318_resolucion_listas_sorteo_trib.pdf)  
> Primero.- Publicar los listados, por Cuerpo y Especialidad, del personal funcionario elegible ...  
En estos listados figura el número inicial según el orden alfabético, así como el número aleatorio asignado a cada uno, resultante del acto público de sorteo (para obtener el número aleatorio se utilizó el programa informático OPOS.).  
Segundo.- Publicar el número aleatorio obtenido en el acto público (comprendido entre 0 y 9999), al que se aplicará la fórmula para determinar el número a partir del cual se designarán los vocales titulares y suplentes, siendo el resultante el número 6589.  
Tercero.- Publicar, por Cuerpo y Especialidad, el número desde el cual se designarán los vocales titulares y suplentes necesarios para la formación de los tribunales, una vez aplicada la fórmula expuesta anteriormente.

**24 marzo 2021**

[Recibo resolución denegatoria](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2021-03-24-ResolucionCodigoFuenteSorteo.pdf)  

Iré a CTBG, pero resumo aquí ideas tras leerla:

> De acuerdo con el apartado 1. j) del artículo 14 de la LTIBG, el derecho de acceso podrá ser limitado cuando acceder a la información suponga un perjuicio para la propiedad intelectual. El código fuente es el archivo o conjunto de archivos que tienen un conjunto de instrucciones muy precisas, basadas en un lenguaje de programación, que se utiliza para que los diferentes programas informáticos que lo utilizan se puedan ejecutar sin mayores problemas. El código fuente es, por tanto, una parte del programa de ordenador y pertenece como el resto de las partes, de entrada, a su autor, a su creador. Además, y puesto que el código fuente se expresa de forma escrita, está protegido por el derecho de autor como obra literaria, tal y como establece el artículo 1 de la Directiva (91/250/CEE) del Consejo Europeo sobre la protección jurídica de programas de ordenador.  
...  
Realizado el test del interés público, no cabe considerar justificada la solicitud referida al código fuente de la aplicación informática utilizada para el sorteo de los miembros de los tribunales de oposición, ya que dicha aplicación ha sido diseñada para ejecutar el procedimiento informático mediante el que este sorteo se realiza, que, como ya se ha señalado, se encuentra publicado junto con el resto de información relativa al procedimiento selectivo en la web https://www.comunidad.madrid/servicios/educacion/personal-educacion. Es decir, el peticionario conoce el criterio bajo el cual actúa esta institución pública.  
Por otro lado, hay que tener en cuenta que a través del código fuente se pueden conocer detalles de un programa informático y de sus vulnerabilidades, quedando, en caso de facilitarse el acceso solicitado, desprotegida la aplicación frente a posibles ataques y usos indebidos. Asimismo, la función de revisión y comprobación del cumplimiento de las especificaciones funcionales de dicha aplicación corresponde, en todo caso, a la propia Dirección General de Recursos Humanos.  

**30 marzo 2021**

Planteo reclamación CTBG

---

Considero que de manera geneal el código fuente utilizado por la administración en sus decisiones debe ser público y transparente, tal y como se argumenta aquí  
https://civio.es/novedades/2019/07/02/que-se-nos-regule-mediante-codigo-fuente-o-algoritmos-secretos-es-algo-que-jamas-debe-permitirse-en-un-estado-social-democratico-y-de-derecho/  

Intento rebatir los argumentos de la resolución denegatoria:

>1. Se indica "De acuerdo con el apartado 1. j) del artículo 14 de la LTIBG, el derecho de acceso podrá ser limitado cuando acceder a la información suponga un perjuicio para la propiedad intelectual"

No se indica de quién es la propiedad intelectual y qué perjuicio conlleva.

>2. Se indica "El código fuente es, por tanto, una parte del programa de ordenador y pertenece como el resto de las partes, de entrada, a su autor, a su creador."

No se indica quién es su autor, su creador, al que pertenece.

No se indica si es un desarrollo propio de la Comunidad de Madrid o un programa comercial.

>3. Se indica "está protegido por el derecho de autor como obra literaria, tal y como establece el artículo 1 de la Directiva (91/250/CEE)"

https://eur-lex.europa.eu/legal-content/ES/TXT/?uri=CELEX:31991L0250

El punto 3 de dicho artículo indica "El programa de ordenador quedará protegido si fuere original en el sentido de que sea una creación intelectual propia de su autor. No se aplicará ningún otro criterio para conceder la protección."

Si implementa el algoritmo indicado, algoritmo público, se limita a realizar una ordenación al azar. 

No solo creo que considerar una ordenación al azar una creación intelectual es discutible, sino que en este caso entra en colisión con el derecho a la transparencia.

>4. Se indica "dicha aplicación ha sido diseñada para ejecutar el procedimiento informático mediante el que este sorteo se realiza, que, como ya se ha señalado, se encuentra publicado" y también "Asimismo, la función de revisión y comprobación del cumplimiento de las especificaciones funcionales de dicha aplicación corresponde, en todo caso, a la propia Dirección General de Recursos Humanos."

Si se afirma que la aplicación ha sido diseñada para ejecutar un procedimiento público, no debería existir ningún motivo para evitar que cualquiera pueda verificar dicha afirmación, lo que supone revisar la aplicación.

Resulta llamativo que al poner solicitud en 2018 09-OPEN-00030.7/2018 en la que pedía "Listados de funcionarios de carrera, en los que figuren ambos apellidos, y desglosados por especialidades, que pueden ser participar en el sorteo para la formación de tribunales de los procesos selectivos docentes de 2018." se inadmitió considerándola se había realizado de forma abusiva e irrazonable, con una finalidad no amparada por la Ley de Transparencia, y posteriormente CTBG desestimó en RT0176/2018 alegando artículo 18.1.e, y sin embargo:

+en 2021 

+en la misma administración

+se publica esa información ampliada (no solo incluye apellidos y desglose por especialidad como indicaba en mi solicitud en 2018, sino que añade nombre y NIF ofuscado)

+se publica de forma activa sin que nadie lo solicite, y sin haber problemas de volumen ni de notificar a terceros afectados.

https://www.comunidad.madrid/servicios/educacion/tribunales-oposiciones-cuerpos-secundaria-fp-re-2021#formacion-tribunales

https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210318_resolucion_listas_sorteo_trib.pdf

"Cuarto.- Los listados aludidos en la base primera y tercera, se publicarán en el portal www.comunidad.madrid,  dentro  del  apartado  “Administración  electrónica:  Punto  de  acceso general”, escribiendo en el texto del buscador “oposiciones secundaria 2020”"

Ejemplo de listados para las especialidades cuerpo 0590

https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210318_590_listas.pdf

+se indica que dichos listados se facilitan con anterioridad a un notario

https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210302_nota_informativa_sorteo_trib.pdf

"Con  carácter  previo  a  la  fecha  de  celebración  del  sorteo,  se  remitirán  al  Notario designado  los  listados  del  personal  funcionario  susceptible  de  poder  participar  en  los  tribunales,  una  vez  excluidos  los  indicados  en  la  base  6.1.5  de  la  convocatoria,  ordenados alfabéticamente, por cada una de las especialidades convocadas. "

También se indica que tras el sorteo, el Notario coteja listados iniciales y los ordenados al azar

https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210318_resolucion_listas_sorteo_trib.pdf

"-Los   listados   del   personal   funcionario   elegible,   para   cada   especialidad, ordenados    alfabéticamente,    son,    mediante    una    aplicación    informática,    numerados y ordenados aleatoriamente. Estos listados son entregados una vez finalizado  el  acto  al  Notario,  para  su  verificación  y  comprobación  con  los  remitidos inicialmente.

- A  continuación,  y  también  informáticamente,  se  sortea  un  número  aleatorio  (comprendido entre 0 y 9999). "

Siendo bienvenida la transparencia, no se alcanza a comprender por qué en 2021 es necesario que un notario revise los listados (listados que no eran públicos en 2018 ni se llevaban al notario, pero sí se hace en 2021) pero no es necesario que nadie revise el proceso de elección en base a esos listados.

Sin presuponer mal comportamiento en la administración, de la misma manera que ha acudido a un notario para aportar transparencia y garantías sobre los listados, se debe aportar sobre el código fuente que supone procesamiento y decisiones sobre esos listados, sin que baste afirmar que "dicha aplicación ha sido diseñada para ejecutar el procedimiento informático". Ello incluye tanto la ordenación como el sorteo del número aleatorio, ambas realizadas informáticamente.

>5. Se indica "Por otro lado, hay que tener en cuenta que a través del código fuente se pueden conocer detalles de un programa informático y de sus vulnerabilidades, quedando, en caso de facilitarse el acceso solicitado, desprotegida la aplicación frente a posibles ataques y usos indebidos."

No se alcanza a comprender, teniendo un conocimiento informático básico de que la aplicación OPOS citada tiene como función ordenar listados y generar números aleatorios y debe trabajar en local, sin necesidad de conexión alguna, qué  posibles ataques y usos indebidos permitiría conocer su código fuente.

>6. La aplicación OPOS realiza un tratamiento total o parcialmente automatizado de datos personales, por lo que le aplica Ley Orgánica 3/2018

https://www.boe.es/buscar/act.php?id=BOE-A-2018-16673#a2

Sin dudar de la legitimación del tratamiento de datos realizado (enmarcado en artículo 8 Ley 3/2018, obligación legal, interés público o ejercicio de poderes públicos), este tratamiento automatizado refuerza el derecho a conocer detalles del algoritmo

https://www.boe.es/buscar/doc.php?id=DOUE-L-2016-80807

Artículo 13.2.f "información significativa sobre la lógica aplicada"

Ya he comentado con anterioridad que la descripción de la lógica que se debe aplicar a los listados se ha publicado en 2021, pero de la misma manera que en 2018 la lógica estaba publicada (sorteo de letra) pero la transparencia era insuficiente sin incluir transparencia en listados, en 2021 considero que además de publicar los listados se debe hacer público el código fuente.

>7. Como consideración final, considero que ese código fuente es sencillo, solo debe usar unas pocas librerías para gestionar listados y manejar números aleatorios, y su transparencia generaría un beneficio al poder ser reutilizado en otras administraciones.

---

**31 marzo 2021**

Se asigna RT 0253/2021

**12 mayo 2021**

Escribo a CTBG citando  Real Decreto-ley 9/2021 publicado en BOE que cita algoritmos.

**19 noviembre 2021**

[Resolución estimatoria RT 0253/2021](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2021-11-19-report_RT_0253_2021.pdf)  
Aquí enlace en web CTBG: [RT0253/2021 código fuente aplicación informática sorteo de tribunales procesos selectivos](https://www.consejodetransparencia.es/ct_Home/dam/jcr:ddb7deb0-76fd-46cd-bf14-3b14f3f3dcc7/RT_0253_2021.pdf) 

**8 febrero 2022**

Presento escrito de requerimiento usando el formulario genérico  

https://tramita.comunidad.madrid/prestacion-social/formulario-solicitud-generica  

(no se puede vía registro electrónico general https://gestiona7.madrid.org/ereg_virtual_presenta/run/j/InicioDistribuidor.icm sin un formulario concreto, aunque en el enlace anterior en un punto concreto permite "descargar el formulario cumplimentado en formato pdf y presentarlo más tarde por registro electrónico.")

09/201476.9/22

**4 abril 2022**

Me notifican que Madrid inicia contencioso PO 0000005_2022

**20 abril 2022**

Realizo personación vía correo administrativo en oficina correos.

---

AL JUZGADO CENTRAL DE LO CONTENCIOSO ADMINISTRATIVO Nº 11

Procedimiento Ordinario 0000005/2022  
Yo, Enrique García Simón, con DNI *** y domicilio en ***, ante el Juzgado Central de lo Contencioso Administrativo nº 11 comparezco y como mejor proceda,

DIGO

Que habiendo sido emplazado se me concedió el plazo de nueve días para personarme en los autos.  
Que teniendo interés directo en el citado procedimiento es por lo que al amparo del art.50.3 LJCA me persono y comparezco.
Por todo lo cual,  
AL JUZGADO SUPLICO que tenga por presentado este escrito y, en consecuencia, se me tenga por personado y parte en mi nombre y representación en el Procedimiento Ordinario 5/2022.  
Es justicia que pido en Azuqueca de Henares, a 20 de abril de 2022.

---

**27 abril 2022**

Recibo notificación por correo certificado indicando que es obligatorio que tenga abogado.

Incluyo comentario de abril 2022 sobre sorteo oposiciones primaria 2022 para ver la opacidad:

[https://twitter.com/lge_my/status/1512093136163586053](https://twitter.com/lge_my/status/1512093136163586053)  
Sobre el sorteo para tribunal de oposiciones en Madrid.  
1. Aleatoriamente, te dan un número que nadie te notifica.  
2. Hacen una fórmula matemática. Sale el número, el 2473.  
3. Como no sabes tu número y tampoco el número de miembros necesarios para cubrir la demanda, te quedas =.  


En 2021:

-9 de marzo se realiza ordenación aleatoria listado y se sortea número

-18 de marzo se publica ordenación listado y número sorteado.

Creo que sería más transparente publicar primero la lista con números aleatorios, y luego, con la lista ya publicada, realizar el sorteo y publicar el número. Que ambas cosas sean al tiempo y sin más información, es fácil que la ordenación y el número hagan que salgan o dejen de salir ciertas personas.

Sin conocer el código fuente, el notario no puede dar fe de la aletoriedad, solo de lo que da como resultado el programa.

**21 julio 2022**

Comento ideas de alegaciones en base a lo que argumenta Madrid:

>1. https://www.boe.es/buscar/doc.php?id=DOUE-L-2016-80807

Artículo 15

Derecho de acceso del interesado

...

h)la existencia de decisiones automatizadas, incluida la elaboración de perfiles, a que se refiere el artículo 22, apartados 1 y 4, y, al menos en tales casos, **información significativa sobre la lógica aplicada** , así como la importancia y las consecuencias previstas de dicho tratamiento para el interesado.

>2.  "desprotegida la aplicación frente a posibles ataques y usos indebidos"

La idea es que esa aplicación "DEBE funcionar sin estar conectado a una red externa si no se justifica lo contrario". Si este código con esta funcionalidad tan simple puede funcionar conectado a una red externa y eso permite ataques y usos indebidos [su argumento], aumenta mucho más la importancia de conocer su código fuente. El mero indicio de que la funcionalidad que implementa el código pueda ser atacada y tener un uso indebido hace imprescindible conocer su código.

 

**2 noviembre 2022**

Recibo [sentencia a favor Madrid y en contra de resolución de CTBG](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2022-10-31-PO_5_2022-Sentencia_158_2022-anonimizada.pdf)  

**6 noviembre 2022**

Planteo una idea: en este caso el código es "poco relevante" (aunque la transparencia en oposiciones sí lo es), pero me pareció desde el principio un caso útil para sentar precedente, aparte de por ser un código muy simple (ordenar al azar una lista de nombres) porque DEBE funcionar sin estar conectado a una red externa si no se justifica lo contrario. Si este código con esta funcionalidad tan simple puede funcionar conectado a una red externa y eso permite ataques y usos indebidos \[el argumento de la administración para no darlo], aumenta mucho más la importancia de conocer su código fuente. El mero indicio de que la funcionalidad que implementa el código pueda ser atacada y tener un uso indebido hace imprescindible conocer su código.

La Comunidad no aclara si funciona localmente o permite conexión externa, y eso sería clave:

- Si funciona localmente sin conexión externa, es imposible un ataque externo, y se desmontaría su argumento.

- Si funciona en red y ello permite una conexión externa, sería muy relevante para la transparencia conocer la "funcionalidad" de dicha conexión. 

Pero hay otro detalle que hace este caso único y creo que interesante para usarlo de punta de lanza para la transparencia de algoritmos, aparte de que la funcionalidad, y por lo tanto el código, sea muy simple:

LA EJECUCIÓN DEL CÓDIGO SE HA HECHO ANTE NOTARIO.

https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210318_resolucion_listas_sorteo_trib.pdf 

"Se contó con la participación de un Notario (nº Colegiado 8104041), designado por el Ilustre Colegio Notarial de Madrid, para dar fe de dicho acto."

Si se hubiera hecho bien, el notario debería haber contado con un perito informático, dar fe del código recibido, dar fe de que se compilaba dicho código según lo que le indicaba el perito, y que se ejecutaba el resultado de dicha compilación. Dudo que se haya hecho eso, pero ... debe haber un acta notarial de lo que "hizo" el notario, y quizá ese acta aclare algo básico: si el notario puede dar fe de si se ejecutó de modo local o se ejecutó con conexión a red. Si el acta no lo dijese, se podría citar al notario como testigo:

- Si el acta/notario confirman la ejecución en local, se desmonta su argumento.

- Si el acta/notario confirman la ejecución en red, el argumento de ser atacable se mantiene, pero entonces su argumentación del pseudocódigo facilitado se derrumba, porque no describe qué uso se hace de la conexión.

- Si el acta/notario no puede confirmar nada, muestra que la presencia del notario es una falsa garantía de transparencia.

Tras esta idea, hago solicitud de transparencia:

---

Solicito copia del acta notarial que da fe del sorteo realizado el día 9 de marzo de 2021 a las 12:30 asociado a la siguiente resolución

https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210318_resolucion_listas_sorteo_trib.pdf 

para la que se indica

"Se contó con la participación de un Notario (nº Colegiado 8104041), designado por el Ilustre Colegio Notarial de Madrid, para dar fe de dicho acto."

---

59/292086.9/22 

**7 noviembre 2022**

[¿Regulados por programas y algoritmos que permanecen en secreto? Cuéntanos tu caso](https://comunidad.civio.es/t/regulados-por-programas-y-algoritmos-que-permanecen-en-secreto-cuentanos-tu-caso/76/46?u=fiquipedia)   

El hecho de que sea ante notario creo que permite sentar un precedente para aclarar que, en algoritmos, la presencia de un notario no garantiza nada si no hay transparencia en el código ejecutado

**14 noviembre 2022**

Recibo [resolución inadmitiendo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2022-11-14-ResolucionInadmision-09-OPEN-00195.7_2022.pdf)  

Confirma que la presencia del notario en un sorteo informático con un código no transparente no garantiza nada. 

"Su fin es, pues, otorgar transparencia al acto, para garantizar igualdad de oportunidades a los participantes, **sin que tenga necesariamente que recoger consideraciones sobre conocimientos periciales que el notario no tiene por qué tener.**"

**17 noviembre 2022**

Reclamación CTyPM

---

Se inadmite el acceso al acta considerando que una sentencia del contencioso que la administración ha puesto contra una resolución estimatoria del CTBG para facilitar el código fuente aplica a todo el procedimiento, cuando lo que estoy solicitando en este caso es un acta notarial.  
Resolución indica "Las actas de sorteo son instrumentos públicos creados con el fin de garantizar la fiabilidad de este tipo de actividades.  Con su intervención, el notario verifica que se realiza conforme a las bases reguladoras ...los documentos públicos autorizados por notario gozan de fe pública y su contenido se presume veraz e íntegro de acuerdo con lo dispuesto en la ley. Su fin es, pues, otorgar transparencia al acto, para garantizar igualdad de oportunidades a los participantes, sin que tenga necesariamente que recoger consideraciones sobre conocimientos periciales que el notario no tiene por qué tener."  
Si el documento es público, garantiza la fiabilidad, su contenido se presume veraz, y su fin es otorgar transparencia, el acceso al mismo refuerza dichas afirmaciones, y la falta de acceso las cuestiona.  
La resolución realiza una valoración para justificar la inadmisión y considerarla no justificada con la finalidad de la ley "quizás sobre ... o sobre ... pudiera ... más allá del interés público de la divulgación"  
Otras administraciones han facilitado actas notariales, se adjunta ejemplo resolución.  

---

**5 diciembre 2022**

Escribo a CTPCM

---

Quisiera informar, que dado que me personé en el PO5/2022 asociado a la sentencia, mi abogado me ha informado que el CTBG ha apelado a la Audiencia Nacional la sentencia 158/2022 de 31 de octubre de 2022, el Juzgado Central de lo Contencioso-Administrativo de Madrid que estimaba el contencioso puesto por la Comunidad de Madrid contra resolución estimatoria RT/0253/2021  
Aunque ese recurso de CTBG no aparece todavía reflejado en la web del CTBG  
[Recursos del año 2022 sobre resoluciones de Administraciones Territoriales  > 60. Acceso al código fuente aplicación informática sorteo de tribunales procesos selectivos](https://consejodetransparencia.es/ct_Home/Actividad/recursos_jurisprudencia/Recursos_AATT/2022/60-acceso-c-digo-fuente-inform-tica-tribunales.html)  
considero que es muy relevante para esta reclamación, ya que un argumento de la consejería para inadmitir mi solicitud de acta notarial era citar dicha sentencia (sentencia que ya comenté que no aplica al acta que solicito ahora), pero además ahora la sentencia está recurrida y por lo tanto no es firme.
En cuanto tenga más información, asumo que se me volverá a citar para que me persone, la haré llegar.

---

**19 diciembre 2022**

CTPCM me confirma que se ha admitido a trámite como RDACTPCM352/2022

**17 enero 2023**

Recibo [alegaciones de Madrid](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2023-01-11-5_AlegacionesreclamacionRDACTPCM352_2022.pdf)  

**19 enero 2023**

Escribo a CTyP mi respuesta

---

Comento las alegaciones del documento recibido:- "Primero"  Vuelven a citar la sentencia del contencioso que ganaron, que, ellos no citan y asumo que saben, me consta CTBG ha recurrido a AN, de modo que no es sentencia firme.  
Indican "si, como señala la sentencia, no es necesario facilitar ni conocer el código fuente de la aplicación para lograr el objetivo de transparencia fijado en la LTIBG, tampoco debe serlo la actual petición del interesado \[del acta]". Dado que la sentencia no es firme y está recurrida por CTBG, no es una base válida. Además la sentencia es sobre el código, no sobre el acta: la valoración de que código y acta tienen la misma consideración es una interpretación suya.  
- "Segundo" No aceptan que usasen lenguaje subjetivo "quizá ... pudiera" y consideran que lo que ellos indican es objetivo. Citan de nuevo la sentencia, y citan capturas de un foro que es público: comparto siempre de manera transparente y abierta lo que hago. Sin comentar las capturas se limitan a citar "«Bajo el prisma de la Ley 19/2013, no cabe todo (Sentencia No 120/2019 del Juzgado Central contencioso-administrativo no 5 de Madrid)". Considero que la transparencia en algoritmos es un tema muy importante para los ciudadanos, por eso puse (aparece en su captura) "El mero indicio de que la funcionalidad que implementa el código pueda ser atacada y tener un uso indebido hace imprescindible conocer su código." y más adelante (no está en su captura pero sí está en el enlace "El hecho de que sea ante notario creo que permite sentar un precedente para aclarar que, en algoritmos, la presencia de un notario no garantiza nada si no hay transparencia en el código ejecutado"- "Tercero" Citan otra vez la resolución judicial no firme. No les vale el ejemplo de que otras administraciones sí hayan facilitado actas notariales, y ahora mezclan deliberadamente el acceso al acta con el acceso al código.  
Donde antes decían (respuesta 14 noviembre)  
“sin que tenga necesariamente que recoger consideraciones sobre conocimientos periciales que el notario no tiene por qué tener”
ahora dicen (respuesta 11 enero)  
“Facilitar al interesado el acta…podría suponer, en el caso de que se hubieran recogido por el notario, el acceso a datos internos de la aplicación, incluso al código fuente de la misma, que le ha sido denegado judicialmente.”  
- "Tercera y última" Insisten en que mi reclamación es abusiva. Me parece peculiar que la normativa de transparencia en la relación ciudadano - administración solo recoja el concepto de “comportamiento abusivo” por parte del ciudadano. Indican "Conociendo, pues, el peticionario todos los detalles del procedimiento para la selección de los vocales de los tribunales selectivos, por haber sido publicados y facilitados con anterioridad y por haberlo así declarado el juzgador en la Sentencia no 158/2022 de 31 de octubre de 2022, acceder ahora a lo solicitado debe considerarse una solicitud abusiva".  
La idea esencial de fondo es que no se cuestiona en este caso que haya transparencia en la publicación del procedimiento, lo que se cuestiona es si hay transparencia real en el procedimiento real seguido si no hay transparencia en el código ejecutado; el ciudadano no tiene transparencia que le permita saber si el algoritmo implementado y ejecutado por el código cumple con el procedimiento  publicado.

---

**11 abril 2023**

Se anuncia sorteo informático, y se puede ver online

https://www.comunidad.madrid/sites/default/files/doc/educacion/rh20/rh20_ni_sorteo_oposiciones_sec_re_230411.pdf

**12 abril 2023**

Recibo [respuesta a petición ver sorteo online](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2023-04-12-Correo%20respuesta%20Solicitud%20acceso%20retransmision%20Sorteo%20P%C3%BAblico%20aleatorio%20para%20la%20designaci%C3%B3n%20de%20vocales%20del%20Tribunal%20de%20Selecci%C3%B3n.pdf) 

**13 abril 2023**

Envío correo a CTyP sobre RDACTPCM352/2022

Buenas tardes  
Informo que hoy 13 de abril de 2023 se ha realizado el sorteo para las oposiciones de 2023 equivalente al sorteo de 2021 para las oposiciones de 2021 del que reclamo el acta.  
En este caso han permitido la asistencia online al sorteo  
https://www.comunidad.madrid/sites/default/files/doc/educacion/rh20/rh20_ni_sorteo_oposiciones_sec_re_230411.pdf  
He solicitado asistencia y he grabado el sorteo, que ha durado unos 10 minutos (no lo adjunto por su volumen), pero sí el correo en el que se me enviaba enlace  
https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2023-04-12-Correo%20respuesta%20Solicitud%20acceso%20retransmision%20Sorteo%20P%C3%BAblico%20aleatorio%20para%20la%20designaci%C3%B3n%20de%20vocales%20del%20Tribunal%20de%20Selecci%C3%B3n.pdf

Considero que el hecho de que retransmita el sorteo en el que aparece el notario (se ha indicado su nombre, Don Jesús Roa Martínez) es un argumento a favor de que se facilite el acta notarial de ese proceso. En el sorteo anterior también se indicó que se retransmitió telemáticamente.  
https://www.comunidad.madrid/sites/default/files/doc/educacion/rh03/edu_rh03_20210318_resolucion_listas_sorteo_trib.pdf

**23 mayo 2023**

Recibo [resolución estimatoria RDA134_2023](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2023-05-19-RDA134_2023_FDO-anonimizada.pdf)  

**9 junio 2023**

Recibo ejecución con el acta 

[2023-06-08-ResolucionEjecucionRDA134-2023.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2023-06-08-ResolucionEjecucionRDA134-2023.pdf)  

[2023-06-09-CorreccionErroresResolucionEjecucionRDA134-2023.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2023-06-09-CorreccionErroresResolucionEjecucionRDA134-2023.pdf)  

[2021-03-09-ActaNotarialSorteo-RecibidoAnonimizandoNifsPagina6-eliminadaFirmaPagina9.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2021-MadridSorteoOposiciones/2021-03-09-ActaNotarialSorteo-RecibidoAnonimizandoNifsPagina6-eliminadaFirmaPagina9.pdf)  

Lo comento aquí

https://comunidad.civio.es/t/regulados-por-programas-y-algoritmos-que-permanecen-en-secreto-cuentanos-tu-caso/76/53

Un “acta de presencia” y una “diligencia de constancia de hechos”, donde la presencia del notario no deja constancia del algoritmo ejecutado nos ha costado 1477,60€

Asociado al algoritmo, el notario se limita a decir

> Para llevar a cabo el procedimiento del sorteo previamente se me han remitido el día cinco de marzo de dos mil veintiuno, al correo corportativo … CIENTO OCHO (108) listados del personal funcionario elegible, uno por cada especialidad, ordenados alfabéticamente.
> Dichos listados por medio de una aplicación informática serán numerados y ordenados alfabéticamente.
> Y a continuación, también informáticamente, se obtendrá un número aleatorio comprendido entre el 0 y 9999.
> … 
> me hace entrega, acto seguido, del “pendrive” que previamente había extraído del blíster en el que se hallaba empaquetado y cerrado, sin estrenar, y hecho entrega al técnico de apoyo …, para su introduce en el correspondiente puerto del ordenador que se ha efectuado el proceso y la descarga en de la información de que se trata. A continuación y también informáticamente se procede al sorteo para la obtención de un número aleatorio entre el 0 y 9999, siendo el número obtenido el 6589…

Ni siquiera certifica que los listados sobre los que se aplica el algoritmo son los que le enviaron días antes.

**22 octubre 2024**
Sentencia Audiencia Nacional

[SAN, a 22 de octubre de 2024 - ROJ: SAN 5981/2024](https://www.poderjudicial.es/search/AN/openDocument/ca5b4297e8d3eb21a0a8778d75e36f0d/20241205)  
ECLI:ES:AN:2024:5981 Ponente: FELIPE FRESNEDA PLAZA Nº Recurso: 49/2023  

> Que debemos desestimar y desestimamos el recurso de apelación número
49/2033, interpuesto por la representación procesal del Consejo de Transparencia y
Buen Gobierno, frente a la sentencia del Juzgado Central de lo Contencioso-
Administrativo Núm. 11 de 1 de octubre de 2022, debiendo estarse al fallo de dicha
sentencia en cuanto a la desestimación del recurso contencioso-administrativo, todo
ello con imposición de costas la parte actora, en los términos razonados en el
precedente fundamento de derecho quinto.

### Transparencia algoritmo admisión

Ya había planteado la importancia de transparencia en otro algoritmo asociado a educación, cuando se comenta que no existe un programa centralizado informatizado, ya que ese algoritmo condiciona la segregación.

[twitter FiQuiPedia/status/1234404546673246209](https://twitter.com/FiQuiPedia/status/1234404546673246209)  
Dos ideas:  
-Veo el tema suficientemente importante como para que que el código fuente de ese programa/algoritmo que asigna alumnos a centros  de cualquier administración sea público  

En mayo 2021 veo la poca transparencia en el caso de admisión de mi hija en ESO, de modo que documento

**28 mayo 2021**

Salen listados de asignación provisional. Ya había salido baremo definitivo

https://www.educa.jccm.es/educacion/cm/educa_jccm/tkContent?idContent=176695

Se cita

https://www.educa.jccm.es/es/normativa/orden-5-2017-19-enero-consejeria-educacion-cultura-deportes

https://www.educa.jccm.es/es/noticias/numero-desempate-proceso-admision-2-ciclo-infantil-primar-1.ficheros/342811-Resolucion%20N%C3%BAmero%20de%20Desempate.pdf

https://www.educa.jccm.es/es/normativa/decreto-1-2017-10-enero

> Artículo 2. Principios generales  
...  
\3. Los padres o tutores legales del alumnado, o este, cuando haya alcanzado la mayoría de edad, tienen derecho a elegir centro educativo y, en ausencia de puestos escolares vacantes en alguno de los centros elegidos, a que se tenga en consideración todas sus opciones, de acuerdo con su propio orden de prelación.  
...  
Artículo 13. Prioridad en la admisión.  
\1. La puntuación total obtenida en aplicación de los criterios anteriormente establecidos, que en ningún caso será superior a 27 puntos para las enseñanzas de segundo ciclo de la Educación Infantil, Educación Primaria y Educación Secundaria Obligatoria, o 32 puntos para las enseñanzas de Bachillerato, decidirá el orden final de admisión.  
\2. En caso de empate, se dilucidará el mismo mediante la selección de aquellos solicitantes que tengan hermanos o hermanas matriculados en el centro. De persistir el empate se seleccionará a quienes hayan obtenido la mayor puntuación en cada uno de los criterios según la siguiente prelación:  
a) Mayor puntuación obtenida en el apartado de proximidad al centro, tanto del domicilio como del lugar de trabajo.  
b) Existencia de padres o tutores legales que trabajen en el centro.  
c) Existencia de discapacidad en el alumno o alguno de sus padres/tutores legales, hermanos o hermanas.  
d) Condición legal de familia numerosa.  
e) Situación de acogimiento familiar del alumno o la alumna.  
f) Rentas anuales de la unidad familiar.  
g) Expediente académico, en el caso de enseñanzas de Bachillerato.  
\3. La Consejería competente en materia de educación establecerá anualmente el procedimiento para resolver situaciones de empate entre solicitantes en el caso de mantenerse el mismo una vez aplicados los criterios establecidos en los apartados anteriores. Dicho procedimiento se basará en un sistema de sorteo previo de carácter regional.  
...   
Artículo 14. Resolución del procedimiento de admisión de alumnado.  
...  
\2. Las resoluciones provisionales sobre admisión de alumnado serán supervisadas, con anterioridad a su publicación por las correspondientes Comisiones de Garantías de Admisión, quienes verificarán que dicha asignación se ha realizado respetando la valoración del baremo contenido en este decreto y el orden de prelación de los solicitantes.  
Disposiciones adicionales:  
...  
Séptima. Transparencia del proceso de admisión de alumnado.  
Para una mayor garantía de transparencia en las distintas fases del proceso de admisión, la Consejería competente en materia de educación establecerá procedimientos de coordinación y apoyo administrativo entre las Direcciones Provinciales, los centros educativos y las respectivas Comisiones de Garantías de Admisión, y pondrá a disposición de las personas titulares de las direcciones de los centros públicos y de los titulares de los centros privados concertados, recursos informáticos de carácter centralizado para la baremación de las solicitudes presentadas en sus centros y la asignación de puestos escolares vacantes conforme al orden de prelación de los solicitantes, así como para la gestión de los puestos escolares vacantes o de las reclamaciones que pudieran presentarse  

https://www.educa.jccm.es/es/noticias/numero-desempate-proceso-admision-2-ciclo-infantil-primar-1

Número de desempate del proceso de admisión de 2º ciclo de E. Infantil, E. Primaria, ESO y Bachillerato 2021/2022: 20.685 

"Cuando se produzcan situaciones de empate entre solicitantes una vez aplicados los criterios de baremación establecidos, las solicitudes se ordenarán de manera ascendente y correlativa, partiendo de la solicitud que cuente con dicho número."

El algoritmo no está bien detallado:

\1. ¿Cómo se asigna número de solicitud? Si es correlativo, es importante conocerlo, personas que quieren ir juntas maximizan probabilidades haciendo la solicitud al tiempo y teniendo números próximos.  
\2. ¿Cómo funciona el desempate? Viendo casos en admisión 2021-2022, el algoritmo parece ser así:  
1º. Para cada centro, se orden las peticiones de primera opción y se asigna centro a las que tienen mayor puntuación, siempre que el número de alumnos con esa puntuación máxima no supere el número de vacantes.   
Por ejemplo:  
Centro A con 100 vacantes, solicitudes 1ª opción: 50 baremo 20, 25 baremo 15 y 50 baremo 10. Se asignan las 75 primeras, pero no las 50 siguientes.  
Centro B con 100 vacantes, solicitudes 1ª opción: 25 baremo 20, 20 baremo 15 y 100 baremo 10. Se asignan las 40 primeras, pero no las 100 siguientes.  
Centro C con 100 vacantes, solicitudes 1ª opción: 75 baremo 20, 25 baremo 15 y 10 baremo 10. Se asignan las 100 primeras, pero no las 10 siguientes. Como estas 10 ya no pueden elegir la segunda opción, se toma su segunda opción: pongamos que 5 son a centro A y 5 a centro B de segunda opción.  
NO SE DESEMPATA ENTRE LOS QUE PIDIERON PRIMERA OPCIÓN.  
2º. Con las solicitudes que quedan sin asignar para cada centro, se agrupan por baremo y se asignan según criterio de desempate. Eso implica que si en centro A quedaron 50 vacantes sin asignar en primera opción, se combinan con las 5 que quedan sin asignar en segunda opción.  
SE DESEMPATA ENTRE LOS QUE PIDIERON DISTINTA OPCIÓN SIN TENER EN CUENTA EL ORDEN DE PRELACIÓN.  
Según eso no tiene ningún efecto haber pedido el centro A en primera opción, y según Decreto 1/2017 se entendería que se debería tener en cuenta "dicha asignación se ha realizado respetando la valoración del baremo contenido en este decreto y el orden de prelación de los solicitantes."  

**28 junio 2021**

Se publica asignación definitiva.

https://www.educa.jccm.es/es/noticias/publicada-asignacion-definitiva-proceso-admision-2-ciclo-in

Listados en 

https://www.educa.jccm.es/educa-jccm/cm/educa_jccm/tkContent?locale=es_ES&idContent=170425

No se responde de manera argumentada a las reclamaciones.

Si la desestimación se hace también de manera automática, el algoritmo que lo decide debe ser público. Si se realiza una intervención humana / manual, se debe poder pedir información.

https://www.boe.es/buscar/act.php?id=BOE-A-2015-10565#a53

Planteo texto de solicitud transparencia

---

Solicito copia del código fuente asociado al algoritmo de la aplicación informática utilizada en el proceso de admisión de alumnado de curso 2021-2022 en Castilla-La Mancha.

En normativa se cita la transparencia y recursos informáticos de carácter centralizado

https://www.educa.jccm.es/es/normativa/decreto-1-2017-10-enero

Séptima. Transparencia del proceso de admisión de alumnado.

Para una mayor garantía de transparencia en las distintas fases del proceso de admisión, la Consejería competente en materia de educación establecerá procedimientos de coordinación y apoyo administrativo entre las Direcciones Provinciales, los centros educativos y las respectivas Comisiones de Garantías de Admisión, y pondrá a disposición de las personas titulares de las direcciones de los centros públicos y de los titulares de los centros privados concertados, recursos informáticos de carácter centralizado para la baremación de las solicitudes presentadas en sus centros y la asignación de puestos escolares vacantes conforme al orden de prelación de los solicitantes, así como para la gestión de los puestos escolares vacantes o de las reclamaciones que pudieran presentarse 

La solicitud se basa en que no existe transparencia en cómo se usa el orden de prelación de las solicitudes, ya que ha habido asignaciones se ha utilizado solamente la baremación y el desempate para la asignación, ignorando el orden de prelación y haciendo que personas que han puesto un centro en primera opción lo lo tengan asignado y otras con el mismo centro no en primera opción y mismo baremo sí lo tengan asignado.

Solicitar el código fuente lo considero adecuado ya que es solicitar lo que realmente ha ejecutado la administración, no una transcripción en texto que puede suponer una interpretación. No existe ningún problema de derechos de autor, se trata de implementar un algoritmo público de manera transparente.

Para este curso se indicó

https://www.educa.jccm.es/es/noticias/numero-desempate-proceso-admision-2-ciclo-infantil-primar-1

Número de desempate del proceso de admisión de 2º ciclo de E. Infantil, E. Primaria, ESO y Bachillerato 2021/2022: 20.685 

"Cuando se produzcan situaciones de empate entre solicitantes una vez aplicados los criterios de baremación establecidos, las solicitudes se ordenarán de manera ascendente y correlativa, partiendo de la solicitud que cuente con dicho número."

El algoritmo no está bien detallado:

\1. ¿Cómo se asigna número de solicitud? Si es correlativo, es importante conocerlo, personas que quieren ir juntas maximizan probabilidades haciendo la solicitud al tiempo y teniendo números próximos.  
\2. ¿Cómo funciona el desempate? Viendo casos en admisión 2021-2022, el algoritmo parece ser así:  
1º. Para cada centro, se orden las peticiones de primera opción y se asigna centro a las que tienen mayor puntuación, siempre que el número de alumnos con esa puntuación máxima no supere el número de vacantes.   
Por ejemplo:  
Centro A con 100 vacantes, solicitudes 1ª opción: 50 baremo 20, 25 baremo 15 y 50 baremo 10. Se asignan las 75 primeras, pero no las 50 siguientes.  
Centro B con 100 vacantes, solicitudes 1ª opción: 25 baremo 20, 20 baremo 15 y 100 baremo 10. Se asignan las 40 primeras, pero no las 100 siguientes.  
Centro C con 100 vacantes, solicitudes 1ª opción: 75 baremo 20, 25 baremo 15 y 10 baremo 10. Se asignan las 100 primeras, pero no las 10 siguientes. Como estas 10 ya no pueden elegir la segunda opción, se toma su segunda opción: pongamos que 5 son a centro A y 5 a centro B de segunda opción.  
NO SE DESEMPATA ENTRE LOS QUE PIDIERON PRIMERA OPCIÓN.  
2º. Con las solicitudes que quedan sin asignar para cada centro, se agrupan por baremo y se asignan según criterio de desempate. Eso implica que si en centro A quedaron 50 vacantes sin asignar en primera opción, se combinan con las 5 que quedan sin asignar en segunda opción.  
SE DESEMPATA ENTRE LOS QUE PIDIERON DISTINTA OPCIÓN SIN TENER EN CUENTA EL ORDEN DE PRELACIÓN.  
Según eso no tiene ningún efecto haber pedido el centro A en primera opción, y según Decreto 1/2017 se entendería que se debería tener en cuenta "dicha asignación se ha realizado respetando la valoración del baremo contenido en este decreto y el orden de prelación de los solicitantes."  

--- 

número de registro 2274589/2021

**2 agosto 2021**

Recibo [notificación ampliación de plazo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/algoquedaquedecir/Algoritmos/2021-CLM-Admisi%C3%B3nAlumnado/2021-08-02-FIRMADO-SAIP_21_180200_000031-Resoluci%C3%B3ndeAmpliaci.PDF). No se inadmite ni se desestima.

https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/algoquedaquedecir/Algoritmos/2021-CLM-Admisi%C3%B3nAlumnado/2021-08-02-FIRMADO-SAIP_21_180200_000031-Resoluci%C3%B3ndeAmpliaci.PDF

**6 septiembre 2021**

Reclamo a CTBG tras mes de silencio

---

2 agosto se me notifica ampliación de plazo por complejidad, indicando explícitamente que el plazo de resolución de esta concluirá el 2 de septiembre de 2021, fecha ya superada sin recibir respuesta.

El hecho de ampliar plazo indicando "complejidad existente para la obtención de la información solicitada" creo que supone un argumento a favor de la solicitud, ya que  
- no han denegado y hablan de obtención de la información  
- facilitar el código supondría transparencia que facilitaría analizar e interpretar la información compleja, mientras que una "interpretación" de lo que se supone que debe hacer el algoritmo de admisión no garantizaría que el código que se está ejecutado implementa esa interpretación.  

--- 

RT0748/2021

**16 septiembre 2021**

Recibo [resolución estimatoria SAIP/21/180200/000031](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/algoquedaquedecir/Algoritmos/2021-CLM-Admisi%C3%B3nAlumnado/2021-09-16-FIRMADO-SAIP_21_180200_000031-Resoluci%C3%B3nEstimatori.PDF)  

Adjuntan [dos documentos](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/algoquedaquedecir/Algoritmos/2021-CLM-Admisi%C3%B3nAlumnado/2021-09-16-anexos): uno es código SQL, y otro es una transcripción en texto del algoritmo

Escribo a CTBG

---

Adjunto correo y adjuntos recibidos hoy.  
Dan parte del código fuente (SQL), pero en otra parte hacen una reelaboración y elaboran una descripción (pseudocódigo) sin dar el código fuente del algoritmo.  
Quiero dejar claro que no desisto: creo que si han podido dar código fuente de parte lo pueden dar de todo: tal y como indiqué en mi reclamación "facilitar el código supondría transparencia que facilitaría analizar e interpretar la información compleja, mientras que una "interpretación" de lo que se supone que debe hacer el algoritmo de admisión no garantizaría que el código que se está ejecutado implementa esa interpretación."

---

**11 enero 2022**

Recibo [resolución estimatoria RT 0748/2021](https://www.consejodetransparencia.es/ct_Home/dam/jcr:971c8cfe-0ef5-4cdf-afdd-191e9c8733b4/RT_0748_2021.pdf)  

Resolución cita [resolución sobre código sorteo oposiciones Madrid RT 0253/2021](https://www.consejodetransparencia.es/ct_Home/dam/jcr:ddb7deb0-76fd-46cd-bf14-3b14f3f3dcc7/RT_0253_2021.pdf)  

También cita [RT 0058/2021 sobre cálculo pensiones](https://www.consejodetransparencia.es/ct_Home/dam/jcr:445a5ac9-c09a-4163-bf72-2a407aec15e4/R-0058-2021-bis.pdf)  

**13 febrero 2022**

Presento escrito usando https://registrounicociudadanos.jccm.es/registrounicociudadanos/acceso.do?id=SJLZ

Lo dirijo la consejería en general aunque la resolución la emitió la secretaría general. 

Nº Registro 551529  
Fecha de registro 13/02/2022 19:18:11

**14 febrero 2022**

Recibo correo de transparencia.educacion@jccm.es

---

En relación con su solicitud de 13/02/2022, cuya copia se adjunta, tal y como se ha informado asimismo al Consejo de Transparencia y Buen Gobierno mediante correos electrónicos enviados a la dirección de este organismo con fecha 10-1-22 y 21-1-22, su solicitud inicial que motivó la reclamación ante el CTBG ya había sido resuelta estimatoriamente y notificada con fecha de 16 de septiembre de 2021, por lo que entendemos que la Resolución de dicha reclamación CTBG RT 748/2021 ya ha sido plenamente ejecutada con anterioridad a que esta fuera dictada.

Le adjunto justificante del envío de la resolución, así como la documentación que ya le fue remitida en su momento acompañando a dicha resolución.

---

Respondo

---

Tras leer el correo recibido, intento aclarar la situación:

-1/07/2021 planteo solicitud SAIP/21/180200/000031 con registro 2274589/2021  
-2/08/2021 recibo comunicación de ampliación de plazo  
-6/09/2021 superada la ampliación y ante el silencio, planteo reclamación RT0748/2021 a CTBG  
-16/09/2021 recibo, fuera de plazo, resolución que Castilla-La Mancha considera estimatoria a SAIP/21/180200/000031 con adjuntos
Esa resolución es estimatoria según la interpretación de Castilla-La Mancha, ya que no facilita el código fuente.El mismo día 16/09/2021 remito la información a CTBG confirmando que no desisto de mi reclamación. Adjunto correo e incluyo el texto del mismo"Adjunto correo y adjuntos recibidos hoy.Dan parte del código fuente (SQL), pero en otra parte hacen una reelaboración y elaboran una descripción (pseudocódigo) sin dar el código fuente del algoritmo.  
Quiero dejar claro que no desisto: creo que si han podido dar código fuente de parte lo pueden dar de todo: tal y como indiqué en mi reclamación "facilitar el código supondría transparencia que facilitaría analizar e interpretar la información compleja, mientras que una "interpretación" de lo que se supone que debe hacer el algoritmo de admisión no garantizaría que el código que se está ejecutado implementa esa interpretación."  
-11/01/2022 recibo resolución estimatoria RT0748/2022 en la que se indica facilitar "Copia del código fuente asociado al algoritmo de la aplicación informática utilizada en el proceso de admisión de alumnado de curso 2021-2022 en Castilla-La Mancha."  
-13/02/2022 realizo requerimiento a Castilla-La Mancha-14/02/2022 recibo este correo al que responde en el que Castilla-La Mancha, citando lo ya recibido el 16/09/2021 indica "entendemos que la Resolución de dicha reclamación CTBG RT 748/2021 ya ha sido plenamente ejecutada con anterioridad a que esta fuera dictada."  
Pongo en copia a CTBG para aclarar que en mi opinión, tal y como ya manifesté en mi respuesta el 16/09/2021, no se ha facilitado la información solicitada, que el código fuente, sino un pseudocódigo. Enlaza con el FJ5 de RT0748/2021:"Mientras no se instauren otros mecanismos que permitan alcanzar los fines señalados con garantías equivalentes –como podrían ser, por ejemplo, auditorías independientes u órganos de supervisión–, el único recurso eficaz a tales efectos es el acceso al algoritmo propiamente dicho, a su código, para su fiscalización, tanto por quienes se puedan sentir perjudicados por sus resultados como por la ciudadanía en general, en aras de la observancia de principios éticos y de justicia."

---

### Transparencia algoritmo días desempleo

Recuerdo este tema en 2022 y lo rescato. Lo tengo escrito en mayúsculas porque formulario indicaba presentar en mayúsculas. Retoco porque citaba un documento con CSV que ya no se puede recuperar en 2022.

---

EL 18/07/2016 PRESENTÉ EN LA OFICINA DE EMPLEO DE AZUQUECA "PARTE DE CONSULTAS E INCIDENCIAS" CON TEXTO:

"EN AGOSTO 2015 ESTABA RECIBIENDO PRESTACIÓN, Y FUI CONTRATADO DÍA 01/09/2015, PERO EL DÍA 31/08/2015 NO ESTÁ COTIZADO. SÉ QUE A NIVEL DE PAGO LOS MESES SON DE 30 DÍAS Y NO ESTOY PIDIENDO COBRAR MÁS, ESTOY PIDIENDO QUE EL DÍA 31/08/2015 ESTÉ COTIZADO, EN AGOSTO DE 2014 SÍ LO COTICÉ, SIMPLEMENTE PORQUE FUI CONTRATADO MÁS TARDE DEL 1 DE SEPTIEMBRE; SÍ SE PUEDE COTIZAR EL DÍA 31 DE AGOSTO, Y NO VALE QUE "LA APLICACIÓN NO LO PERMITE"; DEBE RELLENAR DÍA 31 SI EL ALTA ES POSTERIOR A DÍA 31"

HE RECIBIDO RESPUESTA DE LA DIRECCIÓN PROVINCIAL CON FECHA EL 27/07/2016 EN LA QUE:

- SE ME INDICA "EL 2014 SE LE ABONARON PRESTACIONES DESDE EL 25/07/2014 AL 07/09/2014, ABONÁNDOLE EL MES DE AGOSTO 30 DÍAS Y COTIZANDO 30 DÍAS"

ESA INFORMACIÓN NO ES CIERTA: EN DOCUMENTO VIDA LABORAL ELABORADO POR LA SEGURIDAD SE INDICA SITUACIÓN DE ALTA A 31.08.2014: DÍA 31 ESTABA DE ALTA Y SÍ FUE COTIZADO.

-SE ME CITA ERRONEAMENTE RDL 80/2015; ES RDL 8/2015

EN ESTA NORMATIVA, SE INDICA EN EL ARTÍCULO 166.1

https://www.boe.es/buscar/act.php?id=BOE-A-2015-11724#a166

"...la situación legal de desempleo total durante la que el trabajador perciba prestación por dicha contingencia será asimilada a la de alta."

EL DÍA 31 DE AGOSTO YO ERA DESEMPLEADO Y DEBO ESTAR DADO DE ALTA, CREO QUE SE ESTÁ CONFUNDIENDO EL HECHO DE QUE EL IMPORTE DE LA PRESTACIÓN SE CALCULE DIARIAMENTE Y SE MULTIPLIQUE POR 30 SEA CUAL SEA LA DURACIÓN DEL MES CON EL HECHO DE QUE UNA PERSONA ESTÁ DESEMPLEADA HASTA EL DÍA INMEDIATAMENTE ANTERIOR A SU CONTRATACIÓN.

----

Planteo texto solicitud

---

Solicito copia del código fuente asociado al algoritmo de la cálculo de días cotizados / fecha de último día cotizado utilizado por el SEPE cuando finaliza una prestación por desempleo. Solicito las modificaciones que ese código que haya podido tener desde el año 2014.

El motivo es que si una persona está en situación de desempleo y es contratado por ejemplo día 1/9, el día 31/08 no está cotizado. Sé que a nivel de pago de prestación por desempleo los meses son de 30 días; lo que pido es el código para comprobar que hace que el día 31/8 esté cotizado. En mi caso puedo documentar ejemplos de días 31/8 cotizados como desempleado si la contratación fue 8/9 y días 31/8 no cotizados si la contratación fue 1/9. En consultas en oficinas del SEPE se me ha indicado "la aplicación no lo permite"

En artículo 166.1 RDL 8/2015 https://www.boe.es/buscar/act.php?id=BOE-A-2015-11724#a166

"...la situación legal de desempleo total durante la que el trabajador perciba prestación por dicha contingencia será asimilada a la de alta."

Si el día 31/8 se está desempleado se debe estar dado de alta; creo que la implementación del algoritmo está confundiendo el hecho que el importe de la prestación se calcule diariamente y se multiplique por 30 sea cual sea la duración del mes con el hecho de que una persona está desempleada hasta el día inmediatamente anterior a su contratación.

---

**9 marzo 2022**

Recibo [resolución estimatoria](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/algoquedaquedecir/Algoritmos/2022-SEPE/2022-03-08-16-%2065322%20Resoluci%C3%B3n%20concesi%C3%B3n%20-%20FIRMADO.pdf), pero no dan código

> Por tanto, la realización de la cotización por un periodo de 30 días viene derivado del propio funcionamiento del Sistema de Liquidación Directa implantado por la TGSS, no existiendo un código o algoritmo que realice el ajuste a 30 días como tal, sino que la unidad competente, al realizar las cotizaciones mediante las aplicaciones corporativas encargadas de dicha tarea, se limita al seguimiento de las reglas establecidas al efecto.

Vamos, que dicen que las **aplicaciones** corporativas siguen reglas sin que exista un código o algoritmo 

Reclamaré a CTBG, aunque por la respuesta les va a explotar la cabeza al ver que alguien les pide el código de las "aplicaciones corporativas encargadas de dicha tarea"

Sobre el fondo de lo que yo les argumenté y ellos responden, ya sé y les dije que el pago siempre es por 30 días, pero entiendo que dicen que los días 31 de mes estando en desempleo uno está de alta pero no cotiza, y no me cuadra (se puede ver el texto de mi solicitud). Si hay algún experto por aquí y puede aclarar encantado de entenderlo y aprender.

Ellos dicen que se muestra una cosa pero es otra !?  
> Por todo ello, aunque **aparentemente** en la información que se pueda mostrar sobre este aspecto en algunos meses **estén cotizados 31 días**, realmente **la cotización** y el pago **se realiza por 30 días**.

Citan Artículo 265. Real Decreto Legislativo 8/2015

https://www.boe.es/buscar/act.php?id=BOE-A-2015-11724#a265

que a su vez cita Artículo 273 

https://www.boe.es/buscar/act.php?id=BOE-A-2015-11724#a273

que a su vez cita Artículo 270

https://www.boe.es/buscar/act.php?id=BOE-A-2015-11724#a270

Citan Artículo 26 Real Decreto 625/1985

https://www.boe.es/buscar/act.php?id=BOE-A-1985-8124#art26

Quizá se interprete artículo 265 _Abono de la aportación de la empresa correspondiente a las cotizaciones a la Seguridad Social **durante** la percepción de las prestaciones por desempleo_ como que si se abonan 30 días se cotizan 30 días, pero es que al indicarse temporalmente **durante** yo veo que son los días que haya, no los días pagados.

Citan un manual de 2019 Reglas para determinar el control de bases con ajuste mensual

https://www.seg-social.es/wps/wcm/connect/wss/1bee7b8d-88cd-4d18-b9a1-188c91b3fd62/Ajuste+mensual+control+bases092019.pdf?MOD=AJPERES

 que a mi me asusta solo de verlo

Facilitar este manual lo veo similar al caso de CLM  donde facilitaron un pseudocódigo y no el código, y en RT_0748/2021 se estimó que deben facilitar el código. 

**13 marzo 2022**

Planteo reclamación CTBG

---

La resolución indica que se concede el acceso pero no se facilita el código, ya que se indica "...viene derivado del propio funcionamiento del Sistema ...implantado..., no existiendo un código o algoritmo ...sino que ... mediante las aplicaciones corporativas encargadas de dicha tarea, se limita al seguimiento de las reglas establecidas al efecto."

La afirmación de que se usan aplicaciones corporativas pero no existe un código o algoritmo no tiene sentido: ejecutar una aplicación informática implica ejecutar código. En caso de ciertas aplicaciones comerciales el código ejecuta un algoritmo a través configuración de las mismas.

La resolución se remite a facilitar unas reglas e indicar que las aplicaciones informáticas las siguen. Procede citar aquí RT0748/2021, que cita otros ejemplos sobre transparencia de código como RT 0253/2021 y RT 0058/2021.

"Mientras no se instauren otros mecanismos que permitan alcanzar los fines señalados con garantías equivalentes –como podrían ser, por ejemplo, auditorías independientes u órganos de supervisión–, el único recurso eficaz a tales efectos es el acceso al algoritmo propiamente dicho, a su código, para su fiscalización, tanto por quienes se puedan sentir perjudicados por sus resultados como por la ciudadanía en general, en aras de la observancia de principios éticos y de justicia."   

En solicitud se argumenta sobre artículo 166.1 Real Decreto Legislativo 8/2015 interpretando que alta implica cotizar, y respuesta remite a un documento de reglas de la TGSS, indicando que por ellas "aunque aparentemente en la información que se pueda mostrar sobre este aspecto en algunos meses estén cotizados 31 días, realmente la cotización y el pago se realiza por 30 días." No se indica normativa en la que se indique que alta no implica cotización, y viendo que en informe de vida laboral aparecen columnas con la palabra "ALTA" y una única columna "DÍAS", considero que la transparencia sobre lo que hace realmente la aplicación y lo que aparentemente pueda interpretarse es de interés.

Por último citar que, dado que la resolución cita "Sistema de Liquidación Directa", que la propia web indica "Los objetivos prioritarios del Sistema de Liquidación Directa son: ... mejorar la transparencia..."

https://www.seg-social.es/wps/portal/wss/internet/InformacionUtil/5300/2837

También se cita transparencia en la presentación general del proyecto

https://www.seg-social.es/descarga/es/188979

---

Lo presento vía sede electrónica CTBG.

100-006550 

**11 enero 2023**

[Resolución estimatoria CTBG R007-2023](https://www.consejodetransparencia.es/ct_Home/dam/jcr:67ea34b9-1e41-4a64-8f61-636ff6b62bff/R_007-23.pdf)

S/REF: 001-065322  
N/REF: R-0240-2022; 100-006550 \[Expte. 129/2022]

III. RESOLUCIÓN  
En atención a los antecedentes y fundamentos jurídicos escritos, procede  
PRIMERO: ESTIMAR la reclamación presentada por D. ENRIQUE GARCÍA SIMÓN frente a la resolución el M MINISTERIO DE TRABAJO Y ECONOMÍA SOCIAL/SEPE.  
SEGUNDO: INSTAR al MINISTERIO DE TRABAJO Y ECONOMÍA SOCIAL/SEPE a que, en el plazo máximo e 20 días hábiles, emita al reclamante la siguiente información:  
copia el código fuente asociado al algoritmo de cálculo de días cotizados, en los términos previstos en el fundamento jurídico 6 de esta resolución.

**27 enero 2023**

Recibo [ejecución](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Algoritmos/2022-SEPE/2023-01-25-NOT_744037_RESOLUCION_EJECUCI%C3%93N_65322.pdf), incumplen al dar pseudocódigo y no código. 

### Reparto tribunales

**7 noviembre 2022**

https://comunidad.civio.es/t/regulados-por-programas-y-algoritmos-que-permanecen-en-secreto-cuentanos-tu-caso/76/46?u=fiquipedia

Me ha surgido una idea de petición similar, de algo muy simple pero creo muy trascendente: la posible existencia de algoritmo/código asociado al reparto “al azar” entre juzgados, como puede ser que este asunto lo haya tratado un juzgado en concreto

[ Acuerdo de 8 de marzo de 2018, de la Comisión Permanente del Consejo General del Poder Judicial, por el que se publica el Acuerdo de la Sala de Gobierno del Tribunal Superior de Justicia de Madrid, relativo al texto refundido de las normas de reparto entre los juzgados de lo Contencioso-Administrativo del partido judicial de Madrid. ](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2018-3971)

> Segunda. Publicidad de las normas de reparto.  
> 1. Las normas de reparto son públicas. Cualquier interesado puede solicitar y obtener copia de las mismas si así lo solicitase mediante escrito dirigido al Juzgado Decano.  
> 2. Los magistrados y secretarios judiciales podrán estar presentes en la práctica de las **operaciones de reparto** de asuntos y solicitar comprobaciones de los libros y **asientos informáticos** del mismo.  
> 3. Los interesados podrán solicitar comprobaciones del resultado del reparto mediante escrito dirigido al Juzgado Decano.

## Software libre

**5 julio 2023**

[twitter JaimeObregon/status/1676523670045425664](https://twitter.com/JaimeObregon/status/1676523670045425664)  
Algunos llevamos dos décadas reivindicando algo de sentido común:  
📢 Public money, public code.  
https://publiccode.eu/es  
Esto significa que el «software» desarrollado con financiación pública debería ser… ¡público! 🎉  
Y algunos gobiernos europeos ya han comenzado a entenderlo.   
![](https://pbs.twimg.com/media/F0QlYbuXoAAKzA5?format=jpg)  

Motivaciones para el código público

Ahorro de impuestos

Aplicaciones similares no tienen que ser programadas desde cero una y otra vez.

Colaboración

Los esfuerzos en grandes proyectos se pueden compartir con lo que se gana experiencia y se reducen costos.

Accesible a todos

Las aplicaciones pagadas por los ciudadanos deben estar disponibles para todos.

Estimular la innovación

Con procesos transparentes los otros no tienen que reinventar la rueda.

El Software Libre da a todos el derecho a usar, estudiar, compartir y mejorar el software. En estos derechos se fundan otras libertades fundamentales como la libertad de expresión, de prensa y el derecho a la privacidad.

### Otras referencias

**28 febrero 2017**

[¿Cómo Auditar un Algoritmo? - Jorge García Herero](https://jorgegarciaherrero.com/auditar-algoritmo/)  
La plena transparencia puede no ser suficiente, ni posible  
Como ya anticipamos en el anterior post, la transparencia plena (la publicación completa del código fuente de un algoritmo) no es ni tiene por qué ser suficiente para acreditar la justicia de premisas o la regularidad de funcionamiento del sistema: hay muchos factores relevantes que no son controlables así.  
Pero además siempre nos encontraremos con elementos de knowhow que los responsables del algoritmo no estarán dispuestos a mostrar públicamente. Y ello para evitar su copia por terceros, y la pérdida de la ventaja competitiva de su producto. Lo que viene siendo la lógica y necesaria protección de un secreto empresarial. Un tema que no es baladí. 

**30 septiembre 2020**

[Algoritmos: villanos o héroes de nuestro tiempo - newtral](https://www.newtral.es/algoritmos-transparencia-sesgos-que-es/20200930/)  
Los algoritmos nos rodean y en general nos hacen la vida más fácil. Pero las decisiones que toman algunos pueden tener consecuencias problemáticas, sobre todo cuando tienen sesgos y no son transparentes o auditables.

**7 febrero 2021**

[Sesgos de género en los algoritmos: un círculo perverso de discriminación en línea y en la vida real - eldiario](https://www.eldiario.es/tecnologia/sesgos-genero-algoritmos-circulo-perverso-discriminacion-linea-vida-real_129_7198975.html)  
La “condición algorítmica” está alterando los derechos humanos, que prohíben la discriminación por razón de sexo o género. La industria tecnológica no está haciendo lo suficiente para abordar estos sesgos. El problema no es solo que están ocultos, sino también que la mayoría de las empresas que utilizan algoritmos similares ni siquiera quieren saber que incurren en ellos

**8 noviembre 2021**

[twitter htejero_/status/1457631363453136898](https://twitter.com/htejero_/status/1457631363453136898)  
El pasado viernes, @MasPais_Es registramos una enmienda a los Presupuestos Generales del Estado para pedir una Agencia Estatal de Auditoría de Algoritmos. Voy a intentar explicar en este hilo (laargo) el sentido de la propuesta:  

**22 noviembre 2021**

[La política parlamentaria comienza a tomar en serio las violencias de la inteligencia artificial y el big data - elsaltodiario](https://www.elsaltodiario.com/inteligencia-artificial/politica-parlamentaria-comienza-tomar-serio-riesgos-ia-big-data
)  
Unidas Podemos y Más País inician el camino hacia una agencia estatal de algoritmos que controle el uso de los mismos por parte de las instituciones públicas y empresas. Las voces técnicas y de la academia lo ven con buenos ojos, pero indican que en la mayoría de los casos no es suficiente

**13 diciembre 2021**

[La educación como arma frente a la incertidumbre que causan los algoritmos - publico](https://www.publico.es/sociedad/libros-educar-comprender-algoritmos-poderosos-no-quieren-ciudadanos-criticos.html)  
La sociedad necesita educarse en los nuevos códigos invisibles, pero es muy complejo hacer que la gente tenga conciencia de qué está ocurriendo con sus vidas, guiadas de un lado a otro por un lenguaje que no comprende; formar una ciudadania crítica (y libre) es, precisamente, lo que plantean los autores de una nueva obra.


'El algoritmo de la incertidumbre' (Gedisa, 2021) se suma a un extenso cuerpo de libros y ensayos que tratan de alertar sobre las consecuencias de vivir en un mundo en el que las grandes empresas tecnológicas han impuesto sus códigos a campos como la política, la educación y la cultura.

**16 enero 2023**

[Alfabetización algorítmica o la necesidad de repartir el poder - elpais](https://elpais.com/tecnologia/2023-01-16/alfabetizacion-algoritmica-o-la-necesidad-de-repartir-el-poder.html)  
La inteligencia artificial está en todas partes y entender los algoritmos será lo que nos permita seguir siendo libres

