+++
author = "Enrique García"
title = "Reclamar a inspección educativa"
date = "2024-01-31"
tags = [
    "normativa", "Madrid", "educación", "comisiones de servicio"
]
toc = true

+++

Revisado 27 junio 2024

## Resumen

Una de las vías de reclamación es inspección educativa: intento reflejar cómo se reclama y ejemplos.  

Ver [!Reclamad, malditos!](https://algoquedaquedecir.blogspot.com/2018/07/reclamad-malditos.html)  

## Detalle

La primera idea es que hay dos inspecciones:  
* Inspección de la administración de las CCAA que tienen transferida la educación  
* Alta inspección del ministerio de educación  

A la hora de reclamar a una de ellas es importante tener claro sus competencias para ver si van a poder actuar sobre el tema reclamado

### Alta inspección

Ley Orgánica 2/2006, de 3 de mayo, de Educación. 

[Artículo 6 bis. Distribución de competencias.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a6bis)  
> 1. Corresponde al Gobierno:  
> ...  
> e) La alta inspección y demás facultades que, conforme al artículo 149.1.30.ª de la Constitución, le corresponden para garantizar el cumplimiento de las obligaciones de los poderes públicos.  

[TÍTULO VII Inspección del sistema educativo CAPÍTULO I Alta Inspección](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#ci-7)  


[Artículo 150. Competencias.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a150)  
> 1. En el ejercicio de las funciones que están atribuidas al Estado, corresponde a la Alta Inspección:  
> a) Comprobar el cumplimiento de los requisitos establecidos por el Estado en la ordenación general del sistema educativo en cuanto a modalidades, etapas, ciclos y especialidades de enseñanza, así como en cuanto al número de cursos que en cada caso corresponda.  
> b) Comprobar la inclusión de los aspectos básicos del currículo dentro de los currículos respectivos y que éstos se cursan de acuerdo con el ordenamiento estatal correspondiente.  
> c) Comprobar el cumplimiento de las condiciones para la obtención de los títulos correspondientes y de los efectos académicos o profesionales de los mismos.  
> d) Velar por el cumplimiento de las condiciones básicas que garanticen la igualdad de todos los españoles en el ejercicio de sus derechos y deberes en materia de educación, así como de sus derechos lingüísticos, de acuerdo con las disposiciones aplicables.  
> e) Verificar la adecuación de la concesión de las subvenciones y becas financiadas con cargo a los Presupuestos Generales del Estado, a los criterios generales que establezcan las disposiciones del Estado.  
> 2. En el ejercicio de las funciones de alta inspección, los funcionarios del Estado gozarán de la consideración de autoridad pública a todos los efectos, pudiendo recabar en sus actuaciones la colaboración necesaria de las autoridades del Estado y de las Comunidades Autónomas para el cumplimiento de las funciones que les están encomendadas.  
> 3. El Gobierno regulará la organización y régimen de personal de la Alta Inspección, así como su dependencia. Asimismo, el Gobierno, consultadas las Comunidades Autónomas, regulará los procedimientos de actuación de la Alta Inspección.

La misma información aparece en la web  
[Inspección del sistema educativo - educacionyfp.gob.es](https://educagob.educacionyfp.gob.es/sistema-educativo/inspeccion-sistema-educativo.html)  

### Inspección de Madrid

En reclamación respondida por inspección en enero 2024 me citan [Artículo 3 de ORDEN 732/2021 DE LA CONSEJERÍA DE EDUCACIÓN Y JUVENTUD POR LA QUE SE DESARROLLA EL DECRETO 61/2019, DE 9 DE JULIO, DEL CONSEJO DE GOBIERNO POR EL QUE SE REGULA LA ORGANIZACIÓN, ESTRUCTURA Y FUNCIONAMIENTO DE LA INSPECCIÓN EDUCATIVA DE LA COMUNIDAD DE MADRID](https://www.comunidad.madrid/transparencia/sites/default/files/orden_732_2021_desarrollo_reglam_in.pdf)  

Marco algunas ideas que asocio a no cumplir normativa  

> Artículo 3. Funciones, atribuciones y actuaciones de la Inspección Educativa.  
> 1. Las funciones de la Inspección Educativa de la Comunidad de Madrid son las siguientes:  
> a) **Supervisar**, evaluar y **controlar**, desde el punto de vista pedagógico y organizativo, el funcionamiento de los centros educativos, así como los proyectos y **programas** que desarrollen, con respeto a su marco de autonomía.  
> ...  
> d) Velar por el **cumplimiento, en los centros educativos, de las leyes, reglamentos y demás disposiciones vigentes** que afecten al sistema educativo.  
> e) Velar por el **cumplimiento y aplicación de los principios y valores recogidos en las leyes**, incluidos los destinados a fomentar la igualdad real entre hombres y mujeres.  
> ...  
> g) **Emitir los informes** solicitados por las Administraciones educativas respectivas **o que se deriven del conocimiento de la realidad propio de la inspección educativa**, a través de los cauces reglamentarios.  
> ...
> 2. Para cumplir con estas funciones, la Inspección Educativa de la Comunidad de Madrid tendrá las siguientes atribuciones:  
> ...  
> e) **Elevar informes y hacer requerimientos cuando se detecten incumplimientos en la aplicación de la normativa**, y levantar actas, ya sea por iniciativa propia o a instancias de la autoridad administrativa correspondiente.  


### Dónde reclamar

Madrid: [Formulario solicitud genérica. Presentación de escritos y comunicaciones. Formulario genérico](https://sede.comunidad.madrid/prestacion-social/formulario-solicitud-generica/tramitar)  

Alta inspección: por correo [Sede 1. Alta Inspección de Educación en Madrid](https://mpt.gob.es/delegaciones_gobierno/delegaciones/madrid/servicios/educacion.html#Madrid.Sede1)  

### Ejemplos de reclamaciones

* Reclamación sobre JH Newman en 2022, ver [Admisión de alumnado](https://algoquedaquedecir.blogspot.com/2021/10/admision-de-alumnado.html)  
* Reclamación sobre normativa que incumple normativa básica en 2024, ver [Finalidad evaluación para clasificación centros](../finalidad-evaluacion-para-clasificacion-centros)  
* Reclamación sobre comisiones de servicio en 2024, ver [Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia](../comisiones-de-servicio-madrid-iv-programas-en-centros-bachillerato-de-excelencia)



