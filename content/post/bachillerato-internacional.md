+++
author = "Enrique García"
title = "Bachillerato internacional"
date = "2021-10-11"
tags = [
    "educación", "privatización", "Acceso Universidad", "post migrado desde blogger", "Centros docentes extranjeros"
]
toc = true

description = "Bachillerato internacional"
thumbnail = "https://pbs.twimg.com/media/FdupnmhXwAANyCP?format=png"
images = ["https://pbs.twimg.com/media/FdupnmhXwAANyCP?format=png"]

+++

[Comentario]:<>  (Imagen tomada de https://twitter.com/JavierRujasMN/status/1575039198704259072)

Revisado 27 septiembre 2024

## Resumen

Intento recopilar información para informar e informarme sobre el Bachillerato Internacional.

Resumiendo mucho, se puede ver el BI como un centro "extranjero y privado" al que se le está dejando operar dentro de centros públicos pagándole con dinero público, y creo que enlaza con segregación y con privatización. 

Posts relacionados:  
[Asignación gastos funcionamiento centros púbicos](../post/asignacion-gastos-funcionamiento-centros-publicos)

## Detalle

Es un tema que había pasado por mi TL de Twitter varias veces, y este post lo escribo al salir de nuevo

[twitter mammacansina/status/1592548803114303489](https://twitter.com/mammacansina/status/1592548803114303489)  
@FiQuiPedia, @ProfaDeQuimica, claustro virtual: en breve votaremos la implementación del bachillerato internacional en mi centro. Todo son loas y parabienes. ¿Es realmente IBO una entidad sin ánimo de lucro?¿Esto es un negocio de la admón. o un programa ilusionante? Ambos?  

Algunas veces anteriores que había salido por mi TL, pongo inicialmente en orden cronológico inverso, y cuando se citen artículos o informes los pongo por separado en orden cronológico directo, y según volumen puedo crear apartados separados  

**7 septiembre 2022**

[twitter FiQuiPedia/status/1567874235548270596](https://twitter.com/FiQuiPedia/status/1567874235548270596)
Programa de los Años Intermedios (PAI) = Middle Years Programme (MYP)  
Docentes de centros públicos cobrando complementos pagados con dinero público por un programa de una entidad privada.	  

[Programa de los Años Intermedios - ibo.org](https://ibo.org/es/programmes/middle-years-programme/)

**16 agosto 2022**

[twitterEnriqueJDiez/status/1559518699400400902](https://twitter.com/EnriqueJDiez/status/1559518699400400902)  
Análisis del Bachillerato Internacional en @diarioeduca  
📝"El negocio escandaloso y segregador del #BachilleratoInternacional"   

Ver artículo 2018

Una persona responde defendiéndolo

[twitter Rafamaravi/status/1560254057595428864](https://twitter.com/Rafamaravi/status/1560254057595428864)  
Me parece que otra vez viene la Universidad a arreglar los problemas que tiene la educación secundaria, de una manera un tanto sesgada.  
Si interesa mi punto de vista, abro hilo 👇  
1 Estoy de acuerdo con el problema de falta de financiación de la educación , pero la ratio 5 alumnos, no tengo noticia de que sea posible. Tal vez en Latín, cosa que se disculpa, si queremos mantener vivas las lenguas clásicas.  
Lo habitual en el BI en centros públicos, son 25 alumnos, que son demasiados para este tipo de educación.  
Solución: bajar ratios para todos  
Menos ratio= más calidad y más financiación.  
Pero las ratios en BI deben ser bajas, porque atiendes y guías al alumnado en una serie de trabajos, monografías, prácticas e investigación.  
Más de 20 alumnos/as es volver al sistema actual de querer atender a costa d tiempo personal  
Una vez que separamos, financiación y atención “a unos pocos alumnos”.  
Seguimos:  
La organización del BI, no es propiamente una empresa privada, es una ONG, no tiene dueños ni accionistas que ganen dinero por beneficios.
Tiene gastos y trabajadores que cobran, como cobramos ...Cobramos todos los que trabajamos. Tal vez una parte del profesorado trabaje más de lo que legalmente sea exigible. (Recuerdo el tema de financiación)  
Los gastos hay que pagarlos, y es mejor que sean los poderes públicos quienes lo hagan. En su defecto las familias si hay becas. Becas para los que no pueden pagarlo. Ya se beca por estudiar en un bachillerato normal privado o concertado.  
El coste en un centro público es de 500-700€, en un privado de muchos miles.
Mejor que sea público.  
El coste es por materiales, personal, prácticas y exámenes.👇  
Y los corrige alguien, que debe cobrar.  
No hay coste “por marca, ni franquicia” son los costes y el control para que nadie aproveche el prestigio de otros.  
Quien quiera entrar debe pasar un control de calidad y ajustarse a un sistema metodológico, parecido a la LOMLOE, más avanzado, pero más realista, el BI exige formación para el profesorado e inversión en instalaciones.  
El sistema de acceso a la Universidad es mejorable y se les debería dar ventajas, pero es la Universidad quien controla la EVAU y los plazos de matrícula y no los centros de secundaria, que cada vez tenemos menos días para preparar un examen memorístico de EVAU, diferente al BI.  
Por tanto los alumnos de centros públicos de BI tienen que hacer un esfuerzo extra.  
Hay que dar información a familias y alumnado para evitar decepciones por ...  
este esfuerzo extra.  
No veo que sea un negocio financiado por la administración.  
Es preferible que los centros públicos oferten estas enseñanzas que hasta hace poco, sólo estaban al alcance de los más pudientes.  
Es un buen programa, pero no hay fórmulas mágicas en educación 👇  
Sobre todo la calidad de la educación va a depender del profesorado, de su calidad profesional, sus condiciones de trabajo y motivación.  
Reconozcamos la labor que se ha hecho en estos años tan duros.  
Este programa es voluntario para profesores y alumnos,  demos facilidades a ...  
a los que se implican en estos programas.  
Recomiendo ojear otras informaciones:  

Y aporta artículo de 2021 que habla de "falsos mitos"

También comenta otra persona que indica es corrector

[twitter Sergio_Mira/status/1560304218912821249](https://twitter.com/Sergio_Mira/status/1560304218912821249)  
La corrección en el BI es exhaustiva. Pasas una prueba cada año para poder corregir y cada examen se corrige tres veces y por varias personas. Y los correctores cobran (cobramos) muy bien, lejos de lo que se ingresa por presentarte voluntario a corregir EBAU.  

[twitter FiQuiPedia/status/1559568713107611648](https://twitter.com/FiQuiPedia/status/1559568713107611648)  
No es lo mismo.  
-El Bachillerato Internacional lo lleva una empresa externa, http://ibo.org  
-El Bachillerato de excelencia es un programa de Madrid que tiene un único centro de excelencia, el IES San Mateo, y otros centros con aulas de excelencia  

**14 agosto 2022**

[twitter Sin_N0mbre_/status/1558698680357715969](https://twitter.com/Sin_N0mbre_/status/1558698680357715969)  
@FiQuiPedia  Hola, ¿tienes info sobre lo q le cuesta anual/ a la Comunidad de Madrid (nos cuesta a lxs madrileñxs) la acreditación de centros públicos y privados concertados para impartir el Bachillerato Internacional y su posterior desarrollo una vez obtenida la acreditación?  
¿Datos sobre cuántxs alumnxs se han matriculado en BI por centro, desde su implantación, número máx. de alumnxs por aula, requisitos académicos para cursarlo, etc ? Es por empezar a pedir información a la Consejería de Educación. Gracias.  

No tengo información, solo conozco lo que se publica de forma activa. No sé si el pago con dinero público a IBO aplica también a privados con concierto.  

La lista de centros se puede ver aquí  
[find an IB school. Spain](https://www.ibo.org/programmes/find-an-ib-school/?SearchFields.Region=&SearchFields.Country=ES&SearchFields.Keywords=&SearchFields.Language=&SearchFields.BoardingFacilities=&SearchFields.SchoolGender=)

**2021**  
[Los alumnos de Bachillerato Internacional culminan este mes su particular selectividad - elpais](https://elpais.com/educacion/2021-05-22/los-alumnos-de-bachillerato-internacional-culminan-este-mes-su-particular-selectividad.html)  
> Esta especialidad, con casi 4.000 estudiantes en España, basa el aprendizaje en la indagación.  
**Ha ganado notoriedad porque es la que cursará el año que viene la princesa de Asturias**  

En este artículo se cita la idea de acceso a la universidad desde Bachillerato Internacional, que comento por separado.

**2022**  
[La Comunidad de Madrid extiende el Bachillerato Internacional a dos nuevos institutos públicos ](https://www.comunidad.madrid/notas-prensa/2022/02/11/comunidad-madrid-extiende-bachillerato-internacional-dos-nuevos-institutos-publicos)  

**23 mayo 2021**

[twitter FiQuiPedia/status/1396398186135265282](https://twitter.com/FiQuiPedia/status/1396398186135265282)  
IES San Mateo es Bachillerato Excelencia, no Internacional.  
Un caso de Bachillerato Internacional implantado 2020-21  
[IES Miguel Catalan - Bachillerato Internacional](https://site.educa.madrid.org/ies.miguelcatalan.coslada/index.php/bachilleratointernacional/)  
Ese centro, público, también sale aquí 

[IES Miguel Catalán. Escuelas changemakers Ashoka](https://spain.ashoka.org/portfolio-items/ies-miguel-catalan/)

Aparte de lo anterior intento recopilar información

**28 septiembre 2022**

[twitter JavierRujasMN/status/1575034330253996034](https://twitter.com/JavierRujasMN/status/1575034330253996034)  

[The International Baccalaureate in Madrid: A silent expansion of internationalization](https://epaa.asu.edu/index.php/epaa/article/view/7179)  
> the IB Diploma seems to expand more intensely in decentralized educational systems where neoliberal and neoconservative policies promote autonomy, competition, internationalization and “excellence”, as is the case in Madrid.

[twitter JavierRujasMN/status/1575039198704259072](https://twitter.com/JavierRujasMN/status/1575039198704259072)  
Los procesos de internacionalización de la educación (IE) se han intensificado en las últimas décadas en el mundo, y también en España. Aunque el BI es uno de los programas más antiguos (1968), en Madrid se ha expandido muy fuertemente en la segunda década del siglo XXI.  
![](https://pbs.twimg.com/media/FdupnmhXwAANyCP?format=png)  

[twitter Sin_N0mbre_/status/1559232720433209345](https://twitter.com/Sin_N0mbre_/status/1559232720433209345)  
Bueno, Sres., como prácticamente llevo 2 días hablando sola, voy a aprovechar el tirón para escribir el ¿último? hilo sobre el #BachilleratoInternacional  
A ver, por dónde empiezo ... #educación  

La cuenta de Twitter en España es
[SpainIB](https://twitter.com/SpainIB)  
aunque indica   
> Información útil para la comunidad del Bachillerato Internacional. Cuenta personal Antonio Muñoz: IB World Schools Senior Manager

### Gasto público en un programa educativo privado en centros públicos

[CUOTA ANUAL BACHILLERATO INTERNACIONAL CURSO 2020-21 (IBO)](https://contratos-publicos.comunidad.madrid/contrato/4024232)  
Tipo de publicacion: Contratos Menores  
Número expediente: 28062023-449/2020  
Importe adjudicación (con IVA) 8.300,00  

[PAGO TASAS EXAMEN ALUMNOS BACH. INTERNACIONAL (IBO)](https://contratos-publicos.comunidad.madrid/contrato/4024233)  
Tipo de publicacion: Contratos Menores  
Número expediente: 28062023-504/2020  
Importe adjudicación (con IVA) 12.240,00  

![](https://pbs.twimg.com/media/FaG-UprXwAAZx5O?format=jpg)  

![](https://pbs.twimg.com/media/FaG-U0QXgAIY6SA?format=jpg)  

[ORDEN de 26 de agosto de 2022, de la Consejería de Economía, Hacienda y Empleo, por la que se modifica la Orden de 18 de febrero de 2022 de la Consejería de Economía, Hacienda y Empleo, por la que se establecen criterios objetivos para la asignación de productividad a los funcionarios de Cuerpos Docentes no Universitarios, por la participación en programas de enseñanza bilingüe, de innovación educativa y que impliquen especial dedicación al centro.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/09/07/BOCM-20220907-3.PDF)

Donde aparece esto

> Profesor de centro público de Educación Secundaria que desempeña la función de coordinador del Programa de los Años Intermedios  
241,20 €  
Profesor de centro público de Educación Secundaria que desempeña simultáneamente la función de coordinador del Programa de los Años
Intermedios y de coordinador del Programa del Bachillerato Internacional  
283,30 €  

[Presupuestos Madrid 2024. Educación, Ciencia y Universidades](http://www.madrid.org/presupuestos/index.php/presupuestos-generales/presupuestos-cm/2024/educacion-ciencia-universidades-2024)

[Presupuestos Madrid 2024. Libros](http://www.madrid.org/presupuestos/index.php/presupuestos-generales/publicaciones-2024)  

[Presupuestos Madrid 2024. Libro Consejería Educación, Ciencia y Universidades](http://www.madrid.org/presupuestos/attachments/category/62/2024-presupuesto-libro-10-educacion.pdf#page=66) 

> 1.- CONSOLIDAR LOS SERVICIOS Y ENSEÑANZAS PRESTADOS POR LA RED DE CENTROS PÚBLICOS DE
EDUCACIÓN SECUNDARIA Y FORMACIÓN PROFESIONAL.  
El Programa Diploma del Bachillerato Internacional de la Oficina del Bachillerato Internacional (OBI),
tiene como objetivo formar alumnos que logren una excelente amplitud y profun didad en sus
conocimientos, al tiempo que crezcan física, intelectual, emocional y éticamente . En los cursos
precedentes se han ampliado los centros donde se oferta esta enseñanza que progresivamente se va
incorporando a la impartición de las enseñanzas.  
Se mantendrá el “Programa de los Años Intermedios” del Bachillerato Internacional (OBI) que es un
marco académico riguroso que anima a los alumnos de 11 a 16 años a establecer conexiones
prácticas entre sus estudios y el mundo real, en la etapa de la ESO. Estas enseñanzas se imparten en
los centros: IES Julio Caro Baroja, de Fuenlabrada, IES San Isidro de Madrid, IES Gerardo Diego de
Pozuelo de Alarcón, IES Complutense de Alcalá de Henares y el IES Rosa Chacel de Colmenar Viejo.

En 2023 el proyecto de presupuestos (el documento se eliminó al no aprobarse), citaba  
> Implantación del “Programa de los Años Intermedios” del Bachillerato Internacional (OBI) en el IES San Isidro de Madrid Capital, IES Gerardo Diego de Pozuelo de Alarcón e IES Complutense de Alcalá de Henares y la continuidad en el Programa del IES Rosa Chacel de Colmenar Viejo que ya lo implantó de manera experimental el curso 2021-2022.

**26 septiembre 2024**

Recibo [resolución donde aparece el detalle de gasto en Bachillerato Internacional](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/CosteEducaci%C3%B3n/Asignaci%C3%B3nCentrosP%C3%BAblicos/2024-09-26-ResolucionAcceso-09-OPEN-00159.6-2024.pdf)  

10 centros 8715 € asociado IBO  
4 centros 7140 € asociado PAI  
2 centros 16233 € asociado IBO / PAI  

Total 148176 € anuales

### Gestión del programa educativo privado en centros públicos

Cómo funciona la formación y selección de docentes, las correcciones, las reclamaciones de alumnos  

[twitter Sin_N0mbre_/status/1559457959851196417](https://twitter.com/Sin_N0mbre_/status/1559457959851196417)  
Si se os ocurre investigar sobre el Bachillerato Internacional, aviso: váis a encontrar sacos y sacos de mierda. Una de esas mierdas se refiere a lxs docentes:  
\1. Para ser profe de BI, tendrás que hacer los talleres de formación de la Universidad Camilo José Cela, y no sé si el máster  
...  
Como la Comunidad educativa siga cooperando con esta marranada, sea de forma activa o pasiva, desaparecerá la docencia como servicio público.  
...  
He sido coordinadora de un centro desde la solicitud para ser centro solicitante (🤦🏻‍♀️x2) así que he hecho dos cursos, el de coordinación y el de mi asignatura. Ambos han sido online (mayo y junio de 2021), y ambos directamente con la organización, no con la Camilo José Cela.  
...  
Si un profesor está interesado en hacerlo por su cuenta, se lo paga él/ella. Si se gestiona a través del instituto, cada centro se organiza como quiere. Los centros (públicos) que yo conozco se lo suelen repercutir a las familias de los alumnos participantes (…)  

[twitter Sin_N0mbre_/status/1559461904615198720](https://twitter.com/Sin_N0mbre_/status/1559461904615198720)  
Repito 📢📢 Las inspecciones educativas no pueden inspeccionar nada que tenga que ver con el #BachilleratoInternacional Cualquier reclamación, a la entidad privada que lo gestiona, y no directamente: a través del Coordinador del BI del IES.




### El acceso a la universidad desde sistemas educativos extranjeros

Es algo relevante que surge como comentario asociado a Bachillerato Internacional.  
Poder acceder a universidad sin EvAU es un agujero relevante: se pude ver el BI como un centro extranjero y privado que a veces está dentro de un centro público. Realmente es un título extranjero de una entidad privada.  

**2021**  
[Los alumnos de Bachillerato Internacional culminan este mes su particular selectividad - elpais](https://elpais.com/educacion/2021-05-22/los-alumnos-de-bachillerato-internacional-culminan-este-mes-su-particular-selectividad.html)  
> Los exámenes de mayo son los únicos que los alumnos realizan en los dos años que dura el Programa Diploma. Con su calificación pueden entrar de manera directa a algunas de las universidades más prestigiosas del mundo, como Harvard, Oxford o Cambridge. **Para acceder a las españolas pueden hacerlo a través de la vía de estudios extranjeros o realizar la EVAU** -hay centros que preparan también para esta opción-.

En julio 2022 se intentó modificar un RD (aparte de otro que podía cambiar ratios) que cambiaba más cosas y afectaba a ese agujero  

[Proyecto Real Decreto sobre régimen de centros docentes extranjeros en España](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2022/prd-centros-docentes-extranjeros.html)  


[Proyecto de Real Decreto sobre régimen de Centros docentes extranjeros en España - munozabogadoseducacion](https://munozabogadoseducacion.es/proyecto-de-real-decreto-sobre-regimen-de-centros-docentes-extranjeros-en-espana/)  

[El Gobierno obligará a los colegios extranjeros a impartir más horas de clase en las "lenguas nacionales" - el mundo](https://www.elmundo.es/espana/2022/07/18/62d1888e21efa04d3c8b45a7.html)  
Educación cambiará la normativa para que las escuelas internacionales adapten horarios e instalaciones a los estándares españoles, con más castellano, catalán, gallego o vasco

La presión de los centros privados lo paralizó

[El Gobierno recula con las nuevas normas contra los colegios extranjeros](https://www.elmundo.es/espana/2022/07/21/62d9a313fc6c83a6358b458c.html)  

[El Ministerio de Educación retira los proyectos de Real Decreto de requisitos mínimos y de centros extranjeros](https://educacionprivada.org/noticia/2022-07-22/36951)  

**14 febrero 2024**

Se inicia consulta pública 

[Proyecto de real decreto por el que se establece la normativa básica que regula el acceso y la admisión a la universidad del alumnado en posesión de un título, diploma o estudio equivalente al título de Bachiller, obtenido o realizado en los sistemas educativos extranjeros contemplados en la disposición adicional trigésima sexta de la Ley Orgánica 2/2006, de 3 de mayo, de Educación](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/consulta-publica-previa/abiertos/2024/prd-alumnado-extranjero.html)  


### Artículos

En Madrid se puede mirar [Buscador Comunidad de Madrid "bachillerato internacional"](https://www.comunidad.madrid/buscar?cadena=bachillerato+internacional)

**13 febrero 2018**

[El negocio escandaloso y segregador del Bachillerato Internacional - eldiariodelaeducacion](https://eldiariodelaeducacion.com/2018/02/13/negocio-escandaloso-segregador-del-bachillerato-internacional/)  
El Bachillerato Internacional, título de una empresa privada, está siendo sufragado en buena medida por las administraciones, a pesar de los grandes recortes en la pública.

**24 enero 2020**

[Ampliamos la oferta de Bachillerato Internacional a cuatro nuevos centros](https://www.comunidad.madrid/noticias/2020/01/24/ampliamos-oferta-bachillerato-internacional-cuatro-nuevos-centros)

**18 febrero 2021**

[Los falsos mitos sobre el Bachillerato Internacional - elpais](https://elpais.com/educacion/2021-02-18/los-falsos-mitos-sobre-el-bachillerato-internacional.html)  
A pesar de tratarse de un sistema educativo muy prestigioso y con más de cincuenta años de recorrido, a menudo existen una serie de ideas previas erróneas, como que solo se da en inglés

**28 febrero 2021**

[Ponemos en marcha el Bachillerato Internacional en dos nuevos institutos públicos](https://www.comunidad.madrid/noticias/2021/02/28/ponemos-marcha-bachillerato-internacional-dos-nuevos-institutos-publicos)

**22 mayo 2021**

[Los alumnos de Bachillerato Internacional culminan este mes su particular selectividad](https://elpais.com/educacion/2021-05-22/los-alumnos-de-bachillerato-internacional-culminan-este-mes-su-particular-selectividad.html)  
Esta especialidad, con casi 4.000 estudiantes en España, basa el aprendizaje en la indagación. Ha ganado notoriedad porque es la que cursará el año que viene la princesa de Asturias

**5 agosto 2021** 

[Así es el Bachillerato Internacional, un modelo que prioriza el pensamiento crítico y la educación personalizada - elpais](https://elpais.com/economia/formacion/2021-08-05/asi-es-el-bachillerato-internacional-un-modelo-que-prioriza-el-pensamiento-critico-y-la-educacion-personalizada.html)  
Los estudiantes que se gradúan dentro del programa tienen vía de acceso preferente en muchas universidades, y en algunas comunidades están exentos de hacer la EBAU

**11 febrero 2022**

[La Comunidad de Madrid extiende el Bachillerato Internacional a dos nuevos institutos públicos](https://www.comunidad.madrid/notas-prensa/2022/02/11/comunidad-madrid-extiende-bachillerato-internacional-dos-nuevos-institutos-publicos)
