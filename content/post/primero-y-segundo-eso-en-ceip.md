+++
author = "Enrique García"
title = "Primero y segundo ESO en CEIP"
date = "2024-10-27"
tags = [
    "educación", "ESO", "Primaria"
]
toc = true

+++

Revisado 1 marzo 2025

## Resumen

En septiembre 2024 se anuncia en Madrid que 1º y 2º ESO se van a impartir en los colegios.  
Aquí se intentan poner información y detalles (el anuncio inicial es poco concreto) y normativa asociada, incluyendo el trámite de audiencia cuando se produzca.  
Es algo que enlaza con el anuncio de tener jornada partida (que aplicaría a los 1º y 2º ESO en CEIP), pero no es lo mismo.  

Ver [Jornada escolar](../post/jornada-escolar) 

## Detalle

### Resumen cronológico desde septiembre 2024

**12 septiembre 2024**  
[Díaz Ayuso anuncia que los nuevos colegios públicos también impartirán 1º y 2º de ESO y tendrán jornada partida - comunidad.madrid](https://www.comunidad.madrid/noticias/2024/09/12/diaz-ayuso-anuncia-nuevos-colegios-publicos-tambien-impartiran-1o-2o-eso-tendran-jornada-partida)  
> La presidenta de la Comunidad de Madrid, Isabel Díaz Ayuso, ha anunciado hoy que todos los nuevos colegios públicos de Educación Infantil y Primaria que se construyan en la región también impartirán 1º y 2º de Educación Secundaria Obligatoria (ESO) ...

**15 octubre 2024**  
[La Comunidad de Madrid incorporará desde el próximo año los cursos de 1º y 2º de Secundaria a colegios públicos de Infantil y Primaria - comunidad.madrid](https://www.comunidad.madrid/noticias/2024/10/15/comunidad-madrid-incorporara-proximo-ano-cursos-1o-2o-secundaria-colegios-publicos-infantil-primaria)  

[Los colegios que ya están funcionando podrán impartir el 'modelo EGB': de primaria hasta 2º de la ESO en el centro - elespanol](https://www.elespanol.com/madrid/comunidad/20241015/colegios-funcionando-podran-impartir-modelo-egb-primaria-centro/893661043_0.html)  

**17 octubre 2024**  
[El plan de Ayuso de llevar los primeros cursos de la ESO a las escuelas va contra la ley - eldiario](https://www.eldiario.es/sociedad/plan-ayuso-llevar-primeros-cursos-escuelas-ley_1_11741716.html)  

[UGT amenaza con ir a los tribunales si finalmente incorporan 1º y 2º de ESO en los colegios públicos de Infantil y Primaria de Madrid - eleconomista](https://www.eleconomista.es/actualidad/noticias/13038642/10/24/ugt-amenaza-con-ir-a-los-tribunales-si-finalmente-incorporan-1-y-2-de-eso-en-los-colegios-publicos-de-infantil-y-primaria-de-madrid.html)  


**18 octubre 2024**  
[Madrid rescata el modelo de EGB para que los alumnos sigan en su colegio en 1º y 2º de ESO - 20minutos](https://www.20minutos.es/noticia/5644076/0/madrid-rescata-el-modelo-de-egb-para-que-los-alumnos-sigan-en-su-colegio-en-1-y-2-de-eso/)  

**23 octubre 2024**  
[¿Alumnos de 1º y 2º de la ESO en colegios de Primaria? Los padres dicen sí, los sindicatos no - elpais](https://elpais.com/espana/madrid/2024-10-23/alumnos-de-1-y-2-de-la-eso-en-colegios-de-primaria-los-padres-dicen-si-los-sindicatos-no.html)  
> CC OO, UGT, CSIF y ANPE critican la opacidad en la implementación de la medida y dudan de su legalidad, mientras las asociaciones de padres y el Colegio Oficial de Docentes de Madrid no lo ven con malos ojos

[Vuelta del alumnado de 1º y 2º de la ESO a los colegios: CCOO alerta de la presión a los equipos directivos, de inseguridad jurídica y posible ilegalidad de la medida](https://feccoo-madrid.org/noticia:707802--Vuelta_del_alumnado_de_1_y_2_de_la_ESO_a_los_colegios_CCOO_alerta_de_la_presion_a_los_equipos_directivos_de_inseguridad_juridica_y_posible_ilegalidad_de_la_medida&opc_id=9f666eca08d47ba26c6a09927d7abbbb)

**25 octubre 2024**  
[1º y 2º de la ESO de vuelta en los colegios: ¿Qué puede salir mal? - magisnet](https://www.magisnet.com/2024/10/1o-y-2o-de-la-eso-de-vuelta-en-los-colegios-que-puede-salir-mal/)  
> Según el consejero, Emilio Viciana, la medida «no ofrece ninguna complicación» para el profesorado, ya que en la región existen centros integrados desde hace años –los llamados CEIPSO, colegio público de Educación Infantil, Primaria y Secundaria–. El cambio consistiría, según Viciana, en que en el centro pasaría a haber un jefe de estudios de Primaria y otro de Secundaria. Las materias de Primaria las seguirían impartiendo profesores de Primaria y las de Secundaria, docentes de Secundaria.  

**26 octubre 2024**  
Comento unas ideas a partir del artículo de 25 octubre en Magisnet  
[twitter FiQuiPedia/status/1850088334632112201](https://x.com/FiQuiPedia/status/1850088334632112201)  
>Si es línea 3, en 1 y 2ESO son 9 h de FQ (si hablamos de otras materias...).
@EVicianaDuro asume:  
- Compartir centros  
- Impartir materias fuera de cuerpo/ especialidad/forzar ámbitos   
El [artículo 13.2 de RD 132/2010](https://boe.es/buscar/act.php?id=BOE-A-2010-4132#a13) es básico según [Disposición final primera](https://boe.es/buscar/act.php?id=BOE-A-2010-4132#dfprimera) 
No puede llevarse 1 y 2ESO a un CEIP (ver requisitos art14).  
Son expertos en retorcer: querrán hacer CEIPSOs uniendo administrativamente edificios CEIP e IES  
Creo Madrid optará por la unión administrativa de centros creando "un centro" CEIPSO con lo que antes eran centros y edificios separados  
Por arte de magia no desplazan, nadie itinera ni comparte centro, y los profes de 1 y 2 ESO tienen jornada partida

Me comentan que enlaza con los [institut escola](https://educacio.gencat.cat/ca/arees-actuacio/centres-serveis-educatius/centres/tipus-centres/segons-ensenyament/institut-escola/) de Cataluña  

**30 octubre 2024**  
[Los 25 colegios que se suman a la 'EGB' de Ayuso: Madrid adaptará sus instalaciones para acoger 1º y 2º de la ESO - elespanol](https://www.elespanol.com/madrid/comunidad/20241030/colegios-suman-egb-ayuso-madrid-adaptara-instalaciones-acoger/897160717_0.html)

**18 noviembre 2024**  
[Sumar 1º y 2º de la ESO a un colegio puede ser su salvación: “Aumentará la matrícula y el centro no morirá” - elpais](https://elpais.com/espana/madrid/2024-11-18/sumar-1-y-2-de-la-eso-a-un-colegio-puede-ser-su-salvacion-como-en-leganes-aumentara-la-matricula-y-el-centro-no-morira.html)   
El Antonio Machado de Leganés, que pasó de tener 450 alumnos a 260 en seis años, es uno de los 25 centros de la Comunidad de Madrid en los que se probará esta medida el curso que viene  

**19 noviembre 2024**

[twitter EVicianaDuro/status/1858819499916411236](https://x.com/EVicianaDuro/status/1858819499916411236)  
Hoy el Consejo Escolar de la @ComunidadMadrid celebra una jornada sobre horario escolar y CEIPSO, con familias, docentes y expertos.  
Apostamos por impartir 1º y 2º de ESO y recuperar la jornada partida en colegios públicos para proteger la infancia y la preadolescencia.

[La jornada escolar a debate. JORNADA ESCOLAR Y CEIPSO: SITUACION EL INTERES DEL MENOR EN EL CENTRO DEL DEBATE - comunidad.madrid](https://www.comunidad.madrid/noticias/2024/11/19/jornada-escolar-debate)  

[Jornada escolar y CEIPSO: un debate en torno al interés superior del menor - Eventos Consejo Escolar/Jornada escolar](https://www.educa2.madrid.org/web/eventos-consejo-escolar/jornada-escolar)  


Fecha interna de un documento pdf con el siguiente contenido, recibido más tarde

> A la atención del Consejero de Educación de la Comunidad de Madrid, Emilio Viciana Duro, a
las direcciones de las cinco áreas territoriales de educación, y al resto de la comunidad
educativa:  
Respecto a la implantación de 1º y 2º ESO en los centros de primaria para el curso que viene, a
las maestras y profesoras de la Comunidad de Madrid, que también somos madres, padres y
familias de alumnado, nos surgen muchas dudas ya que en la mayoría de los colegios se está
efectuando este proceso sin informar NADA ni preguntar a las familias, de manera totalmente
opaca.  
La verdad es que nos hubiera gustado que hubiesen establecido una vía de comunicación e
información ante un paso tan importante y determinante en la vida escolar de nuestros hijos.
Suponemos que es una demanda que tendremos que trasladar al Consejo Escolar de Estado y al
Ministerio de Educación, FP y Deportes. Nos preocupan los que se van, pero también los que se
quedan, así que vamos a tratar de ser concisas en nuestras inquietudes, si bien son muchas. Las
remitiremos también a las asociaciones de AFAs, ya que efectivamente, son uno de los cauces
de conexión entre familias y centros educativos.  
Esperamos nuestras dudas tengan una respuesta que seguro podrá tranquilizar esta impresión
de que nos hemos lanzado a una piscina sin saber si hay agua dentro:  
>1. GESTIÓN DE HORARIOS:  
En un centro bilingüe de secundaria los alumnos cursan 32 periodos de 55 minutos
semanales, más recreo/s y comedor (porque según se refiere en todos los medios, la idea es
que sea jornada partida). Por tanto no terminamos de ver cómo eso va a encajar con los 30
periodos de 45 minutos de primaria. ¿Van a tener el mismo horario primaria y secundaria?
¿Van a compartir patio? ¿Compartirán comedor? ¿Va a haber timbres desfasados toda la
mañana? Vemos inviable que entren y salgan a la misma hora, lo cual dificulta la conciliación
de aquellas familias con hijos en ambas etapas.  
>2. INSTALACIONES:  
¿Se va a dotar de laboratorios y otras dependencias como talleres de tecnología? ¿Habrá
espacios suficientes para compartir con 100 niños más? ¿Cómo van a gestionarse las obras?
¿Y la compartición de los espacios disponibles (gimnasio, aula de música…) con 100 niños
más? Esta es la preocupación que menos nos inquieta.  
>3. PROFESORADO:  
Nadie explica quién va a gestionar esos horarios compartidos con un instituto porque
asumimos que por ley les tendrán que dar clase profesores de instituto y que al no haber
horas suficientes se hará compartido. ¿Es legal obligar a un docente de secundaria a hacer
una jornada partida? En los CEIPSOS, que tanto gustan en comparar, el horario de primaria y
secundaria es independiente y secundaria no tiene turno partido. ¿Cómo se va a hacer aquí?
Como sabrá, el nuevo acuerdo de interinos deja a estos la libertad de elegir si se postulan o
no a un puesto. Estos puestos compartidos a jornada partida no van a resultar nada
atractivos y no serán elegidos. ¿Qué pasa si no se cubren esas plazas como YA está pasando
en numerosos centros educativos que están sin profesores desde comienzo de curso y sin
suplir las bajas? ¿Qué profesorado va a "aceptar" esos puestos? ¿Los últimos de las listas? ¿Va
a repercutir en la calidad educativa que reciban nuestros hijos?
>4. PROFESORADO:  
¿Va a dar cada materia un especialista o se van a dar por materias afines para simplificar la
gestión? ¿El de un ámbito va a dar todas? (ciencias, sociolingüístico) ¿Va a ir endetrimento de
la calidad educativa? ¿Se van a hacer ámbitos? Sabe que en ese caso (ámbitos) no se pueden
impartir en inglés. ¿Se va a perder el bilingüismo si la opción elegida son los ámbitos? ¿Cómo
se va a gestionar compartir 10/12 profesores con un instituto? ¿Han tratado este tema con la
asociación de directores de centros educativos? ¿Se ha pedido su opinión? ¿Qué opinan?
Además, ¿cómo sería posible que un profesor de primaria dirija un equipo de profesionales
con mayor nivel de estudios? ¿No sería eso intrusismo profesional? ¿Recibirán entonces
formación específica sobre lo que es la ESO los docentes de primaria?  
>5. BILINGÜISMO:  
Si hay ámbitos no hay bilingüismo. Si no hay ámbitos y sí bilingüismo. ¿Qué va a pasar con los
niños que no alcancen en 6º de primaria la competencia necesaria para cursar 1º de ESO en
inglés (entre 0 y 4 por CEIP el curso pasado en los centros de la CAM)? ¿Se van a tener que ir
de su colegio? ¿Los niños posiblemente más vulnerables y a los que mejor les vendría esa
medida?  
>6. BILINGÜISMO:  
Si va a haber programa y sección (lo dudamos). ¿Se van a poder gestionar los desdobles de
las materias en inglés?  
>7. OPTATIVAS:  
¿Qué optativas se van a ofertar? ¿Francés? ¿Computación? Ambas son de oferta obligada, pero
ya sabemos en centros donde la dirección ha dicho que no se va a impartir… ¿Habrá solo una
optativa o van a poder elegir entre nueva oferta? Si no se va a impartir francés. ¿Qué pasará
con esos chicos que quieren ir a francés en 3º?  
Además, ¿cómo se van a gestionar los desdobles? Porque no van a elegir 25 alumnos cada
materia optativa, sobre todo si se ofertan más materias.  
¿Y que pasa si por tener un número reducido de alumnos en comparación con un instituto
línea 3 o línea 4 o más no sale francés con ese número de alumnos? Creemos que con la
importancia que da la CAM a los idiomas esto es una debacle.  
>8. HORARIOS:  
¿Cómo se van a gestionar los 32 periodos lectivos? ¿Van a salir algunos días antes y otros
después? ¿Qué pasa con el comedor? ¿Cuál va a ser el horario?  
>9. DISCIPLINA:  
Es cierto que la adolescencia tiene sus problemas, y en nuestro contexto social algunos
pueden asustarnos, pero los institutos no somos un pozo de ignominia. De acuerdo con el
informe sobre el acoso escolar de la propia Comunidad de Madrid, los cursos donde más
acoso hay son 4º y 5º de primaria, disminuyendo en la ESO y Bachillerato. Dicho esto,
cualquier persona que trabaje en un instituto sabe bien que la inmensa mayoría de los
problemas de disciplina los generan los alumnos de 1º y 2º de ESO. Problemas que se van a
trasladar al colegio (porque no, en los institutos no hay otras normas ni menos control).
Móviles, váper, acoso escolar...dependiendo de las zonas de la Comunidad de Madrid, hay
más o menos problemas… pero en los institutos cuentan con amplia experiencia
gestionándolos. ¿Está el colegio preparado? ¿Han pensado en que los más pequeños van a
tener esos problemas en SU colegio? Y no me digan que eso no va a pasar, ni que nadie va a
dar móviles hasta tercero, ni que se va a retrasar la adolescencia, porque la adolescencia
llega a cada uno cuando le tiene que llegar y no es un problema si se sabe gestionar. ¿Está el
colegio preparado?  
>10. COMEDOR:  
Por nuestra experiencia la inmensa mayoría de los problemas de convivencia se dan en el
recreo de comedor. ¿Cómo lo va a gestionar el comedor? ¿Está preparado para problemas
mayores de los que ya se tienen y gestionan de manera muy mejorable? ¿Alguien ha pensado
que en 2º de ESO puede haber un alumno de 16 si repite dos veces? ¿Con niños de 6, 7, 8, 9 o
10 años? Que encima ahora serán LOS MAYORES. Si dan problemas en los institutos siendo los
pequeños. ¿Qué va a pasar ahora?  
>11. ADSCRIPCIÓN:  
¿Perderán los alumnos de primaria su adscripción a su instituto de referencia para 1º y 2º
ESO? Obviamente se la darán a otros centros que no implanten la medida el curso próximo.
De eso no nos cabe duda. ¿Podrían confirmarlo? ¿Qué pasa con los niños con hermanos en el
instituto que quieran cursar 1º ESO en el IES del barrio? ¿Esas familias no tienen derecho a
conciliar? Al perder la adscripción esos y cualquier alumno que quiera ir al instituto irá POR
DETRÁS DE TODOS LOS DEMÁS ADSCRITOS independientemente de sus puntos. ¿Qué pasa
con la libertad de elección de los padresque no quieran o no les venga bien esto?
Además, ¿cómo pasarán los alumnos a tercero, en bloque a un instituto o desperdigados por
las plazas sobrantes en el resto de centros ya ofertando hasta cuarto…? esto no es bueno
para el centro que acoge o para el alumno que termina renunciando a su grupo de referencia
en uno u otro caso.  
>12. TRANSICIÓN:  
Cuando los alumnos pasen a tercero en un centro con alumnos ya consolidados y adaptados.
¿Acaso no van a notar el cambio? Al menos en 1º son todos nuevos, se construyen nuevas
amistades y en un mes están tan felices y tranquilos. Los miedos están en los padres más que
en los niños. Los mayores del instituto NO molestan a los pequeños, más bien les cuidan o
ignoran (por supuesto salvo excepciones).  
>13. CONSEJO ESCOLAR  
Debido al incremento de alumnado, de profesorado de diferentes niveles y familias ¿Han
pensado como quedaría compuesto el Consejo escolar? ¿Cómo quedarían representados
docentes, alumnos y familias?  
>14. LEGALIDAD:  
El RD 132/2010 en su artículo 13. 2 dice:  
“En los centros de educación secundaria que ofrezcan las enseñanzas de educación secundaria
obligatoria se deberán impartir los cuatro cursos de que consta esta etapa educativa con
sujeción a la ordenación académica en vigor. Dichos centros deberán tener, como mínimo, una
unidad para cada curso y disponer de las instalaciones y condiciones materiales recogidas en el
artículo siguiente.”  
¿No contraviene esta medida este RD 132/2010 por partir la etapa de la ESO?  
Agradecemos mucho la atención que nos ha prestado y esperamos una pronta y satisfactoria
respuesta a todas las preguntas que le formulamos.  
Atentamente.  
Docentes anónimos, madres y padres de alumnado de la educación pública madrileña,
fuertemente preocupados por esta medida impuesta sin estudio ni consenso.  

**20 noviembre 2024**

[Consejo Escolar de Madrid: sesión sobre la jornada escolar Y los CEIPSO - feuso](https://feuso.es/comunidad-de-madrid/noticias/19111-consejo-escolar-de-madrid-sesion-sobre-la-jornada-escolar-y-los-ceipso)  
> Finalmente, se expusieron los pros y contras sobre la incorporación de los dos primeros cursos de la ESO a los colegios públicos. En concreto, la Comunidad de Madrid tiene la intención de reinstaurar la jornada partida en todos los colegios públicos y mantener a los alumnos de los primeros cursos de secundaria en los colegios. Se argumenta que esto busca el bienestar del menor, considerándolo menos vulnerable a estas edades en los colegios donde han cursado la primaria.

**13 diciembre 2024**  
[RESOLUCIÓN de 22 de noviembre de 2024, de la Viceconsejería de Política y Organización Educativa, por la que se dictan instrucciones sobre la participación en el proceso de admisión de alumnos en centros docentes sostenidos con fondos públicos que imparten segundo ciclo de Educación Infantil, Educación Primaria, Educación Secundaria Obligatoria, Bachillerato, así como las etapas de Educación Especial en la Comunidad de Madrid para el curso 2025-2026](https://www.comunidad.madrid/sites/default/files/bocm-20241213-26.pdf)  

> ANEXO IV   
CALENDARIO DE ACTUACIONES PARA EL PROCESO ORDINARIO DE ADMISIÓN DE
ALUMNOS PARA EL CURSO 2025/2026   
> Los centros con adscripción única a otros centros verifican que han
informado en el Sistema Integral de Gestión Educativa Raíces a
todos los alumnos que finalizan sus enseñanzas en el centro.  
Hasta el 21 de febrero de 2025

**19 diciembre 2024**  
[Primero y Segundo de la ESO en los CEIP - eldiariodelaeducacion](https://eldiariodelaeducacion.com/2024/12/19/primero-y-segundo-de-la-eso-en-los-ceip/)  

**23 diciembre 2024**  
[Emilio Viciana, consejero de Educación: "Hay acuerdo entre CCAA del PP para contenidos comunes en selectividad - elespanol](https://www.elespanol.com/madrid/comunidad/20241223/emilio-viciana-consejero-educacion-acuerdo-ccaa-pp-contenidos-comunes-selectividad/909659157_0.html)  
> P: Respecto a la jornada partida en los colegios y el modelo EGB, ¿tienen cifras sobre cuántos centros podrían adoptar estas medidas?  
R: Estas iniciativas, aunque complementarias, requieren diferentes abordajes. En el caso de primero y segundo de la ESO, comenzamos con la idea de implementarlo en nuevos colegios pero viendo la buena acogida lo queremos extender siempre que sea posible. Calculamos que al menos 25 centros podrán adaptarse a este modelo para el curso 2025-2026.  

**3 febrero 2025**  
[Díaz Ayuso anuncia que el próximo curso más de medio centenar de colegios públicos comenzarán a impartir Educación Secundaria  - comunidad.madrid](https://www.comunidad.madrid/noticias/2025/02/03/diaz-ayuso-anuncia-proximo-curso-medio-centenar-colegios-publicos-comenzaran-impartir-educacion-secundaria)  
> La presidenta de la Comunidad de Madrid ha anunciado hoy que 52 colegios públicos de la región comenzarán el próximo curso a impartir Educación Secundaria. Esta pionera medida, ha señalado, “mejora la convivencia en los centros, protege a la infancia, la aleja del mundo de las drogas, las bandas y las nuevas adiciones”. A su juicio, también ayuda a las familias también a conciliar y estar más tranquilas porque saben que están en un lugar seguro y un entorno mucho más protegido.  

> En todos los casos la petición tenía que venir avalada por el Consejo Escolar, en el que están representados el equipo directivo, maestros, personal de administración y servicios, padres y responsables municipales. Y también acompañada por un plan de espacios que confirmara que sus instalaciones podían acoger los nuevos cursos con adaptaciones o pequeñas obras.  
Los técnicos de la Consejería han verificado que los 52 centros cumplen estos requisitos, por lo que ya se les ha comunicado que se convertirán en CEIPSO (Colegio de Educación Infantil, Primaria y Secundaria Obligatoria) y acogerán dos cursos de Secundaria, empezando por 1º en 2025/26.

[La Comunidad de Madrid amplía la lista de colegios que incluirán 1º y 2º de la ESO a partir del siguiente curso y serán 52 - elpais](https://elpais.com/espana/madrid/2025-02-03/la-comunidad-de-madrid-amplia-la-lista-de-colegios-que-incluiran-1-y-2-de-la-eso-a-partir-del-siguiente-curso-y-seran-52.html)  

> Los centros incluidos en la medida para el próximo septiembre se han conocido este lunes. CC OO denuncia que muchos de ellos no estaban enterados del proceso

[Ayuso dificulta la elección de instituto a los alumnos de los nuevos centros que acogen ESO - elsaltodiario](https://www.elsaltodiario.com/comunidad-madrid/comunidad-madrid-dificulta-eleccion-instituto-alumnos-nuevos-centros-acogen-eso)  

[Ayuso recula y mantendrá la adscripción a institutos del alumnado que proviene de colegios con Secundaria - elsaltodiario](https://www.elsaltodiario.com/comunidad-madrid/ayuso-recula-mantendra-adscripcion-institutos-del-alumnado-proviene-centros-secundaria)

> En menos de 24 horas, la presión de las familias, unido a lo publicado por El Salto y El País, ha conseguido que la Consejería de Educación del Gobierno de la Comunidad de Madrid, presidido por Isabel Díaz Ayuso, recule. Después de que transcendiera que el alumnado que proviene de los nuevos centros CEIPSO, colegios que antes eran de Infantil y Primaria y ahora también albergarán 1º y 2º de la ESO, perdían la adscripción a los institutos que hasta ahora eran de referencia, desde la Federación de familias FAPA Giner de los Ríos han explicado a El Salto que la consejería se ha comprometido verbalmente, tras reunirse con ellos por la tarde, a vigilar “centro a centro”, manteniendo las adscripciones.

**4 febrero 2025**  
[Reunión en la sede de la FAPA con el consejero de Educación - fapaginerdelosrios.org](https://www.fapaginerdelosrios.org/publicaciones/noticias/reunion-en-la-sede-de-la-fapa-con-el-consejero-de-educacion)  

> En los temas abordados en la reunión cabe destacar las, de momento, 52 autorizaciones de CEIPSO previstas para el curso 2025/2026 que, como ya se ha anunciado hace tiempo desde la FAPA, se preveía que alcanzarían al menos el medio centenar, como así ha sido; conclusiones a las que la federación llegó después de un intenso y minucioso trabajo basado en datos reales, porque la FAPA no tiene la necesidad de inventar la realidad, ya que la conoce y representa. En la actualidad, superan ampliamente el centenar las peticiones realizadas por los centros desde que se conoció la noticia, una cifra que sin duda seguirá aumentando.  
> Teniendo en cuenta que las autorizaciones de CEIPSO se basan en una normativa publicada que ya existe, estando la que se debe tener en cuenta vigente desde 2015, se ha mediado para ajustar ciertas cuestiones que pudieran causar dificultades a las familias, entre ellas, la desaparición de las adscripciones a los Institutos de Secundaria, acordándose el mantenimiento de éstas a los centros que tienen autorizado su paso a CEIPSO. A este respecto, debemos recordar que la normativa fija las adscripciones en el momento en el que el alumnado debe abandonar un centro de forma obligatoria al terminar en éste los estudios que imparte, por lo que, al pasar a ser CEIPSO, ese momento ya no sería al finalizar la Educación Primaria, sino cuando se termine el último curso que se pueda cursar en ellos. No obstante, se le ha trasladado al consejero de Educación que las familias escolarizaron en esos nuevos CEIPSO con la idea de cambiar de centro al finalizar la Primaria y, aquellas que mantengan esa idea, deben mantener asegurado su derecho a poder hacerlo.  

**10 febrero 2025**  
[Se abre el debate con la ESO en España, ¿tiene sentido empezar dos años más tarde? - cadenaser](https://cadenaser.com/nacional/2025/02/10/tiene-sentido-que-los-alumnos-de-los-primeros-cursos-de-la-eso-sigan-en-el-colegio-y-no-cambien-al-instituto-cadena-ser/)  
> La Comunidad de Madrid quiere implantar este modelo en 52 colegios públicos pero expertos en educación advierten de que romper la etapa de secundaria puede dificultar la detección de problemas y la orientación  

**11 febrero 2025**  
[Directores de institutos piden no incorporar 1º y 2º de la ESO en colegios de Primaria de Madrid - telemadrid](https://www.telemadrid.es/noticias/madrid/Directores-de-institutos-piden-no-incorporar-1-y-2-de-la-ESO-en-colegios-de-Primaria-de-Madrid-0-2751624847--20250211061618.html)  


**13 febrero 2025**  
[El plan de Ayuso para incluir Secundaria en los colegios pende de un hilo legal y enfada a profesores y directores  - eldiario](https://www.eldiario.es/madrid/somos/plan-ayuso-incluir-secundaria-colegios-pende-hilo-legal-enfada-profesores-directores_1_12046505.html)  


**14 febrero 2025**  
[Díaz Ayuso anuncia una inversión de 4 millones para adaptar 52 colegios públicos que impartirán Secundaria el próximo curso - comunidad.madrid](https://www.comunidad.madrid/noticias/2025/02/14/diaz-ayuso-anuncia-inversion-4-millones-adaptar-52-colegios-publicos-impartiran-secundaria-proximo-curso)  

**16 febrero 2025**  
Me llega documento pdf que no cita autoría

>Escribo estas líneas con motivo de la nueva medida de supuesta implantación en
el curso 2025/26, consistente en albergar en los CEIPs los dos primeros cursos
de la Educación Secundaria Obligatoria.  
Si bien puedo entender que esta medida pudiera resultar atractiva para algunas
familias, quizás porque piensen que 11-12 años es una edad muy temprana para
abandonar los CEIPs, algo que se puede someter a debate (ver la sección quinta
de este escrito), pongo en su conocimiento algunos aspectos que hacen que, en
mi humilde opinión, la medida tenga también algunos inconvenientes difíciles
de soslayar.  
>1. Posible infracción del ordenamiento jurídico  
El artículo 13.2 de RD 132/2010 establece que los centros que impartan la
Educación Secundaria Obligatoria habrán de hacerlo de los cuatro cursos.
Además, la disposición final primera de este Real Decreto establece que esta
norma es básica. Este detalle es importante, porque si una norma es básica
(ver artículo 149 de la Constitución Española), mi interpretación es que no
puede ser contravenida por una Ley de una Comunidad Autónoma, como lo
haría esta medida, pues sólo pretende incluir los dos primeros cursos de la
ESO.  
>2. Ausencia de transparencia  
Se preguntarán, si está en contradicción con una norma básica, ¿por qué no ha
sido impugnada la medida? La respuesta es sencilla. La Comunidad de Madrid
no ha publicado hasta el momento ninguna normativa asociada a la misma.
En algunos comunicados a los padres de sexto curso de Educación Primaria
por parte de los Directores de los Centros implicados, dejan entrever que no
hay normativa ni la va a haber, pues los CEIPs se convertirán en CEIPSOs, una
realidad en la región. Por tanto no se trata de un proyecto experimental. Esto
no es verdad. Un CEIPSO alberga los cuatro cursos de la ESO, no dos, así
que los centros que acojan esta medida no serán CEIPSOs.  
Todo el proceso está envuelto de una gran opacidad. Aparte de los
comunicados que realizan a través del portal de la Consejería: [12/09/24],
[15/10/24], [26/12/24], [05/02/25], [14/02/25], o directamente comentarios
fugaces en los medios de comunicación, no hay información pública sobre la
medida. Me consta que algunos de los CEIPs que implementarán la medida lo
aprobaron en el Claustro y lo llevaron al Consejo Escolar, incluyendo una
votación y posteriores reuniones informativas a los padres de sexto. Pero
existen a día de hoy muchos interrogantes sin resolver, y falta muy poco para
las inscripciones en los IES/CEIPs.  
De hecho, no sé si habrá sido así en todos y cada uno de los CEIPs que se
acogen a la medida, pero en algunos me consta que han instado a las familias a
cumplimentar un formulario antes del 21/02/2025 fecha en la que las familias
deben decidir si sus hijos continuarán sus estudios en el CEIP o si se
inscribirán en un IES. La fecha en cuestión viene decidida en la Resolución de
22 de noviembre de 2024. El 21 de febrero, el Equipo Directivo de sus CEIPs
debe comunicar a los IES adscritos la previsión de alumnos que continuarán
sus estudios allí. Supongo que han pedido esto por el revuelo y la rápida
rectificación por parte de la CM con respecto a este particular. Es decir, en un
principio iban a eliminar la adscripción única a los IES. Ahora se supone que la
van a mantener, pero ... ¿solo a aquellos que marquen la casilla IES en el
formulario que os han pedido cumplimentar? No lo sé. Desconozco también si
este ha sido el modo de proceder en todos y cada uno de los CEIPs que se
acogen a esa medida, pero sí ha sido el caso de los que están en mi entorno y
de los que, por tanto, me ha llegado la información.  
>3. Posibles dificultades técnicas de implantación  
Como ya he mencionado en el punto anterior, no estamos ante la creación de
CEIPSOs, tal y como se entienden a día de hoy. Cierto es que los CEIPSOs
gozan de una regulación clara, recogida en la Orden 2968/2014 y la Orden
1017/2015. Pero, como ya aclaré en el punto 2, esta nueva medida no
comporta la creación de CEIPSOs. Es otra cosa. A continuación les enumero
algunos de los los problemas prácticos/técnicos que yo veo relacionados con la
medida. Dejo claro que lo que sigue a continuación es especulación y
opinión:  
• Comparación con la velocidad de implantación de la LOGSE: la
LOGSE se publica en 1990 y termina de implantarse en 2002, año en el que
se extingue COU (FPII se extingue un año después, en realidad). Estamos
hablando de 12-13 años de implantación. Aun así, en el proceso de
transformación I.B. → IES era frecuente ver barracones y un desbarajuste
curioso.  
• Instalaciones: la CM sostiene que se ha realizado un estudio de las
infraestructuras de los centros de primaria que albergarán los dos
primeros cursos de la ESO. Para el curso 2025/26 seguramente aulas habrá
si se trata de colegios pre-LOGSE. Antes había 2 niveles de infantil y 8 de
primaria y ahora hay 3 de infantil y 6 de primaria, así que “sobra” uno, que
habrán destinado a otros menesteres pero que seguramente se pueda
reacondicionar. De acuerdo. Ahora lanzo dos preguntas: ¿qué ocurrirá en el
curso académico 2026/27? Y ¿qué ocurre con las infraestructuras
necesarias para impartir la ESO que dicta el artículo 14 del RD 132/2010?
¿También están garantizadas? Se reducirán espacios que actualmente se
están utilizando: bibliotecas, desdobles, laboratorios...  
• Profesorado: ¿Quién va a impartir las clases? No pueden ser maestros
(salvo los que se acojan a la disposición transitoria primera de la LOE-
LOMLOE, pero estas posiciones son a extinguir). Han de ser Profesores de
Secundaria. Esto está regulado en el artículo 17 del RD 132/2010. Surgen
muchísimos interrogantes al respecto. El concurso de traslados de Cuerpo
de funcionarios públicos de Secundaria ha tenido ya lugar (no se ha
resuelto aún), y los CEIPs que implementarán la medida no eran centros
elegibles. Aún queda el llamado coloquialmente concursillo (donde
participa el funcionariado interino/en prácticas, y funcionarios de carrera
sin destino definitivo) en el que entiendo y espero se ofertarán, y a algunos
profesores se les asignarán estos centros. No puedo asegurarlo. No hay
información sobre ello. Parece que no es un aspecto que preocupe a la
Consejería de Educación. Hay un problema intrínseco adicional a la hora
de elegir el profesorado. Tomemos un CEIP línea 2 que pasa a albergar
1ESO en el curso entrante. Se necesitará un profesor que imparta 8 horas
de Matemáticas (4 por línea). ¿Qué ocurriría con las 12 restantes de las 20
en total que ha de hacer? ¿Se haría una educación por ámbitos en la que
un mismo profesor impartiría no sólo su materia sino también materias
afines? En tal caso, quizás la calidad de la educación se vea resentida.
Alternativamente, se podría pensar que se crearán plazas compartidas, en
las que un profesor se desplazará entre su IES de referencia y el CEIP en
cuestión. Esas plazas son poco atractivas, y pueden resultar difíciles de
cubrir. Por otro lado, ¿qué ocurrirá con el Departamento de
Orientación? La orientadora, PTs y ALs de los CEIPs ¿habrán de atender
más alumnado? ¿se amplía el personal?  
• Convivencia: hay muchos problemas de convivencia en los primeros
cursos de la ESO y la naturaleza de los mismos es con frecuencia un poco
distinta a los que tienen en Primaria. Antes de la implantación de la
LOGSE, los CPs (con 7° y 8°EGB) tenían una amplia experiencia en gestión
de este tipo de problemas. Pero ahora esa experiencia está en los IES.  
• Se ha anunciado que la jornada será partida. ¿Cómo será en concreto?
Las sesiones en Secundaria tienen una duración distinta a las de Primaria.
¿Cómo se gestionará el comedor? Si este aspecto es importante para las
familias, y es un factor de peso a la hora de tomar una decisión, algunos
IES tienen acuerdos con CEIPs y parte del alumnado del IES hace uso del
servicio de comedor.  
• Bilingüismo: ¿Qué modalidad se ofrecerá? ¿Esta característica va a ser
puesta en conocimiento de los padres antes de cumplimentar la matrícula?  
• Optativas: ¿qué optativas se ofertarán? Asumo que la oferta será menor.
• Adscripción: Para este interrogante, ver el punto anterior.  
Notar que en los últimos puntos he realizado muchas especulaciones. Me
parece pertinente hacerlas, no sólo por su interés intrínseco sino también
para subrayar la importancia de tener una normativa que regule una
medida con tantos cambios estructurales.  
>4. Una mirada histórica. Implantación de la LOGSE  
Como ya les he anticipado arriba, la LOGSE fue una ley de implantación lenta.
Fue el cambio estructural más importante del país en materia de Educación de
la segunda mitad del sXX y lo que llevamos del sXXI. Se empezó a preparar a
mediados de los 80s, se discutió durante largo tiempo, contó con numerosos
detractores. Pero se debatió. En este artículo de Marchesi pueden leer un buen
resumen de la historia de la ley. Les animo a que lean la parte relacionada con
la decisión de establecer la Educación Secundaria Obligatoria como el tramo
de los 12 a los 16 años. Fue un aspecto controvertido, pero se recibieron
propuestas, se estudiaron y luego se tomó la decisión. Quizás errónea, pero fue
un proceso sometido a debate, abierto a propuestas y dilatado en el tiempo.  
>5. Tramos de la Educación Secundaria a debate  
Enlazando con el punto anterior, a mí me parecería legítimo pensar que
podríamos reabrir el debate. Quizás la decisión que se tomó no fue la correcta.
Puede ser. Las cuestiones que me planteo son (esto es también opinión):  
• ¿Se puede someter la cuestión a debate? Sí  
• ¿Dónde sería legítimo someterlo a debate? En el Ministerio de
Educación, a la hora de elaborar una Ley Orgánica de Educación, no en el
seno de la Consejería de una Comunidad Autónoma.  
• ¿Ha existido un debate previo y se ha explicado bien a la ciudadanía
el porqué de esta medida? Rotundamente no.  
>6. Artículos de prensa y enlaces que pueden ser de su interés  
• Algo queda que decir: resumen cronológico y exhaustivo de la medida.  
• Magisnet: 1° y 2˚ESO de vuelta a los colegios, ¿qué puede salir mal?  
• Telemadrid: directores de institutos piden no incorporar 1° y 2° de la ESO
en los centros de primaria.  
• Federación de enseñaza de CCOO: CCOO alerta de la presión a los equipos
directivos, de inseguridad jurídica y posible ilegalidad de la medida.  
• El Economista: UGT amenaza con ir a los tribunales si finalmente
incorporan 1° y 2° de ESO en los CEIPs de Madrid.  
• El Diario: El plan de Ayuso para incluir Secundaria en los colegios pende de
un hilo legal y enfada a profesores y directores.  
• FAPA Giner de los Ríos: parece que aquí dicen que la medida se basa en una
normativa publicada que ya existe (la de los CEIPSOs). Esto sería
importante aclararlo.  
NOTA: No soy experta en leyes ni tengo ninguna formación al respecto. Navego
por ellas y voy aprendiendo progresivamente. Puede haber incorrecciones.  

**17 febrero 2025**  

[Institutos y colegios públicos claman contra el proyecto de Ayuso de dividir la ESO](https://cadenaser.com/nacional/2025/02/17/institutos-y-colegios-publicos-claman-contra-el-proyecto-de-ayuso-de-dividir-la-eso-cadena-ser/)  

Las federaciones estatales que agrupan a centros públicos de primaria y secundaria exigen al Ministerio de Educación una regulación específica para evitar un efecto contagio

### Solicitudes transparencia

**3 febrero 2025**  

Solicito copia o enlace, asociada a la información publicada por la Comunidad de Madrid el 3 de febrero en https://www.comunidad.madrid/noticias/2025/02/03/diaz-ayuso-anuncia-proximo-curso-medio-centenar-colegios-publicos-comenzaran-impartir-educacion-secundaria para el centro CEIP Nuestra Señora del Val (Alcalá de Henares)  
- Acta del consejo escolar en la que se avala solicitar impartir 1º y 2º ESO  
- Plan de espacios que confirme que sus instalaciones pueden acoger los nuevos cursos con adaptaciones o pequeñas obras  
- Escrito dirigido a la consejería para solicitar impartir 1º y 2º ESO  
- Documento de los técnicos de la consejería en el que se haya verificado que cumple los requisitos  
- Comunicación de la consejería a los centros indicándoles que se convierten en CEIPSO y acogerán dos cursos de secundaria  
- Normativa que respalde impartir solo dos cursos de secundaria en un CEIP contradiciendo artículo 13.2 de RD 132/2010 https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a13 que es normativa básica

03/140462.9/25

**10 febrero 2025**

Recibo resolución de inadmisión

>RESUELVE  
Inadmitir la solicitud de acceso a la información solicitada en base a la siguiente motivación:  
Desde la Dirección General de Educación Infantil, Primaria y Especial de la Consejería de
Educación, Ciencia y Universidades le indicamos que tomando en consideración el objeto de su solicitud,
la misma queda fuera del ámbito de aplicación de la Ley 19/2013, de 9 de diciembre, de transparencia,
acceso a la información pública y buen gobierno (en adelante, LTAIBG).  
Tal y como se desprende de su Preámbulo, la LTAIBG tiene por objeto “ampliar y reforzar la
transparencia de la actividad pública, regular y garantizar el derecho de acceso a la información relativa
a aquella actividad y establecer las obligaciones de buen gobierno que deben cumplir los responsables
públicos, así como las consecuencias derivadas de su incumplimiento”. A estos efectos, su artículo 12
reconoce el derecho de todas las personas a acceder a la información pública, definida en el artículo 13
como los contenidos o documentos, cualquiera que sea su formato o soporte, que obren en poder de
alguno de los sujetos incluidos en el ámbito de aplicación de este título y que hayan sido elaborados o
adquiridos en el ejercicio de sus funciones.  
A tenor del literal de la presente solicitud, el peticionario no solicita información pública sobre una
materia, sino aclaraciones a actuaciones materiales de la Administración en el marco de sus
competencias.  
La Consejería de Educación, Ciencia y Universidades conforme a lo dispuesto en el artículo 1 del
Decreto 248/2023, de 11 de octubre, tiene atribuidas las competencias en relación con la enseñanza,
constituyéndose en la administración educativa competente de la Comunidad de Madrid, a los efectos
que previene el conjunto del ordenamiento jurídico vigente en materia educativa. Tiene por ello
competencia, entre otras, para autorizar las enseñanzas que se imparten en los centros y para adaptar
la normativa de organización y funcionamiento de los centros docentes.  
La autorización de enseñanzas en los centros educativos públicos se adopta por la consejería
competente en materia de educación en función de las necesidades de planificación de la oferta
educativa en las diferentes zonas de la región y en el marco de la normativa aplicable, teniendo la
información solicitada un carácter auxiliar o de apoyo, y no de información pública.  
Por lo tanto, cabe concluir que el objeto de la solicitud no puede considerarse como “información
pública” a los efectos de los artículos 12 y 13 de la LTAIBG y, en consecuencia, procede su inadmisión de
conformidad con el apartado b) del artículo 18.”.  

**16 febrero 2025**  

Texto reclamación consejo de transparencia Madrid

https://sede.comunidad.madrid/denuncias-reclamaciones-recursos/consejo-transparencia-proteccion-datos/tramitar

---

Se inadmite alegando "no solicita información pública sobre una materia, sino aclaraciones a actuaciones materiales" y "teniendo la información solicitada un carácter auxiliar o de apoyo, y no de información pública."
Sin embargo lo que solicito es un acta, un plan de espacios, un escrito  dirigido a la consejería, un documento de los técnicos de la consejería, una comunicación de la consejería a los centros y normativa, todo ello documentación existente y que tiene carácter público.
Considero que la documentación existente solicitada no tiene carácter auxiliar, ya que es  relevante para reflejar cómo se gestiona la implantación de 1º y 2º ESO en primaria, que afecta a la comunidad educativa, familias y profesorado. El acta solicitada debe reflejar si se ha votado conociendo el detalle de qué normativa respalda y va a regular lo votado en esos consejos escolares.
En este caso no solo no se facilita normativa que respalde la implantación en esos 52 centros sino que tampoco ha habido trámite de consulta públia ni trámite de audiencia según indican https://boe.es/buscar/act.php?id=BOE-A-2015-10565#a133 y https://boe.es/buscar/act.php?id=BOE-A-1997-25336#a26 y https://www.bocm.es/boletin/CM_Orden_BOCM/2021/03/25/BOCM-20210325-1.PDF, ni se haya justificado el por qué no se han realizado. 

---

Enviado 16 febrero 2025 03/221807.9/25

**24 febrero 2025**  

---

Solicito copia o enlace, asociada a la información publicada por la Comunidad de Madrid el 14 de febrero en https://www.comunidad.madrid/noticias/2025/02/14/diaz-ayuso-anuncia-inversion-4-millones-adaptar-52-colegios-publicos-impartiran-secundaria-proximo-curso  

- Partidas presupuestarias asociadas a la inversión de más de dos millones de euros en equipamiento digital y más de dos millones de euros en mobiliario y material didáctico para adaptar 52 colegios públicos que impartirán 1º y 2º ESO el próximo curso 2025-2026, indicando el programa en https://www.comunidad.madrid/sites/default/files/doc/hacienda/2025-presupuesto-libro-10-educacion.pdf o indicando si es una partida extraordinaria, aclarando si la gestión de la inversión es realizada directamente por la Consejería o es realizada por los centros.    
- Reparto de la inversión en cada uno de los 52 colegios públicos implicados   
- Licitaciones con número de expediente y enlace al portal de contratación, o bien relación de contratos menores asociados.  

---

24 febrero 2025 03/254826.9/25

**1 marzo 2025**  

---

Solicito copia o enlace de información del número de alumnos de 6º de primaria para los que los centros han indicado que van a continuar sus enseñanzas en el mismo centro en 1º de ESO de cara al curso 2025-2026, con datos desglosados por cada uno de los centros. 
La información está asociada a lo indicado en el anexo IV de RESOLUCIÓN de 22 de noviembre de 2024 https://www.comunidad.madrid/sites/default/files/bocm-20241213-26.pdf en el que se indica que hasta 21 febrero 2025 "Los centros con adscripción única a otros centros verifican que han informado en el Sistema Integral de Gestión Educativa Raíces a todos los alumnos que finalizan sus enseñanzas en el centro.", y lo publicado por la Comunidad de Madrid en https://www.comunidad.madrid/noticias/2025/02/03/diaz-ayuso-anuncia-proximo-curso-medio-centenar-colegios-publicos-comenzaran-impartir-educacion-secundaria

---

1 marzo 2025 03/297924.9/25

### Quejas al Defensor del Pueblo

Texto queja al Defensor del Pueblo

**16 febrero 2025**  

---

Quiero presentar una queja porque la Consejería de Educación de Madrid ha indicado que va a implantar 1º y 2º ESO en 52 CEIP  
https://www.comunidad.madrid/noticias/2025/02/03/diaz-ayuso-anuncia-proximo-curso-medio-centenar-colegios-publicos-comenzaran-impartir-educacion-secundaria  
https://www.comunidad.madrid/noticias/2025/02/14/diaz-ayuso-anuncia-inversion-4-millones-adaptar-52-colegios-publicos-impartiran-secundaria-proximo-curso  
Teniendo en cuenta que en el momento de este escrito:  
- El artículo 13.2 RD 132/2010, que es básico según disposición final primera y de obligado cumplimiento por las CCAA, los centros deberán impartir los cuatro cursos de ESO  
- No hay normativa autonómica publicada asociada a que esos 52 centros implanten 1º y 2º ESO como sí existe para otros CEIPSO en los que se imparten los 4 cursos de ESO (Orden 2968/2014 y orden 1017/2015), ni ha habido trámite de consulta públia ni trámite de audiencia según indican https://boe.es/buscar/act.php?id=BOE-A-2015-10565#a133 y https://boe.es/buscar/act.php?id=BOE-A-1997-25336#a26 y https://www.bocm.es/boletin/CM_Orden_BOCM/2021/03/25/BOCM-20210325-1.PDF, ni se ha  justificado el por qué no se han realizado.   
La implantación en CEIPs solo de 1º y 2º ESO constituye una infracción manifiesta del ordenamiento jurídico  
Solicito la ayuda del Defensor del Pueblo  para haga los requerimientos necesarios para que se cumpla la normativa.  
Se solicita que se realice con urgencia ya la implantación indicada que incumple normativa afecta al proceso de admisión que es inminente.  

---

### Reclamación a inspección

Ver post [reclamación a inspección](../reclamar-a-inspeccion-educativa/)

**14 febrero 2025** 

Expone

La comunidad de Madrid ha indicado que va a implantar 1º y 2º ESO en 52 CEIP  
https://www.comunidad.madrid/noticias/2025/02/03/diaz-ayuso-anuncia-proximo-curso-medio-centenar-colegios-publicos-comenzaran-impartir-educacion-secundaria  
https://www.comunidad.madrid/noticias/2025/02/14/diaz-ayuso-anuncia-inversion-4-millones-adaptar-52-colegios-publicos-impartiran-secundaria-proximo-curso  

Teniendo en cuenta que en el momento de esta solicitud:  
- El artículo 13.2 RD 132/2010, que es básico según disposición final primera y de obligado cumplimiento por las CCAA, los centros deberán impartir los cuatro cursos de ESO
- No hay normativa autonómica publicada asociada a que esos 52 centros implanten 1º y 2º ESO como sí existe para otros CEIPSO en los que se imparten los 4 cursos de ESO (Orden 2968/2014 y orden 1017/2015)
- El artículo 3.1.d de orden 732/2021 atribuye a la inspección educativa "Velar por el cumplimiento, en los centros educativos, de las leyes, reglamentos y demás disposiciones vigentes que afecten al sistema educativo."
 La implantación en CEIPs solo de 1º y 2º ESO constituye una infracción manifiesta del ordenamiento jurídico, por lo que como empleado público en cumplimiento de artículos 52, 53 y 54 del RDL 5/2015 debo ponerla inmediatamente en conocimiento de inspección

Solicita

Que inspección educativa según artículo 3.2.e de orden 732/2021 proceda a "Elevar informes y hacer requerimientos cuando se detecten incumplimientos en la aplicación de la normativa, y levantar actas"

Se solicita que se realice con urgencia ya la implantación indicada que incumple normativa afecta al proceso de admisión que es inminente.

---   

Madrid Capital 09/308855.9/25  
Alta inspección se envía por correo: no cambio el texto, podría buscar y citar la normativa de alta inspección en lugar de la de inspección de Madrid pero creo que se entiende   

**17 febrero 2025**  
Alta inspección me responde

---

Buenos días,

En respuesta a su consulta le indicamos:

Este es el correo institucional del Área de Alta Inspección de Educación de la Delegación del Gobierno en Madrid. En este Área tramitamos la mayor parte de homologaciones de expedientes de Secundaria y Bachillerato de residentes en la Comunidad de Madrid.

Su consulta es competencia de la Comunidad de Madrid, queda fuera de nuestro ámbito competencial. Debe contactar con la DAT que le corresponda, consulte la siguiente página web para más información:

https://www.educa2.madrid.org/web/direcciones-de-area

Un cordial saludo.

---

**18 febrero 2025**  

Respondo

---

Buenas tardes

La Ata Inspección también tiene más competencias 

https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a150

Artículo 150. Competencias.

1. En el ejercicio de las funciones que están atribuidas al Estado, corresponde a la Alta Inspección:

a) Comprobar el cumplimiento de los requisitos establecidos por el Estado en la ordenación general del sistema educativo en cuanto a modalidades, etapas, ciclos y especialidades de enseñanza, así como en cuanto al número de cursos que en cada caso corresponda.

Considero que hay un incumplimiento de normativa básica educativa y Alta Inspección debe intervenir.

Los funcionarios de Alta Inspección también deben cumplir el artículo 53.1 de RDL 5/2015

https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a53

Es un tema que considero tiene una relevancia creciente 

https://cadenaser.com/nacional/2025/02/17/institutos-y-colegios-publicos-claman-contra-el-proyecto-de-ayuso-de-dividir-la-eso-cadena-ser/

---

**19 febrero 2025** 

Recibo respuesta, con copia a aaie.madrid@correo.gob.es

---

Buenos días,

Gracias por recordarnos nuestras competencias.

Queremos aclararle que el correo que usted recibió es una respuesta estándar dado que usted se refería en todo momento a la orden 732/2021 que regula la Inspección  Educativa de la Consejería de Educación.

En la Alta Inspección de Educación ya teníamos conocimiento de las noticias que usted nos traslada.

Un saludo,

---

### Normativa / casos CEIP + ESO 

Está el caso especial de los entornos rurales, y luego otros casos en administraciones, se comenta normativa y noticias

A nivel estatal
[Artículo 2. Denominación de los centros docentes. Real Decreto 132/2010](https://www.boe.es/buscar/act.php?id=BOE-A-2010-4132#a2)  
>Los centros docentes tendrán la denominación genérica que establecen los artículos 111 y 114 de la Ley Orgánica 2/2006, de 3 de mayo, de Educación.

En [Artículo 111.5 LOE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a111)  
> 5. Corresponde a las Administraciones educativas determinar la denominación de aquellos centros públicos que ofrezcan enseñanzas agrupadas de manera distinta a las definidas en los puntos anteriores. 

Ambos artículos son básicos, y parece dejar abierta la posibilidad a las CCAA a que modifiquen

Artículo 13.2 se puede forzar a interpretar como que no aplica si no son "centros de educación secundaria", por lo que si el centro tiene otra denominación aunque ofreza secundaria no tendría que ofertar los cuatro cursos.

Normativa sobre CRA (Centros Rurales Agrupados)  
[Real Decreto 82/1996, de 26 de enero, por el que se aprueba el Reglamento Orgánico de las Escuelas de Educación Infantil y de los Colegios de Educación Primaria. Artículo 3. Modificación de la red de centros de educación infantil y primaria.](https://www.boe.es/buscar/act.php?id=BOE-A-1996-3689#a3)  

En Madrid se llaman CEIPSO. En el [buscador de centros](https://gestiona.comunidad.madrid/wpad_pub/) en 2025 aparecen 41 centros

[Orden 2968/2014, de 18 de septiembre, de la Consejería de Educación, Juventud y Deporte, por la que se autoriza a cinco colegios de Educación Infantil y Primaria a impartir Educación Secundaria Obligatoria y se dan instrucciones para la aplicación de la normativa de organización y funcionamiento en los colegios de Educación Infantil, Primaria y Secundaria Obligatoria de la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2014/10/03/BOCM-20141003-13.PDF)  

[ORDEN 1017/2015, de 15 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se autoriza a tres colegios de Educación Infantil y Primaria a impartir Educación Secundaria Obligatoria y se dan instrucciones para la aplicación de la normativa de organización y funcionamiento en estos centros docentes.](https://www.bocm.es/boletin/CM_Orden_BOCM/2015/05/11/BOCM-20150511-25.PDF)  

En Galicia se llaman CPI

[DECRETO 7/1999, de 7 de enero, por el que se implantan y regulan los centros públicos integrados de enseñanzas no universitarias.](https://www.xunta.gal/dog/Publicados/1999/19990126/Anuncio28B6_es.html)

En Cataluña se llaman Instituts escola

[Els instituts  escola: aspectes organitzatius, curriculars i d’orientació](https://xtec.gencat.cat/web/.content/curriculum/xarxa-instituts-escola/documents-dorientacio/documents21.pdf)

Castilla y León

[ORDEN EDU/491/2012, de 27 de junio, por la que se concretan las medidas urgentes de racionalización del gasto público en el ámbito de los centros docentes no universitarios sostenidos con fondos públicos de la Comunidad de Castilla y León](https://www.educa.jcyl.es/es/resumenbocyl/orden-edu-491-2012-27-junio-concretan-medidas-urgentes-raci)  

> Cursos de primero y segundo de educación secundaria obligatoria en colegios de
educación infantil y primaria.   
>1. Aquellos colegios públicos de educación infantil y primaria que en el curso escolar
2020-2021 están impartiendo primero y segundo de educación secundaria obligatoria
podrán seguir impartiendo las mencionadas enseñanzas hasta el curso 2024-2025 siempre
que concurran las siguientes condiciones:   
a) Que cuenten con un mínimo 8 alumnos, tanto en primero como en segundo curso
de educación secundaria obligatoria.  
b) Que las familias del alumnado, con carácter previo a su escolarización en
los colegios para cursar primero y segundo curso de educación secundaria
obligatoria, opten expresamente por ello en vez de acudir al instituto de enseñanza
secundaria.  
c) No computarán para alcanzar el mínimo de alumnado requerido aquellos alumnos
que necesiten utilizar transporte para acudir al colegio, si cuentan con un instituto
de educación secundaria a una distancia menor desde su localidad de residencia.   
La consejería competente en materia de educación determinará la prórroga curso
a curso para posibilitar impartir primero y segundo de educación secundaria obligatoria
cuando se cumplan las condiciones señaladas.  

**27 junio 2012**  
[La Junta traslada la ESO rural a los Institutos de Educación Secundaria en la provincia](http://comunicacion.jcyl.es/web/jcyl/Comunicacion/es/Plantilla100Detalle/1281372051501/_/1284242231543/Comunicacion)  
> La consejería de Educación, consciente de la progresiva disminución del número de alumnos y de una situación de transitoriedad que se ha alargado en el tiempo, ha decidido normalizar la escolarización de los alumnos de los centros rurales en un contexto pedagógico-administrativo apropiado y con suficientes garantías de calidad. Los alumnos que en el actual curso académico han recibido enseñanzas del primer ciclo de la ESO, así como aquellos que han promocionado de 6º de Educación Primaria, se escolarizarán, a partir del próximo curso académico 2012/2013 en los IES de referencia para completar la etapa de educación obligatoria.  

**26 marzo 2013**  
[La supresión del primer ciclo de la ESO en el mundo rural abre otro frente ciudadano contra la Junta](https://red.diariocritico.com/noticia/1490554/noticias/la-supresion-del-primer-ciclo-de-la-eso-en-el-mundo-rural-abre-otro-frente-ciudadano-contra-la-junta.html)  
El consejero justifica la medida en que los alumnos "están mejor" en la etapa que les corresponde
PP y PSOE de León, unidos contra la decisión del Gobierno regional  
La Diputación de Segovia pide a la Junta que mantenga la ESO rural  

### Centros de nueva construcción

Parece que Madrid lo quiere aplicar a centros de nueva construcción, donde el hecho de que solo cursen 1º y 2º ESO se puede vender como una situación transitoria por ser nuevo centro y estar implantándose enseñanzas

**27 enero 2025**  
[El Cañaveral tendrá el próximo curso el primer colegio público de nueva construcción con 1º y 2º de ESO - telemadrid](https://www.telemadrid.es/noticias/madrid/El-Canaveral-tendra-el-proximo-curso-el-primercolegio-publico-de-nueva-construccion-con-1-y-2-de-ESO-0-2747125299--20250127051817.html)  

> El centro tendrá jornada partida y comenzará con 12 aulas de segundo ciclo de Infantil, a las que se añadirán
otras 24 de Primaria y 16 de ESO  

Quizá buscando contratos asociados y modificaciones en pliegos se podrían detectar cosas

Este contrato se adjudica el 31 enero 2025  
[Servicio de redacción de proyectos de obras de refuerzo de la red educativa: "terminación del CEIP (línea 6) en el barrio de El Cañaveral de Madrid”, "terminación del nuevo IES (línea 6) en Arganda del Rey", “construcción de nuevo IES en Cobeña”, “construcción de nuevo CEIP en Colmenar Viejo”, “construcción de nuevo CEIP en Tres Cantos” y "construcción de nuevo IES en el barrio de Los Molinos de Getafe" A/SER-005174/2024](https://contratos-publicos.comunidad.madrid/contrato-publico/servicio-redaccion-proyectos-obras-refuerzo-red-educativa-terminacion-ceip-linea-6)  


### Cambio de llevar 7º y 8º de EGB a los IES, con maestros en IES.
  
**9 septiembre 1997**  
[Adiós a la EGB - elpais](https://elpais.com/diario/1997/09/09/sociedad/873756011_850215.html)  
> La Educación General Básica ha desaparecido del sistema educativo. Ha sobrevivido 27 años y por sus ocho cursos han pasado casi 15 millones de españoles, empezando por los que nacieron en 1961. El calendario de la reforma educativa sigue pasando hojas, con notables diferencias según las comunidades, pero 2º de Enseñanza Secundaria Obligatoria (ESO) ya ha tomado el relevo al antiguo 8º de EGB en todo el país.

**15 marzo 2024**  
[Los últimos maestros de EGB que quedan en la ESO: “Lo que siempre supe es que quería enseñar” - elpais](https://elpais.com/educacion/2024-03-15/los-ultimos-maestros-de-egb-que-quedan-en-la-eso-lo-que-siempre-supe-es-que-queria-ensenar.html)  
> Fueron decenas de miles. La mayoría se han jubilado o no les queda mucho para hacerlo. No hay un registro oficial que precise exactamente cuántos llegaron a ser ni cuántos quedan, pues su figura se ha ido entrelazando con otras, pero Comisiones Obreras estima que en 2014 aún rondaban los 10.000 y que ahora, probablemente, no llegan al millar. Entre finales de los años noventa y los primeros años de este siglo, un gran número de profesores de la antigua Educación General Básica (EGB) se incorporaron a la nueva Educación Secundaria Obligatoria (ESO) ―manteniendo su nivel funcionarial previo y un sueldo menor―. Solo podían dar clase en el primer ciclo, en 1º y 2º de la ESO, que se correspondían con los desaparecidos 7º y 8º de EGB.   


También ha habido situaciones expeciales
[ORDEN EDU/67/2017, de 8 de febrero, por la que se modifica la Orden EDU/491/2012, de 27 de junio, por la que se concretan las medidas urgentes de racionalización del gasto público en el ámbito de los centros docentes no universitarios sostenidos con fondos públicos de la Comunidad de Castilla y León.](https://bocyl.jcyl.es/boletines/2017/02/16/pdf/BOCYL-D-16022017-3.pdf)  
> Cursos de primero y segundo de educación secundaria obligatoria en colegios de educación infantil y primaria.  

### Titularidad y mantenimiento de los centros

Una idea es que los colegios suelen ser de titularidad de los ayuntamientos y mantenidos por ellos, mientras que los IES suelen ser de la comunidad autónoma. Unificar en un único código supone un tema administrativo 

[Alivio para los Ayuntamientos: el mantenimiento de colegios deben asumirlo las CCAA](https://www.elconfidencial.com/juridico/2022-01-15/alivio-ayuntamientos-ccaa-mantenimiento-colegios_3357901/)  
El Supremo determina que los consistorios solo están obligados a costear "la conservación y el mantenimiento" de los centros públicos de infantil, primaria y educación especial cuya titularidad ostenten  

[Disposición adicional decimoquinta. Municipios, corporaciones o entidades locales.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dadecimoquinta)  

> 2. La conservación, el mantenimiento y la vigilancia de los edificios destinados a centros públicos de educación infantil, de educación primaria o de educación especial, corresponderán al municipio respectivo. Dichos edificios no podrán destinarse a otros servicios o finalidades sin autorización previa de la Administración educativa correspondiente.  
> 3. Cuando el Estado o las Comunidades Autónomas deban afectar, por necesidades de escolarización, edificios escolares de propiedad municipal en los que se hallen ubicados centros de educación infantil, de educación primaria o de educación especial, dependientes de las Administraciones educativas, para impartir educación secundaria o formación profesional, asumirán, respecto de los mencionados centros, los gastos que los municipios vinieran sufragando de acuerdo con las disposiciones vigentes, sin perjuicio de la titularidad demanial que puedan ostentar los municipios respectivos. Lo dispuesto no será de aplicación respecto a los edificios escolares de propiedad municipal en los que se impartan, además de educación infantil y educación primaria o educación especial, el primer ciclo de educación secundaria obligatoria. Si la afectación fuera parcial se establecerá el correspondiente convenio de colaboración entre las Administraciones afectadas.

### Conciertos de centros privados

Creo que puede ser un tema no trivial: aunque primaria y ESO son etapas obligatorias y ambas se pueden concertar en centros privados, puede que las unidades concertadas sean distintas y pasar 1º y 2º ESO a primaria suponga modificar la normativa de los conciertos y la vigencia y efectos de los que estén en vigor.  
