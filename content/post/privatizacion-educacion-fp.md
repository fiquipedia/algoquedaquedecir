+++
author = "Enrique García"
title = "Privatización educación: FP"
date = "2021-12-13"
tags = [
    "educación", "privatización", "FP", "post migrado desde blogger"
]
toc = true

description = "Privatización educación: FP"
thumbnail = "https://pbs.twimg.com/media/FGci1OvXMAEFOaN?format=jpg"
images = ["https://pbs.twimg.com/media/FGci1OvXMAEFOaN?format=jpg"]

+++

Revisado 7 octubre 2024

## Resumen

Al hablar de privatización de la educación hay quien cuestiona que esté ocurriendo o su relevancia. Se intentan aportar datos con fuente, centrándose en Formación Profesional (FP), que aparece de varias maneras en 2021, como en la nueva ley de FP, falta de plazas públicas en Madrid, o compra de grupos educativos por fondos de inversión. En 2022 enlaza con las "becas" (cheques SOLO para privados) de FPGM y FPGS en Madrid.

Posts relacionados:

* [Datos educación por titularidad](https://algoquedaquedecir.blogspot.com/2020/11/datos-educacion-por-titularidad.html) (se ve evolución de alumnado en FP por titularidad, y el desmantelamiento de la educación pública / potenciamiento de la educación privada)
* [Becas FP Grado Medio Madrid](https://algoquedaquedecir.blogspot.com/2022/02/becas-fp-grado-medio-madrid.html) (solo para privados)
* [Becas FP Grado Superior Madrid](https://algoquedaquedecir.blogspot.com/2022/06/becas-fp-grado-superior-madrid.html) (solo para privados)
* [Becas y ayudas educación Madrid: manipulación y conciertos](https://algoquedaquedecir.blogspot.com/2018/11/becas-y-ayudas-educacion-madrid.html) (se ve como gran parte de dinero público va a centros privados pero se esconde esa privatización bajo el nombre de "becas y ayudas")
* [Becas para el estudio de Programas de Segunda Oportunidad Madrid](https://algoquedaquedecir.blogspot.com/2021/02/becas-para-el-estudio-de-programas-de-segunda-oportunidad.html) (se ve como lo que según nombre venden como becas realmente da, sin decimales, 100% dinero a privados, con o sin concierto)
* [No existe ningún centro que sea concertado](https://algoquedaquedecir.blogspot.com/2018/02/no-existe-ningun-centro-que-sea-concertado.html) (sobre la idea de que los centros son privados con concierto y son empresas, se comentan ideas sobre servicio público)
* [Privados con concierto en etapas no obligatorias: lo singular como normal](http://algoquedaquedecir.blogspot.com/2017/08/privados-con-concierto-en-etapas-no.html)
* [Universidades privadas Madrid](https://algoquedaquedecir.blogspot.com/2019/02/universidades-privadas-madrid.html) (sobre la idea de potenciamiento de la educación privada)
* [Trilema y Ashoka](https://algoquedaquedecir.blogspot.com/2018/01/trilema.html) (sobre la entrada de entidades "filantrópicas" en educación)
* [Datos gasto en educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-gasto-en-educacion.html)
* [Gratuidad de la educación](https://algoquedaquedecir.blogspot.com/2021/10/gratuidad-de-la-educacion.html)
* [Aulas Profesionales Emprendimiento](../aulas-profesionales-emprendimiento)  Se citan fondos europeos para FP

## Detalle

En el post surgen ideas sobre Madrid, aunque se comentan datos generales y de Madrid según se van tratando temas (alumnado, gasto, plazas)

### Conceptos

### Privatización y servicio público

Un punto de partida es definir qué es privatización, ya que con el tiempo se ha conseguido difuminar palabras y normalizar cosas. Por ejemplo habrá quien considere que aumentar centros privados que tienen concierto no es privatizar, ya que se supone que prestan un servicio público: enlaza con el post [No existe ningún centro que sea concertado](https://algoquedaquedecir.blogspot.com/2018/02/no-existe-ningun-centro-que-sea-concertado.html) y con la idea de educación como servicio público, no como mercancía con la que obtener beneficio económico.

Privatización es hacer que un servicio público se preste con empresas privadas, que por definición buscan beneficio económico, no (solo) prestar servicio público. El tema de la eficiencia o diferencia de gasto entre público y privado es algo separado; lo que es innegable es que habiendo una empresa privada de por medio parte del gasto debe ir a beneficios de esa empresa, no al servicio prestado.  

Cuando alguien me habla de que la gestión privada de un servicio público no es privatizar, suelo proponer que piensen en privatizar el ejercito o privatizar la justicia (privatizar la sanidad ya es un hecho en ciertos países y se avanza en España). Eso sería no tener soldados ni jueces sino que una empresa privada lo gestionase; enlaza con la necesidad de funcionarios, que desempeñan (deben) de manera neutral y funcional una función que cubre un servicio público independientemente de la ideología del gobierno. Ya puestos se podría pensar también en privatizar la política, lo que vendría a ser que una empresa fuera contratada para hacer  leyes, y la democracia pasase a ser un concurso público en el que los ciudadanos eligiesen la empresa privada que creen que mejor lo va a hacer. Eso sí, la gestión de los papeles y los trámites para el concurso de elección de empresa gobernante lo debería hacer ... ¿otra empresa?

Hacen falta servicios públicos independientes de empresas privadas. Si un gestor público lo único que se sabe hacer es subcontratar empresas privadas (pagándolas con dinero público), igual se tiene que replantear si vale para gestor público o debería ser un gestor privado. Lo que pasa es que para llegar a ser gestor público no tiene que demostrar mérito ni capacidad, solo conseguir que le voten o que alguien a quien hayan votado le nombre a dedo, mientras que para llegar a gestionar en la empresa privada sí que tiene que demostrar mérito y capacidad.  

Enlaza con los apartados "Atacar a lo público / atacar a lo privado" y "Privatización en general"

### Tipos de FP 

Aclaro los tipos y las diferencias, que implican qué tipo de fondos públicos reciben: por ejemplo si se pueden concertar o no.

La FP tiene actualmente 3 tipos:

* FP Básica (FPB)
* FP Grado Medio (FPGM)
* FP Grado Superior (FPGS)

**FP Básica** se trata en el post [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html), y es educación básica (introducido por LOMLOE artículo 3 LOE ) que tiene conciertos generales (introducido por LOMCE en artículo 116 LOE). Es nivel educativo CINE 3, mismo que FPGM y Bachillerato (ver post [LOMCE: abandono escolar temprano](http://algoquedaquedecir.blogspot.com/2018/08/lomce-abandono-escolar-temprano.html))  
**FP Grado medio** es educación secundaria de segundo ciclo, y es nivel educativo CINE 3. No es educación obligatoria, por lo que los conciertos son singulares. El acceso a FPGM está garantizado desde FPB sin prueba de acceso.  
**FP Grado Superior** es educación superior, y es nivel educativo CINE 5. No es educación obligatoria, por lo que los conciertos son singulares (ver [Privados con concierto en etapas no obligatorias: lo singular como normal](http://algoquedaquedecir.blogspot.com/2017/08/privados-con-concierto-en-etapas-no.html))

### Datos alumnado FP por tipo, titularidad y año

Es algo tratado en post [Datos educación por titularidad](https://algoquedaquedecir.blogspot.com/2020/11/datos-educacion-por-titularidad.html), donde inicialmente miraba datos de Madrid. Aquí intento centrarme más en FP y a nivel global. 

**8 agosto 2022**

[El auge de la FP privada triplica su porcentaje de alumnos en 10 años - elpais.com](https://cincodias.elpais.com/cincodias/2022/08/05/economia/1659721970_687897.html)  
![](https://d500.epimg.net/cincodias/imagenes/2022/08/05/economia/1659721970_687897_1659905496_noticia_normal_recorte1.jpg)

La educación superior se privatiza poco a poco. El porcentaje de alumnos que cursan una formación profesional (FP) en un centro privado se ha más que triplicado en una década, pasando del 6,6% sobre el total en el curso 2010-2011, al 20,5% en el de 2019-2020.

**13 agosto 2022**

[¿Se está privatizando la educación? Uno de cada cinco alumnos de FP ya estudia en centros privados](https://www.lasexta.com/noticias/sociedad/esta-privatizando-educacion-uno-cada-cinco-alumnos-estudia-centros-privados_2022081362f7c895142d7b00018a21ec.html)  

[Datos y cifras curso 2020-2021](https://www.educacionyfp.gob.es/dam/jcr:89c1ad58-80d8-4d8d-94d7-a7bace3683cb/datosycifras2021esp.pdf)  

Alumnado en Enseñanzas no universitarias por sexo y titularidad del centro. Curso 2019-2020

Se pueden ver datos globales: 

[Enseñanzas no universitarias. Alumnado matriculado](https://www.educacionyfp.gob.es/servicios-al-ciudadano/estadisticas/no-universitaria/alumnado/matriculado.html)  

Y ver series

[Enseñanzas no universitarias. Alumnado matriculado. Series](https://www.educacionyfp.gob.es/servicios-al-ciudadano/estadisticas/no-universitaria/alumnado/matriculado/series.html)  

[Enseñanzas Régimen General > Alumnado matriculado por enseñanza](http://estadisticas.mecd.gob.es/EducaDynPx/educabase/index.htm?type=pcaxis&path=/no-universitaria/alumnado/matriculado/series/gen-alumnado&file=pcaxis&l=s0)  

6.1 Alumnado de Ciclos Formativos de FP Básica por titularidad del centro, comunidad autónoma y curso académico.

6.3 Alumnado de Ciclos Formativos de FP Grado Medio por titularidad del centro, comunidad autónoma y curso académico.

6.11 Alumnado de Ciclos Formativos de FP Grado Superior por titularidad del centro, comunidad autónoma y curso académico.

También se pueden ver datos sobre FP a distancia

[Enseñanzas Régimen General > Porcentaje de alumnado matriculado en centros público](http://estadisticas.mecd.gob.es/EducaDynPx/educabase/index.htm?type=pcaxis&path=/no-universitaria/alumnado/matriculado/series/gen-porcen-gen&file=pcaxis&l=s0)  

6 Porcentaje de alumnado matriculado en centros públicos, por grado. Ciclos Formativos de FP por comunidad autónoma y curso académico.

![](https://pbs.twimg.com/media/FDTvUk_WYAE0vAE?format=jpg)  

También se puede ver

[Los fondos de inversión buscan negocio en la FP ante la escalada de demanda y la escasez de plazas - elpais.com](https://elpais.com/educacion/2021-09-18/los-fondos-de-inversion-buscan-negocio-en-la-fp-ante-la-escalada-de-demanda-y-la-escasez-plazas.html)  
La compra de una red de centros por parte de un fondo estadounidense por 200 millones evidencia el auge de los estudios profesionales. La privada se abre un gran hueco allí donde no llega la pública 

Incluye gráficas de alumnado FPGS por titularidad: España, Madrid y Cataluña

Ver datos globales en artículo 8 marzo 2022

[La Formación Profesional, el nuevo nicho de mercado de la educación privada - eldiario.es](https://www.eldiario.es/sociedad/formacion-profesional-nuevo-nicho-mercado-educacion-privada_1_8781541.html)  

**21 agosto 2024**  

[Los estudiantes de la FP privada se disparan un 460% en una década por la falta de plazas en la educación pública - elpais](https://elpais.com/educacion/2024-08-21/los-estudiantes-de-la-fp-privada-se-disparan-un-460-en-una-decada-por-la-falta-de-plazas-en-la-educacion-publica.html)  
El auge de los centros privados no concertados se apoya en los ciclos formativos a distancia, cuyos estudiantes crecen un 3.000% y suponen más de la mitad de su matrícula

**23 agosto 2024**

[Formación Profesional: educación y negocio - editorial elpais.com](https://elpais.com/opinion/2024-08-23/formacion-profesional-educacion-y-negocio.html)  
El aumento exponencial de alumnos en centros privados obliga a todas las administraciones a reflexionar sobre la dotación de plazas públicas

### Datos alumnado FP Madrid

Las variaciones globalmente no son tan grandes como en Madrid

Hilo en el que recopilo información y enlaces, e incluye imágenes
**30 julio 2021**
[twitter FiQuiPedia/status/1421014022015623168](https://x.com/FiQuiPedia/status/1421014022015623168)  
![](https://pbs.twimg.com/media/Fij3yKrWIAAYESn?format=jpg)  

**24 noviembre 2021**  

[La Comunidad de Madrid ha creado más de 43.000 nuevas plazas en FP desde el curso 2018/19 - comunidad.madrid](https://www.comunidad.madrid/notas-prensa/2021/11/23/comunidad-madrid-ha-creado-43000-nuevas-plazas-fp-curso-201819)  
> La Comunidad de Madrid ha incrementado en más de 43.000 plazas la oferta de Formación Profesional en los centros educativos de la región desde el curso 2018/19. El aumento se ha producido durante estos años tanto en Grado Medio (+23.028) como en Grado Superior (+20.090).  

[twitter FiQuiPedia/status/1463646089907744769](https://twitter.com/FiQuiPedia/status/1463646089907744769)  
Usando sus propios datos tomados de https://estadisticas.educa.madrid.org se puede comprobar que lo que dicen @eossoriocrespo y @educacmadrid no es cierto.  
![](https://pbs.twimg.com/media/FE_puHYXMAMDaiH?format=jpg)  

Aunque los datos de 2020-2021 sean provisionales, la diferencia es demasiado grande.

Lo significativo es la variación porcentual.

[twitter FiQuiPedia/status/1470183703947186180](https://twitter.com/FiQuiPedia/status/1470183703947186180)  

![](https://pbs.twimg.com/media/FGci1OvXMAEFOaN?format=jpg)  
![](https://pbs.twimg.com/media/FGcj1Y5XMAEOyA1?format=jpg)  

**17 junio 2022**

[Las 10.000 plazas de Formación Profesional pública ofertadas por la Comunidad son insuficientes, cada año se excluyen unos 20.000 alumnos - madridnorte24horas.com](https://www.madridnorte24horas.com/onda-cero-madrid-norte/para-ugt-la-oferta-de-formacion-profesional-publica-en-madrid-para-el-proximo-curso-sigue-siendo-insuficiente/)  

**24 junio 2022**

[Ni ascensor social ni formación profesional - publico.es](https://blogs.publico.es/otrasmiradas/61100/ni-ascensor-social-ni-formacion-profesional)  
> Los datos del curso 2021-2022 hablan por sí solos. Primero fueron 6.938 jóvenes madrileños los que se quedaron sin plaza pública de FP de Grado Medio. Luego, 17.976 jóvenes vieron rechazada su solicitud de plaza pública de formación profesional de Grado Superior (el 54% de las solicitudes). Después, 4.422 jóvenes no obtuvieron plaza en la FP a distancia (un 46% de los solicitantes). Un total de 29.336 personas se quedaron sin una plaza pública de FP este curso.

**13 agosto 2022**

[Más de la mitad de los aspirantes madrileños para la FP se quedan sin plaza: “Te obligan a irte a la privada para seguir estudiando” - elpais.com](http://elpais.com/espana/madrid/2022-08-13/mas-de-la-mitad-de-los-aspirantes-madrilenos-para-la-fp-se-quedan-sin-plaza-te-obligan-a-irte-a-la-privada-para-seguir-estudiando.html)  
> Al menos 33.000 jóvenes se quedan fuera de la educación pública, el 45% en grados medios y el 61% en grados superiores, según un estudio realizado por el sindicato CC OO de la enseñanza  

**14 agosto 2022**

[El 55% de los alumnos madrileños de FP no tendrá plaza en la pública, mientras Ayuso subvenciona con 43 millones de euros los centros privados - elplural.com](https://www.elplural.com/autonomias/madrid/55-alumnos-madrilenos-fp-no-tendra-plaza-en-publica-mientras-ayuso-subvenciona-con-43-millones-euros-centros-privados_295725102)  
Según un estudio del sindicato CCOO Enseñanza, el 45% de los estudiantes de Grados Medios y el 61% de los de Grados Superiores se quedará fuera

**17 agosto 2022**

[Títulos educativos que en Madrid solo oferta la privada - eldiario.es](https://www.eldiario.es/madrid/titulos-educativos-madrid-oferta-privada_1_9146349.html)  
A diferencia de otras comunidades, la de Madrid no oferta enseñanzas deportivas de régimen especial

**18 agosto 2022**

[La Comunidad de Madrid ofrece en septiembre más de 12.500 plazas públicas de Formación Profesional para el curso escolar 2022/23 - comunidad.madrid](https://www.comunidad.madrid/noticias/2022/08/18/comunidad-madrid-ofrece-septiembre-12500-plazas-publicas-formacion-profesional-curso-escolar-202223)  
> La Comunidad de Madrid ofrecerá en septiembre más de 12.500 plazas de estudio en los distintos niveles de Formación Profesional (FP) para el próximo curso 2022/23. Más de 1.600 de estas vacantes corresponden a Grado Básico, más de 5.100 son para especialidades de Grado Medio y 5.800 del Superior.  
La relación de centros y enseñanzas estará disponible en la página web oficial de la Comunidad de Madrid a partir del 1 de septiembre, y las solicitudes de admisión se deberán tramitar en el centro correspondiente, los días 5 y 6 de este mismo mes para las titulaciones de Básico y Medio, mientras que en el Superior se realizarán el 7 y el 8. 

**3 octubre 2022**
[El agujero negro de las FP en Madrid: "Es un trasvase de fondos públicos a manos privadas" - publico.es](https://www.publico.es/sociedad/agujero-negro-fp-madrid-trasvase-fondos-publicos-manos-privadas.html)  
> El 53,8% de los solicitantes en Madrid de una plaza pública en alguna Formación Profesional se han quedado sin plaza. Tendrán que recurrir a la enseñanza privada para poder formarse y entrar en el mercado laboral.

**29 noviembre 2022**

[La FP privada se multiplica por diez en Madrid mientras miles de alumnos se quedan sin plaza pública - eldiario.es](https://www.eldiario.es/madrid/fp-privada-multiplica-diez-madrid-miles-alumnos-quedan-plaza-publica_1_9753336.html)  
El alumnado de ciclos formativos en centros privados se dispara un 1.523% en los grados medios y un 844% en los superiores en la Comunidad, que en los últimos años también ha elevado la cuantía de las “becas” exclusivas para la red particular 

**23 septiembre 2024**

[La Comunidad de Madrid deja fuera de la FP pública a la mitad del alumnado que pide plaza: 50.000 aspirantes este año - eldiario](https://www.eldiario.es/sociedad/comunidad-madrid-deja-fuera-fp-publica-mitad-alumnado-pide-plaza-50-000-aspirantes-ano_1_11673821.html)  
> Dos de cada tres peticiones son excluidas en el Grado Medio, según un estudio de CCOO; el sindicato denuncia que la Consejería apenas ofrece plazas en áreas claves como la sanitaria, dejando casi toda la oferta para lo privado 

**1 octubre 2024**

[Cuando la administración te empuja a la escuela privada - eldiario](https://www.eldiario.es/sociedad/educacion/administracion-empuja-escuela-privada_132_11694941.html)  
> Cuando te quedas sin plaza en la pública básicamente tienes tres opciones: te vas a la privada, te quedas en tu casa o te buscas un trabajo. La privada ni siquiera es una opción para muchas familias: son entre 2.000 y 4.000 euros anuales, según el centro, en un país donde el salario más habitual son 14.586 euros anuales. Ya solo quedan dos: quedarte en casa o buscar un trabajo, opción esta última que para alguien con la Secundaria como mucho, el lugar habitual desde el que se va a la FP, no parece la panacea.   

**6 octubre 2024**

[Los fondos de inversión hacen su gran desembarco en la FP, el nuevo filón de la educación privatizada - eldiario](https://www.eldiario.es/sociedad/fondos-inversion-gran-desembarco-fp-nuevo-filon-educacion-privatizada_1_11687076.html)

Ver tabla "Suben los alumnos en los centros privados de FP en 10 años"

### Datos gasto FP

Se pueden ver datos generales en [Datos gasto en educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-gasto-en-educacion.html)

Se pueden ver posts sobre "becas" FP Grado Medio Madrid y "becas" FP Grado Superior Madrid, SOLO para privados.

Se pueden citar algunas ideas enlazadas allí.

Si se fuerza que se realice gasto privado en un servicio público se está privatizando.

**18 septiembre 2018** 

de IVIE y BBVA

[El mayor potencial de mejora de los resultados educativos reside en gestionar eficazmente los recursos y garantizar la igualdad de oportunidades - ivie.es](https://www.ivie.es/es_ES/el-mayor-potencial-de-mejora-de-los-resultados-educativos-reside-en-gestionar-eficazmente-los-recursos-y-garantizar-la-igualdad-de-oportunidades/)  

[Presentación monografía: Diferencias educativas regionales 2000-2016. Condicionantes y desempeños - ivie slideshare.net](https://www.slideshare.net/Iviesa/presentacin-monografa-diferencias-educativas-regionales-20002016-condicionantes-y-desempeos)  

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgWQOXhjfZJTjm_5iYaqJmAec7-5f4_TNcBN7bBvMRp3E09mjJLJ6yqVreJPfRIBv_blmr8MCYNLpoWw_s3es_eHBP0lI-gtN4nvQyIwScBDGVDzbhNLxBV0gYDPII5lpUOawrxS_Zs6Tle/s16000/GastoPrivadoEnEducacion.png)

[Equity in school education in Europe. Structures, policies and student performance. Eurydice report](https://op.europa.eu/en/publication-detail/-/publication/a18e3a88-1e4d-11eb-b57e-01aa75ed71a1/language-en/format-PDF/source-search)  

Figure II.2.2: Private (household) expenditure as a percentage of total public expenditure on education (ISCED 1-3), 2016 (%)

![](https://pbs.twimg.com/media/ElKhnDNX0AALRfC?format=jpg)  

Sin mirar solo Madrid y solo FP, se pueden ver datos de gasto educativo global y gasto en conciertos (y otras vías de dar dinero) en distintas CCAA con el tiempo.

**30 julio 2019**  

[La financiación de la educación concertada subió un 25% en diez años mientras se estancó la de la pública - eldiario.es](https://www.eldiario.es/sociedad/gasto-educacion-concertada-subio-publica_0_926008033.html)  
> Entre 2007 y 2017, la inversión en la pública quedó casi clavada con un 1,4% positivo, tras decrecer entre 2011 y 2014
La escuela pública acoge a casi todos los alumnos de entornos desfavorecidos. Las comunidades que más apuestan por los conciertos son País Vasco, Navarra y Madrid  
Las familias aumentan su gasto en Educación para compensar el estancamiento en la financiación pública: gastan hoy un 41% más que en 2007  

Incluye varias gráficas con evolución temporal de gasto.

En fuente hay gráficas interactivas  

### Datos gasto FP Madrid

**27 octubre 2021**

[La Comunidad de Madrid aprueba el proyecto de Presupuestos 2022 - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/10/27/comunidad-madrid-aprueba-proyecto-presupuestos-2022)  
> **Más fondos para FP, digitalización y obras**  
La educación madrileña contará con un presupuesto de 5.723 millones de euros, un 16,4% más de lo asignado en las anteriores cuentas prorrogadas. El mayor crecimiento en los programas presupuestarios de la Consejería de Educación, Universidades y Ciencia se produce en la Formación Profesional (+126,5%), que avanza de 17,7 millones de euros a 40,1 millones. Este aumento responde a la apuesta del Gobierno regional para impulsar la FP con la creación de nuevas plazas educativas, la implantación de nuevos ciclos, la adquisición de recursos digitales o el estreno de nuevas Aulas de Emprendimiento.

**10 noviembre 2021**

[Ayuso aumenta un 20% el presupuesto para la educación concertada - elsaltodiario.com](https://www.elsaltodiario.com/educacion-concertada/ayuso-aumenta-un-106percent-el-presupuesto-para-la-educacion-concertada)  
> La financiación de la escuela concertada sigue en aumento en el proyecto de ley de los Presupuestos Generales de la Comunidad de Madrid para 2022. Sindicatos y la Marea Verde critican unas cuentas que considerándolas elitistas y segregadoras.  
Ella misma se refiere a la financiación que, de ser aprobados los Presupuestos, irá destinada a la Formación Profesional (FP): “Aunque el Consejero de Educación ha dicho previamente que Madrid cuenta con 136.000 alumnos de Formación Profesional, los presupuestos de este año contemplan solamente 81.000 alumnos, es decir, un 60% del total. Continúa así la estrategia privatizadora de Ayuso: crea un déficit de plazas públicas y deja espacio al crecimiento de la oferta educativa privada”. Esta falta de oportunidades es aprovechada por fondos de inversión y fondos buitre, afirma la misma Galvín, para hacer negocio con la educación, “como si este derecho se vendiera en el libre mercado”.

[La letra pequeña de los presupuestos de Ayuso: menos inversión para Sanidad e impulso a la educación concertada - eldiario.es](https://www.eldiario.es/madrid/letra-pequena-presupuestos-ayuso-inversion-sanidad-e-impulso-educacion-concertada_1_8440142.html)  

**5 noviembre 2021**

[twitter isabelgalvin/status/1456635332884062210](https://twitter.com/isabelgalvin/status/1456635332884062210)  
Imagina sacar unos presupuestos que, de entrada, dejan fuera al 40% de los jóvenes que quieren estudiar FP.  
Los presupuestos de Ayuso son una película de terror. Va hilo largo sobre el tema porque no se está hablando de esto lo suficiente

Aparte de datos de FPGM y FPGM concertados, hay otras vías de que centros privados reciban dinero público, ver [Becas para el estudio de Programas de Segunda Oportunidad Madrid](https://algoquedaquedecir.blogspot.com/2021/02/becas-para-el-estudio-de-programas-de-segunda-oportunidad.html), donde el resumen en imagen inicial habla por sí solo.

![](https://pbs.twimg.com/media/ERn7uj4WkAIZNXJ?format=png)  
 
[twitter FiQuiPedia/status/1441696634203619330](https://twitter.com/FiQuiPedia/status/1441696634203619330)  
Alguien diciendo que no hay que dar financiación pública para FP desde la administración que le ha puesto a dedo pagándole 93.855,00€ anuales de dinero público https://comunidad.madrid/transparencia/persona/juan-manuel-lopez-zafra, la misma administración que financia FP privada y reduce FP pública  

![](https://pbs.twimg.com/media/FAHvcfXUUAEKJRm?format=jpg)  
Tras decir @jmlopezzafra que no haya financiación pública dice "Que sean las empresas y los estudiantes quienes encuentren la forma de financiarla".  
Los centros PRIVADOS en Madrid lo tienen claro: dinero público a través de "becas"

[twitter FiQuiPedia/status/1389349163645685766](https://twitter.com/FiQuiPedia/status/1389349163645685766)  
Recomendación de @esne_es: "becas" FP Grado Superior de @educacmadrid que son solo para centros privados. El 90% de sus alumnos, de un centro privado, disfrutan de esa "beca".  
[CFGS – Animaciones 3D, Juegos y Entornos Interactivos - esne.es](https://esne.es/oferta-academica/ciclos-formativos-grado-superior/animaciones-3d-juegos-y-entornos-interactivos/)  
![](https://pbs.twimg.com/media/E0f1spVXMAENTLH?format=jpg)  

**21 julio 2022**

[Un estudio de CCOO deja al descubierto la estrategia de Ayuso para desmantelar la FP pública - feccoo-madrid.org](https://feccoo-madrid.org/noticia:633206--Un_estudio_de_CCOO_deja_al_descubierto_la_estrategia_de_Ayuso_para_desmantelar_la_FP_publica&opc_id=9f666eca08d47ba26c6a09927d7abbbb
)  
> * A pesar de que el pasado curso 30.000 jóvenes se quedaron sin plaza en la pública y de que la demanda aumenta, la Comunidad de Madrid sólo ha creado 10.000 plazas públicas  
> * En FP Básica y Grado Medio, la oferta privada alcanza ya el 32% y el 34%, respectivamente  
> * CCOO advierte que la privatización de la Formación Profesional en Madrid dejará sin plan de futuro a los jóvenes que no puedan endeudarse

**12 octubre 2022**

[UGT denuncia que el Gobierno de Ayuso devuelve la financiación estatal recibida para Formación Profesional pública - diario16.com](https://diario16.com/ugt-denuncia-que-el-gobierno-de-ayuso-devuelve-la-financiacion-estatal-recibida-para-formacion-profesional-publica/)  
La sorpresa surge cuando desde el sindicato comprobaron que la Comunidad de Madrid ha devuelto al tesoro 2.339.903€, por lo que dicho presupuesto, no ejecutado, hipoteca y se deduce de la cuantía total que le correspondería en 2022

[Resolución de 28 de septiembre de 2022, de la Secretaría General de Formación Profesional, por la que se publica el Acuerdo de la Comisión General de Educación, de 21 de julio de 2022, por el que se aprueban los criterios de distribución y la distribución resultante del crédito destinado en el año 2022 al Programa de Cooperación Territorial **de Calidad en Formación Profesional del sistema educativo español.**](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2022-16423)  

[Resolución de 28 de septiembre de 2022, de la Secretaría General de Formación Profesional, por la que se publica el Acuerdo de la Comisión General de Educación, de 21 de julio de 2022, por el que se aprueban los criterios de distribución y la distribución resultante del crédito destinado en el año 2022 al Programa de Cooperación Territorial **de Formación, Perfeccionamiento y Movilidad del Profesorado de Formación Profesional.**](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2022-16424)  

### Paso de privados con concierto a privados sin concierto en FPGS

Es algo que comenté en [Privados con concierto en etapas no obligatorias: lo singular como normal](https://algoquedaquedecir.blogspot.com/2017/08/privados-con-concierto-en-etapas-no.html)

**25 junio 2015**

[El TSJM anula la orden que derivó en la supresión de conciertos de FP de grado superior - europapress.es](https://www.europapress.es/madrid/noticia-tsjm-anula-orden-derivo-supresion-conciertos-fp-grado-superior-20150625180002.html)  

[Roj: STSJ M 7590/2015 - ECLI:ES:TSJM:2015:7590](https://www.poderjudicial.es/search/doAction?action=contentpdf&databasematch=AN&reference=7437235&links=%221483%2F2013%22&optimize=20150717&publicinterface=true)  

Que la comunidad llevó a TS y perdió

[STS 5027/2016 - ECLI:ES:TS:2016:5027](https://www.poderjudicial.es/search/AN/openCDocument/47c54a4d73e1a196fefa7815863395e15a353b67ac9e0f41)  

**25 marzo 2022**

[Manuel de Castro (Escuelas Catolicas de Madrid): “Los centros de FP que demandaron a la CAM han sido estafados” - exitoeducativo.net](https://exitoeducativo.net/manuel-de-castro-pte-de-escuelas-catolicas-de-madrid-los-centros-de-fp-que-demandaron-a-la-cam-han-sido-estafados/)  

[Desolación e indignación en los centros concertados de FP de Madrid - cope.es](https://www.cope.es/religion/hoy-en-dia/iglesia-espanola/noticias/desolacion-indignacion-los-centros-concertados-madrid-20220325_1992410)  

Comento la idea: enlaza con post pendiente sobre supresión líneas en públicos vs líneas en privados con concierto

[twitter FiQuiPedia/status/1508503047495991298](https://twitter.com/FiQuiPedia/status/1508503047495991298)  
Aquí se ve como si la administración decide que no hay consignación presupuestaria para renovar conciertos cuando toca, no se renuevan. Así que sí, se pueden eliminar conciertos si hay voluntad. Aquí la hay de privatización de FP, recibirán dinero público vía cheques/"becas"

**30 marzo 2022**

[Madrid retira el concierto de FP a 11 centros católicos de labor social pese a una sentencia en contra del Supremo - elpais](https://elpais.com/espana/madrid/2022-03-30/madrid-retira-el-concierto-de-formacion-profesional-a-11-centros-catolicos-de-labor-social-pese-a-una-sentencia-en-contra-del-supremo.html)  

**12 abril 2022**

[Los conciertos de FP en 11 escuelas católicas se prorrogarán por 10 años - magisnet.com](https://www.magisnet.com/2022/04/los-conciertos-de-fp-en-11-escuelas-catolicas-se-prorrogaran-por-10-anos/)  
Escuelas Católicas de Madrid celebra que la Consejería de Educación haya dado marcha atrás en la supresión de los conciertos de Formación Profesional Superior en 11 escuelas con 1.800 alumnos, que ahora serán prorrogados por 10 años, y anuncia que tienen prevista una reunión con la presidenta Isabel Díaz Ayuso el próximo 18 de abril.

**7 mayo 2022**

Tras ver que se introduce como disposición adicional en decreto currículo primaria, pongo alegaciones

[Proyecto de Decreto por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la etapa de Educación Primaria.](https://www.comunidad.madrid/transparencia/proyecto-decreto-que-se-establece-comunidad-madrid-ordenacion-y-curriculo-etapa-educacion-primaria)  

> Disposición adicional única. Régimen de conciertos educativos.  
> 1. El régimen previsto en el Decreto 3/2021, de 13 de enero, del Consejo de Gobierno, por el que se modifica el Decreto 31/2019, de 9 de abril, por el que se regula el régimen de conciertos educativos en la Comunidad de Madrid, será de aplicación a todos los conciertos educativos vigentes en el momento de la entrada en vigor del mencionado decreto, con independencia de que tuvieran un plazo de vigencia diferente al ordinario fijado mediante acuerdo entre la administración y los centros educativos, salvo renuncia expresa de su titular, manifestada en el plazo de quince días hábiles a contar desde la entrada en vigor de este decreto.  
> 2. De forma análoga, aquellos convenios suscritos con centros privados por acuerdos específicos con la administración educativa para la financiación de enseñanzas postobligatorias, serán renovados, con la conformidad de los titulares, por el período que corresponda, de modo que su vigencia final abarque la totalidad del actual período de conciertos.

---

El "Proyecto de Decreto por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la etapa de Educación Primaria." incluye "Disposición adicional única. Régimen de conciertos educativos" regulando aspectos que:
- No tiene que ver con la ordenación y currículo  
- No tiene que ver con Educación Primaria. La disposición aplica a "todos los conciertos" por lo que también aplica a ESO. La disposición cita explícitamente "enseñanzas postobligatorias"  

Además se añade esa disposición adicional en una tramitación urgente, cuando la "Orden declaración tramitación urgente"
no cita la urgencia asociada a esta disposición adicional  
[Orden 457/2022 del Consejero de Educación, Universidades, Ciencia y Portavoz del Gobierno por la que se declara la tramitación urgente del procedimiento de elaboración y aprobación del proyecto de Decreto del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la etapa de Educación Primaria](https://www.comunidad.madrid/transparencia/sites/default/files/orden_urgente_dec_primaria_sin_firma.pdf)  

Es tremendamente irregular que se use un trámite urgente de ordenación y currículo de primaria, urgencia que no cuestiono en absoluto, para introducir un cambio totalmente ajeno a ese objetivo.  
En MAIN se limita a indicar "Por último, la disposición adicional única referida al régimen de conciertos educativos se incorpora para responder a las necesidades de crecimiento de las enseñanzas de Formación Profesional que, de manera exponencial, han aumentado en los últimos años."

Sin cuestionar que haya que responder a una necesidad urgente, se debe hacer de manera correcta en un trámite separado, ya que parece razonable que se realice trámite de audiencia para ver si la respuesta es únicamente vía conciertos de centros privados, como se hace en esta disposición adicional, o vía plazas públicas.

La tramitación como disposición adicional además de irregular hace que pueda pasar desapercibido por personas que quieran realizar alegaciones sobre el tema pero ignoren que se trata en base al título del proyecto de ley.

---

09/885212.9/22

**9 mayo 2022**

[El clasismo de Ayuso - eldiario.es](https://www.eldiario.es/escolar/clasismo-ayuso_132_8979451.html)  
Gran parte de las políticas públicas de la Comunidad de Madrid de Isabel Díaz Ayuso funcionan como Robin Hood pero al revés: robar el dinero a los pobres para dárselo a los ricos 

Al tiempo que recortaba en vagones de Metro, hace unas semanas, la Comunidad de Madrid [anunció un programa de becas de 73 millones de euros](https://www.eldiario.es/madrid/ayuso-financiara-bachillerato-fp-privados-familias-ingresan-100-000-euros_1_8924902.html) para apoyar a las familias que pagan estudios privados de la Formación Profesional o el Bachillerato. Se trata de ayudas de 3.000 euros anuales por estudiante que, [según el boletín oficial](https://bocm.es/boletin/CM_Orden_BOCM/2022/04/19/BOCM-20220419-42.PDF) de la Comunidad de Madrid, estarán destinadas a las “rentas bajas”. ¿Suena bien? El demonio está en los detalles. 

El Gobierno de Ayuso se ha inventado una nueva definición de “renta baja”. Para la Comunidad de Madrid, a efectos de acceder a estas becas, una renta baja es una familia de tres miembros –una pareja y un hijo– que gane menos de 107.000 euros al año. O una familia de cuatro que gane menos de 143.000 euros al año.  

Dato importante: la renta media de los hogares madrileños es de 37.051 euros anuales, casi cuatro veces menos. 

Como estos centros privados suelen costar más de 3.000 euros anuales, en la práctica son las familias acomodadas las grandes beneficiarias de estas becas. 

A esto, en teoría política, se le llama regresividad fiscal. Y en la práctica funciona como Robin Hood pero al revés: consiste en quitarles el dinero a los pobres para dárselo a los ricos.

**21 junio 2022**

[Conciertos educativos: más vale lo bueno conocido… - ecmadrid.org](https://www.ecmadrid.org/es/blog-ecmadrid/conciertos-educativos-mas-vale-lo-bueno-conocido)  
Finalizó el sorprendente episodio de la amenaza de supresión del concierto educativo del que disfrutaban los “once irreductibles”, es decir, los centros de FP de Grado Superior que recuperaron su concierto por sentencia del Tribunal Supremo. Como ya dijimos, sólo cabe felicitarnos por ello y agradecer a la Consejería de Educación que pusiera los elementos para posibilitar ese feliz desenlace.  

**9 diciembre 2022**

Asociado a la reforma del delito de malversación conozco una sentencia de TC que hubiera aplicado al tema de usar una disposición adicional para algo nada relacionado.

[Sentencia TC 172/2020](https://www.boe.es/buscar/doc.php?id=BOE-A-2020-16819)  

En primer lugar, los recurrentes alegan que la disposición final primera de la Ley Orgánica 4/2015 no guarda conexión alguna con la norma reformada –la Ley Orgánica 4/2000, de 11 de enero, sobre derechos y libertades de los extranjeros en España y su integración social–, lo que representa un fraude del procedimiento parlamentario de debate de los proyectos y proposiciones de ley que vulnera el art. 23.2 CE (SSTC 119/2011, de 5 de julio, y 136/2011, de 13 de septiembre). 

[Sentencia TC 119/2011. Vulneración del derecho al ejercicio del cargo parlamentario: admisión como enmiendas de unos textos que no guardaban relación material alguna con la iniciativa legislativa a enmendar.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2011-13309)  

> 2) Declarar la nulidad de los acuerdos de la Mesa del Senado de 2 y 3 de diciembre de 2003, por los que se admitieron a trámite las enmiendas núm. 3 y 4 presentadas por el Grupo Parlamentario Popular al proyecto de Ley Orgánica complementaria de la Ley de arbitraje.  

[Sentencia TC 136/2011.... validez de las disposiciones legales que, no incluidas en una ley de presupuestos, modifican tributos o regulan materias no directamente relacionadas con la ejecución de los presupuestos o la política económica del Gobierno](https://www.boe.es/buscar/doc.php?id=BOE-A-2011-16024)  
> Desestimar el presente recurso de inconstitucionalidad.

Pero tiene votos particulares y uno discrepa  

> La admisión y aprobación de enmiendas sin relación con el texto (o textos, en su caso) de una iniciativa legislativa, sino referidas a otras leyes distintas, supone un evidente fraude a la Constitución –que claramente distingue, como ha quedado expuesto, entre iniciativa y enmienda– conforme hemos tenido ocasión de advertir recientemente en nuestra STC 119/2011
 
### Qué supone tener FP privada

Comentado en post [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html): en FPB son los centros privados los que deciden inicialmente si titular tras aprobar las materias. Con LOMLOE se ha pasado a titular siempre en FPB si aprueba las materias, y ahora está en mano del centro aprobar las materias para dar el título.

Creo que es objetivo que cuando hay intereses privados (y a veces pasa también en universidades públicas "privatizadas políticamente", se puede pensar en casos de Cifuentes y Casado) las calificaciones y las titulaciones se pueden "comprar" / pueden estar infladas por presiones de los "clientes".

Por ejemplo se puede ver sobre nivel general de alumnado de centros privados frente a los públicos, adicional al efecto académico de la segregación socioeconómica general (ver [Datos segregación educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-segregacion-educacion.html))

Ver [Estadísticas pruebas acceso universidad](https://algoquedaquedecir.blogspot.com/2020/06/estadisticas-pruebas-acceso-universidad.html)

[twitter Picanumeros/status/1403411766131019780](https://twitter.com/Picanumeros/status/1403411766131019780)  
El mes de junio siempre es convulso en Twitter España por motivo de la Selectividad (Evaluación para el Acceso a la Universidad, EvAU).  
@Vdot_Spain me sugirió analizar el efecto de la titularidad del centro (público, privado, concertado) sobre las calificaciones, y así hice   
...  
3) La caída en el porcentaje de sobresalientes en bachiller y en la fase general es mayor en el alumnado de centros priv/conc que en el alumnado de centros públicos. Esto podría indicar que los sobresalientes en los priv/conc rinden un poco peor que sus pares en los públicos.  
3b) Sin embargo, esto no implica que *todo* el alumnado de la privada/concertada sea (o rinda) peor, y se ve que en la fase general siguen estando por delante; esto aplicaría sólo a los *sobresalientes* de la privada/concertada, que es donde se observa esa mayor caída.  

Además está el efecto de delegar en empresas la formación y correr el riesgo de que utilicen al alumnado como mano de obra sin formación real. Educación no como servicio público.

[Amazon Paid for a High School Course. Here’s What It Teaches.](https://www.vice.com/en/article/bvndja/amazon-paid-for-a-high-school-course-heres-what-they-teach)  

### Empresas privadas haciendo negocio con FP

Las empresas privadas si entran en la FP es para ganar dinero con la formación, aparte hacer negocio con personal a bajo precio.

Reducir la oferta pública es necesario para hacer crecer la privatización; durante 2021 se visualiza la entrada de fondos de inversión en educación.

**6 agosto 2021**

[Unidas Podemos pide la comparecencia del consejero de Educación de Madrid por el déficit de plazas en FP - publico.es](https://www.publico.es/politica/unidas-pide-comparecencia-consejero-educacion-madrid-deficit-plazas-fp.html)  
El diputado Agustín Moreno advierte sobre el "desprecio" del gobierno de Isabel Díaz Ayuso hacia la Formación Profesional. CCOO ha denunciado este viernes  que "más de la mitad" de los jóvenes madrileños que querían estudiar un Grado Superior de Formación Profesional se han quedado sin plaza.

**9 agosto 2021**

[RESOLUCIÓN de 23 de julio de 2021, de la Dirección General de Educación Concertada, Becas y Ayudas al Estudio, por la que se da publicidad a las autorizaciones y modificaciones o extinciones de la autorización de centros docentes privados aprobadas por la Consejería de Educación y Juventud en el cuarto trimestre de 2020.](https://www.bocm.es/boletin/CM_Orden_BOCM/2021/08/09/BOCM-20210809-7.PDF)  

**26 agosto 2021**

[La empresa malagueña Medac, líder de la FP privada, vendida por 200 millones al fondo americano KKR - diariosur.es](https://www.diariosur.es/economia/empresas-malaguenas/empresa-malaguena-medac-vendida-fondo-americano-kkr-20210826125936-nt.html)  
La sociedad inversora sigue avanzando hacia su objetivo de construir un gigante de la educación en España, tras adquirir Master-D y el Instituto Técnico de Estudios Profesionales

**27 agosto 2021**

[twitter Toni_Valero/status/1431165881564942343](https://twitter.com/Toni_Valero/status/1431165881564942343)  
Los fondos de inversión se están haciendo con todo, con el beneplácito del gobierno andaluz  
1° limitan la oferta pública por debajo de la demanda.  
2° licitan a empresas privadas la apertura de centros de FP  
3° fondos de inversión se hacen con esas empresas  
La privatización de la educación va viento en popa: aumentan los conciertos mientras se cierran líneas públicas, se licitan centros privados de FP mientras se limita la oferta pública. Muchos jóvenes se ven obligados a ir a la privada por la falta de plaza en la pública.  

[El fondo de inversión neoyorquino KKR pesca en Málaga: casi 200 M por la líder en FP Medac - elespanol.com](https://www.elespanol.com/malaga/economia/20210827/fondo-inversion-neoyorquino-kkr-malaga-fp-medac/607190571_0.html)  
El Instituto Oficial de FP se sume a otras empresas recientemente adquiridas por la firma estadounidense para conformar un auténtico gigante de la educación en España.

**17 septiembre 2021**

[La FP y el techo de cristal de los chicos de la clase obrera - eldiario.es](https://www.eldiario.es/opinion/tribuna-abierta/fp-techo-cristal-chicos-clase-obrera_129_8300359.html)  
Una comunidad como Madrid, que tiene un 35% de tasa de paro juvenil y una alta precariedad laboral, no puede cometer el disparate de dejar sin atender a una buena parte de la demanda de formación de los jóvenes  
Son un total de 24.914 solicitudes rechazadas de personas, obligados ahora a ir centros privados y concertados o a quedarse en su casa por no poder pagar tasas de 7.000 a 10.000 euros. Este desastre de la FP en Madrid pone de manifiesto que no estudia quien quiere sino quien puede.

**18 septiembre 2021**

[Los fondos de inversión buscan negocio en la FP ante la escalada de demanda y la escasez de plazas - elpais](https://elpais.com/educacion/2021-09-18/los-fondos-de-inversion-buscan-negocio-en-la-fp-ante-la-escalada-de-demanda-y-la-escasez-plazas.html)  
La compra de una red de centros por parte de un fondo estadounidense por 200 millones evidencia el auge de los estudios profesionales. La privada se abre un gran hueco allí donde no llega la pública

Incluye gráficas de alumnado FPGS por titularidad: España, Madrid y Cataluña

**21 septiembre 2021**

[Ayuso y el abandono de la educación pública madrileña - publico.es](https://blogs.publico.es/otrasmiradas/52096/ayuso-y-el-abandono-de-la-educacion-publica-madrilena/)  
El escándalo de la Formación Profesional (FP):  Una grave crisis de matriculación en la región. En julio, se quedaron 6.938 personas sin poder matricularse en Ciclos Formativos de Grado Medio y 17.976 sin una plaza pública en ciclos de Grado Superior (NOTA 1). Ello suma 24.914 solicitudes rechazadas, la mayoría de las cuales tampoco encontrarán plaza pública en septiembre. Se les está obligando a buscarse la vida en centros privados y concertados o a quedarse en su casa. El déficit de plazas y la crisis en el proceso de admisión es estructural por la falta de inversión y, sobre todo, por una brutal estrategia privatizadora. Ello está afectando a la equidad y a sectores del alumnado con menos recursos que eligen la FP como itinerario formativo. Negar una mayor cualificación profesional a decenas de miles de jóvenes, es abocarles al paro, la precariedad y los bajos salarios, en una comunidad con un 35% de paro juvenil. 

**2 octubre 2021**

[El negocio redondo de las FP: así se llena la educación profesional de empresas privadas - elconfidencial](https://www.elconfidencial.com/amp/espana/2021-10-02/pelotazo-formacion-profesional-fp-fondos_3298273/)  
Los cambios en los criterios para montar un centro de formación profesional y las campañas de promoción institucional atraen a las empresas ante la falta de plazas públicas  
El repentino interés de los fondos privados por este tipo de formación se debe, por un lado, a la alta demanda que ha experimentado la FP en los últimos años y, por el otro, a los cambios normativos que han liberalizado la entrada de estos fondos, tanto en su modalidad presencial como a distancia.  
Por ejemplo, actualmente, ni en Cataluña ni en Madrid existe una limitación sobre qué ciclos pueden impartirse o no a distancia. En la primera comunidad nunca la ha habido y en la segunda se levantó en 2015. Además, desde ese año, los centros privados madrileños no tienen que adscribirse a un instituto público que lleve un control sobre las matriculaciones y expedientes de los alumnos. “Desde entonces, depende de la seriedad y honestidad del centro privado, pero pueden colarla muy fácilmente. Puede pasar, por ejemplo, que un alumno se saque un grado sin tener la ESO o la FP básica, porque la inspección no llega a todo y siempre te van a pedir el último título”, cuenta María Eugenia Alcántara, inspectora de Educación de Madrid que ha visto como el número de centros de FP que controla ha pasado de uno a cuatro en dos años. “A partir de ese cambio, el crecimiento ha sido exponencial porque se les ha dado muchas facilidades y ahora mismo cualquiera puede abrir un centro”, añade la también secretaria de Política Social e Igualdad de la Federación Regional de Enseñanza de CCOO.

**8 octubre 2021**

[Madrid anima a sus alumnos a estudiar grados de FP que no quieren cursar - cadenaser](https://cadenaser.com/emisora/2021/10/08/radio_madrid/1633673147_037094.html)  
El criterio de empleabilidad prevalece en el modelo de 'libertad educativa' madrileño por delante de la demanda de los alumnos  
Otros 4.500 alumnos de FP, en este caso a distancia, se han quedado sin plaza pública en su primera opción

[La enseñanza privada pide un cheque escolar para los estudios de FP - magisnet](https://www.magisnet.com/2021/10/la-ensenanza-privada-pide-un-cheque-escolar-para-los-estudios-de-fp/)  
La Asociación Española de Centros Autónomos de Enseñanza Privada (Acade) ha pedido a la ministra de Educación y Formación Profesional, Pilar Alegría, un cheque escolar para los estudios de FP, después de que miles de alumnos se hayan quedado sin plaza en los centros públicos ante la gran demanda.

**24 octubre 2021**

[Los fondos de capital riesgo ponen sus manos en la educación española - elpais](https://elpais.com/economia/negocios/2021-10-24/los-fondos-de-capital-riesgo-ponen-sus-manos-en-la-educacion-espanola.html)  
Los grandes inversores internacionales intensifican la compra de grupos educativos nacionales. Buscan altas rentabilidades en un sector en plena transformación  
En este contexto cobran todavía más sentido los últimos movimientos que están mirando a la formación profesional —el fondo estadounidense KKR compró este verano la red de centros de FP Medac— para completar junto a las universidades privadas —que ya han ganado algo más de 150.000 alumnos en la última década, mientras las públicas han perdido 2.600 estudiantes— ecosistemas que puedan atender desde todos los flancos las nuevas demandas. La FP de grado superior, colocada en un escalón educativo similar al de los estudios universitarios, es la que más interés está despertando entre el capital privado.  
...  
Con la compra de Medac el pasado agosto, KKR completa una operación que inició con las adquisiciones del Instituto Técnico de Estudios Profesionales (ITEP) y de MásterD, centro especializado en preparación de oposiciones y cursos de FP.  
Investindustrial, la gestora de capital riesgo que dirige Andrea C. Bonomi, por su parte, compró por 60 millones en mayo pasado uno de los clásicos de la formación a distancia, Ceac, y Deusto Formación. CCC, el gigante de la formación a distancia, cambió de manos el mes pasado tras ocho décadas en manos de la familia Azcárate y ahora es propiedad del grupo inversor suizo Crescendo, que pretende disparar el negocio en Latinoamérica. Y el fondo Magnum, que a través de Digital Talent controla el Instituto Superior para el Desarrollo de Internet (ISDI) y el Instituto Superior de Derecho y Economía (ISDE), se hizo el mes pasado con el Centro de Estudios Superiores de la Industria Farmacéutica (asociado al CEU), el Instituto Superior de Estudios Psicológicos y el Centro Europeo de Estudios Profesionales (de FP), con las que ha creado el grupo Metrodora Education, especializado en formación sanitaria.  
...  
… “El negocio de la educación no está solo en el hecho de que hay mucha inversión privada, sino que hay también mucho dinero público”, insiste el profesor de Sociología de la Universidad Autónoma de Barcelona Toni Verger.Y no se trata solo de esas tecnologías, [sino de las escuelas concertadas ya mencionadas](https://elpais.com/educacion/2020-12-21/los-claroscuros-de-la-concertada-los-frenos-de-la-ley-celaa-a-la-eleccion-del-alumnado.html), que obtienen esos 6.600 millones de dinero público más otros 2.200 millones que se gastan las familias, según las últimas cifras del INE.  

**28 octubre 2021**

[Scientia School: "Duplicaremos los ingresos este año hasta 10 millones de euros" - eleconomista](https://www.eleconomista.es/mercados-cotizaciones/noticias/11454135/10/21/Scientia-School-Duplicaremos-los-ingresos-este-ano-hasta-10-millones-de-euros.html)  
Scientia School es la última incorporación española a Euronext, con una capitalización superior a los 100 millones de euros. Con cinco colegios concertados en propiedad y otros dos centros de formación profesional, su objetivo es crecer a base de implementar en el sistema educativo la transformación digital.  
...  
El colegio es la parte que da estabilidad, mientras que la parte de tecnología ofrece la mayor aportación al ebitda [80%]. No se entiende un beneficio de los centros concertados. Son las otras empresas del grupo las que prestan servicios complementarios. 

**4 noviembre 2021**

[Ayuso deja fuera de sus presupuestos el 40% de la demanda de plazas de FP - cadenaser](https://cadenaser.com/emisora/2021/11/04/radio_madrid/1636005630_344288.html)  
Madrid prevé en sus cuentas unas 81.500 plazas públicas de Formación Profesional, aunque ahora mismo la región cuenta con 136.000 alumnos  
En dos años, la oferta de plazas públicas ha crecido un 17% menos que la demanda

**11 noviembre 2021**

[Quién es quién detrás de los colegios privados en España - expansion](https://www.expansion.com/empresas/2021/11/11/61010d78e5fdea04588b45eb.html)  
Hay 9.427 colegios privados en España, pero sólo una decena de empresas tiene un peso relevante en un sector muy atomizado. En los últimos años, grupos internacionales y fondos de inversión han protagonizado destacadas operaciones revolucionando el negocio educativo.  

También se puede ver la idea de puertas giratorias en FP.

En Madrid la que fue directora de FP y potenció la FP privada pasa a ser directora de FP de CCC

[twitter FiQuiPedia/status/1424116414949036035](https://twitter.com/FiQuiPedia/status/1424116414949036035)  
Quizá @Guadalupbragado, desde su nuevo puesto en FP privada, pueda desglosar ya que no lo hacen @IdiazAyuso ni @eossoriocrespo  
[DECRETO 142/2015, de 9 de julio, del Consejo de Gobierno, por el que se nombra Directora General de Formación Profesional y Enseñanzas de Régimen Especial a doña Guadalupe Bragado Cordero.](http://www.bocm.es/boletin/CM_Orden_BOCM/2015/07/10/BOCM-20150710-15.PDF)  
![](https://pbs.twimg.com/media/E8N6Cr1WUAEirtH?format=png)  

**2 marzo 2022**

[MEDAC amplía su oferta educativa y abre 11 nuevos centros formativos de FP - elespanol.com](https://www.elespanol.com/sociedad/educacion/20220302/medac-amplia-oferta-educativa-centros-formativos-fp/651185134_0.html)  
El Instituto de Formación Profesional desembarca por primera vez en Asturias con un centro en Oviedo y sigue creciendo en Madrid, Zaragoza, Andalucía y Levante.

**8 marzo 2022**

[La Formación Profesional, el nuevo nicho de mercado de la educación privada - eldiario.es](https://www.eldiario.es/sociedad/formacion-profesional-nuevo-nicho-mercado-educacion-privada_1_8781541.html)  
La gran demanda de plazas junto a la insuficiente respuesta pública de las comunidades dispara el alumnado que acude a centros privados en una vía formativa que empieza a atraer la atención de los fondos de inversión
![](https://pbs.twimg.com/media/FMykrsJWQAUgU44?format=jpg)  

**18 marzo 2022**

[La ley del silencio dentro y fuera del aula - elpais](https://elpais.com/educacion/secundaria-bachillerato-fp/2022-03-18/la-ley-del-silencio-dentro-y-fuera-del-aula.html)  
El grupo Medac de enseñanza FP, fundado entre otros por Javier Imbroda, el actual consejero andaluz de Educación, impone cláusulas de confidencialidad a sus profesores, que denuncian prácticas laborales irregulares

**21 julio 2022**

[CCOO denuncia: hay "interés de los fondos de inversión" en la FP de Madrid - lavanguardia](https://www.lavanguardia.com/vida/20220721/8423429/ccoo-denuncia-hay-interes-fondos-inversion-fp-madrid.html)  

**6 octubre 2024**

[Los fondos de inversión hacen su gran desembarco en la FP, el nuevo filón de la educación privatizada - eldiario](https://www.eldiario.es/sociedad/fondos-inversion-gran-desembarco-fp-nuevo-filon-educacion-privatizada_1_11687076.html)

Pendiente: Artículo ECatólicas con números, presupuestos. 

### FP Dual

La descripción oficial de FP dual no diferencia de FP normal, ya que en todos los ciclos de FP hay FCT (Formación en Centros de Trabajo)

[Cómo, Cuándo y Dónde estudiar > Cómo estudiar > FP Dual - todofp.es](https://www.todofp.es/como-cuando-y-donde-estudiar/como-estudiar/formacion-profesional-dual.html)

[Cómo, Cuándo y Dónde estudiar > Cómo estudiar > FP Dual > Preguntas frecuentes- todofp.es](https://www.todofp.es/como-cuando-y-donde-estudiar/como-estudiar/formacion-profesional-dual/preguntas-frecuentes.html)  
> ¿Qué es la FP Dual?  
Es una modalidad dentro de las enseñanzas de Formación Profesional en la que se combina la formación teórico-práctica recibida en un centro educativo con la actividad práctica en un centro de trabajo.

Una definición rápida de la FP Dual es una modalidad de FP en la que la formación en la empresa tiene más peso en % de horas

> Se realizará un mínimo del 33% de las horas de formación en empresas incluyendo la Formación en Centros de Trabajo. Este porcentaje podrá ampliarse en función de las características de cada módulo profesional y de la empresa participante.

En la nueva ley de FP se potencia la FP dual

Trámite de audiencia junio 2021  
[Anteproyecto de Ley Orgánica Ordenación e Integración de la Formación Profesional.](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2021/anteproyecto-lo-ordenacion.html)  

[Texto ley orgánica FP (pdf)](https://www.educacionyfp.gob.es/dam/jcr:70853d4b-db9c-4533-a22a-ece434561786/texto-lo-fp.pdf)  

> Por otra parte, toda la formación profesional tendrá carácter dual.  
...  
El carácter dual ya no tiene solo en cuenta el tiempo en la empresa, sino la calidad del mismo y los resultados de aprendizaje que se trabajan durante la estancia.  
...  
se establece un margen de duración de la fase dual, **mínima del 25% y máxima del 50%** de la totalidad de la formación

[twitter Pilar_Alegria/status/1450808650939961350](https://twitter.com/Pilar_Alegria/status/1450808650939961350)  
La nueva #LeyFP es un proyecto de país:  
...   
🔴Que las empresas apuesten por la formación dual

[twitter JuanJoseJuarez/status/1456231416702832642](https://twitter.com/JuanJoseJuarez/status/1456231416702832642)
@mjansen_madrid en #7CongresoFP dice, "si las empresas necesitan profesionales en lugar de quejarse deben mojarse" y según sus estudios la participación en la #FPDual produce un aumento en la tasa de retención de candidatos del 12% frente a FP tradicional.   

Imagen cita desventajas

> Sin una adecuada regulación, la formación ofrecida por parte de las empresas podría resultar específica.  
> La posible falta de competencias transversales puede dificultar la adaptación a cambios tecnológicos.  

### Fondos europeos y FP

**15 octubre 2021** 

[El presupuesto de becas y ayudas del Ministerio de Educación y Formación Profesional sube un 44% desde 2017 - lamoncloa.gob.es](https://www.lamoncloa.gob.es/serviciosdeprensa/notasprensa/educacion/Paginas/2021/151021-becas.aspx)  
Impulso a la Formación Profesional  
La Formación Profesional para el Empleo es el segundo programa con mayor financiación, con 1.151 millones de euros, lo cual supone un incremento del 24,46% respecto al año anterior. El MEFP asumió estas competencias en 2020, dentro del proceso de modernización de la Formación Profesional. El [proyecto Ley Orgánica de Ordenación e Integración de la Formación Profesional](https://www.lamoncloa.gob.es/consejodeministros/resumenes/Paginas/2021/070921-cministros.aspx), actualmente en trámite parlamentario, diseña un único sistema que integra la formación profesional de estudiantes y trabajadores empleados y desempleados.  

[twitter educaciongob/status/1450069165847482371](https://twitter.com/educaciongob/status/1450069165847482371)  
El @boegob publica hoy la distribución a las CCAA de 301 M€ del Plan estratégico de impulso a la FP dentro del #PlanDeRecuperación. Son fondos para 2021 para acreditación de competencias profesionales, creación de nuevas plazas o digitalización.  
[Resolución de 7 de octubre de 2021, de la Secretaría General de Formación Profesional, por la que se publica el Acuerdo de la Conferencia Sectorial de Educación de 21 de julio de 2021, por el que se aprueba la propuesta de distribución territorial y los criterios de reparto de los créditos gestionados por Comunidades Autónomas en el marco del componente 20 "Plan estratégico de impulso a la Formación Profesional", del Plan de Recuperación, Transformación y Resiliencia, en el ejercicio presupuestario 2021.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-16952)  
![](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-16952)  
Madrid 29 millones de euros  

**16 noviembre 2021**

[Referencia del consejo de ministros - lamoncloa.gob.es](https://www.lamoncloa.gob.es/consejodeministros/referencias/Paginas/2021/refc20211116.aspx)  
Fondos Europeos. Mecanismo de Recuperación y Resiliencia. Componente 20. ACUERDO por el que se autoriza la propuesta de distribución territorial y los criterios de reparto de los créditos gestionados por comunidades autónomas destinados al desarrollo de acciones de Reskilling y Upskilling de la población activa en el ejercicio presupuestario 2021, para su sometimiento a la Conferencia Sectorial del Sistema de Cualificaciones y Formación Profesional para el Empleo, en el marco del Componente 20 "[Plan estratégico de impulso de la Formación Profesional](https://www.lamoncloa.gob.es/consejodeministros/referencias/Paginas/2021/refc20211116.aspx#trabajadores)", del Mecanismo de Recuperación y Resiliencia (MRR), para la formación de cualificación y recualificación de la población activa, vinculada a cualificaciones profesionales en sectores estratégicos, cuidado de las personas y zonas en riesgo de despoblación, por un importe de 87,7 millones de euros.

### Atacar a lo público / atacar a lo privado

Cuando se muestra el incremento en educación publica vs privada, se habla de "atacar", "ir contra" el otro.

Cuando se cita que crecen los fondos a los privados se escuchan cosas del estilo "no se trata de atacar a los privados con concierto, sino centrarse en mejorar la educación pública. Deben coexistir pública y privada subvencionada, ambas prestan un servicio público"

La clave es que pública y privada son excluyentes: el % de gasto público en educación pública es justo el % complementario al % de gasto público en educación privada (vía concierto o vía otro tipo de subvenciones). Y la educación privada son empresas que buscan beneficio y globalmente segregan, por lo que no prestan realmente el servicio público de educación. Ver [No existe ningún centro que sea concertado](https://algoquedaquedecir.blogspot.com/2018/02/no-existe-ningun-centro-que-sea-concertado.html) donde se comentan ideas sobre servicio público y se comentan ejemplos de educación no centrados solo en FP, como el de noviembre 2021 

El informe recalca que la mayor parte de centros privados conveniados con la Junta se concentra en zonas donde el negocio es rentable, es decir, en las zonas de las grandes ciudades con rentas altas, especialmente en Sevilla, Málaga y Córdoba. Esta radiografía trasluce un desfase entre oferta y demanda. "El modelo de oferta privada basado en una lógica de mercado no resulta suficiente para responder a la demanda ni a las necesidades sociales", reza el documento.

...

Las principales patronales del sector, como ACES, Coordinadora de Escuelas Infantiles y Escuelas Infantiles Unidas, que engloban tanto a franquicias y empresas como a pequeñas cooperativas de madres de alumnos, respaldan el criterio del Gobierno andaluz, conscientes de que la implantación de un sistema público 100% de guarderías terminaría por cerrarles el negocio. 

**1 diciembre 2021**

[El Gobierno obliga a las comunidades autónomas a aumentar el número de plazas públicas de FP - elmundo.es](https://www.elmundo.es/espana/2021/12/01/61a7a11afc6c834b2b8b4580.html)  
Malestar en la educación concertada por un cambio de última hora en la ley orgánica que "va en contra del consenso"

### Privatización en general

Ya comentadas ideas de servicio público en post separado

[La campaña en Francia que triunfa en las redes: "Cuando todo sea privado, estaremos privados de todo. Viva el servicio público" ](https://www.publico.es/tremending/2021/10/25/la-campana-en-francia-que-triunfa-en-las-redes-cuando-todo-sea-privado-estaremos-privados-de-todo-viva-el-servicio-publico/)  
Vive le service public. Quand tout sera privé, on sera privé de tout  
![](https://www.publico.es/tremending/wp-content/uploads/2021/10/FCbSDKWXIAw4I7c-526x439.jpg)

[Cuando todo sea privado seremos privados de todo - coordinacionbaladre.org](https://www.coordinacionbaladre.org/noticia/cuando-todo-sea-privado-seremos-privados-todo)  

[privatisation-resistance.fr - archive.org](https://web.archive.org/web/20210619035232/https://privatisation-resistance.fr/)  

[https://twitter.com/BasitMahmood91/status/1557653724566364161](https://twitter.com/BasitMahmood91/status/1557653724566364161)  
The only chart you’ll need when the Tories tell you they want to level up and all people need to do progress is work hard.  
![](https://pbs.twimg.com/media/FZ3lwPRWQAA0GqF?format=jpg)  

The graph is based on data from the IFS which can be found here:
[The growing gap between state school and private school spending](https://ifs.org.uk/publications/15672)

The data was used to produce the same graph for the New Statesman here:

[How the funding gap between state schools and private schools has dramatically widened](https://newstatesman.com/chart-of-the-day/2021/10/how-the-funding-gap-between-state-schools-and-private-schools-has-dramatically-widened)  

**8 septiembre 2022**

[Radiografía de un saqueo: la educación en el Madrid de Ayuso - publico.es](https://blogs.publico.es/otrasmiradas/63410/biografia-de-un-saqueo-la-educacion-en-el-madrid-de-ayuso/)  
Privatización galopante, que ha provocado que el alumnado escolarizado en los centros públicos sea del 54% en Madrid frente al 68% en España y a más del 90% en los países europeos. En el caso de Madrid capital, el alumnado de la pública no llega al 40%. Este destrozo se consigue por varias vías: deteriorando la educación pública con los recortes, negando la demanda de las familias de plazas públicas y la construcción de nuevos centros públicos, transfiriendo recursos inmensos a la concertada (1.300 millones este año), y con la política de cheques-regalo a familias ricas con ingresos de 143.652 €/año si son cuatro personas

**13 mayo 2023**

[El cambio de enfoque de las empresas que se meten en la educación: de ofrecer recursos para el aula a modelar políticas - eldiario.es](https://www.eldiario.es/sociedad/cambio-enfoque-empresas-meten-educacion-ofrecer-recursos-aula-modelar-politicas_1_10091576.html)  
Compañías energéticas, tecnológicas o bancos ofrecen recursos para el uso del profesorado en clase bajo el axioma, sin justificar, de que es necesario cambiar las prácticas docentes, según explica una investigación de la Universidad Autónoma de Madrid
