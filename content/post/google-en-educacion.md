+++
author = "Enrique García"
title = "Google en educación"
date = "2021-06-11"
tags = [
    "educación", "google", "post migrado desde blogger"
]
toc = true

description = "Google en educación"
thumbnail = "https://upload.wikimedia.org/wikipedia/commons/3/32/Google_for_Education_logo.svg"
images = ["https://upload.wikimedia.org/wikipedia/commons/3/32/Google_for_Education_logo.svg"]

+++

Revisado 24 agosto 2024

## Resumen

Recopilo y comparto información sobre el uso de Google en educación. Hay aspectos asociados a huella digital y privacidad que enlazan con RGPD. Además de administraciones educativas firmando convenios con Google, hay docentes y centros que lo usan sin conocer RGPD.

En 2022 varias administraciones de varios países prohíben Google en educación.

En 2023 hay una sanción de AEPD a la Consejería de Educación de Canarias por el uso de Google 

En 2024 hay una sanción de AVPD a la Consejería de Educación de País Vasco por el uso de Google 

Posts relacionados:
* [Google en educación: Madrid](../google-en-educacion-madrid)  
* [Normativa webs, aplicaciones y protección de datos en educación](https://algoquedaquedecir.blogspot.com/2019/06/normativa-webs-aplicaciones-y-proteccion-datos.html) (además de ideas generales como normativa sobre uso de aplicaciones, se comentan cosas comunes a varias plataformas como Google forms y Microsoft forms)  
* [Microsoft Teams en educación Madrid](https://algoquedaquedecir.blogspot.com/2020/06/microsoft-teams-en-educacion-madrid.html)  (aunque tiene ese título cito otras comunidades)  
* [Ejemplos RGPD en educación](https://algoquedaquedecir.blogspot.com/2022/09/ejemplos-rgpd-en-educacion.html)
* [EducaMadrid: qué y por qué](https://algoquedaquedecir.blogspot.com/2021/01/educamadrid-que-y-por-que.html) (para mostrar qué cosas no son EducaMadrid, como algunas cosas propietarias)  

## Detalle

La idea general es que nada es gratis: si te ofrecen un servicio gratis es probable que lo que estén ganando es recoger datos. Si eso además pasa con educación de menores, puede ser grave y hay que tener toda la información posible de qué se hace y poder demostrar que se hace bien/qué se hace mal.

Aquí trato Google, pero aplica a otros como Microsoft, Facebook, ...

### Introducción: el caso de Comunidad Valenciana

Comienzo poniendo aquí este hilo de 28 febrero 2020, que cita varias referencias de Google en educación. En post sobre Teams se puede ver que en diciembre 2020 la Comunidad Valenciana firma convenio con Microsoft y ya no es "la única región de Europa"

En Madrid a través de EducaMadrid había una plataforma basada en software libre con cierta analogía a lo que se comenta para Comunidad Valenciana, pero se ha mezclado con cosas que no son EducaMadrid. Ver post [EducaMadrid: qué y por qué](https://algoquedaquedecir.blogspot.com/2021/01/educamadrid-que-y-por-que.html)

[twitter manuelabat/status/1233413827334754305](https://twitter.com/manuelabat/status/1233413827334754305)  

Hilo que resume ideas y cita el caso de Comunidad Valenciana
[threader.app/thread/1233413827334754305](https://threader.app/thread/1233413827334754305)  
Hola amigos, hoy no es sábado, sabadete, pero toca hilo de [#privacidad](https://twitter.com/search?q=%23privacidad) y [#eticadelosdatos](https://twitter.com/search?q=%23eticadelosdatos). ¿Sabías que la Comunidad Valenciana es la ÚNICA REGIÓN DE EUROPA que protege la privacidad de sus estudiantes en los colegios públicos a través de herramientas y plataformas [#OpenSource](https://twitter.com/search?q=%23OpenSource)?  
Pero, ¿en qué consiste esta protección a los estudiantes? : cada palabra que tu hijo escribe, cada video que mira, o música que escucha, cómo escribe con el teclado, donde se encuentra físicamente en cada momento… es captado por las herramientas de los gigantes tecnológicos.  
¿Para qué? Para reunir toda esa información y confeccionar lo que se denominan perfiles. ¿Qué pueden extraer e inferir de todos esos datos que extraen de los estudiantes en las escuelas que usan Google o Microsoft?  
Que tu hijo tiene un cuadro de ansiedad, que es disléxico, si sus padres tienen problemas en casa, dónde vive, si tiene dificultades de aprendizaje, qué religión profesa, o su raza, si vive en un barrio socio económico alto o bajo...  
Si tiene tendencias suicidas, que esté sufriendo acoso, si está sufriendo un desequilibrio emocional, o si tiene depresión, si es un superdotado, si es un líder (positivo o negativo).  
¿Y? Me dirás padre, o madre que te da igual porque “no tienes nada que esconder”. Pues que, cuando tu hijo haga carrera política y pueda llegar a ser el líder de su partido o Presidente del Gobierno, salga info suya que diga...  
Usted nunca podrá ser Presidente del Gobierno, o CEO de esta empresa, o cargo de responsabilidad, porque padece dislexia, arrastra un trauma de niño por la separación de sus padres, vivía en un barrio socioeconómico pobre, y se codeaba con lo peorcito.  
¡Upsi! ¿A que ahora resulta que SÍ TENEMOS COSAS QUE ESCONDER?  
Pues padre o madre de la Comunidad Valenciana, ¿sabes que tu gobierno regional está protegiendo a tu hijo para QUE EN SU FUTURO NUNCA JAMÁS LE PASE ESTO?  
Porque se ha esforzado en redactar unas Instrucciones y Resoluciones que impiden bajo cualquier circunstancia que los datos de tus hijos se alojen en la nube, en servidores de gigantes tecnológicos a los que TUS HIJOS LE IMPORTAN TRES PITOS, Y SÓLO le importan sus datos.  
Pero, seamos más concretos aún. Vamos a ver qué está pasando en el resto de Europa, y de España, pero que no pasa en la Comunidad Valenciana.  
No hay nada como demostrar las cosas científicamente y afirmar hechos basados en evidencias. He estado unas dos semanas investigando qué pasa en las escuelas que usan GSuite, y qué repercusión tiene en sus alumnos.  
Fuente de investigación: "Teachers In The Cloud: Teachers’ Feelings Of Comfort Integrating Google Suite For Education". Mayores preocupaciones y problemas son los derivados de la [#Privacidad](https://twitter.com/search?q=%23Privacidad). Nombrada 54 veces.  
[Teachers In The Cloud: Teachers’ Feelings Of Comfort Integrating Google Suite For Education](https://viurrspace.ca/bitstream/handle/10613/13915/Ly.pdf?sequence=1&isAllowed=y)  
![](https://pbs.twimg.com/media/ER36n9MWoAANJcN.jpg)  
["Infraestruturas, economia e política informacional: o caso do google suite for education"](http://www.uel.br/revistas/uel/index.php/mediacoes/article/view/32320)  
![](https://pbs.twimg.com/media/ER37fe1W4AIg0od.jpg)  
Ahora, vamos a hablar de lo que ocurre en las aulas. Resulta que las escuelas que firman un acuerdo con GSuite permiten una vigilancia a dos manos de sus estudiantes.  
Dato. Los datos en los servidores de Google se almacenan en un formato cifrado para que no se puedan leer... A MENOS QUE ALGUIEN CON EL ACCESO NECESARIO, NECESITE DESCIFRARLOS.  
Dato. Google Suite cumple con docenas de estándares para almacenar datos confidenciales, pero estas protecciones NO PROMETEN CIFRADO DE EXTREMO A EXTREMO.  
DATO. Los estudiantes deben censurarse a sí mismos.  
Detección de dislexia de un estudiante al escribir mal los dictados.  
Autocensura - no pueden escribir experiencias personales o información como el divorcio de los padres o la creencia religiosa del hermano en Google Drive.  
“En lugar de invertir en tecnologías seguras y libres de vigilancia, las escuelas confían en soluciones integrales rentables como "G Suite for Education" y transfieren la responsabilidad de la privacidad a maestros y estudiantes.”  
Fuente: 
[Suiza hace cumplir las normas suizas de protección de datos a Google en las aulas.](https://www.republik.ch/2019/12/19/bald-gilt-switzerland-first-bei-google-co)  
“La Inspección de datos (noruega) está investigando si es legal usar Google en la escuela.  
ESTE DEBATE HA PASADO DESAPERCIBIDO, dice el director Bjørn Erik Thon”.
Fuente:  [Datatilsynet undersøker om det er lovlig å bruke Google i skolen](https://www.aftenposten.no/norge/i/pLvba6/datatilsynet-undersoeker-om-det-er-lovlig-aa-bruke-google-i-skolen)  
¿Sabías que los padres, en las escuelas en las que usan GSuite no pueden oponerse a su uso? Porque si lo hacen, discriminan a sus hijos excluyéndolos de las actividades porque no pueden ser grabados, ni fotografiados para después ser colgados en las herramientas de Google.  
Ante la presión y la discriminación de sus hijos, los padres no tienen más remedio que ceder, y regalar la [#privacidad](https://twitter.com/search?q=%23privacidad) de sus hijos.  
Por lo tanto, los niños terminan teniendo una relación de cliente con una gran empresa comercial. Los padres que no desean que sus hijos usen la herramienta no tienen la oportunidad de oponerse.  
Y hay enorme incertidumbre sobre lo que sucede con los perfiles de los niños CUANDO SALEN DE LA ESCUELA.  
Más reacciones en Europa. El acuerdo "Suiza primero" pronto se aplicará a Google.  
[Suiza hace cumplir las normas suizas de protección de datos a Google en las aulas.](https://www.republik.ch/2019/12/19/bald-gilt-switzerland-first-bei-google-co)  
Es decir, que Suiza ha impuesto que GSuite se someta a la ley de protección de datos suiza, que la jurisdicción sea los Tribunales suizos, y que la responsabilidad ante problemas de seguridad, sea de Google.  
PERO UNA COSA SIN RESOLVER:  
Con la "Ley de la Nube", las autoridades de EE. UU. aseguran el acceso a los datos confidenciales de los estudiantes de los gigantes tecnológicos con sede en EE. UU.  
Más reacciones en Europa: Las autoridades holandesas y el grupo de protección de datos de Hesse, se resistieron a la transferencia de datos a los EE. UU: no cumplen con el RGPD.  
El artículo ["Newsrooms, let’s talk about G Suite"](https://freedom.press/training/blog/newsrooms-lets-talk-about-gsuite/)  escrito por un ex-empleado de Google ha sido de los más reveladores.  
El artículo dice que los estudiantes son "espiados" a dos bandas: 1. Los datos de los estudiantes se van a servidores que NO ESTÁN CIFRADOS DE EXTREMO A EXTREMO. Tal y como decía antes, no se leen... hasta que son leídos.  
Y ESTE HECHO LAMENTABLE: Los administradores pueden LEER EL CONTENIDO DE LOS EMAILS y de Google Drive, así como metadatos (por ejemplo, fechas, líneas de asunto, destinatarios).  
Todos estos datos pueden registrarse y conservarse, según el administrador configure G Suite.  
La cuenta Enterprise es la que más niveles de monitorización ofrece.  
Esto hace que las personas se dividan en buenas y malas, según el criterio del administrador. Sin olvidar el acoso al que alguien puede ser sometido, y las fobias que se puedan desatar.  
Las diferentes soluciones que da este ex-empleado de Google son:  
1. Una alternativa cifrada de extremo a extremo, o  
2. Almacenamiento local, o  
3. Fuera de los ordenadores por completo.  
Pues la opción número dos es la que da la Comunidad Valenciana.  
Y, aún así, tenemos preguntas sin responder:  
¿Cuántas personas en Google tienen acceso a los datos de los estudiantes?  
¿Cómo se determina ese acceso?  
¿A qué tipo de datos de usuario tienen acceso y en qué circunstancias?  
(MÁS)  
¿Para qué quieren estos datos?  
¿Cuántas personas pueden extraer datos de usuarios que responden a una solicitud legal?  
NO LO SABEMOS  
Y, ¿qué ocurre en las escuelas públicas de la Comunidad Valenciana?  
Situación general:  
Uso de Software libre.  
Programas de código abierto (Open Source).  
OBJETIVO: Consolidar una solución eficaz y sostenible para los puestos de trabajo del empleado público.  
http://www.dgtic.gva.es/documents/85347/85378/_1475822924381_instruccion-num+-3-fomento-y-uso-software-l.pdf/c0a99f8d-4dfc-437e-bee9-db218ee22b67  
**Enlace no operativo en 2024**  
Artículo 13.3.1. RESOLUCIÓN de 28 de junio de 2018.  
PROHIBIDO transmitir o alojar información de la Generalitat Valenciana en sistemas de información externos, como la nube, excepto autorización expresa de la Conselleria, y previo análisis de riesgos.  
Publicación datos de los alumnos en Redes Sociales:  
Consentimiento inequívoco de las personas implicadas.  
Informar:  
Datos que se publicarán  
En qué redes sociales  
Finalidad*  
Quién accede a los datos*  
Cómo ejercitar los derechos de acceso, rectificación, cancelación y oposición  
La Conselleria de Educación, Cultura y Deporte pone a disposición de los centros educativos:  
- ITACA-Web Familia 2.0. Plataforma de contacto con los responsables familiares.  
- Módulo Docente 2.0.  
(PLATAFORMAS OPEN SOURCE PROPIAS DE LA CONSELLERÍA).  
[RESOLUCIÓN de 5 de julio de 2019 del secretario autonómico de Educación y Formación Profesional, por la que se aprueban las instrucciones para la organización y el funcionamiento de los centros que imparten Educación Infantil de segundo ciclo y Educación Primaria durante el curso 2019-2020.](https://intersindical.org/stepv/docs/2019_7218.pdf)  
Artículo 13.3.a) Resolución 5 de julio:  
“Está expresamente desautorizado el uso de redes sociales que incluyan cualquier tipo de publicidad o que puedan ser utilizadas para una finalidad diferente de la misma comunicación.”  
Se permite el uso de Telegram está aceptado como canal de difusión y comunicación con los padres SIEMPRE QUE NO SE INCLUYA NINGÚN DATO PERSONAL.  
Además, tienen una enorme conciencia del peligro del uso de aplicaciones en el ámbito educativo, expresando esta preocupación, en el mismo artículo 13 la Resolución del 5 de julio:  
![](https://pbs.twimg.com/media/ER4IH7cWkAAhC6Q.jpg) 
Y añaden algo realmente revelador en una Administración Pública: la afirmación de que los dispositivos cedidos por estos gigantes tecnológicos ESPÍAN A LOS ALUMNOS:  
![](https://pbs.twimg.com/media/ER4JC0rW4AA3O1Z.jpg)  
La administración pública está para HACER EL BIEN COMÚN, para proteger a sus ciudadanos, y aún más, a los niños y a los menores.  
La Comunidad Valenciana da ejemplo a España y a Europa, y se ha adelantado 2 años a la gran tendencia global: la ética de los datos.  
En España, la Secretaria de Estado [@carmeartigas](https://twitter.com/carmeartigas) en una entrevista de [@el_pais](https://twitter.com/el_pais) propone liderar en Europa el debate del humanismo tecnológico. No es otra cosa que adquirir la mentalidad ética de la GVA, adelantada a su tiempo.  
[Carme Artigas: “La inteligencia artificial ayudará a controlar a los gobiernos y grandes empresas” - elpais](https://elpais.com/tecnologia/2020/02/21/actualidad/1582290641_645896.html)  
Esto es todo. Como siempre, gracias por leerme.  


Otros enlaces sobre Comunidad Valenciana
[RESOLUCIÓN de 28 de junio de 2018, de la Subsecretaría de la Conselleria de Educación, Investigación, Cultura y Deporte, por la que se dictan instrucciones para el cumplimiento de la normativa de protección de datos en los centros educativos públicos de titularidad de la Generalitat. - gva.es](https://ceice.gva.es/documents/162640623/167743489/2018_11040.pdf/0baf10b6-bfff-4039-8b96-5430a960a9a3)  
[RECOMENDACIÓN 2019-002: USO DE APLICACIONES EXTERNAS POR PARTE DE LOS CENTROS EDUCATIVOS QUE IMPLICAN EL TRATAMIENTO DE DATOS PERSONALES - gva.es](https://ceice.gva.es/documents/161634279/172752165/DPD+Recomendaci%C3%B3n+2019_002.Uso+de+herramientas+externas+en+centros+educativos+p%C3%BAblicos.pdf/c84f7fd2-99bc-467d-8d6f-dd3efea326ac?t=1607099470839)  

### Uso de Google en informe Human Rights Watch 

**25 mayo 2022**

[“How Dare They Peep into My Private Life?”. Children’s Rights Violations by Governments that Endorsed Online Learning During the Covid-19 Pandemic - hrw.org](https://www.hrw.org/report/2022/05/25/how-dare-they-peep-my-private-life/childrens-rights-violations-governments)  
Of the 164 EdTech products reviewed, 146 (89 percent) appeared to engage in data practices that put children’s rights at risk, contributed to undermining them, or actively infringed on these rights.  
...  
Through dynamic analysis commissioned by Human Rights Watch and conducted by the Defensive Lab Agency, Human Rights Watch detected students’ AAID sent from Rumah Belajar to Google and to Facebook. Specifically, children’s AAID were sent to the Google-owned domain app-measurement.com, and to the Facebook-owned domain graph.facebook.com.  

### Gsuite en educación 

Algunos enlaces se citan en introducción

["Teachers In The Cloud: Teachers’ Feelings Of Comfort Integrating Google Suite For Education".](https://https://viurrspace.ca/bitstream/handle/10613/13915/Ly.pdf?sequence=1&isAllowed=y)  

["Infraestruturas, economia e política informacional: o caso do google suite for education"](http://www.uel.br/revistas/uel/index.php/mediacoes/article/view/32320)

**19 mayo 2020**

[Google, doctrina del shock y liquidación de la escuela pública - elsaltodiario](https://www.elsaltodiario.com/el-rumor-de-las-multitudes/google-doctrina-del-shock-y-liquidacion-de-la-escuela-publica)  
Las plataformas educativas no son “herramientas pedagógicas”, sino espacios virtuales cuya estandarización bajo el control de los grandes gigantes tecnológicos puede llevar a la desaparición de la escuela pública.

**30 junio 2020**

[Google Classroom. Seguridad y Cumplimiento Normativo - cni.es](https://www.ccn-cert.cni.es/informes/abstracts/5072-google-classroom-seguridad-y-cumplimiento-normativo/file.html)  

**25 febrero 2020**
[Escándalo en Google: así "espía" a millones de niños en el colegio y en su casa - elmundo.es](https://www.elmundo.es/tecnologia/2020/02/25/5e5459fcfc6c8366368b4577.html)  
Los servicios del gigante de Internet se utilizan en colegios españoles. Recopila datos sobre la localización de los menores, las páginas web que visita, sus grabaciones de voz y su agenda de contactos

[Documento marzo 2020. Ponente: Herminia Gil Pérez. Jefa del Servicio de Protección de Datos de la Delegación de Protección de Datos en la Consejería de Educación y Juventud 917200468. Protección de Datos. Profesorado SAED, PTSC y Orientadores](
https://dpd.educa2.madrid.org/web/educamadrid/principal/files/69a6601a-e199-4519-a2eb-192663576204/Presentaciones/CTIFNORTE%20SAED-PTSC-ORIENTADORES/1.%20PARTE%20II.%20PROTECCION%20DATOS%20.PROF%20SAED-PTSC-ORIENTADORES.pdf?t=1583934301833#page=38)   
En página 38 se habla de Gsuite for education  
En página 48 se habla de Cloud Act  

**29 septiembre 2020**

[Federal Judge Dismisses New Mexico’s Lawsuit Against Google](https://www.govtech.com/security/federal-judge-dismisses-new-mexicos-lawsuit-against-google.html)  
In the February lawsuit, the state of New Mexico alleged Google was using free Chromebook computers to scoop up federally protected personal data, but it has now been dismissed by a federal judge.

**2 diciembre 2020**

[New Mexico AG Files Notice of Appeal in Suit Against Google Regarding Alleged Violations of COPPA](https://www.huntonprivacyblog.com/2020/12/02/new-mexico-ag-files-notice-of-appeal-in-suit-against-google-regarding-alleged-violations-of-coppa/)  

**16 abril 2021**

Hilo interesante que comenta el DPIA de Holanda. Me parece interesante la idea esencial que rebate el argumento de Google: si es encargado de tratamiento no basta que diga lo que no hace (perfiles, publicidad ...) sino que tiene que decir explícitamente qué es lo que hace.

[twitter manuelabat/status/1383008181417357314](https://twitter.com/manuelabat/status/1383008181417357314)  
Los responsables del tratamiento deben especificar los propósitos del tratamiento en un acuerdo con el encargado del tratamiento. Los encargados del tratamiento de datos solo pueden tratar datos personales en nombre del responsable del tratamiento de datos. #GSuite #GWorkspace  
Por lo tanto, es posible que Google, como encargado del tratamiento, no determine qué propósitos son compatibles con el propósito principal de brindar técnicamente el servicio de G Suite o mantenerlo seguro.  
Independientemente del acuerdo contractual, si Google determina algún propósito (compatible) de tratamiento, actúa como RESPONSABLE del tratamiento de datos y no como encargado del tratamiento de datos.  
Google no ofrece una lista exhaustiva de propósitos específicos que, como encargado del tratamiento, tiene que tratar datos. Sólo se compromete a no tratar los datos personales del cliente con fines publicitarios ni ofrecer publicidad en los Servicios (principales)".  
Fragmento del la Evaluación de Impacto de Protección de datos de la Autoridad Holandesa de protección de datos a Google G Suite Enterprise  
[DPIA Google G Suite Enterprise. Data protection impact assessment on the processing of personal data on 3 platforms with the Chrome browser and as installed apps](https://www.tweedekamer.nl/kamerstukken/detail?id=2021D08774&did=2021D08774)  

[DPIA Google G Suite Enterprise. Data protection impact assessment on the processing of personal data on 3 platforms with the Chrome browser and as installed apps. PDF. Version 1 – for consultation with the Dutch DPA Date9 July 2020, with update on 12 February 2021](https://www.tweedekamer.nl/downloads/document?id=2021D08774)

**12 marzo 2021**

[DPIA on the use of Google G Suite (Enterprise) for Education. For the University of Groningen and the Amsterdam University of Applied Sciences. 15 July 2020, update 12 March2021](https://www.surf.nl/files/2021-06/updated-g-suite-for-education-dpia-12-march-2021.pdf)  

**8 junio 2021**
[Consejo de AP: deje de usar Google para el próximo año escolar](https://www-agconnect-nl.translate.goog/artikel/advies-ap-stop-voor-aankomend-schooljaar-met-google?_x_tr_sl=auto&_x_tr_tl=es&_x_tr_hl=es&_x_tr_pto=ajax,nv)  
La Autoridad Holandesa de Protección de Datos (AP) aconseja a las instituciones educativas que dejen de usar Google G Suite for Education a partir del 1 de agosto, si no se eliminan las incertidumbres y los riesgos asociados a su uso. Así lo informa SIVON, una cooperativa de juntas escolares de educación primaria y secundaria.

**18 marzo 2021**

[Krutka, D.G., Smits, R.M. & Willhelm, T.A. (2021). Don’t Be Evil: Should We Use Google in Schools? TechTrends 65, 421–431.](https://link.springer.com/article/10.1007/s11528-021-00599-4)  
Abstract  
Google is a multinational technology company whose massive advertising profits have allowed them to expand into many areas, including education. While the company has increasingly faced public scrutiny, the use of Google software and hardware in schools has often resulted in little debate. In this paper, we conduct a technoethical audit of Google to address ethical, legal, democratic, economic, technological, and pedagogical concerns educators, students, and community members might consider. We describe how Google extracts personal data from students, skirts laws intended to protect them, targets them for profits, obfuscates the company's intent in their Terms of Service, recommends harmful information, and distorts students’ knowledge. We propose that educators and scholars more closely interrogate the tools of Google and other technology companies to move toward more democratic and just uses of technology in schools.

### Sanciones a Google

Sanciones en general, no solo las limitadas a educación

**16 abril 2021**

[La Justicia australiana condena a Google por engañar a los usuarios sobre la recogida de datos de localización - elmundo](https://amp.elmundo.es/tecnologia/2021/04/16/6079306ffdddfff5b68b45b8.html)  
El Tribunal Federal de Australia determinó que Google engañó a sus usuarios sobre los datos de localización personal recopilados a través de sus dispositivos

**26 noviembre 2021**

[Italy: AGCM fines Google €10M for unlawful commercial practices](https://www.dataguidance.com/news/italy-agcm-fines-google-10m%C2%A0-unlawful-commercial)  
The AGCM stated that Google had failed to provide clear and immediate information on the acquisition and use of user data for commercial purposes. In particular, the AGCM established that Google, both in the phase of creating an account, which is indispensable for the use of all the services offered, and during the use of the services themselves, omitted relevant information that consumers need in order to make an informed decision when accepting that the company collects and uses their personal information for commercial purposes.

**5 enero 2023**

[twitter wendyndavis/status/1611025408861917189](https://twitter.com/wendyndavis/status/1611025408861917189)  
.@Google agrees to $23 million settlement of 13-year-old lawsuit over claims the company “leaked” search users' names to publishers' via referer headers.  
Deal lets @Google searchers between 10/06 & 9/13 put in claims.  
some prior coverage:  
[Google To Settle Long-Running Privacy Battle Over Data Leakage](https://www.mediapost.com/publications/article/367565/google-to-settle-long-running-privacy-battle-over.html)  

### Sanciones asociadas a uso de Google en educación

Pueden ser sanciones a centros privados o "apercibimiento" a consejerías de educación 

**2021 Apercibimiento consejería educación Madrid**

[Expediente N.º: PS/00286/2021  Escuela Oficial de Idiomas (EOI) de Madrid "Jesús Maestro"](https://www.aepd.es/es/documento/ps-00286-2021.pdf)  

Se considera que la reclamada ha infringido el RGPD en el artículo 32 del RGPD, que indica:

> 4. El responsable y el encargado del tratamiento tomarán medidas **para garantizar que cualquier persona que actúe bajo la autoridad del responsable o del encargado y tenga acceso a datos personales solo pueda tratar dichos datos siguiendo instrucciones del responsable**, salvo que esté obligada a ello en virtud del Derecho de la Unión o de los Estados miembros.”. (el subrayado es de la AEPD). 


**27 julio 2023 Apercibimiento consejería educación Canarias**

[twitter fjavier_sempere/status/1684512561457160192](https://twitter.com/fjavier_sempere/status/1684512561457160192)  
Uso de Google Suite para la Educación en centros educativos de una CCAA: resolución sancionadora de la @AEPD_es  Deficiencias en el derecho de información y análisis de riesgos. Consulta  

[Expediente N.º: EXP20210252 ps-00176-2022.pdf](https://www.aepd.es/documento/ps-00176-2022.pdf)
> PRIMERO: SANCIONAR con apercibimiento a la CONSEJERÍA DE EDUCACIÓN,
UNIVERSIDADES, CULTURA Y DEPORTES DEL GOBIERNO DE CANARIAS, con
NIF S3511001D, por las infracciones del RGPD en sus artículos:  
- 13 del RGPD, de conformidad con el artículo 83.5.b), del RGPD, y a efectos de
prescripción como leve en el artículo 74.a) de la LOPDGDD.  
- 32 del RGPD, de conformidad con el artículo 83.4 a) del RGPD, y a efectos de
prescripción como grave en el artículo 73. f) de la LOPDGDD,  

**2023 Sanción a centro por obligar a usar Google Chromebook**
[Expediente N.º: EXP202212248](https://www.aepd.es/documento/ps-00504-2023.pdf)  
SE ACUERDA:  
PRIMERO: INICIAR PROCEDIMIENTO SANCIONADOR a B.B.B., con NIF ***NIF.1,
por:  
-La presunta infracción del Artículo 13 del RGPD, tipificada en el artículo 83.5 ,
y calificada como muy grave a efectos de prescripción en el artículo 72.1 h) de
la LOPDGDD.  
-La presunta infracción del Artículo 28 del RGPD, tipificada en el artículo 83.4 ,
y calificada como grave a efectos de prescripción en el artículo 73 k) de la
LOPDGDD.  

**2023 Apercibimiento centro educativo Madrid dependiente del Ministerio de defensa**

[PS-00334-2022 (10.11.2023) Sobre uso de la plataforma GOOGLE WORKSPACE for Education (GW)  en centro educativo COLEGIO MENOR NUESTRA SEÑORA DE LORETO](https://www.aepd.es/documento/ps-00334-2022.pdf)  

Resolución 56 páginas

PRIMERO: SANCIONAR al ***CARGO.1- COLEGIO MENOR NUESTRA SEÑORA DE LORETO (MINISTERIO DE DEFENSA, EJÉRCITO DEL AIRE Y DEL ESPACIO ) con NIF S2801407D, con una sanción de apercibimiento por cada una de las infracciones del RGPD, de los artículos:
-28, de conformidad con el articulo 83.4 a) del RGPD, tipificada como grave a efectos de prescripción en el artículo 73.k) de la LOPDGDD.  
-13, de conformidad con el articulo 83.5 b) del RGPD, tipificada como leve a efectos de prescripción en el artículo 74.a) de la LOPDGDD.  
SEGUNDO: NOTIFICAR la presente resolución al MINISTERIO DE DEFENSA, (EJÉRCITO DEL AIRE Y DEL ESPACIO- ***CARGO.1- COLEGIO MENOR NUESTRA SEÑORA DE LORETO) con NIF S2801407D  

**14 agosto 2024 Apercibimiento consejería País Vasco**

[Apercibimiento al Gobierno vasco por su acuerdo con Google que expone los datos de 400.000 estudiantes - eldiario](https://www.eldiario.es/euskadi/apercibimiento-gobierno-vasco-acuerdo-google-expone-datos-400-000-estudiantes_1_11586667.html)  
La Agencia Vasca de Protección de Datos sanciona a Educación por no evaluar el impacto que el uso en las aulas de Google Workspace for Education

[Utilización de los servicios Google Workspace for Education en centros docentes de la CAE - avpd.eus](https://www.avpd.eus/resolucion_avpd/r24_045/webavpd00-dictamenes/es/)  

[RESOLUCIÓN Nº R24-045 DEL PROCEDIMIENTO DE INFRACCIÓN Nº PI23-025](https://www.avpd.eus/contenidos/resolucion_avpd/r24_045/es_def/adjuntos/PI23-025_RES_R24-045.pdf)  

### Prohibición Google en educación

**15 julio 2022**

[Dinamarca acaba de prohibir todos los productos de Google en múltiples escuelas. El resto de Europa puede ir detrás - xataka](https://www.xataka.com/privacidad/dinamarca-acaba-prohibir-todos-productos-google-a-nivel-publico-resto-europa-va-detras)  

**31 julio 2022**

[Denmark is effectively banning Google services including Gmail Docs Calendar Drive and many more in schools – Tech news hindi](https://www.globenewsinsider.com/2022/07/31/denmark-is-effectively-banning-google-services-including-gmail-docs-calendar-drive-and-many-more-in-schools-tech-news-hindi/)  

[Despues de Dinamarca, en Países Bajos y Alemania también prohíben el uso de servicios de Google](https://www.linuxadictos.com/despues-de-dinamarca-en-paises-bajos-y-alemania-tambien-prohiben-el-uso-de-servicios-de-google.html)  

### Uso de Gsuite en otras CCAA y otra información sobre Google

Algunos ususo están reflejados en el apartado de sanciones

**22 marzo 2020**

[La comodidad de Google o el reto del sistema propio: el cierre de clases fuerza el debate sobre la digitalización de las escuelas - eldiario](https://www.eldiario.es/tecnologia/comodidad-google-herramientas-tecnologia-escuelas_1_1030144.html)  
Los colegios e institutos que contaban con las herramientas de Google se encuentran en una posición "privilegiada", aunque ven cómo los datos de los alumnos son almacenados por la multinacional fuera de España  
**Google, colegios y privacidad de los menores**  
El terremoto que el coronavirus ha provocado en el sistema educativo español ha intensificado el debate sobre el creciente de peso de Google en las aulas. "Hay una enorme incertidumbre con lo que pasa con los perfiles de los niños cuando salen de la escuela", avisa [Manuela Battaglini](https://twitter.com/manuelabat), abogada especializada en protección de datos y ética de los datos. "Los niños terminan teniendo una relación de cliente con una gran empresa comercial y los padres no pueden oponerse", recalca.  
La experta, directora de la consultora Transparent Internet, detalla que varias autoridades europeas, como la suiza o la noruega, han expresado sus dudas acerca de la presencia de Google en las escuelas. Asegura que esto permite a la multinacional inferir "cuadros de ansiedad, dislexia, problemas en casa o dificultades de aprendizaje" de cada alumno. También "qué religión profesa, su raza, cuál es el nivel socioeconómico de su barrio o si tiene tendencias suicidas, si está sufriendo acoso, un desequilibrio emocional, depresión, si es un superdotado o si es un líder (positivo o negativo)".  
Google lo niega. "Solo utilizamos los datos para dar servicio a las herramientas que ofrecemos, pero no los almacenamos ni usamos para elaborar perfiles comerciales ni de ningún otro tipo", asegura [Marc Sanz López](https://www.linkedin.com/in/marcsanzlopez/?originalSubdomain=es), jefe de Google for Education para el sur de Europa, oriente medio y el norte de África. "No es que lo diga yo, es que está en todos los contratos. Si comercializáramos los datos o les mostráramos anuncios sería una manera clarísima, una absoluta autopista, para perder la confianza de los usuarios. Eso es algo que no nos podemos permitir", asevera en conversación con eldiario.es. 

**21 diciembre 2020**

[Google Classroom no es una opción para la Educación en Europa. El maltrato de la educación de la mano de la tecnología en tiempos del COVID. Luis Fajardo López](https://encanarias.info/posts/33961)  

**7 febrero 2021**

[La irrupción de Google en la escuela pública canaria alarma a expertos en privacidad de datos](https://www.eldiario.es/canariasahora/sociedad/irrupcion-google-escuela-publica-canaria-alarma-expertos-privacidad-datos_1_7198094.html
)  
Los abogados Luis Fajardo y Manuela Battaglini advierten de los riesgos de ceder los datos sensibles del alumnado a una empresa que "hace negocio" con ellos

Algunos enlaces:

[No firméis la autorización para utilizar Google Suite en las escuelas - xnet](https://xnet-x.net/no-autorizar-google-suite-escuelas/)  

[Escuelas que buscan alternativas a Google](https://elpais.com/espana/catalunya/2020-12-10/escuelas-que-buscan-alternativas-a-google.html)  
Arranca un proyecto piloto del Ayuntamiento de Barcelona e Xnet para dar herramientas de software libre y con control de los datos a cinco escuelas

Ejemplos documentos centros ver en [Ejemplos RGPD en educación](https://algoquedaquedecir.blogspot.com/2022/09/ejemplos-rgpd-en-educacion.html)


**29 julio 2021**

[twitter Maestra_enfu/status/1420818162220740617](https://twitter.com/Maestra_enfu/status/1420818162220740617)  
Mientras eres consejera le contratas a google la gestión de las cuentas de correo y toda una suit d aplicaciones para todos los alumnos y docentes de la región y TA-TA-TACHÁN  
[Esperanza Moreno, exconsejera de Educación, 'ficha' por Google](https://www.laopiniondemurcia.es/comunidad/2021/07/29/esperanza-moreno-exconsejera-educacion-ficha-55650886.html)  

[La exconsejera Esperanza Moreno ficha por 'Google for Education'](https://www.laverdad.es/murcia/exconsejera-esperanza-moreno-20210729212310-nt.html)  

**29 mayo 2021**

[Una alternativa a Google en las aulas: docentes canarios promueven el uso de tecnología ética en Educación - eldiario](https://www.eldiario.es/canariasahora/sociedad/alternativa-google-aulas-docentes-canarios-promueven-tecnologia-etica-educacion_1_7981842.html)  
Un colectivo de profesores de la Universidad de La Laguna impulsa una fundación para evitar la continuidad en los centros educativos de productos comercializados por empresas que “hacen negocio con los datos” y ofrecer recursos alternativos que respeten la privacidad

**18 julio 2024**

[Por qué a la Agencia Española de Protección de Datos no le gusta Google Workspace for Education - digitalforeurope.eu](https://digitalforeurope.eu/por-que-a-la-agencia-espanola-de-proteccion-de-datos-no-le-gusta-google-workspace-for-education)  

Se cita

[Informe APED tras consulta sobre acuerdo INTEF Google 2023-0050.pdf](https://www.aepd.es/documento/2023-0050.pdf)  

[INFORME Google y Agencia Española de Protección de Datos: el caso de la implantación de soluciones digitales en la educación. Asociación Europea para la Transición Digital](https://digitalforeurope.eu/wp-content/uploads/2024/07/AETD_IMPLANTACION_SOLUCIONES_DIGITALES.pdf)  

**19 agosto 2024**

[La AETD alerta de problemas de privacidad de Google relativas a los datos de alumnos menores en su plataforma educativa - europapress](https://www.europapress.es/sociedad/noticia-aetd-alerta-problemas-privacidad-google-relativas-datos-alumnos-menores-plataforma-educativa-20240819123938.html)  
> Por ello, la Asociación Europea para la Transición Digital recomienda descargar el Informe del Gabinete Jurídico de la AEPD y, si el colegio donde se está usando la plataforma de Google es público, remitir el informe a la Consejería de Educación correspondiente en cada Comunidad Autónoma para que se pronuncie claramente sobre si se debe usar o no dicha plataforma educativa. En caso de que el colegio sea concertado o privado, aconseja remitir el informe al Delegado de Protección de Datos del centro educativo.


