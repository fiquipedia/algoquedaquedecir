+++
author = "Enrique García"
title = "Grupo Planeta y educación Andalucía"
date = "2024-01-27"
tags = [
    "educación", "Andalucía", "Privatización", "Planeta"
]
toc = true

+++

Revisado 28 enero 2024

## Resumen

El grupo Planeta, que controla muchos medios de comunicación relevantes en España, recibe un contrato por 18,4 M€ en 2024, relacionado con aulaplaneta y [Grupo Planeta y Educación Madrid](../grupo-planeta-y-educacion-madrid) ha recibido y recibe dinero y favores asociados a educación en Madrid.  
La licitación se hace en 2022, meses después de que termine un proyecto de 2 años por 1,8 M€ en el que los docentes elaboran recursos educativos abiertos, sin límite de plazo.  

## Detalle

### Cronología de aulaplaneta en Andalucía

**23 mayo 2022**

[OBJETOS DE APRENDIZAJES DIGITALES](https://www.juntadeandalucia.es/haciendayadministracionpublica/apl/pdc_sirec/perfiles-licitaciones/detalle-licitacion.jsf?idExpediente=422345)


Descripción : SUMINISTRO DE OBJETOS DE APRENDIZAJE DIGITALES PARA CENTROS SOSTENIDOS CON FONDOS PÚBLICOS QUE IMPARTEN ENSEÑANZAS DE EDUCACIÓN PRIMARIA Y EDUCACIÓN SECUNDARIA OBLIGATORIA

Nº de lote : 1  
Título : SUMINISTRO DE OBJETOS DIGITALES CON LICENCIA DE USO PARA EDUCACION PRI  
Valor estimado 5.000.000,00€  

Nº de lote : 2  
Título : SUMINISTRO OBJETOS DIGITALES CON LICENCIA DE USO PARA CURSOS 1 Y 2 SEC  
Valor estimado 6.869.000,00€  

Nº de lote : 3  
Título : SUMINISTRO OBJETOS DIGITALES CON LICENCIA DE USO PARA CURSOS 3 Y 4 SEC  
Valor estimado 6.869.000,00€  

* [(PPT - Pliego de prescripciones técnicas) Pliego de prescripciones técnicas.PDF (.PDF) ](https://www.juntadeandalucia.es/haciendayadministracionpublica/apl/pdc_sirec_documentacion/rest/descargar/documento/45288) 23/05/2022 09:13
* [(PCAP - Pliego de Cláusulas Administrativas Particulares) Pliego de Cláusula Administrativa.PDF (.PDF) ](https://www.juntadeandalucia.es/haciendayadministracionpublica/apl/pdc_sirec_documentacion/rest/descargar/documento/45289) 23/05/2022 09:13

**17 junio 2022** 

[Acta mesa calificación administrativa](https://www.juntadeandalucia.es/haciendayadministracionpublica/apl/pdc_sirec_documentacion/rest/descargar/documento/51415)

> ... ofertas presentadas a licitación:  
-INETUM ESPAÑA, S.A. (LOTE 1-2-3)  
-EDITORIAL PLANETA SA (LOTE 1-2-3)  
-GRUPO ANAYA SA (LOTE 1-2-3)  
-ODILO TID SL (LOTE 3)  

Planeta (que es la que se lo lleva según los anuncios de 2024), indica para los 3 lotes

> La duración total del licenciamiento será de 481 meses.

Eso son 40 años!!!

Para lote 1 (5.189.612,86 ) IVA incluido.  
Para lote 2 (7.061,610,47 ), IVA incluido.  
Para lote 3 (7.061.638,71 ), IVA incluido.  

Luego en total Planeta se lleva 19 M€

**21 julio 2022**  

Recuso y desestimiento de ANELE (como en Madrid)  
[Resolución desestimiento](https://www.juntadeandalucia.es/haciendayadministracionpublica/apl/pdc_sirec_documentacion/rest/descargar/documento/59910)  
> Sexto: Con fecha 13 de junio de 2022 se presenta en el registro telemático de la Junta de Andalucía escrito de
interposición de recurso por parte de la ASOCIACION NACIONAL DE EDITORES DE LIBROS Y MATERIAL DE
ENSEÑANZA (ANELE) dirigo al TRIBUNAL ADMINISTRATIVO DE RECURSOS CONTRACTUALES DE LA JUNTA DE
ANDALUCÍA donde se solicita que tenga por interpuesto Recurso Especial en Materia de Contratación frente al anuncio
de licitación y los pliegos que rigen el contrato de suministro “Objetos de aprendizaje digitales para centros públicos
sostenidos con fondos públicos que imparten enseñanzas de EPO y ESO (Expte. CONTR 2022 0000229276) y, tras la
tramitación oportuna, dicte resolución por la que, previos los trámites de ley, se sirva estimarlo, anulando los referidos
actos en materia de contratación, así como la adopción de la medida cautelar, consistente en la suspensión del
procedimiento de licitación.

**24 enero 2024**

[Del Pozo destaca el impulso a la transformación digital educativa con el acceso a más de 70.000 nuevos recursos didácticos - juntadeandalucia](https://www.juntadeandalucia.es/presidencia/portavoz/educacion/189811/ObjetosDigitalesEducativos/ODEs/Aulaplaneta/recursoseducativos/propuestasdidacticas/docentes/acompanamientoformativo/ConsejeriadeDesarrolloEducativoyFormacionProfesional/JuntadeAndalucia)  

> El proyecto 'Objetos Digitales Educativos (ODE)', con una inversión de 18,4 millones, llega a 57.000 docentes y 500.000 estudiantes para enriquecer contenidos y dinámicas en las clases  

> Aulaplaneta, el área de educación del Grupo Planeta, es el proveedor de los recursos digitales educativos que se incorporarán al ecosistema educativo digital de Andalucía. Esta empresa cuenta con una sólida experiencia y un firme compromiso en la educación digital, reflejada en proyectos como 'E-Dixgal' en Galicia, o 'Madrid5e' en la Comunidad de Madrid.  

E-Dixgal no lo conocía; para mirar y quizá post separado  

[E-Dixgal](http://www.edixgal.com/)  
EDUCACIÓN DIXITAL - Implantación do libro dixital nos centros educativos.   

[aulaplaneta e-dixgal](http://www.edixgal.com/p/aulaplaneta.html)  

### Recursos abiertos en Andalucía previos a aulaplaneta

Recuerdo haberlo comentado en 2020  

[twitter FiQuiPedia/status/1337493429102784521](https://twitter.com/FiQuiPedia/status/1337493429102784521)  
Una referencia para comparar (aunque no son las mismas materias):  
-Madrid, recursos educativos privados, hechos por empresas privadas, uso por dos años: 17 M€  
-Andalucía, recursos educativos abiertos, hechos por docentes, sin límite temporal uso: 2M€  

En el enlace recuerdo la convocatoria, se hacía por docentes. En mismo enlace en 2024 aparece información general y habla de LOMLOE

[Proyecto REA/DUA - juntadeandalucia](https://www.juntadeandalucia.es/educacion/portals/web/transformacion-digital-educativa/rea)  
> La Consejería de Desarrollo Educativo y Formación Profesional presenta el #ProyectoREADUAAndalucía, conformado por más de 250 recursos educativos abiertos (REA) de distintas materias de Primaria, Secundaria y Bachillerato.

**9 diciembre 2020**

[Resolución de 20 de noviembre de 2020, de la Dirección General de Formación del Profesorado e Innovación Educativa, por la que se efectúa convocatoria de selección y nombramiento de profesorado para la elaboración de recursos educativos abiertos de enseñanzas no universitarias y se establece la naturaleza de estos.](https://www.juntadeandalucia.es/boja/2020/236/14)

> Dentro de dicho proyecto se contempla la elaboración de Recursos Educativos Digitales Abiertos (REA) por una cuantía máxima de 1.822.600€. Para dicha elaboración la Consejería de Educación y Deporte promueve con esta Resolución que sea el propio profesorado del sistema educativo público andaluz el protagonista de esta. 

> Primero. Objeto y ámbito temporal.  
La presente resolución tiene como objeto efectuar la convocatoria para la selección y el nombramiento de 185 docentes para la elaboración de recursos educativos abiertos de las materias troncales de Primaria y Secundaria: Matemáticas, Lengua y Primera Lengua Extranjera (Inglés) desde 1.º de Primaria hasta 4.º Educación Secundaria Obligatoria y materias de libre configuración autonómica con contenidos de pensamiento computacional, robótica e investigación aeroespacial aplicada al aula desde 5.º de Educación Primaria hasta 2.º de Bachillerato y se establece la naturaleza de estos recursos.  
Su elaboración se llevará a cabo en cuatro períodos: el primer período será el comprendido entre el nombramiento de todo el profesorado y el necesario para la elaboración del diseño global del proyecto con sus bases tanto pedagógicas como organizativas por parte del equipo de personas que coordinan el proyecto previsto para el primer trimestre del curso 20/21. Los restantes tres períodos serán los comprendidos entre la finalización del diseño global del proyecto y la fecha de finalización de elaboración prevista para marzo de 2022.

**11 diciembre 2020**

[Convocatoria de selección y nombramiento de profesorado para la elaboración de recursos educativos abiertos de enseñanzas no universitarias.](https://www.juntadeandalucia.es/educacion/eaprendizaje/convocatoria-seleccion-nombramiento-rea/)


[Crea tu REA para el Proyecto REA Andalucía](https://edea.juntadeandalucia.es/bancorecursos/file/6d215d5c-1983-41e3-80d1-50572b852eb9/1/guia_crea_tu_rea_proyecto_rea_andalucia.zip/index.html)

> Se nombran 185 docentes para la elaboración de recursos educativos abiertos de las materias troncales de Primaria y Secundaria:  
Matemáticas, Lengua y Primera Lengua Extranjera (Inglés) desde 1º de Primaria hasta 4º Educación Secundaria Obligatoria y materias de libre configuración autonómica con contenidos de pensamiento computacional, robótica e investigación aeroespacial aplicada al aula desde 5º de Educación Primaria hasta 2º de Bachillerato.


**24 febrero 2022**

[twitter cepcastilleja/status/1496772897041432578](https://twitter.com/cepcastilleja/status/1496772897041432578)  
PRIMEROS RECURSOS DEL PROYECTO REA/DUA  
Ya se encuentran disponibles los primeros Recursos Educativos Digitales Abiertos que están elaborando 186 docentes de Andalucía y que se irán ampliando en los próximos meses.  




