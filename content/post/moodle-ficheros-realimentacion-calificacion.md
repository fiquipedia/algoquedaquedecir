+++
author = "Enrique García"
title = "Moodle: ficheros realimentación calificación"
date = "2024-10-26"
tags = [
    "moodle", "EducaMadrid"
]
toc = true

+++

Revisado 26 octubre 2024

## Resumen

Se comentan los detalles para subir ficheros de realimentación en la calificación de tareas moodle.  
El planteamiento inicial es hacerlo asociado a exámenes escritos de modo que el fichero sea el examen corregido, para que el alumno pueda consultar la corrección viendo sus errores: la realimentación es la clave para entender los errores y mejorar, y poder ver la corrección con tiempo, no solo un tiempo en clase que es lo habitual, aporta transparencia a la calificación.
    
## Detalles

Los exámenes en física y química se hacen manuscritos ya que lo habitual es hacer desarrollos con diagramas y ecuaciones y vía online no es viable.   
Al no usar libro de texto y usar aula virtual de EducaMadrid implementada en moodle, las calificaciones de los exámenes las pongo en el aula virtual a través del calificador, creando una tarea ya que es más flexible que un ítem de calificación.  
Desde hace tiempo pensaba que era una buena idea que los alumnos tuvieran una copia de sus exámenes (siempre les doy acceso a enunciado y resolución, y siempre dejo que hagan fotos de los exámenes corregidos si quieren), pero hasta 2024-2025 no lo lo he realizado por primera vez con cierta automatización que evite hacerlo uno a uno.  

Al inicio de 2024-2025 realicé una formación en el centro y compartí la idea  

[EducaMadrid-CalificaciónEnAulasVirtuales.pdf](https://cloud.educa.madrid.org/s/at7TBAqnxmKETYA)  

>Realimentación avanzada  
En Moodle existe la posibilidad de añadir como realimentación un fichero pdf, lo
que lleva a una posibilidad que aporta transparencia: subir al aula virtual el
examen escaneado corregido en pdf, y entregar el examen físico al alumno.  
>[twitter p_trivino/status/1639701181260877825](https://x.com/p_trivino/status/1639701181260877825)  
>- Ven lo que se pregunta, lo que han escrito y cómo se ha corregido.  
>- No se sacan originales del centro si se corrige sobre el pdf.  
>Implica trabajo: escanear, separar en pdfs y luego subir. Si son muchos se pueden subir todos a la vez.  
[Mediateca. Archivos retroalimentación en AV. Subir un examen. Pablo Jesús Treviño](https://mediateca.educa.madrid.org/video/vtfu8q9ewydis92e)  
Existen algunas utilidades creadas para separar los pdfs identificando portadas
[twitter chavarria1991/status/1325504836176109569](https://x.com/chavarria1991/status/1325504836176109569)  
Cree este pequeño script para automatizar la tarea de dividir el PDF grande en
PDFs individuales y crear las carpetas para subirlo a Moodle.  

Un script en shell no aplica a windows, y el vídeo habla de realimentación tras corrección sobre el pdf, por lo que no aplica a mi caso que sigo corrigiendo en papel.  
Dentro de que no es algo sencillo, intento detallarlo.  

## Permitir ficheros de realimentación en moodle

Para poder subir ficheros en una actividad tipo tarea, dentro de   
_"Configuración" > "Tipos de realimentación"_  
deben estar marcadas las opciones _Archivos de retroalimentación_ y _Hoja de calificaciones externa_  
Una vez activas, en "Calificar todas las tareas" aparece un desplegable 
_"Acción sobre las calificaciones"_  
con opciones  
_"Descargar hoja de calificaciones"_ y _"Subir múltiples archivos de realimentación en un ZIP"   

## Subir ficheros de realimentación en moodle

Es lo que en la presentación comentaba como _Si son muchos se pueden subir todos a la vez._

Una vez que se tiene el zip, al elegir la opción y tras eligir el fichero, luego da la opción de importar, visualizando antes qué fichero o ficheros se va a subir o a actualizar para cada alumno

## Formato del zip con archivos de realimentación 

En el vídeo se descargan las entregas el zip y ya tiene una estructura de directorios, pero si no hay entregas la estructura no existe y hay que crearla.  
La idea básica es que el fichero con calificaciones se descarga en csv, con varios campos:

Primer campo "Identificador", que tiene contenido "ParticipanteXXXXXX" siendo XXXXXX un número  
Segundo campo "Nombre completo", que tiene de contenido el nombre del estudiante, con espacios  

La estructura del zip es una carpeta para cada alumno con nombre

"Nombre compleo" + "_" + "XXXXXX" + "_assignsubmission_file"

## Generación de estructura de directorios

Importando el csv en LibreOffice, se puede crear mediante una macro.  
Hago y pruebo esta macro en linux antes de probar el script 

```
Sub CrearDirectorios()
    Dim oDoc As Object
    Dim oSheet As Object
    Dim i As Long
    Dim nombreDir As String
    Dim rutaBase As String
    Dim parteNumerica As String
    Dim valorCol1 As String
    Dim valorCol2 As String
    Dim oFile As Object

    oDoc = ThisComponent
    oSheet = oDoc.CurrentController.ActiveSheet
    rutaBase = "/home/enrique/Descargas/pruebamoodle/" ' Poner la ruta base acabada en /
    
    ' Recorremos las filas de la hoja (asumiendo que los datos empiezan en la fila 0)
    For i = 0 To oSheet.Rows.Count - 1
        valorCol1 = oSheet.getCellByPosition(0, i).String ' Primera columna (A)
        valorCol2 = oSheet.getCellByPosition(1, i).String ' Segunda columna (B)
        
        ' Extraer la parte numérica de valorCol1
        parteNumerica = ""
        For j = 1 To Len(valorCol1)
            If IsNumeric(Mid(valorCol1, j, 1)) Then
                parteNumerica = parteNumerica & Mid(valorCol1, j, 1)
            End If
        Next j
        
        ' Crear el nombre del directorio
        If parteNumerica <> "" Then
            nombreDir = valorCol2 & "_" & parteNumerica & "_assignsubmission_file"
            ' Crear el directorio
            Call CrearDirectorio(rutaBase & nombreDir)
        End If
    Next i
End Sub

Sub CrearDirectorio(ruta As String)
    Dim oFile As Object
    oFile = CreateUnoService("com.sun.star.ucb.SimpleFileAccess")
    
    ' Comprobar si el directorio ya existe
    If Not oFile.Exists(ruta) Then
        oFile.CreateFolder(ruta) ' Usamos CreateFolder para crear el directorio
    End If
End Sub
```

## Separación de pdfs escaneados 

Es la tarea que lleva más tiempo: se puede utilizar una herramienta (pdftk, masterpdfeditor, ...) y separar a mano tras ver cuántas hojas son de cada alumno, y poner a mano el nombre de cada alumno en el fichero pdf (poner el nombre en el fichero ayuda a verificar que el pdf se pone en la carpeta correcta y a la hora de subir el zip a verificar que cada alumno tiene su pdf correcto)

En la presentación cito un script que tiene fuente en github, que indica Copyright (C) 2020 Marcos Chavarría Teijeiro pero que contactado indica que es libre  

[chavaone/exam-split.sh](https://gist.github.com/chavaone/a3eafac77fa0467b936bd1cd1681e51f)  

El script tiene ayuda en el código; parte del csv y busca una cadena, asignando esas páginas como inicio de cada examen  



```
	echo "Usage: ./exam-split [OPTIONS] file.pdf"
	echo ""
	echo "Cadena option is mandatory."
	echo ""
	echo "OPTIONS:"
	echo -e "\t -s|--string STRING: Text that should identify each exam first page."
	echo -e "\t -c|--csv CSV_FILE: CSV File generated by Moodle which contents student names and ids."
	echo -e "\t -g|--geometry: ImageMagick geometry parameter."
	echo -e "\t -o|--output: Output folder name."
	echo -e "\t -z|--zip: Compress the Moodle folders in a ZIP file."
	echo -e "\t --help: Show this message."
	echo -e "\t --debug: Show DEBUG info."
```

También cita qué programas deben estar instalados:  
* [ImageMagick](https://imagemagick.org/index.php) con proyecto [github.com ImageMagick](https://github.com/ImageMagick/ImageMagick) 
* tesseract con proyecto en [github.com tesseract-ocr](https://github.com/tesseract-ocr)   
* pdf-stapler con proyecto en [sourceforge pdfstapler](https://sourceforge.net/projects/pdfstapler/) que parece sin actualizar desde 2013, con [algunos archivos de 2015 en otros enlaces](http://security.ubuntu.com/ubuntu/pool/universe/s/stapler/)  

Busco las líneas de pdf-stapler y miro como cambiarlas para usar [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) que sí tengo instalado y es un proyecto actual

Código anterior:

```
pdf_stapler_bin=`which pdf-stapler`
if [ -z $pdf_stapler_bin ]
then
	echo "You must have PDF Stapler installed."
	exit
fi
```

debe pasar a ser  

```
pdftk_bin=`which pdftk`
if [ -z "$pdftk_bin" ]; then
    echo "You must have pdftk installed."
    exit 1
fi
```

Código anterior:

```
pdf-stapler sel $1 $oldpage-$((newpage - 1)) ${TMP_PATH}/${FILE_PATH}_pdfs/$oldnumber.pdf
```

debe pasar a ser  

```
pdftk "$1" cat $oldpage-$((newpage - 1)) output "${TMP_PATH}/${FILE_PATH}_pdfs/${oldnumber}.pdf"
```

Código anterior:

```
pdf-stapler sel $1 ${oldpage}-`ls -1 ${TMP_PATH}/${FILE_PATH}_fullimgs/ | wc -l` ${TMP_PATH}/${FILE_PATH}_pdfs/$oldnumber.pdf #creamos o último ficheiro.
```

debe pasar a ser  

```
pdftk "$1" cat ${oldpage}-$(ls -1 "${TMP_PATH}/${FILE_PATH}_fullimgs/" | wc -l) output "${TMP_PATH}/${FILE_PATH}_pdfs/${oldnumber}.pdf"
```

Comentario sobre funcionamiento tras hablar con el autor:

La idea es incluir un pequeño texto en la cabecera de la portada que incluya una palabra clave (MATES) y el número del CSV. El script lo que hace es recortar un rectángulo de la parte superior de cada página donde debería estar esa palabra clave. A continuación hace OCR e intenta buscar la palabra clave. Si la encuentra almacena el número de esa página y también el número que detecta.  
El proceso era corregirlos, escanearlos, descargar el csv con de moodle y mientras se pasan las notas al CSV editar el PDF (con Xournal++) para incluír el número del CSV, y luego pasar el programa para dividir

Es casi transparente para los alumnos, porque no tiene que haber nada salvo un hueco en el que no escribir.  

Mi planteamiento sería hacer una hoja portada impresa de cada examen con el nombre del alumno, y al corregir poner cada portada delante antes de escanear, sin ser necesario ordenarlos, lo que lo haría totalmente transparente para el alumno que hace el examen sin ninguna limitación, y en el procesado permitiría identificar primera página y asociar examen al alumno al reconocer su nombre vía OCR sin editar el PDF. 

Otra opción es poner en el csv un campo con número de páginas de cada alumno y ordenar los exámenes, hacer que el programa trocee esos números de páginas asumiendo ese orden y no sería necesario ni hacer OCR.
