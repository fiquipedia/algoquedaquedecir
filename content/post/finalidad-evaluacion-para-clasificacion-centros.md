+++
author = "Enrique García"
title = "Finalidad evaluación para clasificación centros"
date = "2018-01-17"
tags = [
    "educación", "Madrid", "normativa", "post migrado desde blogger"
]
toc = true

+++

Revisado 26 septiembre 2024

## Resumen

Creo que se suele conocer que uno de los principales cambios de la LOMCE fue introducir las evaluaciones finales/reválidas, pero creo que se conoce poco un cambio importante en la LOMCE, deliberado y no casual: modificar la finalidad de la evaluación para permitir la clasificación de centros para hacer rankings.  
En el programa electoral de PP y Casado se vuelve a decir que se harán ránkings, cuando ya se realizan y se permiten.
En diciembre 2020 con LOMLOE se revierte el cambio, y no se permiten rankings. Ver post [LOABLE reversión LOMCE](https://algoquedaquedecir.blogspot.com/2018/11/loable-reversion-lomce.html).

*Tras la implantación de LOMLOE cambio el título del post, ya que no tiene sentido asociarlo solo a LOMCE derogada: la idea de los rankings es algo general y hay administraciones que siguen haciéndolo*

En 2022 la ley Maestra lo vuelve a permitir en Madrid. Enlaza con post [Ley maestra de liberad de enseñanza](../ley-maestra-de-libertad-de-eleccion-educativa)

Enlaza con post [LOMCE: evaluaciones finales 4º ESO](http://algoquedaquedecir.blogspot.com/2017/11/lomce-evaluaciones-finales-4-eso.html)

También enlaza con [Datos segregación educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-segregacion-educacion.html) (ver apartado Segregación por resultados académicos) y con [Programa Bachillerato Excelencia Madrid](../programa-bachillerato-excelencia-madrid) (al hablar aquí de clasificaciones surge el IES San Mateo)

## Detalles 

### Cambios asociados a rankings centros en LOMCE y LOMLOE

La LOMCE modificó la LOE, y en concreto el artículo 140
En su redacción LOMCE quedó así

[Artículo 140. Finalidad de la evaluación.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a140)
> 1. La evaluación del sistema educativo tendrá como finalidad:  
a) Contribuir a mejorar la calidad y la equidad de la educación.  
b) Orientar las políticas educativas.  
c) Aumentar la transparencia y eficacia del sistema educativo.  
d) Ofrecer información sobre el grado de cumplimiento de los objetivos de mejora establecidos por las Administraciones educativas.  
e) Proporcionar información sobre el grado de consecución de los objetivos educativos españoles y europeos, así como del cumplimiento de los compromisos educativos contraídos en relación con la demanda de la sociedad española y las metas fijadas en el contexto de la Unión Europea.  

Como indica el BOE

> Se suprime el apartado 2 por el art. único.86 de la Ley Orgánica 8/2013, de 9 de diciembre. [Ref. BOE-A-2013-12886](https://www.boe.es/buscar/doc.php?id=BOE-A-2013-12886).

¿Qué decía este apartado 2?

> 2. La finalidad establecida en el apartado anterior no podrá amparar que los resultados de las evaluaciones del sistema educativo, independientemente del ámbito territorial estatal o autonómico en el que se apliquen, puedan ser utilizados para valoraciones individuales de los alumnos o para establecer clasificaciones de los centros.

[twitter FiQuiPedia/status/718102011649134592](https://twitter.com/FiQuiPedia/status/718102011649134592)  

![](https://pbs.twimg.com/media/Cfc1-QbUYAA8SpA.png)

Además de ese artículo 140, se puede ver también artículo 144.3

En LOE decía  
> En ningún caso, los resultados de estas evaluaciones podrán ser utilizados para el establecimiento de clasificaciones de los centros", que desparece con LOMCE.

**30 diciembre 2020**

Se publica en BOE LOMLOE, y se puede ver como se revierte este cambio.

[Artículo 140. Finalidad de la evaluación.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=201&tn=1&p=20201230#a140) 

> Se añade el apartado 2 por el art. único.74 de la Ley Orgánica 3/2020, de 29 de diciembre  

> 2. La finalidad establecida en el apartado anterior no podrá amparar que los resultados de las evaluaciones del sistema educativo, independientemente del ámbito territorial estatal o autonómico en el que se apliquen, puedan ser utilizados para valoraciones individuales del alumnado o para establecer clasificaciones de los centros.

[twitter FiQuiPedia/status/1344599616709398530](https://twitter.com/FiQuiPedia/status/1344599616709398530)  
También se puede ver [texto consolidado LOE con LOMLOE con cambios marcados respecto a LOMCE en web @educaciongob](https://educagob.educacionyfp.gob.es/dam/jcr:f92577f1-f2b4-4135-8f4a-c41e80628954/loe-con-lomloe-texto.pdf)  
![](https://pbs.twimg.com/media/Eqj6NzGXMAI2y0E?format=jpg)  
![](https://pbs.twimg.com/media/Eqj6ONgXAAYmQ-e?format=jpg)  

También se cambia artículo 144.3, volviendo a incluir la frase que estaba en LOE y eliminó LOMCE

> En ningún caso, los resultados de estas evaluaciones podrán ser utilizados para el establecimiento de clasificaciones de los centros

[INEE Nacional Evaluaciones LOMLOE](https://www.educacionfpydeportes.gob.es/inee/evaluaciones-nacionales/evaluaciones-lomloe.html)  

[twitter educaINEE/status/1782711431231627705](https://twitter.com/educaINEE/status/1782711431231627705)  
¿Sabías que? La LOMLOE prohíbe establecer rankings en base a los resultados de las Evaluaciones de diagnóstico. «En ningún caso, los resultados de estas evaluaciones podrán ser utilizados para el establecimiento de clasificaciones de los centros». Artículo 144.3  


### Publicación rankings centros

Se cambió normativa para publicar rankings centros, cuando la realidad es que ya se estaban publicando, y luego se negaba que se fuesen a publicar: que el gobierno de PP diga lo contrario; tras cambiar la ley precisamente para permitir eso, lo considero una tomadura de pelo  

[twitter abc_conocer/status/717794557417635841](https://twitter.com/abc_conocer/status/717794557417635841)  
La evaluación de final de Primaria no se usará para realizar ranking de centros http://ww.abc.es/10mYK5

El enlace es a artículo 6 abril 2016
[Educación se compromete a no publicar los resultados de la evaluación de Primaria para evitar rankings](http://www.abc.es/sociedad/abci-educacion-compromete-no-publicar-resultados-evaluacion-primaria-para-evitar-rankings-201604061616_noticia.html)

Imagen artículo 7 abril 2016
[twitter _mcontreras/status/717840243722989568](https://twitter.com/_mcontreras/status/717840243722989568)  

![](https://pbs.twimg.com/media/CfZH2qkWEAAyeH7.jpg)  

En este hilo hablé con una persona que se creía ese compromiso del ministro, y argumentando no me lo pudo rebatir

[twitter FiQuiPedia/status/718161467934130176](https://twitter.com/FiQuiPedia/status/718161467934130176)  
De qué sirve que @IMendezdeVigo "se comprometa" si ley no lo prohíbe  
¿Qué pasa si alguien lo incumple?  
[Indecente publicación resultados pruebas CDI](http://docentesconeducacion.es/viewtopic.php?f=42&t=670)  
Cito un hilo de un foro, lo comento en este post más adelante

(réplica por su parte) las leyes se puede rectificar cuando hay errores. Si el ministro se ha comprometido,seguro q lo solucionará

@IMendezdeVigo cambió ley al ir en programa [Programa electoral Partido Popular 2011](http://www.pp.es/sites/default/files/documentos/5751-20111101123811.pdf#page=84) …   
No es un error, es el objetivo @ppopular fin artículo 140.2   

En el documento, punto 5, se puede leer

> Evaluaremos los conocimientos de los alumnos con una prueba de carácter nacional. Haremos públicos los resultados de los centros para estimular la calidad educativa en todas las comunidades autónomas.

.@IMendezdeVigo Consejo Escolar del Estado pide derogar supresión art. 140.2 hecha por LOMCE  
[El Consejo Escolar pide limitar la difusión de resultados de la 'reválida' de Primaria - europapress](https://www.europapress.es/sociedad/educacion-00468/noticia-consejo-escolar-pide-limites-difusion-resultados-revalida-primaria-20160425151842.html)  
[Dictamen 10/2016 Consejo Escolar Estado](http://www.mecd.gob.es/dctm/cee/el-consejo/dictamenes/2016/dictamen102016.pdf?documentId=0901e72b820f1edb#page=4)  

> Es por ello que se propone que la redacción “los difundirán a la comunidad educativa” exprese
con exactitud qué información, qué difusión, durante cuánto tiempo tiene que estar disponible
la información y debería, asimismo, poner límites para que no sea usada en ningún caso para
hacer clasificaciones de centros.	

![](https://pbs.twimg.com/media/Cg-k87RW0AACxUT.jpg)

El el hilo citaba varias cosas

[PROPUESTAS DEL PP PARA UN PACTO POR LA REFORMA Y MEJORA DE LA EDUCACIÓN EN ESPAÑA (27 páginas). Presentado por PP en enero 2010. - archive.org](https://web.archive.org/web/20140716173745/http://www.elpais.com/elpaismedia/diario/media/201112/20/sociedad/20111220elpepisoc_2_Pes_PDF.pdf)  
En página 11 se indica

> Los resultados de cada centro, en su conjunto, serán públicos.

[Indecente publicación resultados pruebas CDI](http://docentesconeducacion.es/viewtopic.php?f=42&t=670)
Madrid, cuando estaba vigente LOE y artículo 140.2 existía y no permitía publicar resultados para clasificar centros, los publicaba:

**-Primero de manera sutil:** casualmente se filtraban a los medios los rankings y luego todos los publicaban

[Notas de todos los colegios de Madrid en la prueba de nivel de sexto de primaria - elpais 2005](https://elpais.com/sociedad/2005/10/11/actualidad/1128981602_850215.html)  

[La oposición acusa a Educación de hacer un uso político de las notas del examen sorpresa - elpais 2005](https://elpais.com/diario/2005/10/11/madrid/1129029865_850215.html)  
El consejero Peral replica que ha evitado "una presentación sesgada de los resultados"

[Sobre la publicación de los resultados de las pruebas CDI - adimad - 2011](https://web.archive.org/web/20110601000000*/http://www.adimad.org/2011/03/sobre-la-publicacion-de-los-resultados-de-las-pruebas-cdi/)
Comunicado de la junta directiva de ADiMAD sobre sobre la publicación de los resultados de la pruebas CDI para los alumnos de 3º de ESO celebrada en 2010.  

[Los 100 primeros de la clase - elpais 2010](http://www.elpais.com/articulo/madrid/primeros/clase/elpepuespmad/20101223elpmad_2/Tes)  
39 centros concertados, 33 públicos y 28 privados tienen las mejores notas en la prueba de 6º de Primaria de la Comunidad Las Rozas y Boadilla tienen la nota media más alta y Parla y Fuenlabrada la más baja

> EL PAÍS ha tenido acceso a los resultados y reproduce el listado con los 100 colegios con mejor nota. La información de todos los centros se puede consultar en elpais.com.

[Clasificación de colegios de la Comunidad de Madrid (examen de 6º de Primaria) - elpais - archive 2011](https://web.archive.org/web/20110429082604/http://www.elpais.com/especial/clasificacion-colegios-madrid/)  

[El ranking de calidad de los institutos de Madrid - abc 2011](https://www.abc.es/espana/madrid/abci-institutos-201102110000_noticia.html)  
Un instituto de Carabanchel encabeza el listado de equidad, elaborado con los criterios del informe PISA, según el cual 253 de los 308 centros de la Comunidad mejoraron sus notas en 2010

> El llamado Informe sobre la Equidad en Institutos de Enseñanza Secundaria de la Comunidad de Madrid, basado en la metodología del Informe PISA de la OCDE y realizado por la Consejería de Educación sobre las notas de los institutos en la prueba CDI de Secundaria, destaca la convergencia al alza del conjunto de los institutos de la región, al reducirse ostensiblemente la brecha en las notas medias obtenidas.  

> En el informe, explican, se constata que las diferencias más significativas de notas se producen en mayor medida entre alumnos de un mismo centro que entre los distintos institutos.

**-Luego de manera descarada:** con PP en gobierno nacional tras elecciones noviembre 2011 con mayoría absoluta pero todavía sin LOMCE que anulase artículo 140.2, ya publicó resultados en web de Madrid.

Ejemplo diciembre 2011:

[El colegio público Méjico saca la mejor nota en el examen de la Comunidad - madridiario - 2011](https://www.madridiario.es/2011/Diciembre/educacion/edu_educacion/211574/colegio-publico-mejico-mejor-nota-prueba-conocimiento-destrezas-6-primaria-madrid.html03)  

> **Publicación en la web**  
El Gobierno regional **[ha colgado por primera vez en su página web los resultados](http://www.madrid.org/cs/Satellite?c=CM_InfPractica_FA&cid=1142669638412&language=es&pagename=PortalEducacion%2FCM_InfPractica_FA%2FEDUC_InfPractica)** de todos los centros de la Comunidad (desde 2008 hasta 2011) detallando el número de alumnos presentados, la nota media del centro y los porcentajes de aprobados (tanto el total como en las distintas áreas que se sometieron a examen).</i>

[Declaraciones Lucía Figar publicidad resultados pruebas diagnóstico 2010 - youtube](https://www.youtube.com/watch?v=0LNiUrDj3CM)  
> ... hemos empezado en Madrid a difundir las informaciones importantes, relevantes que exigían los padres. Hemos empezado por el rendimiento en la escuela de varias estructuras educativas en varios ciclos, a nivel elemental, a nivel de 2º ciclo. Hace 5 años esto levantó mucha polémica. Ahora la situación se ha consolidado y los padres pueden comparar los diferentes centros educativos de la misma zona, tienen información importante para su decisión, empezando por el resultado escolar de los alumnos y siguiendo por el conocimiento, el acceso directo a los principios educativos...  

**3 marzo 2012**  
[El TSJM permite a Educación dar resultados de sus exámenes](https://elpais.com/ccaa/2012/12/03/madrid/1354572951_033918.html)  
El Alto Tribunal ampara que ofrezca resultados de las pruebas externas de CDI La FAPA Giner de los Ríos denunció a la Consejería por "facilitar clasificaciones"  

> La sección novena de la Sala de lo Contencioso Administrativo del Alto Tribunal desestima el recurso que presentó en 2008 la Federación de Asociaciones de Padres y Madres Giner de los Ríos, mayoritaria en el sector público. La FAPA denunció la publicación de los datos en las pruebas de evaluación de 6º de primaria del curso 2007-2008 después de que Educación diera las notas.  
La LOE prohíbe que los resultados de una evaluación “puedan ser utilizados para valoraciones individuales de los alumnos o para establecer clasificaciones de los centros”. Tanto la FAPA como otros miembros de la comunidad educativa (sindicatos, oposición o la Federación de Movimientos de Renovación Pedagógica) protestaron por la difusión pública de los resultados. La Giner de los Ríos recordó además a la entonces presidenta de la Comunidad, Esperanza Aguirre, que en 2005 se había comprometido a no volver a facilitar una clasificación.  
A juicio del TSJM, esos datos suponen “un simple resumen de los resultados globales obtenidos tomando en consideración la naturaleza de públicos, concertados y privados de los centros, lo que evidentemente no implica ninguna clasificación”. Algunos medios, entre ellos EL PAÍS, elaboraron una lista de centros de mayor a menor nota con esos resultados.  
Los detractores de la prueba, instaurada en 2005, critican que no tiene en cuenta el contexto ni los recursos de los centros o la composición del alumnado. El examen de 6º (a los que se suma uno en 1º de primaria y otro en 3º de secundaria) evalúa en dos pruebas de 45 minutos los niveles de lengua y matemáticas. Educación publica los resultados del examen en su web.

[Roj: STSJ M 15196/2012 - ECLI:ES:TSJM:2012:15196](https://www.poderjudicial.es/search/AN/openDocument/8d379913cbb96aeb/20130108)  

Aparte de esta publicación, a través de una funcionalidad que se ofrece en la web de la comunidad de Madrid "Buscador de colegios" se permite clasificar varios centros según pruebas.

Enlace enero 2018 tiene "url amigable" http://www.madrid.org/buscadordecolegios pero redirige a
http://gestiona.madrid.org/wpad_pub/run/j/MostrarConsultaGeneral.icm?tipoCurso=ADM&sinboton=S
(enlace cambió, antes era gestiona.madrid.org/catcent)

[Buscador centros 2023](https://gestiona.comunidad.madrid/wpad_pub/)  

Este comparador incluso lo publicitaron cuando era ilegal, se sentían impunes y sabían que PP vía LOMCE lo haría legal

**25 abril 2013**  
[La Comunidad de Madrid estrena la web que permite comparar el nivel de los colegios - libertaddigital](https://www.libertaddigital.com/espana/2013-04-25/la-comunidad-de-madrid-estrena-la-web-que-permite-comparar-las-notas-de-los-colegios-1276488503/)  
El objetivo es estimular la competencia entre centros y que los padres tengan toda la información para poder elegir.

> La Comunidad de Madrid [ **ha estrenado una web** ](http://www.madrid.org/wpad_pub/run/j/MostrarConsultaGeneral.icm) que permite consultar los datos de los centros públicos, privados y concertados, incluidas **las calificaciones obtenidas en las pruebas a nivel** implantadas en sexto de primaria. El objetivo es que los padres conozcan todos los aspectos del colegio que quieren para sus hijos, incluido su nivel académico, a la hora de elegir.  
La web permite a los padres comparar los resultados con la media de la Comunidad de Madrid, así como **compararlos con otros centros** . En cuanto al resto de información, se incluyen aspectos como la situación, el medio de transporte, si es bilingüe o no, si incluye comedor...
La Consejería de Educación enmarca esta iniciativa en su estrategia para fomentar la calidad en los centros madrileños, al considerar que se **estimulará la competencia entre los colegios** para fomentar la excelencia entre su alumnado. Esta medida se enmarca en el establecimiento de la Zona Única en la elección de centro en la Comunidad de Madrid, que se implanta por primera vez este año para el próximo curso escolar 2013-2014, y que ha arrancado este jueves con la apertura del proceso de admisión.

[twitter FiQuiPedia/status/869208608583684096](https://twitter.com/FiQuiPedia/status/869208608583684096)  
Se anuncia algo que permite clasificar centros por pruebas de evaluación, convertido en "legal" gracias a la LOMCE  
![](https://pbs.twimg.com/media/DBAMPUJXsAQjYkG.jpg)

En abril 2018 se intenta engañar desde un responsable de la consejería  
[twitter FiQuiPedia/status/988820992226185216](https://twitter.com/FiQuiPedia/status/988820992226185216)  
Lo que dice @sanz_ismael no es cierto y lo sabe: http://gestiona.madrid.org/wpad_pub/run/j/MostrarConsultaGeneral.icm?tipoCurso=ADM&sinboton=S permite comparar centros por resultados pruebas  
[https://twitter.com/sanz_ismael/status/988684048917770240](https://twitter.com/sanz_ismael/status/988684048917770240)  
@zajo3 ¿comunidad y normativa? LOMCE deliberadamente permitió la clasificación de centros para rankings  
![](https://pbs.twimg.com/media/Dbj9RsgWkAIlHvI.jpg)  

Así que añado algo más sobre el centro que él citaba en ese hilo (IES Beatriz Galindo) que muestra que además de comparar, muestra datos de resultados del centro y de la comunidad. No lo veo ahora pero creo recordar que antes mostraba datos de CDI, igual lo están cambiando para mostrar datos de reválidas LOMCE ...

![](https://3.bp.blogspot.com/-nO6T1Gcn4z8/Wt92OwlGLnI/AAAAAAAAOyA/lPpbfhjlsUM8PA6GVEcOLtKKY5buYtw5QCLcBGAs/s16000/Screenshot-2018-4-24%2BServicio%2BWeb%2Ba%2BPadres%2Bde%2BAlumnos%2Bde%2Bla%2BComunidad%2Bde%2BMadrid.png)

Mirando más, localizo

[Servicio de evolución, desarrollo, puesta en marcha y mantenimiento del buscador de Centros Educativos de la Comunidad de Madrid y creación de catálogos de datos abiertos](https://contratos-publicos.comunidad.madrid/contrato-publico/servicio-evolucion-desarrollo-puesta-marcha-mantenimiento-buscador-centros)
[ Expediente:    C-321M/004-17 (ASER0017822017) - contrataciondelestado.es](https://contrataciondelestado.es/wps/portal/!ut/p/b0/04_Sj9CPykssy0xPLMnMz0vMAfIjU1JTC3Iy87KtUlJLEnNyUuNzMpMzSxKTgQr0w_Wj9KMyU1zLcvQjPT3DgqJSQ_08ygIDvY1CXNMi3Cu1HW1t9Qtycx0BieThvg!!/)  

Número de expediente C-321M/004-17

Y en [Prescripciones Técnicas 1574386 (Publicado el 21 agosto 2017)](https://contratos-publicos.comunidad.madrid/medias/prescripciones-tecnicas-1574386/download)

> Comparativa  de  centros,  que  permite  obtener  información  de  distintos  centros  para que  las  familias  puedan  seleccionar  la  oferta  educativa  que  más  se  adapta  a  sus  necesidades.

[Los colegios y los institutos con las mejores notas de la Comunidad de Madrid](https://www.abc.es/espana/madrid/colegios-institutos-mejores-notas-comunidad-madrid-20231017122406-nt.html)  
El San Mateo de Madrid obtiene una media de 8,57 en selectividad el último lustro

### Páginas web / empresas privadas que se dedican a los rankings

Son páginas privadas adicionales a las clasificaciones que pueden publicar las consejerías como ocurre en Madrid  
A menudo se usan en artículos de prensa  

[Buscador de centros educativos. En España hay 28.500 colegios, ¿cuál es el mejor para tus hijos? - elconfidencial](https://www.elconfidencial.com/alma-corazon-vida/educacion/2021-02-28/buscador-colegios-espana-filtros_2965308/)  

internamente usa

[micole](https://www.micole.net/)  
El buscador de colegios, escuelas infantiles e institutos nº1 de España  
Busca, compara y contacta en 1 clic  

[Consulta los resultados académicos de ESO y Bachillerato de la Comunidad de Madrid - magisnet](https://www.magisnet.com/2022/01/consulta-los-resultados-academicos-de-eso-y-bachillerato-de-la-comunidad-de-madrid-4204/)  

Cita [Consulta los colegios más demandados y los resultados académicos de la Comunidad de Madrid - Scholarum](https://www.scholarum.es/es/contenidos/elegir-colegio/consulta-los-colegios-mas-demandados-y-los-resultados-academicos-de-la-comunidad-de-madrid)  

[Los 100 mejores colegios de España en 2023 - bankinter](https://www.bankinter.com/blog/finanzas-personales/ranking-mejores-colegios-espana)  
Forbes ha analizado cuáles son los 100 mejores colegios de España tras analizar su rendimiento, servicios y dedicación.  

[buscocolegio](https://www.buscocolegio.com/)  

### Efectos de los rankings, argumentos a favor y en contra 

En este informe se citan los rankings y su efecto

[twitter FiQuiPedia/status/960846947081912321](https://twitter.com/FiQuiPedia/status/960846947081912321)  

[Magnitud de la Segregación escolar por nivel socioeconómico en España y sus Comunidades Autónomas y comparación con los países de la Unión Europea](https://ojs.uv.es/index.php/RASE/article/view/10129/10853)  

![](https://pbs.twimg.com/media/DVWc4u1WAAAVwNp.jpg)  

Lo pongo también en texto

> La segregación escolar en las Comunidades ofrece resultados extremos, algunas se encuentran entre las más bajas de Europa (Illes Balears, Galicia y Aragón) y otras como la Comunidad de Madrid con una segregación muy alta, solo superada por Hungría dentro de la Unión Europea. Con todo ello se observa la incidencia de las políticas educativas regionales respecto a los criterios de admisión de centros en la segregación escolar, mostrando que políticas como el fomento de la educación privada, de **la competencia de centros mediante la publicación de rankings** o la creación de un distrito único pueden configurar sistemas educativos inequitativos que atentan a la
igualdad de oportunidades.

En post [LOMCE: evaluaciones finales 4º ESO](http://algoquedaquedecir.blogspot.com/2017/11/lomce-evaluaciones-finales-4-eso.html) aparece esta imagen

![](https://4.bp.blogspot.com/-Ne7jyGqV8uQ/Ws5OoWVGEhI/AAAAAAAAOvM/duHTaAjK5NkMB-DrnR2_XSGTUrONPGGxQCLcBGAs/s320/RevalidasLomceRankings.jpg)

El objetivo de clasificar con pruebas está bien definido aquí

https://twitter.com/PsicEduM/status/1081106473277640704

PISA y pruebas externas: testeos para comparar centros y beneficiar a los q gozan de un entorno social favorable y penalizar al resto. Resultados q usan los centros para captar y fidelizar clientes. Datos q legitiman políticas de privatización, mercantilización y comercialización

Recogida y análisis de datos realizado por empresas privadas. Tablas para proponer soluciones tecnocráticas simples a nuestros problemas sociales complejos. Cuantofrénicos que se han convertido en los nuevos 'expertos' en educación sin pisar un aula.

**25 marzo 2019**
[Casado quiere las mismas oposiciones a profesor en toda España y que se pueda hacer ránkings de colegios - elmundo](https://amp.elmundo.es/espana/2019/03/25/5c98df3021efa0277f8b465a.html)  

> Para ello, una de las principales armas del nuevo PP va a ser recuperar las evaluaciones externas o reválidas al final de cada etapa educativa que desacafeinó precisamente el Gobierno de Mariano Rajoy. Las llamadas reválidas servirán como listón para que en todos los lugares de España se alcancen los mismos objetivos. Y eso se conseguirá no sólo con estos exámenes sino, sobre todo, con la publicación de los mismos.  
>  
>CAMBIO DE LÍNEA  
>  
> En otras palabras: Casado quiere recuperar la posibilidad de que se hagan públicos los resultados de las reválidas y está a favor de que se puedan hacer ránkings con ellos, algo que expresamente prohibió el ministro  *popular* Íñigo Méndez de Vigo.  
En el foro ha defendido expresamente una "evaluación pública de conocimientos" y ha explicado que por "pública" entiende que las pruebas se realicen por parte del Estado -no por las comunidades autónomas, como dice la  *Ley Celaá* - y **"se publiquen para que los padres puedan basar su elección en cómo va el centro y para que las autoridades con competencias en la administración educativa puedan también administrar esos centros y esos profesores".**  
**La publicación de los ránkings, opina el PP, servirá para que los colegios y las comunidades autónomas se pongan las pilas y compitan para elevar su calidad.** Como Casado quiere que haya unos mínimos comunes en toda España, eso va a relanzar, en la práctica, aquella idea que tenía José María Aznar de que en todas las comunidades autónomas se estudiara lo mismo.  

**17 abril 2021**

[Educación pública y concertada, cuando 1+1=3 - abc](https://www.abc.es/opinion/abci-ismael-sanz-jorge-sainz-educacion-publica-y-concertada-cuando-113-202104162355_noticia.html)  
«Existe una relación causal entre la libertad de elección y la mejora de los resultados de los alumnos de los centros públicos»

Respondo a uno de los autores 

[twitter FiQuiPedia/status/1383342003409145863](https://twitter.com/FiQuiPedia/status/1383342003409145863)  
Solo veo título y entradilla.  
¿Qué son mejores resultados?   
https://twitter.com/FiQuiPedia/status/1200705741365108736?s=19
¿La relación causal de educación privada con concierto y segregación global se trata en el artículo?

[twitter FiQuiPedia/status/1200705741365108736](https://twitter.com/FiQuiPedia/status/1200705741365108736)  
Los privados con concierto globalmente segregan. Ninguna visita a centros puntuales va a cambiar eso.  
"Lo puntual no niega lo global: que haya un centro puntual en el que pase algo no invalida ni es argumento para negar una tendencia global."  
[Datos segregación educación - algoquedaquedecir](https://algoquedaquedecir.blogspot.com/2020/07/datos-segregacion-educacion.html)  

La idea es esta

[twitter FiQuiPedia/status/1200705741365108736](https://twitter.com/FiQuiPedia/status/1200705741365108736)  
Y sobre 6, resultados, lo primero habría que definir cuál es el objetivo del servicio público de educación y qué es un buen resultado, y enlaza con segregación y cohesión social.  
[twitter danielegrasso/status/1180022868823941120](https://twitter.com/danielegrasso/status/1180022868823941120)  
Y PISA nos enseña que esto importa: coles de rentas altas consiguen notas más altas. Es una consecuencia de la segregación  
![](https://pbs.twimg.com/media/EGBIilSXYAAToWu?format=jpg)  

Más tarde veo artículo completo

[twitter FiQuiPedia/status/1383367131811618820](https://twitter.com/FiQuiPedia/status/1383367131811618820)  
Ya he leído el artículo completo.  

> "la competencia hace que la oferta pública se esfuerce más para evitar la huida de los estudiantes".  

Los padres eligen llevar a estudiantes a privados buscando segregación.  

> "pruebas externas y estandarizadas que permitan a las familias saber cuál es la evolución de sus hijos y, llegado el caso, poder optar por otros centros"

### Reclamación ilegalidad clasificación centros Ley Maestra

**20 diciembre 2021**

[Dictamen de la Comisión de Educación, Universidades y Ciencia sobre el Proyecto de Ley PL 1(XII)/21 RGEP 8682, Maestra de libertad de elección educativa de la Comunidad de Madrid.](https://www.asambleamadrid.es/static/docs/registro-ep/RGEP20306-21.pdf#page=32)

En página 32 enmienda transaccional 15 a la enmienda número 22 de VOX que añadió el artículo 5.1.h

No estaba en texto enviado a Asamblea en julio 2021  
[Boletín Oficial de la Asamblea de Madrid Número 7 15 de julio de 2021](https://www.asambleamadrid.es/static/doc/publicaciones/BOAM_12_00007_fasciculo1.pdf)  

[twitter FiQuiPedia/status/1473048920104312835](https://twitter.com/FiQuiPedia/status/1473048920104312835)  

Dado que [artículo 140.2 LOE](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a140) es básico según [disposición final quinta](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta) esta enmienda transaccional que @ppmadrid ha aceptado es inconstitucional @MorenoG_Agustin  @MariaPastorV  
![](https://pbs.twimg.com/media/FHFSLyMWYAAIQqa?format=jpg)  


**25 agosto 2022**

[twitter FiQuiPedia/status/1562729200225640449](https://twitter.com/FiQuiPedia/status/1562729200225640449)  
El plan de inspección de @educacmadrid  menciona explícitamente que se revise cumplir art5.1.h, que fue añadido por V🤮X y que contradice normativa básica 140.2 LOE. @ppmadrid  insiste en usar evaluación para la clasificación y la segregación de centros.  
[RESOLUCIÓN de 10 de agosto de 2022, de la Viceconsejería de Organización Educativa, por la que se aprueba el Plan Anual de Actuación de la Inspección Educativa para el curso 2022-2023](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/08/25/BOCM-20220825-4.PDF)  

**17 septiembre 2022**

[Formulario solicitud genérica. Presentación de escritos y comunicaciones. Formulario genérico](https://tramita.comunidad.madrid/prestacion-social/formulario-solicitud-generica/tramitar)  

---

Expone: 

En RESOLUCIÓN de 10 de agosto de 2022, de la Viceconsejería de Organización
Educativa, por la que se aprueba el Plan Anual de Actuación de la Inspección
Educativa para el curso 2022-2023, 4.2.3. Supervisión de la organización, las
enseñanzas y los resultados de los centros (HMR3) se indica  
https://www.bocm.es/boletin/CM_Orden_BOCM/2022/08/25/BOCM-20220825-
4.PDF  
"Supervisar los resultados académicos, con especial atención a los referidos en el
artículo 5.1.h. de la Ley 1/2022, de 10 de febrero, así como comprobar su difusión
entre los miembros de la comunidad educativa."  
En dicho artículo 5.1h se indica  
https://www.boe.es/buscar/act.php?id=BOE-A-2022-6768#a5  
h) Los resultados individualizados por cada centro de todas las pruebas generales en
la que participen los alumnos serán públicos, detallando el resultado obtenido en
cada caso por etapa educativa y en cada una de las dimensiones evaluadas. En
especial, los resultados de las pruebas de acceso a la Universidad, pruebas externas
de carácter nacional e internacional, así como las pruebas de evaluación de la
Comunidad de Madrid.  
En artículo 140.2 de LOE se indica  
https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a140  
2. La finalidad establecida en el apartado anterior no podrá amparar que los
resultados de las evaluaciones del sistema educativo, independientemente del ámbito
territorial estatal o autonómico en el que se apliquen, puedan ser utilizados para
valoraciones individuales del alumnado o para establecer clasificaciones de los
centros.  
Dicho artículo 140.2 es básico según Disposición final quinta  
https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta  
Ahora mismo en el buscador de centros educativos de la Comunidad de Madrid  
https://gestiona3.madrid.org/wpad_pub/  
Además de mostrar resultados académicos, existe la opción "comparar centros", que indica "Seleccione un indicador:" en el que permite elegir "Prueba de nivel de inglés en centros bilingües y PAU/EvAU" lo que objetivamente puede ser utilizado para establecer clasificaciones de centros. (se adjunta captura)  

Solicita:

Que inspección aclare si artículo 5.1.h Ley 1/2022 contradice normativa básica (artículo 140.2 LOE) y no se puede cumplir. Si efectivamente no se puede cumplir:  
- Que interponga si procede un recurso para que se anule dicho artículo por no cumplir normativa básica de rango superior e incumplir artículo 9.3 de la Constitución Española.  
- Que realice acciones para que el buscador de centros de la Comunidad de Madrid no permita comparaciones por resultados de evaluaciones incumpliendo artículo 140.2 LOE.  

---

Tras varios intentos me da error, uso Clave permanente

49/886300.9/22

No recibo ninguna respuesta de inspección.

**22 diciembre 2023**

[RESOLUCIÓN conjunta de 4 de diciembre de 2023, de las Viceconsejerías de Política Educativa y de Organización Educativa, sobre la información a las familias que deben contener las páginas web de los centros educativos de la Comunidad de Madrid, en cumplimiento de la Ley 1/2022, de 10 de febrero, Maestra de Libertad de Elección Educativa de la Comunidad de Madrid.](https://bocm.es/boletin/CM_Orden_BOCM/2023/12/22/BOCM-20231222-20.PDF)  

> 2 Contenidos obligatorios que deben incluir las páginas web de los centros  
> ...  
> Resultados del centro en todas las pruebas generales en la que participen los alumnos, detallando el resultado obtenido por etapa educativa y en cada una de las dimensiones evaluadas; entre ellos al menos:  
• Pruebas de acceso a la Universidad.  
• Pruebas externas de carácter comunitario, nacional e internacional.  
• Otras pruebas de evaluación de la Comunidad de Madrid, incluidas las de evaluación del programa Bilingüe español-inglés  

**3 enero 2024**

Planteo nuevo escrito

[Formulario solicitud genérica. Presentación de escritos y comunicaciones. Formulario genérico](https://tramita.comunidad.madrid/prestacion-social/formulario-solicitud-generica/tramitar)  

(El expone lo pongo en pdf separado, y en formulario indico "Se adjunta documento externo.")

---

Expone: 

Como empleado público me aplica artículo 54.3 del EBEP

https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a53 

> 3. Obedecerán las instrucciones y órdenes profesionales de los superiores, salvo que constituyan una infracción manifiesta del ordenamiento jurídico, en cuyo caso las pondrán inmediatamente en conocimiento de los órganos de inspección procedentes.

Habiendo identificado una normativa de la Consejería de Educación de Madrid cuyo cumplimiento constituye una infracción manifiesta de la jerarquía normativa garantizada por el artículo 9.3 de la Constitución Española, lo detallo y pongo en conocimiento de los órganos de inspección procedentes para que como empleados públicos y en su función de inspección, realicen las acciones necesarias para garantizar el cumplimiento del ordenamiento jurídico y la Constitución.

En RESOLUCIÓN conjunta de 4 de diciembre de 2023, de las Viceconsejerías de Política Educativa y de Organización Educativa, sobre la información a las familias que deben contener las páginas web de los centros educativos de la Comunidad
de Madrid, en cumplimiento de la Ley 1/2022, de 10 de febrero, Maestra de Libertad de Elección Educativa de la Comunidad de Madrid.

https://bocm.es/boletin/CM_Orden_BOCM/2023/12/22/BOCM-20231222-20.PDF

Se indica

> 2 Contenidos obligatorios que deben incluir las páginas web de los centros  
> ...  
> Resultados del centro en todas las pruebas generales en la que participen los alumnos, detallando el resultado obtenido por etapa educativa y en cada una de las dimensiones evaluadas; entre ellos al menos:  
• Pruebas de acceso a la Universidad.  
• Pruebas externas de carácter comunitario, nacional e internacional.  
• Otras pruebas de evaluación de la Comunidad de Madrid, incluidas las de evaluación del programa Bilingüe español-inglés  

Dicha publicación aparentemente es legal ya que se ampara en artículo 5.1.h. de la Ley 1/2022 que indica  
https://www.boe.es/buscar/act.php?id=BOE-A-2022-6768#a5  
h) Los resultados individualizados por cada centro de todas las pruebas generales en  la que participen los alumnos serán públicos, detallando el resultado obtenido en cada caso por etapa educativa y en cada una de las dimensiones evaluadas. En especial, los resultados de las pruebas de acceso a la Universidad, pruebas externas de carácter nacional e internacional, así como las pruebas de evaluación de la Comunidad de Madrid.  

Sin embargo en artículo 140.2 de LOE se indica  
https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a140  
> 2. La finalidad establecida en el apartado anterior no podrá amparar que los resultados de las evaluaciones del sistema educativo, independientemente del ámbito territorial estatal o autonómico en el que se apliquen, puedan ser utilizados para valoraciones individuales del alumnado o para establecer clasificaciones de los centros.

Dicho artículo 140.2 es básico según Disposición final quinta  
https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta  

Ahora mismo en el buscador de centros educativos de la Comunidad de Madrid  
https://gestiona.comunidad.madrid/wpad_pub  
Además de mostrar resultados académicos, existe la opción "comparar centros", que indica "Seleccione un indicador:" en el que permite elegir "Prueba de nivel de inglés en centros bilingües y PAU/EvAU" lo que objetivamente puede ser utilizado para establecer clasificaciones de centros.

Por lo tanto, se puede afirmar que tanto la resolución de 4 de diciembre 2023 indicada como el artículo 5.1.h. de la Ley 1/2022 no respetan el ordenamiento jurídico y la Constitución.

Solicita:

Que inspección aclare si artículo 5.1.h Ley 1/2022 contradice normativa básica (artículo 140.2 LOE) y no se puede cumplir. Si efectivamente no se puede cumplir:  
- Que interponga si procede un recurso para que se anule dicho artículo por no cumplir normativa básica de rango superior e incumplir artículo 9.3 de la Constitución Española.  
- Que realice acciones para que el buscador de centros de la Comunidad de Madrid no permita comparaciones por resultados de evaluaciones incumpliendo artículo 140.2 LOE.  
- Que anule la parte incorrecta de la resolución de 4 de diciembre de 2023 citada en expone.  

Parte de esta solicitud la realicé el 17 septiembre 2022 dirigida a Subdirección General de Inspección Educativa con referencia de registro 49/886300.9/22, sin respuesta. Dirigiré la misma reclamación a otras instancias como el Defensor del Pueblo o Inspección educativa del Ministerio de Educación en caso de no obtener respuesta.

---

El caso de Newman de los 1800 € me hicieron caso y lo dirigí a Dirección de Área Territorial de Madrid-Capital, no a Subdirección General de Inspección Educativa, pero no lo envío a la DAT.

09/006828.9/24

Mismo día lo envío a alta inspección con correo a educacion.madrid@correo.gob.es  

---

Buenos días

No sé si existe una vía más formal / registro para hacerlo llegar, por lo que envío este correo y adjunto documentos.
(la dirección la he encontrado aquí https://mpt.gob.es/portal/delegaciones_gobierno/delegaciones/madrid/servicios/educacion.html)

He puesto hoy reclamación a inspección de Madrid, pero no obtuve respuesta en 2022 a pesar de presentarlo por registro.

---

**8 enero 2024**

Recibo respuesta educacion.madrid@correo.gob.es

---

Buenos días,

Con esta respuesta le confirmamos la recepción de su correo.

Le estamos muy agradecidos por habernos hecho llegar la reclamación sobre incumplimiento de normativa básica de educación en Madrid que usted ha dirigido a la Subdirección General Inspección Educativa de la Comunidad de Madrid. Le rogamos que nos mantenga informados de cualquier novedad que se produzca con respecto  a dicha reclamación.

Reciba un cordial saludo,

ALTA INSPECCIÓN DE EDUCACIÓN  
educacion.madrid@correo.gob.es  
ÁREA DE ALTA INSPECCIÓN DE EDUCACIÓN - educacion.madrid@correo.gob.es  
Calle García Paredes, 65 4ª planta  
Teléfono: 912729252  
Dir3: EA0040725  

---

**10 enero 2024**

Recibo [respuesta de Madrid a reclamación clasificación centros](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Clasificaci%C3%B3nCentros/2023-01-10-RespuestaMadridReclamaci%C3%B3nClasificaci%C3%B3nCentros.pdf)

Respondo

---

Respondo por esta vía, no sé si es necesario que lo envíe vía registro, poniendo en copia al área de alta inspección de educación de la delegación de gobierno

La respuesta recibida hoy no rebate ni niega que la normativa de Madrid incumpla normativa básica; que una norma haya cumplido los procedimientos de elaboración no impide que pueda tener puntos que incumplan normativa básica y que sean susceptibles de ser recurridos ante los tribunales, que pueden incluso anularlos, como ha realizado por ejemplo en numerosas ocasiones por el Tribunal Constitucional.

Respecto al informe de la abogacía general que se cita en la respuesta, cuyo enlace directo es https://www.comunidad.madrid/transparencia/sites/default/files/42_inf_serv_juridico_sj458_21.pdf  se puede ver que tiene fecha de junio 2021, por lo que es anterior a la enmienda transaccional 15 a la enmienda número 22 de VOX que añadió el artículo 5.1.h que se puede ver en página 32 del pdf https://www.asambleamadrid.es/static/docs/registro-ep/RGEP20306-21.pdf de diciembre 2021 y que no estaba en proyecto de ley enviado a la Asamblea en julio 2021 como se puede ver en página 88 del pdf https://www.asambleamadrid.es/static/doc/publicaciones/BOAM_12_00007_fasciculo1.pdf, por lo que durante el trámite de audiencia, en el que yo mismo realicé alegaciones e incluso alguna fue tenida en cuenta en el texto definitivo, nadie pudo realizar alegaciones sobre ese artículo 5.1.h.

Por lo tanto la abogacía no ha emitido informe respectivo al artículo 5.1.h sobre el que planteo la reclamación; desconozco si existe un servicio jurídico adicional de la Asamblea que haya realizado un informe sobre la enmienda.

--- 

Pido en [Asamblea > Transparencia > Derecho de Acceso a la Información > Unidad de Informacion > Solicitud de acceso a la información pública](https://www.asambleamadrid.es/transparencia/derecho-acceso-informacion/unidad-informacion/solicitud-de-acceso-a-la-informacion-publica)

---

Copia de los informes jurídicos si existen realizados por servicios jurídica de la asamblea o por la abogacía de la comunidad asociados a validar las enmiendas realizadas sobre Ley 1/2022, en especial  sobre enmienda transaccional 15 a la enmienda número 22 de VOX que añadió el artículo 5.1.h

Motivación

En la elaboración de ley 1/2022 hubo informe de la abogacía general https://www.comunidad.madrid/transparencia/sites/default/files/42_inf_serv_juridico_sj458_21.pdf  se puede ver que tiene fecha de junio 2021, por lo que es anterior a la enmienda transaccional 15 a la enmienda número 22 de VOX que añadió el artículo 5.1.h que se puede ver en página 32 del pdf https://www.asambleamadrid.es/static/docs/registro-ep/RGEP20306-21.pdf de diciembre 2021 y que no estaba en proyecto de ley enviado a la Asamblea en julio 2021 como se puede ver en página 88 del pdf https://www.asambleamadrid.es/static/doc/publicaciones/BOAM_12_00007_fasciculo1.pdf, por lo que durante el trámite de audiencia, en el que yo mismo realicé alegaciones e incluso alguna fue tenida en cuenta en el texto definitivo, nadie pudo realizar alegaciones sobre ese artículo 5.1.h.  
Por lo tanto el informe de abogacía citado no contempla el artículo 5.1.h

Mismo día 10 enero reenvío a alta inspección por correo electrónico

**15 enero 2024**

Recibo respuesta alta inspección 

---

Buenos días,

Por el presente correo electrónico le confirmamos la recepción de los dos correos que no envió el pasado día 10 de enero.

Le agradecemos que nos haya informado de la respuesta que dio la Subdirección General Inspección Educativa de la Comunidad de Madrid a su reclamación, y tomamos nota de los comentarios que hace respecto al informe de la Abogacía.

Atentamente,

---

**24 febrero 2024**

Reclamo a Consejo de Transparencia

26 febrero me indican recepción RDACTPCM078/2024

**13 marzo 2024**

Recibo documento de Unidad de Información - Asamblea de Madrid uisa@asambleamadrid.es

[Resolución 1/24](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Normativa/Madrid/LeyMaestra/2024-03-13-Resoluci%C3%B3n%201-24%20Nominativa.pdf) 

Escribo a Consejo

>Envío esta información que creo está asociada a RDACTPCM078/2024 (de la que se me indicó recepción pero no admisión, y como ya he indicado el correo no menciona brevemente el asunto de la reclamación, por lo que teniendo varias a veces no es fácil asociar a cuál)  
He respondido con copia al consejo, pero he recibido respuesta indicando que no permiten recibir información.  
Se me indica "En relación a la cuestión planteada se traslada que puede consultar en la página web de la Asamblea, el Dictamen aprobado por la Comisión de Educación, Universidades y Ciencia, en su sesión de 14 de diciembre de 2021, sobre el Proyecto de Ley 1(XII)/21, maestra de libertad de elección educativa de la Comunidad de Madrid, donde se recogen los trabajos desarrollados por la Ponencia en relación a las enmiendas referidas en su consulta."  
Pero no se aporta enlace y no encuentro dictamen en https://www.asambleamadrid.es/actividad/sesiones/sesion?sesion=2T-4783  
Creo que hay un criterio general que indica que las resoluciones cuando remiten a información deben especificar el enlace.

**18 marzo 2024**

Recibo correos del Consejo

> Le informamos que nos pondremos en contacto con la Asamblea en el día de hoy para que le faciliten el enlace directo a la información, independientemente de la tramitación de su reclamación RDACTPCM078/2024, que sigue su curso y en la actualidad se encuentra pendiente de admisión a trámite. 

>Tras ponernos en contacto con la Asamblea en relación a lo que nos indicó en su correo anterior, nos informan que se verificaron los enlaces y que están correctos. De todas formas nos indican estos tres enlaces a través de los cuales podrá acceder a la información que solicitó:   
Dicho documento está disponible en tres enlaces (adjunto los remito):  
    BOAM: https://www.asambleamadrid.es/static/doc/publicaciones/BOAM_12_00028.pdf  
    DDSS: https://www.asambleamadrid.es/static/doc/publicaciones/XII-DS-133.pdf  
    INFORME COMISIÓN: https://www.asambleamadrid.es/static/docs/registro-ep/RGEP20306-21.pdf  
Quedamos a la espera de su respuesta para saber si pudo acceder a la información.  

Respondo:


Confirmo que puedo acceder a dichos documentos, pero eso no resuelve mi solicitud de acceso ni mi reclamación.

En mi solicitud pedía  
"
Copia de los informes jurídicos si existen realizados por servicios jurídica de la asamblea o por la abogacía de la comunidad asociados a validar las enmiendas realizadas sobre Ley 1/2022, en especial  sobre enmienda transaccional 15 a la enmienda número 22 de VOX que añadió el artículo 5.1.h  
"  
e indicaba como motivación  
"  
En la elaboración de ley 1/2022 hubo informe de la abogacía general https://www.comunidad.madrid/transparencia/sites/default/files/42_inf_serv_juridico_sj458_21.pdf  se puede ver que tiene fecha de junio 2021, por lo que es anterior a la enmienda transaccional 15 a la enmienda número 22 de VOX que añadió el artículo 5.1.h que se puede ver en página 32 del pdf https://www.asambleamadrid.es/static/docs/registro-ep/RGEP20306-21.pdf de diciembre 2021 y que no estaba en proyecto de ley enviado a la Asamblea en julio 2021 como se puede ver en página 88 del pdf https://www.asambleamadrid.es/static/doc/publicaciones/BOAM_12_00007_fasciculo1.pdf, por lo que durante el trámite de audiencia, en el que yo mismo realicé alegaciones e incluso alguna fue tenida en cuenta en el texto definitivo, nadie pudo realizar alegaciones sobre ese artículo 5.1.h.  
Por lo tanto el informe de abogacía citado no contempla el artículo 5.1.h
"  

Por lo tanto se puede ver que mi solicitud ya citaba el documento que ahora me enlazan https://www.asambleamadrid.es/static/docs/registro-ep/RGEP20306-21.pdf  
En ese documento no existe ningún análisis jurídico. Si la respuesta es que ese análisis jurídico que solicito no existe, se debería indicar explícitamente, ya que mi solicitud indicaba "copia de los informes jurídicos SI EXISTEN realizados por servicios jurídica de la asamblea o por la abogacía de la comunidad..."

**8 abril 2024**

Asociado a pruebas de evaluación veo esto en RESOLUCIÓN CONJUNTA DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA POR LA QUE SE DICTAN INSTRUCCIONES PARA LA CELEBRACIÓN DE LAS PRUEBAS DE LOS ALUMNOS DE SEXTO CURSO DE EDUCACIÓN PRIMARIA Y CUARTO CURSO DE EDUCACIÓN SECUNDARIA OBLIGATORIA DE LA COMUNIDAD DE MADRID PARA EL CURSO ESCOLAR 2023-2024.  
CSV 0889204556721578998904

> Asimismo, tal y como establece el artículo 140.2 de la Ley Orgánica 2/2006, de 3 de mayo, de Educación, los resultados de las evaluaciones del sistema educativo, independientemente del ámbito territorial estatal o autonómico en el que se apliquen, no podrán ser utilizados para valoraciones individuales del alumnado o para establecer clasificaciones de los centros.

En [RESOLUCIÓN DE 16 DE ABRIL DE 2024, DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y ORGANIZACIÓN EDUCATIVA, POR LA QUE SE DICTAN NUEVAS INSTRUCCIONES RELATIVAS A LAS PRUEBAS PARA LA EVALUACIÓN DE DIAGNÓSTICO DE LOS ALUMNOS DE CUARTO CURSO DE EDUCACIÓN PRIMARIA Y SEGUNDO CURSO DE EDUCACIÓN SECUNDARIA OBLIGATORIA Y DE LAS PRUEBAS PARA LA EVALUACIÓN DE FIN DE ETAPA DE LOS ALUMNOS DE SEXTO CURSO DE EDUCACIÓN PRIMARIA Y CUARTO CURSO DE EDUCACIÓN SECUNDARIA OBLIGATORIA EN LA COMUNIDAD DE MADRID PARA EL CURSO ESCOLAR 2023-2024.](https://www.educa2.madrid.org/web/educamadrid/principal/files/secondary/ad13fd46-4e09-4e5e-a16c-7bae7d340c8a/Resolucio%CC%81n%2016%20de%20abril%20de%202024%20Pruebas%20Diagno%CC%81sticas%20y%20Pruebas%20de%20Fin%20de%20Etapa.pdf?t=1713531782192)  precisamente eliminan ese párrafo.

**13 septiembre 2024**

Recibo notificación Consejo de Transparencia y Protección de datos indicando que retoman la reclamación al no existir el Consejo de Transparencia y Participación que inició RDACTPCM078/2024

> En relación con el expediente arriba indicado, iniciado con motivo de la reclamación
formulada por usted ante el Consejo de Transparencia y Participación, le informamos que, de
acuerdo con lo establecido en la disposición transitoria única de la Ley 16/2023, de 27 de diciembre,
de medidas para la simplificación y mejora de la eficacia de instituciones y organismos de la
Comunidad de Madrid, las reclamaciones en materia de acceso a la información pendientes de
resolución a 22 de mayo de 2024, fecha del nombramiento del Presidente del Consejo de
Transparencia y Protección de Datos, serán resueltas por este Consejo.  
De la documentación facilitada por el extinto Consejo de Transparencia y Participación, se
desprende que su reclamación no ha sido resuelta. Le informamos que su tramitación se ajustará,
según establece el artículo 49 de la Ley 10/2019, de 10 de abril, de Transparencia y Participación de
la Comunidad de Madrid (LTPCM), a lo dispuesto en la legislación básica del Estado, concretamente,
al contenido de la Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las
Administraciones Públicas (LPAC).  
Asimismo, damos traslado de su reclamación a la Asamblea de Madrid para que, según lo
dispuesto en los artículos 79 y 82 LPAC, remitan informe en relación con el asunto objeto de la misma
y formulen las alegaciones que consideren oportunas, ya que no consta en el expediente que dicho
traslado haya sido realizado por el anterior Consejo.  
LA SECRETARIA GENERAL DEL
CONSEJO DE TRANSPARENCIA Y PROTECCIÓN DE DATOS  
Alicia Alonso Valenzuela  


