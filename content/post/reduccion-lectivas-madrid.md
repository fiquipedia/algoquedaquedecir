+++
author = "Enrique García"
title = "Reducción lectivas Madrid"
date = "2024-04-22"
tags = [
    "educación", "Madrid", "horario", "recortes"
]
toc = true

+++

Revisado 8 marzo 2025

### Resumen

En abril 2024 la Comunidad de Madrid realiza una "propuesta" de reducción de horas lectivas que se rechaza y lleva a una convocatoria de huelga los días 8 y 21 mayo 2024. Intento comentar cómo se ha llegado ahí y lo que dice esa propuesta. Vuelve a haber huelga el 29 octubre 2024.  

Posts relacionados:

* [Horarios secundaria en distintas CCAA](https://fiquipedia.gitlab.io/algoquedaquedecir/post/horarios-secundaria-en-distintas-ccaa/)  
* [Horarios docentes secundaria](http://algoquedaquedecir.blogspot.com/2017/09/horarios-docentes-secundaria.html)  
* [Derogar RDL14/2012](http://algoquedaquedecir.blogspot.com/2018/08/derogar-rdl142012.html)  

## Detalle

Desde 2011 en Madrid se está a 20 periodos lectivos en secundaria y 25 en primaria en lugar de los 18 y 23 que había anteriormente. Se hizo como recortes, primero en Madrid y luego a nivel estatal con RDL14/2012, y tras la derogación de RDL14/2012 en 2020 se ha ido regresando a la situación anterior en las CCAA, que son las que tienen las competencias en educación, pero no lo ha hecho Madrid.  
El cambio lo deben hacer las CCAA porque al derogar RDL14/2012 no se puso el máximo de 18/23 en la normativa básica, sino solo una recomendación de ese valor. 

**19 diciembre 2023**
[Cadena humana en apoyo a la reducción de horas lectivas para el profesorado de la Comunidad de Madrid ](https://www.elsaltodiario.com/educacion-publica/8000-firmas-apoyo-reduccion-horas-lectivas-profesorado-comunidad-madrid)  
>  La asamblea de docentes “Menos lectivas” convoca esta tarde un acto con la entrega de firmas de 8.000 docentes para exigir la bajada de horas lectivas en todas las etapas educativas. Madrid es la única Comunidad donde persiste esta carga para todo el profesorado.

**9 enero 2024**

CCOO plantea una encuesta

[Acuerdo Sectorial: apartados horario y ratios](https://feccoo-madrid.org/noticia:681492--Acuerdo_Sectorial_apartados_horario_y_ratios&opc_id=9f666eca08d47ba26c6a09927d7abbbb)  


**30 enero 2024**

[Huelga de profesorado en Madrid ante el “inaceptable” nuevo acuerdo para la reducción de horas lectivas](https://www.elsaltodiario.com/educacion-publica/huelga-profesorado-madrid-inaceptable-nuevo-acuerdo-reduccion-horas-lectivas)  
> Mientras la mesa sectorial negocia un preacuerdo “insuficiente” que no baja las horas lectivas de los maestros y maestras de Infantil y Primaria, la asamblea Menos Lectivas, con el apoyo de CGT, STEM y CNT, convoca tres paros en febrero. 

**9 febrero 2024**

Resultados encuesta CCOO. 

[Consulta de CCOO al profesorado: Alta participación y firme apoyo](https://feccoo-madrid.org/noticia:684395--Consulta_de_CCOO_al_profesorado_Alta_participacion_y_firme_apoyo&opc_id=9f666eca08d47ba26c6a09927d7abbbb)  

Con polémica por publicarlos inicialmente mal, y por lo extraño de que los profesor de primaria apoyen un acuerdo que les ignora

[twitter diegoredondo13/status/1755983972000051525](https://twitter.com/diegoredondo13/status/1755983972000051525)  
Tras el error de @CCOOEducaMa con los porcentajes en Secundaria que no sumaban 100% muy bien por borrarlo y publicarlo de nuevo pero con un pequeño detalle, ya sin permitir comentarios. Abiertos al diálogo... Sigue sin encajarme que los maestros voten a favor de firmar

**22 febrero 2024**

[Claves de la huelga de profesorado en la Comunidad de Madrid](https://www.elsaltodiario.com/educacion-publica/claves-huelga-educativa-comunidad-madrid)  
> Los próximos 27, 28 y 29 de febrero los sindicatos CGT, STEM y CNT convocan una huelga entre el profesorado no universitario para exigir la reducción en las horas lectivas, volviendo al escenario anterior a la crisis económica de 2011. 

**7 marzo 2024**

[La Comunidad de Madrid rompe las negociaciones sobre la reducción del horario lectivo en Educación ](https://www.elsaltodiario.com/educacion-publica/comunidad-madrid-rompe-negociaciones-reduccion-del-horario-lectivo-educacion)  
> El gobierno regional informa a los sindicatos de la mesa sectorial que retira la propuesta de reducir las horas lectivas del profesorado en Secundaria, FP y Régimen Especial, así como la bajada de ratios. 

**8 abril 2024**

[Madrid retira su oferta para reducir la jornada lectiva de los profesores, que siguen sin recuperar sus derechos laborales de antes de 2011](https://elpais.com/espana/madrid/2024-04-08/madrid-retira-su-oferta-para-reducir-la-jornada-de-los-profesores-que-siguen-sin-recuperar-sus-derechos-laborales-de-antes-de-2011.html#)  
>La Administración ofrece acortar el horario una hora a la semana en lugar de dos, como propuso a finales de 2023, y deja fuera a los de Primaria. Sindicatos de enseñanza como CC OO, UGT y CSIF preparan un calendario de manifestaciones
Un niño en una mesa de un aula con distinto material escolar durante una visita de la presidenta de la Comunidad de Madrid, Isabel Díaz Ayuso al Colegio público de Educación Infantil y Primaria San Juan Bautista.  

[twitter rosa_linaresr/status/1777776994278662586](https://twitter.com/rosa_linaresr/status/1777776994278662586)  
Docentes de Madrid: La propuesta de acuerdo de la Consejería de Educación es una vergüenza. Hay que armarse de paciencia para terminar de leer el documento. Si frente a esto no hay movilizaciones, no quiero imaginarme las condiciones laborales que nos esperan.  
![](https://pbs.twimg.com/media/GKvuwJtXsAADD5W?format=jpg)  
![](https://pbs.twimg.com/media/GKvuwJtX0AA8JQy?format=jpg)  

Pongo aquí en formato texto:  

>PROPUESTA DE ACUERDO DE LA CONSEJERÍA DE EDUCACIÓN, CIENCIA Y UNIVERSIDADES A LA MESA SECTORIAL DEL PERSONAL DOCENTE NO UNIVERSITARIO.  
Sin el gran trabajo y esfuerzo del personal docente de la Comunidad de Madrid, no sería posible mantener el elevado nivel de calidad de nuestro sistema educativo. El Gobierno de la Comunidad es consciente de que el profesor y el maestro son esenciales e insustituibles, y apuesta por ellos. De ahí nuestras políticas e ingente esfuerzo presupuestario de estos años:  
>* Una histórica reducción de ratios en primaria y secundaria, con un gran esfuerzo presupuestario en contratación de casi 2.700 docentes y 130M€ anuales, así como de construcción y adaptación de infraestructuras educativas. La reducción de ratios era una reivindicación constante y el Gobierno de Díaz Ayuso ha sido el primero en llevarla a cabo.  
>* Estabilización de la plantilla de docentes mediante convocatoria anual de concurso-oposición con un elevado número de plazas. Desde 2019 han sido más de 13.000 y se han ofertado 1.300 plazas para el curso que viene.  
>* Incrementos retributivos. Desde el curso 2022-2023, se ha aplicado mejora salarial a más de 86.000 docentes: 1.050 € anuales al cuerpo de maestros / 1.260 € al cuerpo de profesores con una inversión de 100 M€ anuales.  
>* Incremento progresivo del número de docentes en la educación pública hasta alcanzar la cifra record en la actualidad.   
>* Incremento de las productividades con 40,000 docentes beneficiados y una inversión de 41 M€ anuales. Entre ellas, las tutorías, que prácticamente suponen el 25% de esta inversión. Este Gobierno apuesta por el papel del tutor, quien mejor conoce al alumno y las familias.  
>* Mejoras en la formación docente, con el Plan de Capacitación docente, incluyendo la retribución de los tutores de prácticas, y todas las actividades anuales.  
>* Aprobación de un Acuerdo sobre teletrabajo.  
>* Aprobación de un Acuerdo sobre personal interino.  
>* Aumento de la plantilla de personal administrativo para rebajar las trabas burocráticas, con la contratación de más de 820 profesionales a lo largo de esta legislatura  
>La representación sindical en la Mesa Sectorial del Personal Docente no Universitario reivindica la necesidad de recuperar el régimen de 18 horas lectivas que los profesores de secundaria disfrutaban hasta 2012 y que se vio incrementado hasta 20 como consecuencia de la crisis financiera.  
Debido a la enorme inversión llevada a cabo durante este tiempo, unido a la complicada situación económica nacional e internacional, con unas previsiones de crecimiento económico muy preocupantes y la ausencia de Presupuestos Generales del Estado, así como de entregas a cuenta las Comunidades Autónomas, a fecha actual, por parte del Gobierno Central, esta reducción deberá hacerse progresivamente.  
Empezando con una reducción de 1 hora lectiva a los docentes no tutores de enseñanzas secundaria, formación profesional y de régimen especial durante el curso 2026/2027.  
Esta primera reducción horaria de la jornada lectiva se realizará de manera compatible con la progresiva reducción de alumnos por aula y se seguirá desarrollando una vez concluya la bajada de ratios en todos los cursos, hasta cumplir el objetivo de horas lectivas al que se refiere la representación sindical en la Mesa Sectorial del Personal Docente no Universitario.  
Manifestamos la firme voluntad y el compromiso de revisar este acuerdo en cuanto se produzca una mejora sustancial de la coyuntura económica y presupuestaria, para avanzar hasta lograr el objetivo propuesto.  

**28 octubre 2024**
[Madrid considera "razonable" la bajada del horario lectivo de los docentes - larazon](https://www.larazon.es/madrid/madrid-considera-razonable-bajada-horario-lectivo-docentes_20241028671f99b4d8f8950001d31fe1.html)  
> El consejero de Educación, Ciencia y Universidades, Emilio Viciana, considera "razonable" la bajada del horario lectivo de los docentes, según ha explicado este lunes durante su visita al instituto público San Juan de la Cruz, en el municipio de Pozuelo de Alarcón.  
"Cada vez que me he reunido con los sindicatos así lo he manifestado y es una de las cuestiones que queremos abordar en la reunión que vamos a hacer", ha expresado el consejero, en referencia al encuentro que tendrá con los sindicatos de la Mesa Sectorial de Educación (CC.OO., ANPE, CSIF y UGT) el próximo jueves, 31 de octubre, a las 13 horas.

**31 octubre 2024**  

[twitter EVicianaDuro/status/1851991579671568472](https://x.com/EVicianaDuro/status/1851991579671568472)   
Hoy he presentado a los sindicatos un acuerdo para reducir ya desde el próximo curso 2025/26 las horas lectivas de los profesores de la región a 19h.  
Una propuesta responsable, que permite seguir avanzando en la atención que reciben a los alumnos y que espero estudien con rigor.  

Todos los sindicatos la rechazan


[Valoración de la reunión con el Consejero de Educación 31 octubre 2024 - csif](https://www.csif.es/es/articulo/comunidaddemadrid/educacion/60044)  

> Resumen de la propuesta de hoy:  
Se nos propone solo en secundaria una bajada progresiva: 
25/26 19 horas para los no tutores y 20 tutores  
27/28 18 horas para los tutores  
28/29 18 horas para los no tutores   


[CCOO rechaza la propuesta del Consejero y llama al profesorado a secundar las movilizaciones convocadas](https://feccoo-madrid.org/noticia:708959--CCOO_rechaza_la_propuesta_del_Consejero_y_llama_al_profesorado_a_secundar_las_movilizaciones_convocadas&opc_id=9f666eca08d47ba26c6a09927d7abbbb)

[ACUERDO DE LA MESA SECTORIAL DE PERSONAL DOCENTE NO UNIVERSITARIO POR EL QUE SE REGULA LA REDISTRIBUCIÓN DEL HORARIO DEL PERSONAL DOCENTE EN LOS CENTROS PUBLICO$ NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID, EN EL MARCO DE OTRAS MEDIDAS PARA
MEJORAR LA ATENCIÓN A LOS ALUMNOS, LA CALIDAD EDUCATIVA Y EL APOYO A LA LABOR DOCENTE, ASÍ COMO PARA COMBATIR EL ABANDONO ESCOLAR.](https://feccoo-madrid.org/196ce7ecd233051dc9a23ba502537b74000063.pdf)  

Lo pongo aquí en modo texto

> ACUERDO DE LA MESA SECTORIAL DE PERSONAL DOCENTE NO UNIVERSITARIO POR EL QUE SE REGULA LA REDISTRIBUCIÓN DEL HORARIO DEL PERSONAL DOCENTE EN LOS CENTROS PUBLICO$ NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID, EN EL MARCO DE OTRAS MEDIDAS PARA
MEJORAR LA ATENCIÓN A LOS ALUMNOS, LA CALIDAD EDUCATIVA Y EL APOYO A LA LABOR DOCENTE, ASÍ COMO PARA COMBATIR EL ABANDONO ESCOLAR.  

En los últimos años, las condiciones de trabajo del personal docente en la Comunidad
de Madrid han variado significativamente, debido a las dificultades para dar clase en el
mundo actual (problemas de atención, disciplina, estímulos contrarios a la educación) y
también por el incremento de la carga de trabajo con la puesta en marcha de nuevos
proyectos educativos que se han implantado en los últimos años.  
Para atender a los alumnos como necesitan, y en aras de la calidad educativa, y apoyar
a las familias y a los profesores en su labor docente, la Comunidad de Madrid pone en
marcha diversas medidas, como la bajada de ratios. Así mismo, se procede a redistribuir
el horario del personal docente en los centros públicos no universitarios de la
Comunidad de Madrid, que permita el desempeño y el reconocimiento profesional
llevado a cabo, y la carga de trabajo que cada docente ha asumido.  
Teniendo además en cuenta que las horas no lectivas son complementarias, y que en
ellas los profesores realizan una labor no menos necesaria, como se regula en la Orden
de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la
organización y funcionamiento de los institutos de educación secundaria.  
Tras diferentes reuniones de negociación con las organizaciones sindicales presentes
en la Mesa Sectorial de Personal Docente no Universitario (CC. OO., ANPE, CSIF y
FeSP UGT Madrid), conforme a lo dispuesto en el capítulo IV del título 111 del Texto
Refundido de la Ley del Estatuto Básico del Empleado Público, aprobado por Real
Decreto Legislativo 5/2015, de 30 de octubre, se ha alcanzado el siguiente  
ACUERDO  
Primero  
Objeto  
El presente Acuerdo tiene por objeto establecer la distribución del horario semanal del
personal docente que imparte enseñanza en los centros públicos no universitarios de la
Comunidad de Madrid, que asciende a 37 horas y 30 minutos, con la finalidad de reducir
la carga lectiva, lo que redundará en la mejora de la calidad educativa y de las
condiciones laborales del personal docente.  
Segundo  
Distribución del horario del personal docente que imparte enseñanzas en los centros
públicos de Educación Secundaria, Formación Profesional y Régimen Especial.  
1. El horario semanal del personal docente que imparte enseñanzas en los centros
públicos de Educación Secundaria, Formación Profesional y Régimen Especial será de
37 horas y 30 minutos. De estas 37 horas y 30 minutos, 30 horas serán de obligada
permanencia en el centro. Este horario de obligada permanencia comprenderá una parte
lectiva y otra de carácter complementario.  
En el curso 2025/2026, para los profesores que no tengan la condición de tutor, la parte
lectiva será de 19 periodos semanales y la parte complementaria, será la que reste hasta
llegar a treinta periodos semanales.  
• Con esta medida la parte lectiva para todo el profesorado que imparte
enseñanzas en los centros públicos de Educación Secundaria, Formación
Profesional y Régimen Especial será de 19 periodos semanales, y la parte
complementaria, la que reste hasta llegar a treinta periodos semanales.  
Los tutores, por su parte, tienen, además, una hora dedicada a la convivencia
ya acordada en virtud del ACUERDO de 9 de enero de 2018, del Consejo de
Gobierno, por el que se aprueba expresa y formalmente el Acuerdo de 15 de
diciembre de 2017, de la Mesa General de Negociación del Personal
Funcionario, por el que se ratifica el Acuerdo de 21 de junio de 2017, de la Mesa
Sectorial de Personal Docente no Universitario de la Comunidad de Madrid, para
la mejora de las condiciones de trabajo de los funcionarios docentes (BOCM 17
de enero de 2018).  
En el curso 2027/2028, la parte lectiva para los profesores que tengan la condición de
tutores será de 18 periodos semanales y la parte complementaria, la que reste hasta
llegar a treinta periodos semanales.  
En el curso 2028/2029, la parte lectiva para los profesores que no tengan la condición
de tutores será de 18 periodos semanales y la parte complementaria, la que reste hasta
llegar a treinta periodos semanales.  
El progresivo incremento del horario complementario permitirá que el personal docente
atienda con mayor dedicación horaria las tutorías, los diferentes programas educativos
de la Comunidad de Madrid, así como al centro y a la comunidad educativa.  
2. El tiempo restante, hasta completar las 30 horas de obligada permanencia, le serán
computadas a cada profesor como horario no fijo o irregular. Las 7 horas y 30 minutos
que no son de obligada permanencia en el centro se dedicarán a los deberes inherentes
a la función docente.  
Tercero  
Reducción de ratios de alumnos en las diferentes etapas de Enseñanza no universitaria  
En los sucesivos cursos se continuarácon la reducción de las ratios de alumnos por
clase, ya iniciada en Educación Infantil, concluyendo en esta etapa escolar en el
presente curso 2024/2025 y llevándose a cabo en el segundo curso de la enseñanza
secundaria obligatoria. En el curso 2025/2026 se reducirán las ratios del primer curso
de educación primariay tercer curso de educación secundaria obligatoira. Y, en el curso
2026/2027, se extenderá esta medida al segundo curso de educación primaria y cuarto
curso de educación secundaria obligatoria. Los siguientes cursos se continuará con la
reducción de ratios en los restantes cursos de educación primaria.  
Cuarto  
Seguimiento del Acuerdo  
Las medidas de seguimiento de este Acuerdo se acordarán acordadas por las partes
firmantes del mismo en el seno de la Mesa Sectorial.  
En Madrid a  

## Comentando lo que dice la propuesta abril 2024

Es una versión inicial, intento enlazar ideas y cuando puedo datos o planteamiento de cómo mirar datos

> Una histórica reducción de ratios en primaria y secundaria, con un gran esfuerzo presupuestario en contratación de casi 2.700 docentes y 130M€ anuales, así como de construcción y adaptación de infraestructuras educativas. La reducción de ratios era una reivindicación constante y el Gobierno de Díaz Ayuso ha sido el primero en llevarla a cabo.  

En mi opinión la reducción de ratios se hace de forma paulatina, año a año, para que todos los años la puedan publicitar. Realmente se hace porque baja la natalidad y los centros privados con concierto lo necesitan. Ver [Posible bajada de ratios](http://algoquedaquedecir.blogspot.com/2018/07/posible-bajada-de-ratios.html)  

La frase _"contratación de 2700 docentes y 130M€ anuales"_ no dice nada, enlaza con [Docentes: su número y variación](https://algoquedaquedecir.blogspot.com/2020/09/docentes-su-numero-y-su-variacion.html) y cómo se vio la mentira de las cifras dadas por la administración en el inicio curso 2020-2021  
La frase es deliberadamente poco concreta  
* ¿Contratación de 2700 docentes en qué intervalo de tiempo?  
* Contratación de 2700 docentes no implica aumento de 2700 docentes: puede incluso suponer reducción si con la interinidad se reducen cupos.
* "130M€ anuales" en qué conceptos: ¿contratación? (si no es incremento no dice nada), ¿construcción y adaptación de infraestructuras? (sin concretar no se sabe si es mantenimiento?

> * Estabilización de la plantilla de docentes mediante convocatoria anual de concurso-oposición con un elevado número de plazas. Desde 2019 han sido más de 13.000 y se han ofertado 1.300 plazas para el curso que viene.  

Vuelvo a citar [Docentes: su número y variación](https://algoquedaquedecir.blogspot.com/2020/09/docentes-su-numero-y-su-variacion.html) y cómo se vio la mentira de las cifras dadas por la administración en el inicio curso 2020-2021

> * Incrementos retributivos. Desde el curso 2022-2023, se ha aplicado mejora salarial a más de 86.000 docentes: 1.050 € anuales al cuerpo de maestros / 1.260 € al cuerpo de profesores con una inversión de 100 M€ anuales.  

Habría que ver qué parte de ese incremento es el incremento al que la administración se ha visto obligada por normativa estatal, y hasta qué punto es incremento retributivo supone o no mejora o pérdida de poder adquisitivo.

**2022**  
[Hacienda acuerda subir el sueldo de los funcionarios hasta un 3,5% en 2023](https://www.lamoncloa.gob.es/serviciosdeprensa/notasprensa/hacienda/Paginas/2022/031022-funcionarios.aspx)  

[Díaz Ayuso adelanta la subida salarial a los docentes madrileños que cobrarán en la nómina de noviembre con efectos retroactivos desde el inicio del curso](https://www.comunidad.madrid/noticias/2022/11/11/diaz-ayuso-adelanta-subida-salarial-docentes-madrilenos-cobraran-nomina-noviembre-efectos-retroactivos-inicio-curso)  

**2024**  
[Incremento adicional del 0,5% en las retribuciones de los funcionarios docentes](https://anpe.es/notices/29071/Incremento-adicional-del-0,5-en-las-retribuciones-de-los-funcionarios-docentes)  
> El jueves 8 de febrero de 2024 se publicó en el BOE el Acuerdo del Consejo de Ministros por el que se aprueba el incremento del 0;5% en las retribuciones de los funcionarios, previsto en el artículo 19. Dos. 2. b) de la Ley 31/2022, de 23 de diciembre, de Prepuestos Generales del Estado para el año 2023 (LPGE para 2023).  


> * Incremento progresivo del número de docentes en la educación pública hasta alcanzar la cifra record en la actualidad.   

La clave es el número de docentes real en plantilla  
En [Personal docente en centros públicos no universitarios de la Comunidad de Madrid](https://www.comunidad.madrid/servicios/educacion/personal-docente-centros-publicos-no-universitarios-comunidad-madrid)   

En cursos 23-24 y 22-23 se desglosa funcional y orgánica, y se indica

> La plantilla funcional (cupo docente) es la suma de la plantilla orgánica y las necesidades que requiere cada centro, en función del alumnado y de los requerimientos académicos en cada curso por lo que esta plantilla funcional está formada por plazas estables (plantilla orgánica) y vacantes eventuales.


23-24 orgánica, funcional: 37469, 50734  
22-23 orgánica, funcional: 37239, 48837  

En cursos anteriores a 22-23 la plantilla funcional está como texto en descripción centro, se indica solo "total", al indicar decimales incluye la funcional

21-22 48843,35  
20-21 53108,59 (46310,59 e indica "No incluye el incremento del personal docente  para afrontar la situación del covid-19 de 6,798  en centros públicos")
19-20 45574,98  
18-19 44756,57  

Mirando en https://estadisticas.educa.madrid.org la cifra de total en centros públicos

22-23 55.822  
21-22 55.132  
20-21 58.900   valor mayor asociado a curso tras pandemia  
19-20 52.212
18-19 52.107  

> * Incremento de las productividades con 40,000 docentes beneficiados y una inversión de 41 M€ anuales. Entre ellas, las tutorías, que prácticamente suponen el 25% de esta inversión. Este Gobierno apuesta por el papel del tutor, quien mejor conoce al alumno y las familias.  
 
Las tutorías es una labor que no está bien pagada aunque fuera mucho más.  
¿Inversión de 41 M€ desde cuándo?  
¿Inversión de 41 M€ asocida solo al incremento o es solo de las productividades?

> * Mejoras en la formación docente, con el Plan de Capacitación docente, incluyendo la retribución de los tutores de prácticas, y todas las actividades anuales.  

No conozco los datos, así que los pido el 21 abril 2024

>Solicito copia o enlace a documentación sobe el Plan de Capacitación Integral Docente iniciado en curso 2022/2023 según https://www.comunidad.madrid/noticias/2022/03/31/comunidad-madrid-estrenara-mir-educativo-proximo-curso-escolar-aumentando-formacion-docentes:
- Número de docentes que han realizado la formación para ser tutores de prácticas, desglosado por cuerpo y curso.  
- Coste presupuestado, desglosando la retribución de los tutores en prácticas.  
- Coste ejecutado, desglosando la retribución de los tutores de prácticas.  

03/508607.9/24    

> * Aprobación de un Acuerdo sobre teletrabajo.  

¿Qué coste ha supuesto eso?  
¿Cuántos docentes hay que estén teletrabajando?  

**24 septiembre 2021**  
[Acuerdo por el que se regula el teletrabajo en el ámbito docente](https://anpemadrid.es/notices/160612/Acuerdo-por-el-que-se-regula--el-teletrabajo-en-el-%C3%A1mbito-docente)  

**25 septiembre 2021**  
[Firmamos un acuerdo para regular el teletrabajo del personal docente](https://www.comunidad.madrid/noticias/2021/09/25/firmamos-acuerdo-regular-teletrabajo-personal-docente)  


Pongo solicitud 22 abril 2024  

Solicito copia o enlace a la siguiente documentación asociada al acuerdo de teletrabajo de personal docente firmado en septiembre 2021 https://www.comunidad.madrid/noticias/2021/09/25/firmamos-acuerdo-regular-teletrabajo-personal-docente  
- Número de personal docente, desglosado por cuerpo, que ha accedido a la modalidad de teletrabajo de manera voluntaria según artículo 4.5  
- Número de personal docente, desglosado por cuerpo, que ha abandonado la modalidad de teletrabajo según artículo 4.5  
- Coste asociado a la implementación del acuerdo, como puede ser el de los ordenadores asociados a artículo 7.5.a  
 
03/514105.9/24 


> * Aprobación de un Acuerdo sobre personal interino.  

¿Qué coste ha supuesto eso?  


> * Aumento de la plantilla de personal administrativo para rebajar las trabas burocráticas, con la contratación de más de 820 profesionales a lo largo de esta legislatura  

De nuevo "contrataciones" ¿adicionales o reposición?  
Al decir "a lo largo de esta legislatura" ¿desde cuándo y hasta cuándo? ¿incluye los futuros?  

> Debido a la enorme inversión llevada a cabo durante este tiempo, unido a la complicada situación económica nacional e internacional, con unas previsiones de crecimiento económico muy preocupantes y la ausencia de Presupuestos Generales del Estado, así como de entregas a cuenta las Comunidades Autónomas, a fecha actual, por parte del Gobierno Central, esta reducción deberá hacerse progresivamente.  

"la complicada situación económica nacional e internacional, con unas previsiones de crecimiento económico muy preocupantes"

[La economía de Comunidad de Madrid crece un +2,1 interanual y sigue subiendo por encima de la media de España](https://www.comunidad.madrid/noticias/2023/12/11/economia-comunidad-madrid-crece-21-interanual-sigue-subiendo-encima-media-espana)  

[Situación económica de la Comunidad de Madrid. Subdirección General de Análisis Económico. Dirección General de Economía. I/2023](https://www.comunidad.madrid/sites/default/files/doc/economia/situacion_economica_cm_i_2023.pdf)  
> I. Síntesis de la situación económica  
La evolución de la actividad económica en el primer trimestre de 2023 ha resultado ser notablemente más boyante
de lo que las previsiones apuntaban.

[La Cámara de España eleva sus previsiones de crecimiento para la economía española en 2024](https://www.camara.es/camara-espana-eleva-previsiones-crecimiento-economia-espanola-2024)  

"la ausencia de Presupuestos Generales del Estado, así como de entregas a cuenta las Comunidades Autónomas, a fecha actual, por parte del Gobierno Central"

¿El resto de CCAA que sí han bajado el horario lectivo no están en el mismo estado?  

> Empezando con una reducción de 1 hora lectiva a los docentes no tutores de enseñanzas secundaria, formación profesional y de régimen especial durante el curso 2026/2027.  

Aparte de retrassarlo hasta 2026/2027, está la idea clave de **docentes no tutores**: 

- Aplica a muy pocos docentes: casi todos los docentes son tutores o jefes de departamento (alguna vez ambas cosas)  

Se pueden echar unas cuentas rápidas. El número de grupos real no es público, pero se puede estimar de manera conservadora. Por ejemplo hay grupos de diversificación que son más pequeños.

[La educación en cifras](https://www.comunidad.madrid/servicios/educacion/educacion-cifras)  
[datos y cifras de la educación 2023-2024](https://gestiona3.madrid.org/bvirtual/BVCM051108.pdf)  

Tabla 1. Alumnos matriculados en Enseñanzas de Régimen General por enseñanzas, titularidad y régimen de financiación
del centro  
2022-2023  
C públicos ESO 160.105 
C públicos Bachillerato 60.308 

160105/30=5337 grupos mínimo
60308/35=1723 grupos mínimo

Eso implica mínimo 5337+1723=7060 tutores

Tabla 13. Profesores que imparten enseñanzas en centros de Régimen General por cuerpo-categoría, titularidad del centro
y sexo. Curso 2022-2023  
Catedráticos y Profesores de E. Secundaria 19.981 

Eso supone que los tutores son como mínimo 7060/19981=35%, lo veo más cercano al 50%.

- Desprecia la carga de trabajo de los tutores al no considerarlos merecedores de esa reducción.  
Los tutores tienen "una reducción" de lectivas de clase directa  
[INSTRUCCIONES DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA, SOBRE COMIENZO DEL CURSO ESCOLAR 2023-2024 EN CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID.](https://www.comunidad.madrid/transparencia/sites/default/files/regulation/documents/resolucion_conjunta_instrucciones_inicio_de_curso_23-24.pdf)  
>6.1.4.3. Horario lectivo  
El horario lectivo semanal del profesorado será distribuido de lunes a viernes con un mínimo de dos
periodos lectivos diarios.  
Se considerarán periodos lectivos:  
>7. La tutoría a un grupo completo de alumnos.  
>11. Los tutores de ESO contabilizarán un segundo periodo lectivo de su horario personal para el
desarrollo de actuaciones recogidas en el Plan de Convivencia del centro a que hace
referencia el Decreto 32/2019, de 9 de abril, por el que se establece el marco regulador de la
convivencia en los centros docentes de la Comunidad de Madrid. Asimismo, los tutores de
los cursos primero y segundo de Bachillerato contabilizarán un periodo lectivo de su horario
personal para el desarrollo de actuaciones recogidas en dicho Plan de Convivencia del
centro.

En cierto modo están insinuando que esas horas lectivas asociadas a tutoría no son lectivas, "ya les han reducido y no están a 20" y que por eso no merecen una reducción.

## Comentando lo que dice la propuesta octubre 2024

En el título del pdf está en negrita "PARA MEJORAR LA ATENCIÓN A LOS ALUMNOS, LA CALIDAD EDUCATIVA Y EL APOYO A LA LABOR DOCENTE, ASÍ COMO PARA COMBATIR EL ABANDONO ESCOLAR." pero en foto original está todo el título.  

El primer párrafo reconoce que es más complicado dar clase, y que hay más carga de trabajo por "la puesta en marcha de nuevos proyectos educativos que se han implantado en los últimos años". Ahí se puede citar el plan incluyo y sus anexos.  

El segundo párrafo reconoce que hay que tomar medidas para mejorar la atención al alumnado, y lo concreta en horario lectivo y ratios.   

El tercer párrafo remite a la orden de 1994 para aclarar que aunque se reduzcan lectivas se siguen teniendo las mismas complementarias

El cuarto párrafo da por aceptado el acuerdo "tras diferentes reuniones": ellos imponen y asumen se acepta. 

El artículo primero de objeto no dice nada, salvo quizá la mentira de 37 h 30 min, cuando no se cumple.  

En artículo segundo se habla de secundaria y FP y dice "De estas 37 horas y 30 minutos, 30 horas serán de obligada permanencia en el centro". Esa frase mezcla deliberadamente horas y periodos, y al haber usado antes horas como 60 minutos (asociado a 37 horas), se puede entender que luego habla de 30 horas también de 60 minutos, y no es así.   
Más adelante sí que usa 30 periodos " ... la que reste hasta llegar a treinta periodos semanales."

Es la misma frase que tienen las instrucciones en 6.1.3.1. Distribución del horario  
[INSTRUCCIONES DE LA VICECONSEJERÍA DE POLÍTICA Y ORGANIZACIÓN EDUCATIVA, SOBRE COMIENZO DEL CURSO ESCOLAR 2024-2025 EN CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID](https://www.comunidad.madrid/sites/default/files/instrucciones_inicio_curso_2024_2025.pdf)  

Se indica  
> "En el curso 2025/2026, para los profesores que no tengan la condición de tutor, la parte lectiva será de 19 periodos semanales ..."  

Están vendiendo una mejora solo para los no tutores, ya explicado que son muy pocos docentes. 

Pero es que luego dicen  
>Con esta medida la parte lectiva para **todo** el profesorado que imparte
enseñanzas en los centros públicos de Educación Secundaria, Formación
Profesional y Régimen Especial será de 19 periodos semanales  

No es cierto, porque los tutores tienen 20 lectivas, ya que según instrucciones 2024-2025 6.1.3.3 Horario lectivo  
- La tutoría es lectiva "7 La tutoría a un grupo completo de alumnos."  
- Los tutores tienen una lectiva asociada a convivencia "11. Los tutores de ESO contabilizarán un segundo periodo lectivo de su horario personal para el desarrollo de actuaciones recogidas en el Plan de Convivencia del centro a que hace
referencia el Decreto 32/2019, de 9 de abril..."  

En lugar de referenciar ese decreto, citan el acuerdo de 2018. Ver post [Recordar antes de acordar: acuerdo sectorial educación Madrid 2017-2021](http://algoquedaquedecir.blogspot.com/2017/09/recordar-antes-de-acordar-acuerdo.html)  

Esas dos sesiones son lectivas para los tutores, están rebajando solo a los no tutores y por tanto están echando cuentas quitándole la consideración de lectivas a lo que sí lo es.  

No pueden pretender redefinir "lectivas" solo como "docencia directa"  

Bajar lectivas solo a no tutores desprecia la carga de trabajo de los tutores al no considerarlos "merecedores" de esa reducción. En cierto modo están insinuando que las horas lectivas asociadas a tutoría no son lectivas, "ya les han reducido y no están a 20" y que por eso no merecen una reducción  

Hay que tener cuidado porque la definición de qué es una hora lectiva no es normativa básica, así que si por ejemplo las dos horas de tutoría las convirtiesen en no lectivas, sin hacer nada podrían decir que todos los tutores están ya a 18  

Los párrafos siguientes confirman esto: reducen primero a tutores a 18 (porque consideran ya reducida a 19) y luego a no tutores a 18.    
> En el curso 2027/2028, la parte lectiva para los profesores que tengan la condición de
tutores será de 18 periodos semanale.sy la parte complementaria, la que reste hasta
llegar a treinta periodos semanales.  
En el curso 2028/2029, la parte lectiva para los profesores que no tengan la condición
de tutores será de 18 periodos semanales y la parte complementaria, la que reste hasta
llegar a treinta periodos semanales  
El progresivo incremento del horario complementario permitirá que el personal docente
atienda con mayor dedicación horaria las tutorías, los diferentes programas educativos
de la Comunidad de Madrid, así como al centro y a la comunidad educativa.  

## Comentando lo que dice la propuesta diciembre 2024

El documento en pdf [lo publica ANPE desde un correo](https://mailsystem.es/campaign-content/49894/eyJpdiI6Imlvd0t4dk1weThyQkNYZU15bmNmUnc9PSIsInZhbHVlIjoid0FsKzZYQWJHN2hzWjZlRmtRd3lXdENlamdQcEZ2RVJhTzRtdlZ2SFd1WjB6SkFrd1ViUVp0VHEzUHBlL0I3Q2FBSmo4QXZoQXBTM2pNa0lMWWFEK1E9PSIsIm1hYyI6ImMwNTBhMDFmN2Q3MjVmMDM3ZTQ2ZTYwZmJjMWFiZDdhMzlkNjUwYzIyYjhkYWIwODYwN2JjNDU5YWQzNDlhNDMiLCJ0YWciOiIifQ==)
 
[Propuesta Acuerdo reducción horas lectivas 2.pdf](https://documentos.anpemadrid.es/docs/Propuesta%20Acuerdo%20reduccion%20horas%20lectivas%202.pdf)  

El documento en metadatos indica fecha 19 diciembre y autor "DE LA FLOR MARTIN, M. TERESA"

https://www.comunidad.madrid/transparencia/persona/ma-teresa-flor-martin

Subdirectora General de Gestión de Profesorado de Educación Infantil, Primaria y Especial

Lo pongo aquí como texto  

> ACUERDO DE LA MESA SECTORIAL DE PERSONAL DOCENTE NO UNIVERSITARIO POR EL QUE SE REGULA LA REDISTRIBUCIÓN DEL HORARIO DEL PERSONAL DOCENTE EN LOS CENTROS PUBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID, EN EL MARCO DE OTRAS MEDIDAS PARA MEJORAR LA ATENCIÓN A LOS ALUMNOS, LA CALIDAD EDUCATIVA Y EL APOYO A LA LABOR DOCENTE, ASÍ COMO PARA COMBATIR EL ABANDONO ESCOLAR.  
En los últimos años, las condiciones de trabajo del personal docente en la Comunidad
de Madrid han variado significativamente, debido a las dificultades para dar clase en el
mundo actual (problemas de atención, disciplina, estímulos contrarios a la educación) y
también por el incremento de la carga de trabajo con la puesta en marcha de nuevos
proyectos educativos que se han implantado en los últimos años.  
Para atender a los alumnos como necesitan, y en aras de la calidad educativa, y apoyar
a las familias y a los profesores en su labor docente, la Comunidad de Madrid pone en
marcha diversas medidas, como la bajada de ratios. Así mismo, se procede a redistribuir
el horario del personal docente en los centros públicos no universitarios de la
Comunidad de Madrid, que permita el desempeño y el reconocimiento profesional
llevado a cabo, y la carga de trabajo que cada docente ha asumido.  
Teniendo además en cuenta que las horas no lectivas son complementarias, y que en
ellas los profesores realizan una labor no menos necesaria, como se regula en la Orden
de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la
organización y funcionamiento de los institutos de educación secundaria.  
Tras diferentes reuniones de negociación con las organizaciones sindicales presentes
en la Mesa Sectorial de Personal Docente no Universitario (CC. OO., ANPE, CSIF y
FeSP UGT Madrid), conforme a lo dispuesto en el capítulo IV del título III del Texto
Refundido de la Ley del Estatuto Básico del Empleado Público, aprobado por Real
Decreto Legislativo 5/2015, de 30 de octubre, se ha alcanzado el siguiente  
ACUERDO  
Primero  
Objeto  
El presente Acuerdo tiene por objeto establecer la distribución del horario semanal del
personal docente que imparte enseñanza en los centros públicos no universitarios de la
Comunidad de Madrid, que asciende a 37 horas y 30 minutos, con la finalidad de reducir
la carga lectiva, lo que redundará en la mejora de la calidad educativa y de las
condiciones laborales del personal docente.  
Segundo  
Distribución del horario del personal docente que imparte enseñanzas en los centros
públicos de Educación Secundaria, Formación Profesional y Régimen Especial.
>1. El horario semanal del personal docente que imparte enseñanzas en los centros
públicos de Educación Secundaria, Formación Profesional y Régimen Especial será de
37 horas y 30 minutos. De estas 37 horas y 30 minutos, 30 horas serán de obligada
permanencia en el centro. Este horario de obligada permanencia comprenderá una parte
lectiva y otra de carácter complementario.  
En el curso 2025/2026, para los profesores que no tengan la condición de tutor, la parte
lectiva será de 19 periodos semanales y la parte complementaria, será la que reste hasta llegar a treinta periodos semanales.  
Con esta medida la parte lectiva para todo el profesorado que imparte enseñanzas en los centros públicos de Educación Secundaria, Formación Profesional y Régimen Especial será de 19 periodos semanales, y la parte complementaria, la que reste hasta llegar a treinta periodos semanales.  
Los tutores, por su parte, tienen, además, una hora dedicada a la convivencia ya acordada en virtud del ACUERDO de 9 de enero de 2018, del Consejo de Gobierno, por el que se aprueba expresa y formalmente el Acuerdo de 15 de diciembre de 2017,
de la Mesa General de Negociación del Personal Funcionario, por el que se ratifica el Acuerdo de 21 de junio de 2017, de la Mesa Sectorial de Personal Docente no Universitario de la Comunidad de Madrid, para la mejora de las condiciones de trabajo
de los funcionarios docentes (BOCM 17 de enero de 2018).  
En el curso 2027/2028, la parte lectiva para los profesores que tengan la condición de tutores será de 18 periodos semanales. y la parte complementaria, la que reste hasta llegar a treinta periodos semanales.  
En el curso 2028/2029, la parte lectiva para los profesores que no tengan la condición de tutores será de 18 periodos semanales y la parte complementaria, la que reste hasta llegar a treinta periodos semanales.  
El progresivo incremento del horario complementario permitirá que el personal docente atienda con mayor dedicación horaria las tutorías, los diferentes programas educativos de la Comunidad de Madrid, así como al centro y a la comunidad educativa.
>2. El tiempo restante, hasta completar las 30 horas de obligada permanencia, le serán computadas a cada profesor como horario no fijo o irregular. Las 7 horas y 30 minutos que no son de obligada permanencia en el centro se dedicarán a los deberes inherentes a la función docente.  
Tercero  
Distribución del horario de maestros de Educación Infantil y de Educación Primaria.  
El horario semanal de los maestros de Educación Infantil y de Educación Primaria será
de 37 horas y 30 minutos. Los maestros permanecerán en el colegio treinta horas
semanales, de las cuales 25 serán lectivas y 5 complementarias. Estas horas tendrán
la consideración de obligada permanencia en el centro.  
A partir del curso 2024/2025, de las 5 horas complementarias una de ellas dejará de ser
de obligada permanencia en el centro y pasará, como las restantes, hasta las treinta y
siete horas y media semanales, a ser de libre disposición para la preparación de las
actividades docentes o cualquier otra actividad pedagógica complementaria.  
Cuarto  
Complemento de productividad por el desempeño de tutorías con alumnos a todos los cursos en
los ciclos formativos de Formación Profesional  
El complemento de productividad por tutorías se aplicará a todos los cursos en los ciclos
formativos de Formación Profesional.  
La cuantía será la establecida por la Orden de la Consejería de Economía, Hacienda y
Empleo para la asignación de productividad a funcionarios docentes no universitarios.  
Quinto  
Reducción de ratios de alumnos en las diferentes etapas de Enseñanza no universitaria  
En los sucesivos cursos se continuará con la reducción de las ratios de alumnos por
clase, ya iniciada en Educación Infantil, concluyendo en esta etapa escolar en el
presente curso 2024/2025 y llevándose a cabo en el segundo curso de la enseñanza
secundaria obligatoria. En el curso 2025/2026 se reducirán las ratios del primer curso
de educación primaria y tercer curso de educación secundaria obligatoria. Y, en el curso
2026/2027, se extenderá esta medida al segundo curso de educación primaria y cuarto
curso de educación secundaria obligatoria. Los siguientes cursos se continuará con la
reducción de ratios en los restantes cursos de educación primaria.  
Sexto  
Reducción de la burocracia  
La disminución de la burocracia en los centros es uno de los factores que, sin duda,
puede ayudar a mejorar el entorno educativo y de trabajo de los docentes. Por ello, en
el seno de la mesa sectorial se constituirá un grupo de trabajo cuyos resultados serán
tenidos en cuenta a tal fin.  
Séptimo  
Las partes firmantes de este acuerdo se comprometen a proseguir con las
negociaciones en curso, tales como licencias y permisos, oferta de empleo público y
actualización de centros de difícil desempeño, con el objetivo de mejorar las condiciones
laborales de los funcionarios docentes no universitarios de la Comunidad de Madrid, así
como la calidad de la enseñanza impartida.  
Octavo  
Seguimiento del Acuerdo  
Las medidas de seguimiento de este Acuerdo se acordarán por las partes firmantes del
mismo en el seno de la Mesa Sectorial.  
En Madrid a   

El texto inicial y puntos primero y segundo son totalmente idénticos: no hay ningún cambio respecto a horas lectivas y sigue la tomadura de pelo anterior.

La nueva propuesta incluye nuevos puntos Tercero y Cuarto pasando el anterior tercero a ser Quinto.  
Aparece un nuevo punto Sexto pasando el anterior Cuarto a ser el Séptimo.  

> Tercero  
Distribución del horario de maestros de Educación Infantil y de Educación Primaria.  
El horario semanal de los maestros de Educación Infantil y de Educación Primaria será
de 37 horas y 30 minutos. Los maestros permanecerán en el colegio treinta horas
semanales, de las cuales 25 serán lectivas y 5 complementarias. Estas horas tendrán
la consideración de obligada permanencia en el centro.
A partir del curso 2024/2025, de las 5 horas complementarias una de ellas dejará de ser
de obligada permanencia en el centro y pasará, como las restantes, hasta las treinta y
siete horas y media semanales, a ser de libre disposición para la preparación de las
actividades docentes o cualquier otra actividad pedagógica complementaria.  

Venden algo en EI y EP cuando lo único que hacen es que una complementaria en 2024/2025 (con curso ya iniciado, muestra de que no cambia nada) deja de ser de obligada permanencia.

Cuarto  
Complemento de productividad por el desempeño de tutorías con alumnos a todos los cursos en
los ciclos formativos de Formación Profesional  
El complemento de productividad por tutorías se aplicará a todos los cursos en los ciclos
formativos de Formación Profesional.  
La cuantía será la establecida por la Orden de la Consejería de Economía, Hacienda y
Empleo para la asignación de productividad a funcionarios docentes no universitarios.  

Venden que darán complemento productividad por el desempeño de tutorías en FP, sin concretar nada, ni la cuantía.

Parece que es algo que ya se estaba haciendo

**17 septiembre 2024**  
[FP: Cobro Tutoría 2º FP - feccoo-madrid.org](https://feccoo-madrid.org/noticia:703872--FP_Cobro_Tutoria_2_FP&opc_id=edd9274f6a8110693a678e6756ceb910)

> Sexto  
Reducción de la burocracia  
La disminución de la burocracia en los centros es uno de los factores que, sin duda,
puede ayudar a mejorar el entorno educativo y de trabajo de los docentes. Por ello, en
el seno de la mesa sectorial se constituirá un grupo de trabajo cuyos resultados serán
tenidos en cuenta a tal fin.  

Venden un grupo de trabajo cuyos resultados serán tenidos en cuenta: es humo de la nada. 
Es un tanto en su contra que citen disminuir la burocracia de los docentes, porque están reconociendo que como gestores han puesto una burocracia actual que es excesiva.  

**23 diciembre 2024**  
[Emilio Viciana, consejero de Educación: "Hay acuerdo entre CCAA del PP para contenidos comunes en selectividad - elespanol](https://www.elespanol.com/madrid/comunidad/20241223/emilio-viciana-consejero-educacion-acuerdo-ccaa-pp-contenidos-comunes-selectividad/909659157_0.html)  
> P: Hemos empezado con política y terminaré con política. En cuanto a la relación con los sindicatos, ¿cómo están las negociaciones para la reducción de jornada?  
R: La relación con los sindicatos es buena; me he reunido con ellos en varias ocasiones y también hay reuniones técnicas frecuentes. Estamos avanzando y somos optimistas. Aunque los sindicatos han continuado con sus movilizaciones, estoy convencido de que llegaremos a un buen acuerdo.  
P: ¿Incluyendo la reducción de la jornada lectiva?  
R: Creo que sí. Hemos presentado una oferta buena, aunque quedan detalles por pulir. Estoy convencido de que alcanzaremos un consenso.  

**3 febrero 2025**  

[ COMUNICADO CONJUNTO DE LAS ORGANIZACIONES SINDICALES DE LA MESA SECTORIAL DE EDUCACIÓN](https://ugtspmadrid.es/comunicado-conjunto-de-las-organizaciones-sindicales-de-la-mesa-sectorial-de-educacion/)  

Los sindicatos de la Mesa Sectorial de Educación —CCOO, ANPE, CSIF y UGT—, tras la reunión celebrada hoy, queremos manifestar lo siguiente:

Hemos trasladado al director de Recursos Humanos, D. Miguel Zurita, nuestro rechazo unánime al borrador de acuerdo presentado el pasado 19 de diciembre. Tras las consultas realizadas al profesorado, consideramos que dicho acuerdo no introduce mejoras significativas para el cuerpo de Maestros y que la recuperación de las 18 horas lectivas para el resto de cuerpos se dilata en exceso.

Los cuatro sindicatos hemos insistido en la necesidad de reducir las horas lectivas del cuerpo de Maestros así como de dotar a los centros de Educación Infantil y Primaria de mayores recursos mediante un incremento de cupos para apoyos y atención a la diversidad.

Asimismo, ante el nuevo escenario que se abre a nivel estatal con la posible negociación de un Estatuto Docente que contemple mejoras laborales para el profesorado, entendemos que algunas de estas mejoras podrían ser implementadas directamente por el Ministerio.

Para avanzar en las negociaciones, hemos propuesto que se aborden los distintos apartados de la propuesta del acuerdo de manera independiente, comenzando por el acuerdo de permisos y licencias cuyo contenido, con algunas aportaciones muy concretas, es avalado por la inmensa mayoría del profesorado.

Consideramos que alcanzar acuerdos más específicos y concretos demostraría la buena voluntad de la Consejería y supondría un impacto positivo en las condiciones laborales del profesorado madrileño.

El director de Recursos Humanos nos ha manifestado de que trasmitirá nuestra posición y reivindicaciones al consejero de Educación y nos ha emplazado a una nueva reunión a mediados de febrero para abordar esta cuestión.

En los próximos días, seremos convocados para continuar negociando la oferta de empleo público y las listas de inspectores accidentales.

**19 febrero 2025**  

[Convocatoria mesa sectorial](https://feccoo-madrid.org/54b8dd948fc106f3db2af1300d0e03f5000063.pdf)  

La Consejería de Educación, Ciencia y Universidades convoca a la Mesa Sectorial de
Educación para el martes, 25 de febrero de 2025, a las 12:00 horas, mediante
videoconferencia, con el siguiente orden del día:  
1. Continuación de la negociación de la reducción de la jornada lectiva  
2. Procesos selectivos personal docente no universitario.  
3. Oferta de Empleo Público 2025 de personal docente no universitario.  
4. Ruegos y preguntas  


## Comentando lo que dice la propuesta marzo 2025

**6 marzo 2025**

Nueva propuesta de acuerdo

[Propuesta de Acuerdo de la Mesa Sectorial - anpemadrid](https://anpemadrid.es/notices/193068/Propuesta-de-Acuerdo-de-la-Mesa-Sectorial)
[Documento de propuesta pdf en web anpe](https://documentos.anpemadrid.es/docs/pdf/Propuesta%20Acuerdo%20reduccion%20horas%20lectivas.pdf) 

[Documento de propuesta pdf en web ccoo](https://feccoo-madrid.org/15d1cf6853c1ce51666dcf95145663f1000063.pdf)  


Lo pongo aquí como texto

> ACUERDO DE LA MESA SECTORIAL DE PERSONAL DOCENTE NO UNIVERSITARIO POR EL QUE SE REGULA LA REDISTRIBUCIÓN DEL HORARIO DEL PERSONAL DOCENTE EN LOS CENTROS PUBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID, EN EL MARCO DE OTRAS MEDIDAS PARA MEJORAR LA ATENCIÓN A LOS ALUMNOS, LA CALIDAD EDUCATIVA Y EL APOYO A LA LABOR DOCENTE, ASÍ COMO PARA COMBATIR EL ABANDONO ESCOLAR.  
En los últimos años, las condiciones de trabajo del personal docente en la Comunidad de Madrid han variado significativamente, debido a las dificultades que afrontan en su labor docente particularmente por estímulos contrarios a la educación y también por el incremento de la carga de trabajo con la puesta en marcha de nuevos proyectos educativos que se ido implantado.  
Al objeto de dar una mejor y más adecuada respuesta a las necesidades de los alumnos, obteniendo con ello una mejora de la calidad educativa, y apoyar a las familias y a los profesores en su labor docente, la Comunidad de Madrid pone en marcha diversas medidas, como la bajada de ratios. Así mismo, se procede a redistribuir el horario del personal docente en los centros públicos no universitarios de la Comunidad de Madrid, que permita mejorar el desempeño y el reconocimiento profesional llevado a cabo, y la carga de trabajo que cada docente ha asumido.  
Se debe considerar también que las horas no lectivas son complementarias, y que en ellas los profesores realizan una labor no menos necesaria, como se regula en la Orden de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la organización y funcionamiento de los institutos de educación secundaria.  
Tras diferentes reuniones de negociación con las organizaciones sindicales presentes en la Mesa Sectorial de Personal Docente no Universitario (CC.OO., ANPE, CSIF y FeSP UGT Madrid), conforme a lo dispuesto en el capítulo IV del título III del Texto Refundido de la Ley del Estatuto Básico del Empleado Público, aprobado por Real Decreto Legislativo 5/2015, de 30 de octubre, se ha alcanzado el siguiente    
ACUERDO  
Primero  
Objeto  
El presente Acuerdo tiene por objeto establecer la distribución del horario semanal del personal docente que imparte enseñanza en los centros públicos no universitarios de la Comunidad de Madrid, que asciende a 37 horas y 30 minutos, con la finalidad de reducir la carga lectiva, lo que redundará en la mejora de la calidad educativa y de las condiciones laborales del personal docente.  
Segundo  
Distribución del horario del personal docente que imparte enseñanzas en los centros públicos de Educación Secundaria, Formación Profesional y Régimen Especial.
>1. El horario semanal del personal docente que imparte enseñanzas en los centros públicos de Educación Secundaria, Formación Profesional y Régimen Especial será de 37 horas y 30 minutos. De estas 37 horas y 30 minutos, 30 horas serán de obligada permanencia en el centro. Este horario de obligada permanencia comprenderá una parte lectiva y otra de carácter complementario.  
En el curso 2025/2026, para los profesores que no tengan la condición de tutor, la parte lectiva será de 19 periodos semanales y la parte complementaria, será la que reste hasta llegar a treinta periodos semanales.  
Con esta medida la parte lectiva para todo el profesorado que imparte enseñanzas en los centros públicos de Educación Secundaria, Formación Profesional y Régimen Especial será de 19 periodos semanales, y la parte complementaria, la que reste hasta llegar a treinta periodos semanales.  
Los tutores, por su parte, tienen, además, una hora dedicada a la convivencia ya acordada en virtud del ACUERDO de 9 de enero de 2018, del Consejo de Gobierno, por el que se aprueba expresa y formalmente el Acuerdo de 15 de diciembre de 2017, de la Mesa General de Negociación del Personal Funcionario, por el que se ratifica el Acuerdo de 21 de junio de 2017, de la Mesa Sectorial de Personal Docente no Universitario de la Comunidad de Madrid, para la mejora de las condiciones de trabajo de los funcionarios docentes (BOCM 17 de enero de 2018).  
En el curso 2027/2028, la parte lectiva para los profesores que tengan la condición de tutores será de 18 periodos semanales. y la parte complementaria, la que reste hasta llegar a treinta periodos semanales.  
En el curso 2028/2029, la parte lectiva para los profesores que no tengan la condición de tutores será de 18 periodos semanales y la parte complementaria, la que reste hasta llegar a treinta periodos semanales.  
El progresivo incremento del horario complementario permitirá que el personal docente atienda con mayor dedicación horaria las tutorías, los diferentes programas educativos de la Comunidad de Madrid, así como al centro y a la comunidad educativa.
>2. El tiempo restante, hasta completar las 30 horas de obligada permanencia, le serán computadas a cada profesor como horario no fijo o irregular. Las 7 horas y 30 minutos que no son de obligada permanencia en el centro se dedicarán a los deberes inherentes a la función docente. 
Tercero  
Distribución del horario de maestros de Educación Infantil y de Educación Primaria e incremento de recursos en esta etapa.  
El horario semanal de los maestros de Educación Infantil y de Educación Primaria será de 37 horas y 30 minutos. Los maestros permanecerán en el colegio treinta horas semanales, de las cuales 25 serán lectivas y 5 complementarias. Estas horas tendrán la consideración de obligada permanencia en el centro.  
A partir del curso 2025/2026, de las 5 horas complementarias una de ellas dejará de ser de obligada permanencia en el centro y pasará, como las restantes, hasta las treinta y siete horas y media semanales, a ser de libre disposición para la preparación de las actividades docentes o cualquier otra actividad pedagógica complementaria.  
En esta etapa se producirá también un incremento progresivo de recursos para atender las necesidades del sistema educativo, atención a las necesidades educativas específicas y de atención a la diversidad, para reducción de ratios, para apoyo docente y para atender las necesidades escolarización. Se aumentará un total de 850 cupos que se distribuirán durante los cursos 2005/26, 20026/27 y 2027/28, a razón de 300, 275 y 275 respectivamente, prestando especial atención a aquellos centros cuyas características lo aconsejen, para garantizar una distribución equitativa de los recursos.  
Cuarto  
Complemento de productividad por el desempeño de tutorías con alumnos a todos los cursos en los ciclos formativos de Formación Profesional  
El complemento de productividad por tutorías se aplicará a todos los cursos en los ciclos formativos de Formación Profesional.  
La cuantía será la establecida por la Orden de la Consejería de Economía, Hacienda y Empleo para la asignación de productividad a funcionarios docentes no universitarios.  
Quinto  
Reducción de ratios de alumnos en las diferentes etapas de Enseñanza no
universitaria  
En los sucesivos cursos se continuará con la reducción de las ratios de alumnos por clase, ya iniciada en Educación Infantil, concluyendo en esta etapa escolar en el presente curso 2024/2025 y llevándose a cabo en el segundo curso de la enseñanza secundaria obligatoria. En el curso 2025/2026 se reducirán las ratios del primer curso de educación primaria y tercer curso de educación secundaria obligatoria. En el curso 2026/2027 se extenderá esta medida al segundo curso de educación primaria y cuarto curso de educación secundaria obligatoria. Los siguientes cursos se continuará con la reducción de ratios en los restantes cursos de educación primaria    
Sexto  
Reducción de la burocracia  
La disminución de la burocracia en los centros es uno de los factores que, sin duda, puede ayudar a mejorar el entorno educativo y de trabajo de los docentes. En el seno de la mesa sectorial se negociará un plan de simplificación en el que, tras analizar la problemática, proponer medidas conducentes a su reducción, velando por su puesta en práctica con la mayor inmediatez posible.   
Séptimo  
Las partes firmantes de este acuerdo se comprometen a proseguir con las
negociaciones en curso, tales como licencias y permisos, oferta de empleo público y actualización de centros de difícil desempeño, con el objetivo de mejorar las condiciones laborales de los funcionarios docentes no universitarios de la Comunidad de Madrid, así como la calidad de la enseñanza impartida.  
Octavo  
Seguimiento del Acuerdo  
Las medidas de seguimiento de este Acuerdo se acordarán por las partes firmantes del mismo en el seno de la Mesa Sectorial.  
En Madrid a  

El resumen es:  
- Cambio en punto tercero (solo asociado a infantil y primaria), se añade que se aumentará un total de 850 cupos que se distribuirán durante los cursos 2005/26, 20026/27 y 2027/28, a razón de 300, 275 y 275 respectivamente  
- Cambios de redacción sin relevancia en texto inicial, puntos cuarto, quito y sexto   
- No cambia absolutamente nada en puntos primero, segundo, séptimo y octavo  

CCOO plantea una encuesta, que permite votar a favor o en contra de cada punto (séptimo y octavo no se votan), y acaba diciendo

> VALORACIÓN FINAL  
¿Crees que esta propuesta debe firmarse y seguir manteniendo la presión y movilización hasta conseguir el resto de objetivos propuestos?  

Con 4 opciones  
- Sí, manteniendo las movilizaciones  
- Sí, sin movilizaciones  
- No  
- Me abstengo 


En esa encuesta la última pregunta está redactada reflejando que sí o sí van a firmar ese acuerdo, porque une en la pregunta firmar y seguir la movilización  

Sí que piden DNI (otras veces ha habido polémicas sobre el control en las respuestas a las encuestas), y luego ponen información de protección de datos


> FEDERACION DE ENSEÑANZA DE MADRID DE COMISIONES OBRERAS está especialmente sensibilizada en la protección de datos de carácter personal de los Usuarios del sitio web. Mediante la presente Política de Privacidad (o Política de Protección de Datos) FEDERACION DE ENSEÑANZA DE COMISIONES OBRERAS informa a los usuarios del sitio web: fe.ccoo.es de los usos a los que se someten los datos de carácter personal que se recaban en sitio web, con el fin de que decidan, libre y voluntariamente, si deseas facilitar la información solicitada. FEDERACION DE ENSEÑANZA DE MADRID DE COMISIONES OBRERAS se reserva la facultad de modificar esta política con el objeto de adaptarla a novedades legislativas, criterios jurisprudenciales, prácticas del sector, o intereses de la entidad. Cualquier modificación en la misma será anunciada con la debida antelación, a fin de que tengas perfecto conocimiento de su contenido.1. RESPONSABLE. El responsable del tratamiento de los datos de carácter personal es: FEDERACION DE ENSEÑANZA DE MADRID DE COMISIONES OBRERAS, CIF: G78092525 y domicilio en Calle Lope de Vega,38, 4 planta. Puedes contactar con nosotros a través de http://feccoo-madrid.org/2. FINALIDAD DEL TRATAMIENTO. La finalidad de la recogida y tratamiento de los datos personales, responden, según el caso concreto, para gestionar y atender a solicitudes de Inscripción de los cursos ofertados. Encontrarás información detallada en materia de protección de datos para cada tratamiento en la información ampliada.3. PLAZO DE CONSERVACIÓN. Los datos basados en el consentimiento se mantendrán mientras no revoques el consentimiento o te opongas al tratamiento. Puedes consultar el plazo de conservación de los diferentes tratamientos en la información ampliada.4. LEGITIMACIÓNFEDERACION DE ENSEÑANZA DE MADRID DE COMISIONES OBRERAS está legitimada al tratamiento de datos personales, en base al consentimiento otorgado por el interesado para uno o varios fines específicos, tal y como recoge el artículo 6.1. a) al cumplimentar los formularios y marcar la casilla habilitada para tal efecto. Puedes consultar las bases legitimadoras de los diferentes tratamientos en la información ampliada.5. EXACTITUD DE LOS DATOS. Por otro lado, con el fin de que los datos obrantes en nuestros ficheros, informáticos y/o en papel, siempre correspondan a la realidad, se tratará de mantener actualizados. De manera que, a estos efectos, deberás realizar los cambios, directamente, cuando así esté habilitado o comunicándose, por medio fehaciente, al área o departamento correspondiente de FEDERACION DE ENSEÑANZA DE MADRID DE COMISIONES OBRERAS.6. DESTINATARIOS. Los datos personales no serán cedidos o comunicados a terceros, salvo en los supuestos necesarios para el desarrollo, control y cumplimiento de la/s finalidad/es expresada/s, en los supuestos previstos según Ley. Puedes consultar las comunicaciones de datos de los diferentes tratamientos en la información ampliada. FEDERACION DE ENSEÑANZA DE MADRID DECOMISIONES OBRERAS cuenta con encargados de tratamiento con los que tiene firmados un contrato de acceso a datos conforme al contenido previsto en el artículo 28 del RGPD.7. DERECHOS DE LOS USUARIOS. En todo caso podrás ejercitar los derechos que te asisten, de acuerdo con el Reglamento General de Protección de Datos, y que son: Derecho a solicitar el acceso a los datos personales. Este derecho consiste en obtener información sobre si en FEDERACION DE ENSEÑANZA DE MADRID  DE COMISIONES OBRERAS estamos tratando tus datos o no y en caso afirmativo, el acceso a la información de los datos y plazo de tratamiento, así como los fines y destinatarios. Derecho a solicitar su rectificación en el caso de que los datos sean inexactos o incompletos. Derecho a la supresión de tus datos cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. Derecho a solicitar la limitación de su tratamiento. Este derecho consiste en que obtengas la limitación del tratamiento de tus datos que realiza el responsable. Podrás ejercitar tales derechos mediante solicitud acompañada de una fotocopia de tu D.N.I especificando cuál de éstos solicitas sea satisfecho, y remitida a la dirección: Calle RamÍrez De Arellano, 19 - 28043 (Madrid), o a la siguiente dirección de email: dpd@ccoo.es Ponemos a tu disposición los siguientes formularios: formulario de ejercicio de derecho de supresión, formulario de ejercicio de derecho de acceso, formulario de ejercicio de derecho de rectificación, formulario de ejercicio de derecho de portabilidad, formulario de ejercicio de limitación del tratamiento, formulario de ejercicio de oposición. En caso de considerar vulnerado tu derecho a la protección de datos personales, podrás interponer una reclamación ante la Agencia Española de Protección de Datos (www.aepd.es).8. MEDIDAS DE SEGURIDAD Finalmente se informa que FEDERACION DE ENSEÑANZA DE MADRID DE COMISIONES OBRERAS adoptará en su sistema de información las medidas técnicas y organizativas adecuadas, a fin de garantizar la seguridad y confidencialidad de los datos almacenados, evitando así, su alteración, pérdida, tratamiento o acceso no autorizado; teniendo en cuenta el estado de la técnica, los costes de aplicación, y la naturaleza, el alcance, el contexto y los fines del tratamiento, así como riesgos de probabilidad y gravedad variables asociadas a cada uno de los tratamientos.  




