+++
author = "Enrique García"
title = "Ley Maestra de Libertad de Elección Educativa"
date = "2021-01-20"
tags = [
    "educación", "normativa", "Madrid", "post migrado desde blogger"
]
toc = true

description = "Ley Maestra de Libertad de Elección Educativa"
thumbnail = "https://1.bp.blogspot.com/-NX9l_3Kq2Cw/YAh9cu7c5WI/AAAAAAAAVq0/d8M9u-a4PB4k-eZK2GFQ4uA-_JENZIfHQCLcBGAsYHQ/s16000/Screenshot_2021-01-20%2BAntet%25C3%25ADtulo%2B-%2B210120_cg%2Bpdf.png"
images = ["https://1.bp.blogspot.com/-NX9l_3Kq2Cw/YAh9cu7c5WI/AAAAAAAAVq0/d8M9u-a4PB4k-eZK2GFQ4uA-_JENZIfHQCLcBGAsYHQ/s16000/Screenshot_2021-01-20%2BAntet%25C3%25ADtulo%2B-%2B210120_cg%2Bpdf.png"]

+++

Revisado 14 septiembre 2024

## Resumen

El 19 enero 2021, fecha en la que entra en vigor LOMLOE, el gobierno de Madrid anuncia una ley educativa. El 20 enero 2021 se inicia trámite como proyecto de ley. Intento aportar detalles relacionándolo con otros temas. Este post, como todos, lo iré revisando.

_Este post es retirado por blogger después de que alguien reclame, y es el origen de migrar el blog entero abandonando blogger._

**Posts relacionados**  

*  [Lo básico de normativa básica](https://algoquedaquedecir.blogspot.com/2019/01/lo-basico-de-normativa-basica.html)  
*  [LOABLE reversión LOMCE](https://algoquedaquedecir.blogspot.com/2018/11/loable-reversion-lomce.html)   
*  [Finalidad evaluación para clasificación centros](../finalidad-evaluacion-para-clasificacion-centros) relacionado con plantear en 2024 la inconstitucionalidad de la Ley Maestra 
*  [Madrid quiere blindar los conciertos](https://algoquedaquedecir.blogspot.com/2018/10/madrid-quiere-blindar-los-conciertos.html)  
*  [LOMCE: plazas públicas y demanda social](http://algoquedaquedecir.blogspot.com/2018/09/lomce-plazas-publicas-y-demanda-social.html)   
*  [#LibertadDeEnseñanza](https://algoquedaquedecir.blogspot.com/2019/10/libertaddeensenanza.html)  
*  [Madrid crea centros privados con concierto en diferido](https://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html)  
*  [#YoAdoctrino y PIN Parental](https://algoquedaquedecir.blogspot.com/2019/08/yoadoctrino.html) (enlaza ya que se pacta con Vox)  
*  [Gratuidad de la educación](https://algoquedaquedecir.blogspot.com/2021/10/gratuidad-de-la-educacion.html) (enlaza con la privatización y lo pactado con Vox)  

## Detalle
 
### Cronología
 
**19 enero 2021**  
[twitter ComunidadMadrid/status/1351523211784159233](https://twitter.com/ComunidadMadrid/status/1351523211784159233])  
La Ley Maestra de Libertad de Elección Educativa garantizará en nuestra región:  
-Libertad de elección.  
-Calidad de la enseñanza.  
-Educación especial.  
-Español como lengua vehicular.  
@IdiazAyuso  anuncia el comienzo de su tramitación   


[Díaz Ayuso garantiza la libertad, la calidad de la enseñanza, la educación especial y el español - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/01/19/diaz-ayuso-garantiza-libertad-calidad-ensenanza-educacion-especial-espanol)  


[La ley 'anti Celaá' de la Comunidad de Madrid prevé ceder suelo público para centros concertados - huffingtonpost.es](https://www.huffingtonpost.es/amp/entry/ley-educacion-comunidad-de-madrid_es_60071488c5b6df63a91a5cbc/)  
 
**20 enero 2021**  
[Acuerdos de consejo de gobierno 20 enero 2021 - comunidad.madrid](https://www.comunidad.madrid/sites/default/files/210120_cg.pdf)  
> CONSEJERÍA DE PRESIDENCIA  
> • Informe  oral  sobre  el  sometimiento  al  trámite  de  consulta  pública  la elaboración  del  Anteproyecto  de  Ley  maestra  de  libertad  de  elección educativa.  
 
[Iniciamos la tramitación de la nueva Ley Maestra de Libertad de Elección Educativa - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/01/20/iniciamos-tramitacion-nueva-ley-maestra-libertad-eleccion-educativa)  
> La Comunidad de Madrid ha iniciado la tramitación de la nueva Ley Maestra de Libertad de Elección Educativa, una normativa autonómica con la que el Ejecutivo madrileño, dentro de sus competencias, pretende garantizar los pilares de la educación en su territorio que se ven amenazados con la entrada en vigor de la Ley Orgánica de modificación de la LOE (LOMLOE). El Consejo de Gobierno ha dado luz verde hoy esta tramitación para que pueda, finalmente, ser aprobada en la Asamblea de Madrid.  

**24 enero 2021**  
[twitter eossoriocrespo/status/1353298048240263169](https://twitter.com/eossoriocrespo/status/1353298048240263169)  
Hoy se celebra el #diainternacionaldelaeducacion. En la @ComunidadMadrid defendemos el sistema educativo como un pilar fundamental para generar una sociedad cada vez mejor formada y más libre. Acabamos de aprobar esta semana nuestra Ley Maestra de Libertad de Elección Educativa con la que vamos a seguir defendiendo un modelo de enseñanza que garantice a las familias madrileñas su capacidad de decidir el centro dónde quieran escolarizar a sus hijos.  

### Declaraciones y opiniones
Creo apartado separado de cronología y tramitación del proyecto de ley, donde también surgen declaraciones y opiniones. Algunas declaraciones y opiniones son de la propia comunidad / consejería / consejero y algunas pueden estar en algún subapartado, por ejemplo asociadas a tramitación / debates.
 
**5 febrero 2021**  
[Enrique Ossorio: "La Ley Maestra de Libertad de Elección Educativa de Madrid es más constitucional que la Ley Celaá" - 20minutos.es](https://www.20minutos.es/noticia/4571036/0/entrevista-consejero-educacion-comunidad-madrid/)  
> Antes de enviarse al Parlamento tendrá un informe de los servicios jurídicos, si opinaran que alguno de los preceptos no es constitucional, pues no lo incluiremos.

**10 febrero 2021**  
[Acuerdos de consejo de gobierno 10 febrero 2021 - comunidad.madrid](https://www.comunidad.madrid/sites/default/files/210210_cg.pdf)  
> Acuerdo por el que se determina la realización de los trámites a seguir en la  elaboración  del  Anteproyecto  de  Ley  maestra  de  libertad  de  elección educativa de la Comunidad de Madrid  
 
Ese mismo día la comunidad realiza un anuncio sobre un centro público (el mismo que quería hacer privado con concierto en diferido Madrid crea centros privados con concierto en diferido) y aprovecha para citar esta ley   

[Adjudicamos la construcción del nuevo Centro de Educación Especial público de Torrejón de Ardoz - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/02/10/adjudicamos-construccion-nuevo-centro-educacion-especial-publico-torrejon-ardoz)  
> Estas actuaciones dan respuesta a las necesidades de escolarización de los diferentes municipios de la zona y forman parte de la apuesta del Ejecutivo madrileño de mejorar las infraestructuras educativas públicas, facilitar la escolarización y favorecer la libre elección de centro para las familias madrileñas.  
> En este sentido, la Comunidad de Madrid está tramitando la **Ley Maestra de Libertad de Elección Educativa**, una nueva normativa que garantizará y reforzará la libertad de elección, la calidad de la enseñanza y la Educación Especial. En la actualidad hay más 57.000 alumnos con necesidades específicas de apoyo educativo en la región, de los que el 95,5% estudia en centros sostenidos con fondos públicos y el 90,5% se encuentran integrados en centros ordinarios.  
>  
> Asegurar la elección de las familias  
>  
> La nueva Ley permitirá a las familias que puedan seguir eligiendo entre las diferentes modalidades de escolarización existentes en la región para los alumnos con necesidades educativas especiales: en centro ordinario, aulas específicas de educación especial en centros ordinarios, centros de Educación Especial o educación combinada (el alumno se matricula en ordinario y especial y acude a uno u otro en función de los días).  
> Esta escolarización se revisará de forma continua para que en cada momento se cuente con el entorno educativo más inclusivo y potenciador de las capacidades del alumno. De esta manera, se garantiza en la región la continuidad de los centros de educación especial en la Comunidad de Madrid, pese a la aprobación de la Ley Orgánica de modificación de la LOE (LOMLOE), que trata de vaciar este tipo de centros.  
> La Administración educativa dotará de los recursos humanos y materiales necesarios, y se impulsará la formación del profesorado junto con la investigación e innovación en la atención del alumnado con necesidades educativas especiales. Además, se promoverá la implicación de las familias del alumnado, recogiendo estas actuaciones en los documentos de organización para hacer efectiva la comunicación y participación de la familia en la vida de los centros educativos.
 
**23 febrero 2021**  
[Otorgaremos máxima libertad a las familias para que puedan elegir el proyecto educativo - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/02/23/otorgaremos-maxima-libertad-familias-puedan-elegir-proyecto-educativo)  
> Para ello, el Gobierno regional remitirá a la Asamblea de Madrid el proyecto de **Ley Maestra de Libertad de Elección Educativa**, que “busca la mejora de nuestro sistema educativo avanzando en los principios de libertad, calidad e igualdad de oportunidades, dentro del margen que nos permite la vigente Ley Orgánica de Educación”, ha explicado Ossorio.  
> La nueva normativa en la Comunidad de Madrid “proclamará la búsqueda de la excelencia académica y el reconocimiento del esfuerzo y, en esa línea, impulsará la realización de evaluaciones externas dirigidas a mejorar la calidad, la equidad y la excelencia”, ya que, como ha asegurado el consejero, “la Ley Celaá es una norma anclada en las viejas leyes educativas socialistas y radicalizada por las fobias de la extrema izquierda y los independentistas”.  
> Además, la Ley Maestra de Libertad de Elección Educativa, fomentará un sistema que tenga la mayor semejanza a la zona única educativa, con el objeto de respetar la decisión de los padres sobre el centro en el quieren escolarizar a sus hijos. Del mismo modo, Ossorio ha garantizado la existencia de plazas suficientes para las enseñanzas declaradas gratuitas por la Ley, teniendo en cuenta la oferta existente de centros públicos y privados concertados y la demanda social.  
> En opinión del consejero madrileño, la nueva Ley Orgánica de Educación, “choca frontalmente con los principios que se han aplicado en la Comunidad de Madrid en materia educativa durante los últimos 25 años”, y “por ese motivo, hemos reaccionado con rapidez y, además de retrasar en la medida de lo posible algunas disposiciones de esta norma, hemos planteado una Ley autonómica que quiere avanzar en la mejora de nuestro sistema educativo defendiendo la libertad de elección de las familias, la calidad educativa, la igualdad de oportunidades, la educación especial y el español”.   

**29 marzo 2021**  
[twitter emilio_daz/status/1376462898374008836](https://twitter.com/emilio_daz/status/1376462898374008836)  
En @_CEIM_ , pregunto a @IdiazAyuso si, tras las elecciones y bajo su probable presidencia, continuará la tramitación de la #LeyMaestradeLibertad, y si cabría incorporar en ella un mandato de extensión de los conciertos a #Bachillerato y #FP de grado superior. 
@IdiazAyuso contesta que la #LeyMaestraLibertad continuará su tramitación, aunque pospone su compromiso de concertación del #Bachillerato a un mejor momento económico, bastando por ahora con las becas   
 
**3 abril 2021**  
[El Anteproyecto de Ley maestra de libertad de elección educativa del Gobierno de Ayuso supone una violación del derecho a la educación inclusiva. Ignacio Campoy Cervera Profesor Titular de Filosofía del Derecho Universidad Carlos III de Madrid y especialista en derechos de los niños - publico.es](https://m.publico.es/columnas/110670741219/dominio-publico-el-anteproyecto-de-ley-maestra-de-libertad-de-eleccion-educativa-del-gobierno-de-ayuso-supone-una-violacion-del-derecho-a-la-educacion-inclusiva/amp)  
 
**14 abril 2021**  
[Elecciones en Madrid: dos modelos educativos enfrentados por la libre elección, el ‘pin’ y los conciertos - abc.es](https://www.abc.es/espana/madrid/abci-elecciones-madrid-modelos-educativos-enfrentados-libre-eleccion-y-conciertos-202104140058_noticia_amp.html)  
> Y quiere seguir adelante con la **Ley Maestra** que «blinda» la concertada.  
 
**9 mayo 2021**  
[Retrato de la nueva concertada: sociedades mercantiles, beneficios, dividendos y altos salarios tras las subvenciones públicas - eldiario.es](https://www.eldiario.es/madrid/retrato-nueva-concertada-madrilena-sociedades-mercantiles-beneficios-dividendos-altos-salarios_1_7886541.html)  
> Este es el modelo que el PP ha impulsado en los últimos 20 años, los que han pasado desde que Madrid adquiriera las competencias en Educación. El sector que se manifestó contra la Lomloe en torno a la plataforma Más Plurales porque se ponía en riesgo casi su propia existencia y la "libertad de elección" de las familias. El modelo que la presidenta regional, Isabel Díaz Ayuso, quiere blindar con una ley educativa ad hoc, la **Ley Maestra**, que va directamente contra los postulados en la materia de la Lomloe y que será de las primeras, si no la primera, que apruebe el nuevo Gobierno.   

**17 mayo 2021**  
[¿QUEREMOS DE VERDAD REDUCIR LA SEGREGACIÓN ESCOLAR? - agendapublica.es](https://agendapublica.es/queremos-de-verdad-reducir-la-segregacion-escolar/)  
> El debate de la nueva ley educativa (Lomloe), y en particular sus apartados relativos a la segregación y la educación especial, aderezado con una reciente campaña electoral en Madrid, ha acrecentado a un nivel extremo la lucha pública-concertada. Viendo cómo la Comunidad de Madrid prepara ya su **Ley Maestra de Libertad de Elección**, uno se pregunta hasta qué punto la falta de consenso con la implementación de la Lomloe puede tener el resultado contrario al deseado, a pesar de lo acertado de algunas de sus propuestas. Podría acabar ocurriendo que, con motivo de esta guerra escolar, la segregación se mantuviera alta allí donde es mayor, e incluso pudiera seguir creciendo, por esas grandes tendencias que la acentúan.   
 
**10 mayo 2021**  
[Isabel Díaz Ayuso convertirá Madrid en el epicentro de la rebelión contra la Ley Celaá - masplurales.es](https://masplurales.es/isabel-diaz-ayuso-convertira-madrid-en-el-epicentro-de-la-rebelion-contra-la-ley-celaa/)  
> El borrador del anteproyecto de la Ley Maestra de Libertad de Elección Educativa de la Comunidad de Madrid condensa toda la rebelión frente a Celaá. Según explican en la Consejería de Educación, se encuentra en fase de estudio de las alegaciones recibidas y a la espera de ser aprobado por el Consejo de Gobierno. La idea es remitirlo a la Asamblea regional para que esté listo antes de final de año.  
 
Es una copia de [Isabel Díaz Ayuso convertirá Madrid en el epicentro de la rebelión contra la Ley Celaá - elmundo.es](https://www.elmundo.es/espana/2021/05/10/609797d621efa0356e8b45f5.html)  

**20 mayo 2021**  
[ENTREVISTA | Isabel Celaá: "En el nuevo curso la presencialidad será total"](https://www.20minutos.es/noticia/4702453/0/entrevista-isabel-celaa-nuevo-curso-presencialidad-total/)  
> La Comunidad de Madrid está elaborando su propia ley educativa. Vivimos en un Estado de derecho y las leyes orgánicas tienen rango superior a las leyes de una asamblea legislativa. Ninguna norma que contradiga la ley orgánica es válida.  

**22 mayo 2021**  
[Así es el plan de Ayuso para convertir Madrid en muro de resistencia contra la Ley Celaá: distrito único, por sexos... - 20minutos.es](https://www.20minutos.es/noticia/4704858/0/ayuso-madrid-muro-de-resistencia-ley-celaa-blindaje-concertada-distrito-unico-educacion-sexos/)  

**10 junio 2021**  

[La Comunidad de Madrid apuesta por la calidad, el bilingüismo, la igualdad de oportunidades y la libre elección educativa - comunidad.madrid](https://www.comunidad.madrid/notas-prensa/2021/06/10/comunidad-madrid-apuesta-calidad-bilinguismo-igualdad-oportunidades-libre-eleccion-educativa)  	

**11 junio 2021**  
[Ossorio anuncia un centro “del más alto prestigio” de la Comunidad de Madrid para formar profesores. El Consejero de Educación y portavoz del Gobierno autonómico hace una defensa de la escuela concertada ante una asamblea de Escuelas Católicas - elpais.com](https://elpais.com/espana/madrid/2021-06-11/ossorio-anuncia-un-centro-del-mas-alto-prestigio-de-la-comunidad-de-madrid-para-formar-profesores.html)  
> Todo en defensa de la educación concertada. “Como contrapeso a la Lomloe, y siempre dentro de la constitucionalidad, porque a nosotros nos gusta siempre cumplir la ley”, matizó Ossorio, por si había dudas, “hemos llevado a cabo diversas actuaciones”. Para empezar, relató que desde el Gobierno regional se ha elaborado un anteproyecto de ley maestra que persigue avanzar en los principios de libertad, calidad e igualdad de oportunidades.  
> Esa ley, prometió el consejero, fomentará un sistema que tenga la mayor semejanza a la zona única educativa, “con el objeto de respetar la decisión de los padres sobre el centro donde quieren escolarizar a sus hijos”. Ossorio recordó que Madrid “va a defender que haya plazas suficientes para las enseñanzas gratuitas declaradas por ley, teniendo en cuenta tanto la oferta que existe en los centros públicos, privados y concertados por la demanda social”. “Un concepto que desgraciadamente desaparece de la Lomloe, y no desaparece por casualidad”.  
>  
> Centros concertados  
>  
La promesa más esperada no tardó en llegar. ¿Qué va a pasar con los conciertos? Tranquilidad. La ley de Ossorio plantea la posibilidad de que se pueda convocar concursos públicos para la construcción y gestión de centros concertados en suelo público dotacional. “Algo que la Lomloe también olvida”, insistió. “Hemos garantizado la libertad de elección de más de 360.000 alumnos a través del decreto de conciertos del 13 de enero de 2021, [que elevó de seis a 10 años la duración de los conciertos educativos](https://elpais.com/espana/madrid/2020-12-15/ayuso-trata-de-blindar-la-educacion-concertada-con-un-decreto-que-amplia-a-10-anos-la-vigencia-de-los-contratos.html)”, recordó. La Ley Orgánica de Educación y la normativa autonómica, explicó, previó un plazo de seis años, aunque se permitía que las Comunidades Autónomas pudieran cambiar ese plazo. “Nosotros lo que hemos hecho es alargarlo. ¿Por qué? Porque si no dentro de dos años vencían los conciertos y por supuesto que con un Gobierno como el nuestro es evidente lo que iba a suceder, ¿no?”.  
Todas estas medidas, insistió, han sido adoptadas “por la total disconformidad respecto a la decisión del Gobierno de la nación de intentar reducir el papel de la educación concertada”.  

**5 septiembre 2021**  
[Enrique Ossorio: «Las leyes Celaá y Castells aleccionan a los alumnos en mantras de la izquierda» - larazon.es](https://www.larazon.es/madrid/20210905/neyjjc4wpze6fiim5euasmnlbm.html)  
> –¿Hasta dónde puede resistirse Madrid en su batalla contra la «ley Celaá»?  
> –Nos resistimos en todo aquello que, según nuestros principios, pensamos que es equivocado y hasta donde nuestras competencias nos permiten. Los temas en los que no tenemos margen de desarrollo, aunque no nos gusten, lo tenemos que aceptar. El año pasado adelantamos el proceso de elección de directores, pero este año ya se rige por la «Ley Celaá». Y a nosotros nos gusta cumplir la ley. Otro tema que no hay más remedio que cumplir es el decreto de evaluación, promoción y titulación. Tampoco podemos hacer nada. Si se dice que el alumno de segundo de Bachillerato con un suspenso puede titular, nosotros no podemos legislar en sentido contrario o si un alumno tiene ocho suspensos en la ESO el claustro puede tomar la decisión de que supere el curso. Sí hemos mantenido el concepto de la demanda social, porque es de cajón. O la construcción de centros concertados en suelo público. Hemos actuado en todo aquello que, según nuestros principios, pensamos que es bueno para los estudiantes madrileños y tenemos margen legal.  
 
**11 octubre 2021**  
[La gallinita de la Pampa y la segregación educativa. Agustín Moreno - publico.es](https://blogs.publico.es/otrasmiradas/52638/la-gallinita-de-la-pampa-y-la-segregacion-educativa/)  
> La derecha, y especialmente Isabel Díaz Ayuso, actúa como la gallinita de la Pampa que da las voces en un sitio y pone los huevos en otro. Es lo que está haciendo con la llamada "ley maestra". Establece falsos debates (supuestos ataques de la LOMLOE a la concertada, al castellano como lengua vehicular o la supresión de colegios de educación especial); pero amarra lo principal: los privilegios de una Iglesia que adoctrina en un espacio que tendría que ser laico por su propia naturaleza y la existencia de un sector privado que hace negocio con una educación segregadora que pagamos con fondos públicos. Hacen ruido y, de paso, buscan la enésima confrontación con el Gobierno central, aunque sus falacias nada tengan que ver con la realidad.  

**18 octubre 2021**  
[La Comunidad apuesta por el esfuerzo con la realización de pruebas externas de la Ley Maestra de Libertad Educativa - comunidad.madrid](
https://www.comunidad.madrid/notas-prensa/2021/10/18/comunidad-apuesta-esfuerzo-realizacion-pruebas-externas-ley-maestra-libertad-educativa)  
> La Comunidad de Madrid apuesta por el esfuerzo con la realización de pruebas externas entre el alumnado, frente a la premisa del Gobierno central con la LOMLOE de promover la titulación y la promoción con suspensos. Así lo recoge el proyecto de la Ley Maestra de Libertad Educativa, una normativa regional que también asegura la libertad de centro educativo, la igualdad de oportunidades, el derecho a recibir las enseñanzas en castellano y la excelencia académica, asegurando la transparencia informativa en todos los procesos.  
> El consejero de Educación, Universidades y Ciencia y portavoz del Gobierno, Enrique Ossorio, ha asistido hoy a las Primeras Jornadas sobre Educación en Libertad en España que organiza Sociedad Civil España Cívica. Esta iniciativa está encaminada a analizar las iniciativas legislativas del Gobierno del Estado promulgadas recientemente, así como recordar el derecho de los padres a elegir libremente el modelo de aprendizaje más adecuado para sus hijos.   

Para ver la ideología y la privatización asociada a España Cívica  
[España Cívica. Modelo sanitario - espanacivicaes - archive.org](https://web.archive.org/web/20210506034311/https://espanacivica.es/wp-content/uploads/2020/07/MODELO-SANITARIO.pdf)  

> Colaboración público privada (modelos PPP) desvirtuada  
> El modelo PPP, esta anatematizada por un fundamentalismo ideológico falto de argumentos, cuando en otras latitudes ha dado muy buenos resultado y es un modo de aliviar la carga económica sanitaria. Aunque tiene ciertas carencias y debería reformarse, tenemos el ejemplo en el modelo Muface, con el que están satisfechos y renuevan anualmente más del 90% de sus usuarios. Ya que muchos servicios de salud han creado empresas públicas sanitarias, podría estudiarse extender este tipo de modelo, depurado, a la población general.  
 
**2 noviembre 2021**  
[twitter hazteoir/status/1455459701890633728](https://twitter.com/hazteoir/status/1455459701890633728)  
La #LeyMaestra no hace ninguna referencia al #PinParental  
Pide a @IdiazAyuso que lo implante en la nueva ley de educación de la Comunidad de Madrid, FIRMA la petición:  
[Ayuso, incluya el Pin Parental en su nueva ley de educación - citizengo.org](https://citizengo.org/hazteoir/fm/204708-ayuso-incluya-pin-parental-su-nueva-ley-educacion)  
\#PinParentalYA   
 
La petición es de 10 octubre 2021  
 
**12 noviembre 2021**  
[La Comunidad garantiza la continuidad de los centros de Educación Especial con su Ley Maestra - comunidad.madrid](https://www.comunidad.madrid/notas-prensa/2021/11/11/comunidad-garantiza-continuidad-centros-educacion-especial-su-ley-maestra)  
 
**2 febrero 2022**  
[Ayuso, enemiga de la educación pública. Agustín Moreno - eldiario.es](https://www.eldiario.es/opinion/tribuna-abierta/ayuso-enemiga-educacion-publica_129_8708761.html)  
> La llamada “ley Maestra de libertad de elección educativa”, impulsada por el Partido Popular en la Comunidad de Madrid, no es otra cosa que un nuevo asalto a la educación pública.   

**3 febrero 2022**  
[twitter IdiazAyuso/status/1489289131326947332](https://twitter.com/IdiazAyuso/status/1489289131326947332)  
Aprobamos la Ley Maestra de Libertad Educativa para proteger la elección de las familias, blindamos la concertada y la especial y garantizamos la calidad frente al aprobado general del Gobierno socialista.  
Libertad, calidad e igualdad de oportunidades para los alumnos.  
 
**5 febrero 2022**  
Tuit citando tuit de Ayuso anuniciando la tramitación  
[twitter EdukFuenla/status/1489880908803067904](https://twitter.com/EdukFuenla/status/1489880908803067904)  
Este neoliberalismo educativo se ha encapsulado en la defensa de la libertad negativa, q considera que un individuo es libre en la medida en que nada o nadie restringe su acción de elegir o que puede ejercer su voluntad individual. Pero oculta y niega la libertad positiva
La libertad positiva, la capacidad colectiva para establecer los medios y mecanismos que permitan y posibiliten ejercerla a todos, al contar con las posibilidades para ello.  
En un Estado Social y de Derecho es la comunidad social, a través los poderes públicos y las Administraciones educativas quienes establecen las leyes, normas y reglas que generan las condiciones de posibilidad para garantizar a todos y todas el #DerechoALaEducación  
\#DerechoALaEducación en equidad, planificando y programando el sistema educativo de forma pública en función del bien común y no de los intereses individuales.  
Para ello restringen la libertad negativa interesada de los individuos (por ejemplo, en la conducción vial prohibiendo conducir por el carril izquierdo o saltarse los semáforos en rojo) en aras de preservar y garantizar tanto las libertades positivas de todos y todas,...
...y también las libertades de quienes no tienen los mismos recursos y medios en una sociedad tan desigual como la que se mantiene estructuralmente en un sistema como el capitalista.  
@EnriqueJDiez  
Bernabé Martínez, C   
   
### Tramitación del proyecto de ley
Se trata de una ley, y no se puede hacer vía decreto como decreto 31/2019: ver [Madrid quiere blindar los conciertos](https://algoquedaquedecir.blogspot.com/2018/10/madrid-quiere-blindar-los-conciertos.html)  
Eso implica que pasará por la Asamblea, no solo por consejo de gobierno.  
 
Inicialmente el día 19 comento que tendrá que tener trámite de audiencia, pero el día 20 consejo de gobierno cita consulta pública.  
Sobre el proceso de consulta pública / trámite de audiencia ver apartado "Participación ciudadana al elaborar normativas" en post [¡Reclamad, malditos!](https://algoquedaquedecir.blogspot.com/2018/07/reclamad-malditos.html)  
 
A 20 enero 2021 no aparece en [consultas públicas comunidad.madrid](https://www.comunidad.madrid/transparencia/normativa-planificacion/consulta-publica)  
 
Me parece llamativo que a 20 enero 2021 no esté publicado el plan anual normativo de 2021 conteniéndolo, cuando el de 2020 se aprobó en diciembre 2019, y el de 2019 se aprobó en abril de 2018.   
 
Otro tema es el tema de vender que se quiere "pacto/acuerdo" educativo, y cuando se puede se hace algo que no lo tiene. Ver post [Pacto educativo: cronología subcomisión y efecto real; prorrogar LOMCE](https://algoquedaquedecir.blogspot.com/2017/09/pacto-educativo-cronologia-subcomision.html)  
 
#### Consulta pública
 
**21 enero 2021**  
Publicación consulta pública en \https://www.comunidad.madrid/transparencia/anteproyecto-ley-libertad-eleccion-educativa  
Plazo para presentar aportaciones:  
De 22/01/2021 hasta 05/02/2021  
    [Resolución de apertura de trámite](https://www.comunidad.madrid/transparencia/sites/default/files/resolucion_consulta_publica_ley_maestra_0.pdf)   (444.01 KB)   
    [Memoria justificativa del proyecto](https://www.comunidad.madrid/transparencia/sites/default/files/memoria_anteproy_ley_liberta_elecc_1.pdf) (538.09 KB)  

**26 enero 2021**  
Las aportaciones en trámite de consulta son públicas
[Anteproyecto de Ley maestra de libertad de elección educativa - participa.comunidad.madrid](https://participa.comunidad.madrid/content/anteproyecto-ley-maestra-libertad-eleccion-educativa)  
 y se ve la primera, que es para pedir que se implante el PIN parental  
[participa.comunidad.madrid comment-1163](https://participa.comunidad.madrid/comment/1163#comment-1163)  
Ver post [#YoAdoctrino y PIN parental](http://algoquedaquedecir.blogspot.com/2019/08/yoadoctrino.html)  

**28 enero 2021**  
Veo que además de ser públicas las aportaciones, se pueden responder.  
Respondo al del PIN parental.  
 
Planteo una alegación. Es pública pero pongo texto aquí 
[La libertad de elección no puede dar derecho a segregar](https://participa.comunidad.madrid/comment/1166#comment-1166)  

--- 

La " Memoria Anteproyecto ley maestra libertad elección educativa" indica "Los  cambios  recientes en  la  normativa  básica  estatal  hacen  que  sea  el  momento  oportuno  para  que,  en cumplimiento  de  lo  dispuesto en la misma, se  garantice mediante  ley  de la  Comunidad  de  Madrid un  sistema educativo acorde con la Constitución"  
Considero un disparate jurídico argumentar que se va a hacer una normativa autonómica para garantizar que se cumple una normativa estatal básica; si se considera que la normativa estatal no es acorde a la Constitución, la vía es el Tribunal Constitucional, no una normativa autonómica. Si se hace lo contrario, es la normativa autonómica que no respete normativa básica la que puede ser llevada al Tribunal Constitucional.  
Se cita " la admisión de los alumnos en centros sostenidos con fondos públicos" que está tratada en artículo 84 Ley 2/2006, y que según Disposición final quinta. Título competencial, tiene carácter básico.  
Por ejemplo la memoria fija como objetivo "Garantizar la gratuidad de las enseñanzas obligatorias que se imparten en los centros financiados con fondos públicos." cuando el artículo 88 de Ley 2/2006 lo regula con carácter básico.  
Considero que el aspecto esencial a contemplar es la reducción de la segregación educativa, que es un problema en la Comunidad de Madrid.  
Se pueden ver datos en [Datos segregación educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-segregacion-educacion.html), pero pongo algunos detalles.  

* [De la segregación socioeconómica a la educación inclusiva ANEXO: Comunidad de Madrid MÉZCLATE CONMIGO, Save The Children](https://www.savethechildren.es/sites/default/files/imce/docs/mezclate_conmigo-anexo_cm.pdf)  
> La Comunidad de Madrid tiene un índice de segregación de 0,36,el más alto después de Hungría.  
> A nivel estatal, en los últimos seis años el índice de segregación ha aumentado en un 10,6%, lo que significa que la comunidad madrileña registra un incremento de más del triple que el crecimiento medio en España.  
...  
El decreto de libre elección Los  nuevos  criterios  de  selección  de  alumnado  junto  con  el  mecanismo  de  Boston  generan  segregación.  
...  
En la Comunidad de Madrid, casi 2 de cada 10 colegios son guetos,el doble que la media estatal, y más de 4 de cada 10 sufren concentraciónde alumnado vulnerable.  
Casi 8 de cada 10 centros gueto son de titularidad pública, y son también los que acogen un mayor número de estudiantes pertenecientes al perfil socioeconómico más bajo (74,9%).  

* [Magnitud de la Segregación escolar por nivel socioeconómico en España y sus Comunidades Autónomas y comparación con los países de la Unión Europea, F. Javier Murillo, Cynthia Martínez-Garrido](https://ojs.uv.es/index.php/RASE/article/view/10129)  
> ...la Comunidad de Madrid con una segregación muy alta, solo superada por Hungría dentro de la Unión Europea. Con todo ello se  observa la incidencia de las políticas educativas regionales respecto a los criterios de admisión de centros en la segregación escolar, mostrando que políticas como el fomento de la educación privada, de la competencia de centros mediante la publicación de rankings o la creación de un distrito único puede configurar sistemas educativos inequitativos que atentan a la igualdad de oportunidades.  

* [Effects of School Choice on Students’ Mobility:Evidence from Madrid, David Mayor](http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-Disposition&blobheadervalue1=filename%3DWEB+effectofschoolchoice-paper.pdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1352938014755&ssbinary=true)  
> The best schools undergo the greatest rise in the within-school socioeconomic heterogeneity. However, those schools situated in the poorest districts suffer an increase in segregation. As a result, there is a decrease in the overall level of segregation together  with an increase in the degree of segregation of schools located in low-income districts.  

No es legítima una libertad de elección que permita elegir segregar.  
Cuando entran en colisión derechos fundamentales, se pondera y prevalece uno.  
No puede prevalecer libertad a elegir de padres con recursos públicos frente al derecho a educación en igualdad sin segregación de los alumnos.  
Sería dar derecho a segregar.  
Segregar no es un derecho, y como los privados con concierto segregan globalmente, la administración debe intervenir para garantizar los derechos de los ciudadanos y "remover los obstáculos que impidan \[la igualdad]" (art9 CE)   

Considero llamativo que en la memoria no se citen un punto que sí han sido citado en la web oficial y en la cuenta oficial de la Comunidad

[Díaz Ayuso garantiza la libertad, la calidad de la enseñanza, la educación especial y el español - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/01/19/diaz-ayuso-garantiza-libertad-calidad-ensenanza-educacion-especial-espanol)
> Garantizar el "español como lengua vehicular" es algo llamativo en una comunidad sin lengua cooficial, y en la que existe un programa de bilingüismo en el que no hay alumnado que solo utiliza el castellano como lengua vehicular en asignaturas instrumentales, recibiendo menos horas en castellano que alumnos en otras comunidades.  

También considero que se debe tener en cuenta que en artículo 140 de Ley 2/2006 (normativa básica), el punto 2 no permite establecer clasificaciones de los centros, cosa que la administración educativa de Madrid realiza amparándose en la libertad de elección.  

--- 

A día 10 febrero reviso alegaciones finalizada consulta pública (finalizó día 5 febrero)  

26ene PIN parental   
28ene mi respuesta por a mensaje 26ene sobre PIN parental  
Segregación (mi aportación)   
2feb propuestas UGT  
3feb gratuidad escuelas infantiles  
4feb PIN parental (Plataforma acTÚa FAMILIA)  
4feb PIN parental  
4feb familias privados con concierto católicos (EDUCACIÓN Y FAMILIAS_FEDERACIÓN DE AMPAS DE MADRID, parece ser https://www.educacionyfamilias.org/)  
4feb propuestas UGT  
4feb semipresencialidad  
4feb PIN parental (no explícito)  
4feb propuestas UGT  
4feb propuestas UGT  
4feb adoctrinamiento y opinión familias (AMPA RCSICILA, que parece https://www.santaisabel.es/)  
4feb Educatio Servanda  
5feb Educadores contra el adoctrinamiento  
5feb propuestas UGT  
5feb propuestas UGT  
5feb Confederación Española Familias de Personas Sordas FIAPAS  
5feb Asociación de Colegios Privados Independientes - CICAE  
5feb FEUSO  
5feb propuestas UGT  
5feb recursos, inversión, temporalidad ...  
5feb propuestas UGT  
5feb APRODIR Asociación de Directivos de Educación Secundaria de Madrid  
5feb propuestas UGT  
5feb propuestas UGT  
5feb inclusión  
5feb fomentar la transformacion de CEIP en CEIPSO   
5feb recursos, inversión, temporalidad ...  

#### Trámite de audiencia

**5 marzo 2021**  
Se publica trámite de audiencia, alegaciones de 8 a 29 de marzo en \https://www.comunidad.madrid/transparencia/anteproyecto-ley-maestra-libertad-eleccion-educativa-comunidad-madrid  
 
Comento unas ideas rápidas  
[twitter FiQuiPedia/status/1367913919520604166](https://twitter.com/FiQuiPedia/status/1367913919520604166)  
Un par de ideas del anteproyecto de 31 páginas:  
-Art 5.1.b. Se explicita poder concertar privados que segregan por sexo.  
-Art 7. Se rescata demanda social y cesión suelo a privados para conciertos en diferido, ambas introducidas por LOMCE y ambas derogadas por LOMLOE  
Sobre lo anunciado de castellano como lengua vehicular, artículo 4.1.c es lo suficientemente vago para no corregir el mal implementado bilingüismo madrileño.  
Colaborar con otras CCAA, quizá sea ir allí para hablar castellano y no inglés.  
Colaboración público-privada que no falte.  
Se puede ver en MAIN (memoria del análisis de impacto normativo) resumen que hacen de la consulta.  
Dicen que contemplan las aportaciones: la mía era sobre segregación, y la única vez que se cita en art6.2 es para decir que la zona única evita CUALQUIER TIPO de segregación   
 
Las alegaciones al final se incorporan al MAIN, y se citan ideas más adelante en la remisión a la Asamblea.  
 
#### Dictamen del Consejo Escolar de Madrid
**26 marzo 2021**  
[Choque de leyes. Emilio Díaz. Responsable de Comunicación y Relaciones Institucionales de Escuelas Católicas Madrid](https://www.madridiario.es/choque-leyes)  
> "...a punto de someter esta iniciativa legislativa al dictamen del Pleno del Consejo Escolar de Madrid el próximo día 9 de abril..." 
 
**22 marzo 2021**  
[twitter emilio_daz/status/1374048915893403650](https://twitter.com/emilio_daz/status/1374048915893403650)  
En la Comisión Permanente del Consejo Escolar de Madrid, en el trámite de aprobación del dictamen sobre el proyecto de Ley Maestra de la Libertad Educativa, para su presentación ante el Pleno del Consejo.   

**7 abril 2021**  
[Rechazo frontal de la comunidad educativa madrileña a la Ley Maestra de Libertad de Elección](https://madrid.ccoo.es/noticia:583718--Rechazo_frontal_de_la_comunidad_educativa_madrilena_a_la_Ley_Maestra_de_Libertad_de_Eleccion&opc_id=852be2dd2d75b4a6251836508219d73b)  
CCOO, UGT y la FAPA Giner de los Ríos presentarán un dictamen alternativo ante el Consejo Escolar  
Las organizaciones de la comunidad educativa madrileña han expresado este miércoles su rechazo al anteproyecto de Ley Maestra de Libertad de Elección Educativa de la Comunidad de Madrid. CCOO, UGT y la FAPA Francisco Giner de los Ríos han anunciado en rueda de prensa que el próximo viernes, 9 de abril, presentarán un dictamen alternativo ante el Consejo Escolar.  

Miro si hay algo que impida celebrar un consejo escolar con la Asamblea disuelta  
[Decreto 46/2001, de 29 de marzo, por el que se aprueba el Reglamento de Funcionamiento interno del Consejo Escolar de la Comunidad de Madrid](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=1395&cdestado=P#no-back-button)  

Creo entender por artículos 7 y 8 que los miembros ya están nombrados y los mandatos no terminan con la disolución de Asamblea por lo que sí es posible.  
[Composición del consejo escolar](https://www.comunidad.madrid/sites/default/files/11drupal_composicion.pdf)  
 
[twitter FiQuiPedia/status/1379854848733089792](https://twitter.com/FiQuiPedia/status/1379854848733089792)  
9 abril, Asamblea disuelta, dictamen sobre ley que quiere legalizar #ConcertadosEnDiferido  
Votan consejeros nombrados a dedo:  
-@MCLA73  
-José Ignacio Martín Blasco  
-Mª Mercedes Marín García  
-José María Rodríguez Jiménez 
-Nikolay Yordanov Bogdanov  
 
Se puede ver información de cada uno  
[José María Rodríguez Jiménez - comunidad.madrid](https://www.comunidad.madrid/transparencia/persona/jose-maria-rodriguez-jimenez)   
En 2023 no funciona enlace \https://www.comunidad.madrid/transparencia/persona/nikolay-yordanov-bogdanov
 
Presidenta también, no sé si con voto  
María del Pilar Ponce Velasco  
 
[La FAPA Giner de los Ríos, CCOO y UGT se oponen a la tramitación de la ley educativa tras disolver la Asamblea - europapress.es](https://amp.europapress.es/madrid/noticia-fapa-giner-rios-ccoo-ugt-oponen-tramitacion-ley-educativa-disolver-asamblea-20210407143449.html)  

**8 abril 2021**  
[twitter p_pdelgado/status/1380182825203339271](https://twitter.com/p_pdelgado/status/1380182825203339271)  
Se habla estos días de la Ley Maestra de Educación de la Comunidad de Madrid. Normal, mañana viernes 9 de abril se ha convocado el Consejo Escolar regional para emitir dictamen. Sí, se han empeñado, a pesar de que la Asamblea está disuelta.  
¿Cuáles son los contenidos?  
Hilo  
Para empezar, se incumplen los “principios de buena regulación” (ley 39/2015), pues lo que se pretende regular o ya está recogido en la Ley estatal o puede aplicarse mediante disposiciones reglamentarias de desarrollo.  
Tampoco contribuye al cumplimiento del principio de seguridad jurídica (art. 9.3 de la CE), habida cuenta de que tanto el derecho a la educación como a la libertad de enseñanza están garantizados en las normas estatales.  
Sobre los conciertos educativos el texto no añade nada nuevo pues las competencias de las CCAA están limitadas a poco más que a la suscripción material de los conciertos con arreglo a  la normativa estatal.  
Eso sí, incurre en inconstitucionalidad al insistir en pretender arrogar para la Comunidad de Madrid la capacidad de ceder terrenos de suelo público dotacional para la construcción de colegios concertados a pesar de que la ley estatal no lo permite.  
Sobre Educación Especial el texto parafrasea los principios regulatorios estatales. Eso sí, ojo, dado el énfasis puesto y que se asocia a la “libre elección de centro” cabe suponer que se pretende abrir camino a nuevos conciertos educativos en este ámbito.  
En cambio, mucho se habla de dotar de recursos suficientes a la Educación Especial, pero ni hay memoria económica ni compromiso presupuestario alguno.  
De traca lo dedicado a la pretendida defensa del castellano, cuando en Madrid sólo hay una lengua oficial, precisamente el castellano. Si el texto se aprueba definitivamente igual se puede solicitar, en base a este precepto, el cese del programa bilingüe...  
La derecha madrileña insiste en defender la enseñanza diferenciada por sexo con fondos públicos, cuando el ministerio ha suprimido en la LOMLOE esta posibilidad y la CM no tiene competencia para restablecerla. Más fuente de  inconstitucionalidad.  
El art. 5.1 contempla el “esfuerzo y la excelencia” como elementos valorables para la admisión a un centro escolar. También inconstitucional, pues hablando de educación OBLIGATORIA no caben obstáculos al derecho a la educación gratuita y OBLIGATORIA (art. 27.3 de la CE)  
La derecha pretende seguir mirando para otro lado haciendo la vista gorda con la deslealtad que representa que la Concertada cobre cuotas prohibidas a su alumnado. Ni una sola palabra al respecto sobre el papel de la Inspección Educativa.  
La derecha pretende violentar el derecho a elegir centro, curiosamente, pues la ley no garantiza a las familias un centro público en cada zona.  
En definitiva, un texto puramente propagandístico. Tan propagandístico que se tramita con la Asamblea de Madrid disuelta, que incluye a sabiendas preceptos inconstitucionales y que da la espalda, una vez, más a las necesidades reales.  

**9 abril 2021**  
[twitter ecatolicmadrid/status/1380394189230313472](https://twitter.com/ecatolicmadrid/status/1380394189230313472)  
¡Buenos días! Durante el día de hoy, participamos en el Pleno del Consejo Escolar de Madrid para dictaminar el proyecto de ley maestra de la libertad educativa de la Comunidad de Madrid. Feliz viernes.   

Director General de Educación Infantil, Primaria y Especial de la Comunidad de Madrid.
[twitter Nachetemartin/status/1380474959718457349](https://twitter.com/Nachetemartin/status/1380474959718457349)  
El Pleno del Consejo Escolar de @ComunidadMadrid ha votado la aceptación del Anteproyecto de Ley Maestra de Libertad de Elección Educativa.  
La Consejería de Educación trabajando por la educación madrileña y con toda la Comunidad Educativa.   
(incluye capturas de videoconferencia con los participantes)  

La cuenta de Manuel Bautista era MCLA73 en 2021, en 2024 es mbautista_pp  
[twitter mbautista_pp/status/1380477089032331265](https://twitter.com/mbautista_pp/status/1380477089032331265)  
Hoy en el pleno del Consejo Escolar de la @ComunidadMadrid se ha votado la aceptación del anteproyecto de la Ley Maestra de libertad de Elección Educativa.  
Gran noticia! Seguimos trabajando @IdiazAyuso  
(incluye capturas de videoconferencia con los participantes)  
 
**10 abril 2021**  
[twitter CCOOEducaMa/status/1380803957057015810](https://twitter.com/CCOOEducaMa/status/1380803957057015810)  
Ayer llegaba la Ley Maestra al Consejo Escolar para recibir dictamen.  
Tenemos que hablar de esta ley que pretende mantener viva la LOMCE en la Comunidad de Madrid, y de su plan para acabar con la educación pública que conocemos.  
Va hilo   

**10 abril 2021**  
Pongo solicitud transparencia para pedir dictamen y votos particulares, además de otra documentación  

**12 abril 2021**  
[La FAPA Giner de los Ríos, CCOO y UGT denuncian el anteproyecto de Ley Maestra de Libertad de Elección Educativa. Por considerar que no debería continuar su tramitación en periodo electoral y con la Asamblea de Madrid disuelta](https://madrid.ccoo.es/noticia:584085--La_FAPA_Giner_de_los_Rios_CCOO_y_UGT_denuncian_el_anteproyecto_de_Ley_Maestra_de_Libertad_de_Eleccion_Educativa&opc_id=711af56c79ef209f3b5831a8b38f22b1)  
 
Otro enlace similar de mismo día
[La FAPA Giner de los Ríos, CCOO y UGT denuncian el anteproyecto de Ley Maestra de Libertad de Elección Educativa](http://www.feccoo-madrid.org/noticia:584087--La_FAPA_Giner_de_los_Rios_CCOO_y_UGT_denuncian_el_anteproyecto_de_Ley_Maestra_de_Libertad_de_Eleccion_Educativa&opc_id=d088ffe0c061e63c016a6783356593c2
)  
en este caso se adjunta una nota de prensa  
[Nota de prensa. La FAPA Giner de los Ríos, CCOO y UGT denuncian el anteproyecto de Ley Maestra de Libertad de Elección Educativa](http://www.feccoo-madrid.org/22ebc743fd9960ee0a9369b173513f13000063.pdf)  
  
[twitter feuso_madrid/status/1381569027227680770](https://twitter.com/feuso_madrid/status/1381569027227680770)  
El Consejo Escolar de Madrid aprueba el dictamen a favor de la Ley Maestra de Libertad de Educación 
FEUSO MADRID FUE EL ÚNICO SINDICATO QUE PIDIÓ EL CONCIERTO DE FP DE GRADO SUPERIOR Y EL RECONOCIMIENTO DE LA ASIGNATURA DE RELIGIÓN Y A SU PROFESORADO   
Incluye [enlace](https://mailchi.mp/72d51a6d1113/el-consejo-escolar-de-madrid-aprueba-el-dictamen-a-favor-de-la-ley-maestra-de-libertad-de-educacin-835089?e=[UNIQID])   
> FEUSO MADRID FUE EL ÚNICO SINDICATO QUE PIDIÓ EL CONCIERTO DE FP DE GRADO SUPERIOR Y EL RECONOCIMIENTO DE LA ASIGNATURA DE RELIGIÓN Y A SU PROFESORADO EN IGUAL DE CONDICIONES QUE EL RESTO  
> El pasado viernes 9 de abril fue aprobado el dictamen del Consejo Escolar de la Ley Maestra de Libertad de Educación.  
> FEUSO MADRID votó, como no podía ser de otra manera, a favor. FEUSO MADRID hizo algunas aportaciones:  
> Fue el único sindicato que hizo referencia a la  necesidad de ofertar en igualdad de condiciones que el resto, la asignatura de Religión y al profesorado que la imparte.  
> También destacamos la necesidad de reconocer el concierto educativo de FP de grado superior. Es inestimable la labor social de muchos centros de Formación Profesional en zonas muchas veces marginales, que permiten la promoción de alumnos con escasas posibilidades.  
> FEUSO MADRID hizo notar que para que exista verdadera libertad, hay que asegurar la calidad y equidad de la educación en todos los centros sostenidos con fondos públicos:  
> Los profesionales de los centros sostenidos con fondos públicos deben de tener las mismas condiciones de trabajo.  
    Salario: Incluir sexenios y diferentes complementos también en la enseñanza concertada.  
    Jornada: Mismo número de horas lectivas en todos los centros sostenidos con fondos públicos  
    Se deben cubrir las sustituciones del profesorado desde el primer día de ausencia. Las condiciones del personal sustituto deben ser las mismas que las del personal titular.  
    Disminución de las ratios de alumnos en el aula para una mejor atención personalizada al alumno a 20 en infantil y primaria, 25 en secundaria y 30 en bachillerato.  
    Facilitar el acceso a la jubilación de los trabajadores mayores mediante la jubilación parcial y jubilación especial permitiendo la entrada de jóvenes bien formados en idiomas y nuevas tecnologías.  
 
**13 abril 2021**  
[twitter eossoriocrespo/status/1381944413740486657](https://twitter.com/eossoriocrespo/status/1381944413740486657)  
Hemos mantenido hoy una reunión con representantes de @MasPlurales para  abordar el estado de desarrollo de la 'Ley Celáa', de la que todavía estamos a la espera de los decretos que prometió llevar a cabo el @educaciongob.  
[twitter eossoriocrespo/status/1381944416118652930](https://twitter.com/eossoriocrespo/status/1381944416118652930)  
La @ComunidadMadrid sigue adelante con la tramitación de la Ley Maestra de Libertad de Elección Educativa, cuyo anteproyecto ya fue aprobado en el Consejo Escolar la semana pasada.   

[twitter Jose_Luis_Pazos/status/1381971302697799683](https://twitter.com/Jose_Luis_Pazos/status/1381971302697799683)  
\[1/n] Esta nota de prensa recoge una gran mentira: el Consejo Escolar de la @ComunidadMadrid no ha aprobado la ley, sólo un dictamen preceptivo a un anteproyecto que trata de corregir errores de redacción especialmente inaceptables en una norma educativa.  

[La Comunidad de Madrid garantiza la existencia de plazas suficientes en las enseñanzas obligatorias y gratuitas - comunidad.madrid](https://www.comunidad.madrid/notas-prensa/2021/04/12/comunidad-madrid-garantiza-existencia-plazas-suficientes-ensenanzas-obligatorias-gratuitas)  
> El consejero de Educación y Juventud y portavoz del Gobierno autonómico, Enrique Ossorio, ha mantenido hoy una reunión con Más Plurales, una plataforma educativa que engloba a entidades como las asociaciones de padres COFAPA y CONCAPA, los sindicatos FSIE y USO y las patronales Escuelas Católicas o CECE.  

\[2/n] Dictamen en el que además se dice que, en caso de que alguna vez se aprobara como ley en la Asamblea de Madrid, no entre en vigor hasta que no tenga memoria económica y un calendario de aplicación vinculado a ella.  
\[3/n] Y no es un error al expresarse del consejero de Educación, @eossoriocrespo, es una actuación deliberada y reiterada del @ppmadrid para manipular el papel del CE cuando les interesa, como ocurre ahora de nuevo.  
\[4/4] Un anteproyecto contrario a ley en materia básica y que nunca verá la luz, lo que nos ahorrará la vergüenza ajena de verlo anulado por inconstitucional.  

Tras ver la reunión con @masplurales, me surge que debe estar en su agenda, así que planteo "reclamación", que pongo como "solicitud de acceso", ya que no localizo buzón. Lo muevo al post sobre [ley de transparencia en Madrid](http://algoquedaquedecir.blogspot.com/2019/03/ley-de-transparencia-madrid.html)  

[twitter educacion_psoeM/status/1382078208812711942](https://twitter.com/educacion_psoeM/status/1382078208812711942)  
La Ley Maestra de Libertad de Elección Educativa NO ha sido aprobada por el Consejo Escolar de Madrid, sólo un dictamen preceptivo que pone condiciones a su entrada en vigor, pero NO llegará a aprobarse @asambleamadrid  
 
**15 abril 2021**  
[El Consejo Escolar regional rechaza la tramitación de la Ley Maestra de Libertad Educativa](https://www.rivasactual.com/el-consejo-escolar-regional-rechaza-la-tramitacion-de-la-ley-maestra-de-libertad-educativa/)  
> El rechazo se fundamenta en una enmienda aprobada por el Consejo y propuesta por la FAPA Giner de los Ríos, en la que se exige, antes de avalar el anteproyecto de Ley del Gobierno regional, que el mismo cuente con la preceptiva memoria económica y el calendario de implantación.  

**20 abril 2021**  
Veo publicado dictamen y votos particulares. 

Lo comento en Twitter 
[twitter FiQuiPedia/status/1384497495926059011](https://twitter.com/FiQuiPedia/status/1384497495926059011)  
Disponibles en web dictamen y votos particulares Consejo Escolar Comunidad de Madrid sobre Ley Maestra de Libertad de Elección Educativa. Publican por pedir vía transparencia, no actualizaban web desde 2020.  
[Dictámenes e informes - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/dictamenes-e-informes)  

Dictamen 6/2021

[Al Anteproyecto de Ley Maestra de Libertad de Elección Educativa de la Comunidad de Madrid.](https://www.comunidad.madrid/sites/default/files/d_6_2021_ant_ley_maestra.pdf) (13 de abril de 2021)  

[Voto particular conjunto presentado por los representantes de CC.OO, UGT, FAPA.](https://www.comunidad.madrid/sites/default/files/voto_particular_ccoo_admision_dictamen_cp_ley_maestra_lib._ed.pdf) (13 de abril de 2021)  

[Votos particulares presentados por los representantes de la FAPA.](https://www.comunidad.madrid/sites/default/files/vp_fapa_dictamen_6.pdf) (13 de abril de 2021)  

[Votos particulares presentados por los representantes de UGT.](https://www.comunidad.madrid/sites/default/files/vp_ugt_dictamen_6.pdf) (13 de abril de 2021)  

[Votos particulares presentados por los representantes de FERECECA.](https://www.comunidad.madrid/sites/default/files/votos_particulares_ferececa_madrid.pdf) (9 de abril de 2021)  

[Voto particular presentado por CC.OO.](https://www.comunidad.madrid/sites/default/files/voto_particular_ccoo_admision_dictamen_cp_ley_maestra_lib._ed.pdf) (13 de abril de 2021)  

Enmienda 51 @ecatolicmadrid
 
> "..  concursos públicos para la gestión de centros concertados construidos por la propia administración sobre suelo público dotacional..La administración sería la dueña  del inmueble..”  

Centros SON públicos o privados, privados pueden ESTAR concertados  
Esa propuesta es la privatización de la educación al estilo de la sanidad. @ecatolicmadrid @joseanpoveda se saltan [artículo 108 LOE](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a108), no alterado ni por LOMCE ni LOMLOE, No existe ningún centro que sea concertado, son privados.

**1 mayo 2021**  
Veo que ya está la reunión de 13 abril en la agenda, y convocó Ossorio
[Reunión Plataforma Más Plurales - comunidad.madrid](https://www.comunidad.madrid/transparencia/agenda/reunion-plataforma-mas-plurales)  

**11 mayo 2021**  
Recibo [resolución denegatoria a solicitud 10 abril 2021](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Normativa/Madrid/LeyMaestra/2021-05-11-Denegatoria.pdf)  
No citan que ya está publicado dictamen de Consejo Escolar   

#### Remisión a Asamblea 

**7 julio 2021**  
[ACUERDOS CONSEJO DE GOBIERNO 7 de julio de 2021](https://www.comunidad.madrid/sites/default/files/210707_cg.pdf)  
> Acuerdo por el que se aprueba el proyecto de ley maestra de libertad de elección educativa de la Comunidad de Madrid y se ordena su remisión a la Asamblea de Madrid.  

[Aprobamos el proyecto de Ley Maestra de Libertad Educativa que asegura la concertada y especial - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/07/07/aprobamos-proyecto-ley-maestra-libertad-educativa-asegura-concertada-especial)  

Pongo petición MAIN actualizada, dictamen CJA y documentación 49/457705.9/21  
 
**15 julio 2021**  
Publicado en web Asamblea
[twitter FiQuiPedia/status/1415695818787491841](https://twitter.com/FiQuiPedia/status/1415695818787491841)  

[PL-1/2021 Proyecto de Ley maestra de libertad de elección educativa de la Comunidad de Madrid, acompañado de certificación del Acuerdo de 7-07-21 y de la documentación anexa que se relaciona](https://www.asambleamadrid.es/actividad/iniciativa?iniciativa=329841)  
Incluye [doc 496 páginas con MAIN, informes, memoria,  alegaciones, dictamen y votos Consejo Escolar, informe abogacía](https://web.archive.org/web/20210715230125/https://www.asambleamadrid.es/static/docs/registro-ep/RGEP8682-21.pdf).  
_(el archivo se enlaza en archive org, eliminado de web de asamblea tras reclamar a DPD CSV 0944690896445215074681)_  
![](https://pbs.twimg.com/media/E6WP1YXXEAAynyz?format=jpg)  
![](https://pbs.twimg.com/media/E6WP1wQWUAEgH8t?format=jpg)  
No hay dictamen CJA  
![])https://pbs.twimg.com/media/E6WP2BsWEAMSv_L?format=jpg)  

Abogacía @ComunidadMadrid  (no creo que pro-LOMLOE, dice "mientras no sea declarada inconstitucional") aclara que no se puede concertar diferenciada por sexo: "legislación estatal básica opera como canon de constitucionalidad de
las leyes autonómicas".  
![](https://pbs.twimg.com/media/E6WSr_oWYAAj5s-?format=jpg)  
@educacmadrid lo retuerce  
![](https://pbs.twimg.com/media/E6WSsffWYAQ_Pjx?format=jpg)  
 
A ver, @eossoriocrespo, lo que dice MAIN respondiendo a consideración esencial Abogacía @ComunidadMadrid es un disparate. Claro que artículo 2 de la Convención relativa a la lucha contra las discriminaciones en la esfera de la enseñanza aplica: leedlo.   

Cito [twitter FiQuiPedia/status/984580040364101632](https://twitter.com/FiQuiPedia/status/984580040364101632)  
UNESCO art2 "En caso Estado las admita" se permite a) por sexo y b) por religión. Contexto: 1960, firmantes podían tener ya educación diferenciada, se dejaba firmar sin convertir en ilegal lo que ya estaba haciendo, pero añadirlo en leyes sXXI, de traca  
Enlazo [Educación separada por sexos: conciertos, legalidad y LOMCE](http://algoquedaquedecir.blogspot.com/2017/10/educacion-separada-por-sexos-conciertos.html)  

Es para leer despacio, pero muestra que algo se puede cambiar, y algo es infinitamente superior a cero: puse 5 y admiten 1. #ReclamadMalditos  
Otro detalle: en documento publican DNIs, firmas, email y domicilios particulares incumpliendo RGPD. Reclamado a DPD.  
![](https://pbs.twimg.com/media/E6WXuP-XIAMtOx1?format=jpg)  

Mi alegación 2 era sobre #ConcertadosEnDiferido  
Me citan [art 28 RD 2377/1985](https://boe.es/buscar/act.php?id=BOE-A-1985-26788#a28)  
Cuándo se "inicia el procedimiento de autorización administrativa" de un privado que recibe suelo público antes de existir?  

Sobre alegación 3, no se atiende, cita [DA25 LOE](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#davigesimoquinta) que solo permite segregar por sexo a privados sin concierto  

@eossoriocrespo dice no segregar con dinero público sería discriminatorio por motivos económicos!  
Sería ilegal: DA25 es básica, se debe cumplir  

Aplicando mismo argumento de @eossoriocrespo, artículo 87 y 88 LOE, básicos, solo permiten segregar y cobrar a las familias a privados sin concierto, por lo que no segregar con dinero público sería discriminatorio por motivos económicos  

**21 julio 2021**  
Proyecto ley maestra publicado en BOAM 15 julio  
_enlace es otro en octubre 2023_  
[Boletín Oficial de la Asamblea de Madrid Número 7 15 julio 2021](https://www.asambleamadrid.es/static/doc/publicaciones/BOAM_12_00007_fasciculo1.pdf#page=79)  
[Enmiendas hasta 13 septiembre](https://asambleamadrid.es/static/docs/registro-sp/RGSP1211-21.pdf)  
 
Los ciudadanos de Madrid también pueden formular consideraciones #ReclamadMalditos  
Trabado en Madrid pero entiendo que no soy ciudadano de Madrid porque tengo domicilio en CLM, por lo que no puedo aunque sí pude en trámite de consulta y de audiencia  
[Reglamento de la Asamblea. TÍTULO VII. DEL PROCEDIMIENTO LEGISLATIVO. CAPÍTULO II. Del procedimiento legislativo común. Sección Primera. De los proyectos de ley](https://asambleamadrid.es/servicios/normativa/reglamento-asamblea/t7-capitulo-dos)  
> 5. Durante los diez primeros días del plazo de veinte establecido para la presentación de enmiendas al articulado, se abrirá un periodo durante el que cualquier persona que ostente la condición de ciudadano de la Comunidad de Madrid podrá formular consideraciones acerca del texto del proyecto de ley, mediante escrito dirigido a la Mesa de la Asamblea.  
 
**23 julio 2021**  
Me responden solicitud de acceso, indicando que no hay dictamen CJA y remiten a  
[Proyecto de Ley maestra de libertad de elección educativa de la Comunidad de Madrid - comunidad.madrid/transparencia](https://www.comunidad.madrid/transparencia/proyecto-ley-maestra-libertad-eleccion-educativa-comunidad-madrid)  
donde en ese momento no hay todavía nada publicado, salvo apartados.  

Recibo comunicación asociada a reunión 13 abril, lo comento en post sobre ley de transparencia Madrid  
[twitter FiQuiPedia/status/1418522472413122563](https://twitter.com/FiQuiPedia/status/1418522472413122563)  
Comunicación de Consejo de Transparencia y Participación de Madrid (recibida vía correo postal certificado) sobre mi denuncia sobre la reunión de 13 abril 2021 de @eossoriocrespo con lobby @MasPlurales #ReclamadMalditos cc @civio @ConsejoTBG  

#### Tramitación en la Asamblea
**5 septiembre 2021**  
Artículo lo cita  
[Enrique Ossorio: «Las leyes Celaá y Castells aleccionan a los alumnos en mantras de la izquierda» - larazon](https://www.larazon.es/madrid/20210905/neyjjc4wpze6fiim5euasmnlbm.html)  
> –¿Cuándo entrará en vigor la Ley Maestra de Libertad educativa madrileña?  
> –Septiembre es el plazo de enmiendas a la totalidad y parciales. En octubre se constituirá la ponencia y confío que un plazo razonable sería que la ley estuviera en vigor a principios del año 2022.   
 
**16 septiembre 2021**  
Se pueden ver enmiendas a la totalidad  
[PL-1/2021 Proyecto de Ley maestra de libertad de elección educativa de la Comunidad de Madrid, acompañado de certificación del Acuerdo de 7-07-21 y de la documentación anexa que se relaciona](https://www.asambleamadrid.es/actividad/iniciativa?iniciativa=329841)  
 
RGEP12890-21 (16/09/2021) Enmienda a la Totalidad Grupo Unidas Podemos de 16/09/2021
RGEP13398-21 (20/09/2021) Enmienda a la Totalidad Grupo Socialista de 20/09/2021
RGEP13401-21 (20/09/2021) Enmienda a la Totalidad Grupo Más Madrid de 20/09/2021
 
**7 octubre 2021**  
Se debaten enmiendas la totalidad y surgen noticias   
[twitter PPAsamblea/status/1445346660230565889](https://twitter.com/PPAsamblea/status/1445346660230565889)  
La Ley Maestra de Libertad Educativa de la Comunidad de Madrid es fundamental para el futuro de nuestro sistema educativo. 
Queremos garantizar nuestro modelo de éxito y asegurar la libertad de las familias para elegir la educación de sus hijos. 
@SerranoAlfonso  
_incluye vídeo_  

[twitter EducaPodemosCMa/status/1446077340547747841](https://twitter.com/EducaPodemosCMa/status/1446077340547747841)  
Magnífica intervención de ⁦@MorenoG_Agustin⁩, diputado de Unidas Podemos y portavoz en la Comisión de Educación de la Asamblea de Madrid.
Razones para la enmienda a la totalidad de la “Ley Maestra de Libertad de elección educativa” o Ley Ayuso para la segregación educativa 

[Ley Maestra: otra dañina maniobra de Ayuso para erigirse en oposición al Gobierno de España](http://www.feccoo-madrid.org/noticia:603253--Ley_Maestra_otra_danina_maniobra_de_Ayuso_para_erigirse_en_oposicion_al_Gobierno_de_Espana)  
CCOO denuncia que la nueva ley se ha elaborado a espaldas de la comunidad educativa e ignorando las necesidades reales del sistema educativo madrileño  

[Ley Maestra: otra vulneración de la legislación educativa. José Luis Pazos](https://eldiariodelaeducacion.com/2021/10/07/ley-maestra-otra-vulneracion-de-la-legislacion-educativa/)  
Reconozco que me produce cansancio ver como, una y otra vez, la derecha de nuestro país repite el patrón de: no aceptar leyes que provienen de gobiernos de los que no forman parte; tratar de deslegitimar éstas con sus discursos públicos; acudir a los tribunales intentando anularlas mediante el uso, demasiadas veces, solo de argumentos ideológicos sin sustento jurídico; y legislar en otros niveles de gobierno contra lo dispuesto en dichas leyes. Con la denominada Ley Maestra vuelve a suceder lo mismo, en esta ocasión le toca sufrirlo a la LOMLOE   

[ORDEN DEL DÍA DEFINITIVO DE LA SESIÓN PLENARIA DE 7 DE OCTUBRE DE 2021 FIJADO POR LA JUNTA DE PORTAVOCES EN SU REUNIÓN DE 5 DE OCTUBRE](https://www.asambleamadrid.es/static/doc/agenda/DOC1433970.pdf)   

1.2 Expte: PCOP 917(XII)/21 RGEP 14605 Autor/Grupo: Sra. Monasterio San Martín (GPVOX).  
Destinatario: Sra. Presidenta del Gobierno.  
Objeto:  Medidas  que  tienen  previstas  en  la  Ley  Maestra  para  garantizar  la información  a  los  padres  en  los  colegios  sobre  los  contenidos  de  género  a  los niños que establece la LOMLOE.  

2 Proyectos de Ley: Enmiendas a la Totalidad (tramitación acumulada) 2.1 Enmienda a la totalidad, con devolución al Gobierno, presentada por el Grupo  Parlamentario  Unidas  Podemos,  al  Proyecto  de  Ley  PL  1(XII)/21 RGEP 8682, maestra de libertad de elección educativa de la Comunidad de Madrid. (RGEP 12890(XII)/21).  
 
2.2 Enmienda a la totalidad, con devolución al Gobierno, presentada por el Grupo  Parlamentario  Socialista,  al  Proyecto  de  Ley  PL  1(XII)/21  RGEP 8682,  maestra  de  libertad  de  elección  educativa  de  la  Comunidad  de Madrid. (RGEP 13398(XII)/21).  
 
2.3 Enmienda a la totalidad, con devolución al Gobierno, presentada por el Grupo  Parlamentario  Más  Madrid,  al  Proyecto  de  Ley  PL  1(XII)/21  RGEP 8682,  maestra  de  libertad  de  elección  educativa  de  la  Comunidad  de Madrid. (RGEP 13401(XII)/21)

**13 octubre 2021**  
Se ve texto de enmiendas al articulado
[5 octubre enmiendas vox](https://www.asambleamadrid.es/static/docs/registro-ep/RGEP15068-21.pdf)  
[7 octubre enmiendas podemos](https://www.asambleamadrid.es/static/docs/registro-ep/RGEP15601-21.pdf)  
[8 octubre enmiendas MásMadrid](https://www.asambleamadrid.es/static/docs/registro-ep/RGEP15616-21.pdf)  
[8 octubre enmiendas PP](https://www.asambleamadrid.es/static/docs/registro-ep/RGEP15635-21.pdf)  
[8 octubre enmiendas PSOE](https://www.asambleamadrid.es/static/docs/registro-ep/RGEP15655-21.pdf)  

La enmienda nº3 de PP enlaza con [artículo 156 LODE](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978#acincuentayseis) que fue modificado por LOMLOE, y cita cesiones de suelo público.  

**14 octubre 2021**  
Se publican enmiendas en diario de Asamblea
[Boletín Oficial de la Asamblea de Madrid. Número 15. 14 octubre 2021](https://www.asambleamadrid.es/static/doc/publicaciones/BOAM_12_00015.pdf)  

**19 octubre 2021**  
Remisión a la Mesa "Propuesta de Acuerdo de solicitud de informe jurídico en relación con el Proyecto de Ley 1(XII) R.8682, maestra de libertad de elección educativa de la Comunidad de Madrid"  
 
**25 octubre 2021**  
La Mesa, habiéndose celebrado el debate de totalidad del Proyecto de Ley PL 1(XII)/21 RGEP 8682, maestra de libertad de elección educativa de la Comunidad de Madrid en la sesión plenaria celebrada el pasado 7 de octubre, resultando rechazadas las enmiendas a la totalidad presentadas por el Grupo Parlamentario Unidas Podemos (RGEP 12890(XII)/21), por el Grupo Parlamentario Socialista (RGEP 13398(XII)/21) y por el Grupo Parlamentario Más Madrid (RGEP 13401(XII)/21), y habiendo finalizado el plazo de presentación de enmiendas al articulado el pasado 8 de octubre, ACUERDA En aplicación de lo previsto en el artículo 143.1 del Reglamento de la Asamblea, solicitar a los Servicios Jurídicos de la Cámara para que, en el plazo máximo de siete días, emitan informe a los efectos de determinar la corrección técnica del Proyecto de Ley PL 1(XII)/21 RGEP 8682, maestra de libertad de elección educativa de la Comunidad de Madrid , ponderando lo dispuesto en la Constitución Española y en el Estatuto de Autonomía de la Comunidad de Madrid, así como la adecuación competencial de las enmiendas presentadas y la congruencia entre las mismas.

**16 noviembre 2021**  
[Sesión de la Comisión de Educación, Universidades y Ciencia Orden del día](https://www.asambleamadrid.es/static/doc/agenda/DOC1443920.pdf) 
 
Van personas/organismos convocados por partidos para "informar" sobre la ley  
Termina con  
Designación  de  miembros  de  la  Ponencia  para  informar  el  Proyecto  de  Ley  PL  1(XII)/21, maestra de Libertad de Elección Educativa de la Comunidad de Madrid.  
 
Comparecencias sobre la "Ley Maestra de libertad educativa" de l@s portavoces de la ILA y de Asamblea Marea Verde Madrid en la Comisión de Educación de la Asamblea de Madrid a petición del GP Unidas Podemos (16-11-2021) 

[Comparecencias sobre la "Ley Maestra de libertad educativa" de l@s portavoces de la ILA y de Asamblea Marea Verde Madrid en la Comisión de Educación de la Asamblea de Madrid a petición del GP Unidas Podemos (16-11-2021)](https://areaeducacionpodemoscmadrid.blogspot.com/2021/11/comparecencias-sobre-la-ley-maestra-de.html)  
 
[Diario de Sesiones de la Asamblea de Madrid. Número 93. 16 de noviembre de 2021](https://www.asambleamadrid.es/static/doc/publicaciones/XII-DS-93.pdf)  

**7 diciembre 2021**  
[twitter MorenoG_Agustin/status/1468283158625980422](https://twitter.com/MorenoG_Agustin/status/1468283158625980422)  
Atropello antidemocrático de PP y Vox en la Ponencia de la ley "maestra" o ley para la Segregación Educativa de Ayuso. La Ponencia es la primera instancia de debate de enmiendas y búsqueda de acuerdos de la ley  
Abro HILO y recuerdo lo que supone la ley  
Se había acordado por consenso en la sesión anterior que la Ponencia de hoy se dedicaría a la presentación, debate y aprobación en su caso de las enmiendas de los grupos parlamentarios a la ley: 8 enmiendas del PP; 72 de Más Madrid; 15 del PSOE; 30 de Unidas Podemos y 44 de Vox.  
En la tarde de ayer se nos comunicó a través de un grupo de whatsapp que PP y Vox había llegado a un acuerdo que consistía en que Vox presentaba una enmienda transaccional y retiraba 8 enmiendas; el PP votaba 10 enmiendas de Vox y presentaba 26 enmiendas transaccionadas con ellos  
En la reunión se han empezado a debatir las enmiendas y se han resuelto 9 de ellas en, aproximadamente, una hora. A partir de un determinado momento, tanto a PP a Vox y a la presidenta de la Comisión de Educación les han entrado las prisas y han planteado cambiar la metodología..  
... para aprobar a la carrera su acuerdo externo a la Ponencia e impedir el debate de las enmiendas del resto de grupos. Ha habido 3 horas de discusión sobre si podían o no romper el consenso existente sobre la metodología y cambiarla a mitad de sesión de esta reunión de Ponencia  
Ante la falta de acuerdo, PP y Vox han votado dar por cerrada la Ponencia y sus trabajos. Los tres grupos progresistas no hemos participado en la votación, por considerarlo contrario al procedimiento parlamentario que rige en estos casos y porque impide el debate de las enmiendas  
Finalmente, PP y Vox se han empeñado en leer el acuerdo con sus enmiendas transaccionales para votarlas. Pretendía que el resto de grupos votasen 27 enmiendas transaccionales en bloque y no de una en una, y sin poderlas debatir y opinar de algo que empeora más esta lamentable ley  
La actitud del PP y Vox va más allá de aplicar una apisonadora parlamentaria impresentable. Es un atentado al derecho del resto de los grupos parlamentarios, una negación del debate y de la posibilidad de presentar y defender 117 enmiendas elaboradas por los grupos progresistas.  
PP y Vox tienen mayoría para aprobar sus acuerdos. Pero no pueden saltarse los consensos,los procedimientos ni la democracia. El autoritarismo demostrado indica que van a por todas para seguir atentando contra la Educación Pública de Madrid  
Lo querrán repetir con los presupuestos  

[twitter AntonioSD_/status/1468528346871050240](https://twitter.com/AntonioSD_/status/1468528346871050240)  
Me veo obligado a escribir sobre lo que pasó ayer en la Ponencia para la ley Educación de Educación de la Comunidad de Madrid en la Asamblea de todos los madrileños y madrileñas.   
No deja de sorprender cómo el Parlamento pende siempre de un hilo.   
Va hilo.  
En Madrid se está cuajando la Ley de Educación de Ayuso y, en su fase actual, toca "la ponencia", que consiste en encuentros a puerta cerrada con todos los grupos políticos para debatir, comentar y transaccionar -digamos refundir- enmiendas.  
Se tiene una sesión para pactar una metodología para las 136 páginas en enmiendas y se llega a la conclusión de que la mejor opción es leer UNA a UNA las enmiendas. Pero PP y VOX tenían otra cosa preparada para la sesión de trabajo.  
Empieza la sesión normal pero llegada la página 12 de las 136 dicen que quieren cambiar la metodología en mitad de la sesión. Dicen que ellos quieren dar por leídas todas las enmiendas de los grupos de la oposición (UP, PSOE, Más Madrid).  
El problema de ese dar por leídas es que impide "transaccionales" que surjan en el desarrollo de la ponencia. Muchas de ellas pueden ser, incluso, mejoras técnicas.  
A la izquierda tenéis el texto original. A la derecha una propuesta de mejora estilística. Algunos pensamos que una Ley de educación debe estar bien escrita. Pues bien, ni siquiera aceptaron eso. ¿Por qué no aceptar ni siquiera una revisión de este tipo?  
Pues porque no había voluntad alguna de escuchar argumentos. La única voluntad era la de cerrar en media mañana el texto que va a regir el acceso a la educación obligatoria en la Comunidad de Madrid. Respetar el proceso de gestación de la Ley es respetar al pueblo de Madrid.  
Después de esa página deciden, como decía, cambiar la metodología. Ahí se enreda la cosa. Votan cerrar la Ponencia pero después ven que pueden haberse metido en algún lío legal y entonces empiezan a insistir en que la ponencia sigue abierta.  
Proponen -atención- que defendamos cada uno nuestras enmiendas en 5 minutos -¿cómo voy a hablar de 72 enmiendas en 5 minutos? Sale a 4,6 segundos por enmienda si no me equivoco. Igual para PSOE y Unidas Podemos.  
La sesión estaba fijada de 10 a 13:00 -ahí empezaba otra cosa-, pero a las 13:30 siguen insistiendo en cerrar la ponencia votando todo en bloque (después de haber votado ya el cierre de la ponencia y de pronunciar varias veces "la ponencia está cerrada").  
Supongo que su nerviosismo tenía que ver con evitar los líos legales que pueden colegirse de un funcionamiento así. Siento mucha vergüenza y pena por la situación que tuvo que pasar el letrado, al que pusieron en una situación muy difícil.  
Estas sesiones son a puerta cerrada, y ciertamente esto hace valorar más que nunca "la publicidad de las sesiones", principio ilustrado donde los haya.  
Más allá de la "broma" de que no aceptaran ni correcciones de estilo, tanto PSOE, como UP como nosotros  -también el propio PP y VOX- presentamos todo un conjunto de enmiendas que merecían su debate. Ni siquiera eran "nuestras enmiendas" o no eran estrictamente nuestras.  
Las enmiendas son también síntesis de muchas propuestas hechas desde la Sociedad civil. La ley ahora es una ley peor, no solo porque esté mal escrita; si no porque se deja fuera aspectos fundamentales para una ley de educación: garantizar la gratuidad de lo público.  
Dejo aquí el artículo 27 de la Constitución. Pedí la inclusión del mismo -completo- como pórtico para la Ley de educación. Estuvieron en contra. Lo prefieren troceado para poder seguir privatizando la obligatoriedad.  
Otros ejemplos de enmiendas. Garantizar plazas públicas en los diferentes niveles de enseñanzas (esto es, en realidad, otra forma de pedir el cumplimiento de la constitución).  
¿Quisieron debatirlo? No.  
¿Garantizar plazas gratuitas en Madrid dentro de la red educativa pública? Nada, prefieren seguir favoreciendo conciertos que institucionalizan el copago. Primero pagamos con nuestros impuestos la educación concertada y luego mediante cuotas ilegales.  
¿Quisieron debatirlo? No.  
 
**18 enero 2022**  
[La Ley Maestra: una apuesta por la segregación y un atropello parlamentario](https://eldiariodelaeducacion.com/2022/01/18/la-ley-maestra-una-apuesta-por-la-segregacion-y-un-atropello-parlamentario/)  
 
**1 febrero 2022**  
[Manifiesto «Contra la Ley maestra de libertad de elección educativa” (La Marea Verde)](https://laeducacionpublica.es/manifiesto-contra-la-ley-maestra-de-libertad-de-eleccion-educativa-la-marea-verde/)  

**3 febrero 2022**  
[ORDEN DEL DÍA DEFINITIVO DE LA SESIÓN PLENARIA DE 3 DE FEBRERO DE 2022 FIJADO POR LA JUNTA DE PORTAVOCES EN SU REUNIÓN DE 1 DE FEBRERO](https://www.asambleamadrid.es/static/doc/agenda/DOC1466126.pdf)  
 
2 Proyectos de Ley: Dictámenes de Comisión  
2.1 Dictamen de la Comisión de Educación, Universidades y Ciencia sobre el Proyecto de Ley PL 1(XII)/21 RGEP 8682, Maestra de libertad de elección educativa de la Comunidad de Madrid. (RGEP 20306(XII)/21).  
- Escrito de reserva de enmiendas del Grupo Parlamentario Más Madrid (RGEP 20308(XII)/21-RGEP 20418(XII)/21) sobre el Proyecto de Ley PL
1(XII)/21 RGEP 8682, Maestra de libertad de elección educativa de la Comunidad de Madrid.  
- Escrito de reserva de enmiendas del Grupo Parlamentario Unidas Podemos (RGEP 20325(XII)/21) sobre el Proyecto de Ley PL 1(XII)/21 RGEP 8682, Maestra de libertad de elección educativa de la Comunidad de Madrid.  
- Escrito de reserva de enmiendas del Grupo Parlamentario Socialista (RGEP 20392(XII)/21) sobre el Proyecto de Ley PL 1(XII)/21 RGEP 8682, Maestra de libertad de elección educativa de la Comunidad de Madrid.  
 
[twitter IdiazAyuso/status/1489289131326947332](https://twitter.com/IdiazAyuso/status/1489289131326947332)  
Aprobamos la Ley Maestra de Libertad Educativa para proteger la elección de las familias, blindamos la concertada y la especial y garantizamos la calidad frente al aprobado general del Gobierno socialista.  
Libertad, calidad e igualdad de oportunidades para los alumnos.  
 
[twittereducacmadrid/status/1489294632001363969](https://twitter.com/educacmadrid/status/1489294632001363969)  
La Ley Maestra refuerza un modelo de enseñanza que:  
Protege a los estudiantes con necesidades especiales.  
Garantiza la enseñanzas en centros públicos y gratuito.  
Apuesta por el esfuerzo y la excelencia.   
 
 
[La Comunidad de Madrid refuerza la calidad de su sistema educativo con la aprobación de la Ley Maestra - comunidad.madrid](https://www.comunidad.madrid/noticias/2022/02/03/comunidad-madrid-refuerza-calidad-su-sistema-educativo-aprobacion-ley-maestra)  
La Comunidad de Madrid refuerza y aumenta la calidad de su sistema educativo con la aprobación, hoy en la Asamblea regional, de la nueva Ley Maestra de Libertad de Elección Educativa. La normativa entrará en vigor en los próximos días tras su publicación en el Boletín Oficial de la Comunidad de Madrid –BOCM- y estará operativa para este curso escolar 2021/22. El texto validado en votación por la Cámara legislativa autonómica garantiza los pilares básicos del sistema educativo madrileño frente a la amenaza que supone la implantación de la estatal LOMLOE (Ley Orgánica de Modificación de la LOE).
 
[twitter ComunidadMadrid/status/1489305597724008453](https://twitter.com/ComunidadMadrid/status/1489305597724008453)  
La @asambleamadrid ha aprobado la nueva Ley Maestra de Libertad de Elección Educativa.   
Reforzamos la calidad del sistema educativo de la región, con un modelo basado en la libertad, la igualdad y el esfuerzo académico.  
_Incluye vídeo de Ossorio_  

[twitter ComunidadMadrid/status/1489309863788298241](https://twitter.com/ComunidadMadrid/status/1489309863788298241)  
La nueva Ley Maestra garantiza los pilares básicos del sistema educativo madrileño:  
Protege la elección de las familias.  
Blinda la educación concertada y la educación especial.  
Apuesta por la calidad y la cultura del esfuerzo.  

**8 febrero 2022**  
[Comunicación aprobación ley](https://www.asambleamadrid.es/static/docs/registro-sp/RGSP387-22.pdf)  
 
**15 febrero 2022**  
Se publica en BOCM  
[Ley 1/2022, de 10 de febrero, Maestra de Libertad de Elección Educativa de la Comunidad de Madrid]( https://www.bocm.es/boletin/CM_Orden_BOCM/2022/02/15/BOCM-20220215-1.PDF)   
 
**27 abril 2022**  
Se publica en BOE  
[Ley 1/2022, de 10 de febrero, Maestra de Libertad de Elección Educativa de la Comunidad de Madrid.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2022-6768)
 
### Efectos de la ley
Una vez aprobada, intento recoger aquí ideas de los efectos.  
Lo más inmediato es que veo que habrá nuevas cesiones de suelo público   
Ver post [Centros privados con concierto en diferido: inventario Madrid](https://algoquedaquedecir.blogspot.com/2021/03/centros-privados-con-concierto-en-diferido-inventario-madrid.html) y post [El ayuntamiento de Madrid crea centros privados con concierto en diferido](https://algoquedaquedecir.blogspot.com/2021/03/centros-privados-con-concierto-en-diferido-inventario-madrid.html):  en febrero 2022 pregunto de nuevo por la cesión anunciada por ayuntamiento Madrid en diciembre 2020 y me indican que no está firmada por la consejería.
 
**15 febrero 2022**  
[twitter FiQuiPedia/status/1494456352504176641](https://twitter.com/FiQuiPedia/status/1494456352504176641)  
Mismo día 15 se publica ley para cesión suelo público para #ConcertadosEnDiferido, @MCLA73 en Comisión de Educación respondiendo a @MorenoG_Agustin  confirma "necesidad de un nuevo centro y creemos que ese nuevo centro tiene que ser un centro concertado"

**24 febrero 2022**  
[La Comunidad de Madrid garantiza las plazas necesarias en centros educativos públicos y concertados con la Ley Maestra regional - comunidad.madrid](https://www.comunidad.madrid/notas-prensa/2022/02/24/comunidad-madrid-garantiza-plazas-necesarias-centros-educativos-publicos-concertados-ley-maestra-regional)  
 
###  Análisis de la ley
Hasta que no se publique el anteproyecto, solo se pueden analizar ideas generales, que pueden servir para realizar alegaciones llegado el momento del trámite de audiencia.  
Una vez publicado el trámite de audiencia el 5 marzo 2021 con el anteproyecto de ley, utilizo algunos de estos puntos para ir planteando las alegaciones.  
 
Si encuentro análisis en otros blog / prensa los intento enlazar
 
**23 abril 2021**  
[Ley Maestra: la libertad de elección según el gobierno de Ayuso](https://www.elsaltodiario.com/educacion-publica/ley-maestra-la-libertad-de-eleccion-segun-el-gobierno-de-ayuso)  
La propuesta legislativa estrella del gobierno de Madrid en materia de educación apuesta por blindar los centros concertados en la región, distanciándose de la ley estatal y saltándose el debate en la Asamblea. 

#### No existen centros que sean concertados, son centros privados que tienen unidades concertadas
Ver post [No existe ningún centro que sea concertado](https://algoquedaquedecir.blogspot.com/2018/02/no-existe-ningun-centro-que-sea-concertado.html)  
 
10 marzo 2021 escribo "alegación 1: clasificación de centros". Cito artículo 7.3 que es para una alegación separad por sí solo, pero intento poner una alegación por cada tema.

---

En todo el texto del anteproyecto aparece una única vez "centros concertados" sin hacer referencia explícita a privados. Se trata de artículo 7.3 "La Comunidad de Madrid podrá convocar concursos públicos para la construcción y gestión de centros concertados sobre suelo público dotacional."

Creo que en ese caso habría que rectificar e indicar "centros PRIVADOS concertados" como se ha hecho en el resto del texto: en el anteproyecto aparece 6 veces "privados concertados", 1 vez "privado concertado", y explicita en la exposición de motivos "doble red de centros escolares, públicos y privados, mediante un sistema de conciertos"  
Aparece 13 veces "centros privados", haciendo referencia algunas solo a centros privados sin unidades concertadas y otras a centros con unidades concertadas.  
El [artículo 108 de LOE consolidada](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a108) clasifica los centros en públicos y privados, y aclara que los privados pueden estar acogidos al régimen de conciertos.  
Considero un error que en cada una de las menciones a centros privados no se aclare si se hace referencia a centros privados acogidos o no a régimen de conciertos, y creo que se debería añadir.  
 
Por ejemplo disposiciones adicionales tercera y cuarta en su título no aclaran a qué tipo de centro privado hacen referencia, aunque en su contenido sí se aclara en la tercera, pero no en la cuarta.  

Como sugerencia global, propongo que en lugar de "privados concertados", se use "privados total o parcialmente concertados", tal y como hace normativa en BOE, artículo 7 Ley 22/1993](https://www.boe.es/buscar/act.php?id=BOE-A-1993-31153#a7), ya que no se conciertan centros, sino unidades: un mismo centro privado puede tener unidades concertadas en ESO y no tener unidades concertadas en Bachillerato.  

---

09/371569.9/21

#### ¿Puede una ley autonómica contradecir una ley estatal?
La respuesta no es ni sí ni no: depende.  
No importa si la ley estatal es orgánica o no, lo que importa es si es normativa básica. Ver post [Lo básico de normativa básica](https://www.boe.es/buscar/act.php?id=BOE-A-1993-31153#a7)  
Y no toda una ley es normativa básica, solo ciertos artículos  
En el caso de la LOE, los artículos básicos, que no pueden modificar las CCAA, están en [Disposición final quinta. Título competencial](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta)  

> 1. La presente Ley se dicta con carácter básico al amparo de la competencia que corresponde al Estado conforme al artículo 149.1.1.ª, 18.ª y 30.ª de la Constitución. Se exceptúan del referido carácter básico los siguientes preceptos: artículos 7; 8.1 y 8.3; 9; 11.1 y 11.3; 12.4; 14.6; 15.3; 18.4; 18.5; 22.8; 24.6; 24.7; 26.1; 26.2; 35; 42.3; 47; 58.4, 58.5, 58.6, 58.7 y 58.8; 60.3 y 60.4; 66.2 y 66.4; 67.2, 67.3, 67.6, 67.7 y 67.8; 68.3; 72.4 y 72.5; 89; 90; 100.3; 101; 102.3 y 102.4; 103.1; 105.2; 106.2 y 106.3; 111 bis.4; 112.2, 112.4; 112.5 y 112.6; 113.3 y 113.4; 122.2 y 122.3; 122 bis; 123.2, 123.3, 123.4 y 123.5; 124.1, 124.2 y 124.4; 125; 130.1; 131.2 y 131.5; 144.3; 145; 146; 147.2; 154; disposición adicional decimoquinta, apartados 1, 4, 5 y 7; disposición adicional trigésima cuarta, disposición adicional cuadragésima, disposición adicional cuadragésima primera, disposición final tercera y disposición final cuarta.

> 2. Los artículos 30.4; 31.1 y 2; 37; 39.6, primer inciso; 41.2 y 3; 44.1, 2 y 3; 50; 53; 54.2 y 3; 55.2 y 3; 56; 57.2, 3 y 4; 65, se dictan al amparo de la competencia exclusiva del Estado sobre la regulación de las condiciones de obtención, expedición y homologación de títulos académicos y profesionales.
 
Por ejemplo tras LOMCE artículo 116 y su punto 8 eran básicos, pero al suprimir LOMLOE punto 8, no se puede añadir en normativa autonómica.  

#### Cesión de suelo público para crear centros concertados en diferido 
Es algo que se hizo legal con LOMCE, pero que Madrid hacía ilegalmente antes.  
Un concierto se pide por un centro privado existente para ciertas unidades, pero aquí se está dando el concierto a todo el centro antes de existir.  
Ver post [Madrid crea centros privados con concierto en diferido](https://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html)  

Artículo 116.8 desaparece en LOMLOE y deja de permitirlo.  
Es significativo que se cite en anuncio de 19 enero 2021, y en artículo de 14 enero 2021 como "perjuicio"  
[twitter FiQuiPedia/status/1349493874109112321](https://twitter.com/FiQuiPedia/status/1349493874109112321)  
La web de @ComunidadMadrid se ha convertido en un panfleto de @ppmadrid.  
El "lapsus" de poner "sistema madrileño" omitiendo poner "educativo" y citar como perjuicio eliminar ceder suelo a privados creo que es un buen resumen.  

13 enero 2021   
[Elevamos a 10 años la duración de conciertos educativos y garantizamos la libertad de elección - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/01/13/elevamos-10-anos-duracion-conciertos-educativos-garantizamos-libertad-eleccion)  
> Perjuicios de la LOMLOE al sistema madrileño  
> Esta medida ha sido adoptada ante la disconformidad del Ejecutivo regional tras la decisión del Gobierno de la nación de reducir el papel de la educación concertada como una red subsidiaria y no complementaria de la pública, **eliminando** el concepto de la demanda social, **la cesión de terrenos** y la educación diferenciada. En la actualidad, en la Comunidad de Madrid, el 29% de las familias ha optado este curso escolar por educación concertada, lo que se traduce en más de 360.000 alumnos.  
![](https://pbs.twimg.com/media/ErpdhovXUAIMLxb?format=jpg)   

[twitter FiQuiPedia/status/1351554616710852612](https://twitter.com/FiQuiPedia/status/1351554616710852612)  
El empeño de @ppmadrid @IdiazAyuso en la cesión de suelo público a empresas privadas para luego garantizarles el concierto educativo es significativo
19 enero 2021  
[Díaz Ayuso garantiza la libertad, la calidad de la enseñanza, la educación especial y el español](https://www.comunidad.madrid/noticias/2021/01/19/diaz-ayuso-garantiza-libertad-calidad-ensenanza-educacion-especial-espanol)  
> Asimismo, garantizará la existencia de plazas suficientes en las enseñanzas declaradas como obligatorias y gratuitas, teniendo en cuenta la oferta de plazas escolares de los centros sostenidos con fondos públicos y la demanda social, concepto que ha quedado excluido en la LOMLOE. **La Comunidad podrá también convocar concursos públicos para la construcción y gestión de centros concertados sobre suelo público dotacional.**
![](https://pbs.twimg.com/media/EsGvws3WMAAFpIE?format=jpg)  


**10 marzo 2021**  
Escribo alegación 2: imposibilidad concursos públicos para construcción de centro privado concertado, ya que supone incumplir normativa sobre acceso a régimen de conciertos.

---

Artículo 7.3 se indica "La Comunidad de Madrid podrá convocar concursos públicos para la construcción y gestión de centros concertados sobre suelo público dotacional."  

Una vez aclarado que según artículo 108 LOE se trata de "centros privados concertados", es imposible construir un "centro privado concertado", ya que la normativa para acceder al régimen de conciertos exige primero que exista un centro privado, y luego solicitar que se concierten unidades.  

El proceso requiere obligatoriamente dos pasos: 

\1. Cesión de suelo público a entidad privada.  

\2. Un vez construido el centro educativo, solicitud del titular del centro privado de acceso al régimen de conciertos y concesión del concierto por la administración si se cumplen los requisitos.  

El texto pretende garantizar que se llega al final del paso 2 omitiendo que se debe pasar por el 1 con una entidad privada, y por ello habla de "centros concertados" cuando pretende hacer referencia a "centros PRIVADOS concertados", hablando de un posible concierto a futuro.  

Cito esta referencia   

[Revista catalana de derecho público Núm. 51, Diciembre 2015. La cesión de suelo público dotacional para la apertura de centros docentes concertados: una nueva manifestación del estado garante](https://libros-revistas-derecho.vlex.es/vid/cesion-suelo-publico-dotacional-594075354)  

> En definitiva, al no admitir la legislación básica \[53] el otorgamiento directo de conciertos, ni siquiera a quienes compitiendo con otros licitadores hubieran resultado adjudicatarios de suelo público, las CC. AA. carecían de margen para concertar automáticamente las solicitudes de los cesionarios de suelo público.\[54] A estos efectos era irrelevante que en la licitación se hubieran valorado los aspectos educativos del proyecto o tenido en cuenta los criterios de preferencia del artículo 116.2 LOE para el otorgamiento de conciertos.
> \[53] Fundamentalmente la LOE, LODE y el Real Decreto 2377/1985, de 18 de diciembre, por el que se aprueba el Reglamento de Normas Básicas sobre Conciertos Educativos. 
> \[54] De acuerdo con el artículo 116.3 LOE, compete al Gobierno regular los aspectos básicos del concierto, entre los que se encuentran la tramitación de las solicitudes. Según el reglamento de desarrollo de la LODE, estas deben presentarse en el mes de enero. Las resoluciones de aprobación o denegación tendrán que haber recaído antes del 15 de abril. Si no existen fondos suficientes, habrá que acudir a los criterios de preferencia del artículo 116.2 LOE. Por este motivo es dudosa la legitimidad de la adjudicación de conciertos educativos directamente y al margen de los criterios de preferencia de la LOE.

Se cita que para acogerse al régimen de conciertos hay que presenar una solicitud en enero   
[Artículo 19. Real Decreto 2377/1985](https://www.boe.es/buscar/act.php?id=BOE-A-1985-26788#a19)  
\1. Los centros privados que, cumplimentando lo dispuesto en el artículo quinto de este reglamento, deseen acogerse al régimen de conciertos a partir de un determinado curso académico, lo solicitarán de la Administración educativa competente durante el mes de enero anterior al comienzo de dicho curso.  

A su vez cita [artículo 5](https://www.boe.es/buscar/act.php?id=BOE-A-1985-26788#a5)  
\1. Para poder acogerse al régimen de conciertos los centros privados deberán cumplir los requisitos mínimos que se establezcan en desarrollo del articulo 14 de la ley orgánica reguladora del Derecho a la Educación, estar autorizados para impartir las enseñanzas que constituyen el objeto del concierto, someterse a las normas establecidas en el título IV de dicha ley orgánica y asumir las obligaciones derivadas del concierto en los términos establecidos en este reglamento.  

Que a su vez cita [artículo 14 de LODE](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978#acatorce)  
\1. Todos los centros docentes deberán reunir unos requisitos mínimos para impartir las enseñanzas con garantía de calidad. El Gobierno establecerá reglamentariamente dichos requisitos mínimos.  
\2. Los requisitos mínimos se referirán a titulación académica del profesorado, relación numérica alumno-profesor, instalaciones docentes y deportivas y número de puestos escolares.  

Se puede leer el artículado de  Real Decreto 2377/1985 viendo que se hace  referencia a centros privados existentes y a sus titulares.

Mientras el centro esté construido y no exista, no puede estar autorizado ni cumplir ningún requisito, por lo que no se puede garantizar el acceso al régimen de concierto, porque por ejemplo no se puede saber a priori en qué momento va a estar construido el centro.

--- 

09/371581.9/21


#### Conciertos con centros privados que segregan por sexo
Me remito a post [Educación separada por sexos: conciertos, legalidad y LOMCE](https://algoquedaquedecir.blogspot.com/2017/10/educacion-separada-por-sexos-conciertos.html)  
 
Es algo que no anuncian inicialmente pero aparece en el texto del anteproyecto:
 
---
Alegación 3: conciertos con centros privados que segregan por sexo
 
En exposición de motivos III se indica  

"Ahora bien, también es necesario recordar que, conforme al criterio establecido
por el Tribunal Constitucional, no se considera discriminación la admisión de
alumnos o la organización de la enseñanza diferenciadas por sexos, siempre
que la enseñanza que impartan se desarrolle conforme a lo dispuesto en el
artículo 2 de la Convención relativa a la lucha contra las discriminaciones en la
esfera de la enseñanza, aprobada por la Conferencia General de la UNESCO el
14 de diciembre de 1960."

y en artículo 5.1.b se indica 

"No constituye discriminación la admisión de alumnos o la organización de la
enseñanza diferenciadas por sexos, siempre que la enseñanza que impartan se
desarrolle conforme a lo dispuesto en el artículo 2 de la Convención relativa a la
lucha contra las discriminaciones en la esfera de la enseñanza, aprobada por la
Conferencia General de la UNESCO el 14 de diciembre de 1960."

No se concreta el criterio del TC, se asume que hace referencia a Sentencia 31/2018, que se hizo sobre LOMCE, ya derogada, que añadió en [artículo 84.3](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a84) un texto similar al que indica el anteproyecto de Madrid, y que en LOMLOE ha sido derogado, siendo su redacción actual
 
"3. En ningún caso habrá discriminación por razón de nacimiento, origen racial o étnico, sexo, religión, opinión, discapacidad, edad, enfermedad, orientación sexual o identidad de género o cualquier otra condición o circunstancia personal o social"

Dado que ese artículo 84 indica actualmente "EN NINGÚN CASO ... SEXO" y es normativa básica según [disposición final quinta](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta) una administración autonómica no puede incumplirlo, y esa parte del anteproyecto debe eliminarse.
 
Respecto a [convención de UNESCO citada](http://portal.unesco.org/es/ev.php-URL_ID=12949&URL_DO=DO_TOPIC&URL_SECTION=201.html) se cita artículo 2 sin poner en contexto de 1960 el artículo 1, que indica  

"1. A los efectos de la presente Convención, se entiende por “discriminación” toda distinción, exclusión, limitación o preferencia, fundada en la raza, el color, el SEXO,..."

y artículo 2 indica 

"En el caso de que el Estado las admita, las situaciones siguientes no serán consideradas como constitutivas de discriminación en el sentido del artículo 1 de la presente Convención:  
a. La creación o el mantenimiento de sistemas o establecimientos de enseñanza separados para los alumnos de sexo masculino y para los de sexo femenino, ..."

Es decir, no se considera discriminación solamente en caso del que el ESTADO LAS ADMITA.  
Por ponerlo en contexto, algunos países firmantes en 1960 podían tener ya educación diferenciada, y se dejaba flexibilidad para que el país pudiera firmar el convenio sin convertir en ilegal lo que estaba haciendo. De hecho España era un caso en ese momento:  
Ley de 26 de febrero de 1953 sobre Ordenación de la Enseñanza Media.  
En los Institutos mixtos la enseñanza y la educación se dará por separado a alumnos y alumnas   

A nivel de normativa no es lo mismo admitir (tiene que haber una normativa que diga que se admite) que no prohibir (que es simplemente que no haya normativa o no la haya que lo prohíba, por lo que es algo no contemplado y ni se admite ni se prohíbe).  
Decir que como no se prohíbe se admite es una variante de falacia ad silentio. El que alguien no diga algo no puede interpretarse como a favor o en contra (a no ser que la norma recoja ese silencio, como ocurre en el caso del silencio administrativo).  
Esto no es una interpretación, ya que en LOMCE se modificó texto para poner explícitamente que sí se admitía, y la sentencia TC de 2018 está basada en ese texto.  

Ni la modificación de LOMLOE ni ninguna normativa está cuestionando de que centros privados sin concierto puedan tener educación diferenciada por sexo; lo que se está cuestionando y por lo que se está intentando legislar (LOE no permitía segregar por sexo, LOMCE sí, LOMLOE no, anteproyecto sí)  es recibir fondos públicos vía concierto a centros que sí segregan por sexo. Y el convenio de 1960, ratificado por España, menciona algo al respecto:

Artículo 3 indica "d. No admitir, en la ayuda, cualquiera que sea la forma que los poderes públicos puedan prestar a los establecimientos de enseñanza, ninguna preferencia ni restricción fundadas únicamente en el hecho de que los alumnos pertenezcan a un grupo determinado; "

--- 

09/379700.9/21

#### Libertad de elección
Me remito a post [#LibertadDeEnseñanza](https://algoquedaquedecir.blogspot.com/2019/10/libertaddeensenanza.html) y a post [Datos segración educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-segregacion-educacion.html)  
 
#### Demanda social
Me remito a post [Plazas públicas y demanda social](https://algoquedaquedecir.blogspot.com/2020/07/datos-segregacion-educacion.html) y a post [Datos segración educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-segregacion-educacion.html)  
 
---
Alegación 5: demanda social
 
En artículo 7.2 se indica "2. La Comunidad de Madrid garantizará en cualquier caso la existencia de plazas suficientes para las enseñanzas declaradas gratuitas por la ley, teniendo encuenta la oferta existente de centros públicos y privados concertados y la demanda social, así como las consignaciones presupuestarias y el principio de economía y eficiencia en el uso de los recursos públicos."

Se incluye el término "demanda social" que LOMCE introdujo en artículo 109, que actualmente LOMLOE ha eliminado de dicho artículo, siendo ese artículo básico según [disposición final quinta](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta)  

Considero que la inclusión del término "demanda social" supone contravenir normativa básica en la que explícitamente se ha eliminado.

Además la demanda social es un concepto opaco, ya que no existe una información pública y transparente de las solicitudes para valorar esa demanda, ni una definición oficial de esa demanda

En "Resolución de la Dirección General de Educación Concertada, Becas y Ayudas al Estudio por la que se dictan Instrucciones para la tramitación de las solicitudes de los conciertos educativos para el curso 2021-2022", CSV 0907652447585169592379  se cita "
 
"-Los datos de escolarización de los centros en el curso 2020/21 cumplimentados en la aplicación RAICES, (o en SICEP, en el caso de las enseñanzas de formación profesional) y la previsible proyección de los mismos en el curso 2021/22.  
-La demanda previsible de escolarización." 
 
La consideración de "demanda social" sin una concreción clara es subjetiva, y no tengo constancia de ningún documento donde esté definida ni en el que se contemple
 
---
09/511615.9/21
 
#### Español como lengua vehicular
Es algo comentado en el desarrollo de LOMLOE, ver post [LOABLE reversión LOMCE](https://algoquedaquedecir.blogspot.com/2018/11/loable-reversion-lomce.html)  
En su momento muchos dijeron que recurrirían eso al TC: está por ver que lo hagan y qué argumentan.  
[twitter FiQuiPedia/status/1327687962017091584](https://twitter.com/FiQuiPedia/status/1327687962017091584)  
\1. "El castellano desaparece como lengua vehicular"  
LOE: no existía  
LOMCE: lo introduce [dispocición adicional trigésimo octava](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#datrigesimaoctava)  
[Sentencia TC 14/2018](https://boe.es/buscar/doc.php?id=BOE-A-2018-4146) anuló parte texto LOMCE  
LOMLOE:[Informe ponencia congreso](https://www.congreso.es/backoffice_doc/prensa/notas_prensa/78068_1605292068375.pdf#page=101)  
¿llevar frase "de conformidad con la Constitución Española" a TC?  
![](https://pbs.twimg.com/media/EmzkZ7cXcAMQFLD?format=png)  
 
Llama la atención es que se hable de una ley educativa en Madrid, que no tiene lengua cooficial, que trata del español como lengua vehicular. Eso hace que se comente si ese desarrollo va a afectar al "bilingüismo" madrileño.  
[twitter FiQuiPedia/status/1351556738303066112](https://twitter.com/FiQuiPedia/status/1351556738303066112)  
El anuncio de una ley que garantizará en @ComunidadMadrid el "Español como lengua vehicular" supone que @IdiazAyuso y @eossoriocrespo deben realizar un trámite de audiencia que permita alegaciones para acabar con el actual "bilingüismo"  

**27 febrero 2021**  
[Bilingüismo: sobresaliente en segregación escolar](https://www.elsaltodiario.com/educacion/bilinguismo-suspenso-en-ingles-notable-en-segregacion)  
La presidenta de la Comunidad de Madrid, Isabel Díaz Ayuso, presentó un proyecto de ley educativa, llamado Ley Maestra en el que se apuntaba a garantizar a las y los estudiantes madrileños el castellano como lengua vehicular. La propuesta la puso sobre la mesa al hilo de las polémicas en torno al uso de las lenguas del Estado diferentes al castellano, sin embargo, la mandataria parecía olvidar el hecho de que desde hace 16 años en su comunidad autónoma son varias las materias que se estudian en un idioma diferente al del alumnado. 
 
---
Alegación 4: castellano como lengua vehicular
 
En artículo 5.1.c se indica "Derecho a recibir las enseñanzas en castellano. Se garantiza el derecho de los alumnos a recibir las enseñanzas en castellano, como lengua oficial y vehicular de España, de manera que, al finalizar la educación básica, comprendan y se expresen, de forma oral y por escrito, en esta lengua."
 
Ese texto puede parecer que carece de sentido en una comunidad autónoma que no tiene lengua cooficial y en la que en principio todas las enseñanzas se imparten en castellano.
Sin embargo, en el sistema educativo madrileño hay implementado un programa de bilingüismo que no garantiza ese derecho.  
Por ejemplo en [ORDEN 972/2017, de 7 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se regulan los institutos bilingües español-inglés de la Comunidad de Madrid](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/04/27/BOCM-20170427-11.PDF)  
Para Programa Bilingüe solo se exige un mínimo de una materia en inglés, que incluso puede no impartirse (artículo 5.4)  
Para Sección Bilingüe se exige al menos un tercio del horario lectivo semanal, pudiendo ser todas las materias salvo algunas excepciones (artículo 6).  

Además el derecho que se pretende reclamar está recogido en normativa básica  
[Disposición adicional trigésima octava. Lengua castellana, lenguas cooficiales y lenguas que gocen de protección legal.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#datrigesimaoctava)  
[Disposición final quinta. Título competencial.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dfquinta)  
\1. Las Administraciones educativas garantizarán el derecho de los alumnos y las alumnas a recibir enseñanzas en castellano y en las demás lenguas cooficiales en sus respectivos territorios, de conformidad con la Constitución Española, los Estatutos de Autonomía y la normativa aplicable.  

\2. Al finalizar la educación básica, todos los alumnos y alumnas deberán alcanzar el dominio pleno y equivalente en la lengua castellana y, en su caso, en la lengua cooficial correspondiente.  

Considero que el articulado debe mencionar el pograma bilingüe para garantizar el derecho a recibir las enseñanzas en castellano.

---
09/511599.9/21

**5 septiembre 2021** 
[Enrique Ossorio: «Las leyes Celaá y Castells aleccionan a los alumnos en mantras de la izquierda» - larazon](https://www.larazon.es/madrid/20210905/neyjjc4wpze6fiim5euasmnlbm.html)  
> –¿Y qué sentido tiene en Madrid que se fije por ley que el castellano sea vehicular en la educación?  
> –Primero es reafirmar ese concepto y dejar claro que no estamos de acuerdo con esa ausencia de la ley orgánica estatal; también estamos pensando en la posibilidad de llegar a acuerdos con otras comunidades autónomas y poner en común repositorios de textos en castellano para colaborar con otros centros educativos de otras comunidades en esa materia. Va a tener una aplicación práctica.

### Otras leyes de "libertad educativa"

[La ley valenciana de 'libertad' educativa de PP y Vox, una norma “innecesaria, discriminatoria y regresiva” ](https://www.eldiario.es/comunitat-valenciana/politica/ley-valenciana-libertad-educativa-pp-vox-norma-innecesaria-discriminatoria-regresiva_1_11368330.html)  

[https://www.eldiario.es/comunitat-valenciana/politica/pp-libertad-educativa-razones-huelga-general-ensenanza-publica-valenciana_1_11386334.html](https://www.eldiario.es/comunitat-valenciana/politica/pp-libertad-educativa-razones-huelga-general-ensenanza-publica-valenciana_1_11386334.html)
