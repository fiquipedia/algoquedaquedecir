+++
author = "Enrique García"
title = "Coste dinero público de un privado con concierto"
date = "2024-04-22"
tags = [
    "educación", "Madrid", "transparencia", "coste", "privados con concierto", "aulas de enlace"
]
toc = true

+++

Revisado 4 septiembre 2024

### Resumen

Se puede querer saber el coste en dinero público de un centro privado con concierto: hay que saber qué información es pública y cómo se puede consultar y utilizar. Intento comentar aquí cómo averiguarlo y dónde mirar o cómo reclamar. 

Posts relacionados:

* [Datos módulos de conciertos educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-modulos-de-conciertos-educacion.html)  
* [Privados con concierto en etapas no obligatorias: lo singular como normal](http://algoquedaquedecir.blogspot.com/2017/08/privados-con-concierto-en-etapas-no.html)  
* [Profesorado religiosamente pagado con dinero público](https://algoquedaquedecir.blogspot.com/2020/02/profesorado-religiosamente-pagado-con-dinero-publico.html)  
* [Cheque Bachillerato Madrid](http://algoquedaquedecir.blogspot.com/2018/10/cheque-bachillerato-madrid.html)  
* [Datos gasto en educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-gasto-en-educacion.html)  
* [Ratios en educación concertada](https://algoquedaquedecir.blogspot.com/2020/01/ratios-en-educacion-concertada.html)

## Detalle

### Dinero vía módulos de concierto
La idea es que los centros privados con concierto reciben un dinero público fijado en normativa: el importe varía según cada administración. Se puede mirar [Datos módulos de conciertos educación](https://algoquedaquedecir.blogspot.com/2020/07/datos-modulos-de-conciertos-educacion.html) 

Resumiendo mucho, reciben por las unidades concertadas y por gastos. El coste en dinero público del centro incluye el dinero que se paga a los docentes de los centros privados con concierto, aunque al ser vía pago delegado que se da al trabajador se puede alegar que no es dinero que se da al centro; los que suelen alegar eso son los que al tiempo suelen sumar el salario de docentes en el coste de los centros  públicos.  

### Ejemplo
Poniendo un ejemplo, queremos saber cuánto dinero público reciben Tajamar y Los Tilos de Madrid. Miramos en abril 2024 sobre 2023 

Es una aproximación, no sé cómo de correcta es, pero está reflejado cómo se ha hecho y eso permite revisarla

### Paso 1 Buscar código centro y sus datos  

Vamos al [buscador de centros](https://gestiona.comunidad.madrid/wpad_pub/run/j/MostrarConsultaGeneral.icm) y consultamos su código de centro

Tajamar: 28011908, Privado concertado. 

Los Tilos: 28067392, Privado concertado. 

Ambos en detalle indican

>Segundo Ciclo Educación Infantil (LOMLOE) 	Concertada   
Educación Primaria 	Concertada   
Educación Secundaria Obligatoria (LOMLOE) 	Concertada   
Ed. Sec. Postobligatoria Bachillerato (LOMLOE) 	
   Ciencias y Tecnología 	Concertada   
   Humanidades y Ciencias Sociales 	Concertada  
   
   
En el caso del Tajamar además

>Formación Profesional de grado medio  
INFORMATICA Y COMUNICACIONES (LOE)  
Sistemas Microinformáticos y Redes (LOE) 	Concertada   	presencial  /  diurno 	
ARTES GRÁFICAS 	 
Preimpresión Digital (LOE) 	Concertada   	presencial  /  diurno 	 
Impresión Gráfica (LOE) 	Concertada   	presencial  /  diurno    

### Paso 2 Buscar número de unidades concertadas  

Buscamos cuántas unidades concertadas tiene cada uno de esos centros. (El detalle anterior orienta en qué mirar)

Se puede mirar en [Dirección General de Educación Concertada, Becas y Ayudas al Estudio >CONCIERTOS](https://site.educa.madrid.org/dg.concertada-becasyayudas/index.php/conciertos/)

No interesa normativa, sino "aprobación de conciertos" y "modificaciones"

En abril 2024 se cita [Orden 2354/2023, de 28 de junio, del Consejero de Educación, Ciencia y Universidades, por la que se aprueban las modificaciones de los conciertos educativos de los centros docentes privados para el curso 2023/2024](https://site.educa.madrid.org/dg.concertada-becasyayudas//wp-content/uploads/dg.concertada-becasyayudas/2024/01/Orden_2354_2023_Modificaciones-conciertos_curso-2023-2024.pdf).

Al ser eso de curso 2023-2024, si queremos saber el dinero en 2023, sería parte de curso 2022-2023 y parte del curso 2022-2023, y el número de unidades concertadas puede variar. Mejor mirar por curso.

Buscamos 28011908/Tajamar y 28067392/Los Tilos: no aparecen. Son modificaciones, así que hay que mirar algo anterior.

En [la web de Los Tilos](https://colegiolostilos.com/concierto-educativo.php)  
> El colegio Los Tilos tiene concertadas con la Comunidad de Madrid las etapas educativas de II Ciclo de Infantil, Primaria, Secundaria y Bachillerato. A través de la [Orden 2725/2017 de 21 de julio](https://www.bocm.es/boletin/CM_Orden_BOCM/2017/07/31/BOCM-20170731-21.PDF) se aprobó la renovación del concierto educativo de Los Tilos y de acuerdo con el [Decreto 3/2021 de 13 de enero](https://colegiolostilos.com/pdf/concierto-educativo-renovacion.pdf), su duración se extiende hasta el año 2027. Las unidades concertadas que actualmente tiene autorizadas este centro educativo se recogen en la [Orden 2100/2022 de 20 de julio (Pág. 106), por la que se aprueban las modificaciones de los conciertos educativos de los centros docentes privados para el curso 2022-2023](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/22/BOCM-20220722-18.PDF). 

En [la web de Tajamar](https://www.tajamar.es/bachillerato-concertado-madrid/)  
> En la Comunidad de Madrid no hay muchos Bachilleratos concertados. Tajamar es uno de ellos. Actualmente, el Bachillerato concertado está financiado parcialmente por el Estado. Para completar esa carencia, la Ley de Presupuestos de la Comunidad de Madrid autoriza a los centros a cobrar el importe de 36 euros durante 10 mensualidades de septiembre a junio.


En [Orden 2725/2017 de 21 de julio](https://www.bocm.es/boletin/CM_Orden_BOCM/2017/07/31/BOCM-20170731-21.PDF)

28067392/Los Tilos

| | ED. INFANTIL 2º CICLO | EDUCACIÓN PRIMARIA | ESO 1º/2º | ESO 3º/4º| BTO. |
|:-| :-:| :-:|  :-:| :-:| :-:| 
|UNID. | 6 | 12 | 4 | 4 | 3 |

28011908/Tajamar

| | ED. INFANTIL 2º CICLO | Apoy| EDUCACIÓN PRIMARIA | ESO 1º/2º | ESO 3º/4º| Apoy| Mot |  BTO. |
|:-| :-:| :-:|  :-:| :-:| :-:| :-:| :-:| :-:| 
|UNID. | 9 | 0,5 | 24 | 10 | 9 | 1 | 1 | 8 |

En FP (página 64 del pdf) 

SISTEMAS MICROINFORMATICOS Y REDES 1+1
IMPRESIÓN GRAFICA 1+1
PREIMPRESION DIGITAL 2+2

Podría revisarse si hay alguna modificación posterior, pero sirve para orientarse.

### Paso 3 Localizar los últimos valores para el módulo de concierto 

Se puede mirar en web de centros privados, que tienen mucha información [Escuelas católicas Madrid >  Legislación > Financiación](https://ecmadrid.org/es/legislacion-asesoria/122-financiacion)  

[ORDEN 622/2023, de 27 de febrero, del Vicepresidente, Consejero de Educación y Universidades, relativa a la modificación de los módulos económicos para la financiación de centros docentes privados sostenidos con fondos públicos en el ejercicio 2023.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/03/09/BOCM-20230309-21.PDF)

### Paso 4 Buscar el coste de las unidades concertadas según los módulos de concierto

1 EDUCACIÓN INFANTIL  
(Ratio profesor/unidad concertada 1:1) (en adelante “Ratio”)  
b) Educación infantil 2º ciclo  
(Ratio 1,17:1)  
Importe total anual 63.764,54 € 

2 EDUCACIÓN PRIMARIA  
(Ratio 1,20:1)  
Importe total anual 65.224,19 €

3 EDUCACIÓN SECUNDARIA OBLIGATORIA  
a) Primer y segundo cursos: Maestros  
(Ratio 1,49:1)  
Importe total anual 83.182,36 €  

b) Primer y segundo cursos: Licenciados  
(Ratio 1,49:1)  
Importe total anual 88.933,97 €

c) Tercer y cuarto cursos:  
(Ratio 1,49:1)  
Importe total anual 94.176,02 €

4 ATENCIÓN DE ALUMNOS CON NECESIDADES EDUCATIVAS ESPECÍFICAS EN CENTROS ORDINARIOS  
a1) Educación Infantil (Ratio 1:1)  
Importe total anual 48.923,85 €  

a3) Educación Secundaria (Ratio 1:1)  
Importe total anual 48.948,24 €  

Importe a determinar por la Administración en función de las necesidades concretas a atender tanto del
alumnado como del profesorado  
Motóricos, hasta un máximo 25.657,59 €

7 BACHILLERATO (Ratio 1,64:1)  
Importe total anual 106.186,45 €  

GRUPO 8 Ciclos formativos de:  
b1) Ciclos de 1300 a 1700 horas (ratio 1,56:1)  
b2) Ciclos de 2000 horas (ratio 1,56:1 períodos lectivos en el centro / ratio 0,54:1 períodos formación en la empresa)  

Sistemas Microinformáticos y Redes  

Primer curso 15.425,94 €  
Segundo curso 17.114,78 €  

GRUPO 10 Ciclos Formativos de:
Impresión gráfica  
Preimpresión Digital  

Primer curso 19.648,73 €  
Segundo curso 21.069,33 €  

### Paso 5 Calcular el coste de las unidades concertadas según los módulos de concierto

Para el ejemplo: 

- En 1º y 2º ESO se usa valor maestros, no de licenciados  
- En motóricos se pone el valor máximo indicado 
- Se usa la "ratio" según lo comentado en apartado Dependencia del importe del módulo asociado a conciertos de la ratio de [Ratios en educación concertada](https://algoquedaquedecir.blogspot.com/2020/01/ratios-en-educacion-concertada.html)  

Los Tilos: 6 x 1,17 x 63764,54 + 12 x 1,2 x 65224,19 +  4 x 1,49 x 83182,36 + 4 x 1,49 x 94176,02 + 3 x 1,64 x 106186,45 = 2966348,6856 € (3,0 M€)

<!-- 6*1,17*63764,54 + 12*1,2*65224,19 +  4*1,49*83182,36 + 4*1,49*94176,02 + 3*1,64*106186,45 --> 

Tajamar: 9 x 1,17 x 63764,54 + 0,5 x 1 x 48923,85 + 24 x 1,2 x 65224,19 +  10 x 1,49 x 83182,36 + 9 x 1,49 x 94176,02 + 1 x 48948,24 + 1 x 25657,59 + 1 x 1,56 x 15425,94 + 1 x 1,56 x 17114,78 + 1 x 1,56 x 19648,73 + 1 x 1,56 x 21069,33 + 2 x 1,56 x 19648,73 + 2 x 1,56 x 21069,33 + 8 x 1,64 x 106186,45 = 6785772,8934 € (6,8 M€)

<!-- 9*1,17*63764,54 + 0,5*1*48923,85 + 24*1,2*65224,19 +  10*1,49*83182,36 + 9*1,49*94176,02 + 1*48948,24 + 1*25657,59 + 1*1,56*15425,94 + 1*1,56*17114,78 + 1*1,56*19648,73 + 1*1,56*21069,33 + 2*1,56*19648,73 + 2*1,56*21069,33 + 8*1,64*106186,45 --> 

## Otras vías que suponen coste de dinero público de centros privados 

Los profesores religiosos también reciben dinero fuera del concierto, y ese dinero pasa por el centro, ver [Profesorado religiosamente pagado con dinero público](https://algoquedaquedecir.blogspot.com/2020/02/profesorado-religiosamente-pagado-con-dinero-publico.html)

Los centros privados, que pueden tener ESO concertada pero Bachillerato no, reciben dinero en Madrid vía [Cheque Bachillerato Madrid](http://algoquedaquedecir.blogspot.com/2018/10/cheque-bachillerato-madrid.html). En el caso de tener Bachillerato concertado los centros ya reciben concierto además de cobrar cierto dinero y en ese caso no reciben el cheque bachillerato. 

Otros cheques solo para privados, aunque lo llamen "becas", como  
* ["Becas" FP Grado Medio Madrid](https://algoquedaquedecir.blogspot.com/2022/02/becas-fp-grado-medio-madrid.html)  
* ["Becas" FP Grado Superior Madrid](https://fiquipedia.gitlab.io/algoquedaquedecir/post/cheques-fp-grado-superior/)  
* ["Becas" Educación Infantil Madrid](https://algoquedaquedecir.blogspot.com/2022/06/becas-educacion-infantil-madrid.html)  
* [Becas para el estudio de programas de segunda oportunidad Madrid](https://fiquipedia.gitlab.io/algoquedaquedecir/post/becas-para-el-estudio-de-programas-de-segunda-oportunidad/)  


Los orientadores, ver [orientación concertada](http://algoquedaquedecir.blogspot.com/2018/04/orientacion-concertada.html)  

Otra vía de recibir recursos públicos son las cesiones de suelo

Otro coste de dinero público es el dinero que se deja de recibir cuando un centro obtiene el concierto, ya que a partir de ese momento deja de pagar el IBI al ayuntamiento, y el ayuntamiento lo recibe del estado.

Otro coste de dinero público son deducciones / desgravaciones de las cuotas a privados a través de fundaciones y algunas asociadas a uniformes. Ver [Pagos "voluntarios" en educación y fiscalidad](http://algoquedaquedecir.blogspot.com/2018/12/pagos-voluntarios-en-educacion.html)  

### Aulas bilingües



En [Anexo III Ley 15/2023, de 27 de diciembre, de Presupuestos Generales de la Comunidad de Madrid para el año 2024.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2024-10765) se añade un nuevo concepto que es   
> Importe por unidad bilingüe.  

Con valor 564,62 en Infantil y 1.906,52 en primaria y secundaria  

En el trámite de audiencia [Proyecto de Orden por la que se modifican las órdenes que regulan la enseñanza bilingüe español-inglés en centros privados concertados, para su adaptación a la financiación establecida en la Ley de Presupuestos Generales de la Comunidad de Madrid](https://www.comunidad.madrid/transparencia/proyecto-orden-que-se-modifican-ordenes-que-regulan-ensenanza-bilingue-espanol-ingles-centros)  

En MAIN se indica  

> En cuanto a la financiación con fondos públicos de la enseñanza bilingüe, la Orden 763/2015, de 24 de
marzo, en su artículo 5.2, establece la aplicación por cada unidad escolar bilingüe, de un coeficiente de
0,26 al concepto "Otros Gastos" del módulo económico del concierto educativo de Educación Primaria
y la cuantía equivalente en Educación Secundaria Obligatoria a fin de cubrir los gastos correspondientes
a las horas del auxiliar de conversación por unidad escolar, la adquisición de material didáctico, la
formación del profesorado y los gastos generales de funcionamiento de esta enseñanza.
A su vez, la Orden 988/2023, de 22 de marzo, artículo 4.2, establece la aplicación, por cada unidad
escolar concertada de segundo ciclo de Educación Infantil en la que se implante enseñanza bilingüe,
de un coeficiente de 0,077 al concepto “Otros Gastos” del módulo económico del concierto educativo
de Educación Infantil segundo ciclo, a fin de cubrir los gastos correspondientes al auxiliar de
conversación, la adquisición de material didáctico, la formación del profesorado y los gastos generales
de funcionamiento de esta enseñanza.  
La Ley 15/2023, de 27 de diciembre, de Presupuestos Generales de la Comunidad de Madrid para el
año 2024 ha previsto un cambio en cuanto a la forma de aplicación de la financiación con fondos
públicos de la enseñanza bilingüe que, sin incrementar su cuantía, dará estabilidad a la misma y
facilitará su gestión. En este sentido, en su artículo 48.1.i), se regula la aplicación, por cada unidad
escolar bilingüe, de los módulos económicos recogidos en el Anexo III de la misma Ley.  
El nuevo módulo económico denominado “Importe por unidad bilingüe” que ha sido introducido en la
ley de presupuestos generales para la Comunidad de Madrid, estabiliza la cuantía a abonar a los
centros privados concertados por unidad escolar bilingüe en 564,62€ en el caso de unidades de
segundo ciclo de Educación Infantil, y en 1.906,52€ en el caso de unidades escolares bilingües de
Educación Primaria y Educación Secundaria Obligatoria. De esta forma, los futuros incrementos que
puedan impactar en el módulo económico “Otros gastos” de las próximas leyes de presupuestos no
supondrán un incremento anual en el coste por unidad escolar bilingüe, como venía sucediendo hasta
el momento, comenzando a aplicarse este nuevo módulo económico “Importe por unidad bilingüe”
desde el curso 2024-2025. Este nuevo módulo tendrá continuidad en futuras leyes de presupuestos
generales de la Comunidad de Madrid, pudiéndose modificar cuando, por analogía con el importe
financiado en los centros públicos, se considere oportuno


### Diversificación curricular

[Resolución de 30 de agosto de 2023, de  la Dirección General de Educación Concertada, Becas y Ayudas al Estudio, por la que se autorizan incrementos de ratio de profesorado en centros docentes privados sostenidos con fondos públicos para la organización de grupos de refuerzo en materias de carácter instrumental, y programas de diversificación curricular (PDC), en el curso 2023-2024.](https://www.educa2.madrid.org/web/educamadrid/principal/files/52226c44-afba-43b6-afcc-49329b530a1f/2023.08.30%20Resoluci%C3%B3n%20DGECBAE%20autoriza%20grupos%20de%20refuerzo%20y%20diversificacion%20curso%2023-24.pdf?t=1693469619777)  

En anexo se detallan horas de refuerzo


### Aulas de enlace

[RESOLUCIÓN DE LA DIRECCIÓN GENERAL DE EDUCACIÓN CONCERTADA, BECAS Y AYUDAS AL ESTUDIO, POR LA QUE SE AUTORIZA LA FINANCIACIÓN PARA EL FUNCIONAMIENTO DE AULAS DE ENLACE EN CENTROS PRIVADOS SOSTENIDOS CON FONDOS PÚBLICOS DURANTE EL CURSO 2023-2024.](https://rss.educa2.madrid.org/web/educamadrid/principal/files/52226c44-afba-43b6-afcc-49329b530a1f/2023.06.29_Resolucion_Aulas_Enlace_2023_24_37685041.pdf?t=1688488915535)  

En anexo se detallan aulas autorizadas

[Orden 10347/2012, de 11 de septiembre, de la Consejería de Educación y Empleo, por la que se modifica la Orden 979/2003 de 21 de febrero, de la Consejería de Educación, por la que se regula la financiación de las aulas de enlace implantadas en centros privados sostenidos con fondos públicos. - ecmadrid.org](https://www.ecmadrid.org/es/pdfs-revistas/doc_download/2486-orden-10347-2012-11-septiembre-modifica-orden-979-2003-21-febrero-financiacion-aulas-enlace)  

> Las Aulas de Educación Primaria, Educación Secundaria Obligatoria o Mixtas con una ratio de alumnos/aula mínima de diez alumnos,
tendrán una ratio profesor/unidadescolarde 1:1.  
> Las Aulas de Educación Primaria, Educación Secundaria Obligatoria o Mixtas con una ratio de alumnos/aula entre cinco y diez alumnos,
tendrán una ratio profesor/unidadescolarde 0,5:1.  

> El concepto "otros gastos" de aplicación a todas las aulas de enlace será el correspondiente al módulo económico establecido en la Leyes
anuales de Presupuestos Generales de la comunidad de Madrid para las unidades escolares de educación primaria.

[Orden 979/2003 de 21 de febrero, de la Consejería de Educación, por la que se regula la financiación de las aulas de enlace implantadas en centros privados sostenidos con fondos públicos. - ecmadrid.org](https://www.ecmadrid.org/es/pdfs-revistas/doc_download/2484-orden-979-2003-21-febrero-financiacion-aulas-enlace)  

> Los profesores de estas Aulas serán dados de alta en la nómina de pago delegado de los centros privados sostenidos con fondos públicos y los centros recibirán, mediante libramientos mensuales, las cantidades correspondientes al módulo de otros gastos.

> A cada Aula de enlace se le podrá abonar igualmente hasta la cantidad de 12.299€ para su equipamiento con material didáctico específico y para otros gastos derivados del proceso de enseñanza-aprendizaje individualizado que requieran los alumnos. De dicha cantidad 1.800 €
serán abonados en un único pago por curso escolar al inicio del mismo con la finalidad de dotar las aulas del equipamiento didáctico que precisen.

> Si el Aula de enlace está ubicada en un centro distinto a aquél en que está matriculado el alumno, se podrá financiar el transporte del mismo, en las siguientes cuantías por curso:  
>- 310 € si la distanciaque separalos centroses inferior a 15 Km.  
>- 370 € si dicha distanciaes superiora 15 Km.  

## Otras vías de conocer información

Una vía es el registro mercantil, ver las cuentas de la empresa privada que es el colegio, pero es de pago. Además está el tema de que haya fundaciones paralelas que también haya que mirar.  


Los centros privados con concierto son sujetos obligados a publicidad activa en Madrid según [artículo 3.2 de Ley 10/2019, de 10 de abril, de Transparencia y de Participación de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-2019-10102#a3)  
> 2. Las normas reguladoras de los conciertos y otras formas de participación de entidades privadas en los sistemas públicos de educación, sanidad y servicios sociales establecerán la información que deben publicar estas entidades, de entre la prevista en el Título II, para colaborar en la prestación de los mencionados servicios financiados con fondos públicos. **La relación de la información que deben publicar estas entidades incluirá al menos, en los pliegos o documentos contractuales equivalentes que correspondan, los importes básicos de la concesión (canon y/o precio inicial de licitación), las condiciones de la misma, el seguimiento de las infracciones, las modificaciones económicas que se realicen y su justificación, así como las sanciones o informes de seguimiento establecidos.**

La relación de estos otros sujetos obligados se publica por artículo 3.4  
> 4. El Gobierno de la Comunidad de Madrid publicará anualmente en el Portal de Transparencia un listado de los sujetos incluidos dentro de este artículo, clasificados según el grupo al que pertenezcan.

[Sujetos obligados -  comunidad.madrid/transparencia](https://www.comunidad.madrid/transparencia/sujetos-obligados)

[Entidades participantes en sistemas de educación, sanidad y políticas sociales](https://www.comunidad.madrid/transparencia/entidades-privadas-sistemas-publicos-educacion-sanidad-y-servicios-sociales)  

[Conciertos educación 2023](https://www.comunidad.madrid/transparencia/sites/default/files/open-data/downloads/sujetos_obligados_educacion_total_2023.xlsx)  

Ver post [Publicidad, mérito y capacidad en privados con concierto](https://algoquedaquedecir.blogspot.com/2019/09/publicidad-merito-y-capacidad-en-privados-con-concierto.html) 

