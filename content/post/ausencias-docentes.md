+++
author = "Enrique García"
title = "Ausencias docentes"
date = "2024-11-23"
tags = [
    "profesorado", "RGPD"
]
toc = true

+++

Revisado 8 marzo 2025

## Resumen

En septiembre 2024 surge una resolución en Andalucía que se comenta por profesorado con polémica: se habla de publicar un estadillo de faltas de profesorado en sala de profesores y enviar una copia al consejo escolar. Intento comentar ese caso y la visión general, también citando ideas generales de percepción y realidad con datos estadísticos sobre ausencias de profesorado.  

Enlaza con otros temas que se pueden citar pero son para separar y detallar en posts separados:  
- registro de jornada, jornada real docentes ...  
- motivos de baja del profesorado, condiciones de trabajo, salud mental (ansiedad, depresión), ...   

## Detalle

### Resolución 2024 Andalucía

[Resolución de 30 de septiembre de 2024, de la Dirección General del Profesorado y Gestión de Recursos Humanos, por la que se aprueba el Manual para la gestión del cumplimiento de jornada y horarios en los centros docentes públicos y servicios de apoyo a la educación dependientes de la Consejería con competencias en materia de educación de la Junta de Andalucía](https://www.juntadeandalucia.es/boja/2024/195/BOJA24-195-00017-51646-01_00308595.pdf)  

> 9.3. Publicidad de las ausencias.  
Los datos obrantes en el sistema informático de control de ausencias de Séneca,
que deben ser concordantes con los que figuran en el soporte digital del control diario
establecido, se resumirán en un estadillo mensual que se generará, a través de Séneca,
según el modelo que figura como Anexo B. **Este estadillo se remitirá el día 5 de cada mes, o el siguiente día hábil, a través de mensajería Séneca a todo el personal docente y no docente de los centros o servicios de apoyo a la educación**. En caso de que no pueda difundirse a todo el personal, la persona titular del centro o servicio deberá **publicarlo en los tablones de anuncios de la Sala del Profesorado de los Centros Públicos** o Servicios
de Apoyo a la Educación, desde el día 5 de cada mes, o siguiente día hábil debiendo
permanecer publicado, al menos, hasta el día 5 del mes siguiente. En dicho estadillo se
incluirán todas las ausencias producidas durante el mes anterior, ya sean justificadas o no
justificadas. En ningún caso se podrán reflejar otros datos distintos a los que se recogen
en el modelo de estadillo oficial y, en particular, relativos a diagnósticos de enfermedades.
Una vez generado dicho estadillo, será enviado a través de la plataforma Séneca al
servicio de inspección correspondiente.  
Si se detectasen errores en el estadillo, podrán ser subsanados desde el mismo día
de la publicación hasta el día 10 de cada mes, o el siguiente día hábil, debiendo ser
nuevamente publicado tras la modificación.  
En virtud de las competencias que tiene atribuidas el Consejo Escolar, una vez al
trimestre, la persona titular de la dirección que ejerce la Presidencia de dicho órgano,
**informará al mismo de las ausencias del personal**. La información que se le haga llegar
a este órgano sólo podrá estar relacionada con los datos que aparecen recogidos en el
estadillo mensual, nunca sobre los justificantes de faltas de asistencia ni con los motivos
de ausencia.  

Inicialmente en BOJA no se publica ese anexo B. Lo solicito vía transparencia

**10 octubre 2024**  
[MANUAL FISCALIZADOR DE AUSENCIAS DEL PROFESORADO - docentesxlapublica](https://www.docentesxlapublica.com/manual-fiscalizador-de-ausencias-del-profesorado/)  


En respuesta a solicitud transparencia de anexo B me remiten a BOJA donde se publicó más tarde. 

[Corrección de errores de la Resolución de 30 de septiembre de 2024, de la Dirección General del Profesorado y Gestión de Recursos Humanos, por la que se aprueba el Manual para la gestión del cumplimiento de jornada y horarios en los centros docentes públicos y servicios de apoyo a la educación dependientes de la Consejería con competencias en materia de educación de la Junta de Andalucía y se publican los Anexos A, B, C y D (BOJA núm. 195, de 7 de octubre de 2024).](https://www.juntadeandalucia.es/boja/2024/206/BOJA24-206-00006-52523-01_00309471.pdf)  

![](https://cdn.bsky.app/img/feed_fullsize/plain/did:plc:ygxyvnpzivzxnytl73jxnyou/bafkreig3xhvjhksp6tniqic5lp5csz34s7zrduzolejnlswpjmblq2a3iu@jpeg)  
-Minimización datos no requiere nombre+documento a claustro y Consejo escolar; basta datos agrupados ya que lo individual se tramita por otra vía.  
-Motivo+tipo justificación aunque sea código puede ser sensible.  
-Coste* ¿?  

Al mirar veo que la idea de las instrucciones de Andalucía 2024 ya está en normativa estatal 

[Orden de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la organización y funcionamiento de las escuelas de educación infantil y de los colegios de educación primaria. Cumplimiento del horario por parte del profesorado](https://www.boe.es/buscar/act.php?id=BOE-A-1994-15723#cumplimientodelhorarioporpartedelprofesorado)  

> 87. Sin perjuicio de lo dispuesto en apartados posteriores, los Directores de los centros deberán remitir al servicio de Inspección, antes del día 5 de cada mes, los partes de faltas relativos al mes anterior elaborados por el Jefe de estudios. En los modelos que al efecto se confeccionen por las Direcciones Provinciales se incluirán, asimismo, **las ausencias o retrasos referidos a las horas de permanencia obligada en el centro que haya de cumplir el Maestro, con independencia de que esté o no justificada la ausencia.**

> 89. **Una copia del parte de faltas y otra de la relación de actividades complementarias remitidas al Servicio de Inspección Técnica se hará pública en lugar visible en la sala de Profesores. Otra copia quedará en la Secretaría del centro a disposición del Consejo Escolar**.  

[Orden de 29 de junio de 1994 por la que se aprueban las instrucciones que regulan la organización y funcionamiento de los institutos de Educación Secundaria. Cumplimiento del horario por parte del profesorado](https://www.boe.es/buscar/act.php?id=BOE-A-1994-15565#cumplimientodelhorarioporpartedelprofesorado)  

> 105. **Una copia del parte de faltas y otra de la relación de actividades complementarias remitidas al Servicio de Inspección Técnica se harán públicas, en lugar visible, en la sala de Profesores. Otra copia quedará en la Secretaría del instituto a disposición del Consejo Escolar.**

**21 noviembre 2024**

La consejería me inadmite la solicitud de los códigos y me remite a CAUCE: llamando a CAUCE me dicen que contacte con la consejería. 

**22 noviembre 2024**

Reclamo al Consejo de Transparencia de Andalucía

**7 marzo 2025**

Recibo resolución estimatoria de Consejo de Transparencia de Andalucía  
[2025-03-06-RES2025-0246.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/AusenciasDocentes/2025-03-06-RES2025-0246.pdf)

Aparte de la resolución, sin interesantes respecto a transparencia

Tercero. Consideraciones generales sobre el derecho de acceso a la información pública

Cuarto. Consideraciones de este Consejo sobre el objeto de la reclamación

Quinto. Cuestiones generales sobre la formalización del acceso.

Se cita Sentencia Tribunal Supremo n.º 1547/2017, de 16 de octubre (Sala de lo Contencioso-Administrativo, Sección Tercera)

> Tampoco se entiende que lo solicitado no esté incluido en el ámbito objetivo de la ley, ya que lo solici-
tado es información pública. Sin perjuicio de qué órgano de la entidad reclamada deba responder la
solicitud, lo cierto es que la entidad se limitó a derivarla a otro servicio sin que conste la respuesta a la
persona reclamante.



**Pendiente** reclamar AEPD con el dato concreto de códigos de Andalucía y citando normativa BOE 1994 que habría que revisar tras entrar en vigor RGPD.  

Revisar y citar otras CCAA

Algo relacionado para citar  
2023  
[Demandan a la Seguridad Social por "vulnerar la intimidad" de las mujeres de baja por menstruación o por aborto](https://www.elmundo.es/ciencia-y-salud/salud/2023/12/21/65842bbbfc6c8331598b45ad.html)  
2024  
[Un agujero en la regulación permite a las empresas saber el motivo de la baja laboral en los abortos y reglas dolorosas](https://www.eldiario.es/economia/agujero-regulacion-permite-empresas-motivo-baja-abortos-reglas-dolorosas_1_11752930.html)  
> La protección de estas bajas “especiales” se aprobó en la Ley del aborto y, al tramitarla, la Seguridad Social comunica datos a las compañías que desvelan el origen de la ausencia, algo que desconocen tanto médicos como colectivos de mujeres  

Incumple RGPD: citar caso de códigos de mujeres trabajadoras

### Gestión ausencias profesorado por centros en distintas CCAA

[LO NUEVO DE LAS FALTAS DE ASISTENCIA DEL PROFESORADO EN GESTIóN DE CENTROS - gva](https://ceice.gva.es/soportegc/documents/faltas_profesorado_nuevo.pdf)  

### Percepción y realidad con datos estadísticos sobre ausencias de profesorado

1999  
[Análisis descriptivo. Las bajas laborales docentes. EJG Barona · 1998](https://dialnet.unirioja.es/descarga/articulo/118993.pdf)

2002  
[Bajas laborales y riesgos psicosociales en la enseñanza. Palencia, cursos 98-02](https://www.intersindical.org/salutlaboral/stepv/vall_bajlab.pdf)  

2012  
[Los docentes tienen el menor índice de absentismo de todos los funcionarios de la Generalitat](https://www.20minutos.es/noticia/1602019/0/docentes/bajas/valencia/)  
> Las bajas de profesores duran menos días.

2019  
[El absentismo laboral docente se sitúa en el 3,1% durante los últimos cuatro años](https://elfarodeceuta.es/absentismo-laboral-docente-ceuta/)  
> La ratio de ausencias al puesto de trabajo de maestros y profesores de Ceuta se sitúa casi un 50% por debajo de la media nacional l El MEFP afirma no tener datos de las deducciones de haberes realizadas

2022  
[Crecen las bajas de docentes por ansiedad y depresión: "Todos los problemas se cargan sobre nosotros y se menosprecia la autoridad"](https://www.elmundo.es/espana/2022/11/22/637ccb39fc6c83cf268b4628.html)  

[ANPE presenta su informe estatal del servicio de Defensor del Profesor del curso 2021/22](https://www.anpe.es/notices/24373/ANPE-presenta-su-informe-estatal-del-servicio-de-Defensor-del-Profesor-del-curso-202122)  
EL ÚLTIMO INFORME DEL SERVICIO DEL DEFENSOR DEL PROFESOR DE ANPE ALERTA DEL AUMENTO DE CASOS DE DEPRESIÓN Y ESTRÉS ENTRE LOS DOCENTES Y POR CONSIGUIENTE EL INCREMENTO DE LAS BAJAS LABORALES

2023  
[Educadores y sanitarios triplican las bajas laborales por problemas de salud mental en siete años](https://www.publico.es/sociedad/educadores-sanitarios-triplican-bajas-laborales-problemas-salud-mental-siete-anos.html)  

2024  
[El profesorado es el colectivo con menos porcentaje de ausencias por incapacidad temporal de toda la Administración autonómica - anpecanarias](https://anpecanarias.es/notices/183642/El-profesorado-es-el-colectivo-con-menos-porcentaje-de-ausencias-por-incapacidad-temporal-de-toda-la-Administraci%C3%B3n-auton%C3%B3mica)  

[Educación pide a Salud Pública que estudie el elevado absentismo de la plantilla docente navarra](https://cadenaser.com/navarra/2024/04/23/educacion-pide-a-salud-publica-que-estudie-el-elevado-absentismo-de-la-plantilla-docente-navarra-radio-pamplona/)  
> El porcentaje de personal docente interino de baja superó en marzo el 21 por ciento y el 11 entre quienes son funcionarios mientras que el absentismo medio en Navarra está en el 7 por ciento


### Registro de jornada 

Es algo que surgió en normativa general 
[Real Decreto-ley 8/2019, de 8 de marzo, de medidas urgentes de protección social y de lucha contra la precariedad laboral en la jornada de trabajo.](https://www.boe.es/buscar/act.php?id=BOE-A-2019-3481)  

Modificó [Artículo 34. Jornada. Real Decreto Legislativo 2/2015, de 23 de octubre, por el que se aprueba el texto refundido de la Ley del Estatuto de los Trabajadores.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11430#a34)  
> 9. La empresa garantizará el registro diario de jornada, que deberá incluir el horario concreto de inicio y finalización de la jornada de trabajo de cada persona trabajadora, sin perjuicio de la flexibilidad horaria que se establece en este artículo.

No aplica a docentes según [Artículo 1. Ámbito de aplicación.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11430#a1)  

> 3. Se excluyen del ámbito regulado por esta ley:  
a) La relación de servicio de los funcionarios públicos, que se regirá por las correspondientes normas legales y reglamentarias, así como la del personal al servicio de las Administraciones Públicas y demás entes, organismos y entidades del sector público, cuando, al amparo de una ley, dicha relación se regule por normas administrativas o estatutarias.

Pero se ha aplicado idea de registro de jornada a docentes

[twitter FiQuiPedia/status/1148182488017227776](https://x.com/FiQuiPedia/status/1148182488017227776)  
Sobre registro jornada RDL8/2019 y docentes; detalles Baleares. Si solo se registra entrar y salir del centro, sin controlar resto jornada para verificar se cumple jornada 37,5 h semanales (35 h en CLM), es registro de presencia en centro, no de jornada.  
[UOB rebutja l’obligació de fitxar als centres educatius](https://uob.cat/ensenyament/2019/07/02/uob-rebutja-lobligacio-de-fitxar-als/)  
![](https://pbs.twimg.com/media/D-8p4JSXYAE5scc?format=jpg)

[twitter caty_pou/status/1845063519063912556](https://x.com/caty_pou/status/1845063519063912556)  
STS 4744/2024, 24.09  
✅ "El registro de jornada debe cumplir con las exigencias de fiabilidad, trazabilidad y objetividad".  
🟡 Los representantes de los trabajadores tienen d° a acceder a los datos personales [...]  
[Roj: STS 4744/2024 - ECLI:ES:TS:2024:4744](https://www.poderjudicial.es/search/AN/openDocument/aa4164de9a1a0668a0a8778d75e36f0d/20241011)  



