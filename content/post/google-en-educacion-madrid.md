+++
author = "Enrique García"
title = "Google en educación: Madrid"
date = "2024-08-25"
tags = [
    "educación", "google", "Madrid"
]
toc = true

description = "Google en educación"
thumbnail = "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEggYj5wyCw41koZimcC0-dIk3lMvrOaIgwpMW-yqegW_K8v5JYbdPwTMm5WrMXMly7IujRDiHmXfKjXGy5jVZOZ6z1JTYRYnVqRPtR7bfWV-p-HGuPJbzqkAYjxQs2d16fU7NkRJa0ZRx8a/s16000/Captura+de+pantalla+de+2021-06-17+19-17-45.png"
images = ["https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEggYj5wyCw41koZimcC0-dIk3lMvrOaIgwpMW-yqegW_K8v5JYbdPwTMm5WrMXMly7IujRDiHmXfKjXGy5jVZOZ6z1JTYRYnVqRPtR7bfWV-p-HGuPJbzqkAYjxQs2d16fU7NkRJa0ZRx8a/s16000/Captura+de+pantalla+de+2021-06-17+19-17-45.png"]

+++

Revisado 7 enero 2025

## Resumen

Recopilo y comparto información sobre el uso de Google en educación en Madrid. Es un post separado de [Google en educación](../google-en-educacion) en el que intento separar solo sobre Madrid para descargar.  

![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEggYj5wyCw41koZimcC0-dIk3lMvrOaIgwpMW-yqegW_K8v5JYbdPwTMm5WrMXMly7IujRDiHmXfKjXGy5jVZOZ6z1JTYRYnVqRPtR7bfWV-p-HGuPJbzqkAYjxQs2d16fU7NkRJa0ZRx8a/s16000/Captura+de+pantalla+de+2021-06-17+19-17-45.png)

Posts relacionados:

* [Normativa webs, aplicaciones y protección de datos en educación](https://algoquedaquedecir.blogspot.com/2019/06/normativa-webs-aplicaciones-y-proteccion-datos.html) (además de ideas generales como normativa sobre uso de aplicaciones, se comentan cosas comunes a varias plataformas como Google forms y Microsoft forms)  
* [Microsoft Teams en educación Madrid](https://algoquedaquedecir.blogspot.com/2020/06/microsoft-teams-en-educacion-madrid.html)  (aunque tiene ese título cito otras comunidades)  
* [Ejemplos RGPD en educación](https://algoquedaquedecir.blogspot.com/2022/09/ejemplos-rgpd-en-educacion.html)
* [EducaMadrid: qué y por qué](https://algoquedaquedecir.blogspot.com/2021/01/educamadrid-que-y-por-que.html) (para mostrar qué cosas no son EducaMadrid, como algunas cosas propietarias)  

## Detalle

Ver detalles en [Google en educación](../google-en-educacion) donde se mencionan por ejemplo sanciones por uso de Google en educación Madrid. Aquí se repiten solo las asociadas a Madrid 


### Sanciones asociadas a uso de Google en educación Madrid

Pueden ser sanciones a centros privados o "apercibimiento" a consejerías de educación 

**2021 Apercibimiento consejería educación Madrid**

[Expediente N.º: PS/00286/2021  Escuela Oficial de Idiomas (EOI) de Madrid "Jesús Maestro"](https://www.aepd.es/es/documento/ps-00286-2021.pdf)  

Se considera que la reclamada ha infringido el RGPD en el artículo 32 del RGPD, que indica:

> 4. El responsable y el encargado del tratamiento tomarán medidas **para garantizar que cualquier persona que actúe bajo la autoridad del responsable o del encargado y tenga acceso a datos personales solo pueda tratar dichos datos siguiendo instrucciones del responsable**, salvo que esté obligada a ello en virtud del Derecho de la Unión o de los Estados miembros.”. (el subrayado es de la AEPD). 


**2023 Apercibimiento centro educativo Madrid dependiente del Ministerio de defensa**

[PS-00334-2022 (10.11.2023) Sobre uso de la plataforma GOOGLE WORKSPACE for Education (GW)  en centro educativo COLEGIO MENOR NUESTRA SEÑORA DE LORETO](https://www.aepd.es/documento/ps-00334-2022.pdf)  

Resolución 56 páginas

PRIMERO: SANCIONAR al ***CARGO.1- COLEGIO MENOR NUESTRA SEÑORA DE LORETO (MINISTERIO DE DEFENSA, EJÉRCITO DEL AIRE Y DEL ESPACIO ) con NIF S2801407D, con una sanción de apercibimiento por cada una de las infracciones del RGPD, de los artículos:
-28, de conformidad con el articulo 83.4 a) del RGPD, tipificada como grave a efectos de prescripción en el artículo 73.k) de la LOPDGDD.  
-13, de conformidad con el articulo 83.5 b) del RGPD, tipificada como leve a efectos de prescripción en el artículo 74.a) de la LOPDGDD.  
SEGUNDO: NOTIFICAR la presente resolución al MINISTERIO DE DEFENSA, (EJÉRCITO DEL AIRE Y DEL ESPACIO- ***CARGO.1- COLEGIO MENOR NUESTRA SEÑORA DE LORETO) con NIF S2801407D  

### Uso de Gsuite en educación Madrid

Para casos ver post [Ejemplos RGPD en educación](https://algoquedaquedecir.blogspot.com/2022/09/ejemplos-rgpd-en-educacion.html)

Hay centros de Madrid que han hecho contratos con Gsuite, y ADIMAD lo citó en un comunicado asociado a la pandemia

[twitter FiQuiPedia/status/1286716664868937730](https://twitter.com/FiQuiPedia/status/1286716664868937730)  
\#VueltaSegura #VueltaSinRiesgo  
[Comunicado de ADIMAD](https://ep00.epimg.net/descargables/2020/07/24/849b20a42d0435df9c6daa163748e0e3.pdf)  
"Aunque en la Consejería tenemos jefes, no trabajamos para ellos, ni para atender sus ocurrencias efímeras y variadas. Trabajamos para los ciudadanos: somos servidores públicos, no siervos."  

> Que se deje de jugar con las plataformas educativas y, con las garantías debidas, se respete el trabajo del profesorado. Como señalábamos el curso pasado, el uso bastante generalizado de Google Suite no  es  por  gusto, sino  porque,  según  los  usuarios, es  más  fiable  y  útil  que EducaMadrid  o  Teams,y además,  reduce  la  brecha  digital  porque trabajar  con  EducaMadrid sin  un  ordenador  es  casi  imposible,  mientras  que  con  Google  cualquier  alumno/a  o  familia con cualquier  dispositivo de  gama  baja  puede  seguir  las  clases  con  total  facilidad  gracias  a que  para  todo  hay Apps  muy  sencillas  en  Android  e  IOS.Por  otro  lado,  resulta  inaceptable que  a  los  centros  de  titularidad  no  pública  pero  financiados  con  fondos  públicos  se  les conceda autonomía para utilizar las plataformas que consideren más adecuadas mientras que a  los  centros  de  titularidad  pública  se  nos pongan  todo  tipo  de  dificultades legales,  cuando, que sepamos, la Ley Orgánica de Protección de Datos es igual para todos los centros y todas las plataformas. EducaMadrid puede ser un buen repositorio de materiales educativos pero la imprescindible formación digital de nuestros alumnos, que marca ya la diferencia y la calidad de  cualquier  sistema  educativo,  requiere  el  uso  y  aprendizaje  de  diferentes  programas, plataformas, aplicaciones, etc., que los centros públicos queremos y debemos llevar adelante como parte de nuestro Proyecto Educativo y dentro de nuestra autonomía y cumplimiento de la  legislación  vigente.  Lo  contrario  revela  un  desconocimiento  de  la  realidad  actual  y  de  las perspectivas  de  la  Educación  en  el  siglo  XXI  muy  preocupante  si  procede  de  nuestras autoridades educativas.

En mayo 2020 se lanza  
[G Suite educación para los centros educativos públicos de la Comunidad de Madrid - change.org](https://www.change.org/p/consejero-de-educaci%C3%B3n-de-la-comunidad-de-madrid-g-suite-educaci%C3%B3n-para-los-centros-educativos-p%C3%BAblicos-de-la-comunidad-de-madrid)  
Lo promueve David Santos
[davidsantos.info/](http://davidsantos.info/)  
[twitter davidsantos_a](https://twitter.com/davidsantos_a)  
https://www.gsuiteedupodcast.com/0 _no operativo en 2024_  
David Santos: soy maestro y director de un colegio público de infantil y primaria, en Majadahonda, Madrid. Algunos me conoceréis por mi podcast Píldoras de Educación, donde comparto reflexiones, prácticas y entrevistas para ayudarnos al cambio que creo necesario en la educación. Además soy Google Certified Trainer e Innovator. Me dedico también a la formación del profesorado en tecnología y metodologías activas.  

Incluye enlaces

[Revista Digital EducaMadrid - G Suite for Education](https://www.educa2.madrid.org/web/revista-digital/educacion-a-distancia/-/visor/g-suite-for-education)

> **G Suite for Education**  
Para apoyar el aprendizaje a distancia en la Comunidad de Madrid Google España ofrece su tecnología para apoyar a Colegios, Centros de Formación Profesional, Universidades y cualquier otra Institución Educativa reglada de la Comunidad Autónoma de Madrid para habilitar el aprendizaje a distancia de manera segura, fácil y escalable en esta situación de excepcionalidad de suspensión de las aulas debido al coronavirus.  
Por ello ofrecen el paquete de G Suite for Education de forma gratuita, y con las funcionalidades Enterprise hasta el próximo 1 de julio. Gracias a ello los centros podrán:  
* Impartir clases online, compartiendo pantalla con hasta 250 alumnos simultáneamente.  
* Hacer streaming con hasta 100.000 estudiantes y grabar las sesiones para acceder a ellas en otro momento.
* Compartir el temario, marcar deberes, fomentar el trabajo en equipo o incluso poner exámenes a través Classroom.
[Más información, forma de acceso, garantía para la comunidad educativa  y otros recursos](https://www.educa2.madrid.org/web/educamadrid/principal/files/secondary/ecc1d908-0b92-4885-bbdc-1164c44b78c0/COMUNIDAD%20DE%20MADRID%20-%20Aprendizaje%20a%20distancia%20con%20G%20Suite%20for%20%20Education%20.pdf?t=1584612057241)  

Tras el privacy shield, yo entiendo que no es posible.

Argumentos en documento PROTECCIÓN DE DATOS EN IES CERVANTES  

CSV 1000822903482025338221

> • Un centro no está legitimado para suscribir un contrato de encargo de tratamiento en su propio nombre con el prestador del servicio,  
• Un centro no puede suscribir  un contrato de adhesión que no ha sido supervisado por la Consejería que obliga a aceptar las condiciones del encargado en lugar de ser este quien acepte las del responsable. Aunque el centro incluya en su información al alumnado un enlace sobre la seguridad de Google Classroom, hay que tener en cuenta que en el citado informe se concluye que únicamente la versión G-Suite Enterprise for Education dispone de las características de seguridad adecuadas para un alineamiento efectivo con el Esquema Nacional de Seguridad, y en consecuencia el paquete básico G-Suite for Education no resulta adecuado para su utilización en centros educativos públicos.
• de acuerdo con la reciente sentencia del Tribunal de Justicia de la Unión Europea de 16 de julio de 2020, que invalida la decisión sobre la adecuación de la protección conferida por el Escudo de la Privacidad UE-EE. UU, los centros educativos deben abstenerse de utilizar G Suite y cualquier otra aplicación o plataforma cuyo prestador del servicio tenga su sede social en EE. UU. hasta que se garantice el nivel de protección adecuado de la transferencia internacional de datos personales de acuerdo con el Reglamento General de Protección de Datos.

El G-Suite enterprise parece implicar instalar en CPD propio, y tiene un coste más elevado: al ser por usuario supondría mucho coste para toda una comunidad.

[gsuit pricing](https://gsuite.google.com/intl/es-419/pricing.html)  

**24 septiembre 2020**

[Educación prohíbe a institutos y colegios madrileños el uso de plataformas educativas gratuitas - elpais](https://elpais.com/espana/madrid/2020-09-24/educacion-prohibe-a-institutos-y-colegios-madrilenos-el-uso-de-plataformas-educativas-gratuitas.html)  
El Gobierno regional alega un problema de protección de datos mientras los directores advierten de que, sin ellas, no podrán impartir las clases on line a los casi 350.000 alumnos en semipresencialidad  
“Todo surgió a raíz de la queja de un padre al recibir el consentimiento informado de un instituto” de la capital para permitir que su hijo usara Google Suite, explica Álvarez, lo que obligó al centro “a consultar a la Comunidad si había algún problema”. Educación respondió que sí lo hay, y no menor: en un escrito del día 16, el director general de secundaria y FP afirma que el centro está “incumpliendo la normativa sobre protección de datos" porque “no está legitimado para suscribir un contrato de encargo de tratamiento en su propio nombre con el prestador del servicio", "está suscribiendo un contrato de adhesión que no ha sido supervisado por la consejería” y “de acuerdo con la [sentencia del Tribunal de Justicia de la UE](https://elpais.com/tecnologia/2020-07-16/la-justicia-europea-da-un-golpe-a-la-economia-digital-al-declarar-ilegal-el-acuerdo-entre-la-ue-y-ee-uu-para-transferir-de-datos.html) del 16 de julio, los centros deben abstenerse de utilizar cualquier aplicación o plataforma cuyo prestador del servicio tenga su sede social en EE UU hasta que se garantice el nivel de protección adecuado de la transferencia internacional de datos personales”.  
...  
Por su parte, fuentes de Google afirman que la resolución “contiene alegaciones que no son ciertas”. Su plataforma cuenta [con la certificación de nivel alto del Esquema Nacional de Seguridad](https://www.ccn.cni.es/index.php/es/docman/documentos-publicos/certificados-ens/124-certificado-de-conformidad-google-g-suite/file), los usuarios de Google Suite “no ven anuncios” y “son los propietarios de sus datos, no Google”, tampoco los venden a terceros y no comparten “información personal almacenada" en sus sistemas. En cuanto a la sentencia, según aclara [la propia Agencia Española de Protección de Datos](https://www.aepd.es/es/derechos-y-deberes/cumple-tus-deberes/medidas-de-cumplimiento/transferencias-internacionales/comunicado-privacy-shield#:~:text=El%2016%20de%20julio%20de,internacionales%20de%20datos%20a%20EEUU), el tribunal avaló el uso de las [cláusulas contractuales tipo](https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/standard-contractual-clauses-scc_es), que Google usa desde 2012, lo que a su juicio le permite seguir operando en la UE. La empresa confirma que está “trabajando con la Comunidad en un acuerdo”. Dicho convenio ha sido ya “revisado por Google” y solo queda que lo haga el Gobierno regional. En cualquier caso, su firma “responde a una formalidad y no afecta a las garantías" que ofrecen. “Estamos abiertos a colaborar con la consejería", concluye la tecnológica, que afirma dar gratis sus servicios por su compromiso “con una educación accesible”. De cobrarlos, estiman su coste en 10,40 euros por usuario al mes, 124,8 al año. Gobiernos de comunidades como Murcia, Cataluña, Navarra, Extremadura, Baleares y País Vasco usan la plataforma, así como universidades madrileñas como la Carlos III.

**25 septiembre 2020**

[twitter eossoriocrespo/status/1309425883564957696](https://twitter.com/eossoriocrespo/status/1309425883564957696)  
La Consejería de Educación y Juventud está trabajando para llegar a acuerdos con las principales plataformas que ofrecen servicios educativos, para que todos los alumnos y profesores de @ComunidadMadrid puedan hacer uso de las mismas con todas las garantías de seguridad y protección de datos.  

[Acta de Adimad 25 septiembre 2020](https://drive.google.com/file/d/15U2CkT3esIrE1-OYqyu2D8Dg0jnLUruy/view?usp=sharing)  
> 1º.-Plataformas educativas.  
Esteban Álvarez inicia la reunión explicando que, a raíz de la notificación del Servicio de Inspección al IES  Cervantes con  la  amenaza  de  apertura  de  expediente  si  no  se  dejaba  de  utilizar  de  forma inmediata  la  plataforma  Google  Suite  for  Education,  por  una  presunta  vulneración  de  la  Ley  de Protección de Datos, como presidente de ADIMAD:  
-Se ha reunido con la responsable en relaciones internacionales en Europa de Google.  
-Ha   mantenido   varias   conversaciones telefónicas   con   diferentes   responsables   de   la Consejería de Educación para solicitar explicaciones sobre la citada notificación.  
-Ha  manifestado  la  posición  de  la  Asociación  en  el  artículo  publicado  en  El  País  con  fecha 25/09/2020 “Educación  prohíbe  a  institutos  y  colegios  madrileños  el  uso  de  plataformas gratuitas”.  
El  Consejero  de  Educación,  D.  Enrique  Osorio, ha  replicado  a  este  artículo  de  forma  casi  inmediata en Twitter indicando  que  la  Consejería  de  Educación  y  Juventud  está  trabajando  para  llegar  a acuerdos con las principales plataformas que ofrecen servicios educativos  
Esteban ha tenido conocimiento, a través de diferentes fuentes, que la Consejería ha negociado con Aula Planeta para integrar materiales educativos en Educamadrid, que el convenio con Microsoft está muy avanzado y que se han iniciado las conversaciones con Google,suspendidas hace meses.  

**25 noviembre 2021**

Reunión de ADIMAD, se cita Google en acta

> 5. Nuevas instrucciones de Protección de Datos y convenio con google.
La delegación de protección de datos ha publicado unas nuevas instrucciones
firmadas por todos los directores generales en las que de nuevo se vuelve a
restringir el uso de plataformas distintas a educamadrid. Según han indicado los
responsables de Google a Esteban, tras el convenio firmado en junio las
condiciones técnicas para el uso de google por los centros están a punto de
concluir y la integración y su uso será viable en breve.  
En este sentido estamos pendientes de una reunión con los responsables de
google que están trabajando con la Consejería en esa integración para que nos
aclaren la situación. No entendemos primero el retraso cuando parecía que todo
estaba ya resuelto y nos preocupa la forma en que se articule el uso de la
plataforma. Manuel Suarez señala sobre este asunto que los centros
concertados están usando google de manera sistemática y puesto que forman
parte de la enseñanza pública deben regirse por los mismos criterios que el resto
de centros públicos.  
En todo caso, seguiremos insistiendo en la necesidad de que la Consejería firma
convenidos y ponga a su disposición, como están haciendo la mayoría de las
autonomías, las plataformas, programas, aplicaciones… para que cada centro
utilice después las que considere más oportunas.

**24 febrero 2022**

> Asunto:     Comunicado para trasladar a profesorado  
Fecha:     24-02-2022 12:15  
De:     Seccion de Acceso <seccion.acceso@uah.es>  
Destinatario:  
Rogamos que, ante el enorme volumen de profesores que están solicitando participar en los tribunales EvAU que no lo están realizando de acuerdo con las indicaciones previstas, comuniquen a su profesorado que lean atentamente las mencionadas indicaciones en la web, sobre todo la introducción de dos campos muy importantes:  
NOMBRE Y APELLIDOS: Tiene que escribirse igual que viene en el DNI  
**MAIL: tiene que ser correo institucional, no valen GMAIL ni YAHOO ni HOTMAIL ni otros.**  

### Reclamación a DPD Madrid y AEPD sobre uso Google en Madrid

En noviembre 2020 reclamo sobre el uso de Apple, y recibo respuesta de DPD en enero 2021 confirmando que no se puede usar.

[twitter FiQuiPedia/status/1350041031618781186](https://twitter.com/FiQuiPedia/status/1350041031618781186)  
Comparto [respuesta DPD sobre uso de dispositivos y aplicaciones de apple en centros públicos de @educacmadrid (anonimizada para quitar CSV y NIF en metadatos firma digital)](https://drive.google.com/file/d/1v-ODoZrrRaRMMxW7wcrDnjLnv7Ngqh3q/view)  

Intento hacer lo mismo con Google; para poder denunciar, debo citar casos concretos (en el caso de Apple tenía incluso documentación, ver hilo citado)

**4 diciembre 2020** 

Escribo correo a DPD (todavía no había recibido respuesta sobre Apple)

---

Como continuación del correo anterior, del que todavía no he recibido respuesta, envío este:

Existen centros públicos en Madrid que tienen proyectos educativos que implican usar aplicaciones de Google

IES Colmenarejo  http://ies.colmenarejo.educa.madrid.org/avisos/permanentes/PlantillaAvisoGoogleGSuite.pdf

IES Villaverde https://site.educa.madrid.org/ies.villaverde.madrid/wp-content/uploads/ies.villaverde.madrid/2020/06/CONSENTIMIENTO-USO-CUENTAS-GOOGLE.08.06.2020.docx

CEIP Eloy Saavedra https://www.educa2.madrid.org/web/centro.cp.eloysaavedra.ciempozuelos/plan-de-comunicacion1

CEIP Ángel León http://www.educa.madrid.org/web/cp.angelleon.colmenarviejo/Secretaria/Documentos/6_cuenta_plataforma_google_suite.pdf

CEIP Antonio Machado https://machadocolmenar.com/proyecto-tecnologico/#datos

CEIP Seis de diciembre http://www.educa.madrid.org/web/cp.seisdediciembre.colmenarejo/documentos/PGA1920.pdf

"Uso de aulas virtuales para familias y alumnos usando la plataforma de Google Classroom....Utilización de los Chromebooks y de los servicios de Google tanto por el profesorado como por los alumnos en todas las áreas y más concretamente en el área de Tecnología."

En acta ADIMAD 25 septiembre 2020 se indica "se han iniciado las conversaciones con Google, suspendidas hace meses."

En prensa 14 octubre 2020 se indica “Desde la Consejería de Educación y Juventud se está trabajando para poder firmar convenios con otras plataformas, entre ellas Google”, admite la administración. https://elpais.com/espana/madrid/2020-10-14/tambores-de-paz-entre-educacion-google-y-microsoft.html

Mi consulta es, dado que la Consejería de Educación está negociando con Google y es consciente de que se usa ahora en algunos centros, ya que es información pública:

1. Qué contratos realizados por centros ha autorizado la Consejería de Educación como responsable de tratamiento con Google

2. Qué garantías existen de cumplimiento de RPGD por el uso de Google, ya que es algo externo a EducaMadrid y almacena datos fuera de territorio UE.

3. Qué posibilidades hay de que otro centro pueda utilizar las mismos aplicaciones Google, dado que se está permitiendo en estos centros, mientras no haya un acuerdo formal de la Consejería.---

**30 enero 2021**

Sin haber recibido respuesta, pongo reclamación AEPD (vía https://sedeagpd.gob.es/sede-electronica-web/vistas/formNuevaReclamacion/nuevaReclamacion.jsf?QID=Q500&ce=0 que da reguardo de registro)

---

Reclamación por incumplimiento RGPD de Consejería de Educación de Madrid al no tener contrato escrito con encargado de tratamiento.

Tras escribir correo a DPD de la Consejería de Educación de Madrid el 3 diciembre 2020 sobre Google (lo adjunto), no he recibido ninguna respuesta, si bien sí he recibido respuesta asociada de tema similar de Apple que citaba en el correo (lo adjunto)

Mi resumen es que no existe constancia de un contrato con Google del que sí existe constancia de su uso en centros públicos.
En artículo 28 de Reglamento (UE) 2016/679

https://www.boe.es/buscar/doc.php?id=DOUE-L-2016-80807

indica en su punto 3

"El tratamiento por el encargado se regirá por un contrato u otro acto jurídico"

e indica en su punto 9

"El contrato u otro acto jurídico a que se refieren los apartados 3 y 4 constará por escrito, inclusive en formato electrónico."

En otras CCAA sí existe contrato con Google

Canarias http://sede.gobcan.es/boc/boc-a-2020-213-3753.pdf

Andalucía https://www.juntadeandalucia.es/export/drupaljda/convenio/21/01/Convenio_30.pdf

----

Se asocia Nº de Registro / Cod. Preinscripción "O00007128e2100003626" (el número largo se ve en justificante, en web se ve uno más corto y se puede repetir entre varias el corto)

**8 febrero 2021**

Recibo respuesta

[twitter FiQuiPedia/status/1358828671893651461](https://twitter.com/FiQuiPedia/status/1358828671893651461)  
Comparto [respuesta DPD sobre uso de aplicaciones de Google en centros públicos de @educacmadrid (anonimizada para quitar CSV y NIF en metadatos firma digital)](https://drive.google.com/file/d/1BSZy0zRsElttpkjY5_SnfEu1xAxjJ5-k/view?usp=sharing)  
Véase diferencia de respuesta Google vs Apple, ambas sin convenio vigente (Google tramitándose).

**19 abril 2021**

Uso "Ampliación de documentación de registro de entrada" 

https://sedeagpd.gob.es/sede-electronica-web/vistas/formProcedimientoEntrada/procedimientoEntrada.jsf

Aporto escrito que muestra que oficialmente la Consejería de Educación permitió usar Google Suite entre marzo 2020 y 1 julio 2020, sin tener acto jurídico escrito con Google como encargado de tratamiento.

---

Como añadido a O00007128e2100003626, quiero aportar la siguiente información:

-La Consejería de Educación puso por escrito que autorizaba el uso de G-Suite

https://www.educa2.madrid.org/web/revista-digital/recursos/-/visor/g-suite-for-education 

Se indica "Por ello ofrecen el paquete de G Suite for Education de forma gratuita, y con las funcionalidades Enterprise hasta el próximo 1 de julio."

Desde ahí se enlaza este documento, que tiene en metadatos fecha de creación 19 marzo 2020

https://www.educa2.madrid.org/web/educamadrid/principal/files/secondary/ecc1d908-0b92-4885-bbdc-1164c44b78c0/COMUNIDAD%20DE%20MADRID%20-%20Aprendizaje%20a%20distancia%20con%20G%20Suite%20for%20%20Education%20.pdf?t=1584612057241

Tal y como indiqué, no existe constancia de un acto jurídico de la Consejería de Educación como responsable de tratamiento con Google como encargado de tratamiento, aunque existe constancia, ahora oficial en la propia web de la comunidad, de su uso en centros públicos entre marzo 2020 y 1 julio. (El documento no indica de ninguna manera el año 2020, más allá de citar "en esta situación de excepcionalidad de suspensión de las aulas debido al coronavirus")

En artículo 28 de Reglamento (UE) 2016/679

https://www.boe.es/buscar/doc.php?id=DOUE-L-2016-80807

indica en su punto 3

"El tratamiento por el encargado se regirá por un contrato u otro acto jurídico"

e indica en su punto 9

"El contrato u otro acto jurídico a que se refieren los apartados 3 y 4 constará por escrito, inclusive en formato electrónico."

En dicho documento se indica

Paso 4: Solicita el alta prioritaria rellenando el formulario de uno de nuestros Partners Premier de Google for Education:

○Ieducando (contacto: google@ieducando.com)

○Xenon (contacto: google@xenon.es)

Enlazan unos formularios (uno ya no está disponible) 

En uno se pide consentimiento https://www.xenon.es/proteccion-datos/

---

**11 junio 2021**

Miro en sede AEPD y me indica finalizado!?

https://sedeagpd.gob.es/sede-electronica-web/vistas/consultaExpediente/listadoProcedimientos.jsf

Número de registro O00007128e2100003626   

Fecha de presentación 30/01/2021

Estado Finalizado

TAmbién aparece para escrito adicional de 19 de abril !?

O00007128e2100017576     Fecha de presentación 19/04/2021

Escribo a dpd@dpd.es 

---

Mirando en la sede, veo  
O00007128e2100003626 Fecha de presentación 30/01/2021 en estado finalizado  
O00007128e2100017576 Fecha de presentación 19/04/2021 en estado finalizado  
No me consta haber recibido ninguna comunicación asociada, y quería preguntar si se ha admitido a trámite o se ha rechazado y por qué, y dónde podría ver las notificaciones si han existido, actualmente en 060 solo veo 3 notificaciones de otros procedimientos que sí veo como finalizados.

---

Recibo esta respuesta automática

---

En relación con su correo electrónico recibido en la cuenta [dpd@aepd.es](mailto:dpd@aepd.es) de la Agencia Española de Protección de Datos (AEPD) se confirma su recepción y se comunica que este buzón de correo electrónico está destinado exclusivamente a la atención de consultas relacionadas con el ejercicio de los derechos de acceso, rectificación, cancelación, así como del resto de derechos reconocidos por el Reglamento Europeo de Protección de Datos en relación con los tratamientos de los que es responsable la AEPD.  
Para solicitar el ejercicio de los derechos de acceso, la rectificación, supresión o limitación del tratamiento de los datos personales o para oponerse a su tratamiento, conforme a lo dispuesto en el Reglamento General de Protección de Datos, así como en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personal y garantía de los derechos digitales, debe dirigirse a la Agencia Española de Protección de Datos, como responsable del tratamiento, C/Jorge Juan, 6, 28001- Madrid, o en el registro electrónico de la AEPD https://sedeagpd.gob.es/sede-electronica-web/vistas/formProcedimientoEntrada/procedimientoEntrada.jsf. La solicitud debe incluir la firma del interesado o acreditar la autenticidad de su voluntad expresada por cualquier medio (art. 66.1 Ley 30/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas).  
Puede consultar el correspondiente formulario, así como la información relativa al ejercicio de derechos en https://www.aepd.es/reglamento/derechos/index.html  

Así mismo, se informa de que desde este buzón no se procederá a dar curso y respuesta a cualquier otro asunto, para los que se puede utilizar los siguientes medios según la cuestión de que se trate:  
• La presentación de reclamaciones, o la realización de cualquier trámite relativo a procedimientos seguidos en esta Agencia, podrá efectuarse a través de la Sede Electrónica de la Agencia Española de Protección de Datos en el siguiente enlace:  
https://sedeagpd.gob.es/sede-electronica-web/vistas/formNuevaReclamacion/solicitudReclamacion.jsf;jsessionid=gWXKk7azoSlUEvdofjrTZ86w.  
• Para solicitar la retirada de determinadas imágenes de contenido sexual o que muestran actos de agresión, cuya difusión sin el consentimiento de las personas afectadas está poniendo en ALTO RIESGO sus derechos y libertades, y no se ha logrado su retirada a través de los canales especialmente previstos por el prestador de servicios, puede utilizar el servicio del Canal Prioritario: https://sedeagpd.gob.es/sede-electronica-web/vistas/formNuevaReclamacion/nuevaReclamacion.jsf?QID=Q600&ce=0.  
• Para cuestiones relacionadas con incidencias técnicas se puede contactar con los números de atención telefónica 901 100 099 - 912 663 517. También puede consultar las preguntas frecuentes: https://sedeagpd.gob.es/sede-electronica-web/vistas/infoSede/listadoFAQ.jsf?categoria=14  
Atentamente,

---

Lo presento vía registro O00007128e2100026521    

**17 junio 2021**

Pongo nuevo escrito vía registro. Me ciño a límite 1000 caracteres, sin adjuntar escrito, solo convenio

---

Incumplimiento en el uso de Google en Consejería de Educación Madrid.  
Mediante O00007128e2100026521 cité que en sede veía O00007128e2100003626 y O00007128e2100017576 finalizados sin haber recibido ninguna comunicación.

Quiero aportar la siguiente información que vuelve a mostrar incumplimiento: adjunto convenio firmado en junio 2021

Como he documentado en los registros citados, antes de la firma de este convenio, se estaban utilizando servicios de Google en educación Madrid, por lo que su uso era incorrecto, pero la firma de este convenio no resuelve el incumplimiento:

-Se siguen usando y el convenio indica en ANEXO I se debe publicar relación de servicios "con carácter previo al inicio de la prestación de los servicios objeto de este convenio"  
-Convenio cita en ANEXO I "comunicaciones que la Consejería les curse con motivo del alta en los servicios" cuando hay numerosos docentes y alumnos ya dados de alta en el mismo servicio que ahora se encuentra sujeto a convenio.

---

O00007128e2100027324

**25 agosto 2024**

Tras la noticia de AETD de agosto 2024 intento seguir su recomendación: "remitir el informe a la Consejería de Educación correspondiente en cada Comunidad Autónoma para que se pronuncie claramente sobre si se debe usar o no dicha plataforma educativa"

---

Asunto Informe AEPD  2023-0050 y uso GWS educación Madrid

EXPONE

En base a los siguientes antecedentes:   
- Informe 2024 de AEPD tras consulta sobre acuerdo INTEF y Google https://www.aepd.es/documento/2023-0050.pdf que concluye indicando  
En conclusión, se informa desfavorablemente la firma del Convenio y Anexos planteados por parte de la consultante, por los motivos que se han ido poniendo de manifiesto durante el presente informe.  
- RESOLUCIÓN Nº R24-045 DEL PROCEDIMIENTO DE INFRACCIÓN Nº PI23-025 de la Agencia Vasca de Protección de Datos https://www.avpd.eus/contenidos/resolucion_avpd/r24_045/es_def/adjuntos/PI23-025_RES_R24-045.pdf
- Expediente N.º: EXP202102527 Consejería Educación Canarias https://www.aepd.es/documento/ps-00176-2022.pdf

SOLICITA

Solicito a la Delegación de Protección de Datos de la Consejería de Educación de Madrid que en base al informe de AEPD https://www.aepd.es/documento/2023-0050.pdf  y resto de antecedentes indicados realice o revise la Evaluación de Impacto de Protección de Datos sobre Google WorkSpace for Education y se pronuncie claramente sobre si se debe usar dicha plataforma en la Consejería de Educación en cumplimiento de RGPD.  


---

Presento 59/291399.9/24 como formulario de solicitud genérica; vía registro, ya que correo de DPD no obliga a responder  
https://sede.comunidad.madrid/prestacion-social/formulario-solicitud-generica/tramitar

**11 noviembre 2024**  
Recibo respuesta fechada 8 noviembre
[2024-11-08-Respuesta+reclamaci%C3%B3n+DPD+Google.pdf)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2024-11-08-Respuesta+reclamaci%C3%B3n+DPD+Google.pdf)

Defiende los intereses de la CM cuando lo que debe defender son los derechos y libertades de las personas que están a su amparo.  
Destaca lo que está mal de los otros y argumenta que las legitimaciones son otras. La cuestión es el porqué son otras y si eso es admisible para Google.  
Dice que se ha hecho una EIPD. Habría que revisarla. Ese enlace no lo facilita, cono lo que se convierte en un acto de fe.  
Tampoco indica si se está supervisando el cumplimiento de las directrices  

Pido por transparencia la EIPD para reclamar ante la AEPD.  

---

Solicito copia o enlace a la Evaluación e Impacto de Protección de Datos (EIPD) realizada por la Comunidad de Madrid sobre Google WorkSpace for Education. 
Existe el derecho de acceso a la EIPD según https://www.consejodetransparencia.es/ct_Home/dam/jcr:dd247309-5708-4073-8dac-82e7ba082571/R%20CTBG%202023-0283%20%5BResoluci%EF%BF%BDn_expte.%20R-0917-2022%5D.pdf

---

43/539676.9/24

**9 diciembre 2024** 

Recibo [2024-12-09-Resolucion_de_accesoEIPDGoogle.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/Google/2024-12-09-Resolucion_de_accesoEIPDGoogle.pdf)

Con anexo  
[2024-12-09-Anexo_resolucion_de_accesoEIPDGoogle.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/Google/2024-12-09-Anexo_resolucion_de_accesoEIPDGoogle.pdf)  


### Reclamación AEPD sobre EIPD Google Madrid

**7 enero 2025**  
Planteo reclamar a AEPD

---

El 25 de agosto de 2024 presenté escrito a DPD de la Consejería de Educación de Madrid citando informe y resoluciones de AEPD y de Agencia Vasca de Protección de Datos solicitando que se realice o revise la Evaluación de Impacto de Protección de Datos sobre Google WorkSpace for Education y se pronuncie claramente sobre si se debe usar dicha plataforma en la Consejería de Educación en cumplimiento de RGPD. El contenido del escrito se reproduce en la respuesta recibida. 

El 11 de noviembre de 2024 recibí respuesta de DPD destaca lo que está mal de los otros y argumenta que las legitimaciones en Madrid son otras, sin justificar porqué son otras y si eso es admisible para Google, indicando que se ha hecho una EIPD.  [Adjunto 1](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/Google/2024-11-08-Respuesta+reclamaci%C3%B3n+DPD+Google.pdf)

El 9 de diciembre de 2024 recibí respuesta de la Consejería de Educación de Madrid [adjunto 2](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/Google/2024-12-09-Resolucion_de_accesoEIPDGoogle.pdf) remitiendo un anexo en el que recoge la información de EPID [adjunto 3](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/Google/2024-12-09-Resolucion_de_accesoEIPDGoogle.pdf)   

Planteo esta reclamación a AEPD ya que considero:

1º La EIPD aportada incumple el [artículo 35.7 del RGPD](https://www.boe.es/doue/2016/119/L00001-00088.pdf#page=53) en cuanto a contenido:

> 7. La evaluación deberá incluir como mínimo:  
a) una descripción sistemática de las operaciones de tratamiento previstas y de los fines del tratamiento, inclusive, cuando proceda, el interés legítimo perseguido por el responsable del tratamiento;  
b) una evaluación de la necesidad y la proporcionalidad de las operaciones de tratamiento con respecto a su finalidad;  
c) una evaluación de los riesgos para los derechos y libertades de los interesados a que se refiere el apartado 1, y  
d) las medidas previstas para afrontar los riesgos, incluidas garantías, medidas de seguridad y mecanismos que garanticen la protección de datos personales, y a demostrar la conformidad con el presente Reglamento, teniendo en cuenta los derechos e intereses legítimos de los interesados y de otras personas afectadas.  

Lo que Madrid envía como anexo es checklist de riesgos con medidas de seguridad pero no parece que se incluya la evaluación de necesidad y proporcionalidad ni lo que se aporte sirva para demostrar conformidad con el RGPD.

2º La EIPD aportada incumple el [artículo 24.1 del RGPD](https://www.boe.es/doue/2016/119/L00001-00088.pdf#page=47) en cuanto gestión del riesgo para los derechos y libertades que son obligación del responsable 

> Teniendo en cuenta … así como los riesgos de diversa probabilidad y gravedad para los derechos y libertades de las personas físicas, el **responsable del tratamiento** aplicará medidas técnicas y organizativas apropiadas a fin de garantizar y poder demostrar que el tratamiento es conforme con el presente Reglamento. 

[LA GESTIÓN DEL RIESGO PARA LOS DERECHOS Y LIBERTADES Y LA EIPD junio 2021](https://www.aepd.es/guias/gestion-riesgo-y-evaluacion-impacto-en-tratamientos-datos-personales.pdf)  

> En el caso de encargos de tratamiento, el RGPD establece la obligación de asistir al responsable a la hora de realizar la gestión del riesgo, teniendo en cuenta la naturaleza del tratamiento y la información a su disposición, en el artículo 28, en sus apartados 3.c, f y h, así como en el considerando 83. Para llevar a cabo de forma efectiva dichos deberes, el encargado debe llevar a cabo una gestión del riesgo para los derechos y libertades al menos dentro de los límites del objeto del encargo.  
Por otro lado, un desarrollador o suministrador de un producto o servicio con el que varios responsables van a realizar distintos tratamientos de datos puede llevar a cabo una gestión del riesgo para los derechos y libertades. En ese caso los riesgos identificados y las medidas adoptadas deben incorporarse al proceso de gestión del riesgo del tratamiento que pretenda realizar el responsable. Este proceso debería de ampararse en las garantías contractuales correspondientes a la adquisición del producto o servicio.  
En cualquier caso, con independencia de la gestión realizada por los encargados, desarrolladores o suministradores, el responsable seguirá manteniendo la obligación de realizar su propia gestión del riesgo, o una EIPD cuando las operaciones de tratamiento que lleve a cabo puedan entrañar un alto riesgo para los derechos y libertades de los interesados.  
> J. LA GESTIÓN DEL RIESGO PARA LOS DERECHOS Y LIBERTADES Y LA EIPD  
> El RGPD establece las obligaciones relativas a la evaluación de impacto relativa a la protección de datos (EIPD), fundamentalmente, en los artículos 35 y 36. Como establece el artículo 35:  
“Cuando sea probable que un tipo de tratamiento, …, entrañe un alto riesgo para los derechos y libertades de las personas físicas, el responsable del tratamiento realizará, …, una evaluación del impacto”.  
El RGPD no obliga a que para cualquier tratamiento de datos personales sea necesario realizar una EIPD, pero sí establece que es obligatorio que se realice cuando hay una probabilidad de que entrañe un alto riesgo. La existencia de un grado razonable de presunción de que el tratamiento puede entrañar un alto riesgo hace imprescindible la realización de una EIPD.  
>1. Definición de EIPD como proceso que obliga a actuar  
En el texto del Reglamento no aparece una definición para el término “evaluación de impacto relativa a la protección de datos” o EIPD. El CEPD sí desarrolla la definición de EIPD en las Directrices WP248 como:  
“… un proceso concebido para describir el tratamiento, evaluar su necesidad y proporcionalidad y ayudar a gestionar los riesgos para los derechos y libertades de las personas físicas derivados del tratamiento de datos personales evaluándolos y determinando las medidas para abordarlos.”   
Según esto, el CEPD considera que la EIPD, es un “proceso” y, por tanto:  
>• Reducir la EIPD a una actividad puntual y aislada en el tiempo es incompatible con el concepto de proceso que interpreta las Directrices WP248.   
>• La EIPD ha de estar documentada, pero la EIPD es más que el informe que refleja sus resultados.   
>• La EIPD ha de evaluar los riesgos “determinando las medidas para abordarlos”. La EIPD obliga al responsable a actuar y tiene una dimensión mayor que un mero formalismo plasmado en un documento sobre el que se puedan realizar cambios mínimos para adaptarlo a cualquier tratamiento.  
La EIPD es un proceso de análisis de un tratamiento que se extiende en el tiempo, a lo largo de todo el ciclo de vida de un tratamiento de datos personales, y que se ha de revisar de forma continua, “al menos cuando exista un cambio del riesgo que
representen las operaciones de tratamiento” (art.35.11 del RGPD).  

3º La EIPD aportada en 2024 está fechada el 31 de mayo de 2021, cuando la EIPD es un proceso de análisis de un tratamiento que se extiende en el tiempo.  

Tal y como se ha documentado en punto 2º la EIPD se ha de revisar de forma continua.  
Deberían afirmar categóricamente que las condiciones del tratamiento han sido inmutables desde el 2021 hasta 2024 o aportar la documentación más reciente sobre seguridad y EIPD.  
Por ejemplo la EIPD fechada en 2021 indica en varios casos "medida planificada" y sería razonable una revisión en 2024 de si la medida se puede considerar implantada.  

4º La EIPD aportada asigna probabilidad para las actividades sin argumentar en base a qué criterios objetivos se considera ese nivel. 

Se asigna probabilidad "despreciable" en todos los casos salvo en uno que se indica probabilidad "limitada" sin argumentar en base a qué criterios objetivos se considera ese nivel. 

5º La EIPD aportada asigna impactos para las actividades sin argumentar en base a qué criterios objetivos se considera ese nivel.

Se asigna impacto "despreciable" en todos los casos salvo en uno que se indica impacto "limitado" sin argumentar en base a qué criterios objetivos se considera ese nivel. 

6º La EIPD aportada asigna riesgo inherente para las actividades sin argumentar en base a qué criterios objetivos se considera ese nivel.

Se asigna riesgo inherente "bajo" en todos los casos salvo en uno que se indica riesgo inherente "limitado" sin argumentar en base a qué criterios objetivos se considera ese nivel. 

7º La EIPD aportada no evalúa la necesidad y proporcionalidad  

[LA GESTIÓN DEL RIESGO PARA LOS DERECHOS Y LIBERTADES Y LA EIPD junio 2021](https://www.aepd.es/guias/gestion-riesgo-y-evaluacion-impacto-en-tratamientos-datos-personales.pdf)  

> El CEPD sí desarrolla la definición de EIPD en las Directrices WP248 como:  
“… un proceso concebido para describir el tratamiento, evaluar su necesidad y proporcionalidad y ayudar a gestionar los riesgos para los derechos y libertades de las personas físicas derivados del tratamiento de datos personales evaluándolos y determinando las medidas para abordarlos.”   

No parece que se haya hecho lo indicado anteriormente de juzgar la necesidad y proporcionalidad en la EIPD y en el convenio. Una de las exigencias del artículo 35 del RGPD con relación a la EIPD, es la que aparece en el apartado 35.7.b del RGPD, la obligación de que se realice “una evaluación de la necesidad y la proporcionalidad de las operaciones de tratamiento en cuanto a su finalidad”. 

La AEPD considera que la base de legitimación para el tratamiento de datos personales del convenio que se pretende establecer podría ser tanto la RGPD 6.1.c) como la RGPD 6.1.e), siendo en el caso del convenio de la Comunidad de Madrid la RGPD 6.1.e).

Si se hubiese hecho un juicio de necesidad no sería consistente con la [finalidad descrita en el RAT](https://www.comunidad.madrid/info/rgpd?descripcion=Proporcionar+al+alumnado&sort_by=timestamp&f%5B0%5D=consejeria_path%3AConsejer%C3%ADa+de+Educaci%C3%B3n%2C+Ciencia+y+Universidades)  
> “Proporcionar al alumnado y los docentes entornos de aprendizaje online, así como herramientas de comunicación (correo electrónico educativo, mensajería instantánea, etc.), de colaboración (cloud, webs, foros, etc.) y de elaboración de materiales educativos (gestor de contenidos).”  

Ya que la Consejería dispone de la plataforma EducaMadrid que aporta la misma solución a la necesidad y es institucional y, por tanto, menos intrusiva limitando el riesgo.  

Además, no se puede amparar en la misma base legal el acuerdo con Google porque la necesidad ya está cubierta. Luego se requeriría también del consentimiento para poder usarlo.

Tampoco pasaría el juicio de proporcionalidad porque se están facilitando hasta tres soluciones para el mismo tratamiento: EducaMadrid, Microsoft y Google lo que infiere además añadir riesgos innecesarios al comunicar datos a dos entidades externas aparte de la institucional.

8º La EIPD aportada no refleja si se están poniendo garantías y salvaguardas para demostrar el cumplimiento del las directrices de uso 

Es bastante frecuente que se use Google por los equipos directivos y profesorado (DPD puede comentar los casos analizados) sin el respeto a las directrices. En una responsabilidad proactiva se debe poder demostrar su cumplimiento y si no se cumplen se tienen dos factores negativos:  
>• Se tienen brechas de seguridad que no se están informando de ellas.  
>• Queda clara que la decisión de probabilidad despreciable utilizada en la documentación aportada por en la EIPD no está basada en criterios realistas y objetivos. Las salvaguardas “Formación apropiada del personal sobre protección de datos y comunicación auditable y clara de las responsabilidades del personal en relación con el cumplimiento de las políticas de privacidad de la organización así como de las sanciones aparejadas al incumplimiento de las mismas” o no se están llevando a cabo o no son efectivas.  

9º La EIPD aportada tiene partes literalmente redactadas por Google, lo que lo hace un documento poco serio e indica poca revisión 

En la actividad 

>"En caso de que los datos de carácter personal que trata sean cedidos o transferidos internacionalmente, ¿existe el riesgo de acceso secreto a los datos por parte de autoridades de terceros países?" 

La respuesta indica en varias ocasiones un texto inequívocamente redactado por Google  

> ...Si aún así desde Google **estamos** obligados a responder... **notificaremos** ... **atenderemos** la petición del cliente ... **gestionamos** las solicitudes de información por parte de autoridades... Tal y como puede
comprobarse en **nuestros** informes de transparencia  

10º La EIPD aportada indica que se tratan datos personales, pero no se ha evaluado el riesgo de la inferencia de datos personales por centro educativo y clase.  

Se tratan datos personales: Nombre y apellidos, dirección electrónica, centro educativo, clase

Según el centro se inferen datos  
- Si es un colegio de primaria se deduce que es un menor  
- Si es un centro de educación especial se deduce que sufre de algún tipo de trastorno de aprendizaje o de salud. 
- Si es un centro  UFIL (Unidades de Formación e Inserción Laboral) se deduce que tiene riesgo de exclusión social.
- Si es un centro CREI (Centro Regional de Enseñanzas Integradas) se deduce que tiene medidas jurídicas 

Es decir, que hay datos sensibles o incluso especiales que deben tener una protección especial y estar amparados en su tratamiento por alguna de las excepciones previstas en el artículo 9 o que les afecta el artículo 10 del RGPD.

---

Otras ideas que no incorporo:

La AEPD debería fijar un criterio claro sobre porqué en qué casos es admisible Google, ya que resulta extraño que distintas CCAA para la misma aplicación y usos teóricamente similares utilizan argumentos distintos.  

¿No se basa en el consentimiento pero se tratan datos de menores de 14 años?  

Si es 6.1 e), ¿se ha informado claramente del derecho de oposición a las familias?  


**8 enero 2025**  
Presentado con número de registro O00007128e25P0000209

### Convenio Google con Consejería Educación Madrid

**14 junio 2021**

Se anuncia acuerdo de la consejería de educación con Google

[Firmamos un acuerdo para que los centros educativos puedan utilizar los servicios de Google España](https://www.comunidad.madrid/noticias/2021/06/14/firmamos-acuerdo-centros-educativos-puedan-utilizar-servicios-google-espana)  

No se habla de Gsuite, sino de "Workspace for Education, en su versión básica"

Parece el nuevo nombre surgido en febrero 2021

[More options for learning with Google Workspace for Education](https://www.blog.google/outreach-initiatives/education/google-workspace-for-education)  

La "versión básica" sería la "Fundamentals" que sería gratuita con un almacenamiento compartido de 100TB. Por ver si es para toda la comunidad o por centro.

https://edu.google.com/products/workspace-for-education/

**17 junio 2021**

Consigo acceso al convenio. Tiene CSV que permite recuperar versión original con firmas manuscritas y NIF de Ossorio en metadatos, por lo que no comparto tal cual, sino anonimizado

[CONVENIO DE COLABORACIÓN ENTRE LA COMUNIDAD DE MADRID (CONSEJERÍA DE EDUCACIÓN Y JUVENTUD) Y GOOGLE IRELAND LIMITED PARA CONTRIBUIR A LA MEJORA DE LA COMPETENCIA DIGITAL EN EL ÁMBITO EDUCATIVO MEDIANTE EL USO DE WORKSPACE FOR EDUCATION](http://cloud.educa.madrid.org/index.php/s/zmCoA2McI8UrlZq)  

Veo que cita 5 veces EducaMadrid: la primera aclara que Google es complemento, y 3 veces para indicar que se deben publicar cosas en EducaMadrid, y una quinta para referenciar los datos tratados.

> CUARTO.- Que la Consejería de Educación y Juventud de la Comunidad de Madrid y Google están interesados en colaborar conjuntamente en el desarrollo de la innovación tecnológica facilitando el acceso de los centros de enseñanza de la Comunidad de Madrid a herramientas que integran las tecnologías de la información y la comunicación en el proceso educativo, como complemento de las que integran la plataforma corporativa EducaMadrid.

> TERCERA.- ACTUACIONES DE LAS PARTES  
...  
> 1. Impulsar la transformación digital en los centros educativos de la Comunidad de Madrid  
...  
Corresponderá a la Consejería establecer las limitaciones a las que deben ceñirse los usuarios a la hora de utilizar los servicios ofrecidos por Google, que serán aquellas que se infieran del análisis de riesgos del tratamiento de datos personales realizado por la Consejería, que las publicará en el portal EducaMadrid para conocimiento de la comunidad educativa, junto con las pautas o instrucciones aplicables al respecto y la política de privacidad correspondiente. Google no controlará el cumplimiento de las pautas o instrucciones por los usuarios, pero pondrá a disposición de la Consejería las funcionalidades pertinentes que permitan a los administradores del servicio velar por su cumplimiento.  
...  
> 6. Política de uso  
...  
Los términos de servicio acordados por las partes sobre el uso de Workspace for Education, que se incluyen como Anexo II a este convenio y que se publicarán también en EducaMadrid (“Términos de Workspace for Education”) 

> ANEXO I  
SERVICIOS DE LA SOLUCIÓN WORKSPACE FOR EDUCATION  
...  
La Consejería de Educación y Juventud establecerá y hará pública en el portal de EducaMadrid la relación de servicios que habilitará para el uso que conlleve menor riesgo para la privacidad de los datos, con la colaboración del encargado del tratamiento y **con carácter previo** al inicio de la prestación de los servicios objeto de este convenio, como consecuencia del análisis de riesgos realizado por el responsable del tratamiento.  
Categorías de datos  
Datos relativos a personas proporcionados a Google a través de los Servicios, por la Consejería o los Usuarios Finales, que, de acuerdo con el RGPD, serán los mínimos posibles y de carácter identificativo, incluyeno entre otros, los datos personales correspodientes a las Actividades de Tratamiento con identificadores 2.A.13 y 2.C.24, denominadas “Utilización por parte del profesorado y del alumnado, en el ejercicio de la función educativa, de las herramientas y recursos de la plataforma educativa de servicios online de la Consejería de Educación e Investigación (EducaMadrid)”, de las cuales esa Dirección General es responsable del tratamiento junto con las Direcciones Generales de Educación Infantil y Primaria y Educación Secundaria, Formación Profesional y Régimen Especial, respectivamente. Ambas actividades están incluidas en el Registro de Actividades de Tratamiento de la Consejería  
Planteo solicitud de transparencia, y por ver reclamación a AEPD: mientras no esté el análisis y esté publicado, según convenio no se puede usar.  

---

Solicito enlace la publicación en EducaMadrid de la siguiente información, que es citada en el documento CONVENIO DE COLABORACIÓN ENTRE LA COMUNIDAD DE MADRID (CONSEJERÍA DE EDUCACIÓN Y JUVENTUD) Y GOOGLE IRELAND LIMITED PARA CONTRIBUIR A LA MEJORA DE LA COMPETENCIA DIGITAL EN EL ÁMBITO EDUCATIVO MEDIANTE EL USO DE WORKSPACE FOR EDUCATION:  
1. Análisis de riesgos del tratamiento de datos personales realizado por la Consejería (citado en TERCERA.- ACTUACIONES DE LAS PARTES, 1. Impulsar la transformación digital en los centros educativos de la Comunidad de Madrid)  
2. Los términos de servicio acordados por las partes sobre el uso de Workspace for Education (citado en TERCERA.- ACTUACIONES DE LAS PARTES, 6. Política de uso)  
3. Relación de servicios que habilitará para el uso que conlleve menor riesgo para la privacidad de los datos (citado en ANEXO I SERVICIOS DE LA SOLUCIÓN WORKSPACE FOR EDUCATION)  
Solicito copia o enlace a análisis de riesgos realizado por el responsable del tratamiento (citado en ANEXO I SERVICIOS DE LA SOLUCIÓN WORKSPACE FOR EDUCATION)  

---

49/254083.9/21

**24 junio 2021**

Se publica en BOCM  
[CONVENIO de colaboración de 10 de junio de 2021, entre la Comunidad de Madrid (Consejería de Educación y Juventud) y Google Ireland Limited, para contribuir a la mejora de la competencia digital en el ámbito educativo mediante el uso de Workspace for Education](https://www.bocm.es/boletin/CM_Orden_BOCM/2021/06/24/BOCM-20210624-33.PDF)  

[Google España y la Comunidad unen fuerzas para fomentar la innovación en Educación - magisnet](https://www.magisnet.com/2021/06/google-espana-y-la-comunidad-unen-fuerzas-para-fomentar-la-innovacion-en-educacion/)  

**2 agosto 2021**

Respuesta 

[twitter FiQuiPedia/status/1422197742244438024](https://twitter.com/FiQuiPedia/status/1422197742244438024)  
[Respuesta @educacmadrid sobre datos Google y MS](https://drive.google.com/file/d/1SIUYuZf2MJ_8qLHCEmqiuTVWLJ7ocV43/view?usp=drivesdk)  
Para Google indican  
**"aunque se ha firmado un convenio de colaboración, todavía no se ha implantado ningún servicio por lo que no se ha realizado ningún encargo de tratamiento de datos."**

**4 agosto 2021**

Escribo a DPD

---

En respuesta recibida (obvio comentario sobre error en DG que responde / DG citada en RAT ya citado en correo anterior sobre Microsoft), adjunto captura, se indica

"aunque se ha firmado un convenio de colaboración, **todavía no se ha implantado ningún servicio** por lo que no se ha realizado ningún encargo de tratamiento de datos."

Además de los centros en los que se ha estado utilizando Google, según informé al DPD y no negó en la contestación de 8 febrero 2021, quiero llamar la atención sobre esta información, albergada en EducaMadrid

https://www.educa2.madrid.org/web/revista-digital/educacion-a-distancia/-/visor/g-suite-for-education

donde se enlaza

https://www.educa2.madrid.org/web/educamadrid/principal/files/secondary/ecc1d908-0b92-4885-bbdc-1164c44b78c0/COMUNIDAD%20DE%20MADRID%20-%20Aprendizaje%20a%20distancia%20con%20G%20Suite%20for%20%20Education%20.pdf?t=1584612057241

esa página no tiene fecha en la web de EducaMadrid, pero desde hace aproximadamente un año se enlaza desde aquí

https://www.change.org/p/consejero-de-educaci%C3%B3n-de-la-comunidad-de-madrid-g-suite-educaci%C3%B3n-para-los-centros-educativos-p%C3%BAblicos-de-la-comunidad-de-madrid

Considero un hecho que se usan herramientas de Google cediendo datos y que la Consejería lo ha publicitado, por lo que la respuesta de que no se ha implantado ningún servicio no es correcta y muestra que se utilizan sin un encargo de tratamiento de datos.

---

**5 agosto 2021**

Respuesta DPD

---

Estimado Enrique,

La responsabilidad para establecer las directrices sobre el uso de las plataformas educativas es de la DG de Bilingüismo y Calidad de la Enseñanza, y se corresponde con la actividad de tratamiento de su RAT "Utilización por parte del profesorado y del alumnado, en el ejercicio de la función educativa, de herramientas y recursos no corporativos".

Pero el control del cumplimiento es responsabilidad de las DG de Educación Infantil, Primaria y Educación Especial y la DG de Educación Secundaria, FP u RE, que tienen competencia sobre la actividad de los centros educativos en función de las enseñanzas que aquellos imparten, tal como consta en sus respectivos RAT como "Utilización por parte del profesorado y del alumnado,  en el ejercicio de la función educativa, de las herramientas y recursos de la plataforma educativa  de servicios online  de la Consejería de Educación e Investigación (EducaMadrid y otras plataformas autorizadas de uso complementario)".

Por lo tanto, no se trata de un error.

Actualmente el uso de datos por Microsoft está amparado por el convenio de colaboración que conocemos, que ha sido muy complejo elaborar y ha producido un indeseado retraso en la aprobación de este. No obstante, el estado de alarma permitía la contratación de emergencia, lo que no exime de tramitar el instrumento jurídico que legitime el procedimiento con posterioridad.

En cualquier caso, el principio de responsabilidad proactiva que se aplica al responsable del tratamiento exige un control y supervisión continua del cumplimiento de los requisitos que garantizan la seguridad, confidencialidad y privacidad de los datos, para adoptar las medidas necesarias que eviten o mitiguen las posibles consecuencias sobre los derechos fundamentales y libertades públicas de las personas afectadas.

Si usted considera que se están vulnerando en la actualidad cualquiera de sus derechos o libertades por el uso inadecuado de la plataforma Microsoft 365, por favor, indique en su solicitud al responsable su oposición al tratamiento en el caso de que sea posible y el perjuicio ocasionado para que pueda adoptar las medidas necesarias.

En espera de que esta información te resulte de utilidad, recibe un cordial saludo.

---

**10 agosto 2021**

Recibo [respuesta a mi solicitud 09-OPEN-00094.6/2021 junio 2021](https://drive.google.com/file/d/1_OIG8jFZ1sUXEyeKjfW6bUnqyZD4seT8/view?usp=sharing)  

Informar al ciudadano de que no existe el enlace con la información que solicita ya que este proyecto no ha sido desplegado en la actualidad en los centros educativos de la Comunidad de Madrid. Cuando se lleve a cabo este despliegue se publicará en la página https://www.educa2.madrid.org/recursos, como se ha hecho con otras plataformas complementarias con las que se ha suscrito convenio.  
Quedará excluida de esta publicación el análisis de riesgos que indica en su solicitud, por considerarse un documento interno de trabajo entre la Dirección General y la Delegación de Protección de Datos de la Consejería.

**20 septiembre 2021**

Comienzo un hilo en Twitter donde aproximadamente cada semana intento indicar que no está publicado lo asociado a Google

[twitter FiQuiPedia/status/1440048688508784653](https://twitter.com/FiQuiPedia/status/1440048688508784653)  
Informativo para docentes, padres y alumnos Madrid: a 20 septiembre 2021 sigue sin ser correcto usar Google Workspace (antes GSuite) en @educacmadrid ya que no se ha publicado nada asociado en https://educa2.madrid.org/recursos, aunque se firmase convenio en junio.

**27 septiembre 2021**

Veo curso asociado a Google  
[twitter FiQuiPedia/status/1442560013516611585](https://twitter.com/FiQuiPedia/status/1442560013516611585)  
Es llamativo que uno se pueda inscribir sin ser todavía correcto su uso.  
[https://innovacionyformacion.educa.madrid.org/node/24787](https://innovacionyformacion.educa.madrid.org/node/24787)  
Y que colabore un centro que pone por escrito que usa Google desde 2018 y no pase nada.
[Proyecto de innovación educativa IES Gerardo Diego](https://iesgerardodiego.com/wp-content/uploads/2019/09/PROYECTO-INNOVACION-EDUCATIVA-GD.pdf)  

**25 octubre 2021**

El curso empieza (y luego termina) sin haberse publicado nada

[twitter FiQuiPedia/status/1452636861113978886](https://twitter.com/FiQuiPedia/status/1452636861113978886)  
Día 19 octubre en @educacmadrid comenzó un curso de Google Workspace (antes Gsuite) que todavía no se puede usar, por lo que me imagino que no lo usarán durante el curso, que termina día 9 noviembre  
¿Qué estarán haciendo?
[https://innovacionyformacion.educa.madrid.org/node/24787](https://innovacionyformacion.educa.madrid.org/node/24787)
![](https://pbs.twimg.com/media/FCjNhroWUBAgyj-?format=jpg)  

**20 noviembre 2021**

Escribo a DPD

---

Buenas noches

Planteo, añadiendo el contexto después, unas preguntas de las que solicito respuesta oficial, así como el permiso para compartirla:

¿Es correcto que un centro público realice una redirección entre el correo de EducaMadrid y el correo de Google Workspace? ¿en algún momento y bajo algunas condiciones la cuenta de Google Workspace deja de ser "ajena a la Comunidad de Madrid"? ¿es correcta la redirección antes de que se publique información en EducaMadrid sobre uso de Google? ¿será correcta la redirección después de que se publique información en EducaMadrid sobre uso de Google?

El contexto de las preguntas es el siguiente:

En documento [REDIRECCIÓN DEL CORREO ELECTRÓNICO DE EDUCAMADRID A CUENTAS DE CORREO
AJENAS A LA COMUNIDAD DE MADRID, elaborado por DPD Educación](https://dpd.educa2.madrid.org/web/educamadrid/principal/files/69a6601a-e199-4519-a2eb-192663576204/Informes/20210817%20Prohibici%C3%B3n%20redirecci%C3%B3n%20correo%20corporativo.pdf?t=1629196614468)  

se indica "toda persona usuaria de cuentas de correo de EducaMadrid deberá abstenerse de activar la
redirección de sus mensajes a cuentas de correo particulares ajenas a la Comunidad de Madrid"

Dado que la Comunidad de Madrid firmó un convenio con Google en junio 2021, y el 10 de agosto 2021, en respuesta a solicitud de acceso a información pública 09-OPEN-00094.6/2021 realizada el 24 junio 2021, en la que solicitaba "3. Relación de servicios que habilitará para el uso que conlleve menor riesgo para la privacidad de los datos (citado en ANEXO I SERVICIOS DE LA SOLUCIÓN WORKSPACE FOR EDUCATION)", la D.G. de Bilingüismo y Calidad de la Enseñanza (Educación, Universidades, Ciencia y Portavocía) respondió

"Informar al ciudadano de que no existe el enlace con la información que solicita ya que este proyecto no ha sido desplegado en la actualidad en los centros educativos de la Comunidad de Madrid. Cuando se lleve a cabo este despliegue se publicará en la página https://www.educa2.madrid.org/recursos, como se ha hecho con otras plataformas complementarias con las que se ha suscrito convenio."

A día de hoy, 20 noviembre 2021, no hay nada publicado sobre Google en la dirección indicada, y convenio firmado en junio se indica que esa publicación se realizará *con carácter previo al inicio de la prestación*

"ANEXO I
SERVICIOS DE LA SOLUCIÓN WORKSPACE FOR EDUCATION

...

La Consejería de Educación y Juventud establecerá y hará pública en el portal de EducaMadrid la relación de servicios que habilitará para el uso que conlleve menor riesgo para la privacidad de los datos, con la colaboración del encargado del tratamiento y con carácter previo al inicio de la prestación de los servicios objeto de este convenio, como consecuencia del análisis de riesgos realizado por el responsable del tratamiento."

Un saludo

Enrique

---

**26 noviembre 2021**

Respuesta DPD

---

Estimado Enrique,

Tal como señalas, aún no se han publicado las instrucciones de uso y la forma en que van a ser integradas las cuentas de usuario en *Workspace for Education* . Hasta ese momento los centros educativos no deben iniciar el uso de esta plataforma o continuar haciéndolo, porque sería un contrato de adhesión privado entre el centro educativo y Google, y esta fórmula de uso no está amparada por el Convenio. El centro estaría asumiendo una responsabilidad que no le corresponde en materia de protección de datos personales, cuyo responsable del tratamiento es la Dirección General de Bilingüismo y Calidad de la Enseñanza.

En relación con el uso de aplicaciones externas, las instrucciones sobre protección de datos, tanto las del curso pasado como las que acaban de publicarse y difundirse entre los centros educativos, informan claramente en su apartado 6 de cómo deben proceder los centros.

En espera de que esta información te resulte de utilidad, recibe un cordial saludo. 

---

**5 diciembre 2021**

Planteo solicitud

---

Solicito copia o enlace a las actas y otra documentación pública si procede de las reuniones de la Comisión Mixta de seguimiento del convenio entre Google y la Consejería de educación que se firmó en junio 2021 y que convenio indica que se reunirá como mínimo con carácter semestral

---

**4 enero 2022**

Recibo [respuesta 09-OPEN-00176.7/2021](https://drive.google.com/file/d/1DZ_aYfYnEDUf7SXVQYVGat5hRd2aN5QU/view). Indican que no ha habido ninguna reunión

**10 enero 2022**

Se publica primero

[Google Workspace for Education: Madrid](https://dgbilinguismoycalidad.educa.madrid.org/googlemadrid/index.php/index/info)  

y luego en 

[EducaMadrid. Recursos. Google](https://www.educa2.madrid.org/recursos#exe-tab-0-2)  

Las aplicaciones y herramientas a las que se hace referencia en este apartado **no están alojadas en los servidores de EducaMadrid** . En consecuencia, su utilización en los centros educativos habrá de regirse por la política de privacidad y protección de datos establecida por esta Consejería para el uso de plataformas ajenas a EducaMadrid. Para problemas relacionados con estas aplicaciones y herramientas contactar con el correo: [directrices.innovacion@educa.madrid.org](mailto:directrices.innovacion@educa.madrid.org)

La utilización de los servicios de Google **se limitará a fines educativos y de docencia** . **No deberá utilizarse** para **albergar información personal** no relacionada directamente con la actividad educativa o formativa, ni para **albergar documentación sobre el historial académico** , **valoraciones de pruebas, calificaciones de trabajos, etc** .

[Directrices de uso de los servicios de Google Workspace for Education en centros públicos de la Comunidad de Madrid](https://www.educa2.madrid.org/web/educamadrid/principal/files/0cf74207-b6c3-42dd-af3c-e9fcf4ab433b/directrices_uso_google.pdf?t=1641817246397)

[Política de privacidad sobre el uso de aplicaciones y plataformas puestas a disposición de la comunidad educativa por la Consejería de Educación, Universidades, Ciencia y Portavocía v3](https://www.educa2.madrid.org/web/educamadrid/principal/files/0cf74207-b6c3-42dd-af3c-e9fcf4ab433b/20210426%20POLITICA%20DE%20PRIVACIDAD%20DE%20LAS%20PLATAFORMAS%20EDUCATIVAS%20V3.pdf)

**2 febrero 2022**

Se publican nueva versión documento

[Política de privacidad sobre el uso de aplicaciones y plataformas puestas a disposición de la comunidad educativa por la Consejería de Educación, Universidades, Ciencia y Portavocía v4](https://www.educa2.madrid.org/web/educamadrid/principal/files/0cf74207-b6c3-42dd-af3c-e9fcf4ab433b/20220119%20POLITICA%20DE%20PRIVACIDAD%20DE%20LAS%20PLATAFORMAS%20EDUCATIVAS%20V4.pdf?t=1643803297935)

>Por ejemplo, en el caso de Workspace for Education no se utilizará Classroom como cuaderno del profesor, puesto que esta función debe desarrollarse mediante las herramientas correspondientes de Raíces y de EducaMadrid. No se utilizará Drive para alojar trabajos personales de los alumnos, ya que para ello cuentan con el Aula Virtual y Cloud de EducaMadrid. Tampoco debe utilizarse Meet salvo en situaciones verdaderamente excepcionales, es decir, cuando las herramientas de videoconferencia Jitsi y Webex integradas en EducaMadrid no se encuentren operativas.  
El cumplimiento de estas normas es imperativo, dado que las aplicaciones o plataformas pueden transferir los datos que aloje esta plataforma a cualquier país que no ha sido declarado seguro por la Comisión Europea, e incluso que ha sido declarado no seguro en materia de privacidad, como es el caso de Estados Unidos, de ahí el especial cuidado que debe observarse cuando se alojen datos en cualquiera de los servicios del prestador.

**abril 2022**

[POLÍTICA DE PRIVACIDAD SOBRE EL USO DE APLICACIONES Y PLATAFORMAS PUESTAS A DISPOSICIÓN DE LA COMUNIDAD EDUCATIVA POR LA CONSEJERÍA DE EDUCACIÓN, UNIVERSIDADES, CIENCIA Y PORTAVOCÍA v6](https://rss.educa2.madrid.org/web/educamadrid/principal/files/0cf74207-b6c3-42dd-af3c-e9fcf4ab433b/20220208%20POLITICA%20DE%20PRIVACIDAD%20DE%20LAS%20PLATAFORMAS%20EDUCATIVAS%20V6.pdf?t=1649331832998)  

