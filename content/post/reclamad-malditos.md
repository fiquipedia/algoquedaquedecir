+++
author = "Enrique García"
title = "¡Reclamad, malditos!"
date = "2018-07-31"
tags = [
    "reclamar", "transparencia"
]
toc = true

description = "¡Reclamad, malditos!"
thumbnail = "https://blogger.googleusercontent.com/img/a/AVvXsEjokWLL415McRC6C9lNKntEUZQJ2T-FrtSSa8ANU_q3xe77IgdzCChJjxdC9UZ9SuiqcAnqAaSEnHsQO5YRJdBrOltAJuy52sQRgPRsdhLtuD9rTDgCw3QxUUhP3TiG7wK5kZ-jUm0M7kA6s8K017EPlAGbRKwRb6fqTSrGeLwVPOWgC3Xnsb3r4wj5jg"
images = ["https://blogger.googleusercontent.com/img/a/AVvXsEjokWLL415McRC6C9lNKntEUZQJ2T-FrtSSa8ANU_q3xe77IgdzCChJjxdC9UZ9SuiqcAnqAaSEnHsQO5YRJdBrOltAJuy52sQRgPRsdhLtuD9rTDgCw3QxUUhP3TiG7wK5kZ-jUm0M7kA6s8K017EPlAGbRKwRb6fqTSrGeLwVPOWgC3Xnsb3r4wj5jg"]


+++

Revisado 26 enero 2025

## Resumen

Intento recopilar y compartir información sobre qué, cómo y dónde reclamar, que es una cosa que los ciudadanos deberíamos hacer más a menudo, y a veces no se hace por desconocimiento.  
Lo intentaré ir actualizando e ir incorporando nueva información; varias personas me habían pedido información que intento poner, pero seguro que algo queda que decir. En la medida de lo posible intentaré añadir o enlazar ejemplos.  

En mayo 2022 realizo un taller para el que preparo materiales: reorganizo y reviso ideas respecto a versión inicial post de 2018.
[2022-05-25-ReclamadMalditos.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ReclamadMalditos/2022-05-25-ReclamadMalditos.pdf)  

El taller se grabó y se puede ver aquí  Formación RECLAMACIONES ANTE LA ADMINISTRACIÓN con @FiQuiPedia  
[![Formación RECLAMACIONES ANTE LA ADMINISTRACIÓN con @FiQuiPedia](https://img.youtube.com/vi/Eu8WPPnryFw/0.jpg)](https://www.youtube.com/watch?v=Eu8WPPnryFw "Formación RECLAMACIONES ANTE LA ADMINISTRACIÓN con @FiQuiPedia") 

[![](https://blogger.googleusercontent.com/img/a/AVvXsEjokWLL415McRC6C9lNKntEUZQJ2T-FrtSSa8ANU_q3xe77IgdzCChJjxdC9UZ9SuiqcAnqAaSEnHsQO5YRJdBrOltAJuy52sQRgPRsdhLtuD9rTDgCw3QxUUhP3TiG7wK5kZ-jUm0M7kA6s8K017EPlAGbRKwRb6fqTSrGeLwVPOWgC3Xnsb3r4wj5jg=s16000)](https://blogger.googleusercontent.com/img/a/AVvXsEjokWLL415McRC6C9lNKntEUZQJ2T-FrtSSa8ANU_q3xe77IgdzCChJjxdC9UZ9SuiqcAnqAaSEnHsQO5YRJdBrOltAJuy52sQRgPRsdhLtuD9rTDgCw3QxUUhP3TiG7wK5kZ-jUm0M7kA6s8K017EPlAGbRKwRb6fqTSrGeLwVPOWgC3Xnsb3r4wj5jg=s366)

<!-- imagen tomada originalmente de http://arablogs.catedu.es/blog.php?id_blog=958&id_articulo=138983 --> 

## Detalle

## El concepto de reclamar

Creo que lo primero es diferenciar conceptos:

* [Indignarse/](http://dle.rae.es/?id=LOifhN9)ofenderse: supone manifestar que uno está molesto por algo que le parece incorrecto.
* [Quejarse](http://dle.rae.es/?id=UlzwgCT): supone indicar que algo es incorrecto
* [Protestar](http://dle.rae.es/?id=USEkiZB): supone algo intermedio a quejarse o reclamar, según el contexto
* [Denunciar](http://dle.rae.es/?id=CEJpj4J): supone una queja formal basada en normativa, indicando algo ilegal. 
* [Reclamar](http://dle.rae.es/?id=VS3AoJM|VS3gD1Q): supone indicar que algo es incorrecto y realizar una petición que supone una corrección y/o aporta algo.

Indignarse/ofenderse me parece inútil: el algo subjetivo y además no suele estar argumentado.  
Quejarse es algo más útil, porque se tiene que argumentar, y porque si se reciben muchas quejas (o sugerencias), el receptor puede entender que debe replantear algo que está haciendo mal.  
Reclamar es más útil;  de hecho nos suena que un establecimiento debe tener "una hoja de reclamaciones a disposición de los usuarios". Uno indica lo incorrecto pero al tiempo concreta y pide una corrección.  

## Reclamamos poco y debemos hacerlo más

Como ciudadanos creo que somos poco activos, tenemos poca voluntad de cambiar cosas, nos han hecho y nos hemos hecho conformistas para que aceptemos las cosas como normales, cuando no lo son. Ver post [2018: objetivo evitar normalización](http://algoquedaquedecir.blogspot.com/2018/01/2018-objetivo-evitar-normalizacion.html)  

En general reclamamos poco, y en el caso de reclamar usando la ley de Transparencia esa baja actividad es un hecho: en 2017 "El portal de la transparencia recibe una media de 10 peticiones al día, cuando el Gobierno esperaba recibir más de 80" lo cito en este post [El Gobierno no cumple la Ley de Transparencia ni hace caso a Consejo de Transparencia y Buen Gobierno](http://algoquedaquedecir.blogspot.com/2017/08/el-gobierno-no-cumple-la-ley-de.html))

En este post intento comentar ideas de "Qué se puede hacer como ciudadano", un post que ya ha surgido con @chuso_jar, @ribap
[twitter FiQuiPedia/status/900608930937786370](https://twitter.com/FiQuiPedia/status/900608930937786370)  
apunto como idea para un post (largo) "qué se puede hacer" como ciudadano para denunciar lo ilegal que hace la administración, con ejemplos.  
[twitter FiQuiPedia/status/1020295354103881729](https://twitter.com/FiQuiPedia/status/1020295354103881729)  
Breve resumen (tengo MD abiertos + pendiente post sobre dónde y cómo reclamar cosas):  
-Tras silencio 1 mes reclama a @ConsejoTBG. Descarga pdf, cumplimenta y envía mail con info.  
-Plazo @ConsejoTBG son 3 meses, pero están más que saturados sin recursos y pueden tardar más.  

Si estás leyendo esto, ya hay una parte del recorrido hecha: estás pasando de la pasividad a informarte al menos de qué se puede hacer, por lo que ya hay algo de voluntad.

Lo que comento es a nivel individual: trabajar en grupo puede suponer más fuerza, aunque uno debe buscar qué grupo persigue lo mismo o ha hecho cosas similares. A veces no hace falta identificarse con un grupo o integrarse en él, se trata de colaborar.  
También es importante compartir lo que se hace, individualmente o en grupo, para que sea útil a otros. Eso lleva a ideas del post [¡Compartid, malditos!](http://algoquedaquedecir.blogspot.com/2018/04/compartid-malditos.html)

En varias cosas me centro en reclamaciones de tramitación online; bastante tiempo se gasta como para encima gastarlo en papeles físicos y ventanillas físicas. El tema de la configuración técnica es para post aparte (@firma, autofirma, certificados, navegadores ....): en enero 2019 creo el post [Gestiones digitales con la administración](https://algoquedaquedecir.blogspot.com/2019/01/gestiones-digitales-con-la.html)

En 2019 cito el ciberactivismo, y creo que es importante la idea de AFK (Away From Keyboard).

Reclamar lo que se considera que está mal no creo que sea simplemente útil, sino que es parte de ser ciudadano. Los ciudadanos que no lo hacen creo que se equivocan. Pongo una cita que creo que viene al caso

[wikiquote Albert Einstein](https://en.m.wikiquote.org/wiki/Albert_Einstein)  
> He perceives very clearly that **the world is in greater peril from those who tolerate or encourage evil than from those who actually commit it.**  

Einstein's tribute to [Pablo Casals](https://en.m.wikiquote.org/wiki/Pablo_Casals) (30 March 1953), in *Conversations with Casals* (1957), page 11, by Josep Maria Corredor, translated from *Conversations avec Pablo Casals : souvenirs et opinions d'un musicien* (1955

[twitter literlandweb1/status/1139496505830715392](https://twitter.com/literlandweb1/status/1139496505830715392)  
*"Hay que luchar todos los días, como Sísifo. Esto es lo que no comprendo. Que la vida contiene días, muchos días, y nada se conquista definitivamente. Por todo hay que luchar siempre y siempre. Hasta por lo que ya tenemos y creemos seguro. No hay treguas. No hay la paz"*
*#Pizarnik*

Había leído la frase "El pueblo que no lucha por sus derechos, no merece tenerlos", pero busco la referencia real y es otra

[Frases de Derecho que todo jurista debería conocer - archive.org](https://web.archive.org/web/20230207025634/https://blog.lemontech.com/frases-de-derecho-que-todo-jurista-deberia-conocer/)  
> El jurista alemán **Rudolf von Ihering** es el padre de la **sociología del Derecho**. En el siglo XIX elaboró diferentes teorías cuya influencia todavía se puede apreciar hoy en día en la doctrina jurídica.  
Entre sus frases destacadas no podemos olvidarnos de esta: *“en el Derecho posee y defiende el ser humano su condición moral de existencia, sin el Derecho desciende al nivel del animal. El pueblo que no lucha por su Derecho, no merece tenerlo”* . Con este pensamiento Ihering se refería a la vinculación entre el ordenamiento jurídico y las creencias morales y éticas de una sociedad, señalando que era algo esencial para que pudiera existir precisamente esa sociedad.

 
[El arbitraje español se independiza - elpais](https://cincodias.elpais.com/cincodias/2021/03/17/legal/1616006056_892178.html)  
> El jurista y filósofo alemán Rudolph von Ihering, autor de la obra “La lucha por el Derecho” (Der Kampf ums Recht, 1881) afirmó que “El pueblo que no lucha por su Derecho, no merece tenerlo”.

Hago una ordenación arbitraria, empezando por transparencia y por cosas que he usado.

### El objetivo, conseguirlo o no. Grietas

Lo comento hablando de manifestaciones, pero creo apartado aparte  
[twitter goldie_gemma/status/1140151757357670400](https://twitter.com/goldie_gemma/status/1140151757357670400)  
El objetivo ayuda. Cuanto más concreto y realizable sea, más probabilidad de que las protestas sirvan de algo.

Creo que hay que ser realista: uno no puede pretender conseguir grandes cambios y en breves plazos de tiempo. Uno tiene que saber que puede conseguir su objetivo o no; hay que mentalizarse en luchar intentando aproximarse a un objetivo realizable.  
Yo uso el concepto de grietas: no puedes esperar romper una piedra de un golpe, pero si se insiste continuamente, y es más fácil hacerlo entre muchos, quizá se consiga una primera grieta. Es poco, pero infinitamente superior a cero, y puede ser el germen de una rotura y un cambio mayor.

## Informarse e informar

Hablar de informarse puede parecer una chorrada, pero es esencial. Uno no puede reclamar nada si no entiende bien la situación actual, su origen, qué normativa la respalda, qué pide y qué tipo de cambios son necesarios.  
Cuando tiene información, hay que informar y difundir: puede que uno no pueda o sepa reclamar pero otros sí.  
Enlaza con ideas como informar a medios, [@filtrala](https://twitter.com/filtrala)  
A veces no se tiene información pero sí como comento para extraerla; hay gente que automatiza extracción de datos y la comparte, como [@hmeleiros](https://twitter.com/hmeleiros)

En enero 2019 descubro el buzón Punto de Acceso General, donde se pueden plantear una consulta para que sea redirigida a quien pueda responderla (sugerencia tras preguntar a portal de transparencia vía MD en Twitter)
[Buzón ciudadano](https://administracion.gob.es/pagSugerencias/buzon/buzonCiudadano.htm)  

También se puede contactar con diputados que tienen más acceso a realizar solicitudes de información
[twitter FiQuiPedia/status/1089934926861004800](https://twitter.com/FiQuiPedia/status/1089934926861004800)  
Idea: si como diputado deben facilitarte la información que solicites, te propongo ojees esto y solicites la información que no me dan a mí como ciudadano sobre cómo se dilapidaron para nada 16 millones de euros en Nexus educación

Me comentan que otra idea asociada a informar es el ciberactivismo al "crear opinión": si se crean web/blogs y se documenta algo de modo que al buscarlo se consigue que una idea esté bien posicionada, puede llegar a crear opinión sobre un tema. Por ejemplo este blog habla de ratios en educación, y al buscar para informarse es probable que aparezca un enlace a esta información, y como la gente no suele mirar más que los primeros resultados, puede influir. La idea me la comentan oralmente y me citan [Manual del ciberactivista](https://manualdelciberactivista.es/) de Javier de la Cueva.

El libro va más allá de esa idea; creo apartado de ciberactivismo y lo cito.

## Transparencia y buen gobierno

La [Ley 19/2013, de 9 de diciembre, de transparencia, acceso a la información pública y buen gobierno](https://www.boe.es/buscar/act.php?id=BOE-A-2013-12887) creo que es una herramienta esencial, y la uso mucho.

Los primeros párrafos del [preámbulo](https://www.boe.es/buscar/act.php?id=BOE-A-2013-12887#preambulo) creo que lo dicen todo

>La transparencia, el acceso a la información pública y las normas de buen gobierno deben ser los ejes fundamentales de toda acción política. **Sólo cuando la acción de los responsables públicos se somete a escrutinio, cuando los ciudadanos pueden conocer cómo se toman las decisiones que les afectan, cómo se manejan los fondos públicos o bajo qué criterios actúan nuestras instituciones podremos hablar del inicio de un proceso en el que los poderes públicos comienzan a responder a una sociedad que es crítica, exigente y que demanda participación de los poderes públicos.**  
Los países con mayores niveles en materia de transparencia y normas de buen gobierno cuentan con instituciones más fuertes, que favorecen el crecimiento económico y el desarrollo social. En estos países, los ciudadanos pueden juzgar mejor y con más criterio la capacidad de sus responsables públicos y decidir en consecuencia. Permitiendo una mejor fiscalización de la actividad pública se contribuye a la necesaria regeneración democrática, se promueve la eficiencia y eficacia del Estado y se favorece el crecimiento económico.

La transparencia creo que es esencial contra la corrupción, y para los cargos públicos la transparencia no es una opción: ver [Presunción de inocencia / indecencia, recursos públicos y carga de prueba](http://algoquedaquedecir.blogspot.com/2018/06/presuncion-de-inocencia-indecencia.html)

Lo más habitual como ciudadano es solicitar información (título I, capítulo III, Derecho de acceso a la información pública), se hace una solicitud de transparencia pidiendo información: **una idea básica es que debe ser información ya existente (actas, informes, detalles de gastos) y que respete ciertos límites de protección de datos personales.**

Otra idea básica (aprendida en [Oposiciones: transparencia enunciados](https://algoquedaquedecir.blogspot.com/2018/08/oposiciones-transparencia-enunciados.html) )

>*Este Consejo ha apreciado que sus últimas reclamaciones están basadas en solicitudes de derecho de acceso a la información pública formuladas de manera inadecuada. Así, mediante estas solicitudes no está usted demandando determinada información, sino que solicita que la Administración correspondiente realice una actividad material, actividad que queda fuera del ámbito de la Ley 19/2013, de transparencia, acceso a la información pública y buen gobierno. Como consecuencia de ello, este Consejo no puede sino proceder a su inadmisión a trámite al no tratarse de reclamaciones basadas en solicitudes de derecho de acceso a la información pública.*  
***De cara a futuras reclamaciones, se le sugiere modifique la forma en la que realiza las solicitudes de información y evite utilizar la fórmula “se hagan públicas…” y la sustituya por “solicito obtener copia de..”, “solicito información acerca de….”, “solicito datos en relación con….”. De esta manera este Consejo podrá entrar sobre el fondo de su reclamación y podrá ver satisfechas, o al menos tenidas en cuenta, sus pretensiones. Todo ello sin perjuicio del sentido estimatorio o desestimatorio que se dé a las reclamaciones, en función del estudio de los hechos y documentaciones aportadas y de la normativa que resulta aplicable.***  
*A la espera de que esta sugerencia le resulte de utilidad, reciba un cordial saludo.*  

Hay un portal de transparencia general del gobierno, y luego uno por cada comunidad autónoma. Algunas comunidades tienen sus propias normativas de transparencia. En el portal de transparencia es donde se realiza habitualmente la **publicidad activa**, que es publicar información sin que el ciudadano lo pida.   
Además de la publicidad activa, está el **derecho de acceso a la información pública**, que supone que se solicita información que no tiene por qué publicarse de forma activa, pero a la que sí se tiene acceso. Se plantea una solicitud de información al organismo correspondiente que tiene plazo de un mes en general, y una vez que se ha reclamado puede que haya silencio (había casos como Aragón donde el silencio era positivo, pero se anuló es parte de la normativa) o se deniegue la información, se puede reclamar en un plazo de un mes al consejo de transparencia, órgano independiente. Hay uno estatal que también se ocupa de las comunidades autónomas que no tienen uno propio, y luego varios autonómicos.

Sobre consulta de información se puede ver información compartida  
[Así fue nuestro taller de Acceso a la Información para organizaciones sociales y vecinales #transparencia - civio](https://civio.es/novedades/2016/10/21/asi-fue-nuestro-taller-de-acceso-a-la-informacion-para-organizaciones-sociales-y-vecinales-transparencia/)  

Pero la ley de Transparencia y Buen Gobierno es más que solicitar información:  
[twitter ConsejoTBG/status/1024579276375552000](https://twitter.com/ConsejoTBG/status/1024579276375552000)  
✔️Si es un incumplimiento de publicidad activa (título I de la ley) --> denuncia  
✔️Si es una actuación que atenta sobre el buen gobierno (título II de la ley) --> denuncia  
✔️Si no contesta o deniega una solicitud de información --> reclamación  
[Formularios para los ciudadanos que se dirijan al Consejo de Transparencia y Buen Gobierno])http://consejodetransparencia.es/ct_Home/te-ayudamos/Formularios.html)  

_Ojo a la vigencia de lo que se comenta: puede aplicar a cosas pasadas que se ponen como ejemplo pero ahora son distintas. Hay que tener presente que [hasta 2022 el Consejo de Transparencia y Buen Gobierno no tenía sede electrónica](https://www.consejodetransparencia.es/ct_Home/comunicacion/actualidadynoticias/hemeroteca/2022/20220408.html), se tramitaba con correos. En 2024 el Consejo de Transparencia de Madrid no tiene sede electrónica y se tramita con correo_

**Estado:**
[Portal transparencia Administración General del Estado](http://transparencia.gob.es/)  
[Consejo de Transparencia y Buen Gobierno](http://www.consejodetransparencia.es/)  

Un enlace con información general [Consejo de Transparencia y Buen Gobierno > Te ayudamos > Cómo reclamar](http://www.consejodetransparencia.es/ct_Home/te-ayudamos/Como_reclamar.html)  
Incluye imagen resumen (ya citando la sede electrónica)
![](https://www.consejodetransparencia.es/ct_Home/dam/jcr:cd2ec7a3-7a2d-408c-9fd2-5c42b9ca20b8/Como_reclamar.png)  


**Comunidades autónomas:**
En lugar de volver a copiar aquí (hay algún post donde cito varios, como [Oposiciones: transparencia](http://algoquedaquedecir.blogspot.com/2018/03/oposiciones-transparencia.html)), cito una referencia oficial de listado de portales y consejos:
[Consejo de Transparencia y Buen Gobierno > Transparencia > Transparencia en España](https://www.consejodetransparencia.es/ct_Home/transparencia/transparencia-en-espanya.html)  

El plazo del Consejo de Transparencia es de 3 meses (al menos el estatal está saturado de trabajo y sin recursos, les siguen quitando el escaso presupuesto en los PGE porque al estado no le interesa realmente la transparencia), y puede tardar más. En diciembre 2018 el Concejo de Murcia me pone por escrito que tiene por resolver reclamaciones de 2017: los 3 meses pueden pasar a ser más de un año  
[twitter FiQuiPedia/status/1076274443155107840](https://twitter.com/FiQuiPedia/status/1076274443155107840)  
"Plazos vendo y para mi no tengo"  
Una persona, física o jurídica, debe cumplir plazos administración o hay consecuencias  
Si administración incumple, no pasa nada  
Sin plazos, sin recursos (caso @consejotrm similar a @ConsejoTBG), y sin poder sancionador, no es transparencia

La resolución del Consejo de Transparencia es firme a no ser que la administración la recurra vía contencioso administrativo, algo que debería ser excepcional pero ocurre a veces.

**Ventajas, inconvenientes, ejemplos:**
No tiene coste para el ciudadano, y aunque puede haber silencio inicialmente, vía reclamación del Consejo de Transparencia, se tiene garantía de una respuesta.  
La información obtenida puede demostrar documentalmente algo, pero conseguir algo a partir de ella suele suponer otra batalla aparte.

Se ha conseguido mucha información por periodistas.  
En mi caso un éxito concreto fue conseguir paralizar la cesión de suelo público a 0€ y 40 años para un centro concertado en Torrejón en 2018, consiguiendo actas, y combinando con otras cosas (llega a la Asamblea de Madrid, se crea plataforma ciudadana en Torrejón, se recogen firmas ...)  
[Madrid crea centros privados con concierto en diferido](http://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html)  
No conseguida información porque la administración se niega vía contenciosos o se inventa límites que no existen. Ver post  [El Gobierno no cumple la Ley de Transparencia ni hace caso a Consejo de Transparencia y Buen Gobierno](http://algoquedaquedecir.blogspot.com/2017/08/el-gobierno-no-cumple-la-ley-de.html)  
Gracias a transparencia documenté por escrito oficialmente que Madrid incumple las [ratios en educación](http://algoquedaquedecir.blogspot.com/2017/09/ratios-en-educacion.html)  
Gracias a transparencia conseguidos enunciados y plantillas correctoras oposiciones [Oposiciones: transparencia enunciados](https://algoquedaquedecir.blogspot.com/2018/08/oposiciones-transparencia-enunciados.html) (y aprendidos errores)  

En marzo 2019 se aprueba ley de Madrid de transparencia, que tiene poder sancionador. Ver post [Ley de Transparencia Madrid](https://algoquedaquedecir.blogspot.com/2019/03/ley-de-transparencia-madrid.html). Más tarde modifican la ley para que deje de tener poder sancionador  

En mayo 2019 tras volver el PP a Madrid planteo una idea asociada a transparencia: hacer un grupo de peticiones conjuntas. La comento respondiendo a un post de Javier Panadero donde habla de que los funcionarios debemos ser más activos.  
[twitter FiQuiPedia/status/1133022837294403584](https://twitter.com/FiQuiPedia/status/1133022837294403584)  
Comparto una reflexión en voz alta. Esperaba cambio en C. Madrid que supusiera levantar alfombras y airear información con, deseaba, alguna consecuencia de responsabilidades. Si siguen los mismos no va a pasar si no lo hacemos nosotros. Me limito a lo que sé y puedo aportar 1/n 
Desde que soy funcionario he usado la ley de transparencia para mostrar, con datos, ejemplos de mala gestión. He tocado temas sensibles y han callado o han iniciado contenciosos contra @ConsejoTBG para no dar información de uso de recursos públicos 2/n  
La ley de transparencia no tiene -todavía- poder sancionador, pero es un arma. Se aprobó ley de Madrid que sí sancionará en 2020, y me temo la querrán parar. Solo PP votó en contra. La idea es una "hackatón" de transparencia, juntarse y poner muchas peticiones de información 3/n  
Un volumen crítico de peticiones, incluyendo las que busquen documentar lo que han hecho y pretenden hacer en deterioro de servicios públicos quizá permita que sepan que tienen a ciudadanos fiscalizando su gestión y esa varíe. Deben rendir cuentas siempre, no solo cada 4 años.  
Quedada de funcionarios, en lugar con wifi, cada uno lleva portátil y certificado digital. Todos listos técnicamente para poner peticiones.  
[Gestiones digitales con la administración](https://algoquedaquedecir.blogspot.com/2019/01/gestiones-digitales-con-la.html)  
Explicación del proceso aquí por @civio  
[Así fue nuestro taller de Acceso a la Información para organizaciones sociales y vecinales #transparencia](https://civio.es/novedades/2016/10/21/asi-fue-nuestro-taller-de-acceso-a-la-informacion-para-organizaciones-sociales-y-vecinales-transparencia/)  
Asuntos turbios no faltan.  
Todo lo que se me ocurre lo vuelco aquí.  
[!Reclamad, malditos!](https://algoquedaquedecir.blogspot.com/2018/07/reclamad-malditos.html)  
Esta propuesta es algo que no tiene coste económico personal, solo la dedicación de tiempo y ganas para luchar contra su arma que es vencer por aburrimiento sabiendo es raro vayamos a juicios por tiempo y dinero

Otra idea que surge en transparencia es el anonimato:  
"Si pido información doy mi nombre, y ya saben quién soy. Eso en situaciones puede crear problemas"  
En su momento había una opción de hacerlo anónimo  
https://maldita.es/pedir-informacion/ (no funciona en 2024)  
>Para poder solicitar información a través de Maldita.es y que nosotros podamos certificar que esa información te llega, necesitamos que te hagas maldit@.  
De esa manera nos podemos poner en contacto contigo cuando las administraciones nos respondan. Sólo te pedimos un nombre de usuario, tu provincia y un e-mail y no vendemos tus datos a terceros.  

Tutorial 1 junio 2020 de Samuel Parra (centrado en ministerios)
[Guía para solicitar información a las instituciones públicas](https://www.samuelparra.com/2020/06/01/guia-para-solicitar-informacion-a-las-instituciones-publicas/)  

Otro recurso similar  
[ask the EU](https://www.asktheeu.org/)  
AskTheEU.org is an online platform for citizens to send access to documents requests directly to EU institutions.

También está la idea de formar en transparencia, que además de lo citado
[Así fue nuestro taller de Acceso a la Información para organizaciones sociales y vecinales #transparencia](https://civio.es/novedades/2016/10/21/asi-fue-nuestro-taller-de-acceso-a-la-informacion-para-organizaciones-sociales-y-vecinales-transparencia/)  
tiene otras iniciativas como  
[Transparencia: acceso a la información en las instituciones públicas - maldita.es - archive.org](https://web.archive.org/web/20210617182942/https://educa.maldita.es/transparencia-acceso-a-la-informacion-en-las-instituciones-publicas/)  

[Qué hacemos - maldita.es](https://maldita.es/malditaeduca/que-hacemos-educa/)  
> Transparencia: acceso a la información en las instituciones públicas  
Los ciudadanos y periodistas pueden solicitar datos e información pública a las instituciones, pero no siempre es fácil. Aprende con nosotros cómo hacerlo, cómo reclamar en caso de que sea denegada y cómo transformar esos datos en historias periodísticas interesantes.

Además está la idea de "buen gobierno" como derecho fundamental UE  
[Carta de los Derechos Fundamentales de la Unión Europea](https://fra.europa.eu/es/eu-charter/article/41-derecho-una-buena-administracion)  
>Artículo 41 - Derecho a una buena administración  
>1. Toda persona tiene derecho a que las instituciones, órganos y organismos de la Unión traten sus asuntos imparcial y equitativamente y dentro de un plazo razonable.  
>2. Este derecho incluye en particular:  
a) el derecho de toda persona a ser oída antes de que se tome en contra suya una medida individual que la afecte desfavorablemente;  
b) el derecho de toda persona a acceder al expediente que le concierna, dentro del respeto de los intereses legítimos de la confidencialidad y del secreto profesional y comercial;  
c) la obligación que incumbe a la administración de motivar sus decisiones.  
>3. Toda persona tiene derecho a la reparación por la Unión de los daños causados por sus instituciones o sus agentes en el ejercicio de sus funciones, de conformidad con los principios generales comunes a los Derechos de los Estados miembros.  
>4. Toda persona podrá dirigirse a las instituciones de la Unión en una de las lenguas de los Tratados y deberá recibir una contestación en esa misma lengua.

## Contactar con asociaciones que lleven temas similares

No hay que reinventar la rueda; la situación a la que uno se enfrenta puede haber sido sufrida por otros antes, o similares, y se tiene experiencia en qué hacer, e igual ya se están haciendo cosas.  
De nuevo creo que es esencial compartir, aunque hay organizaciones que para "colaborar" suelen requerir que la persona se integre en su organización, y sin ese paso no comparten: un ejemplo son las reclamaciones de sindicatos.  
Personalmente uso la información pública que comparte sindicatos, pero no estoy afiliado a ninguno: la gente suele decir precisamente que la utilidad es que es más barato usar sus servicios legales, pero he visto errores en sus planteamientos, y no me convence que no compartan y sea necesario afiliarse.  
Ejemplos serían asociaciones de consumidores: OCU, FACUA  
Un ejemplo al que sí me he sumado es una asociación que comparte gastos e información sobre reclamar legalmente [Concurso de traslados: baremación de la interinidad](http://algoquedaquedecir.blogspot.com/2017/11/concurso-de-traslados-baremacion-de-la.html)  
Un ejemplo (que enlaza con firmas y con de contactar con políticos) serían las ILP. Un ejemplo concreto es la ILP escolarización inclusiva de Madrid. Colectivos sociales de Madrid proponiendo una normativa que favorezca la inclusión escolar y la igualdad de oportunidades.  
[ILP ESCOLARIZACIÓN INCLUSIVA](http://ilpescolarizacioninclusiva.blogspot.com/)  

## Formar opinión / concienciar

### Contactar con prensa y medios

La prensa y los medios creo que no tienen la libertad de Twitter; hay mucha prensa tradicional que se podría decir que está ligada a sus pagadores y ya no es prensa de verdad.  
En cualquier caso si un tema se consigue que llegue a la prensa y medios y tiene suficiente repercusión es posible que empiece a moverse a otros niveles.  

Un ejemplo de éxito es el post Madrid crea centros privados con concierto en diferido en el que tras conseguir que saliese en eldiario.es se consiguió parar una cesión de suelo público a 0€ y 40 años para un centro concertado por construir. Se trata de que empuje mucha gente, no es un logro solamente mío. Como comento en el post

> Es un caso de enero 2018, en febrero 2018 se cita en eldiario.es, cadenaser.com, publico.es y actualizo con casos similares previos, sale Cifuentes con declaraciones en La Sexta, se crea un change.org, se crea plataforma ciudadana, se hacen votaciones en Torrejón relativas a otras cesiones  ... aporto detalles de cómo evoluciona en detalles, que finalizan con el anuncio en junio 2018 de que el centro será público. Creo que es un caso que muestra que la movilización consigue cambiar cosas. 

### Ciberactivismo

Ver [Manual del ciberactivista. Teoría y práctica de las acciones micropolíticas](http://manualdelciberactivista.es/) de Javier de la Cueva.

En la web:  
>En principio, este libro va dirigido a cualquier persona. Tal y como se señalará en el ensayo, el ciberactivismo (y por ende lo que en la obra se definen como acciones micropolíticas) tiene unos fines que pueden ir, por ejemplo, desde la exigencia de la implantación de pasos cebras para evitar los peligros de un cruce de peatones al rechazo de la política estatal de viviendas. Así pues, se dirige a toda persona que esté interesada en el uso de la web como herramienta para un ejercicio público de sus derechos. 

En el libro (cc-by-sa)  
>En resumen: esta obra va dirigida a todas las personas que, ante una situación en la que aprecian una injusticia, se preguntan  honestamente si cabe una reparación, se plantean sin veleidades mediáticas un camino de estudio del problema como mecanismo para
intentar cambiar las cosas y se ponen manos a la obra. En definitiva,  se trata de aplicar en un entorno público las tres preguntas kantianas: ¿qué puedo conocer? ¿cómo debo comportarme? ¿qué debo esperar? Como se puede intuir, si bien la tecnología puede ser novedosa, los problemas son los de siempre.

### Manifiesto, carta abierta

Lo veo como una manera ordenada de exponer ideas, aunque creo que a menudo no se hacen propuestas concretas sino que se centran en exponer la situación y argumentar. Tienen poco efecto si no van acompañados de algo más.  
Puede asociarse a participar en elaboración de normas.  
Un ejemplo  
[Ante la posibilidad de un Pacto por la Educación. MANIFIESTO POR LA ESCUELA PÚBLICA, LAICA y GRATUITA](https://laicismo.org/data/docs/archivo_742.pdf)  

### Redes sociales

Creo que crear hashtags y Trendings Topics pueden servir para llamar la atención sobre algo, pero como comento en recogida de firmas, no tiene más efecto que el que pueda suponer que se mueva a otros niveles.  
Hablo de Twitter que es lo que conozco: creo que es una buena plataforma para informar e informarse libremente (mientras no tenga censura), que permite difundir cosas y recibir lo que otros difunden, pero eso no es denunciar.  
En Twitter hay mucho ruido: bots, perfiles anómimos o seudónimos, a veces para trollear y a veces para dar información valiosa u opinar libremente sin miedo a las represalias. Creo que la prensa presta atención a Twitter porque lo ve como un canal de información.

## Reclamar a organismos

### Reclamaciones en el entorno local

Es algo que enlaza con contactar con políticos pero a nivel local: concejales, ayuntamientos. A veces la gente se queja de cosas que van mal sobre las que no ha reclamado. En el entorno local es más fácil contactar y conseguir cosas, suelen haber más cercanía y pueden ser cosas más pequeñas. Por ejemplo yo solicité un paso de cebra en un punto de una calle por el que cruzaba casi todo el mundo, cerca de una guardería, y lejos de otros pasos de cebra, y se atendió y se resolvió pronto.  
A veces comparo la política con una comunidad de vecinos: es habitual que nadie informe de que algo va mal para que se arregle, tan habitual como quejarse de cosas que uno no ha reclamado.

### Reclamaciones formales

Normalmente cada organismo público o cada entidad privada tiene una forma de plantear reclamaciones/quejas/sugerencias/consultas, muchas veces online.  
Algunos ejemplos que he usado (algunos han cambiado de url por cambio de nombre de ministerios):  

**Ministerio de Educación**   
[Quejas y sugerencias del Ministerio de Educación, Formación Profesional y Deportes](https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/catalogo/general/99/998258/ficha/998258.html)  

[Recursos, reclamaciones y revisión de actos](https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/catalogo/recursos-reclamaciones-revision.html)  

[Consulta electrónica al Ministerio de Educación, Formación Profesional y Deportes (Acceso)](http://sede.educacion.gob.es/sede/login/inicio.jjsp?idConvocatoria=404)  

**Comunidad de Madrid** (Quejas y sugerencias)  
[SUGERENCIAS, QUEJAS Y AGRADECIMIENTOS](https://gestiona.comunidad.madrid/suqe_inter/run/j/QuejaAlta.icm)  

**Banco de España** (Reclamaciones, quejas y consultas)  
Antes de poder ir BdE hay que haber ido a la entidad.  
[Reclamaciones y consultas sobre normativa de transparencia y buenas prácticas bancarias](https://sedeelectronica.bde.es/sede/es/menu/administraciones/reclamaciones-y-/Reclamaciones___2e85b7714582d21.html)  

**CNMC** (Comisión Nacional de los Mercados y la Competencia) (Trámites varios: reclamaciones, denuncias ...)  
[CNMC Trámites](https://sede.cnmc.gob.es/tramites)  

**ITSS** (Inspección de Trabajo y Seguridad  Social) (Denuncias)  
[¿Cómo presentar una denuncia formal ante la Inspección de Trabajo y Seguridad Social?](https://www.mites.gob.es/itss/web/Atencion_al_Ciudadano/COMO_DENUNCIAR_ITSS.html)  
>Es importante tener presente que no inspeccionan a la administración pública, por lo que un funcionario debe usar un contencioso administrativo.

A veces hay formularios específicos para algunos temas: por ejemplo para tema calendario escolar 2018-2019 se abrió un formulario del Consejo Escolar de Madrid para recibir "dudas y sugerencias"  
[Formulario Consejo Escolar de Madrid](https://www.educa2.madrid.org/web/actividades-del-consejo/formulario-de-inscripcion)  

ADICAE (asociación de consumidores) tiene su propia plataforma para reclamar con información de dónde reclamar  
[Dónde reclamar](https://reclama.otroconsumoposible.es/donde-reclamar/)  
[Atención al cliente de empresas](https://reclama.otroconsumoposible.es/empresas/)  
[Organismos reguladores](https://reclama.otroconsumoposible.es/reguladores/)  
[Arbitraje](https://reclama.otroconsumoposible.es/arbitraje/)  

**Ventajas/inconvenientes/ejemplos:**   
A menudo no se permiten reclamaciones, son solamente quejas o sugerencias, que son algo, pero la respuesta, si es que se recibe, puede no tener utilidad alguna. En Madrid quejas he puesto poco, más reclamaciones vía transparencia, y en el Ministerio bastantes.  
En el caso de la administración se deben usar recursos de alzada / potestativo: la administración no tiene obligación de contestar, puede alegar que el recurrente no está legitimado, puede alegar que es algo aprobado por los representantes y no se puede hacer nada. Luego implicaría un contencioso que supone coste.

* Ejemplo reclamando ilegalidad normativa: cambio horas lectivas en 2011 en Madrid y Castilla-La Mancha (lo cito en post [Horarios docentes secundaria](http://algoquedaquedecir.blogspot.com/2017/09/horarios-docentes-secundaria.html)). Un resumen (ver post [Legitmación, civismo y transparencia con documentos](https://algoquedaquedecir.blogspot.com/2021/09/legitimacion-civismo-y-transparencia.html))
- En Castilla-La Mancha, recurso de alzada. Rechazado por no ser docente allí y no estar legitimado. Marcial Marín firmó esta frase, "su reclamación se limita a la mera legalidad, insuficiente para recurrir"  
- En Madrid, recurso de alzada. Rechazado porque la ley la aprueba Asamblea y no es modificable por un ciudadano. Aunque sea inconstitucional, el recurso de inconstitucionalidad solamente lo pueden plantear ciertas personas: gobierno, cierto número de diputados, defensor del pueblo, ... (ver post [Legal vs constitucional para un ciudadano](https://algoquedaquedecir.blogspot.com/2018/03/legal-vs-constitucional-para-un-ciudadano.html))  
* Ejemplo de tiempo gastado en queja tras queja hasta comprobar que no querían retirar una mentira [El ministerio de educación miente en su web: manipulan por ver si algo queda](http://algoquedaquedecir.blogspot.com/2017/09/el-ministerio-de-educacion-miente-en-su.html)
* Ejemplo de reclamación a ITSS en [Climatización aulas y la viga de Ohanes](http://algoquedaquedecir.blogspot.com/2018/02/climatizacion-aulas-y-la-viga-de-ohanes.html)
* Ejemplo de uso de Banco de España y CNMC en [Transferencias entre bancos y nóminas funcionarios Madrid](http://algoquedaquedecir.blogspot.com/2017/12/transferencias-entre-bancos-y-nominas.html)

### Recursos administrativos

Es algo relacionado con la administración: han recursos de alzada, potestativos, ... pero en general pueden no tener ningún efecto y ante silencio solo queda una denuncia judicial vía un contencioso administrativo.  

[Derechos de participación administrativa > Recursos - interior.gob](https://www.interior.gob.es/opencms/es/servicios-al-ciudadano/participacion-ciudadana/derechos-de-participacion-administrativa/recursos/)

>Hay tres tipos de recursos, que pueden ser interpuestos según las circunstancias de cada caso:  
* **Alzada**  
* **Potestativo de Reposición**  
* **Extraordinario de revisión**  
El error o la ausencia de la calificación del recurso por parte del recurrente no será obstáculo para su tramitación, siempre y cuando del mismo se deduzca su verdadero carácter.  
**El recurso de alzada**  es el que se interpone contra las resoluciones y actos a los que se refiere el artículo 112.1 de la LPACAP, esto es, contra las resoluciones que no pongan fin a la vía administrativa y los actos de trámite, si éstos deciden directa o indirectamente el fondo del asunto, determinan la imposibilidad de continuar el procedimiento, producen indefensión o perjuicio irreparable a derechos e intereses legítimos.  
En los casos señalados, su interposición es obligatoria para poder acudir, una vez resuelto,  a la jurisdicción contencioso-administrativa.  
**El recurso potestativo de reposición**  se puede interponer contra los actos que pongan fin a la vía administrativa. Dependiendo de la autoridad de la que provengan, puede haber actos de trámite que hayan de ser recurridos a través del recurso de reposición.
Es potestativo, es decir, no es obligatorio para poder acudir a la jurisdicción contencioso-administrativa, aunque, una vez presentado, hay que esperar a  su resolución o a que transcurra el plazo para ello para poder interponer un recurso ante la autoridad judicial de dicho orden.  
**Tanto los recursos de alzada como de reposición**  han de fundarse en cualquiera de los motivos de nulidad o anulabilidad, definidos respectivamente en los artículos 47 y 48 de la LPACAP.  
**El recurso extraordinario de revisión**  es el que puede interponerse contra los actos firmes en vía administrativa que en su momento no fueron impugnados ante la jurisdicción contencioso-administrativa, y han de fundarse en alguna de las circunstancias recogidas en el artículo 125.1 de la LPACAP:  
* Que al dictarlos se hubiera incurrido en error de hecho, que resulte de los propios documentos incorporados al expediente.  
* Que aparezcan documentos de valor esencial para la resolución del asunto que, aunque sean posteriores, evidencien el error de la resolución recurrida.  
* Que en la resolución hayan influido esencialmente documentos o testimonios declarados falsos por sentencia judicial firme, anterior o posterior a aquella resolución.  
* Que la resolución se hubiese dictado como consecuencia de prevaricación, cohecho, violencia, maquinación fraudulenta u otra conducta punible y se haya declarado así en virtud de sentencia judicial firme.  

Si uno se equivoca no impide la tramitación

Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas.  
[Artículo 115. Interposición de recurso.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-10565#a115)  
> 2. El error o la ausencia de la calificación del recurso por parte del recurrente no será obstáculo para su tramitación, siempre que se deduzca su verdadero carácter.  

### Denunciar judicialmente

(Aclaración inicial: ver apartado separado denunciar a la fiscalía)  
Denunciar supone un proceso judicial, y eso supone coste, en tiempo, dinero, y además puede que a nivel personal. La denuncia de situaciones corruptas ha supuesto una pesadilla para denunciantes en lugar de para los corruptos denunciados, y han surgido plataformas de defensa de los denunciantes, con iniciativas para que la ley los proteja.  
[Plataforma por la honestidad](https://plataformaxlahonestidad.es/)  
A nivel judicial, viendo que el contencioso administrativo es la vía para reclamar cuando uno es funcionario (no se puede ir a ITSS), denunciar en ciertos casos es reclamar.  
Poner miles de recursos de alzada y vender eso como "presión administrativa", como han hecho sindicatos alguna vez, me parece ridículo: ellos saben bien que ante silencio queda desestimado, y muchos no van al contencioso por su coste.  

Ejemplo es el contencioso administrativo que tuve en 2017 sobre mi cese en verano de 2015  
[Madrid roba a los interinos (I): cese junio, llamamiento septiembre y pago verano](http://algoquedaquedecir.blogspot.com/2017/12/madrid-roba-los-interinos-i.html)

Una idea interesante es no reclamar de manera individual; compartir batallas y esfuerzos, y compartir el resultado para que sea útil a todo un colectivo.  

Una reflexión es que hay que pensar en la denuncia judicial como parte de una reclamación si se quiere hacer algo que tenga efectos reales, no quedarse en temas simbólicos.  
[twitter jdelacueva/status/1208011033102696454](https://twitter.com/jdelacueva/status/1208011033102696454)  
Por estas razones es importante desde el principio acompañar el activismo con acciones judiciales.  
El Tribunal Supremo holandés ordena al gobierno a que urgentemente y de forma significativa reduzcan emisiones para proteger los derechos humanos  

[twitter jdelacueva/status/1207206259885260800](https://twitter.com/jdelacueva/status/1207206259885260800)  
Por estas razones es importante desde el principio acompañar el activismo con acciones judiciales.  
El Supremo da la razón a Civio y obliga al Tribunal de Cuentas a identificar a todos sus eventuales  

[twitter FiQuiPedia/status/1210152294651715584](https://twitter.com/FiQuiPedia/status/1210152294651715584)  
Transparencia en @HoyPorHoy
[Hoy por Hoy (26/12/2019 - Tramo de 09:00 a 10:00)](https://play.cadenaser.com/audio/cadenaser_hoyporhoy_20191226_090000_100000/)  
Desde 00:40 con @jdelacueva y @Samuel_Parra  
"Activismo…España…camiseta…países…qué tipo de acciones judiciales son las que tienen que acompañar a ese activismo en la calle"  

Hilo muy interesante  
[twitter josemuelas/status/1184795204475412480](https://twitter.com/josemuelas/status/1184795204475412480)  
El viaje en autobús Cartagena-Madrid es bastante aburrido así que voy a matar el tedio con un hilo provocativo. Para ello nada mejor que una buena afirmación inicial:  
Queridos lectores no se hagan ilusiones: las instituciones legales, son dominio exclusivo de los poderosos.  
Los poderosos, las corporaciones, los bancos, las aseguradoras, son usuarios intensivos de la administración de justicia y estos usuarios intensivos disponen de interesantes recursos para hacer que se interpreten las leyes a su favor.  
En general cuando un ciudadano acude ante los tribunales de justicia no persigue que estos fijen unas líneas jurisprudenciales que favorezcan a los particulares, se limita a litigar tratando de obtener justicia en su concreto y particular caso.  
Los usuarios intensivos de la administración de justicia pueden transaccionar o pagar aquellos asuntos que no desean que lleguen más allá de la primera instancia, de forma que resoluciones que podrían perjudicarles jamás alcancen ni siquiera el rango de «jurisprudencia menor»
El abogado que hizo el mayor favor a los consumidores españoles no fue Dionisio, el defensor de Aziz, sino el abogado del banco que, envanecido, se negó a transaccionar el asunto de un don Nadie. Su terca oposición permitió que el asunto llegase al TJUE y el resto es historia.  
Los usuarios intensivos de la administración de justicia, además, complementan toda su estrategia anterior con una eficaz y constante labor de lobby ante gobiernos y partidos de la oposición cuyas campañas financian.  
Si usted lo piensa hay procedimientos cuyo uso es casi exclusivo de estos usuarios intensivos, como por ejemplo las ejecuciones hipotecarias; no parece que la ley tenga la misma deferencia con los consumidores cuyo procedimiento estándar es el "ordinario".
Y sin embargo estos usuarios intensivos, gracias a su labor de lobby han conseguido que se considere a los españoles "querulantes" cuando un ciudadano apenas si va al juzgado una vez en su vida y ellos centenares de miles.  
¿Recuerdan a Gallardón y sus tasas?  
Abogados y abogadas, dentro del panorama descrito, son poderosos guardianes de las instituciones políticas y legales donde la dinámica entre la ley y el cambio social tiene lugar (Galanter 1974, Hunt 1990, Silbey 2005) y, si me lo permiten, déjenme que les ponga algunos ejemplos.  
Cuando en 2012 el ministro de justicia de infausto recuerdo Alberto Ruíz Gallardón estableció unas infames tasas judiciales para poder acceder a la administración de justicia, se cuidó muy mucho de que las tasas que gravaban el acceso a los recursos fuesen las más elevadas.
Esto no era casual, era la forma de colocar más lejos del ciudadano común las instancias donde se fija la jurisprudencia y era la forma de permitir a los poderosos seleccionar los procesos más convenientes para llevar ante ellos.  
Fue con la infamia de las tasas cuando apareció con fuerza en España una abogacía rebelde, bien que con causa, que no estuvo dispuesta a aceptar como inevitable aquella iniquidad.  
Una abogacía que ya no litigaba por el caso concreto sino en el marco de una estrategia procesal más amplia y buscaban casos "perfectos" que poder llevar ante los tribunales españoles y europeos a la busca de resoluciones que pusiesen fin a la infamia de las tasas.
Esta abogacía no no es una abogacía de causas perdidas sino una abogacía de futuro, que ya no busca ganar un caso sino que trabaja para ganar una causa colectiva, ya sea una ampliación de derechos fundamentales, la derogación de una ley injusta o remediar situaciones de hecho.  
Veo este tipo de abogacía en muchas acciones de los abogados de extranjería, laboralistas, de consumidores, partidarios del conocimiento y el software libres... una dimensión ética empieza a generalizarse —siempre ha estado ahí, bien que menos visible— en la abogacía.
Desde los abogados laboralistas que —durante la transición— trabajaron (y a veces murieron) defendiendo un cambio necesario, hasta los modernos abogados tecnológicos la abogacía ha sido siempre un importante agente de cambio y progreso social capaz de forzar avances sorprendentes  
Es tiempo de que abogados y abogadas tomen conciencia de su inmenso potencial como agente de cambio y busquen la manera de organizar y optimizar su trabajo, no sólo en interés de sus clientes, sino de toda la sociedad.  
¿Qué tal si lo hacemos juntos?  
Nos vemos en #Córdoba2019  

En enero 2020 me comentan esto que no conocía
[Ciudadanos > Solicitar Justicia Gratuita](https://www.abogacia.es/servicios/ciudadanos/servicios-de-orientacion-juridica-gratuita/)  

> Tienen derecho a la Justicia Gratuita
* Quienes acrediten insuficiencia de medios económicos  
* Con independencia de sus medios económicos, los trabajadores y beneficiarios de la Seguridad Social (para defender sus derechos laborales), las víctimas de violencia de género, de terrorismo y de trata de seres humanos (en juicios por su condición de víctimas), los menores de edad y las personas con discapacidad psíquica cuando sean víctimas de situaciones de abuso o maltrato.   
Condiciones para acceder a la Justicia Gratuita  
* Que el solicitante no alcance los ingresos económicos señalados por la ley o pertenezca a los colectivos antes indicados.  
* Que se solicite para litigar por derechos propios.  
* Que la pretensión no sea indefendible.  


[Ley 1/1996, de 10 de enero, de asistencia jurídica gratuita.](https://www.boe.es/buscar/act.php?id=BOE-A-1996-750)  

[Artículo 2. Ambito personal de aplicación](https://www.boe.es/buscar/act.php?id=BOE-A-1996-750#a2)  
Se puede ver punto k añadido en 2023 por ley 2/2023   
[Artículo 3. Requisitos básicos](https://www.boe.es/buscar/act.php?id=BOE-A-1996-750#a3)  

Otra idea interesante es el tema de recurrir ante silencio, hay sentencias TS sobre plazos en ese caso

[https://twitter.com/abotgado_es/status/1637044204772655109](https://twitter.com/abotgado_es/status/1637044204772655109)  
El TS confirma su constante doctrina sobre el INNECESARIO recurso administrativo frente al SILENCIO y añade que plantearlo en vía judicial es una DILACIÓN INDEBIDA, contraria a la BUENA ADMINISTRACIÓN y procesalmente TEMERARIA (costas de 8 mil €)  
STS 7/3/23- rec.3069/2021  
![](https://pbs.twimg.com/media/FrfzBnRWcAMOH7m?format=jpg)  
[Roj: STS 799/2023 - ECLI:ES:TS:2023:799](https://www.poderjudicial.es/search/AN/openDocument/57ebc6cd9e7561c2a0a8778d75e36f0d/20230317)  


Un tema que a veces no se piensa lo suficiente es **la obligación de denunciar**

[CONSEJO GENERAL DEL PODER JUDICIAL. Promotor de la Acción Disciplinaria. Unidad de Atención Ciudadana. Guía sobre la denuncia](https://www.poderjudicial.es/stfls/CGPJ/ATENCI%C3%93N%20CIUDADANA/FICHERO/20160922%20Gu%C3%ADa%20sobre%20la%20denuncia.pdf)  

>QUÉ ES UNA DENUNCIA  
Es la declaración, verbal o por escrito, por la que se comunica a la
autoridad cualquier hecho del que se tenga conocimiento y que pueda
ser constitutivo de una infracción penal, aunque no lo haya presenciado
directamente o no le haya ocasionado perjuicio.  
>QUIÉN PUEDE DENUNCIAR Y QUIÉN ESTÁ OBLIGADO A HACERLO  
Toda persona que presencie la comisión de un delito público o que, sin
haberlo presenciado, tenga conocimiento de él por otra forma, está
obligada a ponerlo inmediatamente en conocimiento de la autoridad,
incurriendo en una infracción si no lo hiciere.  
Están obligados especialmente a denunciar: los empleados o
funcionarios públicos y los que conocieran la comisión de un delito por
razón de su cargo, profesión u oficio.    
....  
CÓMO Y DONDE SE PRESENTA LA DENUNCIA  
Cómo:  
· Por escrito o verbalmente  
Dónde:  
· Ante las autoridades policiales de cualquier clase: Comisaría de
Policía de ámbito municipal, autonómico o nacional y cuartel o dependencia de la Guardia Civil.  
· Ante el Fiscal.  
· Ante el Juzgado de Instrucción o el Juzgado de Paz de su domicilio.  

 
Real Decreto Legislativo 5/2015, de 30 de octubre, por el que se aprueba el texto refundido de la Ley del Estatuto Básico del Empleado Público.  
[Artículo 54. Principios de conducta.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a54)  
> 3. Obedecerán las instrucciones y órdenes profesionales de los superiores, salvo que constituyan una infracción manifiesta del ordenamiento jurídico, en cuyo caso las pondrán inmediatamente en conocimiento de los órganos de inspección procedentes.  

Por aclarar qué "órganos de inspección" aplican según el caso

Si se va a fiscalía y dice que hay que ir a contencioso se entiende que aplicaría [punto k de artículo 2 Ley 1/1996](https://www.boe.es/buscar/act.php?id=BOE-A-1996-750#a2)  

### Denunciar a la fiscalía

Es un tema distinto a denunciar, que visualizo como separado en 2020.  
No se trata de iniciar un proceso judicial individualmente, sino convencer a la fiscalía de que lo inicie.

Miro por encima y veo información general  
[Fiscal.es > Atención Ciudadana > Oficina de Atención al ciudadano](https://www.fiscal.es/oficina-de-atenci%C3%B3n-al-ciudadano)  

Ahí se indica

> • Las comunicaciones que la Fiscalía General del Estado reciba a través de esta página no eximirán a sus autores, por sí mismas, del cumplimiento de la normativa vigente que obliga a quien presencie la perpetración de un delito y al que tenga conocimiento de la perpetración de un delito por razón de su cargo, profesión u oficio a denunciarlo al Juez, Fiscal o Policía más cercano.

Hay fiscalías territoriales, por ejemplo  
[Fiscal.es > Fiscalías Territoriales > Comunidad de Madrid](https://www.fiscal.es/web/fiscal/-/comunidad-de-madrid)  
Llamo por teléfono y me indican que hay que ir presencialmente a registro a presentar la denuncia; al preguntar por registro electrónico indican que no les consta que sea posible.

Me planteo si es posible vía [Registro Electrónico General de la AGE](https://sede.administracion.gob.es/PAG_Sede/ServiciosElectronicos/RegistroElectronicoComun.html)  ya que hasta ese momento lo he usado para la administración (ejecutivo) y la fiscalía forma parte del poder judicial.  
Pero veo que al entrar, indica inicialmente  
"Seleccione el Nivel de Administracion: "  
Donde además de estatal, autonómico, local, universidades, y otras instituciones aparece "Administración de Justicia", y permite elegir comunidad autónoma pero no fiscalía.  

No tengo claro si es válido dirigirse al ministerio de justicia
[Solicitudes y escritos -](https://sede.mjusticia.gob.es/es/tramites/solicitudes-escritos)  

[Sede judicial - justicia.es](https://sedejudicial.justicia.es/)  
> Según la ley 18/2011 la Sede Judicial Electrónica es aquella dirección electrónica disponible para los ciudadanos y profesionales de la justicia a través de redes de telecomunicaciones cuya titularidad, gestión y administración corresponde a cada una de las Administraciones competentes en materia de Justicia. La Sede Judicial Electrónica del Ministerio de Justicia abarca el ámbito de sus competencias en materia de Administración de Justicia, es decir, Tribunal Supremo, Audiencia Nacional y las Comunidades Autónomas de Extremadura, Castilla la Mancha, Castilla y León, Islas Baleares, Región de Murcia, Ceuta y Melilla, así como las comunidades con convenio de colaboración en vigor: Principado de Asturias y Comunidad Autónoma de La Rioja. 

Anteriormente citaba   
> Bajo la Ley 18/2011, se reconoce a los ciudadanos el derecho a relacionarse con la Administración de Justicia utilizando medios electrónicos para el ejercicio de los derechos previstos en el capítulo primero y séptimo del título III del libro III de la Ley Orgánica 6/1985, de 1 de junio, del Poder Judicial.

Desde ahí veo

[Servicios a la ciudadanía - poderjudicial.es](http://www.poderjudicial.es/cgpj/es/Servicios/Atencion-Ciudadana/Servicios-a-la-ciudadania)  

Un ejemplo real de denuncia en pdf citado en 
[Attac solicita que la Fiscalía practique diligencias en la investigación sobre Juan Carlos de Borbón](https://www.infolibre.es/politica/attac-solicita-fiscalia-practique-diligencias-investigacion-juan-carlos-borbon_1_1160948.html)  

Planteo consulta 15 julio 2020

> He tenido conocimiento de actuaciones que creo que son constitutivas de delito dentro de la administración:
-cesiones de suelo garantizando un concierto educativo cuando artículo 116.8 LOE no existía y no era legal ya que se exigía la existencia de centro antes de solicitar concierto  
-cesiones de suelo por un plazo superior al que permite la ley  
-cesiones de suelo a empresas creadas a tal efecto: la fecha de registro en BORME de la empresa adjudicataria es posterior a la cesión
Me gustaría poder enviar documento detallado, pero no veo cómo presentarlo de manera online a la fiscalía, a lo que considero que tengo derecho como ciudadano https://www.boe.es/buscar/act.php?id=BOE-A-2011-11605#a5  

Al conocer asociaciones hay que tener cuidado, me habían hablado de ACODAP y tras mirarlo en 2020 lo descarté. [El juez ordena la detención del presidente de la Asociación ACODAP y acuerda el ingreso en prisión de tres investigados](https://www.poderjudicial.es/cgpj/es/Poder-Judicial/Audiencia-Nacional/Oficina-de-Comunicacion/Notas-de-prensa/El-juez-ordena-la-detencion-del-presidente-de-la-Asociacion-ACODAP-y-acuerda-el-ingreso-en-prision-de-tres-investigados)  

En mismas fechas conozco [OLAF (Oficina Europea de Lucha contra el Fraude)](https://anti-fraud.ec.europa.eu/olaf-and-you/report-fraud_es)   

Febrero 2022 me pasa datos una persona que ya había denunciado en fiscalía:

> Presentar una denuncia ante la Fiscalía es un proceso muy sencillo. Ni tan siquiera tienes que entregar la denuncia personalmente: la puedes enviar por correo electrónico aportando las correspondientes pruebas. Te envío el enlace a la página web de la misma  
[Fiscal.es > Fiscalías Territoriales > Fiscalía Provincial de Madrid](https://www.fiscal.es/-/fiscalia-provincial-de-madrid)  
Primero llama a los teléfonos  914934668 - 914934506 para informarte si ha cambiado en algo el procedimiento de denuncia. Te van a coger el teléfono. Son unos funcionarios muy amables.  
Una vez que la Fiscalía recibe la denuncia, abre diligencias. Es en esa fase de investigación cuando el fiscal puede apreciar indicios de delito. Si los hay, a ti la Fiscalía le informa de ello, y envía los resultados de su investigación a un juzgado de instrucción. Si no ve indicios de delitos, la Fiscalía te informará de que se procede al archivo de las actuaciones. No obstante, te informará de que Ud. podrás denunciar los hechos de nuevo ante un juzgado por si el juez competente viese indicios de delito.  
Yo creo que la Fiscalía es un cartucho que siempre hay que utilizar antes de presentar una denuncia ante un juez, porque si la Fiscalía presenta una acusación de oficio por hallar indicios de delito, eso siempre da más fuerza a que se diriman responsabilidades penales.  

## Queja al CGPJ

Es algo que surge en enero 2025 con la declaración de Elisa Mouliaá y quejas por la actuación del juez Carretero

[Qué va a pasar con las diligencias abiertas al juez que tomó declaración a Mouliaá](https://www.newtral.es/expediente-juez-mouliaa-adolfo-carretero/20250123/)  

[Formulario de queja o reclamación ON LINE - poderjudicial.es](https://www.poderjudicial.es/cgpj/es/Servicios/Atencion-Ciudadana/Quejas-y-reclamaciones/Formulario-de-queja-o-reclamacion-ON-LINE/)

[bsky.app madridzapatista.bsky.social/post/3lgnihgixh22y](https://bsky.app/profile/madridzapatista.bsky.social/post/3lgnihgixh22y) 

[bsky.app angelluismm.bsky.social/post/3lgj7oqmgus2m](https://bsky.app/profile/angelluismm.bsky.social/post/3lgj7oqmgus2m)   

Órgano al que se refiere su queja: Juzgado de Instrucción nº 19 de Madrid.

Motivos: A Su Señoría Doña Inmaculada Iglesias, magistrada del Juzgado de Instrucción nº 19 de Madrid. Respetuosamente EXPONGO Que en marzo de 2025 prescribirá la causa contra don Alberto González Amador abierta en ese juzgado, si no se le toma previamente declaración. Por ello, RUEGO No se acepte ningún tipo de aplazamiento como el realizado para 7 febrero de 2025, y se proceda a citar al susodicho Alberto González Amador de manera inaplazable antes de la fecha de prescripción para que preste declaración por los cargos de los que se ha reconocido confeso. Es gracia que espero obtener de Vuestra Ilustrísima. 

Id de registro: 2025016235  
Fecha de registro: 26/01/2025  
Hora de registro: 03:47:27  
Nº exp. CGPJ: 16235/2025   

## Defensor del pueblo

El defensor del pueblo tiene un papel muy importante, porque es de los pocos reconocidos para presentar recursos de inconstitucionalidad.   Responde siempre, pero su respuesta no es vinculante  
[Presentar una queja - defensordelpueblo.es](https://www.defensordelpueblo.es/tu-queja/01-description/)  

Un ejemplo concreto 28 febrero 2019  
[Recurso de inconstitucionalidad contra el ‘Cambridge Analytica’ español](https://www.lainformacion.com/opinion/borja-adsuara/recurso-de-inconstitucionalidad-contra-el-cambridge-analytica-espanol/6493640/)  
> **1. ¿Por qué se pide que interponga el recurso al Defensor del Pueblo?**  
Según la Constitución([art. 162.1.a](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229&p=20110927&tn=1#a162)), están legitimados para interponer el recurso de inconstitucionalidad: el Presidente del Gobierno,el Defensor del Pueblo,50 Diputados, y 50 Senadores Y no parece que ni el Presidente del Gobierno, ni los partidos políticos vayan a hacerlo, pese a que en la última votación del Pleno del Senado [Unidos-Podemos](https://www.eldiario.es/tecnologia/Unidos-Podemos-recurrira-Constitucional-ideologicos_0_838166969.html) se comprometió a ello.  
**2. ¿Por qué tiene que ser antes del 6 de marzo?**  
La Ley Orgánica del Tribunal Constitucional ([art. 33.1](https://www.boe.es/buscar/act.php?id=BOE-A-1979-23709&p=20151017&tn=1#atreintaytres)) establece queel recurso de inconstitucionalidad se debe formular dentro del plazo de tres meses a partir de la publicación de la Ley. Como la nueva Ley Orgánica de Protección de Datos se publicó en el BOE del día 6 de diciembre de 2018 (paradójicamente, el Día de la Constitución), el recurso hay que presentarlo antes del 6 de marzo.  

**5 marzo 2019**  
[El Defensor del Pueblo lleva al Constitucional la Ley de Protección de Datos que permite el spam electoral](https://www.eldiario.es/tecnologia/Defensor-Pueblo-Constitucional-Proteccion-Datos_0_874562912.html)  
Presentará un recurso de inconstitucionalidad contra el texto, al entender que permite a los partidos políticos crear bases de datos ideológicas y enviar spam electoral  
Ahora el Constitucional deberá decidir si suspende la legalización de estas prácticas de cara a las elecciones generales, europeas y autonómicas de abril y mayo  

[PDLI: El recurso del Defensor del Pueblo contra la Ley de Protección de Datos confirma el atropello que representa esta norma para los derechos fundamentales](http://libertadinformacion.cc/el-recurso-del-defensor-del-pueblo-contra-la-nueva-ley-de-proteccion-de-datos-confirma-el-atropello-que-representa-esta-norma-para-los-derechos-fundamentales/)  


### Denuncias anónimas de corrupción/incumplimiento de normativa/canales internos denuncia de corrupción

Se cita en reclamaciones a UE

En 2021 veo esto (no operativo en 2024)  
[Canal de Confidencialidad. Canal de comunicación con la Asesoría Jurídica - asambleamadrid.es](https://web.archive.org/web/20210503102222/https://www.asambleamadrid.es/transparencia/canal-de-confidencialidad)  
>La ASAMBLEA DE MADRID proporciona este canal de comunicación con la Asesoría Jurídica para comunicar irregularidades, incumplimientos o comportamientos contrarios a las normas que rigen la Cámara regional madrileña.  
Las denuncias realizadas a través del presente Canal:  
Se tratarán dentro del anonimato y confidencialmente frente a cualquier persona, incluso las autoridades y altos cargos de la Asamblea.
Necesitarán de soporte probatorio.  

[Ley 2/2023, de 20 de febrero, reguladora de la protección de las personas que informen sobre infracciones normativas y de lucha contra la corrupción. Sistema interno de información en el sector privado. Artículo 10. Entidades obligadas del sector privado.](https://www.boe.es/buscar/act.php?id=BOE-A-2023-4513#a1-2)  
> 1. Estarán obligadas a disponer un Sistema interno de información en los términos previstos en esta ley:  
a) Las personas físicas o jurídicas del sector privado que tengan contratados cincuenta o más trabajadores.  

[Canal de protección del informante- aepd.es](https://www.aepd.es/la-agencia/transparencia/canal-proteccion-informante)  

En 2023 asociados a fondos europeos veo
[Plan de Recuperación, Transformación y Resiliencia en el MEFD > Medidas contra el fraude en el MEFD - educacionyfp.gob.es](https://www.educacionyfp.gob.es/destacados/plan-recuperacion-transformacion-resiliencia/medidas-antifraude.html)  

Hay un canal de denuncias [Canal Interno de Información del MEFD](https://educacionyfp.whistlelink.com/)  

[Comunicación de información sobre fraudes o irregularidades que afecten a fondos europeos - hacienda.gob.es](https://www.igae.pap.hacienda.gob.es/sitios/igae/es-ES/snca/Paginas/ComunicacionSNCA.aspx)  

### Denunciar en la Comisión Europea

Es un tema que trato en post [Denuncia en Comisión Europea por infracción del Derecho de la UE](http://algoquedaquedecir.blogspot.com/2018/01/denuncia-en-comision-europea.html)  
Tiene que ser algo de incumplimiento claro a nivel de derechos, pero creo que tristemente en España no nos falta de eso.  

**Ventajas, inconvenientes, ejemplos:**  
Intentado con tema [Madrid roba a los interinos (III): complementos en jornada parcial](http://algoquedaquedecir.blogspot.com/2017/12/madrid-roba-los-interinos-iii.html), pero el plazo es de un año, y eso asumiendo que lo estimen. Una vez lo estimasen, que se traduzca en algo concreto puede llevar tiempo.  
Es algo que también entiendo que se ha hecho por otros en [Concurso de traslados: baremación de la interinidad](http://algoquedaquedecir.blogspot.com/2017/11/concurso-de-traslados-baremacion-de-la.html) 

Ejemplo 2022 sancionador a España, que cita quejas particulares

[SENTENCIA DEL TRIBUNAL DE JUSTICIA (Gran Sala) «Incumplimiento de Estado — Responsabilidad de los Estados miembros por los daños causados a los particulares por infracciones del Derecho de la Unión — Infracción del Derecho de la Unión imputable al legislador nacional — Vulneración de la Constitución de un Estado miembro imputable al legislador nacional — Principios de equivalencia y de efectividad»](https://curia.europa.eu/juris/document/document.jsf;jsessionid=EFD86B6E63788E229EFEB2A69AA7DB4C?text=&docid=261801&pageIndex=0&doclang=ES&mode=req&dir=&occ=first&part=1&cid=7485812)  
>II.     **Procedimiento administrativo previo**  
16 A raíz de varias quejas formuladas por particulares, la Comisión inició, el 25 de julio de 2016, un procedimiento EU Pilot contra el Reino de España en relación con los artículos 32 y 34 de la Ley 40/2015. La Comisión invocaba una posible vulneración de los principios de equivalencia y de efectividad en la medida en que estos limitan la autonomía de que gozan los Estados miembros cuando establecen los requisitos que rigen su responsabilidad por las infracciones del Derecho de la Unión que les sean imputables. Al haber resultado infructuoso, dicho procedimiento concluyó y la Comisión inició un procedimiento de infracción.

## Modificar normativa

### Participación ciudadana al elaborar normativas

Se puede decir que no es una reclamación, pero sí es algo que permite a los ciudadanos indicar que algo no funciona y proponer cómo debería funcionar, por lo que en cierto modo sí lo es.

Es algo recogido en normativa aunque creo que poca gente lo conoce  
(Pendiente sacar a post separado sobre cómo se elaboran leyes, se citan ideas en post sobre derogación RDL14/2012)

Ley 50/1997, de 27 de noviembre, del Gobierno.  
[Artículo 26. Procedimiento de elaboración de normas con rango de Ley y reglamentos.](https://boe.es/buscar/act.php?id=BOE-A-1997-25336#a26)  
> La elaboración de los anteproyectos de ley, de los proyectos de real decreto legislativo y de normas reglamentarias se ajustará al siguiente procedimiento:  
>1. Su redacción estará precedida de cuantos estudios y consultas se estimen convenientes para garantizar el acierto y la legalidad de la norma.  
> 2. Se sustanciará una consulta pública, a través del portal web del departamento competente, con carácter previo a la elaboración del texto, en la que se recabará opinión de los sujetos potencialmente afectados por la futura norma y de las organizaciones más representativas acerca de:  
a) Los problemas que se pretenden solucionar con la nueva norma.  
b) La necesidad y oportunidad de su aprobación.  
c) Los objetivos de la norma.  
d) Las posibles soluciones alternativas regulatorias y no regulatorias.  
Podrá prescindirse del trámite de consulta pública previsto en este apartado en el caso de la elaboración de normas presupuestarias u organizativas de la Administración General del Estado o de las organizaciones dependientes o vinculadas a éstas, cuando concurran razones graves de interés público que lo justifiquen, o cuando la propuesta normativa no tenga un impacto significativo en la actividad económica, no imponga obligaciones relevantes a los destinatarios o regule aspectos parciales de una materia. También podrá prescindirse de este trámite de consulta en el caso de tramitación urgente de iniciativas normativas, tal y como se establece en el artículo 27.2. La concurrencia de alguna o varias de estas razones, debidamente motivadas, se justificarán en la Memoria del Análisis de Impacto Normativo.  
La consulta pública deberá realizarse de tal forma que todos los potenciales destinatarios de la norma tengan la posibilidad de emitir su opinión, para lo cual deberá proporcionarse un tiempo suficiente, que en ningún caso será inferior a quince días naturales.  
>3. El centro directivo competente elaborará con carácter preceptivo una **Memoria del Análisis de Impacto Normativo**, que deberá contener los siguientes apartados:  
a) Oportunidad de la propuesta y alternativas de regulación estudiadas, lo que deberá incluir una justificación de la necesidad de la nueva norma frente a la alternativa de no aprobar ninguna regulación.  
b) Contenido y análisis jurídico, con referencia al Derecho nacional y de la Unión Europea, que incluirá el listado pormenorizado de las normas que quedarán derogadas como consecuencia de la entrada en vigor de la norma.  
c) Análisis sobre la adecuación de la norma propuesta al orden de distribución de competencias.  
d) Impacto económico y presupuestario, que evaluará las consecuencias de su aplicación sobre los sectores, colectivos o agentes afectados por la norma, incluido el efecto sobre la competencia, la unidad de mercado y la competitividad y su encaje con la legislación vigente en cada momento sobre estas materias. Este análisis incluirá la realización del test Pyme de acuerdo con la práctica de la Comisión Europea.  
e) Asimismo, se identificarán las cargas administrativas que conlleva la propuesta, se cuantificará el coste de su cumplimiento para la Administración y para los obligados a soportarlas con especial referencia al impacto sobre las pequeñas y medianas empresas.  
f) Impacto por razón de género, que analizará y valorará los resultados que se puedan seguir de la aprobación de la norma desde la perspectiva de la eliminación de desigualdades y de su contribución a la consecución de los objetivos de igualdad de oportunidades y de trato entre mujeres y hombres, a partir de los indicadores de situación de partida, de previsión de resultados y de previsión de impacto.  
g) Un resumen de las principales aportaciones recibidas en el trámite de consulta pública regulado en el apartado 2.  
La Memoria del Análisis de Impacto Normativo incluirá cualquier otro extremo que pudiera ser relevante a criterio del órgano proponente.

...

Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas.  
[Artículo 133. Participación de los ciudadanos en el procedimiento de elaboración de normas con rango de Ley y reglamentos.](https://boe.es/buscar/act.php?id=BOE-A-2015-10565#a133)  
>1. Con carácter previo a la elaboración del proyecto o anteproyecto de ley o de reglamento, se sustanciará una consulta pública, a través del portal web de la Administración competente en la que se recabará la opinión de los sujetos y de las organizaciones más representativas potencialmente afectados por la futura norma acerca de:  
a) Los problemas que se pretenden solucionar con la iniciativa.  
b) La necesidad y oportunidad de su aprobación.  
c) Los objetivos de la norma.  
d) Las posibles soluciones alternativas regulatorias y no regulatorias.  
>2. Sin perjuicio de la consulta previa a la redacción del texto de la iniciativa, cuando la norma afecte a los derechos e intereses legítimos de las personas, el centro directivo competente publicará el texto en el portal web correspondiente, con el objeto de dar audiencia a los ciudadanos afectados y recabar cuantas aportaciones adicionales puedan hacerse por otras personas o entidades. Asimismo, podrá también recabarse directamente la opinión de las organizaciones o asociaciones reconocidas por ley que agrupen o representen a las personas cuyos derechos o intereses legítimos se vieren afectados por la norma y cuyos fines guarden relación directa con su objeto.  
>3. La consulta, audiencia e información públicas reguladas en este artículo deberán realizarse de forma tal que los potenciales destinatarios de la norma y quienes realicen aportaciones sobre ella tengan la posibilidad de emitir su opinión, para lo cual deberán ponerse a su disposición los documentos necesarios, que serán claros, concisos y reunir toda la información precisa para poder pronunciarse sobre la materia.  
>4. Podrá prescindirse de los trámites de consulta, audiencia e información públicas previstos en este artículo en el caso de normas presupuestarias u organizativas de la Administración General del Estado, la Administración autonómica, la Administración local o de las organizaciones dependientes o vinculadas a éstas, o cuando concurran razones graves de interés público que lo justifiquen.  
Cuando la propuesta normativa no tenga un impacto significativo en la actividad económica, no imponga obligaciones relevantes a los destinatarios o regule aspectos parciales de una materia, podrá omitirse la consulta pública regulada en el apartado primero. Si la normativa reguladora del ejercicio de la iniciativa legislativa o de la potestad reglamentaria por una Administración prevé la tramitación urgente de estos procedimientos, la eventual excepción del trámite por esta circunstancia se ajustará a lo previsto en aquella.  
> Se declara contrario al orden constitucional de competencias en los términos del f.j. 7 b) y, salvo el inciso de su apartado primero «Con carácter previo a la elaboración del proyecto o anteproyecto de ley o de reglamento, se sustanciará una consulta pública» y el primer párrafo de su apartado 4, en los términos del f.j. 7 c), por Sentencia del TC 55/2018, de 24 de mayo. [Ref. BOE-A-2018-8574](https://www.boe.es/buscar/doc.php?id=BOE-A-2018-8574)

En el recuardo "f.j." hace referencia al Fundamento Jurídico de la sentencia del TC.

Pongo como ejemplo concreto es el Ministerio de Educación  
[Participación pública en proyectos normativos](https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/informacion-publica.html)  
>**Punto de acceso para facilitar la participación pública en el procedimiento de elaboración de normas.**  
Este canal se constituye como el punto de acceso a los trámites de consulta pública previa y de audiencia e información pública. Estos trámites son necesarios en el proceso de elaboración de los anteproyectos de ley, proyectos de real decreto legislativo y proyectos de normas reglamentarias que se impulsen por el Ministerio y sus organismos públicos dependientes o vinculados. De esta manera se da conformidad a lo regulado en el art. 133 de la [Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas](http://boe.es/buscar/act.php?id=BOE-A-2015-10565) y en el art. 26 de la [Ley 50/1997, de 27 de noviembre, del Gobierno](http://boe.es/buscar/act.php?id=BOE-A-1997-25336) 
Con este este punto de acceso se da cumplimiento a lo dispuesto en la [Orden PRE/1590/2016, de 3 de octubre, por la que se publica el Acuerdo del Consejo de Ministros de 30 de septiembre de 2016, por el que se dictan instrucciones para habilitar la participación pública en el proceso de elaboración normativa a través de los portales web de los departamentos ministeriales](http://boe.es/buscar/doc.php?id=BOE-A-2016-9121)

Hay 2 categorías

**Consulta pública previa.**

Los trámites de consulta pública previa tienen por objeto recabar la opinión de ciudadanos, organizaciones y asociaciones antes de la elaboración de un proyecto normativo.

**Audiencia e información pública.**

Los trámites de audiencia e información pública tienen por objeto recabar la opinión de los ciudadanos titulares de derechos e intereses legítimos afectados por un proyecto normativo ya redactado, directamente o a través de las organizaciones o asociaciones que los representen, así como obtener cuantas aportaciones adicionales puedan realizar otras personas o entidades.

**Ventajas/inconvenientes/ejemplos:** 
En mi opinión es algo poco útil mientras no haya transparencia en qué se hace con las opiniones recibidas. En ocasiones se ha filtrado el texto normativo y no ha cambiado una coma a pesar de lo que se ha enviado, ni al pasar por el Consejo Escolar de Estado. Si hubieran tirado las aportaciones sin leerlas nadie notaría la diferencia.

Creo que decir que la LOMCE, impuesta por el rodillo de la mayoría absoluta y con la oposición de todos, se sometió a este proceso, lo dice todo.  

[Ley Orgánica para la Mejora de la Calidad Educativa (LOMCE)](https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2013/lomce.html)  

Mucha gente y yo mismo envié cosas, e hicieron un "resumen de aportaciones de los ciudadanos" en las que no se ven.  
[Aportaciones de los ciudadanos](https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2013/lomce/20121115-aportaciones.html)  

A veces sí las detallan bien, porque les interesa que no nadie pueda cuestionar la tramitación. 
[twitter FiQuiPedia/status/1275437867125243906](https://x.com/FiQuiPedia/status/1275437867125243906)  
935 alegaciones, que MAIN detalla ok en p31-47,52-97 ¿por qué no se detalla lo mismo en todas las MAIN tras trámite de audiencia?  
[Anteproyecto de Ley por el que se modifica la Ley 9/2001, de 17 de julio, del Suelo de la Comunidad de Madrid, para el impulso y reactivación de la actividad urbanística](https://www.comunidad.madrid/transparencia/normativa/anteproyecto-ley-modificacion-ley-92001-del-suelo-comunidad-madrid-0)  
[MEMORIA DE ANÁLISIS DE IMPACTO NORMATIVO DEL ANTEPROYECTO DE LEY POR EL QUE SE MODIFICA LA LEY 9/2001, DE 17 DE JULIO, DEL SUELO DE LA COMUNIDAD DE MADRID PARA EL IMPULSO Y REACTIVACIÓN DE LA ACTIVIDAD URBANÍSTICA.](https://www.comunidad.madrid/transparencia/sites/default/files/regulation/documents/02-maintrasaudienciapublicav1.pdf)  

Creo que hace falta mucha más transparencia.

### Participación ciudadana en comisiones congreso

Lo descubro en julio 2020, pasado el plazo

[Presentación de propuestas a la Comisión para la Reconstrucción Social y Económica - waybackmachine](https://web.archive.org/web/20201111010556/http://www.congreso.es/portal/page/portal/Congreso/Congreso/ParticipacionCiudadana)

**Presentación de propuestas a la Comisión para la Reconstrucción Social y Económica**

El objeto de la Comisión para la Reconstrucción Social y Económica es la recepción de propuestas, la celebración de debates y la elaboración de conclusiones sobre las medidas a adoptar para la reconstrucción social y económica, como consecuencia de la crisis del COVID-19.
Con el fin de fomentar y facilitar la participación ciudadana, el Congreso de los Diputados ha habilitado un buzón de correo electrónico al que se podrán enviar sugerencias y propuestas relacionadas con el trabajo de esta Comisión.
Las aportaciones podrán referirse a uno de los cuatro grupos de trabajo creados en el seno de la Comisión que aparecen a continuación.
- Políticas Sociales y Sistema de Cuidados
- Unión Europea
- Reactivación Económica
- Sanidad y Salud Pública Se ruega la mayor claridad y concreción posibles en las propuestas, que deberán enviarse a [comisionRSE@congreso.es ](mailto:comisionRSE@congreso.es)
**Aviso importante**
**El plazo para la presentación de propuestas y aportaciones ciudadanas quedó cerrado el lunes 29 de junio a las 12 horas a.m.**

Sí hay transparencia en las aportaciones recibidas, muchas a nivel individual, aunque no se pueden consultar en 2024

[Comisión para la Reconstrucción Social y Económica](https://web.archive.org/web/20200805172712/http://www.congreso.es/portal/page/portal/Congreso/Congreso/Organos/Comision?_piref73_7498063_73_1339256_1339256.next_page=/wc/documentacionInformComisiones&idOrgano=390&idLegislatura=14)

## Contactar con políticos 

Los políticos son los que realmente cambian las normativas. A veces se puede hacer vía ILP pero es complicado, luego pasa también por los políticos.  
Hay dos diferencias claras:  
* Políticos que no están en el gobierno (personas o grupos, oposición)  
* Políticos que sí están en el gobierno (cargos, forman parte de la administración o de grupos que no están en la oposición)  

El gobierno presenta proyectos de ley, la oposición propuestas, y llevan caminos distintos.  
Twitter es algo peculiar porque en teoría permite interactuar directamente con mucha gente, pero eso no es realista. Se puede intentar contactar con alguien para que él lo traslade hacia cargos más altos.  
Existen vías de participación peculiar, que no he usado y de las que desconozco la efectividad, por ejemplo

[Escribir al presidente - lamoncloa.gob.es](https://www.lamoncloa.gob.es/presidente/escribiralpresidente) 

En agosto 2018 uso por primera vez tema a escribir presidente, dentro de post
[El Gobierno no cumple la Ley de Transparencia ni hace caso a Consejo de Transparencia y Buen Gobierno](http://algoquedaquedecir.blogspot.com/2017/08/el-gobierno-no-cumple-la-ley-de.html))

Se podría plantear  no contactar con políticos sino unirse a ellos siendo uno mismo político. Fundar un nuevo partido político lo veo algo excepcional.

En 2021 conozco la plataforma [Osoigo](https://www.osoigo.com/)  

## Iniciativa Legislativa Popular

Es algo que enlaza con recogida de firmas y con contactar con políticos.  
Está regulado en [artículo 87.3 de la Constitución](https://app.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=87&tipo=2), que indica 500 000 firmas y que pone ciertas limitaciones.  
> 3. Una ley orgánica regulará las formas de ejercicio y requisitos de la iniciativa popular para la presentación de proposiciones de ley. En todo caso se exigirán no menos de 500.000 firmas acreditadas. No procederá dicha iniciativa en materias propias de ley orgánica, tributarias o de carácter internacional, ni en lo relativo a la prerrogativa de gracia.

La ley orgánica que lo regula es [Ley Orgánica 3/1984, de 26 de marzo, reguladora de la iniciativa legislativa popular.](https://www.boe.es/buscar/act.php?id=BOE-A-1984-7249)

Artículo séptimo se modificó en 2006 y permite recogida de firmas digitales.


## Acciones para intentar modificar comportamiento administración

### Recogida de firmas

La recogida de firmas (en papel o a través de plataformas online tipo change.org) creo que no es efectiva si no se consigue un volumen muy elevado, y ese volumen elevado creo que hace que el tema llegue a interesar a ciertas personas y se mueva a otros niveles.  
Enlaza también con tema de ILP que trato separado, que en Constitución se indica que necesita medio millón de firmas.  
El tema de los datos personales en las firmas (qué hace change.org con ellos, qué hacen con las hojas de papel firmadas con DNI, nombre y apelllidos) y LOPD/RGPD no lo veo nada claro.  
El caso del centro concertado en Torrejón en 2018 se recogieron firmas, pero creo que la presión real fue llegar a la Asamblea de Madrid y la plataforma ciudadana.  
También puede enlazar con adhesiones a manifiesto y formar opinión/concienciar

## Huelga, encierro, manifestación 

La huelga es una vía de reclamación, aunque tiene su proceso legal; suele ser convocada por sindicatos (no es obligatorio) y definir servicios mínimos.  
Ver [Autogestiona tu propia huelga](https://soypublica.wordpress.com/2012/06/21/autogestiona-tu-propia-huelga/)  
> Si no lo sabías, tal vez se deba a que los representantes legales han procurado presentarse a nuestros ojos en el transcurso de las movilizaciones como los únicos agentes capaces de convocar una huelga. Esto es absolutamente falso. De hecho, esta posibilidad de convocar huelga sin respaldo sindical se extiende incluso a los centros de trabajo, es decir, que un solo centro educativo puede ser pionero a la hora de consensuar una huelga y convocarla, de modo que la huelga en educación sea una verdadera resistencia al modo de aldeas de Asterix.  
Creemos que este modelo puede hacer que a partir de los centros más concienciados la huelga se extienda a diversos puntos del Estado.  
A continuación reproducimos la normativa al respecto, así como los documentos legales que en que se haya recogida y que han sido debidamente contrastados. Para más información al respecto, recomendamos leer al completo el citado real decreto, así como [la sentencia del Tribunal Constitucional 11/1981, de 8 de abril](http://tc.vlex.es/vid/53-11-12-13-16-an-17-28-37-18-15034939), que interpreta ciertos aspectos del mismo.  
[El Real Decreto-ley 17/1977, de 4 de marzo, establece en su Título I , Capítulo I](http://noticias.juridicas.com/base_datos/Admin/rdl17-1977.t1.html#), que versa sobre  el derecho a huelga, que la convocatoria de esta puede ser realizada por:  
1.- Representantes de los trabajadores (Comités de empresa o Delegados de personal)  
2.- Sindicatos  
3.- Trabajadores del centro de trabajo o de la empresa (este es el punto que nos interesa)  
Aunque el art. 3 del RD mencionado especifica que en este último caso la decisión deberá adoptarse por votación secreta, decidida por mayoría simple, con una participación de al menos el 25% de trabajadores de la empresa, la sentencia 11/1981 aclara que “el ejercicio del derecho de huelga, que pertenece a los trabajadores, puede ser ejercitado por ellos personalmente, por sus representantes y por las organizaciones sindicales con implantación en el ámbito laboral al que la huelga  se extienda, y que son inconstitucionales las exigencias establecidas en dicho artículo de que el acuerdo de huelga se adopte en cada centro de trabajo, la de que a la reunión de los representantes haya de asistir un determinado porcentaje y la de que la iniciativa para la declaración de huelga haya de estar apoyada por un 25 por 100 de los trabajadores”.  
Así pues, la convocatoria de huelga debe hacerse tras una asamblea de los trabajadores en la que se señale como punto único en el orden del día la decisión de la convocatoria de una huelga, y en la que se decida por mayoría simple en una votación secreta. Pero no existe ningún requisito legal que exija una participación mínima de los trabajadores ni un tanto por ciento de voto favorable a la huelga sobre el total de los trabajadores.  
La convocatoria de huelga puede realizarse por centros de trabajo o bien en el ámbito de toda la empresa (en este caso la Consejería de Educación de cada comunidad autónoma).  
Una vez realizada la asamblea, si la decisión es de convocar huelga, hay que notificarlo a la empresa (Consejería de Educación de la Comunidad y ala Autoridad Laboral-Consejería de Trabajo de la Comunidad), con al menos 10 días naturales de antelación a la fecha de inicio de la huelga, pues es un sector que ofrece servicios esenciales para la comunidad. La comunicación de huelga deberá contener las reivindicaciones u objetivos de esta, las gestiones realizadas para resolver las diferencias, fecha de su inicio y composición del comité de huelga.  

[Procedimiento de declaración y notificación de huelga - iberley.es](https://www.iberley.es/temas/procedimiento-declaracion-notificacion-huelga-161)  

En algunos colectivos no hay unidad ni voluntad de seguirlas, y a veces no tiene efecto si no es indefinida, lo que supone que muchos no cobran y no pueden seguirla, a no ser que se hagan bolsas de resistencia como se hizo en la marea blanca de Madrid.  
A veces las huelgas tienen elementos que las destrozan: plantearlas mal, o un pequeño porcentaje de personas que usen violencia o supongan daños o no lo hagan bien condiciona la opinión sobre el resto. Ejemplos son las huelgas de los controladores aéreos y de taxis.  
A veces la huelga de celo, cumplir solamente horario y tareas obligatorias, puede ser más efectiva: creo que en educación una huelga de celo sería lo que conseguiría de verdad mostrar la sobrecarga docente y mejorar las condiciones.  

Ojo: la huelga de celo es ilegal  
[Real Decreto-ley 17/1977, de 4 de marzo, sobre relaciones de trabajo. Artículo 7](https://www.boe.es/buscar/act.php?id=BOE-A-1977-6061#asiete)  
Dos. Las huelgas rotatorias, las efectuadas por los trabajadores que presten servicios en sectores estratégicos con la finalidad de interrumpir el proceso productivo, las de celo o reglamento y, en general, cualquier forma de alteración colectiva en el régimen de trabajo distinta a la huelga, se considerarán actos ilícitos o abusivos.  

Una idea de conseguir objetivo reclamado con la huelga puede ser focalizar la huelga: acotar muy bien ámbito de la huelga (en educación solo en un centro educativo) y con unos objetivos muy concretos (recuperar una línea perdida, ratio de PT ...). Sería una huelga "claustro a claustro"

[twitter CGTMadrideduc/status/1172028597277732864](https://twitter.com/CGTMadrideduc/status/1172028597277732864)  
Comunicado de Enrique Díaz, compa de CGT, docente del CRA El Jarama y delegado en DAT Norte   
Dijeron #AsíNoEmpezamos, y lo consiguieron  
"Es importante que sintamos que sí se puede, que claro que la lucha, la huelga y la movilización dan resultados"

12 septiembre 2019
[https://twitter.com/CGTMadrideduc/status/1172220749723119617](https://twitter.com/CGTMadrideduc/status/1172220749723119617)  
CEIP Montelindo de Bustarviejo convoca PAROS INDEFINIDOS  
La movilización se expande: otro claustro que se suma a la acción #AsíNoEmpezamos  
Basta ya de recortes y desmantelamiento de la escuela pública y especialmente la rural  
Nota de prensa aquí  

27 mayo 2022
[Seguimiento masivo de docentes y familias en la jornada de huelga en el colegio Miguel Hernández de Getafe](https://feccoo-madrid.org/noticia:627578--Seguimiento_masivo_de_docentes_y_familias_en_la_jornada_de_huelga_en_el_colegio_Miguel_Hernandez_de_Getafe)  
El 99% del profesorado y personal educativo y el 95% de las familias han secundado la huelga convocada por CCOO para reclamar recursos de atención al alumnado con diversidad  

Los encierros pueden ser más o menos efectivos según su duración; de nuevo tienen que tener pretensión de durar para ser efectivos, y eso es complicado.

Realizar manifestaciones es algo que a veces sirve poco y no tiene un efecto claro

Cito este hilo
[https://twitter.com/elnocturno/status/1139965260075610112](https://twitter.com/elnocturno/status/1139965260075610112)  
9/ A Greta, sus papás Pistilo y Pétalo seguramente le han dicho que las manifestaciones pueden cambiar el mundo y quizás tienen algún mito sobre la generación de las flores, el 68, peace and love y un San Francisco psicodélico sin su Charles Manson. Pero no es así.  
10/ Claro que las manifestaciones sirven a las luchas, pero no SON las luchas, no bastan, no son autosuficientes. Una tragedia general del activismo post-Greenpeace es creer que "tienes que arreglar esto porque está mal" es una afirmación capaz de generar decisiones políticas.  

[https://twitter.com/Sayo_cab75/status/1139940362024292352](https://twitter.com/Sayo_cab75/status/1139940362024292352)  
En Hong Kong durante días se han producido manifestaciones Multitudinarias contra una polémica ley de extradición a China.. Hoy el gobierno ha retirado la ley.. Para cuando digan que la lucha en la calle no sirve.. Aquí estamos dormidos y nos chulean.

[https://twitter.com/goldie_gemma/status/1140150085239672835](https://twitter.com/goldie_gemma/status/1140150085239672835)  
Articular una protesta para un cambio concreto es algo muy diferente. Luchar contra"el cambio climático" es incomparable a "protestar por una ley para que se retire".

Y surge la idea de hacer cosas concretas, que creo que es muy importante y aplica a todo el post. Le creo un apartado.

## Otras ideas/ enlaces

En 2022 veo esto 

[Derecho a la Participación de los niños, niñas y adolescentes- comunidad.madrid](https://www.comunidad.madrid/servicios/asuntos-sociales/derecho-participacion-ninos-ninas-adolescentes)  
La Convención de Derechos del Niño de Naciones Unidas, de 20 de noviembre de 1989, (CDN) reconoce a todas las personas menores de 18 años como sujetos de pleno derecho. Por primera vez y de forma global se reconoce la plena ciudadanía de la infancia.  
Para ejercer esta ciudadanía la Convención declara como uno de sus cuatro principios fundamentales la Participación de la infancia y la adolescencia.  
infancia.adolescencia@madrid.org 

Ver parte final de post [Privados con concierto en etapas no obligatorias: lo singular como normal](http://algoquedaquedecir.blogspot.com/2017/08/privados-con-concierto-en-etapas-no.html) dentro del apartado ¿Qué se puede hacer y cómo para quitar esos conciertos ilegales?
Algunas se repiten, pendiente unificar

Crear un partido político y un sindicato serían otras ideas.

**24 marzo 2018**
[Para qué sirve protestar - nytimes.com](https://www.nytimes.com/es/2018/03/24/sirve-protestar-brasil-lula/)  
Algunas veces, no se logra nada concreto de inmediato, pero nuevas ideas ingresan al ámbito de lo posible. Tal vez más tarde asciendan al ámbito de lo necesario y, después, al de lo inevitable.  
...  
Los mítines también podrían ser una forma de generar una nueva conciencia colectiva y promover la solidaridad a mayor escala. Desde la perspectiva del partido gobernante, nada es más democrático que aceptar esto. Las protestas también pueden tener una especie de belleza kantiana, independiente de los resultados. Como Daniel Cohn-Bendit, líder estudiantil de las protestas de 1968 en París, escribió: “Percibimos algo, fugazmente, que luego se desvanece. Pero basta para demostrar que ese algo puede existir”.

Protestar es un derecho
[Right to protest](https://www.opendemocracy.net/en/right-protest/) 

