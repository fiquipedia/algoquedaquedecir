+++
author = "Enrique García"
title = "Becas para el estudio de programas de segunda oportunidad Madrid"
date = "2021-02-21"
tags = [
    "educación", "Madrid", "Privatización", "post migrado desde blogger"
]
toc = true

+++

Revisado 16 marzo 2024

## Resumen

En Madrid existen unas "becas" llamadas de segunda oportunidad en las que el 99,7% de importe y 96% beneficiarios va a centros privados, con o sin concierto. 

![](https://pbs.twimg.com/media/ERn7uj4WkAIZNXJ?format=png) 

Posts relacionados

* [Becas y ayudas educación Madrid: manipulación y conciertos](https://algoquedaquedecir.blogspot.com/2018/11/becas-y-ayudas-educacion-madrid.html)
* [Cheque Bachillerato Madrid](https://algoquedaquedecir.blogspot.com/2018/10/cheque-bachillerato-madrid.html)
* [LOMCE: FSE y educación](https://algoquedaquedecir.blogspot.com/2019/06/lomce-fse-y-educacion.html) (al estar financiadas con FSE)

## Detalle

Este tipo de becas las trataba inicialmente dentro del post [Becas y ayudas educación Madrid: manipulación y conciertos](https://algoquedaquedecir.blogspot.com/2018/11/becas-y-ayudas-educacion-madrid.html) pero en 2021 creo post separado.

Está relacionado con FP y a veces se mezcla / sale información combinada.  
Está relacionado con FSE, ver post [LOMCE: FSE y educación](https://algoquedaquedecir.blogspot.com/2019/06/lomce-fse-y-educacion.html)

[Becas para el estudio de Programas de Segunda Oportunidad](https://www.comunidad.madrid/servicios/educacion/becas-estudio-programas-segunda-oportunidad)  
Cofinanciadas en un 91,89% por el [Fondo Social Europeo](http://ec.europa.eu/esf/home.jsp?langId=es) (FSE) dentro del Programa Operativo de Empleo Juvenil (POEJ)

En 2022 se pasa a financiar con FSE+

[ORDEN 1643/2022, de 13 de junio, del Consejero de Educación, Universidades, Ciencia y Portavoz del Gobierno, por la que se aprueba el Plan Estratégico de Subvenciones para el estudio de programas de segunda oportunidad correspondientes al curso 2022-2023.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/06/23/BOCM-20220623-21.PDF)

Las becas para la realización de Programas de Segunda Oportunidad estarán cofinanciadas por el FSE+ del periodo de programación 2021-2027, bajo el marco del Programa Operativo que elabore al efecto la Comunidad de Madrid o bajo su ámbito, y con la tasa de cofinanciación mínima del 40 %, en el objetivo político 4, y en el Eje/Prioridad de Inversión/Objetivo Específico que en su momento queden determinados en el Programa Operativo.

### Información cronológica / normativa

**21 septiembre 2017**

[twitter sanz_ismael/status/910822137866457091](https://twitter.com/sanz_ismael/status/910822137866457091)

Comunidad de Madrid lanza por 1ª vez Becas FP de Grado Superior públicos y privados.Hasta 27 septiembre #DER17Madrid 

Enlace no operativo en 2021, pero imagen es sobre Becas segunda oportunidad

![](https://pbs.twimg.com/media/DKPjjBHXUAAad3K?format=jpg) 

**21 septiembre 2018**

[twitter sanz_ismael/status/1043197993267154945](https://twitter.com/sanz_ismael/status/1043197993267154945)

Estudias FP Media o FP Superior en Comunidad de Madrid? 10 millones de euros en becas para pagar la matrícula. Solicitudes hasta el 4 de octubre en http://www.comunidad.madrid/servicios/educacion/becas-estudio-programas-segunda-oportunidad

Enlaza una imagen de este artículo

[Suben las ayudas para formar a los jóvenes sin estudios - magisnet](https://www.magisnet.com/2018/09/suben-las-ayudas-para-formar-a-los-jvenes-sin-estudios/)

En inicio curso 2019-2020 miro detalles tras ver publicación 2 septiembre 2019

Becas para el estudio de Programas de Segunda Oportunidad (Bases Reguladoras)
http://www.madrid.org/cs/Satellite?c=CM_ConvocaPrestac_FA&cid=1354725536337&noMostrarML=true&pageid=1331802501637&pagename=PortalCiudadano%2FCM_ConvocaPrestac_FA%2FPCIU_fichaConvocaPrestac&vest=1331802501621
En "normativa aplicable" se cita:

* [Extracto de la Orden 1630/2019, de 22 de mayo, de la Consejería de Educación e Investigación, por la que se aprueba la convocatoria de becas para el estudio de Programas de Segunda Oportunidad correspondiente al curso 2019-2020, cofinanciadas por el Fondo Social Europeo y la Iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil (BOCM nº 208, de 2 de septiembre).](https://www.bocm.es/boletin/CM_Orden_BOCM/2019/09/02/BOCM-20190902-16.PDF)  
* [Orden 2257/2018, de 11 de julio, de la Consejería de Educación e Investigación, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo y la iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil (BOCM nº 168, de 16 de julio)](http://www.bocm.es/boletin/CM_Orden_BOCM/2018/07/16/BOCM-20180716-32.PDF)  

Tras mirar, intento resumir en un tuit, con enlaces

https://twitter.com/FiQuiPedia/status/1168957410809131019
"Becas" programa segunda oportunidad Madrid, usan FSE  
http://comunidad.madrid/servicios/educacion/becas-estudio-programas-segunda-oportunidad  
http://pap.hacienda.gob.es/bdnstrans/GE/es/convocatoria/461065  
Centros privados: 2800€  
Públicos: la tasas (400€ en FPGS). Curso pasado solo unos 100 alumnos de la pública  
"Becas" sin límite renta, se fija a posteriori tras gastar 10  

Cito 4 imágenes, las pongo aquí como texto y con enlaces:

[EXTRACTO de la Orden 1630/2019, de 22 de mayo, de la Consejería de Educación e Investigación, por la que se aprueba la convocatoria de becas para el estudio de Programas de Segunda Oportunidad correspondiente al curso 2019-2020, cofinanciadas por el Fondo Social Europeo y la Iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil](http://www.bocm.es/boletin/CM_Orden_BOCM/2019/09/02/BOCM-20190902-16.PDF)  
>Tercero Bases reguladoras  
Orden 2257/2018, de 11 de julio, de la Consejería de Educación e Investigación, publicada en el BOLETÍN OFICIAL DE LA COMUNIDAD DE MADRID de 16 de julio de 2018.  
Cuarto  
Cuantía El presupuesto destinado a financiar la presente convocatoria asciende a 10.000.000 euros. La dotación de las becas, con carácter general, será de 2.800 euros anuales, distribuidos mensualmente según la cantidad que corresponda por cada uno de los meses de duración del curso y dentro del período de septiembre de 2019 a junio de 2020, ambos inclusive. No obstante, en ningún caso la cuantía total de la beca superará el coste del curso subvencionado.  

[ORDEN 2257/2018, de 11 de julio, de la Consejería de Educación e Investigación, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo y la iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil.](http://www.bocm.es/boletin/CM_Orden_BOCM/2018/07/16/BOCM-20180716-32.PDF)  
>Artículo 3  
Destinatarios de las becas  
Serán destinatarios de las becas los jóvenes inscritos en el fichero del Sistema Nacional de Garantía Juvenil del Ministerio de Empleo y Seguridad Social, que en el año de la convocatoria realicen, en modalidad presencial, alguno de los siguientes estudios:  
a) Curso de preparación de la prueba para la obtención del Título de Graduado en Educación Secundaria Obligatoria, para mayores de 18 años, en centros privados, situados en el ámbito territorial de la Comunidad de Madrid, autorizados por la Consejería competente en materia de Educación.  
b) Curso de formación específico, para el acceso a ciclos formativos de Formación Profesional de Grado Medio, en centros privados, situados en el ámbito territorial de la Comunidad de Madrid, autorizados por la Consejería competente en materia de Educación.  
c) Curso de Formación Profesional de Grado Medio en centros privados, situados en el ámbito territorial de la Comunidad de Madrid, autorizados por la Consejería competente en materia de Educación.  
d) Curso de formación específico, para el acceso a ciclos formativos de Formación Profesional de Grado Superior, en centros públicos, concertados o privados, situados en el ámbito territorial de la Comunidad de Madrid, autorizados por la Consejería competente en materia de Educación.  
e) Curso de Formación Profesional de Grado Superior en centros públicos, concertados o privados, situados en el ámbito territorial de la Comunidad de Madrid, autorizados por la Consejería competente en materia de Educación.  
f) Programas profesionales, dirigidos a la obtención de Certificados de Cualificación Profesional de nivel 1 por alumnos que no tengan título de Educación Secundaria Obligatoria, desarrollados por Ayuntamientos u otras entidades sin ánimo de lucro que desplieguen su actividad dentro del ámbito territorial de la Comunidad de Madrid y no reciban para ello financiación de la Consejería de Educación, Juventud y Deporte u otro organismo público.  
...  
Artículo 13  
Publicación de los listados provisionales de admitidos y excluidos  
>1. Una vez finalizada la instrucción del procedimiento a que se refiere el artículo 9 de esta orden se publicarán en la página web institucional de la Comunidad de Madrid www.madrid.org los listados provisionales de admitidos y excluidos, con indicación de la causa de exclusión, en su caso. Asimismo, se indicará el límite de renta que haya resultado aplicable según los criterios de baremación.  
>2. La consulta detallada de las solicitudes que figuren en las listas provisionales se podrá realizar en la página web institucional de la Comunidad de Madrid www.madrid.org 
Complementariamente, los listados se expondrán en los tablones de anuncios de las Direcciones de Área Territorial y en la Oficina de Información de la Consejería competente en materia de educación.  
>3. Figurar en estas listas provisionales no supondrá adquirir la condición de beneficiario de la convocatoria, condición que se obtendrá únicamente en la resolución de la convocatoria prevista en el artículo 18 de estas bases reguladoras.  
>4. Cuando se acredite la condición de víctima de violencia de género el nombre del centro de matriculación del alumno no figurará en las listas

[Orden 1630 /2019 de la Consejería de Educación e Investigación por la que se aprueba la convocatoria de becas para el estudio de Programas de Segunda Oportunidad, correspondiente al curso 2019-2020, cofinanciada por el Fondo Social Europeo y la Iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil.](http://www.pap.hacienda.gob.es/bdnstrans/GE/es/convocatoria/662625/document/315965)  
> Artículo 17. Cuantía de la beca.  
La cuantía total máxima de la beca para el curso académico 2019-2020 será de 2.800 euros, distribuidos mensualmente según la cantidad que corresponda por cada uno de los meses de duración del curso y dentro del período de septiembre de 2019 a junio de 2020, ambos inclusive. No obstante, en ningún caso la cuantía total de la beca superará el coste del curso subvencionado.  
Cuando la formación sea impartida por un centro docente público, se abonará el importe total que corresponda en concepto beca en un único pago el último mes del curso académico, previa comprobación del cumplimiento de los requisitos por el alumno.  
...  
Artículo 19. Obligaciones de los centros de enseñanza.  
Siguiendo lo dispuesto en los artículos 21 y 24 de la Orden 2257/2018, de 11 de julio, de la Consejería de Educación e Investigación, por la que se aprueban las bases reguladoras para* *la concesión de becas para el estudio de Programas de Segunda Oportunidad, el centro  donde el alumno realice el curso deberá acreditar documentalmente la asistencia del alumno, los cobros y los pagos. El centro es el responsable de acreditar, mediante un certificado a* *mes vencido, el coste del curso y la asistencia del alumno de, al menos, el 90 % de las horas  lectivas, con arreglo al siguiente procedimiento:  
>1. Con carácter previo al abono de la beca, los centros de enseñanza deben certificar el cumplimiento de las obligaciones de la asistencia del alumno al curso mediante la presentación a la entidad colaboradora del certificado que consta como Anexo IV de esta convocatoria.  
>2. Asimismo, los centros están obligados a emitir al alumno justificantes de gasto (facturas o recibos) y justificantes del pago de la beca (recibís firmados por el alumno). Dichos justificantes tendrán todos los requisitos legales y se dejará constancia fehaciente de su  recepción por parte de éste. En caso de retraso en el pago de la beca al alumno, se podrá penalizar al centro con el interés de demora que se establezca en la Resolución del Director General de Becas y Ayudas al Estudio. Si se trata de un centro docente público debe aportar fotocopia del pago de la tasa por el alumno (modelo 030). 

**20 agosto 2020**

[ORDEN 1885/2020, de 17 de agosto, de la Consejería de Educación y Juventud, por la que se modifican determinados requisitos para la concesión de becas, así como el cómputo de los miembros de la unidad familiar establecidos en la Orden 2257/2018, de 11 de julio, de la Consejería de Educación e Investigación, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo y la iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil.](https://www.bocm.es/boletin/CM_Orden_BOCM/2020/08/20/BOCM-20200820-7.PDF)

Se añade excepción 13.2 Ley 38/2003, y se cita 

>La experiencia en la ejecución de estas ayudas muestra que el número de beneficiarios queda limitado por el cumplimiento de una serie de requisitos principalmente de naturaleza fiscal. Dada la naturaleza de esta subvención, destinada a garantizar la igualdad de todos los estudiantes en el acceso a la educación, se ha considerado necesario la modificación de la normativa anterior, de manera que no se excluya a los solicitantes por no cumplir los requisitos establecidos en el artículo 13.2 de la Ley 38/2003, de 17 de diciembre, General de
Subvenciones. En este sentido, el Real Decreto 1721/2007, de 21 de diciembre, por el que se establece el régimen de becas y ayudas al estudio personalizadas, establece en su artículo 4.5 que se podrá obtener la condición de beneficiario de beca aunque no se cumplan los requisitos establecidos en el citado artículo 13.2 de la Ley 38/2003.</i>   

Real Decreto 1721/2007, de 21 de diciembre, por el que se establece el régimen de las becas y ayudas al estudio personalizadas.  
[Artículo 4. Condiciones de los beneficiarios.](https://www.boe.es/buscar/act.php?id=BOE-A-2008-821#a4)

También se elimina mención a privados de artículo 3, ver [versión consolidada Orden 2257/2018 en wleg](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=10432)

**18 septiembre 2020**

[CORRECCIÓN de errores de la Orden 1885/2020, de 15 de septiembre, de la Consejería de Educación y Juventud, por la que se modifican determinados requisitos para la concesión de becas, así como el cómputo de los miembros de la unidad familiar establecidos en la Orden 2257/2018, de 11 de julio, de la Consejería de Educación e Investigación, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo y la iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil.](https://www.bocm.es/boletin/CM_Orden_BOCM/2020/09/18/BOCM-20200918-19.PDF)

**23 diciembre 2020**

[Orden 3359/2020, de 21 de diciembre, del Consejero de Educación y Juventud, por la que se resuelve la convocatoria correspondiente al curso 2020-2021 de las becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo y la Iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil](http://www.bocm.es/boletin/CM_Orden_BOCM/2020/12/23/BOCM-20201223-36.PDF)  
> Transcurrido el período de subsanación, la Comisión de Valoración acordó proponer la concesión de beca a todas aquellas solicitudes que han reunido los requisitos exigidos en la convocatoria y tienen una **renta per cápita igual o inferior a 16.386,35 euros**, al no existir crédito presupuestario disponible para cubrir la totalidad de las ayudas solicitadas, y otorgar la condición de no beneficiario a todas aquellas solicitudes presentadas con una renta per cápita superior a dicha renta de corte.

### Convocatoria 2021-2022

No hay cambios en las bases 

**28 mayo 2021**

[Orden 1503/2021 del Consejero de Educación y Juventud por la que se aprueba el plan estratégico de subvenciones relativo a la concesión de becas para el estudio de programas de segunda oportunidad.](https://www.comunidad.madrid/transparencia/sites/default/files/open-data/downloads/plan_estrategico_becas_segunda_oportunidad_2021-22.pdf)

**12 julio 2021**

[Orden 2061/2021 del Consejero de Educación Universidades, Ciencia y Portavoz del Gobierno, por la que se aprueba la convocatoria de becas para el estudio de Programas de Segunda Oportunidad, correspondiente al curso 2021-2022.](https://tramita.comunidad.madrid/medias/orden-convocatoria-seop-2021-202224738625pdf/download)

**3 septiembre 2021**

[EXTRACTO de la Orden 2061/2021, de 12 de julio, del Consejero de Educación, Universidades, Ciencia y Portavoz del Gobierno, por la que se aprueba la convocatoria de becas para el estudio de Programas de Segunda Oportunidad, correspondiente al curso 2021-2022.](https://www.bocm.es/boletin/CM_Orden_BOCM/2021/09/03/BOCM-20210903-29.PDF)

**23 diciembre 2021**

[ORDEN 3726/2021, de 20 de diciembre, del Consejero de Educación, Universidades, Ciencia y Portavoz del Gobierno por la que se resuelve la convocatoria correspondiente al curso 2021-2022 de las becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo y la Iniciativa de Empleo Juvenil, a través del Programa Operativo de Empleo Juvenil.](https://www.bocm.es/boletin/CM_Orden_BOCM/2021/12/23/BOCM-20211223-20.PDF)  
> Transcurrido el período de subsanación, la Comisión de Valoración acordó proponer la concesión de beca a todas aquellas solicitudes que han reunido los requisitos exigidos en la convocatoria y tienen una **renta per cápita igual o inferior a 11.566,83 euros** al no existir crédito presupuestario disponible para cubrir la totalidad de las ayudas solicitadas, y otorgar la condición de no beneficiario a todas aquellas solicitudes presentadas con una renta per cápita superior a dicha renta de corte.

### [Becas para el estudio de Programas de Segunda Oportunidad (2022-2023)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-segunda-oportunidad)

Enlaza con los cheques solo para privados en Bachillerto, FPGM y FPGS.

**13 junio 2022**

[ORDEN 1643/2022 DEL CONSEJERO DE EDUCACIÓN, UNIVERSIDADES, CIENCIA Y PORTAVOZ DEL GOBIERNO POR LA QUE SE APRUEBA EL PLAN ESTRATÉGICO DE SUBVENCIONES PARA EL ESTUDIO DE PROGRAMAS DE SEGUNDA OPORTUNIDAD CORRESPONDIENTES AL CURSO 2022-2023.](https://www.comunidad.madrid/transparencia/sites/default/files/open-data/downloads/plan_estrategico_becas_segunda_oportunidad_2022-23.pdf)

**23 junio 2022**

[ORDEN 1643/2022, de 13 de junio, del Consejero de Educación, Universidades, Ciencia y Portavoz del Gobierno, por la que se aprueba el Plan Estratégico de Subvenciones para el estudio de programas de segunda oportunidad correspondientes al curso 2022-2023.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/06/23/BOCM-20220623-21.PDF)

**5 agosto 2022**

[Orden 2130/2022, de 29 de julio, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se modifican determinados requisitos establecidos en la Orden 2257/2018, de 11 de julio, de la Consejería de Educación y Juventud, por la que se aprueban bases reguladoras para la concesión de becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo plus a través del Programa FSE+ 2021/2027 de la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/08/05/BOCM-20220805-9.PDF)

**14 septiembre 2022**

[Orden 2610/2022 del Vicepresidente, Consejero de Educación y Universidades, por la que se aprueba la convocatoria de becas para el estudio de Programas de Segunda Oportunidad, correspondiente al curso 2022-2023.](https://www.comunidad.madrid/sites/default/files/doc/educacion/orden_2610-2022_convocatoria_seop_22-23.pdf)

[EXTRACTO de la Orden 2610/2022, de 8 de septiembre, del Vicepresidente, Consejero de Educación y Universidades, por la que se aprueba la convocatoria de becas de Segunda Oportunidad correspondiente al curso 2022-2023.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/09/14/BOCM-20220914-19.PDF)

**27 diciembre 2022**

[ORDEN 3922/2022, de 22 de diciembre, del Vicepresidente, Consejero de Educación y Universidades, por la que se resuelve la convocatoria correspondiente al curso 2022-2023 de las becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo Plus (FSE+).](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/12/27/BOCM-20221227-19.PDF)

> Transcurrido el período de subsanación, la Comisión de Valoración acordó proponer la concesión de beca a todas aquellas solicitudes que han reunido los requisitos exigidos en la convocatoria.

No ha habido renta de corte como otros años.

### [Becas para el estudio de Programas de Segunda Oportunidad (2023 - 2024)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-segunda-oportunidad-23-24)

[Orden 1945/2023, de 2 de junio, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se aprueban bases reguladoras para la concesión de becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Programa FSE+ 2021/2027 de la Comunidad de Madrid](https://bocm.es/boletin/CM_Orden_BOCM/2023/06/14/BOCM-20230614-24.PDF)

[Orden 2439/2023 del Consejero de Educación, Ciencia y Universidades, por la que se aprueba la convocatoria de becas para el estudio de Programas de Segunda Oportunidad curso 2023/2024.](https://www.comunidad.madrid/sites/default/files/doc/educacion/texto_convocatoria_23-24.pdf)

### Búsqueda de transparencia en desglose por titularidad becas 2018-2019

**4 septiembre 2019**

Planteo una consulta vía portal de transparencia. Me comentan que pocos alumnos habían conseguido "beca" en la pública, y que el límite de renta es muy alto, y creo que es información que se puede pedir.

---

Solicito:

- Cuantía e importe de Becas para el estudio de Programas de Segunda Oportunidad concedidas en el curso 2018-2019 desglosado según destinatarios de artículo 3 de orden 2257/2018, y desglosado, en los casos en los que aplique, según centros públicos, privados con concierto y privados sin concierto.  
- Límite de renta que haya resultado aplicable según los criterios de baremación para el curso 2018-2019, según artículo 13 de orden 2257/2018, y desglosado, en los casos en los que aplique, según centros públicos, privados con concierto y privados sin concierto.

---

Su solicitud ha sido registrada con la referencia 49/346323.9/19

**13 septiembre 2019**

Se me notifica inicio con expediente : 09-OPEN-00217.4/2019

**23 octubre 2019**

Tras reclamar a CTBG, le asigna RT 0690/2019.

**28 octubre 2019**

Recibo resolución, fuera de plazo, inadmitiendo

https://drive.google.com/open?id=1MKCiVzZtzpb8qbpFJnQmpXUx6y9hnBlC

Envío la información recibida a CTBG, indicando

---

Sobre lo que se indica, denegando por 18.a y 18.c, realizo la siguiente alegación:

- No considero que tenga sentido alegar, simultáneametne, 18.a (en curso de elaboración) y 18.c (requiere reelaboración)

- La baremación es algo que indica la convocatoria, por lo que objetivamente considero que no requiere reelaboración si efectivamente se ha baremado, ni puede estar en curso dado que son de curso 2018-2019 ya finalizado. 

- Las cuantías son el resultado de la convocatoria, por lo que objetivamente considero que no requiere reelaboración dado que se han entregado, ni puede estar en curso dado que son de curso 2018-2019 ya finalizado.

---

**30 enero 2020**

Recibo resolución estimatoria CTBG

https://drive.google.com/open?id=1jc3bi_t5rLEkWZVGaiOunOMwU-1KuIBl

**25 febrero 2020**
Recibo ejecución resolución de Madrid lo comparto

https://drive.google.com/file/d/10Ab1_vpoEHBswBKqUp5C5411cMAALVDu/view

https://drive.google.com/open?id=125yFtcljKUIB1mbpEhdT-rFxVUm5XqvuTz6WDU7XPOE

Lo comparto como imagen

[twitter FiQuiPedia/status/1232292786759774209](https://twitter.com/FiQuiPedia/status/1232292786759774209)

Datos, muy reveladores sobre a qué llama becas la consejería de @eossoriocrespo @IdiazAyuso, que no ha dado hasta no ir a @ConsejoTBG. 
Sin usar decimales, el 100% del dinero va a privados, con o sin concierto.

![](https://pbs.twimg.com/media/ERn7uj4WkAIZNXJ?format=png)

**5 marzo 2020**

[El gobierno de Ayuso recorta en 700 euros la ayuda por alumno que vuelve al sistema educativo](https://cadenaser.com/emisora/2020/03/05/radio_madrid/1583394450_463626.html)  
Las llamadas becas de segunda oportunidad han pasado de 3.500 euros por alumno al año a un máximo de 2.800 Apenas el 0,3% de estas ayudas financia a los estudiantes de centros públicos

### Búsqueda de transparencia en desglose por titularidad becas 2019-2020 y 2020-2021

Tras salir la resolución de beneficiarios curso 2020-21, me planteo pedir desglose beneficiarios por titularidad, similar a lo conseguido con "becas" bachillerato.

**23 diciembre 2020**

Planteo solicitud similar 4 septiembre 2019, y similar a becas bachillerato

---

Solicito:

1. Copia en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) o enlace al listado de Becas de segunda oportunidad concedidas en cursos 2019-2020 y 2020-2021 desglosado según destinatarios de artículo 3 de orden 2257/2018, que incluya desglose, en los casos en los que aplique, según centros públicos, privados con concierto y privados sin concierto.

Existe una consulta individual de beneficiarios https://www.comunidad.madrid/servicios/educacion/becas-estudio-programas-segunda-oportunidad y entiendo que exista protección de datos y puede ser necesaria cierta anonimización, pero mi solicitud se basa en:  
a. Según artículo 8.1.c de Ley 19/2013 y artículo 25.1.c Ley 10/2019 deben ser públicos datos incluyendo los beneficiarios.  
b. Los listados de beneficiarios existen según indica la orden 3359/2020: punto primero, los solicitantes figuran en el anexo I “Beneficiarios” de la Orden, y punto cuarto, las listas podrán consultarse en la página web institucional de la Comunidad de Madrid y complementariamente serán expuestas en las Direcciones de Área Territorial, así como en el Punto de Información y Atención al Ciudadano de la Consejería de Educación y Juventud.  
c. Orden 2257/2018 en su artículo 13.4 indica que cuando se acredite la condición de víctima de violencia de género el nombre del centro de matriculación del alumno no figurará en las listas, lo que entiende como que sí se publica nombre pero se evita asociar a centro omitiendo el centro.  
d. Se facilitaron datos para para 2017-2018 en Exp.: 09-OPEN-00216.3/2019

2. Límite de renta que haya resultado aplicable según los criterios de baremación para el curso 2019-2020 y 2020-2021, según artículo 13 de orden 2257/2018, y desglosado, en los casos en los que aplique, según centros públicos, privados con concierto y privados sin concierto.

---  

59/035066.9/20

**27 enero 2021**

Recibo resolución. No facilita listados

https://drive.google.com/file/d/1OFnP6u-dQOoNu4BBMZ2Q7d1DXOs3h26_/view?usp=sharing

https://drive.google.com/drive/folders/1MIYOI13UHIU-alBSOOCd9KAndt5IxpfV

![](https://pbs.twimg.com/media/EsvXvT9W4AUykub?format=jpg)

Similar a lo que hago con respuesta de Becas Bachillerato, en lugar de ir a CTBG, planteo una nueva solicitud. Me interesa tema RGPD asociado a este tipo de ayudas.

---

1. Copia o enlace de anexo I “Beneficiarios” citado en punto primero de la Orden 3359/2020, orden que en su punto cuarto indica "Los listados de beneficiarios, no beneficiarios y excluidos se hacen públicos de forma simultánea a esta Orden en la página web institucional de la Comunidad de Madrid www.comunidad.madrid. Complementariamente serán expuestos en las Direcciones de Área Territorial, así como en la Oficina de Información de la Consejería de Educación y Juventud."  
Existe una consulta individual de beneficiarios https://www.comunidad.madrid/servicios/educacion/becas-estudio-programas-segunda-oportunidad y entiendo que exista protección de datos y puede ser necesaria cierta anonimización, pero mi solicitud se basa en artículo 8.1.c de Ley 19/2013 y artículo 25.1.c Ley 10/2019 deben ser públicos datos incluyendo los beneficiarios.  
Los beneficiaros en becas de segunda oportunidad anteriores son públicos con nombre y apellidos pero NIF anonimizado como se pueden ver por ejemplo en  
https://www.pap.hacienda.gob.es/bdnstrans/GE/es/concesiones/convocatoria/414594  

2. Copia o enlace a datos de las Becas de Segunda Oportunidad concedidas en el curso 2020-2021 desglosado según códigos de centros destinatarios, con número de becas e importe por beca y código de centro según destinatarios de artículo 3 de orden 2257/2018. Se facilitaron datos para 2017-2018 en Exp.: 09-OPEN-00216.3/2019

--- 

09/104194.9/21

**17 febrero 2021**

Recibo resolución parcialmente estimatoria. Facilitan códigos pero no listados

Resolución https://drive.google.com/file/d/1UkhEcVrZnt5NKm_U-kBeY_RP70Ba_knV/view?usp=sharing

Datos por centro https://drive.google.com/file/d/1Uk4Xs_xdrBo4lFs75lukM7X5oPiNoLlZ/view?usp=sharing

> 28009525 "SANTA GEMA GALGANI" CENTRO PRIVADO DE EDUCACIÓN INFANTIL, PRIMARIA Y SECUNDARIA 806720 €  
> 28075728     "FP MADRID TORREJON" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA  669625 €  
> 28076976     "CAMPUS FP EMPRENDE" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA  372400 €  
> 28049912     "CEV CENTRO DE ESTUDIOS DEL VIDEO" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA 364000 €  
> 28046911 "CES, ESCUELA SUPERIOR DE IMAGEN Y SONIDO" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA 355600 €  
> 28078407     "INSTITUTO SUPERIOR DE FP CLAUDIO GALENO ARGANDA" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA     311375 €  
> 28078249     "INNOVACION EN FORMACION PROFESIONAL" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA   288400 €  
> 28079679     "INNOVACION EN FORMACION PROFESIONAL II" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA 266000 €  
> 28080177     "MEDAC FUENLABRADA" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA 247660,21 €  
> 28078213     "CAMPUS FP ATOCHA" CENTRO PRIVADO DE EDUCACIÓN SECUNDARIA 224000 €  
> 28076873     "FORMACION PROFESIONAL CORREDOR DEL HENARES" CENTRO PRIVADO DE FORMACIÓN PROFESIONAL ESPECÍFICA  200990 €  

Los 10 primeros centros privados suman 3,9 M€, el 39% del total de "becas"

![](https://pbs.twimg.com/media/Euw7vvxWQAA0lZd?format=png)

Aparece un código de centro "G83169524", que es un CIF, asociado a https://www.pinardi.com/ 

**22 febrero 2021**

Planteo reclamación a CTBG. Creo que es interesante por el tema de RGPD vs transparencia.

---

La existencia del anexo I de beneficiarios lo confirma la resolución y se indica en BOCM, luego no se trata de un tema de elaboración, sino prevalencia entre RGPD y transparencia en datos de beneficiarios de becas. Considero que esta reclamación puede firmar un criterio al respecto.

Considero que existen dos opciones excluyentes:

1. Existe prevalencia de protección de datos personales frente a transparencia, por lo que el anexo I de beneficiarios no puede ser consultado.

2. Existe prevalencia de transparencia frente a protección de datos personales, por lo que el anexo I de beneficiarios no puede ser consultado.

BOCM indica que ese anexo es expuesto físicamente "Complementariamente serán expuestos en las Direcciones de Área Territorial, así como en la Oficina de Información de la Consejería de Educación y Juventud.". La resolución habla de "[consultables] por los propios interesados", cosa que no indica BOCM.

El hecho de que sea consultable (por texto BOCM y en web hacienda como indico en mi solicitud) sugiere la opción 2, prevalece la transparencia, mientras que la resolución, de la misma consejería que hace que sea consultable, sugiere opción 1 al indicar que prevalece la protección de datos personales.

Al considerar que ambas opciones son incompatibles, o bien la resolución es incorrecta, y se debe facilitar anexo, o bien el hecho de que los datos personales hayan sido/sean consultables es incorrecto, y sería una incidencia ante AEPD.

---

**6 julio 2021**

Recibo resolución desestimatoria RT0137/2021

A la vista de los razonamientos expuestos, en la ponderación exigida por el artículo 15.3 de la LTAIBG entre el interés público en la divulgación de la información y los derechos de los afectados, a juicio de este Consejo, debe prevalecer en el presente caso el derecho fundamental a la protección de datos de carácter personal sobre el interés público en el acceso a la información, teniendo en cuenta la naturaleza de los datos, el grado de afectación de los derechos de la esfera personal que su divulgación comporta y el hecho de que las exigencias de publicidad y transparencia ya se han visto satisfechas con anterioridad por medio de la publicación temporal que ha realizado la Comunidad de Madrid en los términos establecidos en la convocatoria de estas becas.

### Búsqueda de transparencia en desglose por titularidad becas  2022-2023

**27 diciembre 2022**

Planteo solicitud similar diciembre 2020

---

1. Copia en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) o enlace al listado de Becas de segunda oportunidad concedidas en cursos 2021-2022 y 2022-2023 desglosado según destinatarios de artículo 3 de orden 2257/2018, que incluya desglose, en los casos en los que aplique, según centros públicos, privados con concierto y privados sin concierto.

Se facilitaron datos para para 2017-2018 en 09-OPEN-00216.3/2019 

2. Copia o enlace a datos de las Becas de Segunda Oportunidad concedidas en el curso 2021-2022 y 2022-2023 desglosado según códigos de centros destinatarios, con número de becas e importe por beca y código de centro según destinatarios de artículo 3 de orden 2257/2018. 

Se facilitaron datos para 2017-2018 en 09-OPEN-00216.3/2019  y para 2020-201 en 09-OPEN-00010.4/2021

3. Valor máximo de renta per cápita a la que se ha concedido en 2022-2023 (resolución 2021 indicó un límite de renta 11.566,83 €), dado que en 2022-2023 se ha concedido a todas aquellas solicitudes que han reunido los requisitos exigidos y en las bases y convocatoria no hay límite de renta.

--- 

59/675709.9/22

Recibo resolución y datos

https://drive.google.com/file/d/1bTh3i4mx7T_Tdn91uC5EVUrIA5CpBiTX/view?usp=drive_link

Datos

https://docs.google.com/spreadsheets/d/1bOZMxeAA-NyDCfE_Qi6hkrPvCTSih5BE/edit?usp=drive_link&ouid=113978260833777516740&rtpof=true&sd=true

### Búsqueda de transparencia en desglose por titularidad becas  2023-2024

**23 diciembre 2023**

La idea es poner solicitud similar a 13 noviembre 2023 para Bachillerato 2023-2024  (asociada 15 noviembre 2022), que sí dieron citando RDA164/2023 asociada a datos de curso 2022-2023.

---

1. Copia en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) o enlace al listado de Becas de segunda oportunidad concedidas en curso 2023-2024 desglosado según destinatarios de artículo 3 de orden 2439/2023, que incluya desglose, en los casos en los que aplique, según centros públicos, privados con concierto y privados sin concierto.

Se facilitaron datos para para 2017-2018 en 09-OPEN-00216.3/2019 

2. Copia o enlace a datos de las Becas de Segunda Oportunidad concedidas en el curso 2022-2023 desglosado según códigos de centros destinatarios, con número de becas e importe por beca y código de centro según destinatarios de artículo 3 de orden 2439/2023. 

Se facilitaron datos para 2017-2018 en 09-OPEN-00216.3/2019, para 2020-2021 en 09-OPEN-00010.4/2021, en 2021-2022 en 09-OPEN-00234.6/2022

3. Valor máximo de renta per cápita a la que se ha concedido en 2023-20234 (resolución 2021 indicó un límite de renta 11.566,83 €), dado que en 2023-2024 se ha concedido a todas aquellas solicitudes que han reunido los requisitos exigidos y en las bases y convocatoria no hay límite de renta.

La resolución definitiva se ha publicado y ha finalizado el plazo de presentación de recursos

ORDEN 4559/2023, de 17 de noviembre, del Consejero de Educación, Ciencia
y Universidades, por la que se resuelve la convocatoria correspondiente al curso 2022-2023 de las becas para el estudio de Programas de Segunda Oportunidad, cofinanciadas por el Fondo Social Europeo Plus (FSE+)

(por errata se indica 2022-2023 en título orden, pero internamente hace referencia a 2023-2024)

https://www.bocm.es/boletin/CM_Orden_BOCM/2023/11/22/BOCM-20231122-16.PDF

--- 

43/629335.9/23

**12 enero 2024**

Recibo resolución inadmitiendo

[2024-01-12 Resolución Becas Segunda Oportunidad.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/BecasSegundaOportunidad/2024-01-12-ResolucionBecasSegundaOportunidad.pdf)

**24 enero 2024**

Planteo reclamación

Inadmiten usando, aunque sin citar explícitamente  artículo 18.a Ley 19/2023 , pero esta asociada a becas ya resueltas: ha finalizado plazo de alegaciones, el abono de esas becas ya se está gestionado con unos centros e importes por centro ya conocidos. No se indica en qué consiste "la finalización de todos los procedimientos" que suponga no poder facilitar ya la información solicitada, y esta el precedente de RDA164/2023.  
Se comenta que "se procederá a la elaboración de los informes (como en el curso 2023/2024 (sic, se asume que quieren decir 2022-2023)) y se procederá a hacer la publicación correspondiente de los mismos", pero la información publicada agosto 2023 asociada a ejecución RDA164/2023 no responde en absoluto en mi solicitud.  
Publicaron para FPGS, Bachillerato y FPGM, pero no para segunda oportunidad en  
https://www.comunidad.madrid/servicios/educacion/informe-beneficiarios-becas-bachillerato-2022-2023  
https://www.comunidad.madrid/servicios/educacion/informe-beneficiarios-becas-formacion-profesional-grado-superior-2022-2023  
https://www.comunidad.madrid/servicios/educacion/informe-beneficiarios-becas-formacion-profesional-grado-medio-2022-2023  
donde se publicaron informes que indican motivos de inadmisión y rentas per cápita, pero se puede comprobar que la información que indican que publicarán no es la asociada a la solicitud inadmitida.  
Se puede comprobar que día 12 están dando la misma respuesta que día 11 a la solicitud 09-OPEN-00204.6/2023 asociada a datos FPGS sin cambiar el texto, y sin que la respuesta dada tenga sentido para los datos solicitados de becas de segunda oportunidad.  

**15 marzo 2024**

Recibo desde consejo de transparencia [documento de alegaciones](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/BecasSegundaOportunidad/2024-03-04-1_ALEGACIONES_RDACTPCM28_24_FDO.pdf)

> De acuerdo con lo establecido en los artículos 43 y 44 del Reglamento de Organización y Funcionamiento de este Consejo, le damos traslado de la información recibida por parte de la administración en relación a la reclamación efectuada por usted cuyo número de expediente es RDACTPCM023/2024.  
En caso de que desee efectuar alegaciones a tenor de la información recibida, puede enviarlas a esta misma dirección de correo en el plazo máximo de 10 días a partir de la recepción del presente correo. 

Respondo:

Reitero lo indicado en mi reclamación: la información solicitada esta asociada a becas ya resueltas, ha finalizado plazo de alegaciones, el abono de esas becas ya se está gestionado con unos centros e importes por centro ya conocidos. No se indica en qué consiste "la finalización de todos los procedimientos" que suponga no poder facilitar ya la información solicitada, y está el precedente de RDA164/2023.  

En las alegaciones indican "tal y como se indicaba en la resolución de la petición efectuada" pero no corrigen que en su resolución también indicaban de que se publicaría "como en el curso 2023/2024 (sic, se asume que quieren decir 2022-2023)" cuando no publicaron nada para becas de segunda oportunidad
 


