+++
author = "Enrique García"
title = "Grabar conversación como prueba"
date = "2023-12-20"
tags = [
    "normativa", "laboral", "jurisprudencia", "post migrado desde blogger"
]
toc = true

+++

Revisado 18 septiembre 2024

## Resumen

Se puede grabar sin consentimiento una conversación en la que seamos parte, pero no difundir sin consentimiento. Es válido el uso de una grabación sin consentimiento de una conversación en la que seamos parte como prueba en un juicio en los defendamos nuestros intereses.

## Detalle

Este post breve lo escribo en 2023 tras este comentario

[twitter javierfpanadero/status/1737014844287537209](https://twitter.com/javierfpanadero/status/1737014844287537209)

Recordatorio:vv
Es legal grabar, SIN AVISAR, una conversación en el trabajo si participas en ella y luego la usas para denunciar en la empresa o juzgado (no para divulgar).vv
Muy importante para casos de acoso, abuso, etc.  
Q nos maticen/corrijan @Laboralista_DCT @StarlessCrimson  

[twitter FiQuiPedia/status/1737056967930081507](https://twitter.com/FiQuiPedia/status/1737056967930081507)

Más que matizar se puede ilustrar con ejemplos de que para los tribunales esas grabaciones son pruebas válidas.

El ejemplo es [ECLI:ES:TSJMU:2014:1402](http://www.poderjudicial.es/search/AN/openDocument/0bef99fff8da9426/20141128) 

> Pues si tienes colegios al lado de tu casa donde no se paga. El problema es tuyo. Cógela y llévatela tú, no la dejes aquí

![](https://pbs.twimg.com/media/GB2uJ8LWEAAVTMl?format=png)  

> CUARTO .- Despejadas estas primeras cuestiones, por su trascendencia en el resultado del recurso, conviene comenzar resolviendo la cuestión relativa a la validez probatoria que ha de darse a las grabaciones, efectuadas por el padre de la menor, de las conversaciones que sostuvo con Doña Adelina (Administradora del Centro y Vicepresidenta de Cescu) y con la hermanada de esta Doña Valentina (Administradora de "Gestión Integral" y Tesorera de "Cescu"), sin su conocimiento y sin su consentimiento.  
A este respecto, resulta meridianamente clara la doctrina sentada por la Sala de lo Penal del Tribunal Supremo en su Sentencia nº 45/2014, de 7 de febrero, dictada en el Recurso nº 1077/2013 , que por su evidente interés se transcribe, en la que declara:  
"la STS 298/2013, 13 de marzo -citada por el Fiscal en su informe de impugnación-, glosa los precedentes de la jurisprudencia constitucional y de esta Sala, que sirven para descartar la tesis de la defensa. Se alude así a la STC 114/1984, de 29 de noviembre , resolución emblemática por cuanto de ella emanó todo el discurso y desarrollo de la teoría de la prueba ilícita en nuestro ordenamiento: "... el actor ha afirmado en su demanda y en sus alegaciones que el hecho ilícito que da fundamento a su queja constitucional fue la inicial violación del secreto de sus comunicaciones por su interlocutor, al proceder éste a grabar la conversación con él mantenida sin su conocimiento. Esta conculcación de su derecho la argumenta el recurrente aduciendo que «el artículo 18.3 no sólo protege la intimidad de la conversación prohibiendo que un tercero emplee aparatos para interceptarla..., sino que la intimidad de la conversación telefónica, como derecho fundamental, puede ser violada mediante la colocación por uno de los comunicantes de una grabadora, sin consentimiento de la otra parte... ». La supuesta infracción se agravaría, en fin, cuando lo así aprehendido se comunicara a terceros y se presentara como prueba ante un Tribunal.  
(...) Con estas advertencias, es necesario determinar si, efectivamente, la grabación de la conversación, en la que fuera parte el actor, constituyó, como se pretende, una infracción del derecho al secreto de las comunicaciones. La tesis del actor no puede compartirse. Su razonamiento descansa en una errónea interpretación del contenido normativo del art. 18.3 de la Constitución . Y en un equivocado entendimiento de la relación que media entre este precepto y el recogido en el núm. 1 del mismo artículo.  
El derecho al «secreto de las comunicaciones... salvo resolución judicial» no puede oponerse, sin quebrar su sentido constitucional, frente a quien tomó parte en la comunicación misma así protegida. Rectamente entendido, el derecho fundamental consagra la libertad de las comunicaciones, implícitamente, y, de modo expreso, su secreto, estableciendo en este último sentido la interdicción de la interceptación o del  conocimiento antijurídico de las comunicaciones ajenas. El bien constitucionalmente protegido es así -a través de la imposición a todos del «secreto»- la libertad de las comunicaciones, siendo cierto que el derecho puede conculcarse tanto por la interceptación en sentido estricto (que suponga aprehensión física del soporte del mensaje -con conocimiento o no del mismo- o captación, de otra forma, del proceso de comunicación) como por el simple conocimiento antijurídico de lo comunicado (apertura de la correspondencia ajena guardada por su destinatario, por ejemplo).  
(...) Sea cual sea el ámbito objetivo del concepto de «comunicación», la norma constitucional se dirige inequívocamente a garantizar su impenetrabilidad por terceros (públicos o privados: el derecho posee eficacia erga omnes) ajenos a la comunicación misma. La presencia de un elemento ajeno a aquéllos entre los que media el proceso de comunicación, es indispensable para configurar el ilícito constitucional aquí perfilado.  
No hay «secreto» para aquél a quien la comunicación se dirige, ni implica contravención de lo dispuesto en el art. 18.3 de la Constitución la retención, por cualquier medio, del contenido del mensaje. Dicha retención (la grabación, en el presente caso) podrá ser, en muchos casos, el presupuesto fáctico para la comunicación a terceros, pero ni aun considerando el problema desde este punto de vista puede apreciarse la conducta del interlocutor como preparatoria del ilícito constitucional, que es el quebrantamiento del secreto de las comunicaciones. Ocurre, en efecto, que el concepto de «secreto» en el art. 18.3 tiene un carácter «formal», en el sentido de que se predica de lo comunicado, sea cual sea su contenido y pertenezca o no el objeto de la comunicación misma al ámbito de lo personal, lo íntimo o lo reservado. Esta condición formal del secreto de las comunicaciones (la presunción iuris et de iure de que lo comunicado es «secreto», en un sentido sustancial) ilumina sobre la identidad del sujeto genérico sobre el que pesa el deber impuesto por la norma constitucional. Y es que tal imposición absoluta e indiferenciada del «secreto» no puede valer, siempre y en todo caso, para los comunicantes, de modo que pudieran considerarse actos previos a su contravención (previos al quebrantamiento de dicho secreto) los encaminados a la retención del mensaje. Sobre los comunicantes no pesa tal deber, sino, en todo caso, y ya en virtud de norma distinta a la recogida en el art. 18.3 de la Constitución , un posible «deber de reserva» que -de existir- tendría un contenido estrictamente material, en razón del cual fuese el contenido mismo de lo comunicado (un deber que derivaría, así del derecho a la intimidad reconocido en el art. 18.1 de la Norma fundamental).  
Quien entrega a otro la carta recibida o quien emplea durante su conversación telefónica un aparato amplificador de la voz que permite captar aquella conversación a otras personas presentes no está violando el secreto de las comunicaciones, sin perjuicio de que estas mismas conductas, en el caso de que lo así transmitido a otros entrase en la esfera «íntima» del interlocutor, pudiesen constituir atentados al derecho garantizado en el artículo 18.1 de la Constitución . Otro tanto cabe decir, en el presente caso, respecto de la grabación por uno de los interlocutores de la conversación telefónica. Este acto no conculca secreto alguno impuesto por el art. 18.3 y tan sólo, acaso, podría concebirse como conducta preparatoria para la ulterior difusión de lo grabado. Por lo que a esta última dimensión del comportamiento considerado se refiere, es también claro que la contravención constitucional sólo podría entenderse materializada por el hecho mismo de la difusión ( art. 18.1 de la Constitución ). Quien graba una conversación de otros atenta, independientemente de toda otra consideración, al derecho reconocido en el art. 18.3 de la Constitución ; por el contrario, quien graba una conversación con otro no incurre, por este solo hecho, en conducta contraria al precepto constitucional citado. Si se impusiera un genérico deber de secreto a cada uno de los interlocutores o de los corresponsables ex art. 18.3, se terminaría vaciando de sentido, en buena parte de su alcance normativo, a la protección de la esfera íntima personal ex art. 18.1, garantía ésta que, «a contrario», no universaliza el deber de secreto, permitiendo reconocerlo sólo al objeto de preservar dicha intimidad (dimensión material del secreto, según se dijo). Los resultados prácticos a que podría llevar tal imposición indiscriminada de una obligación de silencio al interlocutor son, como se comprende, del todo irrazonables y contradictorios, en definitiva, con la misma posibilidad de los procesos de libre comunicación humana. Si a esta solución se debe llegar examinando nuestra Norma Fundamental, otro tanto cabe decir a propósito de las disposiciones ordinarias que garantizan, desarrollando aquélla, el derecho a la intimidad y a la integridad y libertad de las comunicaciones.  
Como conclusión, pues, debe afirmarse que no constituye contravención alguna del secreto de las comunicaciones la conducta del interlocutor en la conversación que graba ésta (que graba también, por lo tanto, sus propias manifestaciones personales, como advierte el Ministerio Fiscal en su escrito de alegaciones)".  
Por su parte en la STC 56/2003, 24 de marzo , insiste en ese entendimiento del contenido material del derecho a la inviolabilidad de las comunicaciones, excluyendo toda lesión de relevancia constitucional derivada de la grabación y ulterior utilización en juicio de lo grabado por uno de los interlocutores.  
La referida doctrina -recuerda la ya citada STS 298/2013, 13 de marzo-, ha sido asumida por esta Sala Segunda en un nutrido grupo de sentencias. Relevante botón de muestra son las SSTS 2008/2006, de 2 de febrero ó 682/2011 de 24 de junio : "... se alega que la grabación de la conversación mantenida entre víctima y acusado ha sido obtenida con vulneración de derechos fundamentales dado que uno de los interlocutores desconocía que estaba siendo grabada, por lo que no tuvo opción de impedir dicha grabación, proteger su intimidad y hacer valer su derecho al secreto de las comunicaciones. (...) La jurisprudencia ha señalado que la grabación que un particular haga de sus propias conversaciones, telefónicas o de otra índole, no suponen el atentado al secreto de las comunicaciones ( STS 20-2-2006 ; STS 28-10-2009, nº 1051/2009 ). E igualmente ha precisado la STS 25-5-2004, nº 684/2004 que las cintas grabadas no infringen ningún derecho, en particular el art. 18-3 CE debiendo distinguir entre grabar una conversación de otros y grabar una conversación con otros. Pues no constituye violación de ningún secreto la grabación de un mensaje emitido por otro cuando uno de los comunicantes quiere que se perpetúe.
A la luz de la citada Jurisprudencia ninguna queja cabe sobre la legitimidad constitucional de la utilización como prueba de las grabaciones, efectuadas por el padre de la menor, de las conversaciones que sostuvo con Doña Adelina (Administradora del Centro y Vicepresidenta de Cescu) y con la hermana de esta Doña Valentina (Administradora de "Gestión Integral" y Tesorera de "Cescu"), por mucho que estas fueran realizadas sin su conocimiento y sin su consentimiento.

Se cita [STC 56/2003](http://hj.tribunalconstitucional.es/es-ES/Resolucion/Show/4831)

Que a su vez cita [STC 114/1984](http://hj.tribunalconstitucional.es/es-ES/Resolucion/Show/367)

Que indica entre otras cosas   
> Que el recurrente reconozca o no la conversación no conculca ningún requisito procesal que afecte a la prueba.

[¿Es legal grabar con el móvil en audio o vídeo a otra persona sin su consentimiento para recabar pruebas? - xataka](https://www.xataka.com/legislacion-y-derechos/legal-grabar-movil-audio-video-a-otra-persona-su-consentimiento-para-recabar-pruebas)  

> Para tenerlo claro  
Así pues, si te planteas grabar algún tipo de conversación, debes tener en cuenta que:  
1. Puedes grabar, en voz o en vídeo, todas las conversaciones de las que formes parte.  
2. Estas grabaciones son legales.  
3. No tienes por qué avisar de que estás grabando la conversación.  
4. Lo que no se puede es revelar el contenido de estas conversaciones.  
5. Sin embargo, pueden servir como prueba en un juicio.  
6. No puedes grabar conversaciones, ni en voz ni en imagen, ajenas, en las que no estés participando.  
7. Si lo haces, puedes incurrir en algún tipo de delito.  

[¿Es delito compartir con otros conversaciones privadas mantenidas por WhatsApp, correo electrónico o redes sociales?](https://andreyferreiroabogados.com/2017/11/28/es-delito-compartir-con-otros-conversaciones-privadas-mantenidas-por-whatsapp-correo-electronico-o-redes-sociales/)



[Las grabaciones como medio de prueba: posibilidades y limitaciones - delajusticia.com El rincón jurídico de José R. Chaves](https://delajusticia.com/2024/09/16/las-grabaciones-como-medio-de-prueba-posibilidades-y-limitaciones/)  
