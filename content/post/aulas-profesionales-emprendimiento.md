+++
author = "Enrique García"
title = "Aulas Profesionales Emprendimiento"
date = "2024-08-20"
tags = [
    "educación", "privatización"
]
toc = true

description = "Aula Profesionales Emprendimiento"
thumbnail = "https://www.libreria.educacion.gob.es/media/mefp/images/edition-179206.jpg"
images = ["https://www.libreria.educacion.gob.es/media/mefp/images/edition-179206.jpg"]
+++

[Comentario]:<> (Imagen tomada de https://www.libreria.educacion.gob.es/libro/creando-aulas-de-emprendimiento-en-formacion-profesional-una-guia-para-empezar-a-emprender_171686/)

Revisado 20 agosto 2024

## Resumen

Asociado a las irregularidades en licitaciones de obras en centros en Madrid miro el tema de las Aulas Profesionales  Emprendimiento que suponen fondos extraordinarios.

### Información Madrid

El proyecto de Aulas de Emprendimiento surge en el curso 2016-2017

**3 julio 2021**  
[Ponemos en marcha 23 nuevas Aulas de Emprendimiento en Formación Profesional](https://www.comunidad.madrid/noticias/2021/07/03/ponemos-marcha-23-nuevas-aulas-emprendimiento-formacion-profesional)  
> La Comunidad de Madrid va a crear el próximo curso 2021-2022 un total de 23 nuevas Aulas de Emprendimiento en Formación Profesional (FP). Con esta ampliación, serán ya 72 los centros docentes públicos que cuenten con estos espacios.   

> El proyecto de Aulas de Emprendimiento surge en el curso 2016-2017 como iniciativa del IES Clara del Rey, al que posteriormente se sumaron otros centros de FP de la región. Asimismo, se engloba dentro de la estrategia para impulsar las habilidades emprendedoras, imprescindibles en el nuevo marco socioeconómico hacia el que se dirige la economía. Se trata de una actuación de carácter eminentemente práctico, encaminada a potenciar el emprendimiento y el autoempleo entre los estudiantes de Formación Profesional.

**2021**  
[Proyecto Orden 1378/2021 por la que se regulan las aulas profesionales de emprendimiento en los centros educativos que impartan enseñanzas de Formación Profesional en la Comunidad de Madrid.](https://www.comunidad.madrid/transparencia/orden-13782021-que-se-regulan-aulas-profesionales-emprendimiento-centros-educativos-que-impartan)  

[Orden 1378/2021 firmada](https://www.comunidad.madrid/transparencia/sites/default/files/orden_1378_aulas_profesionales_firm.pdf)

[Orden 1379/2021 BOCM](https://www.bocm.es/boletin/CM_Orden_BOCM/2021/06/01/BOCM-20210601-1.PDF)  

> Artículo 4  
>2. Los centros públicos interesados en implantar un APE deberán contar con la autorización de la dirección general competente en materia de centros públicos de formación profesional  
>3. Los centros privados concertados comunicarán a la dirección general competente en materia de enseñanza concertada, así como a la dirección de área territorial correspondiente para conocimiento del Servicio de Inspección Educativa, la implantación de un APE  


El [enlace de la guía de 2023-2024](https://site.educa.madrid.org/emprendimientofp.madrid//wp-content/uploads/emprendimientofp.madrid/2024/07/GuiaOperativaAPE_2425.pdf) que usé en julio 2024 deja de estar operativo en agosto 2024.  
En esa guía había apartado 7. Financiación y gastos elegibles Fondo Social Europeo que es el que cito en solicitud 8 julio 2024  

[Aulas de emprendimiento. Gasto cofinanciado por el MEFP y la Unión Europea-NextGenerationEU. Servicio FSE-SG de Centros de FP y RE](https://site.educa.madrid.org/emprendimientofp.madrid//wp-content/uploads/emprendimientofp.madrid/2024/07/2023-24_APEFondoSocialEuropeo.pdf)  
> En nuestro caso, esto supone un ingreso extraordinario al Centro de 5.000 € que deben ser empleados en este aula “inequívocamente”.  

[Guía operativa de las Aulas Profesionales de Emprendimiento (APE) Curso 2024-25](https://site.educa.madrid.org/emprendimientofp.madrid//wp-content/uploads/emprendimientofp.madrid/2024/07/GuiaOperativaAPE_2425.pdf)  
> 6. Financiación y gastos elegibles MRR  

### Información otras CCAAA

[Aulas de emprendimiento CyL](https://aulasdeemprendimientocyl.org/)

[Aulas de emprendimiento - juntadeandalucia.es](https://www.juntadeandalucia.es/educacion/portals/web/formacion-profesional-andaluza/docente/formacion-y-empresa/emprendimiento/aulas-emprendimiento)

[Aulas de emprendimiento - jccm.es](https://www.educa.jccm.es/es/fpclm/innovacion-formacion-profesional/aulas-profesionales-emprendimiento-castilla-mancha)  

[Convocatoria Aulas de Emprendimiento de Formación Profesional para centros educativos (código 3670) (SIA 2843780) - carm.es](https://sede.carm.es/web/pagina?IDCONTENIDO=3670&IDTIPO=240&RASTRO=c$m40288)  

[Creación de Aulas de Emprendimiento en Formación Profesional - gobiernodecanarias.org](https://www.gobiernodecanarias.org/educacion/web/programas_cofinanciados/nextgenerationeu/plan-fp-crecimiento-economico-social-empleabilidad/creacion_aulas_emprendimiento_fp/)  

[Programas de Aulas Profesionales de Emprendimiento - aragon.es](https://educa.aragon.es/-/fp/programas-y-proyectos/ape)  

### Información estatal

[Creando aulas de emprendimiento en Formación Profesional. Una guía para empezar a emprender. Isabel Díaz Picón - educacion.gob.es](https://www.libreria.educacion.gob.es/libro/creando-aulas-de-emprendimiento-en-formacion-profesional-una-guia-para-empezar-a-emprender_171686/)  

[Resolución de 21 de junio de 2023, de la Secretaría General de Formación Profesional, por la que se publica el Acuerdo de la Conferencia Sectorial de Educación de 7 de junio de 2023, por el que se aprueba la propuesta de distribución territorial y los criterios de reparto de los créditos gestionados por comunidades autónomas para el Plan de Modernización de la Formación Profesional, con cargo al presupuesto del Ministerio de Educación y Formación Profesional 2023 y en el marco del componente 20 "Plan Estratégico de Impulso de la Formación Profesional", del Plan de Recuperación, Transformación y Resiliencia.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2023-15429)  
Para Madrid se indica "Total MEFP 33.432.040,00"  

[Resolución de 11 de julio de 2022, de la Secretaría General de Formación Profesional, por la que se publica el Acuerdo de la Conferencia Sectorial de Educación de 23 de junio de 2022, por el que se aprueba la propuesta de distribución territorial y los criterios de reparto de los créditos gestionados por comunidades autónomas, en el ejercicio presupuestario 2022, para el Plan de Modernización de la Formación Profesional, con cargo al presupuesto del Ministerio de Educación y Formación Profesional y en el marco del componente 20 "Plan Estratégico de Impulso de la Formación Profesional", del Plan de Recuperación, Transformación y Resiliencia.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2022-11988)  
![](https://www.boe.es/datos/imagenes/disp/2022/172/11988_11890472_1.png)  
Madrid 53 millones de euros  

[Resolución de 7 de octubre de 2021, de la Secretaría General de Formación Profesional, por la que se publica el Acuerdo de la Conferencia Sectorial de Educación de 21 de julio de 2021, por el que se aprueba la propuesta de distribución territorial y los criterios de reparto de los créditos gestionados por Comunidades Autónomas en el marco del componente 20 "Plan estratégico de impulso a la Formación Profesional", del Plan de Recuperación, Transformación y Resiliencia, en el ejercicio presupuestario 2021.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-16952)  
![](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-16952)  
Madrid 29 millones de euros  

[Resolución de 16 de diciembre de 2020, de la Secretaría General de Formación Profesional, por la que se publica el Acuerdo del Consejo de Ministros de 15 de diciembre de 2020, por el que se distribuye el crédito destinado a las Comunidades Autónomas, para financiar el Plan de Formación Profesional para el crecimiento económico y social y la empleabilidad.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2020-17096)  
![](https://www.boe.es/datos/imagenes/disp/2020/338/17096_4965.png)  
Madrid 33 millones de euros  

### Listado de centros Madrid

**8 julio 2024**  

Planteo solicitud transparencia

---

Solicito copia o enlace a:  
- Listado de centros públicos que han implantado Aulas Profesionales de Emprendimiento (APE)  tras la autorización de artículo 4 de ORDEN 1378/2021  
- Listado de centros privados con concierto que han comunicado la implantación de APE según artículo 4 de ORDEN 1378/2021  
- Dotación económica inicial realizada para creación, y dotación de funcionamiento desglosada por año, de las APE anteriores, dotaciones que se citan en punto 7 de Guía operativa de las Aulas Profesionales de Emprendimiento (APE) Curso 2023 - 24  

---

03/880904.9/24 


**20 agosto 2024**  
Recibo respuesta con dos documentos

* [2024-07-18-Respuesta_DG_Concertadad-APE.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/AulasProfesionalesEmprendimiento/2024-07-18-Respuesta_DG_Concertadad-APE.pdf)  
* [2024-08-20-OPEN-08_Resolucion_de_acceso_parcial-APE.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/AulasProfesionalesEmprendimiento/2024-08-20-OPEN-08_Resolucion_de_acceso_parcial-APE.pdf)  

Para el listado de centros remiten a 

[EmprendimientoFP Madrid - "Centros FP"](https://site.educa.madrid.org/emprendimientofp.madrid/index.php/category/centros-fp/)  

Sobre asignación, se aportan enlaces BOE y se indica   

> cantidades para este tipo de aulas en los Repartos de 2020 (fondos no MRR), 2021, 2022 y 2023.  
Este reparto, canalizado a las Comunidades Autónomas por el Ministerio de Educación, Formación
Profesional y Deporte, se realiza mediante módulos de costes unitarios de 5.000 €/aula/año, recibiendo
cada aula esa financiación para funcionamiento finalista durante tres cursos.  


