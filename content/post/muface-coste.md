+++
author = "Enrique García"
title = "Muface: coste"
date = "2024-10-09"
tags = [
    "muface", "funcionarios", "coste", "servicio público"
]
toc = true

+++

Revisado 2 enero 2025

## Resumen

Este post amplía el post [MUFACE](../muface): aquí se intenta documentar de manera objetiva el coste de MUFACE comparándolo con el coste de la prestación sanitaria pública. Hacerlo implica hablar de servicios públicos y hablar de privatización.  

## Detalle

A la hora de hablar de coste son necesarios otros datos de MUFACE, como número de mutualistas y porcenaje de mutualistas que optan por prestador de servicios sanitarios privados. Aquí usaré datos que están documentados en el post [MUFACE](../muface) 

### Coste global de MUFACE. Porcentaje de coste asociado a prestación sanitaria privada. 

Intento poner datos de [memorias muface](https://www.muface.es/muface_Home/muface/Transparencia/informacion-institucional-organizativa-planificacion/memoria.html)  
Cuando escribo por primera vez el post los últimos datos eran de memoria 2022 

En [memoria 2022 pdf](https://www.muface.es/dam/jcr:2985f319-6b76-4244-a1ff-6058d843cc9e/Memoria_Muface_2022.pdf)  
> 1.4.2 Presupuesto  
Al iniciarse el ejercicio 2022, el presupuesto de MUFACE ascendía a 1.905.793.890 euros, dividido en los siguientes programas:  
a. Programa 312E “Asistencia Sanitaria del Mutualismo Administrativo”, que
supone el 82,6% del presupuesto de gastos y cuya dotación inicial ascendía
a 1.574.174.030 euros.  
b. Programa 222M “Prestaciones económicas del Mutualismo Administrativo”, que representa el 17,2 % del
presupuesto de gastos total, con una dotación inicial de 328.107.800 euros.

En [memoria 2023 pdf](https://www.muface.es/dam/jcr:7770da43-c118-419b-9619-0283022c012f/MUFACE_MEMORIA_2023_WEB.pdf)  
> Al inicio del ejercicio 2023, el presupuesto de MUFACE ascendía a 1.938.916.460 euros dividido en los siguientes programas:
* Programa 222M “Prestaciones económicas del Mutualismo Administrativo”, con
una dotación inicial de 320.516.940 euros.  
* Programa 312E “Asistencia Sanitaria del Mutualismo Administrativo”, cuya dotación inicial ascendía a 1.616.911.150 euros

Más adelante se puede ver desglose 312E con dos partidas relevantes:

> PROGRAMA 312E (Importe en millones de euros)  
> Conciertos de asistencia sanitaria 1.188,10  
> Farmacia 417,49  

Sobre contabilizar solo los conciertos con o sin gasto en farmacia, en el coste de SNS se incluye farmacia (ver datos), así que no se separa.  Eso implica que el gasto en concierto sanitario con empresas privadas parece menor porque el gasto en farmacia, que sí se paga con dinero público a los que tienen sanidad privada, va fuera de ese concierto.  


[Muface > Transparencia > Información económico-presupuestaria y patrimonial (subastas) > Presupuesto](https://www.muface.es/muface_Home/muface/Transparencia/informacion-economico-presupuestaria/presupuesto.html)  
> Las principales partidas de la Mutualidad contempladas en los presupuestos generales del Estado para el año 2023 presentan el siguiente desglose:  
    Cotizaciones de mutualistas: 18,57 %  
    Aportación del Estado: 76,73 %  
    Subvención del Estado: 3,51 %  
    Otros ingresos: 0,25%  
    Remanente de tesorería 0,94%  
Para hacer frente a los gastos de su acción protectora, MUFACE destina aproximadamente el 83,33% de su Presupuesto a la asistencia sanitaria y prestación farmacéutica, el 14,38 % a las restantes prestaciones del Mutualismo Administrativo, el 1,50 %, a las prestaciones de las mutualidades integradas en el Fondo Especial y el 0,79 % a los gastos de funcionamiento.

Se puede ver como el grueso del coste es la asistencia sanitaria, asociada a prestarla mediante entidades privadas  
Se puede ver como las cuotas de los mutualistas no pagan la asistencia sanitaria privada  

### Coste global 2022 a 2024

**10 octubre 2021**

[Muface, la mutua de los funcionarios, pagará más de 3.500 millones de euros a las aseguradoras hasta 2024 - newtral](https://www.newtral.es/muface-aseguradoras-empresas-concierto/20211010/)  

[Concierto para el aseguramiento del acceso a la Prestación de Asistencia Sanitaria en territorio nacional a los beneficiarios de MUFACE durante los años 2022, 2023 y 2024. - contrataciondelestado.es](https://contrataciondelestado.es/wps/wcm/connect/20b2e75d-6e98-418c-9abb-77c893891b76/DOC_CAN_ADJ2021-491138.pdf?MOD=AJPERES)  
>3.521.733.936,5 EUR  

### Coste global a partir de 2025 

**28 agosto 2024**

[Novedades en Muface: el Gobierno apuesta por un presupuesto récord para salvar a la mutualidad - vozpopuli](https://www.vozpopuli.com/actualidad/gobierno-presupuesto-muface-sd.html)  
> Las aseguradoras piden al Gobierno un aumento del 50% en las primas para seguir en Muface  

**8 octubre 2024**

[CONCIERTO DE MUFACE AÑOS 2025-2026 - lamoncloa.gob.es](https://www.lamoncloa.gob.es/consejodeministros/referencias/Paginas/2024/20241008-referencia-rueda-de-prensa-ministros.aspx#muface)  

> El Consejo de Ministros ha autorizado la licitación del concierto de Muface para los años 2025-2026, por el que se ofrece cobertura sanitaria a través de aseguradoras a parte de los funcionarios que prestan servicios en España y en 123 países.  
Este acuerdo se incardina dentro de la planificación estratégica de la Secretaría de Estado de Función Pública, dirigida a modernizar y mejorar el servicio que presta la Mutualidad General de Funcionarios Civiles del Estado (MUFACE).  
Se trata de un concierto a dos años con la finalidad de garantizar la asistencia sanitaria del colectivo cubierto, que supera los 1,5 millones de personas, entre titulares y beneficiarios. En Muface, el 65% son docentes de primaria, secundaria y universitarios y un 17% pertenece a algunos cuerpos de la Administración General del Estado.  
La cuantía total de la contratación asciende a 1.337.059.970 euros en 2025 y 1.344.553.098 euros en 2026, un incremento total del nuevo concierto bianual de 303.949.078 euros respecto al último año de vigencia del concierto anterior.  
Con dicha dotación, el concierto 2025-2026 ofrece una prima del 17,12%, la mayor de la historia desde que existen registros homologables, y que se distribuye en un 16,5% en 2025 y un 0,62% en 2026. En los últimos 25 años, el aumento anual de la prima ha sido de media de un 4,4%.  
Tras esta subida, la prima media por mutualista al año pasará de 1.032,12 euros a 1.208,81 en 2026, lo que supone un aumento de la prima por asegurado de 176,69 euros.

[El Gobierno planea mejorar a las aseguradoras para salvar el convenio de Muface - elpais](https://elpais.com/economia/2024-10-08/el-gobierno-se-abre-a-un-gesto-con-las-aseguradoras-para-salvar-el-convenio-de-muface.html)  


**17 diciembre 2024**  

[Referencia del Consejo de Ministros LICITACIÓN DEL CONCIERTO SANITARIO DE MUFACE PARA 2025, 2026 Y 2027](https://www.lamoncloa.gob.es/consejodeministros/referencias/Paginas/2024/20241217-referencia-rueda-de-prensa-ministros.aspx#licitacionmuface)  
> El Consejo de Ministros ha autorizado la licitación del concierto de MUFACE para los años 2025, 2026 y 2027, por el que se ofrece cobertura sanitaria a través de aseguradoras a parte de los funcionarios que prestan servicios en España y en 123 países.  
Este acuerdo se incardina dentro de la planificación estratégica de la Secretaría de Estado de Función Pública, dirigida a modernizar y mejorar el servicio que presta la Mutualidad General de Funcionarios Civiles del Estado (MUFACE).  
Se trata de un concierto a tres años con la finalidad de garantizar la asistencia sanitaria del colectivo cubierto, que supera los 1,5 millones de personas, entre titulares y beneficiarios. En Muface, el 65% son docentes de primaria, secundaria y universitarios y un 17% pertenece a algunos cuerpos de la Administración General del Estado.  
La cuantía total de la contratación para la asistencia sanitaria de los mutualistas en España asciende a 4.478 millones de euros, 957 millones de euros más que el concierto vigente. El coste por año será de 1.303 millones de euros en 2025, 1.490 millones euros en 2026, y 1.685 millones de euros en 2027. En total, 4.478 millones de euros, lo que supone un aumento de la prima del 33,5% en tres años.  
Con esta dotación, la prima subirá un 19,37% en 2025; un 7,25% en 2026 y un 4,32% en 2027, con un aumento acumulado en tres años del 33,5%. La prima media por mutualista al año pasará de 1.032,12 euros en la actualidad a 1.262,28 en 2027.  
En cuanto al concierto para la asistencia sanitaria en el exterior, el presupuesto asciende a 77,7 millones de euros.  
Proceso más transparente  
En esta ocasión, se ha incrementado la transparencia en el proceso de elaboración de la licitación convocando consultas preliminares de mercado, para que las aseguradoras justificaran cómo fijan sus precios.  
Atendiendo a sus aportaciones, se ha incorporado una novedad al convenio, cambiando la fórmula de cálculo de la prima. Ahora se pasa de una prima lineal a una incremental en función de los tramos de edad, de manera que se pague más por asegurar a los colectivos de más edad, y menos por los más jóvenes.  
Hasta que se adjudique el nuevo concierto, los mutualistas tendrán garantizada su asistencia sanitaria en las condiciones actuales, en virtud del mecanismo de orden de continuidad de los servicios que contempla el artículo 288 a) de la Ley de Contratos del Sector Público.  

**23 diciembre 2024**

Recibo correo de asunto MUFACE - Licitación conciertos, cambio de entidad y continuidad de prestaciones

Estimado/a mutualista:

Nos dirigimos a usted para informarle de que el Consejo de Ministros ha autorizado el 17 de diciembre la licitación de los Conciertos de Asistencia Sanitaria para el periodo 2025-2027. Estas licitaciones se refieren al Concierto de Asistencia Sanitaria Nacional y al Concierto de Asistencia Sanitaria para mutualistas en el exterior y la convocatoria de ambas se ha publicado ya en la Plataforma de Contratación del Sector Público, dentro del Perfil del Contratante de la Dirección General de MUFACE. Para más información, puede consultar el [ENLACE a página web del Ministerio para la Transformación Digital y de la Función Pública](https://digital.gob.es/gl/portalmtdfp/comunicacion/sala-de-prensa/comunicacion_SEFP/2024/12/2024-12-17.html) y el [Perfil del Contratante de MUFACE (ENLACE Licitaciones)](https://contrataciondelestado.es/wps/portal/!ut/p/b0/04_Sj9CPykssy0xPLMnMz0vMAfIjU1JTC3Iy87KtClKL0jJznPPzSooSSxLzSlL1w_Wj9KMyU5wK9CNzzQzd3VUNjJxNi9Nt9Qtycx0B6tPuvA!!/).

Con el fin de garantizar la continuidad de la asistencia sanitaria, MUFACE ha resuelto dictar orden de continuidad del servicio a las entidades aseguradoras lo que garantiza la prestación de los servicios desde el fin de vigencia de los conciertos actuales, en los mismos términos y condiciones de hoy, hasta que se formalice la nueva cobertura, independientemente del momento en que esto ocurra. [(ENLACE a nota WEB MUFACE)](https://www.muface.es/muface_Home/Prestaciones/asistencia-sanitaria-nacional/Novedades-Conciertos-de-Asistencia-sanitaria-2025-2027.html). En este sentido, existe total garantía de continuidad.

Dada la situación generada por el retraso en la convocatoria, derivado de la ausencia de ofertas por parte de entidades aseguradoras en la primera licitación del pasado mes de octubre, los plazos para realizar cambios de entidad en 2025 han de ajustarse. Este ajuste respetará la libertad de elección del colectivo y proporcionará un margen adecuado para garantizar que cada persona pueda ejercer su derecho de elección de manera informada y responsable. Ha de tenerse en cuenta que, al no haber todavía nuevo Concierto, el plazo para elegir entidad concertada ha de posponerse a un segundo momento.

Los plazos de cambio serán los siguientes (rogamos lea atentamente estas indicaciones y las que se vayan publicando [en el apartado de Cambio de Entidad en nuestra página web](https://www.muface.es/muface_Home/Prestaciones/asistencia-sanitaria-nacional/cambio-entidad-sanitaria.html)):

    Del 1 al 31 de enero de 2025: Podrá solicitarse el cambio a INSS (servicios públicos de salud de comunidades autónomas e INGESA en Ceuta y Melilla), en cumplimiento del convenio suscrito con dicho organismo.
    A partir del momento en que esté vigente el nuevo Concierto y hasta la fecha que se disponga: Se abrirá un segundo plazo para realizar el cambio desde servicio de salud autonómico a una entidad concertada o entre entidades concertadas que sean adjudicatarias del servicio. Se informará sobre la fecha de apertura de plazo.

Es importante destacar que aquellas personas que opten por cambiar al INSS durante el mes de enero podrán revertir su decisión en el segundo plazo y adscribirse a una entidad concertada, si así lo desean. Recomendamos consultar la oferta de cada entidad antes de tomar una decisión, para garantizar una elección responsable y adecuada a sus necesidades.

Le mantendremos informado/a de cualquier avance significativo a través de nuestra página web. Le agradecemos su paciencia y queremos transmitirle un mensaje de tranquilidad: la continuidad de las prestaciones está completamente garantizada hasta la entrada en vigor del nuevo Concierto.

Estamos como siempre a su disposición para cualquier duda o consulta.

Un cordial saludo y felices fiestas.

**8 enero 2025**  
[Óscar López, sobre Muface: “Estamos metiendo 1.000 millones de euros más de todos los ciudadanos para financiar el seguro privado de un millón y medio” ](https://www.eldiario.es/economia/oscar-lopez-muface-metiendo-1-000-millones-euros-ciudadanos-financiar-seguro-privado-millon-medio_1_11948604.html)  

**25 enero 2025**
[El Gobierno hará cambios en la licitación de Muface y Adeslas “reconsidera” su negativa - eldiario.es](https://www.eldiario.es/economia/gobierno-suspende-plazo-presentacion-ofertas-muface-hara-cambios-licitacion_1_11992945.html)  


### Evolución del coste

Se pueden ver datos en [Muface, la mutua de los funcionarios, pagará más de 3.500 millones de euros a las aseguradoras hasta 2024 - newtral](https://www.newtral.es/muface-aseguradoras-empresas-concierto/20211010/)  

>  Conciertos de Muface con las empresas aseguradoras para asistencia sanitaria  
Son los presupuestos estimados extraídos de los expedientes de la Plataforma de Contratación del Sector Público. En euros   
> 2022-2023-2024 3522 M€  
> 2020-2021  2223 M€  
> 2018-2019  2161 M€  
> 2016-2017  2052 M€  

En 9 años se ha aumentado un 150%, y el número de titulares+beneficiarios no ha variado significativamente, ver post [MUFACE](../muface) 

**10 noviembre 2024**  
[Hilo Carlos Sánchez Mato](https://threadreaderapp.com/thread/1855598977544032714.html)  
El concurso de MUFACE de asistencia sanitaria a funcionarios ha quedado desierto a pesar del incremento del 17%.
Las aseguradoras privadas dicen que pierden dinero pero acumulan subidas de un 39% desde 2018 frente a un 18,2% del IPC.
¿Qué hay detrás del chantaje del sector?  

**17 diciembre 2024**  
La nueva licitación supone 4478 M€ para 2025-2026-2027  

**27 diciembre 2024**  
[Adeslas no dará servicio de sanidad privada a los funcionarios por Muface pese a la subida de la prima de un 33% - eldiario](https://www.eldiario.es/economia/adeslas-no-dara-servicio-sanidad-privada-funcionarios-muface-pese-subida-prima-33_1_11930376.html)  
La compañía integrada en el Grupo Mutua Madrileña y participada por CaixaBank considera que el modelo de Muface es “económicamente insostenible”. En su opinión, de mantenerse en el concierto “comprometerían la solvencia y el futuro” de la empresa 




### Coste de prestación sanitaria en MUFACE

El argumento de que lo privado tiene menos coste en sanidad es similar al argumento en educación: lo público busca un servicio, lo privado busca negocio, y por eso lo público puede tener coste medio mayor porque cubre todo lo que no cubre lo privado, que deriva a la pública los servicios problemáticos. En sanidad recuerdo el documental Sicko de Michael Moore.  

Ver comentario en [No existe ningún centro que sea concertado](https://algoquedaquedecir.blogspot.com/2018/02/no-existe-ningun-centro-que-sea-concertado.html)  
> Por poner ejemplos, en sanidad una tratamiento médico complejo, en seguridad pagar a la UME, en educación atender a ACNEE o CRA suponen "pérdidas", no hay beneficio económico de los centros que prestan el servicio, pero son servicios públicos que benefician a todos, aparte de que uno lo perciba más si sufre un cáncer, si sufre una catástrofe, o si tiene un hijo que requiera una escolarización especial.  

### Coste medio por mutualista

Se usa a menudo el coste medio, y eso enlaza de nuevo con el coste medio en educación.  

También a veces se habla de "mutualistas" sin hablar de mutualistas jubilados y de beneficionarios, que no contribuyen con cuota.

**18 julio 2020**

[¿Quebraría la Sanidad Pública si el Gobierno deja a los funcionarios sin el modelo Muface?](https://www.larazon.es/salud/20200718/hdzl7jhekzazba6uqjn4raaqoi.html)  
> Por el empleado público atendido a través de Muface, el Estado paga 883 euros al año de media; por el paciente del sistema público abona de media 1.224. Hay pues un diferencial de 341 euros, lo que se traduce en un ahorro para las arcas estatales de 682 millones de euros anuales.  

En [Septiembre 2024 Impacto de las mutualidades en el Sistema Sanitario - fundacionidis](https://www.fundacionidis.com/uploads/informes/Impacto_de_las_mutualidades_en_el_Sistema_Sanitario_v06.pdf) se indica  

> HIPÓTESIS 8 Se considera una prima media de 1.013
euros por mutualista (1).  
HIPÓTESIS 9 El gasto público per cápita en sanidad
estimado para el 2023 es de 2.001 euros (2)
(incluyendo gastos de farmacia). Sin tener en
cuenta estos gastos el coste estimado es de
1.640 euros. (3)  

Donde las fuentes son  
> [(1) Observatorio de la sanidad privada 2024 IDIS.](https://www.fundacionidis.com/uploads/informes/OBS._S.S._PRIVADO_IDIS_20241003_VF3.pdf) 

No aparece la cifra de "1.013" ni "1013" ni "prima media"  
En Gráfica 67 COMPARATIVA ENTRE LA CÁPITA DEL SNS Y LA PRIMA MUFACE, 2018-2023  
aparece 1030 € estimado para 2023  

>[(2) Principales datos del Sistema Nacional de Salud – Febrero 2024- Ministerio de Sanidad](https://www.sanidad.gob.es/estadEstudios/portada/docs/DATOS_SNS_02_2024.pdf)  

Se actualizan mes a mes, en [noviembre 2024](https://www.sanidad.gob.es/estadEstudios/portada/docs/DATOS_SNS_11_2024.pdf)  
Ahí se habla de 2079€ / habitante para 48 millones de habitantes, que suponen 99300 millones € / año  
Se indican 14132 millones de € en recetas de medicamentos sin indicar explícitamente si se incluyen fuera o dentro de los 99300 millones, pero está tabulado y dentro, y tras indicar los 34700 millones al año de gasto sanitario privado  que sí se indica que incluye medicamentos, luego se indica gasto sanitario total en 134000 millones al año, luego son 99300+34700 y sí incluye el gasto en farmacia / medicamentos.   

>(3) ICEA seguros de Salud 2023

El primero usando IDIS como se comenta en [MUFACE](../muface) que tiene un conflicto de intereses.  
El tercero es de pago, ver [El Seguro de Salud a diciembre. Año 2023](https://www.icea.es/es-ES/informaciondelseguro/paginas/fichadetexto.aspx?idpublicacion=3312). Hasta su presentación es de pago [Conoce ICEA](https://www.icea.es/es-ES/conoce-icea). Ver [ICEA (Investigación Cooperativa entre Entidades Aseguradoras y Fondos de Pensiones) - wikipedia](https://es.wikipedia.org/wiki/Investigaci%C3%B3n_Cooperativa_entre_Entidades_Aseguradoras_y_Fondos_de_Pensiones)  


### Cuota de los mutualistas

Un mutualista, que puede ser obligatorio, está obligado a pagar cuota a MUFACE

También a veces se habla de "mutualistas" sin hablar de mutualistas jubilados y de beneficionarios, que no contribuyen con cuota.

Lo que paga cada mutualista se fija por normativa, y depende de lo que cobra el funcionario

[Ley 31/2022, de 23 de diciembre, de Presupuestos Generales del Estado para el año 2023. Artículo 123. Cotización a las Mutualidades Generales de Funcionarios para el año 2023.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-22128#a1-35)  
> 1. El porcentaje de cotización de los funcionarios en activo y asimilados integrados en MUFACE, se fija en el 1,69 por ciento sobre los haberes reguladores establecidos en el apartado Uno del presente Artículo, a efectos de cotización de Derechos Pasivos.  
> 2. La cuantía de la aportación del Estado, regulada en el artículo 35 del Real Decreto Legislativo 4/2000, de 23 de junio, representará el 6,59 por ciento de los haberes reguladores establecidos en el apartado Uno del presente Artículo, a efectos de cotización de Derechos Pasivos. De dicho tipo del 6,59, el 4,10 corresponde a la aportación del Estado por activo y el 2,49 a la aportación por pensionista exento de cotización.

A nivel orientativo son unos 50 € por nómina (son 14 cuotas anuales) para un profesor

[twitter democraciareal/status/1564549948854751234](https://x.com/democraciareal/status/1564549948854751234)  
El concierto de Muface cuesta a todos los ciudadanos 3.571 millones: dinero público para que se lucre el sector privado mientras la pública se deteriora por falta de financiación.  
Además, los funcionarios tienen que pagar unos 50€/mes con su salario, aunque no usen la privada.  

[twitter FiQuiPedia/status/1272621291481202688](https://x.com/FiQuiPedia/status/1272621291481202688)  
Soy funcionario desde 2016. Ya sé que cotizo a SS. El tema es para qué cotizo a muface y si puedo dejar de hacerlo teniendo como único prestador SS. Solo me ha supuesto problemas con la receta electrónica.
![](https://pbs.twimg.com/media/EalCUHCXkAIMSis?format=jpg)  

[Régimen de cotizaciones - Cuotas Ejercicio 2023 - muface](https://www.muface.es/muface_Home/mutualistas/cotizacion/Regimen-de-Cotizaciones.html)  
> La cuota mensual para 2023, según el grupo/subgrupo funcionarial al que pertenezca el mutualista, oscila entre 51,68 € y 21,07 €.  


En [memoria 2022 pdf](https://www.muface.es/dam/jcr:2985f319-6b76-4244-a1ff-6058d843cc9e/Memoria_Muface_2022.pdf)  
> Tabla 11. Desglose del presupuesto de ingresos de MUFACE  
Cuotas de los funcionarios 341,26 M€  
Aportación del estado 1468,94 M€  

### Cálculo de coste al estado, cuotas, ahorro  

En [(1) Observatorio de la sanidad privada 2024 IDIS.](https://www.fundacionidis.com/uploads/informes/OBS._S.S._PRIVADO_IDIS_20241003_VF3.pdf) con conflicto de intereses (actualizado 3 octubre 2024 junto a la renovación) en Gráfica 67 COMPARATIVA ENTRE LA CÁPITA DEL SNS Y LA PRIMA MUFACE, 2018-2023 aparece 1736€ gastos SNS y 1030€ prima MUFACE estimado para 2023  

> En 2023 la diferencia entre lo
que cuesta al Sistema Nacional
de Salud la elección que hace un
funcionario entre sector público
y compañía aseguradora privada
para su asistencia médica es de
706 € al año. Si 1,68 millones
de funcionarios eligen sanidad
privada, esto equivale a un
menor coste de 1.186 millones
de € para el sistema.  

Usan el dato de 1,68 millones de funcionarios, cuando son 1,5: se va al alza   
Usan dato de 1736€ por mutualista en lugar de 1640€ indicados en presentación septiembre: se va al alza

En artículos se dan cifras similares usando IDIS de fuente

[La sanidad de 1,5 millones de funcionarios queda en el aire por la espantada de las aseguradoras de Muface - cincodias](https://cincodias.elpais.com/companias/2024-10-02/la-asistencia-sanitaria-de-15-millones-de-funcionarios-queda-en-el-aire-por-la-espantada-de-las-aseguradoras-de-muface.html)  
> Actualmente, la prima media de Muface es de 984 euros, según detallaba hace dos semanas la Fundación IDIS, frente a los 1.608 euros por ciudadano en el sistema público.  


Si usamos  
1.574.174.030 € de presupuestos 2022 en gasto sanitario  (fuente MUFACE)  
1.496.276 mutualistas en 2022  (fuente MUFACE)  
Supone 1574174030/1496276=1052 € de coste medio por mutualista.  

Eso coincide aproximadamente con lo que dice IDIS

**PERO**

Si usamos solo los 1.095.393 mutualistas en 2022 **que tienen prestador de sanidad privada** (fuente MUFACE)  
Supone 1574174030/1095393=1437 € de coste medio por mutualista que tiene sanidad privada.  

Si descontamos 341,26 M€ de cuotas de los mutualistas en 2022 (fuente MUFACE), que no es coste del estado sino de los funcionarios, pero sí es coste de las empresas privadas que se les paga a ellas  

(1574174030-341,26e6)/1496276=824€ de coste medio de dinero público por mutualista.  
(1574174030-341,26e6)/1095393=1126€ de coste medio de dinero público por mutualista que tiene sanidad privada.  

Si tenemos en cuenta la infrafinanción que indican las empresas sanitarias privadas para pedir más dinero  

[El Gobierno aprueba la licitación de Muface subiendo un 17% el presupuesto - newtral](https://www.newtral.es/gobierno-licitacion-muface/20241008/)  
> Las cifras, no obstante, siguen lejos de la subida del 38% que las aseguradoras privadas calculan que sería necesario para que la prestación del servicio no fuera deficitaria, según EFE.   
> Estas empresas de cobertura sanitaria privada están abiertas a asumir una subida inicial del 25% el primer año, siempre que el segundo se aplicara otra del entorno al menos del 10%.   

Una subida del 38% supone para 1574174030€ anuales (fueron 3500 M€ por tres años) unos 598 M€ anuales

En otros artículos dan otras cifras

[La sanidad de 1,5 millones de funcionarios queda en el aire por la espantada de las aseguradoras de Muface - cincodias](https://cincodias.elpais.com/companias/2024-10-02/la-asistencia-sanitaria-de-15-millones-de-funcionarios-queda-en-el-aire-por-la-espantada-de-las-aseguradoras-de-muface.html)  
> Esta cifra es inferior a la subida del 24% que había deslizado Muface en los últimos días y se aleja del incremento del 40% que pedían las aseguradoras. 

[Las aseguradoras acumulan 429 millones de pérdidas en el convenio actual de Muface - eleconomista](https://www.eleconomista.es/banca-finanzas/noticias/13027969/10/24/las-aseguradoras-acumulan-429-millones-de-perdidas-en-el-convenio-actual-de-muface.html)  
> Las tres compañías estiman que a lo largo del trienio comprendido entre 2022 y 2024 perderán 429 millones de euros. Una deuda que se ha repartido de manera desigual a lo largo de los años, pero que aun así está lejos de las que las fuentes del sector estimaron en mayo, donde situaron esos números rojos en unos 600 millones de euros y que las tres aseguradoras dieron por buenos.  

[Desmantelar Muface exigirá inyectar 1.000 millones en la Sanidad pública y aumentar en casi 4.000 camas la capacidad - elmundo](https://www.elmundo.es/ciencia-y-salud/salud/2024/10/05/66fffc0ce85eceb8148b457b.html)  
> Las compañías ya han dicho que no lo aceptan. Ya que se trata de un porcentaje muy inferior al algo más del 24% propuesto por Muface y muy lejos del 40%   
>...  
> Las aseguradoras recuerdan que solo en 2024 han asumido pérdidas de más de 200 millones de euros.  

Cuando se habla de ahorro se tiende a dar datos al alza, y se suele mezclar ISFAS y MUGEJU

[El mutualismo administrativo: modelo predictivo sobre la elección de los mutualistas de su modelo sanitario y escenarios futuros. Diciembre 2023 - ucm.es](https://docta.ucm.es/entities/publication/d977bd12-70fd-44b2-a897-e01f02216bc4)  
> Respecto a la financiación del modelo del mutualismo administrativo, el gasto sanitario
público estimado para 2023 es de 1.608 euros, mientras que la prima media ponderada
de MUFACE es de 1.014 euros, por lo que la diferencia de 594 euros existente por mutua-
lista y año es muy significativa. Si se aplica este diferencial al conjunto de los mutualistas
(MUFACE, MUGEJU e ISFAS), se calcula que el modelo de mutualismo aporta un ahorro
al Estado superior a los 1.000 millones de euros al año.  

Si consideramos 200 M€ de coste adicional en sanidad privada (coincide aproximadamente con 598/3)  
200e6/1496276=134€ más de coste medio de la sanidad privada por mutualista  
200e6/1095393=183€ más de coste medio de la sanidad privada por mutualista que tiene sanidad privada


Eso serían  
824+134=958€ de coste medio de dinero público por mutualista  
1126+183=1309€ de coste medio de dinero público por mutualista que tiene sanidad privada

Se reduce la diferencia con el coste público

Si se considera el 40% de incremento que los artículos dicen que piden las empresas sanitarias privadas a partir de 2025  
(adeslas cuando se retira en diciembre 2024 pide un 47% de incremento en muface, isfas y mugeju va por seprado)  
958 x 1,4=1341€ de coste medio de dinero público por mutualista  
1309 x 1,4=1833€ de coste medio de dinero público por mutualista que tiene sanidad privada

La diferencia con el coste pública desaparecería (habría que tener en cuenta la variación en el coste medio de dinero público en la sanidad pública)

**PERO**

Aquí se ha hablado de mutualistas en general sin tener en cuenta jubilados que no aportan cuota.

¿Si MUFACE paga a las entidades privadas por prestar atención sanitaria a sus mutualistas, no debería MUFACE pagar a la Seguridad Social cuando un mutualista decide que sus prestador es la Seguridad Social?  
Se está diciendo que la seguridad social va a tener más coste por tener más personas que atender si no van a la privada, pero la seguridad social también debería tener más ingresos si atiende a más personas y esos ingresos antes iban a la privada.  
Un mutualista paga una cuota obligatoria, que en las cuentas va a pagar los conciertos de las prestadoras privadas de sanidad, cuando hay mutualistas que no tienen sanidad privada sino pública.  

En cierto MUFACE se puede pensar que sí paga la sanidad pública solo en ciertos casos donde es inevitable: zonas rurales ( poblaciones pequeñas de menos de 10000/ 20000 habitantes donde no hay entidades privadas porque no les sale rentable para obtener beneficio) donde la sanidad pública es la única que garantiza el servicio público de sanidad universal.  
[Resolución de 22 de diciembre de 2021, de la Mutualidad General de Funcionarios Civiles del Estado, por la que se publica el Concierto suscrito con entidades de seguro para el aseguramiento del acceso a la asistencia sanitaria en territorio nacional a los beneficiarios de la misma durante los años 2022, 2023 y 2024.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-21337)  
> 2.5 Como criterio especial se tendrá en cuenta que, en las zonas rurales expresamente previstas en los convenios a que se refiere el Anexo 2 de este Concierto, y con el alcance y contenido estipulado en cada uno de ellos, la asistencia sanitaria a nivel ambulatorio, domiciliario o de urgencia a cargo del médico general o de familia, pediatra, diplomado en enfermería y matrona se podrá prestar por los Servicios de Atención Primaria y de Urgencia de la Red Sanitaria Pública.  

> Para hacer posible la prestación de servicios sanitarios en las zonas rurales a los beneficiarios adscritos a la Entidad, MUFACE podrá convenir con los Servicios de Salud de las comunidades autónomas la prestación de aquellos ...  

Sin embargo la realidad es que no se paga nada...: ver Costes para MUFACE/SNS que no son coste para empresas privadas, fuera de convenio, pero sí dan servicio a funcionarios que tienen prestación privada

Respecto a la diferencia de coste, la clave está en la media   

Ver [Muestras de información a medias](https://algoquedaquedecir.blogspot.com/2019/03/muestras-de-informacion-medias.html)  

Los servicios públicos tienen más coste MEDIO porque son un servicio universal y atienden lo que los privados evitan por costoso.  
Se puede hacer una analogía con la educación y los privados con concierto.  

Las empresas privadas dan servicio en zonas urbanas y se limitan a servicios básicos para maximizar el beneficio: los entornos rurales, los casos sanitarios y educativos problemáticos (enfermedades graves y crónicas, aulas rurales agrupadas, las aulas hospitalarias, la educación domiciliaria, ...) son derivados y atendidos por la sanidad y educación pública 

El tema de que la sanidad pública no puede atender a todos y si no existiera MUFACE colapsaría la sanidad pública, también tiene un paralelismo en educación con el origen de los privados con concierto: es una situación histórica, pero un servicio público no tiene sentido que se preste con empresas privadas que buscan beneficio, salvo situaciones realmente excepcionales y temporales. 

Las empresas privadas con concierto dicen al tiempo que lo privado es más eficiente y es ahorro, y que están infrafinanciadas, necesitan más dinero público para no tener pérdidas / para hacer lo que no hacían para evitar cosas costosas. Al pedir más dinero están reconociendo que ahorran menos, pero son empresas privadas que tienen que tener beneficios. Si el coste medio en el servicio público sube (por ejemplo en sanidad por envejecimiento de la población) los privados ven más margen para mover la línea y recibir más dinero público pudiendo seguir diciendo que suponen ahorro.

Veo un paralelismo con cheques de educación Madrid **solo para privados** que se defiende argumentando la libertad de elección: nadie cuestiona la libertad de elegir, se cuestiona dar dinero público solo para elegir algo privado. 

El tema de que se argumente que la sanidad pública no puede atender a todos y si no existiera MUFACE colapsaría la sanidad pública (listas de espera, ocupación camas, ...) es tramposo si la situación se ha originado por una privatización.  

En [El mutualismo administrativo: modelo predictivo sobre la elección de los mutualistas de su modelo sanitario y escenarios futuros. Diciembre 2023 - ucm.es](https://docta.ucm.es/entities/publication/d977bd12-70fd-44b2-a897-e01f02216bc4)  
> A nivel de infraestructuras, el sistema público precisaría de 3.975 camas adicionales, lo que su-
pondría un 3,8% adicional a la capacidad actual a nivel nacional.   

Se puede combinar con esto de **un único hospital público**  
[Los madrileños pierden casi 1.000 camas públicas de hospital desde 2015, el equivalente a la desaparición de La Paz - elpais](https://elpais.com/espana/madrid/2024-08-20/los-madrilenos-pierden-casi-1000-camas-publicas-de-hospital-desde-2015-el-equivalente-a-la-desaparicion-de-la-paz.html)  

Mantener la asistencia sanitaria privada en MUFACE efectivamente crea puestos de trabajo en la sanidad privada, puestos pagados con dinero público a una empresa privada que tiene beneficios, de la misma manera que quita puestos de trabajo en la sanidad pública, puestos pagados con dinero público a un organismo público que da un servicio público sin pensar en beneficios. Se puede optimizar la eficiencia de los servicios públicos, pero no se puede evitar que una empresa privada necesite un margen de beneficio.  

### Costes para MUFACE/SNS que no son coste para empresas privadas, fuera de convenio, pero sí dan servicio a funcionarios que tienen prestación privada

Al hablar de coste de MUFACE y coste de convenio con privadas frente a SNS, surgen temas como prestación sanitaria en poblaciones pequeñas y el tema del gasto farmaceútico.  

En su momento me comentaron que "Yo entiendo que yo pago a MUFACE y que MUFACE le paga a la SNS o a la empresa privada que yo elija, pero hay quien dice que solo hace pagos por lo usado"

Tras revisar costes lo veo así:  

- Un funcionario desde hace años cotiza a la SS (no en clases pasivas) y paga sanidad pública, independientemente de que también pague obligado una cuota a muface.  

- Lo que se paga a muface como couta va a empresas privadas pero no a SNS (lo que paga de cuota es solo un % bajo de lo que se paga a esas empresas, la mayoría lo pone el gobierno con los impuestos de todos); muface no paga al SNS para la prestación en poblaciones pequeñas que es un servicio a los mutualistas

[Resolución de 21 de marzo de 2018, de la Subsecretaría, por la que se publica el Convenio entre la Mutualidad General de Funcionarios Civiles del Estado y el Instituto Nacional de la Seguridad Social, para el aseguramiento del acceso a la asistencia sanitaria en territorio nacional a los beneficiarios de la misma y la integración de la información.](https://boe.es/boe/dias/2018/03/23/pdfs/BOE-A-2018-4115.pdf)  
> Quinta. Régimen económico. Este Convenio no comporta obligaciones económicas entre las partes firmantes.
[Resolución de 28 de febrero de 2022, de la Subsecretaría, por la que se publica la Adenda de prórroga y modificación al Convenio entre la Mutualidad General de Funcionarios Civiles del Estado y el Instituto Nacional de la Seguridad Social, para el aseguramiento del acceso a la asistencia sanitaria en territorio nacional a los beneficiarios de la misma y la integración de la información.](https://www.boe.es/boe/dias/2022/03/03/pdfs/BOE-A-2022-3401.pdf)

- Los pagos de medicamentos están fuera de convenio, y es MUFACE quien paga "lo usado" a los funcionarios que tienen sanidad privada sin que lo pague la entidad privada que tiene convenio.

### Sostenibilidad modelo MUFACE

**26 diciembre 2024**  
[El otro riesgo en la financiación de Muface: se duplican los jubilados en apenas trece años - eleconomista](https://www.eleconomista.es/banca-finanzas/noticias/13147758/12/24/el-otro-riesgo-en-la-financiacion-de-muface-se-duplican-los-jubilados-en-apenas-trece-anos.html)  

![](https://cdn.bsky.app/img/feed_fullsize/plain/did:plc:ygxyvnpzivzxnytl73jxnyou/bafkreig7nvgo3uhuov5tuvbltbelrlt3kvma6bqkpaff2ak5kit7p2gzva@jpeg)  
Más jubilados (exentos de cotización y de mayor coste) sin reemplazo de mutualistas cotizantes (7/10 nuevos mutualistas eligen sanidad pública), obligaría a destinar cada vez más dinero público para mantener la sanidad privada en muface

**27 diciembre 2024**  
[Adeslas no dará servicio de sanidad privada a los funcionarios por Muface pese a la subida de la prima de un 33% - eldiario](https://www.eldiario.es/economia/adeslas-no-dara-servicio-sanidad-privada-funcionarios-muface-pese-subida-prima-33_1_11930376.html)  
La compañía integrada en el Grupo Mutua Madrileña y participada por CaixaBank considera que el modelo de Muface es “económicamente insostenible”. En su opinión, de mantenerse en el concierto “comprometerían la solvencia y el futuro” de la empresa 

[Muface queda herida de muerte: Adeslas renuncia a seguir - larazon](https://www.larazon.es/sociedad/adeslas-presentara-muface-suponerle-perdidas-250-millones_20241227676e95c1af2175000101f61a.html)  

> Por otro lado, CSIF también ha apelado a la "responsabilidad" de las aseguradoras implicadas, recordando que han colaborado durante años con el modelo de Muface, por lo que le ha pedido que "no abusen de su situación".  

Responsabilidad y no abusar a empresas privadas que solo buscan beneficio.  

**30 diciembre 2024**  
[DKV comunica a los mutualistas que prestará servicio hasta el 31 de enero de 2025, pero informa que sigue estudiando los pliegos](https://isanidad.com/315916/dkv-comunica-a-los-mutualistas-que-prestara-servicio-hasta-el-31-de-enero-de-2025-pero-informa-que-sigue-estudiando-los-pliegos/)  
> La aseguradora alemana DKV ha anunciado en su página web y en su contestador telefónico que no participará en los nuevos conciertos con las mutualidades. “Le informamos que a partir del 31 de enero de 2025 DKV no continuará con la prestación asistencial a los mutualistas. Por favor, solicite el cambio de entidad a través de su mutualidad”. Esta es la respuesta del teléfono de atención al cliente para mutualistas de DKV. Sin embargo, fuentes de la compañía aseguran a iSanidad que “no han tomado una decisión definitiva y que siguen estudiando los pliegos”.  
Si finalmente DKV se une a Adeslas y se retira de Muface el 65% de los funcionarios quedaría sin cobertura privada  

### Comentarios sobre coste y cobertura real de MUFACE frente a sanidad pública

El tema de desaparición de prestación sanitaria privada a través de MUFACE y que sean atendidos por SNS, con temas de financiación y viabilidad lo comento en post general sobre MUFACE.

**21 enero 2022**

[twitter Medicilio/status/1484623470495518727](https://x.com/Medicilio/status/1484623470495518727)  
Eres de Muface y tienes sanidad concertada con Asisa.  
Te van a operar. Varios especialistas te piden una prueba que multiplica las garantías de éxito.  
La prueba vale 2200€  
Tu seguro no la autoriza.  
Así empieza este #hilo para los de: "Mi seguro me lo cubre todo."  
Especialmente dedicado a los de:  
"Dame lo que pago de impuestos para sanidad, que con eso me pago un seguro que me lo cubre todo."  
Bien, a grandes rasgos eso hace Muface.  
Puedes escoger entre la sanidad pública o le paga tu coste a una aseguradora privada mediante un concierto.  
Y además este acuerdo es como el rey de las pólizas, porque la aseguradora debe asegurarle al estado respetar lo que han acordado que se incluye.  
Sin embargo, es todo tan bonito?  
No.  
En cuanto necesites algún tipo de prueba, tratamiento o cirugía vienen las autorizaciones.  
Y esto le pasó a Juanfran, el protagonista de esta historia.  
Pese a ser de Muface, se negaban a realizarle una prueba concreta, previa a una operación importante.  
Según Asisa, no estaba en el convenio de Muface. Te aguantas.  
Y lo pagas. Como Juanfran.  
2200€ por su prueba.  
Juanfran llevo su caso a los medios de comunicación, concretamente a @La_SER consumidor.  
Se pusieron en contacto con la presidenta de la Asociación Defensor del Paciente, Carmen Flores, para que algún jurídico de la misma les asesora de cómo actuar y los derechos del paciente.  
La propia presidenta envió una carta con la queja del usuario a la propia Asisa.  
Y ¡SURPRISE! Unos días después, la aseguradora, que ya le había enviado por escrito la negativa, al verse presionada públicamente, le llama para informarle que sí cubrirían los gastos de la prueba.  
Pero ojo, ya los había tenido que adelantar. Y no en todas las situaciones que nos plantean una situación así, y más en casos que se relacionan con la salud, el paciente está con fuerzas para pelear por sus derechos.  
Y las aseguradoras lo saben.  
Me diréis, esto es un caso aislado, seguro.  
Pero no.  
Vamos a repasar algunos casos igual de salvajes.  
En esta, le negaban una PCR a una niña que tenía que operarse. Y otra vez, cedieron con la presión mediática.  
Aquí otro tipo de concierto público, el de ISFAS, en el que se negaban a pagarle su ingreso por #COVID19 hasta que les obligan.  
Si no te lo pagan, te lo cubres tú.  
Otro caso.  
A una mujer que tenía el seguro previamente contratado, le deniegan una cirugía de tiroides y le suspenden la póliza.  
Aquí un caso en el que después de 16 años de mutualista no le aprobaban la cirugía del cáncer de próstata.  
Eligió cambiar a la sanidad pública y le operan preferentemente (es lo que tienen los cánceres, que no están para papeleo).  
En este caso, en una longeva póliza familiar, después de aceptar una cirugía, rectifican y alegan que no está cubierto y les pasan la factura.  
Según explicó Carlos Sardinero, abogado experto en defensa de causas relacionadas con la sanidad, malas prácticas, abusos y abogado de la Asociación Defensor del Paciente: "Estos casos ocurren reiteradamente en asuntos en los que la cuantía del servicio es elevada"  
"Lo que hacen las compañías es rechazarlo sin causa justificada basándose en que las pruebas específicas no están incluidas en los convenios” comenta Sardinero.  
Y si cuela, eso que se ahorran.  
Y estamos hablando de la póliza que vigila el estado.  
Imagina de libre contratación.  
Carlos Sardinero explicó por qué actúan de esta manera las compañías de seguros ante sus clientes. “Cuando uno contrata un seguro privado, el asegurador te debe hacer un cuestionario concreto.  
"El problema es que las compañías de seguros, con el ánimo de captar los máximos clientes posibles, hace cuestionarios muy genéricos y después te rechazan el siniestro cuando tienes una patología”, dijo Sardinero criticando el “modus operandi” de las aseguradoras.  
Casos como estos, me he encontrado muchísimos, en documentación y en vivo.  
Pensar que una aseguradora privada o un concierto puede suplir a la #SanidadPublica es vivir en una nube a la que no sé como todavía se sigue subiendo la gente.  

[Twitter mapibaez/status/1761709239112737186](https://x.com/mapibaez/status/1761709239112737186)  
Fui de Muface hasta los 15 años (si mal no recuerdo) y ahora la mayor está en muface y la pequeña en la seguridad social.  
A los que dicen que muface es más barato porque se gestiona mejor porque es privado, dejadme que os de un poco de contexto.  
Yo padezco #SindromeSjögren desde pequeña. Desde la perspectiva del tiempo entiendo que era más lógico achacar las cosas a "está creciendo", "no todo el mundo reacciona igual ante una gripe" o "está exagerando porque quiere atención".  
Pero si ves que las cosas no cambian lo normal es empezar a hacerse preguntas y a calorar otras posibilidades.  
"Es que los casos de Sjögren en niños no existen".  
Los cojones. Yo en los 2000 ya encontré varios estudios. El que mas recuerdo era el de una niña valenciana de 9 años  
Cuando salimos de Muface nos metimos en diferentes Cías. de salud y todas dijeron lo mismo acerca de mis problemas de salud.  
No fue hasta que empecé a trabajar con 21 años y que me puse a morir con 26 que la sanidad pública movió cielo y tierra para conseguir una respuesta.  
Me hicieron TACS, análisis de todo tipo de virus, me sacaron tanta sangre que en una de las visitas la enfermera me miró raro y me soltó "uy, aquí tienes callo" (if you know, you know)  
Dieron con el resultado en tres meses.  
TRES MESES.  
Lo que no hizo Muface en 15 años.  
O lo que no hizo ninguna compañía de salud en 6 años. La sanidad pública lo hizo en 3 meses.  
Cuando decís que la privada se gestiona mejor, decidlo con todas las palabras.  
La sanidad privada es una empresa.  
Una empresa busca beneficios.  
Si para obtener beneficios me tengo que ahorrar gastar en recursos, lo voy a hacer. No te haré esas pruebas carísimas.  
Si tengo que elegir entre mantenerte enfermo lo suficiente como para incrementar el número de visitas y de pruebas baratas en el tiempo o hacerte unas pruebas carísimas y darte una respuesta en menos tiempo, elegiré lo que me de más beneficio.  
Si acabas convirtiéndote en un enfermo crónico, primero te subiré la cuota y si puedes pagarla, te denegaré el servicio con una aséptica carta en la que te diré que estás demasiado enfermo y que no me sales rentable.  
Literalmente.  
He entregado dos de esas cartas.  
En Muface hay tratamientos que las mutuas ya no quieren atender, como los tratamientos del cáncer. Te derivan a la pública.  
Lo sé porque mi pareja está en muface y era tema de conversación antes de la pandemia.  
Al parecer porque el gobierno no pagaba.  
El gobierno no pagaba porque las cías de salud habían aceptado un presupuesto cerrado y el gobierno no tiene la culpa de que en tu ansia por conseguir el contrato te pilles los dedos.  
"Pero yo soy una empresa y me debo a mis accionistas. No te doy servicio"  
Muface no debería existir. Punto.  
Si alguien quiere pagar por tener una habitación privada en lugar de asegurarse que le harán todas las litotricias que precise o que cuando de a luz haya una UCI pediátrica, adelante. Cada uno gasta su dinero como le peta.  
Pero si la excusa es que tardan ocho meses en darte cita para un especialista, la solución no debería ser "carguémonos la pública" sino salir a la calle a defenderla y a exigir que pongan medios, que cubran bajas, que repongan las jubilaciones.  
"Es que no encontramos médicos que quieran trabajar"  
Vamos a ver, pepeluís, pon condiciones acorde al trabajo que van a realizar, no me seas rata, y verás cómo cubres las plazas.  
Me da igual quién esté al mando. La sanidad, como la educación, deberían ser tema en el que todos los partidos coincidieran en lugar de ponerse a darse hostias como en el patio de un colegio.  
Por favor, un poquito de seriedad y madurez.  

### Derivación de la sanidad privada a la sanidad pública de los pacientes de alto coste

**20 febrero 2024**
[Pinilla, J., López-Valcárcel, B.G. & Bernal-Delgado, E. Unravelling risk selection in Spanish general government employee mutual funds: evidence from cancer hospitalizations in the public health network. Eur J Health Econ 25, 1371–1381 (2024).](https://doi.org/10.1007/s10198-024-01671-5)  

**8 abril 2024**  
[Un estudio sobre cáncer prueba que la sanidad privada de los funcionarios esquiva a los pacientes de alto coste - eldiario](https://www.eldiario.es/canariasahora/sociedad/estudio-cancer-prueba-sanidad-privada-funcionarios-esquiva-pacientes-alto-coste_1_11267431.html)  


### Atención primaria

Se comenta que no tiene atención primaria

[twitter CiudadanO_O/status/1867497718190682196](https://x.com/CiudadanO_O/status/1867497718190682196)  
En MUFACE no hay atención primaria.   
Cada persona costó, de media y solo en el primer semestre, 189€ a la Comunidad de Madrid. 885 millones en total en Atención Primaria. La usen o no. Los que tienen seguro privado no la usan  
El modelo MUFACE entusiasma a Ayuso and friends  
![](https://pbs.twimg.com/media/GeqvRyrWwAA86kZ?format=jpg)  

[twitter javierpadillab/status/1867938039722315939](https://x.com/javierpadillab/status/1867938039722315939)  

Los defensores de muface indican que sí la tiene, alegando esto

[Atención Primaria Asistencia Sanitaria Concertada en Territorio Nacional - muface](https://www.muface.es/muface_Home/Prestaciones/asistencia-sanitaria-nacional/atencion-primaria.html)  

La realidad es que no hay centros de salud y no hay funcion de "gatekeeper", clave en cualquier sistema de atención primaria. Hay un médico de familia pero no deja de ser un "especialista genérico" al que se puede ir direcamente como a cualquier especialista sin pasar por él.  

[Paradojas en la derivación de primaria a especializada DOI: 10.1157/13120018](https://www.elsevier.es/en-revista-atencion-primaria-27-articulo-paradojas-derivacion-primaria-especializada-13120018)  
> En los países con médicos generales/de familia con función de filtro (gatekeepers o «porteros») para la atención primaria, la derivación del paciente desde primaria a especializada abre las puertas para el paso del primero al segundo nivel asistencial1-3. La función de filtro pretende adecuar la intensidad de la atención a la gravedad y/o rareza de la enfermedad. Así, a través del filtro, los recursos tecnológicos se reservan para quienes probablemente los necesitan, y se evita su uso innecesario por quienes no los necesitan (se evitan el despilfarro económico y el daño a la salud que provoca el uso indebido de los recursos: prevención cuaternaria). El objetivo es prestar servicios de máxima calidad, mínima cantidad, con tecnología apropiada, tan cerca del domicilio del paciente como sea posible4. El aspecto clave es determinar dónde se atienden mejor los problemas de los pacientes, y cuándo se necesita el concurso del especialista.

### MUFACE: modelo y financiación

Aparte del coste, está de dónde provienen los fondos

[Los sistemas sanitarios en los países de la Unión Europea. Características e indicadores de salud 2019 - sanidad.gob.es](https://www.sanidad.gob.es/estadEstudios/estadisticas/docs/presentacion_es.pdf)  
Se citan dos modelos en página 6:  
> **Servicio Nacional de Salud (modelo Beveridge)**  
* Financiación predominante a través de impuestos  
* Acceso universal  
>...  
> **Sistema de Seguros Sociales (modelo Bismarck)**  
* Financiados por cuotas obligatorias pagadas por
empresarios y trabajadores o a través de los impuestos  
* Los recursos financieros van a parar a los "fondos" que
son entidades no gubernamentales reguladas por ley y
que gestionan estos recursos  
* Los "fondos" contratan hospitales, médicos de familia,
etc. para que provean los servicios a los asegurados
mediante contratos basados en un presupuesto o
mediante pago por acto  
> ...  

El SNS de España se encuadra en modelo Beveridge, pero MUFACE se encuadra en Bismarck  
