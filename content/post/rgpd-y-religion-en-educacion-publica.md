+++
author = "Enrique García"
title = "RGPD y religión en educación pública"
date = "2024-08-19"
tags = [
    "educación", "protección de datos"
]

description = "RGPD y religión en educación pública"
thumbnail = "https://lowres.cartooncollections.com/religious_education-religion-priest-vicar-school-religion-WD600088_low.jpg"
images = ["https://lowres.cartooncollections.com/religious_education-religion-priest-vicar-school-religion-WD600088_low.jpg"]

toc = true

+++


Revisado 12 septiembre 2024

### Resumen

Este post intenta plasmar una idea que tengo en la cabeza desde que pensé en combinar RGPD y educación: impartir religión en la educación pública es incompatible con RGPD. Es algo que tenía como simple comentario en un post aparte [Normativa webs, aplicaciones y protección de datos en educación](https://algoquedaquedecir.blogspot.com/2019/06/normativa-webs-aplicaciones-y-proteccion-datos.html) pero que intento desarrollar aquí, con el objetivo de llevar el tema a Agencia Española de Protección de Datos (AEPD) y al Comité Europeo de Protección de Datos (CEPD)

AEPD responde en septiembre 2024 que no incumple RGPD.   

Posts relacionados:
* [Religión, evaluable, de oferta obligada por los centros 2º Bachillerato Madrid](https://algoquedaquedecir.blogspot.com/2018/03/religion-evaluable-de-oferta-obligada.html)  
* [Normativa webs, aplicaciones y protección de datos en educación](https://algoquedaquedecir.blogspot.com/2019/06/normativa-webs-aplicaciones-y-proteccion-datos.html)  


## Detalle

El tema de religión en educación y RGPD creo que lo comenté por primera vez en este tuit de 2019  

[twitter FiQuiPedia/status/1177999024168869888](https://twitter.com/FiQuiPedia/status/1177999024168869888)  
> Impartir la asignatura de religión en centros públicos obliga a declarar sobre religión, cosa que no permite art 16.2 CE "Nadie podrá ser obligado a declarar sobre su ideología, religión o creencias."  
Y eso creo que es un tema RGPD no trivial @AEPD_es  

Incluso sin entrar en RGPD, hay una aparente colisión entre [artículo 16.2 de la Constitución Española](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a16)  
> 2. Nadie podrá ser obligado a declarar sobre su ideología, religión o creencias.  

y el derecho a recibir formación religiosa de acuerdo a convicciones de los padres en [artículo 27.3 de la Constitución Española](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a27)  
> 3. Los poderes públicos garantizan el derecho que asiste a los padres para que sus hijos reciban la formación religiosa y moral que esté de acuerdo con sus propias convicciones.

Para mi la clave es que artículo 27.3 no indica que esa formación religiosa se deba realizar en horario lectivo, mediante una materia de religión y en los centros educativos públicos (solo lo indican los acuerdos con religiones): los poderes públicos pueden garantizar el derecho que asiste a los padres para que sus hijos reciban la formación religiosa ... en el ámbito privado (hogar, iglesia, sinagoga, mezquita o centro educativo privado). Si se argumenta que la elección de la materia de religión por los padres no contradice 16.2 porque no supone declarar sobre su religión, solo manifestar qué es lo que quieren que cursen sus hijos, es desvirtuar la argumentación de que se deba ofrecer religión en base a respetar sus convicciones según artículo 27.3.  

### Normativa protección de datos y religión
[RGPD Reglamento (UE) 2016/679 Artículo 9 Tratamiento de categorías especiales de datos personales](https://www.boe.es/buscar/doc.php?id=DOUE-L-2016-80807)  
> 1. Quedan prohibidos el tratamiento de datos personales que revelen el origen étnico o racial, las opiniones políticas, **las convicciones religiosas** o filosóficas, o la afiliación sindical, y el tratamiento de datos genéticos, datos biométricos dirigidos a identificar de manera unívoca a una persona física, datos relativos a la salud o datos relativos a la vida sexual o las orientación sexuales de una persona física.  
> 2. El apartado 1 no será de aplicación cuando concurra una de las circunstancias siguientes:  
a)el interesado dio su consentimiento explícito para el tratamiento de dichos datos personales con uno o más de los fines especificados, excepto cuando el Derecho de la Unión o de los Estados miembros establezca que la prohibición mencionada en el apartado 1 no puede ser levantada por el interesado;  
b)el tratamiento es necesario para el cumplimiento de obligaciones y el ejercicio de derechos específicos del responsable del tratamiento o del interesado en el ámbito del Derecho laboral y de la seguridad y protección social, en la medida en que así lo autorice el Derecho de la Unión de los Estados miembros o un convenio colectivo con arreglo al Derecho de los Estados miembros que establezca garantías adecuadas del respeto de los derechos fundamentales y de los intereses del interesado;  
c)el tratamiento es necesario para proteger intereses vitales del interesado o de otra persona física, en el supuesto de que el interesado no esté capacitado, física o jurídicamente, para dar su consentimiento;  
d)el tratamiento es efectuado, en el ámbito de sus actividades legítimas y con las debidas garantías, por una fundación, una asociación o cualquier otro organismo sin ánimo de lucro, cuya finalidad sea política, filosófica, **religiosa** o sindical, siempre que el tratamiento se refiera exclusivamente a los miembros actuales o antiguos de tales organismos o a personas que mantengan contactos regulares con ellos en relación con sus fines y siempre que los datos personales no se comuniquen fuera de ellos sin el consentimiento de los interesados;  
e)el tratamiento se refiere a datos personales que el interesado ha hecho manifiestamente públicos;  
f)el tratamiento es necesario para la formulación, el ejercicio o la defensa de reclamaciones o cuando los tribunales actúen en ejercicio de su función judicial;  
g)el tratamiento es necesario por razones de un interés público esencial, sobre la base del Derecho de la Unión o de los Estados miembros, que debe ser proporcional al objetivo perseguido, respetar en lo esencial el derecho a la protección de datos y establecer medidas adecuadas y específicas para proteger los intereses y derechos fundamentales del interesado;  
h)el tratamiento es necesario para fines de medicina preventiva o laboral, evaluación de la capacidad laboral del trabajador, diagnóstico médico, prestación de asistencia o tratamiento de tipo sanitario o social, o gestión de los sistemas y servicios de asistencia sanitaria y social, sobre la base del Derecho de la Unión o de los Estados miembros o en virtud de un contrato con un profesional sanitario y sin perjuicio de las condiciones y garantías contempladas en el apartado 3;  
i)el tratamiento es necesario por razones de interés público en el ámbito de la salud pública, como la protección frente a amenazas transfronterizas graves para la salud, o para garantizar elevados niveles de calidad y de seguridad de la asistencia sanitaria y de los medicamentos o productos sanitarios, sobre la base del Derecho de la Unión o de los Estados miembros que establezca medidas adecuadas y específicas para proteger los derechos y libertades del interesado, en particular el secreto profesional,  
j)el tratamiento es necesario con fines de archivo en interés público, fines de investigación científica o histórica o fines estadísticos, de conformidad con el artículo 89, apartado 1, sobre la base del Derecho de la Unión o de los Estados miembros, que debe ser proporcional al objetivo perseguido, respetar en lo esencial el derecho a la protección de datos y establecer medidas adecuadas y específicas para proteger los intereses y derechos fundamentales del interesado.  
> 3. Los datos personales a que se refiere el apartado 1 podrán tratarse a los fines citados en el apartado 2, letra h), cuando su tratamiento sea realizado por un profesional sujeto a la obligación de secreto profesional, o bajo su responsabilidad, de acuerdo con el Derecho de la Unión o de los Estados miembros o con las normas establecidas por los organismos nacionales competentes, o por cualquier otra persona sujeta también a la obligación de secreto de acuerdo con el Derecho de la Unión o de los Estados miembros o de las normas establecidas por los organismos nacionales competentes.  
> 4. Los Estados miembros podrán mantener o introducir condiciones adicionales, inclusive limitaciones, con respecto al tratamiento de datos genéticos, datos biométricos o datos relativos a la salud.

Se puede ver como en general punto 2 no trata religión salvo punto 2.d que aplica a organizaciones religiosas. 2.h, 2.i 2.j, 3 (2.h) y 4 se centran en salud.

En la normativa estatal  
[LOPDGDD Ley Orgánica 3/2018 Artículo 9. Categorías especiales de datos.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-16673#a9)  
> 1. A los efectos del artículo 9.2.a) del Reglamento (UE) 2016/679, a fin de evitar situaciones discriminatorias, el solo consentimiento del afectado no bastará para levantar la prohibición del tratamiento de datos cuya finalidad principal sea identificar su ideología, afiliación sindical, religión, orientación sexual, creencias u origen racial o étnico.  
Lo dispuesto en el párrafo anterior no impedirá el tratamiento de dichos datos al amparo de los restantes supuestos contemplados en el artículo 9.2 del Reglamento (UE) 2016/679, cuando así proceda.  
>2. Los tratamientos de datos contemplados en las letras g), h) e i) del artículo 9.2 del Reglamento (UE) 2016/679 fundados en el Derecho español deberán estar amparados en una norma con rango de ley, que podrá establecer requisitos adicionales relativos a su seguridad y confidencialidad.  
En particular, dicha norma podrá amparar el tratamiento de datos en el ámbito de la salud cuando así lo exija la gestión de los sistemas y servicios de asistencia sanitaria y social, pública y privada, o la ejecución de un contrato de seguro del que el afectado sea parte.

En los materiales de un curso de protección de datos de septiembre 2020 indicaba 

> "Cabe notar que no tiene la consideración de categoría especial de datos o datos sensibles el que un alumno curse la asignatura de religión, ya que el mero hecho de cursar la misma no implica revelación de su confesión religiosa."

Como alumno del curso planteé esta duda:

> Si se argumenta que cursar una religión no implica revelación de confesión religiosa (es cierto que por ejemplo siendo una materia que computa para nota se matriculan hijos de musulmanes en religión católica), no es consistente con art 27.3 CE, ya que si se oferta es con el argumento de respetar las "convicciones" de los padres: la materia de religión se ofrece porque en art 27.3 CE indica "Los poderes públicos garantizan el derecho que asiste a los padres para que sus hijos reciban la formación religiosa y moral que esté de acuerdo con sus propias convicciones." y por desarrollos legales basados en ello y en acuerdos con confesiones religiosas, como acuerdos con la Santa Sede.  
Aparte de eso, creo que cursar religión obliga a los padres declarar sobre religión/ideología/convicciones, porque están indicando que hijo curse algo tan sensible como una educación religiosa concreta, cosa que yo creo que no estaría permitido según art 16.2 CE "Nadie podrá ser obligado a declarar sobre su ideología, religión o creencias."  
El derecho a la educación religiosa creo que surge de las creencias propias, y si no se está considerando como dato protegido la elección, es reconocer que la elección no es religiosa, y que por lo tanto no está amparada por art 27.3 CE.  
Cuando dos derechos colisionan, uno debe prevalecer. Creo que es incompatible la protección de datos de la elección de educación religiosa con que se realice en centros públicos; aunque no se pongan listados de los alumnos que van a cierta materia de religión, creo que cursarla implica "declarar sobre religión" haciendo que haya información pública que ven todos los demás. Por poner una analogía, una persona que marca la casilla en la declaración renta sobre la iglesia católica, no necesariamente está declarando que es católico, pero creo que si marcarlo implicase hacer público lo que ha elegido, no se consideraría viable implementarlo.  

No tuve respuesta formal, más allá de comentar con tutores que ellos no se habían planteado que la elección de religión supusiese información pública para el resto de alumnado.   


Sin ser normativa, sobre proporcionalidad
[Principio de proporcionalidad datos personales en LOPDGDD y RGPD (grupo ático34)- protecciondatos-lopd.com](https://protecciondatos-lopd.com/empresas/principio-proporcionalidad)  
> El principio de proporcionalidad no se define directamente ni en la LOPDGDD ni el RGPD, pero sí que se menciona, puesto que se debe aplicar siempre que se lleven a cabo tratamientos de datos personales que puedan tener un impacto en los derechos y libertades de los interesados o suponga una intrusión en los mismos. Es decir, se debe aplicar el principio de proporcionalidad siempre que el tratamiento de datos pueda suponer una invasión en derechos fundamentales recogidos en la Constitución Española.  
 
### Normativa educación y religión

[Ley Orgánica 7/1980, de 5 de julio, de Libertad Religiosa. Artículo segundo.](https://www.boe.es/buscar/act.php?id=BOE-A-1980-15955#asegundo)  
> c) Recibir e impartir enseñanza e información religiosa de toda índole, ya sea oralmente, por escrito o por cualquier otro procedimiento; elegir para sí, y para los menores no emancipados e incapacitados, bajo su dependencia, **dentro y fuera del ámbito escolar**, la educación religiosa y moral que esté de acuerdo con sus propias convicciones.  

[Ley Orgánica 7/1980, de 5 de julio, de Libertad Religiosa. Artículo segundo.](https://www.boe.es/buscar/act.php?id=BOE-A-1980-15955#atercero)  
> Uno. El ejercicio de los derechos dimanantes de la libertad religiosa y de culto tiene como único **límite la protección del derecho de los demás al ejercicio de sus libertades públicas y derechos fundamentales**, así como la salvaguardia de la seguridad, de la salud y de la moralidad pública, elementos constitutivos del orden público protegido por la Ley en el ámbito de una sociedad democrática.

[Ley Orgánica 8/1985. Artículo cuarto](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978#acuarto)  
> 1. Los padres, madres o tutores, en relación con la educación de sus hijos e hijas o pupilos y pupilas, tienen los siguientes derechos:  
>...  
c) A que reciban la formación religiosa y moral que esté de acuerdo con sus propias convicciones.  

[Ley Orgánica 8/1985. Artículo cuarto](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978#asexto)  
> 3. Se reconocen al alumnado los siguientes derechos básicos:  
> ...  
> f) A que se respete su libertad de conciencia, sus convicciones religiosas y sus convicciones morales, de acuerdo con la Constitución

[Ley Orgánica 8/1985. Artículo dieciocho](https://www.boe.es/buscar/act.php?id=BOE-A-1985-12978#adieciocho)  
> 1. Todos los centros públicos desarrollarán sus actividades con sujeción a los principios constitucionales, garantía de neutralidad ideológica y respeto de las opciones religiosas y morales a que hace referencia el artículo 27.3 de la Constitución.

[Ley Orgánica 2/2007. Disposición adicional segunda. Enseñanza de la Religión](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dasegunda)  
> 1. La enseñanza de la religión católica se ajustará a lo establecido en el Acuerdo sobre Enseñanza y Asuntos Culturales suscrito entre la Santa Sede y el Estado español.  
A tal fin, y de conformidad con lo que disponga dicho Acuerdo, se incluirá la religión católica como área o materia en los niveles educativos que corresponda, que será de oferta obligatoria para los centros y de carácter voluntario para los alumnos y alumnas.  
> 2. La enseñanza de otras religiones se ajustará a lo dispuesto en los Acuerdos de Cooperación celebrados por el Estado español con la Federación de Entidades Religiosas Evangélicas de España, la Federación de Comunidades Israelitas de España, la Comisión Islámica de España y, en su caso, a los que en el futuro puedan suscribirse con otras confesiones religiosas.  
>3. En el marco de la regulación de las enseñanzas de Educación Primaria y Educación Secundaria Obligatoria, se podrá establecer la enseñanza no confesional de cultura de las religiones.

[Acuerdo entre el Estado español y la Santa Sede sobre Enseñanza y Asuntos Culturales. Artículo II](https://www.boe.es/buscar/act.php?id=BOE-A-1979-29491)  
> Los planes educativos en los niveles de Educación Preesco­lar, de Educación General Básica (EGB) y de Bachillerato Unificado Polivalente (BUP) y Grados de Formación Profesio­nal correspondientes a los alumnos de las mismas edades in­cluirán la enseñanza de la religión católica **en todos los Cen­tros de educación**, en condiciones equiparables a las demás disciplinas fundamentales.  
Por respeto a la libertad de conciencia, dicha enseñanza no tendrá carácter obligatorio para los alumnos. Se garantiza, sin embargo, el derecho a recibirla.  
Las autoridades académicas adoptarán las medidas opor­tunas para que el hecho de recibir o no recibir la enseñanza religiosa no suponga discriminación alguna en la actividad escolar.  
En los niveles de enseñanza mencionados, las autoridades académicas correspondientes permitirán que la jerarquía ecle­siástica establezca, en las condiciones concretas que con ella se convenga, otras actividades complementarias de formación y asistencia religiosa.

[Acuerdo de Cooperación celebrado por el Estado español con la Federación de Entidades Religiosas Evangélicas de España. Artículo 10](https://www.boe.es/buscar/act.php?id=BOE-A-1992-24853#a10)   
> 1. A fin de dar efectividad a lo dispuesto en el artículo 27.3 de la Constitución, así como en la Ley Orgánica 8/1985, de 3 de julio, Reguladora del Derecho a la Educación, y en la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo, se garantiza a los alumnos, a sus padres y a los órganos escolares de gobierno que lo soliciten, el ejercicio del derecho de los primeros a **recibir enseñanza religiosa evangélica en los centros docentes públicos** y privados concertados, siempre que, en cuanto a estos últimos, el ejercicio de aquel derecho no entre en conflicto con el carácter propio del centro, en los niveles de educación infantil, educación primaria y educación secundaria.  

[Acuedo de Cooperación celebrado por el Estado español con la Federación de Comunidades Israelitas de España. Artículo 10](https://www.boe.es/buscar/act.php?id=BOE-A-1992-24854#a10)  
> 1. A fin de dar efectividad a lo dispuesto en el artículo 27.3 de la Constitución, así como en la Ley Orgánica 8/1985, de 3 de julio, Reguladora del Derecho a la Educación, y en la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo, se garantiza a los alumnos judíos, a sus padres y a los órganos escolares de gobierno que lo soliciten, el ejercicio del derecho de los primeros a **recibir enseñanza religiosa judía en los centros docentes públicos** y privados concertados, siempre que, en cuanto a estos últimos, el ejercicio de aquel derecho no entre en contradicción con el carácter propio del centro, en los niveles de educación infantil, educación primaria y educación secundaria

[Acuerdo de Cooperación celebrado por el Estado español con la Comisión Islámica de España. Artículo 10](https://www.boe.es/buscar/act.php?id=BOE-A-1992-24855)  
> 1. A fin de dar efectividad a lo dispuesto en el artículo 27.3 de la Constitución, así como en la Ley Orgánica 8/1985, de 3 de julio, Reguladora del Derecho a la Educación, y en la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo, se garantiza a los alumnos musulmanes, a sus padres y a los órganos escolares de gobierno que lo soliciten, el ejercicio del derecho de los primeros a **recibir enseñanza religiosa islámica en los centros docentes públicos** y privados concertados, siempre que, en cuanto a estos últimos, el ejercicio de aquel derecho no entre en contradicción con el carácter propio del centro, en los niveles de educación infantil, educación primaria y educación secundaria.  

[Real Decreto 95/2022, Educación Infantil. Disposición adicional primera. Enseñanzas de religión.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-1654#da)
> 2. Las administraciones educativas garantizarán que, al inicio del curso, los padres, las madres, los tutores o las tutoras de los alumnos y las alumnas puedan **manifestar su voluntad de que reciban o no enseñanzas de religión.**  

[Real Decreto 157/2022 Primaria. Disposición adicional primera. Enseñanzas de religión.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-3296#da)  
> 2. Las administraciones educativas garantizarán que, al inicio del curso, las madres, los padres, las tutoras o los tutores de los alumnos y las alumnas puedan **manifestar su voluntad de que estos reciban o no enseñanzas de religión.**  

[Real Decreto 217/2022 ESO. Disposición adicional primera. Enseñanzas de religión.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975#da)  
> 2. Las administraciones educativas garantizarán que, al inicio del curso, los alumnos y alumnas mayores de edad y los padres, madres, tutores o tutoras del alumnado menor de edad puedan **manifestar su voluntad de recibir o no enseñanzas de religión.**  

[Real Decreto 243/2022 Bachillerato. Disposición adicional primera. Enseñanzas de religión](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#da)  
> 2. Las administraciones educativas garantizarán que, al inicio del curso, los alumnos y alumnas mayores de edad y los padres, madres, tutores o tutoras del alumnado menor de edad puedan **manifestar su voluntad de recibir o no enseñanzas de religión.**  



## Planteamiento de escrito a AEPD

Es un texto a ir revisando. En el propio texto incluyo y desarrollo argumentaciones con referencias: según lo vea quizá separe algo para tratarlo por separado en el post

---

**RGPD y enseñanza de Religión en la Educación Pública**

La enseñanza de la religión en el sistema educativo público en España considero que debe ser replanteada ya que no cumple con Reglamento (UE) 2016/679 (RGPD).  

### I. Protección de creencias religiosas como datos sensibles bajo RGPD 

El RGPD, en vigor desde 2018, establece en su artículo 9 que los datos que revelan la ideología, religión o creencias son considerados de categoría especial, "datos sensibles", y su tratamiento está prohibido, salvo en circunstancias excepcionales y bajo condiciones estrictas.

#### I.1. Tratamiento de datos religiosos en educación  

##### I.1.1 Recogida de datos: elección de la materia de religión  

Actualmente los padres de los alumnos pueden realizar la elección de una materia de religión en la educación pública, según [Ley Orgánica 2/2007. Disposición adicional segunda. Enseñanza de la Religión](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#dasegunda)  que en punto 1 para la religión católica remite a [Acuerdo entre el Estado español y la Santa Sede sobre Enseñanza y Asuntos Culturales](https://www.boe.es/buscar/act.php?id=BOE-A-1979-29491) en el que se indica que "Los planes educativos...in­cluirán la enseñanza de la religión católica en todos los Cen­tros de educación... dicha enseñanza **no tendrá carácter obligatorio** para los alumnos. Se garantiza, sin embargo, el derecho a recibirla." y que en punto 2 para otras religiones 2 remite a [Acuerdos de Cooperación celebrados por el Estado español con la Federación de Entidades Religiosas Evangélicas de España](https://www.boe.es/buscar/act.php?id=BOE-A-1992-24853), [la Federación de Comunidades Israelitas de España](https://www.boe.es/buscar/act.php?id=BOE-A-1992-24854), [la Comisión Islámica de España](https://www.boe.es/buscar/act.php?id=BOE-A-1992-24855) y, en su caso, a los que en el futuro puedan suscribirse con otras confesiones religiosas"  

Al indicarse que no tiene carácter obligatorio, debe existir una elección, que se realiza cada curso.  

[Real Decreto 95/2022, Educación Infantil. Disposición adicional primera. Enseñanzas de religión.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-1654#da)
> 2. Las administraciones educativas garantizarán que, al inicio del curso, los padres, las madres, los tutores o las tutoras de los alumnos y las alumnas puedan **manifestar su voluntad de que reciban o no enseñanzas de religión.**  

[Real Decreto 157/2022 Primaria. Disposición adicional primera. Enseñanzas de religión.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-3296#da)  
> 2. Las administraciones educativas garantizarán que, al inicio del curso, las madres, los padres, las tutoras o los tutores de los alumnos y las alumnas puedan **manifestar su voluntad de que estos reciban o no enseñanzas de religión.**  

[Real Decreto 217/2022 ESO. Disposición adicional primera. Enseñanzas de religión.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975#da)  
> 2. Las administraciones educativas garantizarán que, al inicio del curso, los alumnos y alumnas mayores de edad y los padres, madres, tutores o tutoras del alumnado menor de edad puedan **manifestar su voluntad de recibir o no enseñanzas de religión.**  

[Real Decreto 243/2022 Bachillerato. Disposición adicional primera. Enseñanzas de religión](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#da)  
> 2. Las administraciones educativas garantizarán que, al inicio del curso, los alumnos y alumnas mayores de edad y los padres, madres, tutores o tutoras del alumnado menor de edad puedan **manifestar su voluntad de recibir o no enseñanzas de religión.**  

Tal y como está planteado, al ser obligatoria la oferta de religión, se está exigiendo una elección y realizar una manifestación. En ningún punto de la normativa se indica que ante la ausencia de manifestación se opte por una opción. De hecho en algunas administraciones se indica explícitamente la obligación de indicar la preferencia   
https://ias1.larioja.org/boletin/Bor_Boletin_visor_Servlet?referencia=26249261-1-PDF-556197  
> Las solicitudes para cursar la materia de Religión o la alternativa de Escuela de Voluntariado se formalizarán en
el momento en que se realice la matrícula del curso correspondiente ...

##### I.1.2 Categoría del dato recogido: elección de la materia de religión  

La elección de la materia de religión específica revela convicciones de los padres, y es un dato sensible de categoría especial. 

Se puede plantear que el dato recogido no es sensible ya que no refleja necesariamente "las convicciones religiosas" que indica artículo 9.1 RGPD, ya que lo que se recoge es la manifestación de la voluntad de recibir o no enseñanzas de religión, y en caso afirmativo, de qué religión se elige entre las cuatro actualmente posibles (católica, evangélica, judía o islámica).  
Sin embargo, el propio artículo 27.3 de la Constitución Española menciona explícitamente el término convicciones como base para esa elección  
> 3. Los poderes públicos garantizan el derecho que asiste a los padres para que sus hijos reciban la formación religiosa y moral que esté de acuerdo con sus propias convicciones.  

En el caso de centros privados, con o sin concierto, la matriculación supone la aceptación del ideario y va unida con frecuencia a la elección obligatoria de la materia de religión, lo que muestra que la elección de la materia de religión sí refleja convicciones.  

Se podría argumentar que nada impide que los padres puedan manifestar la voluntad de que sus hijos reciban una formación religiosa no acorde a sus convicciones religiosas, como por ejemplo que unos padres con convicciones islámicas indicasen religión católica como elección para su hijo debido a otros motivos (posible efecto académico positivo para su hijo, posible mejor integración de su hijo con el grupo que cursa mayoritariamente religión católica...) pero se puede contraargumentar: 
- En ningún caso la elección en firme de los padres para que su hijo curse una religión concreta deja de suponer mostrar que los padres tienen unas convicciones que les permite la elección de algo tan sensible como la materia de religión concreta que va a cursar su hijo.   
- La oferta de la materia de religión se realiza para garantizar el derecho que asiste los padres para que sus hijos reciban formación religiosa de acuerdo a sus convicciones según artículo 27.3, y por desarrollos legales basados en ello y en acuerdos con confesiones religiosas. Argumentar que el que lo padres manifiesten la voluntad de que sus hijos cursen una materia religiosa no revela las conviciones de los padres supone reconocer la elección no por convicciones, y por lo tanto que el estudio de la materia de religión no se está ofertando en base a su fundamento constitucional por artículo 27.3. 

La propia Conferencia Episcopal asociada a la religión católica [la mayoritaria en educación en España, según se puede ver por ejemplo en datos de Madrid con un 1150/1199=96% del profesorado](https://www.comunidad.madrid/servicios/educacion/personal-docente-centros-publicos-no-universitarios-comunidad-madrid#religion), asocia la elección de la materia de religión a las convicciones de los padres: [Nota de la Comisión Episcopal para la Educación y Cultura sobre las estadísticas de la asignatura de Religión 2022-23](https://www.conferenciaepiscopal.es/nota-comision-episcopal-educacion-cultura-estadisticas-asignatura-religion-2022-23/)  
> las familias siguen optando mayoritariamente por la enseñanza religiosa escolar, lo que pone de manifiesto el valor educativo y formativo de esta asignatura para una parte significativa de la población.   

Para mostrar lo incorrecto de argumentar la categoría no sensible de la elección de la materia de religión, de manera análoga se podría argumentar que el dato de votar a un partido político o participar en un un mitin electoral no refleja "opiniones políticas" ya que solo se refleja la voluntad de realizar el acto realizado sin revelar las convicciones internas, y que por lo tanto no es un dato sensible al que aplique artículo 9.1 RGPD. Sin embargo en 2019 se declaró la inconstitucionalidad sobre el artículo 58.1.bis de LOREG en el que citaba la recopilación de un dato sensible sin concretar su tratamiento, usando la protección de datos en el recurso y sentencia.   
[Resolución adoptada por el Defensor del Pueblo (e.f.), el 4 de marzo de 2019, sobre la interposición de recurso de inconstitucionalidad contra el artículo 58 bis.1 de la Ley Orgánica 5/1985, de 19 de junio, del Régimen Electoral General, incorporado a esta por la disposición final tercera punto dos de la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales](https://www.defensordelpueblo.es/wp-content/uploads/2019/03/Demanda_Recurso_2019.pdf)  
[Pleno. Sentencia 76/2019, de 22 de mayo de 2019. Recurso de inconstitucionalidad 1405-2019. Interpuesto por el Defensor del Pueblo respecto del apartado primero del artículo 58 bis de la Ley Orgánica 5/1985, de 19 de junio, del régimen electoral general, incorporado por la Ley Orgánica 3/2018, de 5 de diciembre, de protección de datos personales y garantía de los derechos digitales. Protección de datos personales, principio de seguridad jurídica, vertiente negativa de la libertad ideológica y derecho a la participación política: nulidad del precepto legal que posibilita la recopilación por los partidos políticos de datos personales relativos a las opiniones políticas de los ciudadanos.](https://www.boe.es/buscar/doc.php?id=BOE-A-2019-9548)  

#### I.1.3 Tratamiento de datos: elección de la materia de religión  

El tratamiento debe cumplir el artículo 5 RGPD Principios relativos al tratamiento, que incluye "la protección contra el tratamiento no autorizado". La organización de alumnos en materias hace inevitable que la información de la elección de materia religiosa sea una información pública para el resto de alumnos del grupo y centro, docentes y resto de comunidad educativa. Los alumnos de un grupo saben qué alumnos de ese grupo van a religión (y a qué tipo de religión si hay varias opciones) y qué alumnos van a alternativa, cuando son datos sensibles que no deberían ser conocidos por nadie. 

Poniendo ejemplos, en un centro público donde la mayoría elige como única opción alternativa a religión, la elección de religión católica que se impartiría necesariamente por un único alumno al no tener ratio mínima puede generar una visibilidad indebida. De la misma manera en un centro público donde la mayoría elige como única opción una religión, la elección de alternativa a religión que se impartirá necesariamente por un único alumno al no estar obligado a cursar religión puede generar una visibilidad indebida.  
En el caso de alumnos de 2º de Bachillerato, donde los acuerdos con la Santa Sede no aplican ya que se redactaron contemplando hasta BUP que equivale a 1º de Bachillerato sin incluir COU que equivale a 2º Bachillerato, los alumnos que no cursan religión directamente no cursan una materia alternativa.  

Es discutible si la exigencia de declarar la eleccion de participar en una asignatura específica, que luego va a ser una información pública al resto de alumnos y comunidad educativa, puede considerarse proporcional y necesaria en el marco de una educación pública que debería ser neutral en términos de creencias.  
Se podría argumentar que la información es pública, de la misma manera que se puede saber si una persona va o no a misa o a ceremonias religiosas de cierta religión mirando en la puerta del lugar de culto, y lo que sería un problema sería su difusión: precisamente se produce una difusión de esa información en el ámbito educativo.   

Un posible tratamiento del dato recogido son las actividades extraescolares que realizan los alumnos, que a veces se organizan en función de las materias que cursan, incluyendo religión o la materia alternativa. En este caso la información está saliendo del centro educativo, y una entidad externa puede saber que tiene un grupo que es "los alumnos de cierto centro que cursan cierta religión".  

Otro ejemplo de tratamiento es el traslado del alumno a otro centro en otra administración (otra comunidad autónoma o incluso otro país): es habitual facilitar una copia del expediente académico en el que se indica qué materia de religión está cursando el alumno. Se incluye la materia de religión en el historial académico cuando no tiene efectos académicos.   
https://www.boe.es/buscar/act.php?id=BOE-A-2015-4392#a3  
> 2. El historial académico, y en su caso el informe personal por traslado, se consideran documentos básicos para garantizar la movilidad del alumnado por todo el territorio nacional.  

#### I.2. Consentimiento libre

El consentimiento para el tratamiento de datos personales debe ser libre, específico, informado e inequívoco según artículo 7 de RGPD.  

En el caso de centros privados, con o sin concierto, tienen derecho a tener un ideario, en el que podrían indicar valores religiosos y la obligatoriedad de cursar cierta materia de religión en concreto, sin dar opción a cursar alternativa a religión. No hay consentimiento libre al cursar religión porque se asume un consentimiento libre de la elección de centro por los padres y por lo tanto una aceptación de su ideario (se puede ver por ejemplo en artículo 10 de acuerdos con religiones evangélicas, judías e islámicas se indica indica "siempre que, en cuanto a estos últimos (privados concertados), el ejercicio de aquel derecho no entre en conflicto con el carácter propio del centro"). Pero la elección de materia religiosa o alternativa sí se garantiza de forma inicialmente libre en centros públicos.  

En la situación actual la administración está obligando a realizar una elección: el "consentimiento" para declarar convicciones religiosas puede no cumplir con estos requisitos ya que puede generar una presión indebida sobre el menor y su familia, violando así el principio de consentimiento libre. Se han comentado en I.1.3 ejemplos en los que se puede genera una visibilidad y exposición indebida en el ámbito educativo en base a la elección e incluso puede generar situaciones de presión para cursar una materia de religión distinta a la de las convicciones.  

Además según artículo 7 RGPD, el interesado tendrá derecho a retirar su consentimiento en cualquier momento, y será tan fácil retirar el consentimiento como darlo. Sin embargo el consentimiento se obliga a dar para todo un curso académico, sin que sea posible, entre otros por motivos organizativos como puede ser profesorado y espacios, que se cambie la manifestación durante el curso.  

Por poner una analogía, una persona que marca la casilla en la declaración IRPF sobre la iglesia católica, no necesariamente está declarando que es católico, pero eso no implica que ese dato pueda ser conocido de ninguna manera por nadie. Si marcarlo implicase hacer público lo que ha elegido, no se consideraría viable implementarlo aunque no sea formalmente indicar que se es católico: forzando la analogía, nadie plantearía atender presencialmente en las oficinas IRPF según cómo se ha marcado la casilla si eso haría innecesariamente visible al resto esa elección.  

#### I.3 Licitud y responsable de tratamiento

La administración educativa toma y trata los datos de elección de materia religiosa amparándose en artículo 6.1.c 6.1.e ya que la normativa actual obliga a la administración a impartir la materia de religión en centros públicos, usando como fundamento a artículo 27.3 que refleja que los padres tienen derecho a que sus hijos reciban formación religiosa acorde a sus propias convicciones.  

Pero en ningún momento artículo 27 indica que esa formación religiosa se deba realizar mediante una materia de religión y en los centros educativos públicos: la formación puede ser en ámbitos privados, que aparte del hogar pueden ser iglesias, sinagogas, mezquitas o centros educativos privados, que sí están amparados en artículo 9.2.d de RGPD y en un consentimiento vía 6.1.a y en artículo 91.  

### II. Aconfesionalidad y neutralidad del Estado en el artículo 16 de la Constitución Española  

El artículo 16 de la Constitución Española establece el principio de aconfesionalidad del Estado, prohibiendo que ninguna confesión tenga carácter estatal. Si bien en 16.3 se indica que los poderes públicos deben tener en cuenta las creencias religiosas de la sociedad española, tenerlas en cuenta implica que los poderes públicos deben conocer las creencias religiosas de la sociedad de manera objetiva, lo que implica necesariamente que la mayoría de la sociedad debe indicarlas, lo que es inconsistente con 16.2 en el que se indica que nadie puede ser obligado a declarar sobre sus creencias.  

La aconfesionalidad del Estado le obliga a mantener una posición de neutralidad en materia religiosa y garantizar que nadie esté obligado a revelar públicamente sus creencias religiosas.  

#### II.1. Obligación de declaración de creencias de los padres

La exigencia de elegir una materia de religión concreta obliga a declarar convicciones religiosas de manera implícita como se a argumentado, lo que puede contravenir la neutralidad religiosa que el Estado debe mantener según el artículo 16.2 de la Constitución que indica que nadie podrá ser obligado a declarar una creencia religiosa. 

#### II.2. Impacto en la libertad de conciencia y el desarrollo del menor  

La obligación de asistir a clases de religión o de elegir entre religión y una asignatura alternativa puede estigmatizar a los alumnos en función de su creencia o no creencia y el contexto, afectando su derecho a la igualdad y no discriminación.  

Se han comentado en I.1.3. ejemplos en los que se puede genera una visibilidad y exposición indebida en el ámbito educativo en base a la elección e incluso puede generar situaciones de presión para cursar una materia de religión distinta a la de las convicciones. Esto puede influir negativamente en el desarrollo de la libertad del menor.

Hablando del interés superior del menor se puede citar esta sentencia del Tribunal Constitucional STC 26/2024  

[Pleno. Sentencia 26/2024, de 14 de febrero de 2024. Recurso de amparo núm. 4958-2021. Promovido por doña N.C.R., en relación con los autos dictados por la Audiencia Provincial de Barcelona y un juzgado de lo civil de esa capital, sobre escolarización de una menor de edad. Vulneración del derecho a que los hijos reciban una formación religiosa y moral acorde con las convicciones de los padres: resoluciones judiciales que acuerdan la escolarización de una menor en un centro concertado religioso que no puede entenderse justificada en su interés superior en el seno de una familia con convicciones religiosas divergentes y que no salvaguardan su derecho a desarrollar sus propias convicciones y creencias en un contexto escolar libre de adoctrinamiento. Votos particulares.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2024-5837)  

> En definitiva, las resoluciones judiciales han soslayado el verdadero conflicto de derechos fundamentales de los padres y, ante el desacuerdo entre ellos, no han identificado correctamente el objeto del debate, que no era otro sino el conflicto entre los derechos fundamentales de ambos progenitores reconocido en el art. 27.3 CE, que ha quedado desplazado por una comparación entre las prestaciones ofrecidas por cada centro educativo, ni tampoco ha acertado al identificar el interés superior de la menor, principio de necesaria observancia siempre. **En este caso, vista la todavía inmadurez de la afectada para el pleno ejercicio de la libertad religiosa, su interés superior debió identificarse con la obligación de atender a que sus convicciones religiosas pudieran formarse o adquirirse sin predeterminaciones escolares, esto es, en un entorno docente neutral desde una perspectiva religiosa**. En su lugar, los órganos judiciales en atención a criterios ajenos al señalado interés superior de la menor se han decantado por atribuir la facultad de elegir centro escolar al progenitor favorable a la educación en un concreto sistema de creencias religiosas (educación católica). Esta decisión no puede entenderse justificada en el interés superior de la menor a que, en el seno de una familia con convicciones religiosas divergentes, **pueda ir desarrollando sus propias convicciones y creencias en un contexto escolar libre de adoctrinamiento.**

Según [Ley Orgánica 1/1996. Artículo 2. Interés superior del menor](https://www.boe.es/buscar/act.php?id=BOE-A-1996-1069#a2)  
> 2. A efectos de la interpretación y aplicación en cada caso del interés superior del menor, se tendrán en cuenta los siguientes criterios generales, sin perjuicio de los establecidos en la legislación específica aplicable, así como de aquellos otros que puedan estimarse adecuados atendiendo a las circunstancias concretas del supuesto:  
>...  
> d) La preservación de la identidad, cultura, **religión, convicciones**, orientación e identidad sexual o idioma del menor, así como la no discriminación del mismo por éstas o cualesquiera otras condiciones, incluida la discapacidad, garantizando el desarrollo armónico de su personalidad.  

[Ley Orgánica 1/1996. Artículo 6. Libertad ideológica](https://www.boe.es/buscar/act.php?id=BOE-A-1996-1069#a6)  
> 1. El menor tiene derecho a la libertad de ideología, conciencia y **religión.**  

### III. Resumen y conclusión

La enseñanza de la religión en la educación pública, tal y como está estructurada en España plantea problemas:  
- Obliga a los padres a expresar convicciones para elegir el estudio de una materia religiosa para sus hijos  
- Implica un tratamiento de datos personales sensibles que podría no cumplir con RGPD.  
- Contradice la Constitución Española: o bien artículo 16.2 al obligar a declarar sobre creencias, o contradice 27.3 al permitir elegir materia religiosa sin que esté basado en las creencias.  

Considero que se debe replantear la enseñanza de la religión en las escuelas públicas para asegurar que se respeten plenamente los derechos fundamentales de los menores y sus familias en relación con la protección de datos y la libertad religiosa.  

La idea básica es que artículo 27.3 refleja que los padres tienen derecho a que sus hijos reciban formación religiosa acorde a sus propias convicciones, pero no indica que esa formación religiosa se deba realizar en horario lectivo, mediante una materia de religión y en los centros educativos públicos (solo lo indican los acuerdos con religiones): la formación puede ser fuera del horario lectivo en ámbitos privados, que aparte del hogar pueden ser iglesias, sinagogas, mezquitas o centros educativos privados, que sí están amparados en RGPD.

Garantizar el cumplimiento de RGPD y el tratamiento de religión en educación pública debería suponer que se realice un juicio de proporcionalidad, validando que el tratamiento es adecuado para la finalidad buscada sin que haya otras formas menos intrusivas, y que tratamiento proporciona más beneficios que inconvenientes para el interés del menor. En función de ese juicio de proporcionalidad, se podría proponer que se realicen los cambios normativos que hagan que la formación religiosa cumpliendo con RGPD no se haga como materia y en horario lectivo en los centros públicos.

---

Presentado como escrito  O00007128e24P0012935 el 9 septiembre 2024  
A través de [trámites ciudadano en sede electrónica AEPD](https://sedeagpd.gob.es/sede-electronica-web/vistas/infoSede/tramitesCiudadano.jsf)  
[Registro inicial](https://sedeagpd.gob.es/sede-electronica-web/vistas/formProcedimientoEntrada/procedimientoEntrada.jsf?coe=c)  
[Escrito](https://sedeagpd.gob.es/sede-electronica-web/vistas/formProcedimientoEntrada/procedimientoEntradaAmpliado.jsf)  


## Respuesta AEPD

Recibida 12 septiembre 2024, en solo 3 días.  
[2024-09-12-150739584022ComunicacionConsulta-anonimizado-SinCsv.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2024-09-12-150739584022ComunicacionConsulta-anonimizado-SinCsv.pdf)  

> Por tanto, la LOE legitima a los centros educativos para recabar y tratar los datos de
los alumnos y de sus progenitores, incluyendo también las categorías especiales de
datos, como serían los datos personales salud o de religión, cuando fuesen necesarios
para el desempeño de la función docente y orientadora.  
A este respecto también hay que tener en cuenta una serie de cautelas ya que los
datos personales no podrán usarse para fines diferentes al educativo (función docente
y orientadora) y el profesorado y resto del personal que acceda a los datos personales
de los alumnos o de sus familias, está sometido al deber de confidencialidad recogido
en el art. 5 LOPDGDD.  
Además, a lo anteriormente señalado se debe añadir el criterio de esta Agencia en
relación con el tema planteado en el documento que nos adjunta, que establece que:  
Así, el hecho mismo de cursar la asignatura de religión no revela necesariamente que
el estudiante profese las creencias a las que tal asignatura se refiere, del mismo modo
que el hecho de no cursarla no revela la inexistencia de esas creencias, sino que tal
circunstancia puede deberse al estudio de la religión en otros foros distintos del
escolar. Es decir, lo único que revela el dato de optar por cursar la asignatura de
religión sería el interés del alumno por conocer los principios, historia y preceptos de la
misma, sin que ello implique una efectiva confesionalidad del mismo, a cuya
declaración no podría encontrarse obligado.  
Por este motivo, el dato relacionado con el hecho de que el alumno curse la
asignatura de religión, no vinculada a la participación del alumno en un rito
relacionado con una religión determinada (lo que sí implicaría que el individuo profesa
dicha creencia religiosa) no puede ser considerado por sí mismo un dato que revele
inmediatamente las creencias religiosas del afectado, por lo que su régimen no tendría
la naturaleza de especialmente protegido (categoría especial de dato).  



Implica reconocer:  
- Que no está obligado a indicar sus creencias religiosas.  
> a cuya declaración no podría encontrarse obligado.  

- Que participar en un rito religioso sí implica profesar la creencia religiosa  
> la participación del alumno en un rito relacionado con una religión determinada (lo que sí implicaría que el individuo profesa dicha creencia religiosa)  

Que combinados supone que no puede obligarse a participar en ritos (misa, comunión) en el centro educativo y me lleva a pensar en casos como este (artículo julio 2024)  

[La Xunta avaló que un colegio concertado financiado con dinero público obligue a los alumnos a ir a misa](https://www.eldiario.es/galicia/xunta-avalo-colegio-concertado-financiado-dinero-publico-obligue-alumnos-misa_1_11510846.amp.html)  




