+++
author = "Enrique García"
title = "Muface"
date = "2024-10-09"
tags = [
    "muface", "funcionarios", "clases pasivas"
]
toc = true

+++

Revisado 2 enero 2025

## Resumen

Al hablar de MUFACE creo que se suele desconocer la realidad, es un tema que genera polémica al haber intereses y hay defensores y detractores. Intento documentar de manera objetiva MUFACE; he trabajado en la empresa privada y ahora soy funcionario y creo que puedo dar una visión distinta a la habitual de gente que solo ha trabajado en la empresa privada y a la habitual de gente que solo ha sido funcionario.  
Opino que que dar a funcionarios públicos la posibilidad de usar sanidad privada pagada con dinero público no tiene sentido.  
Este tema de MUFACE me costó en redes que me bloquease una cuenta que opinaba igual que yo pero que no entendía que era mutualista obligado y creo que refleja que suele haber polarización sin atender a datos ni a argumentos.  


 * [MUFACE: coste](../muface-coste)

## Detalle

Hay mucho que contar sobre MUFACE, y al mismo tiempo mucho que desmentir sobre MUFACE. Intento comentar ideas que puedo reordenar o separar si el post crece mucho. Inicialmente separo el tema del coste.

### Ideas resumen rápido sobre qué sí es y qué no es MUFACE

* MUFACE no aplica a todos los funcionarios: solo a los civiles  
> Por separado está Instituto Social de las Fuerzas Armadas (ISFAS), asociado al defensa, y la Mutualidad General Judicial (MUGEJU), asociada a justicia.

* MUFACE no es una elección de los funcionarios: los funcionarios están obligados a estar en MUFACE  
> No es correcto decir que se elige MUFACE  

* MUFACE no es sanidad privada: los funcionarios sí pueden elegir prestador de servicios sanitarios, y el prestador puede ser el Sistema Nacional de Salud (SNS)  
> No es correcto decir que tener MUFACE es tener sanidad privada

* MUFACE supone cuota obligatoria para un funcionario aunque elija como prestador de servicios SNS.

* MUFACE no es una sanidad privada que se financie solo con la aportación de los mutualistas. Las aportaciones de los mutualistas en 2023 suponen el 18,57 % del presupuesto, del que el 83,33% se dedica a la asistencia sanitaria y prestación farmacéutica  

* MUFACE no es estar en clases pasivas: desde 2011 ley obliga a nuevos funcionarios a cotizar en Régimen General de la Seguridad Social, pero antes había otro régimen llamado de clases pasivas que supone a "funcionarios antiguos" mecanismos distintos por ejemplo para la jubilación.  

* MUFACE no tiene 1,5 millones de funcionarios mutualistas: tiene 1 millón de mutualistas y 500 mil beneficiarios

### Fuentes de datos sobre MUFACE

Se pueden ver datos en [Memorias muface](https://www.muface.es/muface_Home/muface/Transparencia/informacion-institucional-organizativa-planificacion/memoria.html)  

Hay algunos artículos (por ejemplo Newtral) que dan datos y describen muface

Los defensores usan datos de IDIS

**17 septiembre 2021**  
[Twitter JotaEfe_91/status/1438811065610080259](https://x.com/JotaEfe_91/status/1438811065610080259)  
Puedo entender la envidia que genera esto de Muface. Pero analicemos los datos:  
Eliminar Muface supondría (Datos del IDIS)  
-⬆️de costes para la Sanidad: 80M  
-⬇️de recaudación: 3M  
-⬇️35mil empleos  
-⬆️33 días lista de espera  
-⬆️15% ocupación de camas  
Pero, ¿Qué es MUFACE?➡️  

[Quiénes somos - idis](https://www.fundacionidis.com/sobre-idis/quienes-somos)  
> La Fundación Instituto para el Desarrollo e Integración de la Sanidad (IDIS), es una entidad constituida en mayo de 2010, que aglutina al sector sanitario privado y que tiene como finalidad promover la mejora de la salud de los ciudadanos y defender el sistema sanitario español en su conjunto, a través de la puesta en valor de la aportación de la sanidad privada.

El informe tiene un conflicto de intereses, y se actualiza expresamente para no cuestionar la sanidad privada

[Septiembre 2024 Impacto de las mutualidades en el Sistema Sanitario - fundacionidis](https://www.fundacionidis.com/uploads/informes/Impacto_de_las_mutualidades_en_el_Sistema_Sanitario_v06.pdf)   
> El objetivo de este informe es realizar un ejercicio
teórico para el análisis de los impactos de las
potenciales repercusiones que, bajo determinadas
hipótesis, podrían existir en España en el caso de que
la inviabilidad del modelo conllevase la
desaparición de la posibilidad de elección, es decir,
la cobertura de las mutualidades sea 100% pública.

[El mutualismo administrativo: modelo predictivo sobre la elección de los mutualistas de su modelo sanitario y escenarios futuros. Diciembre 2023 - ucm.es](https://docta.ucm.es/entities/publication/d977bd12-70fd-44b2-a897-e01f02216bc4)  
Que tiene un conflicto de intereses ...  
> La elaboración de este informe se enmarca entre los objetivos de la Cátedra Extraordinaria de Salud Sostenible y Responsable, puesta en marcha durante 2022 por la Universidad Complutense de Madrid (UCM) y la **Alianza de la Sanidad Privada Española (ASPE)**

Convenios entre MUFACE y las entidades de prestación sanitarias privadas  
[Resolución de 22 de diciembre de 2021, de la Mutualidad General de Funcionarios Civiles del Estado, por la que se publica el Concierto suscrito con entidades de seguro para el aseguramiento del acceso a la asistencia sanitaria en territorio nacional a los beneficiarios de la misma durante los años 2022, 2023 y 2024.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-21337)  


[Resolución de 21 de marzo de 2018, de la Subsecretaría, por la que se publica el Convenio entre la Mutualidad General de Funcionarios Civiles del Estado y el Instituto Nacional de la Seguridad Social, para el aseguramiento del acceso a la asistencia sanitaria en territorio nacional a los beneficiarios de la misma y la integración de la información.](https://boe.es/boe/dias/2018/03/23/pdfs/BOE-A-2018-4115.pdf)  

[Resolución de 28 de febrero de 2022, de la Subsecretaría, por la que se publica la Adenda de prórroga y modificación al Convenio entre la Mutualidad General de Funcionarios Civiles del Estado y el Instituto Nacional de la Seguridad Social, para el aseguramiento del acceso a la asistencia sanitaria en territorio nacional a los beneficiarios de la misma y la integración de la información](https://www.boe.es/boe/dias/2022/03/03/pdfs/BOE-A-2022-3401.pdf)  

### Visión histórica de MUFACE

Se crea en 1975

[Quiénes somos - muface](https://www.muface.es/muface_Home/muface/quienesSomos.html)  
> La Mutualidad General de Funcionarios Civiles del Estado (MUFACE) es el organismo público encargado de prestar asistencia sanitaria y social al colectivo de funcionarios adscritos. Se crea por la Ley 29/1975, de 27 de junio, sobre Seguridad Social de los Funcionarios Civiles del Estado, con la finalidad de gestionar el sistema de Mutualismo Administrativo de los funcionarios civiles del Estado..

Esa [Ley 29/1975, de 27 de junio, sobre Seguridad Social de los Funcionarios Civiles del Estado](https://www.boe.es/buscar/doc.php?id=BOE-A-1975-13887) se deroga por [Real Decreto Legislativo 4/2000, de 23 de junio, por el que se aprueba el texto refundido de la Ley sobre Seguridad Social de los Funcionarios Civiles del Estado.](https://www.boe.es/buscar/act.php?id=BOE-A-2000-12140)  

En el preámbulo de 1975 se citan las Clases Pasivas, que se remite a 1926!

> El Sistema español de Clases Pasivas, cuyos orígenes se remontan al Estatuto de veintidós de octubre de mil novecientos veintiséis, y que tras numerosas modificaciones y reformas ha cristalizado en el texto refundido de veintiuno de abril de mil novecientos sesenta y seis, reformado en parte por la Ley diecinueve/mil novecientos setenta y cuatro, de veintisiete de julio, constituye una institución de gran tradición y honda raigambre en nuestra Función Pública. Tal sistema, gestionado de forma directa por el Estado y cuya financiación recae esencialmente sobre el mismo, no es fácilmente reconducible a mecanismos típicamente asegurativos; al menos de un modo radical o inmediato. La presente Ley parte, por consiguiente, de tal realidad y pretende su complementación y perfeccionamiento mediante la implantación de un sistema renovado de Mutualismo Administrativo.

En este artículo de 2021 se comentan antecedentes y orígenes, y cita la Edad Media!  
[Muface, la mutua de los funcionarios, pagará más de 3.500 millones de euros a las aseguradoras hasta 2024 - newtral](https://www.newtral.es/muface-aseguradoras-empresas-concierto/20211010/)  
> Antecedentes y orígenes del sistema de mutualidades  
El régimen de protección de los funcionarios se remonta a las primeras entidades organizadas como sistemas mutuales en la Edad Media, que darían lugar a las hermandades de socorro y a los montepíos. Sin embargo, no es hasta principios del siglo XX cuando se promulga la Ley de Accidentes de Trabajo y se crean las mutuas de accidentes.  
En 1941 se aprueba la Ley de Mutualidades que perdurará hasta 1975. En ella, se consideraban mutualidades o montepíos a las asociaciones encaminadas a proteger a sus asociados “contra acontecimientos de carácter fortuito y previsible a los que están expuestos mediante sus aportaciones directas o procedentes de otras entidades protectoras”.  
No obstante, esto suponía una diferencia de aportaciones y de prestaciones entre los funcionarios, además de que muchos empleados públicos no tenían cobertura de ninguna mutualidad. Ya con la Ley de Bases de la Seguridad Social de 1963 queda establecido que entre los regímenes especiales que se permiten se incluya el de funcionarios.  
Tomando como referencia la Mutualidad Nacional de Previsión de la Administración Local (Munpal), que aglutinaba a todos los trabajadores de los entes municipales, surge la necesidad de agrupar las mutualidades y homogeneizarlas. Así, en 1975 se publica la Ley de Seguridad Social de las Fuerzas Armadas, creadora del Isfas, la Ley de Seguridad Social de los Funcionarios Civiles del Estado, creadora de Muface; y tres años más tarde la Mutualidad General Judicial.  

Se puede pensar que esto es algo muy antiguo, pero en este artículo de MUFACE de 2023 se habla de mutualidades preexistentes a 1975

[Envío de certificados a mutualistas del Fondo Especial - muface](https://www.muface.es/muface_Home/muface_comunicacion/hemeroteca-noticias/2023/Noviembre-2023/Fondo-especial-certificados.html)  
> MUFACE ha puesto en marcha una campaña de envío de certificados a las personas mutualistas que pertenecieron a alguna de las **Mutualidades preexistentes a MUFACE** y que quedaron integradas en el organismo actual, conformando el denominado Fondo Especial (por ejemplo, el Montepío Cuerpo General de Policía o la Mutualidad de Enseñanza Primaria).  
El objeto de este certificado es que estos mutualistas puedan acreditar que pertenecieron a dichas mutualidades, dado que la reciente sentencia 707/2023 del Tribunal Supremo ha reconocido el derecho de muchos pensionistas que cotizaron en las antiguas mutuas laborales a recuperar parte de las cuotas declaradas en la tributación al IRPF entre el 1 de enero de 1967 y el 31 de diciembre de 1978.


### Número de mutualistas en MUFACE

**8 diciembre 2021**

En [memoria 2022 pdf](https://www.muface.es/dam/jcr:2985f319-6b76-4244-a1ff-6058d843cc9e/Memoria_Muface_2022.pdf)  
> 3.1 Caracterización de las personas mutualistas  
> Las personas mutualistas y sus beneficiarias y beneficiarios forman un colectivo
que, a 31 de diciembre de 2022 asciende a 1.496.276 personas, de las cuales
1.053.934 son titulares y 442.342 son beneficiarias.

Si se mira la evolución  
> Figura 10. Tendencia del colectivo de MUFACE desde el año 1976 hasta 2022  

Aunque la figura tiene mal los valores en eje horizontal, se ve que cada división es de 1 año y que desde 1986 aproximadamente el total de titulares + beneficiarios está en 1,5 M ± 1 M€ y se puede decir "1,5 M" de manera aproximada 

En [memoria 2023 pdf](https://www.muface.es/dam/jcr:7770da43-c118-419b-9619-0283022c012f/MUFACE_MEMORIA_2023_WEB.pdf)  
>3.1. Caracterización de las Personas Mutualistas  
A 31 de diciembre, había un colectivo mutualista de 1.537.701 personas,
de las cuales 1.103.292 eran titulares y 434.409 beneficiarias. Estas cifras
son prueba evidente de la fortaleza del sistema mutualista, porque los
titulares, que son los que soportan el modelo, son más de dos tercios
del total.  
En comparación con 2022, en 2023 se experimentó un incremento neto
de 41.425 personas mutualistas, resultado del aumento del número de
titulares (49.358), y un descenso de personas beneficiarias (7.933)

En general se habla de 1,5 millones: a veces se parecen dar cifras mayores porque se incluye ISFAS (militares) y MUGEJU (justicia), que no son MUFACE. 

[El Gobierno planea mejorar a las aseguradoras para salvar el convenio de Muface - elpais](https://elpais.com/economia/2024-10-08/el-gobierno-se-abre-a-un-gesto-con-las-aseguradoras-para-salvar-el-convenio-de-muface.html)  
El Consejo de Ministros aprobará hoy el pliego de condiciones para la sanidad concertada de **2,14 millones** de funcionarios y familiares

En [El mutualismo administrativo: modelo predictivo sobre la elección de los mutualistas de su modelo sanitario y escenarios futuros. Diciembre 2023 - ucm.es](https://docta.ucm.es/entities/publication/d977bd12-70fd-44b2-a897-e01f02216bc4) se comentan ideas en el resumen ejecutivo sobre características de los mutualistas  

> Los mutualistas titulares tienen una
edad media de 57,2 años, edad que se
ha incrementado en 3 años en el perio-
do objeto de estudio (2013-2022).  
> El 61,7% de los mutualistas están en
activo, mientras que el 38,3% restante
son mutualistas jubilados. El porcen-
taje de los mutualistas jubilados se ha
incrementado en 9,2 puntos porcentua-
les en los últimos 10 años, pasando de
representar el 29,1% en 2013 al 38,3%
en 2022

Habla del envejecimiento del funcionariado, relacionado con que con mayor edad las empresas sanitarias privadas no dan cobertura a patologías que sí da la sanidad pública  

> A pesar del deterioro que en los últimos años
viene sufriendo la sanidad pública y de su con-
secuente aumento de las listas de espera, el por-
centaje de mutualistas de MUFACE que optan
por la asistencia privada se ha reducido en 9,2
puntos porcentuales en los últimos diez años, ha-
biendo pasado de representar el 81,9% en 2013 al
72,9% en 2022  

**24 junio 2024**  
[El Consejo General de MUFACE aprueba la Memoria 2023](https://www.muface.es/muface_Home/muface_comunicacion/hemeroteca-noticias/2024/Junio-2024/Segundo-Consejo-General-de-MUFACE-de-2024.html)  
> Entre los principales hitos logrados por la Mutualidad en el pasado ejercicio, destacan:  la cifra récord alcanzada por el colectivo mutualista en 2023: 1.537.701 personas, de las cuales 1.103.292 son titulares y 434.409 son beneficiarias


### Obligación de estar en MUFACE

En [memoria 2022 pdf](https://www.muface.es/dam/jcr:2985f319-6b76-4244-a1ff-6058d843cc9e/Memoria_Muface_2022.pdf)  
> Tabla 14. Distribución de mutualistas por situación laboral  

Se puede ver que hay 1.053.934 mutualistas, de los cuales la mayoría son **mutualistas obligatorios** en activo o jubiliados: solo 0,10% son voluntarios.  

En junio 2020 planteo esta consulta a muface  
> Desearía saber si un funcionario de carrera de educación con destino en Madrid puede renunciar a ser afiliado, y si es así, cuál es el procedimiento, teniendo solamente Seguridad Social. No hablo de cambiar de entidad prestadora, desde el principio tengo Seguridad Social.

Respuesta a su consulta Nº 0000855598 realizada el día 15/06/2020  ref:_00D208Z5p._5000J1iQR78:ref 

> Estimado/a mutualista,  
La pertenencia obligatoria a un Régimen de Seguridad Social viene determinada por el Cuerpo de adscripción del funcionario/a.  
Muface gestiona la asistencia sanitaria de los funcionarios de carrera de la Administración Civil del Estado y de  los funcionarios en prácticas que aspiren a ingresar en los Cuerpos de dicha Administración.  
Los funcionarios de carrera/prácticas del Cuerpo de Profesores de Enseñanza Secundaria está incluido en el campo de aplicación de Muface, por lo que en su caso al figurar como funcionario adscrito a ese Cuerpo, obligatoriamente debe estar de alta en Muface.  
Causará baja como mutualista obligatorio el funcionario que pase a la situación de excedencia voluntaria en cualquiera de sus modalidades y el  funcionario que pierda su condición, cualquiera que sea la causa.  
Atentamente  
Muface - Oficina de Información al Mutualista

Parece que hay casos de funcionarios en los que no era obligatorio ser mutualista. Ver en la normativa citada de 2017 "En el caso de que deseen mantener su condición de mutualistas, deberán ejercitar esta opción..."

En el proceso de alta inicial al ser nombrado funcionario en prácticas no hay opción de no elegir muface

[Funcionarios en prácticas direcciones de área](https://www.educa2.madrid.org/web/direcciones-de-area/funcionarios-en-practicas2)  

> 2) A los efectos de alta afiliación a MUFACE:  
La afiliación a MUFACE es **obligatoria** con independencia de que luego elijan como prestación sanitaria entre Seguridad Social o Entidades Privadas Concertadas.  
Deberán tramitar el alta **a partir del 1 de septiembre, TODOS** aquellos que, superado el procedimiento selectivo, hayan sido seleccionados como funcionarios en practicas, a estos efectos disponen de toda la información en el siguiente enlace:  
[MUFACE nuevos afiliados](https://www.muface.es/muface_Home/muface_comunicacion/hemeroteca-noticias/2024/Agosto-2024/Incorporaci-n-masiva-de-docentes-de-ense-anza-no-universitaria-a-MUFACE-en-septiembre-de-2024-.html)   
Para afiliarse a MUFACE es recomendable disponer de certificado digital, DNI-e o Cl@ve PIN.  
La fecha de incorporación al centro de los funcionarios en practicas será el 1 de septiembre. Aquellos que NO PUEDAN INCORPORARSE, deberán comunicarlo debidamente justificado a la mayor brevedad posible tanto por Registro Telemático, como por email a datsurpersonal@madrid.org con el asunto “Funcionario en Prácticas”.  


**13 septiembre 2017**

[El Gobierno extiende el seguro de Muface a más funcionarios - eleconomista](https://www.eleconomista.es/amp/8603515/El-Gobierno-extiende-el-seguro-de-Muface-a-mas-funcionarios)  
> El Gobierno, a través del Ministerio de Hacienda y Administraciones Públicas, ha llegado a un acuerdo con los principales sindicatos de la función pública para extender esta cobertura tanto a los funcionarios que son transferidos a las Comunidades Autónomas como a aquellos que por promoción interna acceden a Cuerpos o Escalas que no tienen esta cobertura.  
Además, todos aquellos funcionarios que habían perdido en el pasado la posibilidad de esta póliza por estos dos motivos podrán recuperarla ahora, ya que la medida se aplicará de manera retroactiva. Los funcionarios de la Administración Civil del Estado que se encuentren en esta situación tienen hasta el 31 de diciembre de este año para solicitar la recuperación de la póliza para el año que viene.  
El derecho a elegir aseguradora privada de Muface por parte del funcionario que promocionaba de cuerpo o de Administración se ha hecho realidad en la disposición final 6º de la Ley General de Presupuestos Generales del Estado 2017.

[Disposición final sexta. Modificación del texto refundido de la Ley sobre Seguridad Social de los Funcionarios Civiles del Estado, aprobado por Real Decreto Legislativo 4/2000, de 23 de junio.](https://www.boe.es/buscar/act.php?id=BOE-A-2017-7387#df-7)



### Porcentaje de mutualistas con prestador de asistencia sanitaria privado ó SNS

[El 75% de los mutualistas de Muface, Isfas y Mugeju elige la opción concertada en la asistencia sanitaria](https://www.newtral.es/mutualidades-prefieren-sanidad-concertada/20211208/)  

En [memoria 2022 pdf](https://www.muface.es/dam/jcr:2985f319-6b76-4244-a1ff-6058d843cc9e/Memoria_Muface_2022.pdf)  
> 4.1 Prestaciones sanitarias  
Tabla 16. Distribución de personas mutualistas y beneficiarias por prestador  

Las entidades privadas suponen 73,21% (1.095.393 incluyendo beneficiarios) y INSS supone 26,79% (400.813 incluyendo beneficiarios) 

**19 septiembre 2022**

[El número de funcionarios que deja la sanidad privada de Muface para ir a la pública crece a un ritmo de más de 10.000 al año - elpais](https://elpais.com/sociedad/2022-09-19/el-numero-de-funcionarios-que-deja-la-sanidad-privada-de-muface-para-ir-a-la-publica-crece-a-un-ritmo-de-mas-10000-al-ano.html)  
> La primera investigación con datos sobre hospitalizaciones del colectivo revela que buena parte de los que toman la decisión son pacientes con enfermedades graves y personas mayores 

**9 octubre 2024**

[Cada vez más funcionarios rechazan el seguro privado de Muface: “Siendo trabajadora pública, es lo coherente” - eldiario](https://www.eldiario.es/economia/vez-funcionarios-rechazan-seguro-privado-muface-trabajadora-publica-coherente_1_11718889.html)  
> El número de funcionarios que elige la sanidad pública frente a las aseguradoras privadas finaciadas por el Estado sube un 66% en 10 años y son ya uno de cada tres  

Tiene gráficas interesantes, desglosando por CCAA

Para 1 enero 2023 indica 457307 en SNS, e indica fuente Muface. La memoria de 2023 no es oficial en octubre 2024. 

La evolución parece combinar envejecimiento del funcionariado y elección de nuevos funcionarios, y haría falta más detalle en los datos para analizarlo; en 2024 intento conseguir datos desglosando por tramos de edad.

**13 noviembre 2024**  

[Los funcionarios más jóvenes optan masivamente por la sanidad pública y precipitan la crisis de Muface - elpais](https://elpais.com/sociedad/2024-11-13/los-funcionarios-mas-jovenes-optan-masivamente-por-la-sanidad-publica-y-precipitan-la-crisis-de-muface.html)  
> Dos de cada tres nuevos mutualistas rechazan los seguros médicos privados, que pierden así ingresos que les permitirían compensar la asistencia a la población más envejecida y con mayor gasto sanitario

**19 noviembre 2024**  
[MUFACE: del seguro privado al Sistema Nacional de Salud Ministerio de Sanidad Noviembre 2024.](https://www.sanidad.gob.es/estadEstudios/estadisticas/sisInfSanSNS/pdf/Informe_transicion_MUFACE_al_SNS.pdf)  

Gráfica 1. Distribución por edad de la población mutualista con aseguradora sanitaria privada a 31 de
octubre de 2024. Fuente: Ministerio de Sanidad 

Tabla 2. Diferencial de distribución por grupos de edad entre la población mutualista de MUFACE con
aseguradora sanitaria privada y la población del Sistema Nacional de Salud.

**26 noviembre 2024**  
[40.000 médicos autónomos dispuestos a tomar las riendas de MUFACE, sin aseguradoras, en 600 hospitales](https://amp.epe.es/es/sanidad/20241126/sanidad-privada-muface-40-000-medicos-autonomos-dispuestos-riendas-aseguradoras-hospitales-112056546)  
La Asociación Unión Médica Profesional, que representa a 10.000 facultativos, ha conseguido que el tribunal de los contratos públicos suspenda las licitaciones sanitarias de ISFAS y MUGEJU  
Lo sugirieron antes del cataclismo en MUFACE con el abandono de las aseguradoras y, ahora, lo vuelven a proponer. Con más fuerza que nunca tras conseguir que el Tribunal Administrativo Central de Recursos Contractuales (Tacrc) haya decidido suspender cautelarmente las licitaciones para la concesión de asistencia sanitaria a las mutualidades del Ejército y Justicia, ISFAS y MUGEJU, respectivamente, tras los recursos que ha interpuesto, la Asociación Unión Médica Profesional (Unipromel), que representa a más de 10.000 facultativos autónomos, vuelve a abogar por hacerse cargo de la asistencia sanitaria de los mutualistas. Sin aseguradoras de por medio.  
Su modelo, denominado **'Mutualidad Directa'**, explican desde Unipromel, pasa porque sean los propios médicos los que presten atención médica de forma directa a los mutualistas. 

En [memoria 2023 pdf](https://www.muface.es/dam/jcr:7770da43-c118-419b-9619-0283022c012f/MUFACE_MEMORIA_2023_WEB.pdf)  
> Elección de prestador de asistencia sanitaria   
Tabla 4.1. Distribución de personas mutualistas y beneficiarias por prestador

Las entidades privadas suponen 70,26% (frente 73,21% de año anterior) (1.080.360 frente a 1.095.393 incluyendo beneficiarios) y INSS supone 29,74% (frente 26,79% de año anterior) (457.307 frente a 400.813 incluyendo beneficiarios) 

### Cambio de prestador de asistencia sanitaria

[Cambio de entidad sanitaria - muface](https://www.muface.es/muface_Home/Prestaciones/asistencia-sanitaria-nacional/cambio-entidad-sanitaria.html)  
>1. CAMBIO ORDINARIO EN EL MES DE ENERO  
>2. CAMBIO FUERA DEL PERIODO ORDINARIO Y CAMBIO EXTRAORDINARIO A INSS (Servicios Públicos de Salud)

[Prestaciones y descarga de impresos > Descarga de impresos](https://www.muface.es/muface_Home/Prestaciones/Impresos.html) 
Cambio de Entidad Médica  
Cambio extraordinario al INSS  

En [memoria 2022 pdf](https://www.muface.es/dam/jcr:2985f319-6b76-4244-a1ff-6058d843cc9e/Memoria_Muface_2022.pdf)  
> 4.1 Prestaciones sanitarias  
> Respecto a la posibilidad de cambio ordinario de entidad durante el mes de enero, ejercieron su derecho a este cambio, 50.355 titulares, el 3,37% del colectivo afiliado.

### Posibilidad de prestador de asistencia sanitaria privada y atención en SNS

[Ley 33/2011, de 4 de octubre, General de Salud Pública. Disposición adicional sexta. Extensión del derecho a la asistencia sanitaria pública.](https://www.boe.es/buscar/act.php?id=BOE-A-2011-15623#dasexta)  
> 2. Lo dispuesto en el apartado anterior no modifica el régimen de asistencia sanitaria de las personas titulares o de beneficiarias de los regímenes especiales de la Seguridad Social gestionados por MUFACE, MUGEJU o ISFAS, que mantendrán su régimen jurídico específico. Al respecto, las personas encuadradas en dichas mutualidades que hayan optado por recibir asistencia sanitaria a través de las entidades de seguro deberán ser atendidas en los centros sanitarios concertados por estas entidades. En caso de recibir asistencia en centros sanitarios públicos, el gasto correspondiente a la asistencia prestada será reclamado al tercero obligado, de acuerdo a la normativa vigente.


### Visión de MUFACE como funcionario. Para qué sirve MUFACE. Qué aporta/quita MUFACE

El tema de clases pasivas se comenta por separado

[Muface, la mutua de los funcionarios, pagará más de 3.500 millones de euros a las aseguradoras hasta 2024 - newtral](https://www.newtral.es/muface-aseguradoras-empresas-concierto/20211010/)  
Para qué sirve Muface?  
Dentro de Muface existen dos mecanismos de cobertura diferentes. Por un lado, gestiona el Régimen del Mutualismo Administrativo que incluye prestaciones como la asistencia sanitaria y la farmacéutica, el subsidio por incapacidad temporal o por riesgo durante el embarazo o durante la lactancia natural, las indemnizaciones por lesiones permanentes no invalidantes o la prestación económica por gran invalidez.  
Por otra parte se encuentra el Régimen de Clases Pasivas, que tiene las competencias para conceder las pensiones de jubilación, viudedad, orfandad y en favor de familiares. Sin embargo, esta cobertura no la gestiona Muface, sino el Ministerio de Inclusión, Seguridad Social y Migraciones.  

Aparte de que al funcionario le suponga pagar una cuota que le resta unos 50€ mensuales de la nómina aunque no use sanidad privada, hay otros inconvenientes para los usuarios de la sanidad pública.  

Hasta 2020 en sanidad pública no podían hacer recetas normales: el funcionario tenía que llevar un "talonario físico de recetas" para que se las hicieran manualmente, obligando a usar en la sanidad pública el sistema de recetas de las sanidad privada. No fue hasta la pandemia de 2020 que se implementó la receta electrónico en sanidad pública para funcionarios.  
	
[Twitter FiQuiPedia/status/1242790722333540353](https://x.com/FiQuiPedia/status/1242790722333540353)  
He escrito y comparto lo que me acaban de contestar (había puesto consulta vía app sábado 21)  
"Por el momento, sí que tienen que ser prescritas por los facultativos de la entidad ...si pudiera obtener (sin receta, se depende de favores) los medicamentos en la farmacia"🤦‍♂️  
![](https://pbs.twimg.com/media/ET9HV6OWsAQJv8v?format=png)  

[Twitter FiQuiPedia/status/1243500023121350656](https://x.com/FiQuiPedia/status/1243500023121350656)  
Hoy 27 he recibido respuesta escrita a mi consulta de día 21  
"intente adquirir los medicamentos sin receta en la farmacia"  
![](https://pbs.twimg.com/media/EUHMmOEWAAAY52W?format=jpg)  

[Twitter FiQuiPedia/status/1255201466916233217](https://x.com/FiQuiPedia/status/1255201466916233217)  
Hoy he tenido que ir en persona al centro de salud @SanidadCLM y me han hecho todas las recetas que quedaban en el talonario, así que he pedido uno nuevo.  
Pedir online un talonario de recetas en papel es vender "modernizar" algo obsoleto perpetuándolo @mufaceoficial

[Twitter FiQuiPedia/status/1263530515916783616](https://x.com/FiQuiPedia/status/1263530515916783616)  
Noticia reciente: hoy he sido la primera persona de muface a la que en mi centro de salud de atención primaria han cargado una receta en la tarjeta de @SanidadCLM   
En la farmacia veían lo recetado, pero no podía dispensarlo por "error sescam". De nuevo lo tengo gracias a un favor

**27 marzo 2024**

[Buenas noticias para los funcionarios de Muface: estos son los nuevos servicios y prestaciones que ofrece - elespanol](https://www.elespanol.com/invertia/mis-finanzas/20240327/buenas-noticias-funcionarios-muface-nuevos-servicios-prestaciones-ofrece/843165732_0.html)  

**25 agosto 2024**

[La ayuda de 300 euros para estos funcionarios: el límite del plazo está cerca - elperiodico](https://www.elperiodico.com/es/economia/20240825/ayuda-estado-funcionarios-300-euros-octubre-dv-102859478)
 
Otro elemento es que el mutualista que opta por sanidad privada deja de poder usar sanidad pública, salvo que sean urgencias o sea en zonas rurales (poblaciones pequeñas).  
[Resolución de 22 de diciembre de 2021, de la Mutualidad General de Funcionarios Civiles del Estado, por la que se publica el Concierto suscrito con entidades de seguro para el aseguramiento del acceso a la asistencia sanitaria en territorio nacional a los beneficiarios de la misma durante los años 2022, 2023 y 2024.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-21337)  
> 2.5 Como criterio especial se tendrá en cuenta que, en las zonas rurales expresamente previstas en los convenios a que se refiere el Anexo 2 de este Concierto, y con el alcance y contenido estipulado en cada uno de ellos, la asistencia sanitaria a nivel ambulatorio, domiciliario o de urgencia a cargo del médico general o de familia, pediatra, diplomado en enfermería y matrona se podrá prestar por los Servicios de Atención Primaria y de Urgencia de la Red Sanitaria Pública.  

> Para hacer posible la prestación de servicios sanitarios en las zonas rurales a los beneficiarios adscritos a la Entidad, MUFACE podrá convenir con los Servicios de Salud de las comunidades autónomas la prestación de aquellos ...  
 
### Clases pasivas

Las clases pasivas gestionan pensiones

[Pensiones de Clases Pasivas - portalclasespasivas.gob.es](https://www.portalclasespasivas.gob.es/sitios/clasespasivas/es-ES/PENSIONESCLASESPASIVAS/Paginas/PensionesClasesPasivas.aspx)  
>El personal incluido en el Régimen de Clases Pasivas del Estado:  
     Funcionarios de carrera y en prácticas de la Admón General del Estado, de la Admón de Justicia, de las Cortes Generales, de otros órganos constitucionales o estatales que lo prevean, y, funcionarios transferidos a las Comunidades Autónomas  
     Militares de carrera, de las Escalas de complemento, de tropa y marinería profesional y los Caballeros Cadetes, Alumnos y Aspirantes de las Escuelas y Academias Militares  
     Ex Presidentes, Vicepresidentes y Ministros del Gobierno de la Nación y otros cargos  
  
Se gestiona fuera de muface e incluye funcionarios militares y de justicia además de funcionarios civiles, además de cargos políticos que no son funcionarios.  

[Pensiones de Clases Pasivas > Pensiones de Jubilación o retiro - portalclasespasivas](https://www.portalclasespasivas.gob.es/sitios/clasespasivas/es-ES/PENSIONESCLASESPASIVAS/pensionesjubilacion/Paginas/pensionesjubilacion.aspx)  

Los funcionarios que están en clases pasivas son "antiguos", ya que desde 2011 se cotizan en el Régimen General de la Seguridad Social 

[Preguntas frecuentes - portalclasespasivas.gob.es](https://www.portalclasespasivas.gob.es/sitios/clasespasivas/es-ES/Paginas/Faqs.aspx)  
> Desde el 1 de enero de 2011, los funcionarios públicos de nuevo ingreso están obligatoriamente incluidos y cotizan al Régimen General de la Seguridad Social, que es el régimen por el que generarán su derecho a la pensión de jubilación o retiro y a la de sus familiares.   

[Real Decreto Legislativo 8/2015, de 30 de octubre, por el que se aprueba el texto refundido de la Ley General de la Seguridad Social. Disposición adicional tercera. Inclusión en el Régimen General de la Seguridad Social de los funcionarios públicos y de otro personal de nuevo ingreso.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11724#datercera)  

Un tema asociado a las clases pasivas es la posibilidad de jubilarse a los 60 años, cosa que en mi experiencia es habitual en docentes  

[Pensiones de Clases Pasivas > Pensiones de Jubilación o retiro > Tipos de jubilación o retiro - portalclasespasivas](https://www.portalclasespasivas.gob.es/sitios/clasespasivas/es-ES/PENSIONESCLASESPASIVAS/pensionesjubilacion/Paginas/TiposdeJubilacion.aspx)  
> JUBILACIÓN O RETIRO VOLUNTARIO  
Los funcionarios públicos incluidos en el Régimen de Clases Pasivas pueden jubilarse o retirarse voluntariamente desde que cumplan los 60 años de edad, siempre que tengan reconocidos 30 años de servicios al Estado.

### Posibilidad legal de integrar MUFACE en el SNS

[Disposición final tercera Ley 14/1986, de 25 de abril, General de Sanidad.](https://www.boe.es/buscar/act.php?id=BOE-A-1986-10499#tercera-3)  

> 2. El Gobierno, mediante Real Decreto, a propuesta conjunta de los Ministerios interesados, dispondrá que los centros, servicios y establecimientos sanitarios de las Mutuas de Accidentes, Mutualidades e Instituciones públicas o privadas sin ánimo de lucro, puedan ser objeto de **integración en el Sistema Nacional de Salud**, siempre que reúnan las condiciones y requisitos mínimos.  

 
### Argumentos en contra y a favor. Polémicas sobre MUFACE

Los argumentos en contra o a favor se pueden ver como polémicas: la polémicas suelen ser propuestas de cambios usando argumentos a favor o en contra, a veces propuestas de cambios normativos 

La polémica esencial es que MUFACE paga con dinero público a empresas privadas para el servicio público de sanidad. Al hablar de coste se comentan ideas asociadas, como que los defensores argumentan que ahorra dinero público, que la sanidad pública no puede atender a todos y si no existiera MUFACE colapsaría la sanidad pública (listas de espera, ocupación camas, ...), y que si no se diera ese dinero público a las empresas privadas supondría pérdidas de empleos.   

Los defensores argumentan que son derechos y que no hay que pedir quitarlos sino pedir que más gente los tenga, sin visualizar que es un privilegio.  

Otro tema es que las clases pasivas su gestión de las jubilaciones hay quien lo veo como "un plan de pensiones" alternativo al que tienen todos los que cotizan a la Seguridad Social que aplica solo a funcionarios públicos pero está pagado con dinero público

También se recogen artículos sobre la "polémica" de plantear la eliminación de [la asistencia sanitaria privada a través de] muface de 2024, la capacidad del SNS de atender a todos los mutualistas 

**23 marzo 2020**

[¿Qué es Muface y por qué Carmen Calvo está ingresada en la Ruber? - huffingtonpost](https://www.huffingtonpost.es/entry/que-es-muface-y-por-que-carmen-calvo-esta-ingresada-en-la-ruber_es_5e78a6b7c5b62f90bc4e1224.html)  

**24 marzo 2020**

[Un modelo público-privado de origen tardofranquista: Muface, la mutua que ha llevado a Calvo a la Ruber - infolibre](https://www.infolibre.es/politica/modelo-publico-privado-origen-tardofranquista-muface-mutua-llevado-calvo-ruber_1_1181467.html)  

**18 julio 2020**

[El plan del Gobierno de eliminar Muface enviará 2 millones de funcionarios de la sanidad privada a la pública - okdiario](https://okdiario.com/espana/plan-del-gobierno-eliminar-muface-enviara-2-millones-funcionarios-sanidad-privada-publica-5910740/)  

[¿Quebraría la Sanidad Pública si el Gobierno deja a los funcionarios sin el modelo Muface? - la razon](https://www.larazon.es/salud/20200718/hdzl7jhekzazba6uqjn4raaqoi.html)  
> Cada paciente de Muface le cuesta al Estado 341 euros menos al año que el atendido en un centro público

**17 agosto 2020**

[Sánchez ordena desmantelar en octubre el sistema propio de pensiones de 900.000 funcionarios - okdiario](https://okdiario.com/espana/sanchez-ordena-desmantelar-octubre-sistema-propio-pensiones-900-000-funcionarios-6020212)  


**28 septiembre 2021**

[161/003131 Proposición no de ley para su debate en la Comisión de Sanidad y Consumo, relativa a la integración del mutualismo administrativo en el régimen general del Sistema Nacional de Salud. - congreso.es](https://www.congreso.es/public_oficiales/L14/CONG/BOCG/D/BOCG-14-D-332.PDF)  

**29 agosto 2022**
[Una laguna legal permite a medio millón de trabajadores cotizar en mutuas privadas al margen de la Seguridad Social - publico](https://www.publico.es/economia/laguna-legal-permite-medio-millon-trabajadores-cotizar-mutuas-privadas-margen-seguridad-social.html/amp	)  

**20 septiembre 2023**

[CSIF reclama que se incremente la financiación del concierto de Muface - elespanol](https://www.elespanol.com/invertia/observatorios/sanidad/20230920/csif-reclama-incremente-financiacion-concierto-muface/795920564_0.html)  

**15 enero 2024**  
[Muface no se toca - diario.red](https://www.diario.red/opinion/remigio-cordero/muface-no-se-toca/20240115060000021871.html)  

**19 marzo 2024**  
[Muface no se toca (II) - diario.red](https://www.diario.red/opinion/remigio-cordero/muface-no-se-toca-ii/20240319060000022008.html)  

**7 octubre 2024**

[Conflicto entre aseguradoras y Muface: preguntas y respuesta - newtral](https://www.newtral.es/conflicto-muface-aseguradoras/20241007/)  

**8 octubre 2024**

[El Gobierno aprueba la licitación de Muface subiendo un 17% el presupuesto - newtral](https://www.newtral.es/gobierno-licitacion-muface/20241008/)  


[CSIF considera "insuficiente" la subida de primas en Muface y amenaza con movilizaciones - elespanol](https://www.elespanol.com/invertia/observatorios/sanidad/20241008/csif-considera-insuficiente-subida-primas-muface-amenaza-movilizaciones/891911007_0.html)  

**9 octubre 2024**

[Asisa, Adeslas y DKV estudian la propuesta del Gobierno para Muface pese a que no corrige la totalidad de sus pérdidas - elespanol](https://www.elespanol.com/invertia/observatorios/sanidad/20241009/asisa-adeslas-dkv-estudian-propuesta-gobierno-muface-pese-no-corrige-totalidad-perdidas/891911044_0.html)  

**12 octubre 2024**

[Muface, el último dique de contención para salvar a la sanidad pública del colapso - elespanol](https://www.elespanol.com/invertia/observatorios/sanidad/20241012/muface-ultimo-dique-contencion-salvar-sanidad-publica-colapso/892660823_0.html)  


[Los funcionarios, ante el desmantelamiento de Muface: "Tendremos que pagar el seguro privado de nuestro bolsillo para no colapsar la sanidad pública" - elmundo](https://www.elmundo.es/economia/2024/10/12/670952b0e9cf4ad6788b45a4.html)  
> El 99,5% de los funcionarios creen que Muface va a desaparecer. Así se desprende de la primera encuesta de calidad de las mutuas realizada por la Central Sindical Independiente de Funcionarios (Csif)... en el año 2014.  


**14 octubre 2024**

[Los sindicatos, esperanzados con un Muface que encara su foto finish el 5N - redaccionmedica](https://www.redaccionmedica.com/secciones/privada/los-sindicatos-esperanzados-con-un-muface-que-encara-su-foto-finish-el-5n-5655)  

**15 octubre 2024**

[twitter javierpadillab/status/1846069053439214056](https://x.com/javierpadillab/status/1846069053439214056)  
¿Podría el Sistema Nacional de Salud asumir los pacientes de MUFACE?   
Ayer me preguntaron mi opinión en la Comisión de Sanidad del @Congreso_Es  

**28 octubre 2024**  
[FEUSO-Madrid reclama al Gobierno la renovación del concierto con MUFACE ](https://feuso.es/comunidad-de-madrid/noticias/18929-feuso-madrid-reclama-al-gobierno-la-renovacion-del-concierto-con-muface)  
> En caso de no renovarse el concierto, un millón y medio de funcionarios tendrían que ser asumidos por la ya colapsada Seguridad Social. 

**5 noviembre 2024**  
[Muface seguirá garantizando la atención sanitaria a todos los mutualistas, tanto titulares como beneficiarios - muface](https://www.muface.es/muface_Home/muface_comunicacion/hemeroteca-noticias/2024/Noviembre-2024/Novedades-Concierto--Muface-seguir--garantizando-atenci-n-sanitaria-a-todos-los-mutualistas.html)  
> Hoy terminaba el plazo para presentar las ofertas al concierto de Muface para los años 2025 y 2026 y la licitación ha quedado desierta.  

> Una vez que la licitación ha quedado desierta, Muface, como órgano de contratación, iniciará los trabajos para una nueva licitación.  

[Las aseguradoras plantan a Muface pese a que el Gobierno les lanzó la mayor oferta de la historia - elsaltodiario](https://www.elsaltodiario.com/sanidad/aseguradoras-plantan-muface-pese-ejecutivo-les-lanzo-mayor-oferta-historia)  

**7 noviembre 2024**  
[¿Cómo impactaría el fin de Muface en la sanidad pública? Un 3,7% más de 'babyboomers' entrarían al sistema - eldiario](https://www.eldiario.es/sociedad/impactaria-muface-sanidad-publica-3-7-babyboomers-entrarian-sistema_1_11796740.html)  

**8 noviembre 2024**  
[Muface no se toca (III): El chantaje de las aseguradoras. Una amenaza infundada y una agenda oculta - diario.red](https://www.diario.red/opinion/remigio-cordero/muface-toca-iii-chantaje-aseguradoras-amenaza-infundada-agenda-oculta/20241107204926038230.html)  

**13 noviembre 2024**  
[La FADSP (Federación de Asociaciones para la Defensa de la Sanidad Pública) ante la situación de MUFACE](https://fadsp.es/fin-muface-sanidad-publica/)  

**18 noviembre 2024**  
[Sanidad acusa a las aseguradoras privadas de seleccionar pacientes y cree que sería “razonable” extinguir Muface - eldiario](https://www.eldiario.es/sociedad/sanidad-acusa-aseguradoras-privadas-seleccionar-pacientes-cree-seria-razonable-extinguir-muface_1_11824446.html)  

## Desaparición de prestación sanitaria privada a través de MUFACE para que sean atendidos por SNS, financiación y viabilidad 

**18 noviembre 2024**  
[Sanidad propone disolver Muface e integrar a sus afiliados en el sistema de salud pública - publico](https://www.publico.es/sociedad/sanidad-propone-disolver-muface-e-integrar-afiliados-sistema-salud-publica.html)  

[Sanidad plantea suprimir Muface e integrar a los usuarios en la sanidad pública en nueve meses - rtve](https://www.rtve.es/noticias/20241118/sanidad-plantea-suprimir-muface-integrar-mutualistas-sanidad-publica/16335227.shtml)  

**9 diciembre 2024**  
[El fin de Muface deja de ser una quimera: ¿puede la sanidad pública atender a un millón de funcionarios? - elpais](https://elpais.com/sociedad/2024-12-09/el-fin-de-muface-deja-de-ser-una-quimera-puede-la-sanidad-publica-atender-a-un-millon-de-funcionarios.html)  


**21 diciembre 2024**  
[El Gobierno se da tres años para repensar Muface - eldiario](https://www.eldiario.es/economia/gobierno-da-tres-anos-repensar-muface_1_11917792.html)  


En diciembre 2024 se añade a la polémica la idea de que algunas CCAA con el dinero que se deje de gastar en convenios de MUFACE ofrezcan sanidad privada a sus funcionarios

**26 diciembre 2024**  
[Ayuso tranquiliza a los funcionarios de Muface: "Si son abandonados por el Gobierno, Madrid les dará la cobertura que merecen" - eleconomista](https://www.eleconomista.es/salud/noticias/13149987/12/24/ayuso-tranquiliza-a-los-funcionarios-de-muface-si-son-abandonados-por-el-gobierno-madrid-les-dara-la-cobertura-que-merecen.html)  


**31 diciembre 2024**  
[Asisa contempla asumir Muface en solitario y garantizar la sanidad privada de los funcionarios ](https://www.eldiario.es/economia/asisa-contempla-asumir-muface-solitario-garantizar-sanidad-privada-funcionarios_1_11933971.html)  
La compañía estudia la nueva oferta del Gobierno y baraja presentarse junto a DKV o en solitario tras la renuncia de Adeslas, algo que descartó en la primera licitación 


### Transparencia MUFACE

Me surge la duda de si MUFACE está sujeta a la ley de transparencia; creo que sería interesante tener el porcentaje de mutualistas que eligen prestación de sanidad pública por tramo de edad

En la web de MUFACE la memoria anual se encuadra en un apartado de transparencia, y no localizo formulario de solicitud de información pública

Localizo esta resolución desestimatoria [Resolución 031/2019 CTBG Cobertura de asistencia sanitaria por MUFACE](https://www.consejodetransparencia.es/ct_Home/gl/dam/jcr:3954a41d-0f6e-47da-8812-8c6c70319471/R-0031-2019.pdf)

Veo que desde 2024 tiene una web de datos abiertos

[Muface > datos abiertos](https://www.muface.es/muface_Home/muface/DATOS-ABIERTOS.html)  

Por ejemplo está EVOLUCIÓN HISTÓRICA VOLUMEN COLECTIVO en formato hoja de cálculo   

Veo que ahí indica 

> Si desea emitir alguna sugerencia respecto a este espacio de Datos Abiertos, puede hacerlo a través de nuestro servicio electrónico de Quejas, Sugerencias y Felicitaciones. Agradecemos de antemano las que pueda formular.

[Buzón de quejas, sugerencias y felicitaciones](https://sede.muface.gob.es/content/buzon-qsf)  

12 octubre 2024 pongo siguiente sugerencia

> En la página de datos abiertos  https://www.muface.es/muface_Home/muface/DATOS-ABIERTOS.html se indica "Si desea emitir alguna sugerencia respecto a este espacio de Datos Abiertos, puede hacerlo a través de nuestro servicio electrónico de Quejas, Sugerencias y Felicitaciones."  
Mi sugerencia es que en los datos de MUTUALISTAS Y BENEFICIARIOS SEGÚN ELECCIÓN DE ENTIDAD SANITARIA, donde ahora solo se desglosa por provincia, se desglosase, de manera adicional o alternativa (podrían ser datos para toda España), por tramo de edad.   
Por ejemplo en tramos de "menos de 30 años, entre 30 y 40, entre 40 y 50, entre 50 y 60 y más de 60" se desglosase entre INSS y el resto de prestadores de servicios sanitarios (no es necesario que estén desglosado por cada prestador de servicios sanitarios privados)

