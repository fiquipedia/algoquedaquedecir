+++
author = "Enrique García"
title = "Derechos de autor en educación"
date = "2021-10-11"
tags = [
    "educación", "normativa", "Derechos de autor"
]
toc = true

+++

Revisado 1 marzo 2024

## Resumen

Al manejar materiales educativos no siempre se es consciente de los derechos de autor asociados. Creo este post para recopilar ideas, creándolo en 2021 ya que una Consejería de Educación usa materiales míos sin citar autoría ni licenciamiento.

Posts relacionados: 

* [!Compartid, malditos!](http://algoquedaquedecir.blogspot.com/2018/04/compartid-malditos.html)
* [Lo básico de normativa básica](https://algoquedaquedecir.blogspot.com/2019/01/lo-basico-de-normativa-basica.html)  (la normativa sobre propiedad intelectual es básica) 
* [Educamadrid: qué y por qué](https://algoquedaquedecir.blogspot.com/2021/01/educamadrid-que-y-por-que.html) (enlaza con la idea de recursos abiertos como el  repositorio de aulas virtuales donde los docentes ceden materiales para ser usados por otros, y recursos cerrados como los millones de € gastados en alquilar materiales de Editorial Planeta)

## Detalle

Los derechos de autor están presentes al usar materiales elaborados por otros y cuando otros utilizan materiales que elaboramos nosotros, y es algo especialmente relevante en educación donde se usan muchos recursos.

Creo este post en 2021 tras ver que una Consejería de Educación ha utilizado materiales míos sin respetar el licenciamiento, aunque lo uso para ir recopilando información asociada como otros posts.

Un punto de partida es conocer lo básico sobre derechos de autor y sobre los licenciamientos de cada material. 

En la web intento cuidar el licenciamiento, y en el apartado licenciamiento intento citar ideas básicas. Pongo parte del texto en [Licenciamiento - fiquipedia](https://www.fiquipedia.es/licenciamiento/) que cita normativa

>**¿Es legal citar cosas, por ejemplo imágenes, que tienen derechos de autor?**  
>Poner imágenes en recursos es útil, pero a veces es complicado encontrar alguna con el licenciamiento adecuado o se necesita una muestra de un “original”  
En la medida de lo posible intento citar la fuente de las imágenes, que se incluyen por ejemplo en actividades o en ejercicios como mera ilustración, en unos materiales cc-by-sa que no tienen ánimo de lucro.  
Yo no tengo puesto explícitamente el “nc” ( [CC’s NonCommercial (NC) licenses prohibit uses that are “primarily intended for or directed toward commercial advantage or monetary compensation."](https://creativecommons.org/faq/#does-my-use-violate-the-noncommercial-clause-of-the-licenses) ), pero ni cuando uso imágenes ni en otros materiales tengo ánimo de lucro; si alguien lo usase con ese fin (al publicarlo yo sin “nc” sí lo permito), sería el que lo hiciese quien tiene que revisarlo.  
Los materiales de la página están destinados a educación, y considero que el uso puntual de imágenes está amparado en [Artículo 32 de Real Decreto Legislativo 1/1996](http://www.boe.es/buscar/act.php?id=BOE-A-1996-8930#a32) , al tiempo que se manifiesto la disposición a retirar una imagen citada en caso de que el propietario de los derechos lo indique. En ese caso intentaría buscar otra imagen o realizarla yo mismo como dibujo si es posible.

### Normativa y artículos sobre derechos de autor

La normativa sobre derechos de autor va cambiando con el tiempo. 

Esta norma es de 1996 pero se enlaza la versión consolidada que en 2021 tiene cambios en 2020
[Real Decreto Legislativo 1/1996, de 12 de abril, por el que se aprueba el texto refundido de la Ley de Propiedad Intelectual, regularizando, aclarando y armonizando las disposiciones legales vigentes sobre la materia.](https://www.boe.es/buscar/act.php?id=BOE-A-1996-8930)

En especial [Artículo 32. Citas y reseñas e ilustración con fines educativos o de investigación científica.](https://www.boe.es/buscar/act.php?id=BOE-A-1996-8930#a32)

Aparte de mención genérica en punto 1, punto 3 hace mención explícita a uso por docentes "El profesorado de la educación reglada impartida en centros integrados en el sistema educativo español..."

[Real Decreto-ley 24/2021, de 2 de noviembre, de transposición de directivas de la Unión Europea en las materias de bonos garantizados, distribución transfronteriza de organismos de inversión colectiva, datos abiertos y reutilización de la información del sector público, ejercicio de derechos de autor y derechos afines aplicables a determinadas transmisiones en línea y a las retransmisiones de programas de radio y televisión, exenciones temporales a determinadas importaciones y suministros, de personas consumidoras y para la promoción de vehículos de transporte por carretera limpios y energéticamente eficientes.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-17910)

[Artículo 68. Utilización de obras y otras prestaciones en actividades pedagógicas digitales y transfronterizas.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-17910#a6-10)  

> 1. No será precisa autorización de los titulares de derechos de propiedad intelectual para los actos de reproducción, distribución y comunicación pública por medios digitales de obras y otras prestaciones a efectos de ilustración con fines educativos siempre que:  
a) sean realizados por el profesorado de la educación reglada impartida en centros integrados en el sistema educativo español y por el personal de universidades y organismos de investigación.  
b) tengan lugar en un entorno electrónico seguro.  
c) se indique la fuente, con inclusión del nombre del autor, siempre que sea posible.  
> 2. Estos actos se entenderán únicamente realizados en territorio español, aunque sus destinatarios no se encuentren en él.  

Recuerdo algunas polémicas de las que cito algún enlace, por ver más detalles

**29 abril 2004**

[No al préstamo de pago en bibliotecas - internautas.org](https://www.internautas.org/html/1668.html)  
> Acogiéndose a una directiva europea (la Directiva 92/100/CE sobre derechos de alquiler y préstamo y otros derechos afines), las entidades de gestión de derechos de autor CEDRO y la SGAE pretenden cobrar por el préstamo en bibliotecas, alegando que la gratuidad de este servicio público atenta contra el derecho de autor.

**27 mayo 2017**

[CIRCULAR SOBRE LOS DERECHOS DE PROPIEDAD INTELECTUAL Y SU APLICACIÓN EN EL ÁMBITO EDUCATIVO, 2017 Madrid](https://formacion.educa.madrid.org/pluginfile.php/131716/mod_page/content/18/Circular_Propiedad_intelectual.pdf)

**25 mayo 2018**

[Forget The GDPR, The EU's New Copyright Proposal Will Be A Complete And Utter Disaster For The Internet](https://www.techdirt.com/articles/20180525/10072939912/forget-gdpr-eus-new-copyright-proposal-will-be-complete-utter-disaster-internet.shtml)

[Change Copyright](https://www.changecopyright.org/es/)  
Change Copyright was Mozilla's action to promote copyright law in the EU that promoted competition and innovation online.

**23 junio 2023**

[¿A quién pertenecen los materiales didácticos que elabora el profesorado? - xarxatic](https://xarxatic.com/a-quien-pertenecen-los-materiales-didacticos-que-elabora-el-profesorado/)

Cita [Artículo 51. Transmisión de los derechos del autor asalariado.](https://boe.es/buscar/act.php?id=BOE-A-1996-8930#a51)  
> 1. La transmisión al empresario de los derechos de explotación de la obra creada en virtud de una relación laboral se regirá por lo pactado en el contrato, debiendo éste realizarse por escrito.  
> 2. A falta de pacto escrito, se presumirá que los derechos de explotación han sido cedidos en exclusiva y con el alcance necesario para el ejercicio de la actividad habitual del empresario en el momento de la entrega de la obra realizada en virtud de dicha relación laboral.  
> 3. En ningún caso podrá el empresario utilizar la obra o disponer de ella para un sentido o fines diferentes de los que se derivan de lo establecido en los dos apartados anteriores.  
> 4. Las demás disposiciones de esta Ley serán, en lo pertinente, de aplicación a estas transmisiones, siempre que así se derive de la finalidad y objeto del contrato.  
> 5. La titularidad de los derechos sobre un programa de ordenador creado por un trabajador asalariado en el ejercicio de sus funciones o siguiendo las instrucciones de su empresario se regirá por lo previsto en el apartado 4 del artículo 97 de esta Ley.

**21 marzo 2024**

[Boletín EducaMadrid. Copyright - 21/03/2024](https://boletines.educa.madrid.org/boletin/d919772665f0399fb1ec614309305fe7)  
> La necesidad de adquirir competencias digitales, impulsadas por Europa, supone un nuevo reto para los docentes. La creación y la modificación de contenidos educativos digitales aparecen en los tres niveles de progresión establecidos: A1 y A2, B1 y B2, C1 y C2 (Resolución de 4 de mayo de 2022).  
Por este motivo EducaMadrid lanza esta campaña de concienciación en materia de derechos de autor y licencias de uso en el ámbito docente.  
La creación de contenidos educativos digitales respetando los derechos de propiedad intelectual crea situaciones de inseguridad en el docente, ya que surgen preguntas como: ¿qué puedo utilizar como docente?, ¿cómo puedo incorporar los recursos de terceros?, ¿cómo comparto mi material?  


Se citan:  
* [Ayuda EducaMadrid sobre el uso de licencias.](https://ayuda.educa.madrid.org/books/aula-virtual-(usuario-medio)/page/licencias)
* [Preguntas frecuentes EducaMadrid sobre derechos de autor y uso de licencias abiertas](https://ayuda.educa.madrid.org/books/preguntas-frecuentes/page/derechos-de-autor-y-licencias-de-uso)
* [Guía práctica de licencias de uso para docentes. INTEF. Proyecto EDIA](http://descargas.intef.es/cedec/proyectoedia/guias/contenidos/guiadelicencias/index.html)

### Reclamando derechos de autor

En la historia de FiQuiPedia solo recuerdo un problema con derechos de autor(aparte del bloqueo de Google  Sites en el que Google no indicó por qué): una editorial contactó porque tenía en la web un pdf sobre normativa de formulación que había descargado del ministerio, y que resultó que el ministerio había colgado sin permiso de la editorial. Lo retiré rápidamente (el ministerio tras contactarle ya lo había retirado), y es una muestra cómo el titular de los derechos de autor reclama.

**16 Mayo 2016 10:51**

---

Estimados Sres,

escribo desde Prensas de la Universidad de Zaragoza.

Advertidos por los autores, y según hemos podido constatar, tienen Uds.
colgado en su web

http://www.fiquipedia.es/home/recursos/quimica/formulacion#TOC-Nomenclatura-de-Qu-mica-Inorg-nica-Recomendaciones-de-2005-Versi-n-espa-ola

un enlace a un documento pdf descargable que corresponde a "Nomenclatura
de Química Inorgánica, Recomendaciones de 2005, Versión española
elaborada por Miguel A. Ciriano (CSIC-Universidad de Zaragoza) y Pascual
Román Polo  (Universidad del País Vasco)"

El pdf está alojado en:

https://a98a95f8-a-62cb3a1a-s-sites.googlegroups.com/site/fiquipediabackup06may2016/home/recursos/quimica/formulacion/libro_rojo_2005_esp.pdf?attachauth=ANoY7cqRifjmgIrBylck-W91yNrFyc0zhG8rTN28UbG7Gi_IY0jSLi7t5IKbAhIlI_B4xjlIJwONfCmudfn-R_IncHgYGpxkCfsjZe_JRBFkPUBqyH1OgX4vuUKGM4hanBYm_0hgzxa7x8uLhkSqzdCVWcgL0ZtFIznx8_cxcNx0DdEs1UbPDHrHLgyj6Ah0ofEVpE-al1uUbk7xqFeR_ZX6oFYCCV9zpasz_Q04uA8lIyGBiIA7xw1TBHQDsY6ykTNY3pSpYFZ28w1KN8vXV8Vqn7sxnrEdSX3eOAW48baU3f0Kl9S2IVA%3D&attredirects=0

Este documento corresponde a un libro que nosotros publicamos y sobre el
cual tenemos los derechos de explotación.

Les pedimos, por favor, que retiren el enlace a la mayor brevedad
posible.

Gracias y saludos,

---

**16 mayo 2021 14:17**

---

Hola
Ya está retirado

Antes de retirarlo en mi página ponía este texto

"En 2007 se hizo una traducción al español por la Universidad de Zaragoza, que es citada por la IUPAC, pero en principio es de pago  
http://old.iupac.org/publications/books/author/RedBook-spanish.html  
Miguel A. Ciriano y Pascual Román Polo  
Editorial: Prensas Universitarias de Zaragoza, junio 2007  
ISBN 978-84-7733-905-2  

Sin embargo se localiza en 2015 en versión PDF descargable dentro de una página del Cidead,  Ministerio de Educación  
http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena8/  
http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena8/libro_rojo_2005_esp.pdf  
Se incluye en esta página entendiendo que no hay problemas de derechos de autor; si es así se retirará  
"  

Ahora el texto es  
"  
En 2015 se localiza en versión PDF descargable dentro de una página del Cidead,  Ministerio de Educación  
http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena8/  
Inicialmente se incluía en esta página entendiendo que si estaba publicada por el Ministerio no había problemas de derechos de autor, pero en 2016 se retira ya que Prensas de la Universidad de Zaragoza que tiene los derechos de explotación confirma que no se puede publicar y también es retirado el pdf de la web del Ministerio.  
"  

Pido disculpas, pero como comentaba lo publiqué porque estaba en la web del Ministerio.

Un saludo  
Enrique

--- 

**16 mayo 2016 14:22**

---

Muchas gracias, Enrique, por tu pronta respuesta

Somos conscientes de que al haber un enlace desde la página del
ministerio, esto ha originado dudas en cuanto a los derechos de autor.

Muchas gracias de nuevo y saludos cordiales

---

En 2021 asociado a [Oposiciones: transparencia valoración](https://algoquedaquedecir.blogspot.com/2021/03/oposiciones-transparencia-valoracion.html) detecto casualmente que la Consejería de Educación de Galicia, al publicar los enunciados de oposiciones 2021, incluye soluciones incluyendo una captura de mi solucionario.

[https://twitter.com/FiQuiPedia/status/1446599014351319041](https://twitter.com/FiQuiPedia/status/1446599014351319041)

La solución oficial de uno de los problemas de oposición FQ 2021 @EduXunta, que es selectividad Madrid 2008-Junio-B1, incluye una imagen capturada de mi solucionario, cc-by-sa, sin atribuir autoría.

![](https://pbs.twimg.com/media/FBNaIMYWYAQGHZ2?format=jpg)  
![](https://pbs.twimg.com/media/FBNaIrjXsAcTnQ8?format=jpg)  

Me planteo poner una reclamación y ver el proceso desde el otro lado:

El procedimiento es [PR004A - Presentación electrónica de solicitudes, escritos y comunicaciones que no cuenten con un sistema electrónico específico ni con un modelo electrónico normalizado.](https://sede.xunta.gal/detalle-procedemento?codtram=PR004A&ano=2017&numpub=1&lang=es)

---

EXPONE:

En julio 2021 la Consellería de Educación de Galicia publica en https://www.edu.xunta.gal/oposicions/ProcesaConsultaPublica.do (asociado a tribunal 1 de Física y Química)

Solución 1ª Proba_opción A_Día 20/6/21  
Solución 1ª Proba_opción B_Día 20/6/21  
Solución 1ª Proba_opción B_Día 10/7/21  
Solución 1ª Proba_opción A_Día 10/7/21  

En el fichero asociado a opción A de día 10/7/21, problema 2, apartado a, se incluye una imagen tomada de unos materiales de elaboración propia, que se pueden consultar en https://www.fiquipedia.es/home/recursos/recursospau/ficherospaufisicaporbloques/F5.2-PAU-%C3%93pticaGeom%C3%A9trica-soluc.pdf, en los que se indica licenciamiento cc-by-sa (Creative Commons Atribución Compartir Igual) https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES

SOLICITA:

Que un plazo de un mes de la recepción de este escrito se modifique el documento citado indicando la autoría (cumplimiento de "atribución" de la licencia de la imagen usada) e indicando el licenciamiento( cumplimiento de "compartir igual" de la licencia de la imagen usada). Quiero dejar claro que en esta solicitud no incluyo ninguna petición de compensación económica, solo solicito que se cumpla el licenciamiento de los materiales que la Consejería ha utilizado.

Si la consejería no lo hace en el plazo indicado, pondré una reclamación por la vía administrativa y las medidas legales que considere oportunas, pudiendo reclamar en ese caso compensación económica.

---

El formulario no se puede descargar, hay que tramitar online

ATENCIÓN! Usted puede cubrir este formulario y presentarlo en el registro electrónico o guardar un borrador en su carpeta del ciudadano. El borrador no puede guardarse en su disco local. Para cubrir un campo sitúe el puntero del ratón en el espacio correspondiente.

*A súa solicitude foi presentada correctamente no Rexistro Electrónico da Xunta de Galicia co número 2021/2397701*

**27 julio 2022**

Recibo este comentario 

[twitter jjcanido/status/1552242057783255041](https://twitter.com/jjcanido/status/1552242057783255041)  
Por si te interesa la puntualización, la publicación no corre a cargo de la Consellería, sino de los presidentes de los tribunales, que han elaborado el práctico previamente, así como la solución. (Acabo de participar en un tribunal como secretario)


### Cursos y recursos asociados a derechos de autor

Recuros distintos de normativa y artículos  
Está relacionado con REA (Recursos Educativos Abiertos): en 2022 realizo una presentación en mi centro sobre REA citando licenciamiento y derechos de autor y comparto materiales 
[REA Recursos Educativos Abiertos](https://cloud.educa.madrid.org/s/iBA1RCENi342YQe)  

Aula Virtual Formación en línea del CRIF Las Acacias > (oculto/s) > Recursos > DERECHOS DE AUTOR  
En 2023 mismo enlace pasa a ser [Aula Virtual Formación en línea ISMIE > RECURSOS EDUCATIVOS ABIERTOS (REA)](https://formacion.educa.madrid.org/course/view.php?id=13&section=4)  

Cursos > En abierto > Espacio_Apoyo_TIC > Aula Virtual y licencias > Creación, reutilización y distribución de contenidos  
En 2023 cambia enlace y pasa a ser [Cursos > En abierto > Kitdigital > Kit de apoyo de cursos tutorizados en línea > Creación, reutilización y distribución de contenidos](https://formacion.intef.es/tutorizados_2013_2019/mod/book/view.php?id=60297)   

[5 mitos sobre derechos de autor y licencias de uso - intef](https://cedec.intef.es/5-mitos-sobre-derechos-de-autor-y-licencias-de-uso/)  

[Licencias - educamadrid](https://ayuda.educa.madrid.org/books/aula-virtual-%28usuario-medio%29/page/licencias)  

### Ideas / dudas

Se pueden plantear muchas, solo voy recopilando aquí. Puede haber situaciones donde los detalles sean propios de cada situación (por ejemplo un contrato escrito que aclare condiciones)

- Hacer (foto)copias de materiales de editoriales (de profesor) y distribuirlos entre los alumnos (si sí/no tienen el libro de alumno).

Ver [Fotocopiar libros para uso personal NO es delito](https://dukebody.blogspot.com/2006/04/fotocopiar-libros-para-uso-personal-no.html)  

Se cita [Código Penal, Sección 1.ª De los delitos relativos a la propiedad intelectual](https://www.boe.es/buscar/act.php?id=BOE-A-1995-25444#s1-7)  

- Ceder un aula virtual elaborada por un docente con materiales propios a un interino que lo sustituye

- Un docente hace materiales que son usados en su centro: derechos de autor a posteriori si se usan fuera de su centro si se ha hecho en horario de trabajo del docente

- Una empresa hace materiales que son usados en educación: derechos de autor de la empresa si se ha pagado a la empresa, o incluso derechos de la empresa para utilizarlos si ya ha cobrado. (ejemplo: los vídeos de matemáticas de unicoos hechos para la consejería de educación de Madrid)



