+++
author = "Enrique García"
title = "Ratios en educación: coste"
date = "2024-03-10"
tags = [
    "educación", "ratio",
]
toc = true

description = "Ratios en educación: coste"
thumbnail = "https://pbs.twimg.com/media/DyMdLoyX0AI6hGU?format=jpg"
images = ["https://pbs.twimg.com/media/DyMdLoyX0AI6hGU?format=jpg"]

+++

Revisado 1 abril 2024

## Resumen
 
Al hablar de bajar ratio se suele citar que es una medida muy costosa y que por eso no se implementa. Sin embargo no se suelen dar datos concretos sobre coste, a pesar de que hay casos "recientes" que permiten estimar coste: el aumento de ratio por recortes de RDL14/2012 y la bajada de ratios por covid19.

Posts relacionados:

* [Derogar RDL14/2012](http://algoquedaquedecir.blogspot.com/2018/08/derogar-rdl142012.html) (asociado a iniciativa legislativa 2018 para derogar entre otros artículo 2 asociado que supuso en 2012 aumento 20% ratios). En marzo 2019 se deroga artículo 2 de RDL14/2012 relativo a ratios.
* [Posible bajada de ratios](http://algoquedaquedecir.blogspot.com/2018/07/posible-bajada-de-ratios.html) (asociado a planteamiento de si bajarán y por qué. En breve pienso que lo harán cuando la privada con concierto lo necesiten, y se comenta iniciativa de 2018 que llegó al Congreso de bajar ratios a 20 en primaria y ESO que no fue aprobada)
* [Lo básico sobre normativa básica](https://algoquedaquedecir.blogspot.com/2019/01/lo-basico-de-normativa-basica.html) (asociado a que la normativa sobre ratios es básica, y no puede modificarse por las CCAA) 
* [Ratios en educación: estadísticas y realidad](https://algoquedaquedecir.blogspot.com/2019/04/ratios-en-educacion-estadisticas-realidad.html) (post centrado en datos y en incumplimiento documentado de la normativa)
* [Ratios en educación concertada](https://algoquedaquedecir.blogspot.com/2020/01/ratios-en-educacion-concertada.html)
* [Ratios ilegales en educación en Castilla-La Mancha](http://algoquedaquedecir.blogspot.com/2017/11/ratios-en-educacion-ilegales-en.html)
* [ Docentes: su número y su variación](http://algoquedaquedecir.blogspot.com/2020/09/docentes-su-numero-y-su-variacion.html) relacionado con coste

## Detalle

Este post es un borrador con ideas, pendiente ampliar detalles y concretar costes y estimaciones.

A la hora de coste la ratio implica porque varía el número de profesorado y su salario es una parte importante del coste educativo. 

Se intentan aportar datos de situaciones concretas de variación de ratio con coste asociado, que hay que valorar teniendo en cuenta el salario del profesorado cuando se aplicó ese cambio de ratio. 

### Aumento 20% RDL14/2012

El RDL14/2012 tengía como objetivo reducir gasto público

[Artículo 1. Objeto.](https://boe.es/buscar/act.php?id=BOE-A-2012-5337#a1)  
> El objeto de este Real Decreto-ley es adoptar medidas urgentes para la racionalización del gasto público en el ámbito de la educación, de conformidad con los principios de eficiencia y austeridad que deben presidir el funcionamiento de los servicios públicos.


El efecto perseguido es claro si se mira la [memoria de análisis e impacto normativo de Real Decreto-Ley 14/2012](https://drive.google.com/open?id=0B-t5SY0w2S8iYmh5aVVxOC1BUEU), página 29 y 32 se indica que esta modificación de ratios (aumentar un 20% en todas las enseñanzas) tiene un impacto económico de 464 M€ (millones de euros) por año.  

![](https://pbs.twimg.com/media/DB6seYxXUAEAIl1.jpg)

En 2022 se comenta esto
[twitter lucas_gortazar/status/1489368129784487938](https://twitter.com/lucas_gortazar/status/1489368129784487938)  
Bajar la ratio media de 25 a 20 medio es subir el presupuesto educativo en un 20% (fácil 5000 millones de euros) para un impacto neutro en muchos educativos centros y muy positivo en el 20/30% que atienden a vulnerables. Yo prefiero invertir los 5000M directamente en esos centros

[https://twitter.com/FiQuiPedia/status/1489377461506879489](https://twitter.com/FiQuiPedia/status/1489377461506879489)  
No me importa la ratio media, hay contextos donde influye poco. Sí tengo datos de que subir la máxima un 20% supuso en 2012 menos de 500 M€. 

### Aumento 10% escolarización tardía

Se desvirtúa y se usa para aumentar un 10% desde inicio de curso.

Planificar el curso comenzando con ratios elevadas un 10% reduce el número de grupos y reduce coste para la administración.

### Bajada ratios COVID19

[11.000 profesores más, bajada de ratios y vuelta escalonada por etapas en Madrid](https://www.magisnet.com/2020/08/curso-2020-21-10-000-profesores-mas-bajada-de-ratios-y-vuelta-escalonada-por-etapas-en-madrid/)  
>El plan, con una inversión de 370 millones de euros, contempla la contratación de 10.610 profesores más este curso escolar en la Comunidad de Madrid (7.948 en la Pública y 2.662 en la Concertada) para afrontar la situación del coronavirus.  
> Además, prevé una disminución del número de alumnos por aula: en el segundo ciclo de Educación Infantil y en Primaria habrá una reducción de ratio de 25 a 20 alumnos por clase, y en 1º y 2º de la ESO de 30 a 23.


[El Gobierno y las CCAA acuerdan volver a las ratios de antes del Covid: 25 alumnos por aula en Primaria, 30 en la ESO y 35 en Bachillerato](https://www.elmundo.es/espana/2021/05/19/60a4fe7a21efa02e4f8b462d.html)  


### Bajada ratios por bajada natalidad

En lugares como Madrid se baja la ratio gradualmente, y aunque se vende de otra manera, la razón es que la natalidad baja, ver [Posible bajada de ratios](http://algoquedaquedecir.blogspot.com/2018/07/posible-bajada-de-ratios.html) 

[La Comunidad de Madrid suma este curso 315 nuevas aulas públicas para bajar la ratio de 1º de la ESO de 30 a 25 alumnos](https://www.comunidad.madrid/noticias/2023/08/23/comunidad-madrid-suma-este-curso-315-nuevas-aulas-publicas-bajar-ratio-1o-eso-30-25-alumnos)  
> La creación de las nuevas aulas, en las que el Gobierno regional está invirtiendo más de 1,7 millones de euros, lleva aparejada la **contratación de 561 profesores, con un presupuesto que supera los 27 millones de euros.** 	

> La bajada de la ratio en la Educación Secundaria se une a la que ya empezó a funcionar en 2022/23 en el primer año del segundo ciclo de Educación Infantil, pasando en este caso de 25 a 20 escolares por clase, y que se extenderá a partir de septiembre al segundo curso de esta etapa. Para ello se han creado 138 nuevas aulas que serán atendidas por **166 docentes, lo que supone una inversión conjunta de cerca de 10 millones de euros.**

[La Comunidad de Madrid ha duplicado este curso la bajada del número de alumnos por aula con la inclusión de la Educación Secundaria](https://www.comunidad.madrid/noticias/2023/12/27/comunidad-madrid-ha-duplicado-este-curso-bajada-numero-alumnos-aula-inclusion-educacion-secundaria)  
> La Comunidad de Madrid ha duplicado la bajada de ratios este curso 2023/24 al extenderla a 1º de Educación Secundaria Obligatoria (ESO), etapa que ha pasado de 30 a 25 alumnos por clase, con una **inversión del Ejecutivo autonómico cercana a los 29 millones de euros y la incorporación de 561 nuevos docentes.**

> Esta reducción se une a la que ya empezó a funcionar en 2022/23 en el primer año del segundo ciclo de Educación Infantil, que pasó de 25 a 20 escolares, y que se ha extendido al segundo curso en 2023/24. Para ello se han creado nuevas aulas con un presupuesto de 370.000 euros que serán atendidas por **166 maestros, y una inversión total de cerca de 10 millones.**

### Bajada ratios por bajada natalidad y privados con concierto

Se niegan a bajar para tener más ...

**19 mayo 2023**

[Madrid 1º de la ESO: solo uno de cada tres centros concertados reducirá la ratio a veinticinco alumnos](https://aqui.madrid/madrid-1o-de-la-eso-solo-uno-de-cada-tres-centros-concertados-reducira-la-ratio-a-veinticinco-alumnos/)


### Bajada ratios ESO vs EGB

Es algo creo que poco comentado: que 7º y 8º de EGB pasaran de estar en colegios y ser "primaria" a estar en IES y ser "secundaria", supuso que la ratio esos dos curso pasaba de 25 a 30, ya que LOGSE permitía 25 y 30 como máximos respectivamente. 

