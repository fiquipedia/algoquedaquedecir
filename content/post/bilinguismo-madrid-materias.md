+++
author = "Enrique García"
title = "Bilingüismo Madrid: materias"
date = "2024-03-08"
tags = [
    "educación", "bilingüismo", "normativa"
]
toc = true

description = "Bilingüismo Madrid: materias"
thumbnail = "https://pbs.twimg.com/media/GBtqqLvW4AA4ngn?format=jpg"
images = ["https://pbs.twimg.com/media/GBtqqLvW4AA4ngn?format=jpg"]
	
+++

Revisado 23 octubre 2024

## Resumen

En 2024 se anuncia formalmente la modificación del bilingüismo para que "Geografía e Historia" (lo referencio en general así aunque hace referencia a Ciencias Sociales en primaria, Geografía e Historia en secundaria e Historia de España e Historia del Mundo Contemporáneo del Bachillerato) se deje de impartir en inglés.

El tema de bilingüismo da para mucho, pero aquí me centro solo en ese cambio y en algunas ideas sobre el bilingüismo asociado a ciertas materias. 

## Detalle

### Trámite de audiencia 2024

El 4 de marzo de 2024 se publica el trámite de audiencia del [Proyecto de decreto por el que se modifican el Decreto 61/2022, por el que se establecen ordenación y currículo etapa Ed. Primaria, el Decreto 65/2022, por el que se establecen ordenación y currículo ESO y el Decreto 64/2022, orden. y cur. Bachillerato](https://www.comunidad.madrid/transparencia/proyecto-decreto-que-se-modifican-decreto-612022-que-se-establecen-ordenacion-y-curriculo-etapa-ed)

_Tras la finalización del trámite cambia la url, ver más adelante_

El plazo para formular alegaciones es de 05/03/2024 hasta 13/03/2024, e intento recopilar aquí ideas para realizar mi alegación:

Se publica [Proyecto de decreto](https://www.comunidad.madrid/transparencia/sites/default/files/23._decreto_audiencia.pdf) y [Memoria del Análisis de Impacto Normativo](https://www.comunidad.madrid/transparencia/sites/default/files/24._main_audiencia.pdf)

El proyecto de decreto establece el abandono del bilingüismo en materias en su preámbulo: solo Geografía e Historia, que ya se había anunciado en diciembre 2023 [Los colegios bilingües de la Comunidad de Madrid impartirán la asignatura de Geografía e Historia en español desde el curso 2024/25 ](https://www.comunidad.madrid/noticias/2023/12/19/colegios-bilingues-comunidad-madrid-impartiran-asignatura-geografia-e-historia-espanol-curso-202425)

[Proyecto de decreto](https://www.comunidad.madrid/transparencia/sites/default/files/23._decreto_audiencia.pdf)  
> La presente modificación persigue tres objetivos principales: **la reducción de las áreas y materias que pueden impartirse en lengua extranjera en la Comunidad de Madrid**, la flexibilización de las opciones de cuarto curso de la Educación Secundaria Obligatoria para
los alumnos que cursen la sección bilingüe y la introducción de nuevos contenidos en la
materia de Geografía e Historia que se imparte a los alumnos de la Educación Secundaria
Obligatoria.  
**Las áreas y materias que no se impartirán en lengua extranjera son: el área de Ciencias Sociales de Educación Primaria, la materia de Geografía e Historia de la Educación Secundaria Obligatoria, así como las materias de Historia de España e Historia del Mundo Contemporáneo del Bachillerato**. La exclusión de estas áreas y materias requiere un ajuste en
el marco legal establecido en la ordenación y currículo de las diferentes etapas incorporando
la referencia de las citadas áreas y materias en el artículo 9 del Decreto 61/2022, de 13 de
julio; en el artículo 16 y la disposición adicional segunda del Decreto 65/2022, de 20 de julio;
y en el artículo 19 y la disposición adicional segunda del Decreto 64/2022, de 20 de julio, en
los que se contemplan las áreas o materias que no podrán impartirse en lengua extranjera,
tanto en el ámbito de la autonomía de los centros como en la implantación de los programas
bilingües institucionales de la Comunidad de Madrid. No obstante, excepcionalmente, en los
grupos de sección lingüística de la Educación Secundaria Obligatoria y Bachillerato podrá
autorizarse la impartición de estas materias en francés o alemán.

En el articulado lo que fija es ampliar las materias que se excluyen de poder ser impartidas en inglés incluyendo "Geografía e Historia"  

Primaria, Decreto 61/2022  
> ... impartir las áreas del currículo, **a excepción** de Matemáticas, Lengua Castellana y Literatura **y Ciencias Sociales**, en lengua extranjera

Secundaria, Decreto 65/2022  
> ...impartir alguna materia del currículo en una lengua extranjera **a excepción de Geografía e Historia**, Latín, Lengua Castellana y Literatura, Matemáticas y Segunda Lengua Extranjera

Bachillerato, Decreto 64/2022  
> impartir alguna materia del currículo en una lengua extranjera **a excepción de Historia de España, Historia del Mundo Contemporáneo**, Griego I y II, Latín I y II, Lengua Castellana y Literatura I y II, Matemáticas I y II, Matemáticas Aplicadas a las Ciencias Sociales
I y II, Matemáticas Generales y Segunda Lengua Extranjera I y II.»

Al mismo tiempo, añade una excepción, permintiéndolo para francés y alemán tanto en Secundaria como en Bachillerato

> Excepcionalmente, en las secciones lingüísticas de francés y alemán, se podrá autorizar la impartición de la materia de Geografía e Historia en el idioma francés o alemán respectivamente.

> Excepcionalmente, en las secciones lingüísticas de francés y alemán, se podrá autorizar
la impartición de las materias de Historia de España y de Historia del Mundo Contemporáneo
en el idioma francés o alemán correspondiente a la sección.

Respecto a la argumentación, hay que ir a [MAIN MEMORIA EJECUTIVA DEL ANÁLISIS DE IMPACTO NORMATIVO](https://www.comunidad.madrid/transparencia/sites/default/files/24._main_audiencia.pdf)

> 3.2. Principales novedades introducidas por la norma propuesta.  
En la adaptación del programa bilingüe de la Comunidad de Madrid a los cambios derivados de
la LOMLOE se ha revisado la estructura y organización el programa bilingüe de la Comunidad de
Madrid para su adecuada adaptación al nuevo sistema educativo.  

Usar LOMLOE como excusa es ridículo

> El Plan para una Educación Libre, Plural y de Calidad que está llevando a cabo el Gobierno de
la Comunidad de Madrid para mejorar la calidad de la enseñanza en esta legislatura y en el que se
recoge que **se ha detectado un detrimento de los contenidos impartidos en el área de Ciencias Sociales de la Educación Primaria, la materia de Geografía e Historia de la Educación Secundaria Obligatoria y las materias de Historia de España y de Historia del Mundo Contemporáneo del Bachillerato en los grupos de alumnos que reciben enseñanzas de estas áreas y materias en lengua extranjera con respecto a aquellos alumnos que utilizan como lengua vehicular el español**. 

Decir que se ha detectado sin decir documentar cómo es absurdo. Con la misma argumentación se puede decidir cambiar de nuevo en cualquier sentido en cualquier momento

La mención al Plan para una Educación Libre, Plural y de Calidad es al programa electoral del PP [Reto 3 POR UNA EDUCACIÓN LIBRE, PLURAL Y DE CALIDAD](https://yoconayuso.es/assets/front/download/extracto_reto3_PE_2023.pdf)  
> 3.19. Reformularemos la carga lectiva en inglés del Programa
bilingüe de la Comunidad de Madrid, de forma que
**la historia se impartirá en español, tanto en primaria como en secundaria**, en los centros educativos
bilingües de la Comunidad de Madrid.


> **La comunidad educativa ha expresado que impartir estas materias en lengua extranjera afecta al nivel de comprensión de los alumnos y reduce la profundidad de los contenidos abordados.**

Decir que "la comunidad educativa ha expresado" sin documentar cómo es absurdo. Me recuerda el cambio en normativa sobre judaísmo, que acabaron argumentando con enlaces a artículos de prensa.

> La LOMLOE introduce un enfoque competencial de la educación que en determinadas materias
genera la exigencia de utilizar como lengua vehicular el español, con el fin de facilitar el desarrollo
y adquisición de las competencias clave. Esto se observa especialmente en las referidas áreas y
materias en las que el uso del lenguaje escrito y oral con un nivel de competencia lingüística de B1
o B2 limita en exceso la profundidad de los contenidos que puedan trabajarse y las alternativas de
comunicación apoyadas en elementos gráficos y otras formas de expresión que, **a diferencia de otras materias, no resultan suficientes para completar la información que debe alcanzar el alumno**. 

Decir que "el uso del lenguaje escrito y oral con un nivel de competencia lingüística de B1 o B2 limita en exceso la profundidad de los contenidos que puedan trabajarse y las alternativas de comunicación apoyadas en elementos gráficos y otras formas de expresión" solo en "Geografía e Historia" pero no "en otras materias", "a diferencia de otras materias, sin documentar qué materias y por qué en ellas es absurdo. 

> No obstante, en las secciones lingüísticas de francés o alemán, de forma excepcional, los
directores de los centros podrán solicitar la autorización para impartir la materia de Geografía e
Historia de la ESO, así como las materias de Historia de España y de Historia del Mundo
Contemporáneo de Bachillerato en la lengua extranjera correspondiente a la sección lingüística,
justificando esta medida en función del contexto del centro educativo y del perfil del alumnado que
tenga escolarizado y, una vez analizado cada caso particular, resolver lo que proceda. Con esta
excepción se permite dar respuesta a determinados grupos de sección lingüística cuyo alumnado y
profesorado cuentan con un nivel de competencia lingüística suficiente para alcanzar los objetivos
de la materia en las mismas condiciones que si fuera impartida en español.

Decir que se permite en francés o alemán de forma excepcional según el contexto y analizado cada caso pero no en inglés es igual de absurdo.

> Esta modificación no implica reducir el aprendizaje del idioma extranjero, ya que el programa
establece el mínimo de horas que se impartirán usando como lengua vehicular la lengua extranjera
y, ese número no se verá afectado. Simplemente acota las materias que deberán impartirse en
español, sin perjuicio de que todas las demás tengan la posibilidad de impartirse en lengua
extranjera.

Decir que se acota una materia pero no las demás sin argumentarlo es absurdo. 

Decir que el número de horas no se verá afectado pero reducir las horas de "Geografía e Historia" implica reconocer que en todos los centros en los que se estaba impartiendo el mínimo de horas que el programa establece incluyendo "Geografía e Historia" implicará impartir materias en inglés que no se estaban impartiendo antes. 

"Geografía e Historia" era una de las más impartidas y en ella se ha detectado el detrimento: sin un análisis documentado, se puede llegar a la misma conclusión en otras materias pasado un tiempo cuando el daño del detrimento ya se haya producido.

Si se mira más documentación del trámite:

* [FAPA Giner de los Ríos](https://www.comunidad.madrid/transparencia/sites/default/files/explicacionvotofapa.pdf) indica  
> necesidad de realizar una evaluación rigurosa del Proyecto Bilingüe por parte de la Administración educativa."

* [CEIM Confederación Empresarial de Madrid-CEOE](https://www.comunidad.madrid/transparencia/sites/default/files/voto_ceim_sin_firma.pdf) indica que no observan disminución de calidad en centros privados y no quieren que se les limite. 

* [Colegio Oficial Doctores y Licenciados en Ciencias y en Letras](https://www.comunidad.madrid/transparencia/sites/default/files/voto_colegio_oficial_sin_firma.pdf) indica  
> Consideramos que el Decreto adolece de motivación fundamentada
metodológicamente, epistemológicamente o técnicamente en estudios e informes
científicos en lo que se refiere a las consecuencias del estudio de la materia Geografía e
Historia en una lengua extranjera

* [FERE-CECA, escuelas católicas](https://www.comunidad.madrid/transparencia/sites/default/files/voto_particular_fere_modific_curriculo_sin_firma.pdf) indica 
> Que elproyecto de decreto dictaminado adolece de la necesaria reflexión y
justificación que motive supuesta en marcha

* [FERE-CECA, escuelas católicas solicita INCORPORACIÓN AL PROYECTO DE DECRETO DICTAMINADO DE UNA
DISPOSICIÓN QUE MODIFIQUE LA ORDEN 763/2015](https://www.comunidad.madrid/transparencia/sites/default/files/voto_particular_fere_modific_orden_735_sin_firma.pdf) indica  
> Que al restringir la posiblidad de impartir ... se ven obligados a que el profesorado encargado deba impartir otra materia para mantener el requisito de dos materias impartidas en lengua extranjera.  
En el caso de Primaria, la alternativa es (si no se viene impartiendo ya) la Música y la
Educación Física (que requieren especialización para tales materias) o **Ciencias Naturales (que la mayoría de los centros ha descartado por su carácter complejo y científico)**. Y en el
caso de la ESO, la especialización del profesor de Geografía e Historia impide que imparta
otras materias al carecer de titulación.

* [CCOO sobre inadmisión FERE](https://www.comunidad.madrid/transparencia/sites/default/files/vpccooinadmisionfere_sin_firma.pdf) indica  
> Esta misma modificación tendría que introducirse en las órdenes reguladoras del
Programa bilingüe en los centros públicos de la Comunidad de Madrid, en las
diferentes etapas.  

En la Asamblea 7 de marzo 2024 el consejero cita otra "argumentación"

En vídeo dice "(antes) no todos teníamos acceso a contenidos en inglés con las nuevas plataformas"  
[twitter EVicianaDuro/status/1765789860470616300](https://twitter.com/EVicianaDuro/status/1765789860470616300)

### Solicitud de transparencia

El 4 marzo 2024 realizo una solicitud de transparencia, que no va a tener respuesta antes de modo que pueda incorporarla a la alegación
> 09-OPEN-00040.1/2024  
Solicito copia o enlace a los análisis o documentación que fundamentan las afirmaciones realizadas en el Proyecto de decreto por el que se modifican el Decreto 61/2022, por el que se establecen ordenación y currículo etapa Ed. Primaria, el Decreto 65/2022, por el que se establecen ordenación y currículo ESO y el Decreto 64/2022, orden. y cur. Bachillerato. En caso de no existir, indicación de que no existe.  
Las afirmaciones que se realizan en MAIN asociadas a la solicitud son:  
>- "se ha detectado un detrimento de los contenidos impartidos en el área de Ciencias Sociales de la Educación Primaria, la materia de
Geografía e Historia de la Educación Secundaria Obligatoria y las materias de Historia de España y de Historia del Mundo
Contemporáneo del Bachillerato en los grupos de alumnos que reciben enseñanzas de estas áreas y materias en lengua
extranjera con respecto a aquellos alumnos que utilizan como lengua vehicular el español."  
>- "La comunidad educativa ha expresado que impartir estas materias en lengua extranjera afecta al nivel de comprensión de los
alumnos y reduce la profundidad de los contenidos abordados."  
>- "a diferencia de otras materias, no resultan suficientes para completar la información que debe alcanzar el alumno.", indicando en
este caso a qué otras materias se hace referencia.  

**3 abril 2024**

Recibo resolución que no responde a nada

[2024-04-03-ResoluciónBilingüismo.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Biling%C3%BCismo/2024-04-03-ResolucionBilinguismo.pdf)  
> El análisis y la fundamentación objetiva se ciñen a la intencionalidad final de la propuesta normativa
que es el refuerzo de la enseñanza de la Historia, siendo conscientes que esta se ve limitada al
impartirla en otra lengua que no sea el castellano.

Ver frase en [Presentación 2018, página 102](https://www.comunidad.madrid/sites/default/files/doc/educacion/sgea_eval_programabilingue_2018.pdf#page=102)  
> 2. Impartir asignaturas científicas y sociales en inglés no reduce el nivel de conocimientos y
competencias adquiridos en estas materias por los alumnos

**30 abril 2024**

Planteo reclamación a consejo: el límite es un mes y estaba esperando dictamen Comisión Jurídica Asesora, pero no está publicado


La respuesta no responde a lo solicitado, indica "extensa y prolija" y hace referencia a historiadores sin enlazar nada, cuando se pide documentación asociada a "detectado un detrimento", "la comunidad educativa ha expresado", "a diferencia de otras materias, no resultan suficientes"  
Existe documentación oficial de Madrid que indica lo contrario a lo que indica ese proyecto de decreto  
https://www.comunidad.madrid/servicios/educacion/evaluacion-programa-bilingue  
https://www.comunidad.madrid/sites/default/files/doc/educacion/sgea_eval_programabilingue_2018.pdf#page=102  
"Impartir asignaturas científicas y sociales en inglés no reduce el nivel de conocimientos y competencias adquiridos en estas materias por los alumnos"  
Procede citar el preámbolo de ley 19/2013 https://www.boe.es/buscar/act.php?id=BOE-A-2013-12887#preambulo  
"Sólo cuando la acción de los responsables públicos se somete a escrutinio, cuando los ciudadanos pueden conocer cómo se toman las decisiones que les afectan"  
y como ciudadano tengo derecho a conocer la documentación que explique cómo se ha tomado la decisión.  

**22 octubre 2024**

Recibo alegaciones de Madrid a la reclamación ante consejo RDACTPCM 163/2024  

El resumen es 

> En relación con la disconformidad con la respuesta que se le facilitó en su momento,
le informamos de que las afirmaciones señaladas en su solicitud inicial no se encuentran
en la Memoria de Análisis de Impacto Normativo final, que acompañó al referido
proyecto de decreto. Esta norma sufrió algunos cambios en el proceso de tramitación
hasta su publicación final en el BOCM, en la línea de mantener la impartición en lengua
extranjera de los contenidos directamente relacionados con las ciencias sociales, como
habrá podido comprobar y puede constatar en el portal de transparencia, ampliándose
la información sobre la cuestión planteada por el interesado. Asimismo, en ese
documento podrá encontrar los términos en los que se atendió su alegación presentada
en el citado portal.  

El 22 de octubre en documento con CSV xxx el en el que se indica que tengo un plazo máximo de 15 días para formular las alegaciones que considere oportunas a las alegaciones de la administración en TAUD RDACTPCM 163/2024 en documento CSV xxx  
En la solicitud que he reclamado ante consejo solicitaba "copia o enlace a los análisis o documentación que fundamentan las afirmaciones realizadas ...En caso de no existir, indicación de que no existe."  
Mi solicitud de transparencia no se altera porque digan que esas afirmaciones ya no estén en la MAIN final, ni altera que esas afirmaciones estaban en la MAIN, ni tampoco porque presentase alegaciones en el TAUD de dicha norma y digan que en MAIN se "atendiesen".  
En la MAIN final enviada a CJA tras TAUD indica "El análisis y la fundamentación objetiva se ciñen a la intencionalidad final de la propuesta normativa" lo que reafirma que hay un análisis y fundamentación que es lo solicitado.

Lo presento por registro 43/441492.9/24 


### Alegaciones

Texto planteado 8 marzo 2024

El preámbulo del decreto indica "la reducción de las áreas y materias que pueden impartirse en lengua extranjera en la Comunidad de Madrid" y se limita reducir Ciencias Sociales en primaria, Geografía e Historia en secundaria e Historia de España e Historia del Mundo Contemporáneo del Bachillerato.

Se limita a esas áreas y materias indicando en MAIN que "se ha detectado un detrimento" y "La comunidad educativa ha expresado que impartir estas materias en lengua extranjera afecta al nivel de comprensión de los alumnos y reduce la profundidad de los contenidos abordados." pero no se aporta ninguna documentación que respalde dichas afirmaciones. 
	
Decir que "el uso del lenguaje escrito y oral con un nivel de competencia lingüística de B1 o B2 limita en exceso la profundidad de los contenidos que puedan trabajarse y las alternativas de comunicación apoyadas en elementos gráficos y otras formas de expresión" solo en esas materias pero no "en otras materias", "a diferencia de otras materias, sin documentar qué materias ni por qué en ellas supone realizar un cambio normativo y educativo sin una fundamentación adecuada. Una afirmación similar igual de genérica en sentido opuesto, sin ser respaldada por documentación que la fundamente, fue la que permitió que se impartiesen las materias que ahora se limitan en inglés y puede permitir igualmente volver a impartir cualquier materia de nuevo en inglés, incluyendo las que ahora se excluyen. 

La política educativa debe partir de evidencias científicas, y su uso implica ser cuidadosos en la objetividad y en la fundamentación. 

Considero relevante citar que en https://www.comunidad.madrid/transparencia/sites/default/files/voto_particular_fere_modific_orden_735_sin_firma.pdf los representantes de la mayoría de centros privados afirman que tampoco es adecuado para Ciencas Naturales "que la MAYORÍA de los centros ha descartado (impartir en inglés) por su carácter complejo y científico"

Alegación 1: incluir referencias al análisis y fundamentación objetiva que demuestre el impacto en nivel de compresión y profundidad para las distintas materias, no solamente Ciencias Sociales en primaria, Geografía e Historia en secundaria e Historia de España e Historia del Mundo Contemporáneo del Bachillerato.

Alegación 2: que la modificación se haga para todas las materias en las que el análisis y fundamentación objetiva lo indique.  

En MAIN se indica que secciones lingüísticas de francés o alemán, de forma excepcional, justificando esta medida en función del contexto del centro educativo y del perfil del alumnado que tenga escolarizado y, una vez analizado cada caso particular, podrán ser una excepción e impartir en lengua extranjera. 

Alegación 3: incluir referencias al análisis y fundamentación objetiva que demuestre si aplicar una excepción "de forma excepcional, justificando esta medida en función del contexto del centro educativo y del perfil del alumnado que tenga escolarizado" tiene sentido solamente en francés o alemán pero no en inglés, o bien tiene sentido en los tres idiomas, o bien no tiene sentido en ninguno. 

Al excluirse Ciencias Sociales en primaria, Geografía e Historia en secundaria e Historia de España e Historia del Mundo Contemporáneo del Bachillerato, dado que la normativa obliga a impartir en secundaria UN TERCIO del horario en inglés (Artículo 6.2 de ORDEN 972/2017) puede suponer que se los centros deban impartan en inglés otras materias que antes se impartían en castellano. 
Esto enlaza con el análisis del impacto de impartir otras materias en inglés, como podría ser Física y Química obligatoria en 2º y 3º ESO.
 
Alegación 4: dado que excluir materias puede llevar a que se incorporen otras, incluir referencias al análisis y fundamentación objetiva que demuestre si procede aplicar la excepción a esas otras materias, y en caso de que esas otras materias también deban ser una excepción, se argumente y se plantee una posible reducción del porcentaje de materias en inglés. 

Presentada 8 marzo 2024 09/477452.9/24


**13 junio 2024**

Se publica en BOCM [DECRETO 59/2024, de 12 de junio, del Consejo de Gobierno, por el que se modifica el Decreto 61/2022, de 13 de julio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la etapa de Educación Primaria, el Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria, y el Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato.](https://bocm.es/boletin/CM_Orden_BOCM/2024/06/13/BOCM-20240613-2.PDF)

Queda en huella normativa [Decreto 59/2024,12 junio, por el que se modifican Decreto 61/2022, por el que se establecen ordenación y currículo etapa Ed. Primaria, el Decreto 65/2022, por el que se establecen ordenación y currículo ESO y el Decreto 64/2022, orden. y cur. Bachillerato](https://www.comunidad.madrid/transparencia/decreto-59202412-junio-que-se-modifican-decreto-612022-que-se-establecen-ordenacion-y-curriculo)  

En concreto [MAIN revisada enviada a CJA](https://www.comunidad.madrid/transparencia/sites/default/files/1_main_para_cja.pdf), en la que se cita la alegación y aparentemente se responde sin argumentar en absoluto

> Alegación (03) con nº de referencia 09/477452.9/24 presentada el 8 de marzo de 2024.  
El interesado alega que deberían incluirse análisis sobre todas las materias y no únicamente del
área de Ciencias Sociales en la Educación Primaria, la materia de Geografía e Historia en la ESO
y las materias de Historia de España e Historia del Mundo Contemporáneo en el Bachillerato para
que la modificación se haga en todas aquellas en las que el análisis y fundamentación objetiva lo
indique. Asimismo, que se analice el motivo de excepción en los casos de sección lingüística en
francés o alemán y, por último, que se incluya el análisis del impacto de impartir otras materias en
inglés como, por ejemplo, Física y Química en 2º y 3º de la ESO.  
El presente decreto tiene una finalidad concreta que es el refuerzo de la enseñanza de la Historia
de España en las distintas etapas educativas para una mejora en el conocimiento de la Historia en
la lengua castellana. El análisis y la fundamentación objetiva se ciñen a la intencionalidad final de
la propuesta normativa que es el refuerzo de la enseñanza de la Historia de España y su limitación
a no impartirla en lengua extranjera.  
En todo caso, se señala que, en atención a las alegaciones recibidas, se ha modificado el texto
definitivo de la norma, restringiéndolo, no solo a las materias que ya se incluían en los decretos que
se modifican, sino también a las materias y contenidos relativos a Historia de España que se añaden
en la presente modificación.  
Se incluye la excepcionalidad en las secciones lingüísticas de francés y alemán, bajo una
justificación precisa en función del contexto del centro educativo y del perfil del alumnado que tenga
escolarizado y, una vez analizado cada caso particular, resolver lo que proceda. Con esta excepción
se permite dar respuesta a determinados grupos de sección lingüística cuyo alumnado y
profesorado cuentan con un nivel de competencia lingüística suficiente para alcanzar los objetivos
de la materia en las mismas condiciones que si fuera impartida en español. Teniendo en
consideración, por ejemplo, si se imparte el programa de Bachibac correspondiente a la doble
titulación de Bachiller en los sistemas educativos francés y español. No se ha considerado
extenderlo a inglés que se encuentra bajo un programa bilingüe muy generalizado.




### Abandono del programa bilingüe

En algunas comunidades como Castilla-La Mancha se puede abandonar el programa, pero hasta donde sé no hay mecanismo en Madrid, aunque se cita en algunos documentos

[Resolución provisional proyectos bilingües y plurilingües a partir del curso 2018/2019](https://www.educa.jccm.es/es/sistema-educativo/idiomas-programas-europeos/noticias-novedades-idiomas/resolucion-provisional-proyectos-bilingues-plurilingues-par)  

[Anexo III. Autorización abandono](https://www.educa.jccm.es/es/sistema-educativo/idiomas-programas-europeos/noticias-novedades-idiomas/resolucion-provisional-proyectos-bilingues-plurilingues-par.ficheros/221969-Anexo%20III.%20Autorizaci%C3%B3n%20abandono.pdf)

[Hasta 90 de los 613 proyectos bilingües de la región van a abandonar este modelo](https://www.eldiario.es/castilla-la-mancha/90-613-proyectos-bilingues-region-abandonar-modelo_1_8367836.html)

[Los 90 coles públicos que abandonan el bilingüismo: "Los alumnos no piensan; sólo memorizan"](https://www.elespanol.com/reportajes/20210926/publicos-abandonan-bilinguismo-alumnos-no-piensan-memorizan/613690103_0.html)

El cambio de 2024 en Madrid podría llevar a que algunos centros abandonasen el programa de manera forzosa según [CCOO sobre inadmisión FERE](https://www.comunidad.madrid/transparencia/sites/default/files/vpccooinadmisionfere_sin_firma.pdf)  
> Todo lo anterior conduce a una situación cierta de no poder cumplir los requisitos
inicialmente previstos en la Orden 763/2015 y en las órdenes reguladoras del Programa
bilingüe en los centros públicos (adicionado) y, en consecuencia, **verse obligados a renunciar al carácter bilingüe autorizado en su momento**.

### Normativa 

[ORDEN 5958/2010, de 7 de diciembre, por la que se regulan los colegios públicos bilingües de la Comunidad de Madrid.](https://www.comunidad.madrid/sites/default/files/doc/educacion/orden_5958-2010_de_7_de_diciembre.pdf)  
> Tercero Organización de las enseñanzas  
Los alumnos de los Colegios Públicos Bilingües podrán cursar todas las áreas
del currículo de Educación Primaria en lengua inglesa a excepción de Matemáticas y Lengua Castellana y Literatura

[ORDEN 763/2015, de 24 de marzo, de la Consejería de Educación, Juventud y Deporte, por la que se regulan los centros privados concertados bilingües del ámbito de gestión de la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2015/04/09/BOCM-20150409-1.PDF)  
> Artículo 3 Características de la enseñanza bilingüe  
> 3. El Inglés y las áreas o materias impartidas en inglés ocuparán, al menos, el 30 por 100 del horario lectivo establecido con carácter general para cada etapa educativa

[ORDEN 972/2017, de 7 de abril, de la Consejería de Educación, Juventud y  Deporte, por la que se regulan los institutos bilingües español-inglés de la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2017/04/27/BOCM-20170427-11.PDF)  
> Artículo 5 Organización de las enseñanzas en la Vía Programa Bilingüe  
>2. Enseñanzas de otras materias en inglés:  
a) En el primer ciclo (1.º, 2.º y 3.º curso) se deberá impartir en inglés, al menos, una materia a elegir entre las siguientes:
1.º Materias específicas obligatorias.  
2.º Tecnología, Programación y Robótica.  
3.º Materias específicas opcionales o materias de libre configuración autonómica
a las que se refiere el artículo 6.4 del Decreto 48/2015, de 14 de mayo, a excepción de las siguientes: Segunda Lengua Extranjera, Recuperación de Lengua, Recuperación de Matemáticas y Ampliación de Matemáticas: Resolución de Problemas.  
b) En el cuarto curso se deberá impartir en inglés, al menos, una materia entre las siguientes:  
1.º Materias específicas obligatorias.  
2.º Materias específicas opcionales o materias de libre configuración autonómica
a las que se refiere el artículo 7.5 del Decreto 48/2015, de 14 de mayo, a excepción de Segunda Lengua Extranjera.  
c) También se podrán impartir en inglés las tutorías de los cuatro cursos de Educación Secundaria Obligatoria.

> Artículo 6 Organización de las enseñanzas en la Vía Sección Bilingüe  
> 2. Enseñanzas de otras materias en inglés: La enseñanza de la materia Primera Lengua Extranjera: Inglés, junto con la de las materias que se impartan en inglés, ocuparán, al
menos, un tercio del horario lectivo semanal.

Relacionado

[Madrid, comunidad bilingüe 2022-2023](https://www.comunidad.madrid/transparencia/sites/default/files/plan/document/bvcm050930.pdf)  
> Los colegios bilingües de la Comunidad de Madrid impartirán en lengua inglesa, al
menos, un 30 por 100 del horario lectivo  

> 4.1. Características de los institutos bilingües  
> El tiempo dedicado a esta materia, junto con el de las materias que se
imparten en inglés, supondrá, al menos, un tercio del horario lectivo semanal.

### Análisis y opiniones sobre bilingüismo

De momento recopilo aquí, ya iré separando y organizando

[The advantages of listening to academic content in a second language may be outweighed by disadvantages: A cognitive load theory approach](https://bpspsychub.onlinelibrary.wiley.com/doi/10.1111/bjep.12468)  
> Our results indicated that listening to the content in French before listening to it in a second language was beneficial for both content and language learning. In contrast, listening to content in a second language not only depressed content acquisition as is to be expected, but also depressed language acquisition. We discuss the relevance of cognitive load theory to frame learning tasks aimed at teaching content through a second language.

[El Bilingüismo - observatorio por la educación pública - oxep.org](http://oxep.org/el-bilinguismo/)  
> Contenido del Informe  
Introducción   
1 Ni aprendemos inglés ni aprendemos science  
2 Segregación explícita  
3 El coste del bilingüismo  
4 El negocio del bilingüismo  
5 Colonialismo mental  
6 Otra educación bilingüe es posible  
Bibliografía  

[Auge y declive de la ventaja bilingüe: un estudio bibliométrico](https://www.cienciacognitiva.org/?p=1560)  
> El fenómeno de la ventaja bilingüe (teoría que propone que los bilingües desarrollan mejores capacidades cognitivas debido al entrenamiento que supone usar dos lenguajes a diario) genera debate entre los científicos que lo investigan. En un estudio reciente hemos mostrado que se ha producido un cambio de tendencia en la publicación de artículos en este ámbito de la ciencia cognitiva, de modo que los artículos en contra de la existencia de esta ventaja superan ya a los que la defienden. Discutimos los resultados en relación con algunos aspectos controvertidos de la publicación en revistas científicas.

[La perversión de la investigación y el bilingüismo en Madrid](https://eldiariodelaeducacion.com/2017/05/04/la-perversion-de-la-investigacion-y-del-bilinguismo-en-madrid/)  
> El uso de las evidencias científicas para hacer política pasa primero por ser extremadamente cuidadosos para determinar qué constituye una evidencia y cuáles, en última instancia, pueden dar paso a políticas concretas.

[Evaluación Programa Bilingüe - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/evaluacion-programa-bilingue)  
[Fase I (diciembre 2016)](https://www.comunidad.madrid/sites/default/files/doc/educacion/sgea_eval_programabilingue_2016.pdf)  
[Presentación del informe final (13/06/2018)](https://www.comunidad.madrid/sites/default/files/doc/educacion/sgea_eval_programabilingue_2018.pdf)  
[Audio del Consejero de Educación e Investigación (13/06/2018)](https://www.comunidad.madrid/sites/default/files/doc/educacion/sgea_eval_bilingue_audio_rafael_vangrieken_-_programa_bilingue_de_la_cam.mp3)  

[La denuncia de una traductora sobre las clases de 'Science' en los colegios bilingües](https://www.elespanol.com/social/20191129/denuncia-traductora-clases-science-colegios-bilingues/447955904_0.amp.html)

[twitter solaportillo/status/1198967744382849025](https://twitter.com/solaportillo/status/1198967744382849025)  
Ayer estuve ayudando a una niña de 7 años a estudiar ciencias naturales. Su libro está ÍNTEGRAMENTE en inglés, por lo del bilingüismo. No puedo estar más en contra de esto y eso que, como traductora, conozco la utilidad de saber una segunda lengua.  
El esfuerzo que hacen los niños para aprender palabras en inglés (por cierto, MUY especializadas) en estas asignaturas los distrae de comprender el contenido en sí. Por muy esponjas que sean (y no siempre es el caso).  
Además, no entienden el texto de sus libros. NADA. Solo las palabras sueltas que les hacen aprender. Esto les impide leer y desarrollar la comprensión. Si habéis dado clase a niños, sabréis que esto de tener 0 comprensión lectora es algo bastante común y MUY grave.

[El Programa Bilingüe a examen. Un análisis crítico de sus fundamentos. Acción Educativa (2017)](http://accioneducativa-mrp.org/wp-content/uploads/2017/10/informe-bilinguismo-2017-low.pdf)

[EVALUATING A BILINGUAL EDUCATION PROGRAM IN SPAIN: THE IMPACT BEYOND FOREIGN LANGUAGE LEARNING. Brindusa Anghel, Antonio Cabrales, Jesus M. Carro](https://onlinelibrary.wiley.com/doi/10.1111/ecin.12305)

**28 septiembre 2016**  
[Los alumnos de centros bilingües en Primaria obtienen peores resultados](https://elpais.com/politica/2016/09/27/actualidad/1474977363_197142.html)

[Las sombras del bilingüismo](https://elpais.com/politica/2017/10/06/actualidad/1507284770_581444.html)
> Profesores, sindicatos y expertos cuestionan el programa estrella de los 17 Gobiernos autonómicos

**4 febrero 2017**  
[Bilingüismo: ni se aprende inglés ni ‘science’](https://www.cuartopoder.es/ideas/2017/02/04/bilinguismo-ni-se-aprende-ingles-ni-science-2/)

[English Impact Report - Madrid - britishcouncil 2017](https://www.britishcouncil.org/exam/english/aptis/research/publications/english-impact/madrid)

[Bilingual education in Spain - britishcouncil](https://www.britishcouncil.es/en/partnerships/success-stories/bilingual)

**17 diciembre 2018**  
[El ‘boom’ del bilingüismo llena las aulas de docentes que no dominan el inglés](https://elpais.com/sociedad/2018/12/05/actualidad/1544011044_830446.html)  
> Los expertos critican el uso de los estudiantes como banco de pruebas

[twitter Isabel_Gonzal/status/1074921044828450816](https://twitter.com/Isabel_Gonzal/status/1074921044828450816)  
Elegir ese sujeto para esa frase esconde una grave intencionalidad. Los impulsores del bilingüismo tienen nombre y apellidos y claros intereses. El profesorado lo sufre, no es el causante. Continuamos con una escuela acosada por una sociedad y unas instituciones enfermas.

[twitter JUc3m/status/1074624461226303488](https://twitter.com/JUc3m/status/1074624461226303488)  
Esto es GRAVÍSIMO (imaginad si fuesen docentes con deficiente alfabetización en español).  
Sus consecuencias finales las notaremos en varios años, cuando muchos de los "ingenieros sociales" que lo parieron estén jubilados o muertos.  
No habrá responsables.  
Lo más grave NO es lo que ya está pasando en las aulas (que sin duda es muy serio).  
Lo más grave es la caída pavorosa de la calidad de la enseñanza en las etapas clave para toda una generación.  
ESO es lo que se comenzará a manifestar como problema nacional dentro de unos 10 años.  
Pero éste es otro de esos problemas en los que el sesgo ideológico se impone a la racionalidad. Como este proyecto es un hijo político de Esperanza Aguirre y otros notables del PP, si eres conservdor tienes que estar a favor y si eres progresista tienes que estar en contra.  
Pero los hechos son durísimos.  
-Profesores que no ya "no dominan", sino que no tienen competencia profesional en inglés.  
-Alumnos que no pueden seguir en inglés a sus profesores, de modo que el nivel decae hasta "la pe con la a".  
-Prescindencia de exámenes escritos de desarrollo.  

[twitter jesusrogero/status/1074788394268532741](https://twitter.com/jesusrogero/status/1074788394268532741)  
Hoy la opinión de brocha gorda sobre educación le corresponde al editorial de El País. Culpa del fracaso de los programas bilingües al profesorado, cuando el problema está en el mismo diseño del modelo (lean este excelente informe [El Programa Bilingüe a examen. Un análisis crítico de sus fundamentos. Acción Educativa (2017)](http://accioneducativa-mrp.org/wp-content/uploads/2017/10/informe-bilinguismo-2017-low.pdf))

[twitter BabelRed21/status/1074792626140930054](https://twitter.com/BabelRed21/status/1074792626140930054)  
El País saca una editorial sobre bilingüismo el mismo día que saca un artículo sobre ese tema. El único inconveniente que le ve al sistema segregador y simplificador de contenidos es que el profesorado no está suficientemente preparado. Algo nos huele mal.

[Bilingual education in Spain: Achievements, challenges and criticism](https://www.researchgate.net/publication/342707658_Bilingual_education_in_Spain_Achievements_challenges_and_criticism)

[‘La Chapuza del Bilingüismo’, una visión crítica del Programa Bilingüe de la Comunidad](https://www.magisnet.com/2020/10/la-chapuza-del-bilinguismo-una-vision-critica-del-programa-bilingue-de-la-comunidad/)

[LA CHAPUZA DEL BILINGÜISMO. DOCUMENTAL](https://www.youtube.com/watch?v=LUfzaKdGVBY)  
> El documental ofrece una visión crítica del programa bilingüe instaurado en colegios e institutos de toda España, centrándose en el análisis del modelo de la Comunidad de Madrid. Impartir diferentes materias como Geografía, Historia o Ciencias Naturales únicamente en inglés, puede tener consecuencias muy negativas para el alumnado. Recoge los testimonios de docentes, familias y alumnos, a la vez que expone datos y conclusiones de diferentes estudios.

**19 noviembre 2021**

[Bilingüismo 'fake': familias y docentes, contra un sistema que según el 80% de su profesorado reduce el rendimiento ](https://www.eldiario.es/sociedad/bilinguismo-fake-familias-docentes-sistema-80-profesorado-reduce-rendimiento_1_8505963.html)

[Los defensores del bilingüismo: “Es un error pensar que los niños hablarán el inglés como el español”](https://elpais.com/educacion/2021-07-14/los-defensores-del-bilinguismo-es-un-error-pensar-que-los-ninos-hablaran-el-ingles-como-el-espanol.html)


**10 junio 2023**

[Los investigadores critican que la política educativa no siga la evidencia científica: “Un ejemplo son los programas bilingües” ](https://www.eldiario.es/sociedad/investigadores-critican-politica-educativa-no-siga-evidencia-cientifica-ejemplo-son-programas-bilingues_1_10279244.html)

**17 abril 2023**

[El fracaso de la educación bilingüe 'obligatoria' que confirman padres y profesores: “Ni aprenden inglés ni la materia” ](https://ileon.eldiario.es/sociedad-y-vida/fracaso-bilingue-obligatoria-confirman-padres-profesores-aprenden-ingles-materia_1_10119237.html)


[Aprender en los coles bilingües - archive.org](https://web.archive.org/web/20120313200924/http://padresycolegios.com/noticia/2826/)

Lo incluyo completo, ahora está en archive, pero por si desaparece

> Buenos días, les escribo porque soy una madre preocupada con la enseñanza bilingüe que está recibiendo mi hijo en un colegio público de la Comunidad de Madrid.  
Está en 3º de Primaria y ahora, el contenido de Science, –a la vista de los malos resultados en los exámenes de todos los alumnos de clase– se ha empezado a duplicar en castellano porque los profesores se están dando cuenta de que no aprenden como deberían. He de decir que él tiene Notables y Sobresalientes. Todos los padres coincidimos en ver que la temida Science se ha convertido en una asignatura que en lugar de "disfrutar" de ella, les está costando muchísimo. Tengo una gran preocupación cuando vaya a cursos superiores porque pienso que, a través del inglés que ellos van adquiriendo, no van a ser capaces de aprender los importantes contenidos que se derivan de esta materia. No hay que olvidar que en Secundaria se desdobla en Ciencias Naturales y Ciencias Sociales. Entonces mi consulta es: ¿Cómo debemos actuar los padres en este caso? ¿preparamos niños para que sepan mucho inglés pero poco Conocimiento del Medio? Por favor, les agradecería mucho si me pudieran orientar sobre cómo debemos hacerlo. Muchas gracias por su atención.  
Esther Domínguez (Madrid)  
El problema que usted plantea no es nuevo ni exclusivo de su colegio. Desgraciadamente es ya un fenómeno extendido en muchos de los llamados colegios bilingües, públicos y privados. Básicamente, lo que usted describe es la implantación de un método conocido por sus siglas en inglés, CLIL (Content and Language Integrated Learning), en virtud del cual se integran objetivos lingüísticos con contenidos de materias específicas.  
Este método tiene que ser muy riguroso en su puesta en práctica y la evaluación de los aprendizajes debe contemplar tanto los contenidos de las ciencias u otras asignaturas como de la lengua en las que se imparten, lo que lógicamente incrementa su dificultad de aprendizaje y pone a prueba la capacidad de planificación del profesor.  
En mi opinión, los padres deben apoyar al centro en esas iniciativas que intentan  suplir las carencias de los alumnos en contenidos curriculares, al mismo tiempo que deben exigir una educación bilingüe de verdad, es decir, que los alumnos conozcan la lengua extranjera en profundidad, que eso es lo que implica el bilingüismo en cualquiera de sus modalidades. También sería bueno cuestionar si es necesario para una formación bilingüe que el niño conozca mayoritariamente vocabulario técnico antes que vocabulario de la vida diaria. Por lo que usted dice, coincidiendo con muchos más padres en la Comunidad de Madrid, lo que se está dando no es un proceso de bilingüismo sino de monolingüismo, ya que el niño solo conoce los conceptos de esa asignatura en la lengua extranjera, si es que llega a adquirirlos.  
A raíz de la pregunta que usted plantea surgen varios puntos que merecen comentarse:  
Primero, si la metodología CLIL no se lleva a la práctica con una estricta planificación en la que el profesor tenga un absoluto control de ambos procesos de evaluación, o sea, del lingüístico y de los contenidos de la asignatura, lo más probable es que el alumno fracase en alguno de ellos.  
Segundo, el aprendizaje de las lenguas no se produce en todas las personas de forma similar e incluso hay personas, aunque sean niños, que tienen fuertes dificultades para aprenderlo, independientemente de su capacidad intelectual. La aptitud para el aprendizaje de idiomas no es universal. Por esta razón, el colegio debería tener prevista la diferenciación de los alumnos que pueden y que no pueden aprender unos contenidos correctamente en una segunda lengua y poner los medios para que esos contenidos los aprendan en su lengua materna.  
Tercero, en los dos primeros ciclos de primaria el niño español está consolidando su lengua materna, especialmente y con gran esfuerzo, la ortografía y la expresión escrita. Sin embargo un niño inglés, dadas las características de su idioma, todavía está empezando a expresarse en él por escrito a esa misma edad, ya que la dificultad fonética de su lengua le impide adelantar el proceso de adquisición de esta habilidad. Consecuentemente, a los niños españoles se les exige un nivel más adelantado de dominio de la lengua escrita que a un nativo en su propia lengua. Lo pedagógicamente aconsejable en cualquier caso es que se introduzca la escritura en la lengua extranjera con bastante posterioridad a su dominio en la lengua materna.  
Cuarto y enlazando con lo anterior, hay que decir que la metodología aplicada en algunas asignaturas en España se basa mucho en la escritura, la redacción de guías, la relación entre lo visual y lo escrito, etc., y la didáctica de las ciencias en edades similares en nativos de lengua inglesa es parcialmente diferente a causa, como decía en el párrafo anterior, de la dificultad ortográfica que ofrece la lengua inglesa, incluso, repito, para los mismos nativos. Si lo que ustedes buscan es que sus hijos tengan un buen nivel de inglés sería conveniente que el centro aumentara el número de horas de clase de lengua inglesa, no de contenidos curriculares en inglés, y que los dos primeros ciclos de la Primaria se centraran en un método de aprendizaje oral realmente eficaz a esas edades, por ejemplo, el método comunicativo.  
La lengua, a esas edades en las que el pensamiento abstracto no está del todo desarrollado, se aprende más rápidamente por interacción que por otros medios, por lo que es conveniente hacer que el alumno se vea implicado en actos de comunicación auténticos, en los que necesite vocabulario real en situaciones reales de comunicación. Además, el alumno de Primaria está en un momento óptimo para la comprensión oral y la discriminación fonética, por lo que dedicar tiempo a otras habilidades para las que no está preparado psicológicamente es perder un potencial grande de aprendizaje y, posiblemente, aumentar la frustración y el rechazo ante el aprendizaje de idiomas. Bien es cierto que el sistema educativo español es muy rígido a la hora de hacer cambios en la distribución horaria de las asignaturas, por eso es por lo que se ha adoptado la decisión de no aumentar horas de inglés y transformar algunas materias en clases de idiomas. Ese enfoque erróneo y poco eficaz es el que está provocando cada día un mayor rechazo a los planes de bilingüismo.  
Carlos Segade Alonso Doctor en Filología Inglesa. Director del Dpto. de Didácticas Aplicadas CUV (UCM)


### Bilingüismo y Física y Química

Es algo adiciona a los análisis generales. Pongo solo algunas ideas, por ampliar. 

Impartí Física y Química en castellano en 2º ESO en un centro bilingüe en el que los alumnos habían cursado Science en 1º, y en una evaluación inicial 1/3 de la clase no me sabía poner fusión en castellano y puso melting.

El bilingüismo real supone por ejemplo aprender dos veces los nombres de la tabla periódica y dos veces nomenclatura. En ESO esa duplicidad hace inviable tratar todo. 

Existen conceptos con nombres muy distintos, y si no se enseñan en los dos idiomas, el alumno no es bilingüe ni puede asociarlo. La normativa habla de solo hablar en inglés y no usar castellano. Por ejemplo flotabilidad (en 4º ESO) es buoyancy.

En uno de los artículos se cita

> También sería bueno cuestionar si es necesario para una formación bilingüe que el niño conozca mayoritariamente vocabulario técnico antes que vocabulario de la vida diaria. Por lo que usted dice, coincidiendo con muchos más padres en la Comunidad de Madrid, lo que se está dando no es un proceso de bilingüismo sino de monolingüismo, ya que el niño solo conoce los conceptos de esa asignatura en la lengua extranjera, si es que llega a adquirirlos.

> la didáctica de las ciencias en edades similares en nativos de lengua inglesa es parcialmente diferente a causa, como decía en el párrafo anterior, de la dificultad ortográfica que ofrece la lengua inglesa, incluso, repito, para los mismos nativos. 


