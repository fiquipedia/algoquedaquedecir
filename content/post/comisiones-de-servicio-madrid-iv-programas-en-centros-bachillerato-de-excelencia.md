+++
author = "Enrique García"
title = "Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia"
date = "2023-02-25"
tags = [
    "normativa", "Madrid", "educación", "post migrado desde blogger", "comisiones de servicio", "Excelencia bachillerato"
]
toc = true

description = "Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia"
thumbnail = "https://pbs.twimg.com/media/Fod3h80XoAMcyLY?format=png"
images = ["https://pbs.twimg.com/media/Fod3h80XoAMcyLY?format=png"]

+++

Revisado 18 enero 2025

## Resumen

En Madrid existe el programa de Bachillerato de excelencia desde 2012 con dos modalidades: centros de excelencia (solo el IES San Mateo) y aulas de excelencia (con varios centros). La provisión especialmente en el IES San Mateo se realiza de manera opaca, ver detalles con [Comisiones de Servicio Madrid (IV): programas en centros](https://algoquedaquedecir.blogspot.com/2020/06/comisiones-servicio-madrid-vi-programas-en-centros.html)

Creo post separado del anterior ya que tras insistir desde 2017 para conseguir más transparencia en las comisiones de servicio, y conseguir en 2023 vía Consejo de Transparencia y Participación que deban publicar las fechas de provisión de las comisiones del IES San Mateo, que tiene plantilla orgánica cero (toda su plantilla son comisiones) [RDA007/2023](https://drive.google.com/file/d/1rF8UKgTvN6vE-eHp34xTWK178wmO6OPS/view)

En febrero 2023 surge un borrador de orden sobre la provisión de puestos en Bachillerato de Excelencia Madrid, que se acaba aprobando en febrero 2024.

Creo post separado sobre el [Programa de excelencia de bachillerato Madrid](../programa-excelencia-bachillerato-madrid)  

Ver [Comisiones de servicio. Normativa y jurisprudencia](../comisiones-de-servicio-normativa-y-jurisprudencia)  

## Detalle

Me centro en la provisión: en otros posts sobre comisiones de servicio hay información sobre número de comisiones de servicio en otros programas, pero en el programa de excelencia, y en especial en el único centro de excelencia, IES San Mateo, la opacidad es especial.

A nivel personal desde 2017 busco transparencia en las comisiones de servicio, y creo que las comisiones de servicio del IES San Mateo es un caso especial: tiene plantilla orgánica cero desde su creación, todos están en comisión. Al tiempo es un centro "atractivo para docentes con muchos méritos", pero no lo pueden pedir porque no se ofrece en la asignación de destinos ni en concurso de traslados, la asignación de las comisiones es opaca. Forzando a que haya provisión vía concurso, es proable que a alguien que lo haya solicitado con méritos objetivos y no se lo den acabe judicializándolo, y una sentencia del TS oblique a baremo público como obligó a convocatoria de comisiones sentencia TS de 2019.

### Comisiones en proyecto de excelencia hasta marzo 2023 

En el post [Programa de excelencia de bachillerato Madrid](../programa-excelencia-bachillerato-madrid) hay información histórica que puede tener relación con las comisiones. 

**30 abril 2011**

[Un catedrático de Latín dirigirá el Centro de Bachillerato de Excelencia de Madrid - telemadrid](https://www.telemadrid.es/noticias/madrid/Latin-Centro-Bachillerato-Excelencia-Madrid-0-1240975911--20110430032351.html)
>  COMISION DE EXPERTOS  
Figar ha anunciado la creación de una Comisión Interuniversitaria de expertos, que a partir de ahora se encargará de asesorar en temas académicos y **seleccionar la totalidad del claustro de profesores del Centro del Bachillerato de Excelencia**.  
Esta Comisión está presidida por el director general de Universidades e Investigación de la Comunidad de Madrid, catedrático de Literatura española, poeta, novelista y ensayista, Jon Juaristi. Además cuenta con los siguientes miembros: la académica de la Real Academia Española de la Lengua y la Academia de las Ciencias de EE UU, Margarita Salas; el académico de la Real Academia de la Historia y Premio Nacional de Traducción, Luis Alberto de Cuenca; el historiador y catedrático de Historia en la Universidad Complutense, Juan Pablo Fusi, entre otros.  
La Comisión valorará aspectos como el expediente académico, los conocimientos en idiomas, experiencia docente en el extranjero, la preparación de alumnos participantes en Olimpiadas matemáticas internacionales o haber impartido clases a estudiantes con Premios extraordinarios.  
**La Comisión llevará a cabo la selección del claustro** para cubrir las plazas de profesores de Lengua, Matemáticas, Inglés, Ciencias Naturales, Historia, Educación Física, Filosofía, Física y Química, Latín y Griego, Dibujo Técnico y Segunda lengua extranjera.

**29 septiembre 2017**

[Profesores a dedo y presiones para inflar notas: la otra cara del bachillerato ‘excelente’ - elpais](https://elpais.com/politica/2017/09/20/actualidad/1505897225_897958.html)  
> La arbitrariedad en la contratación del profesorado está siendo investigada por el Defensor del Pueblo tras una denuncia de Ángel de Andrea González, profesor universitario y de educación secundaria de Física, que trabajó en el centro en el curso 2015-2016. Consiguió el puesto, como la mayoría de los profesores, enviando su currículum directamente a Silvestre. Al terminar, el director decidió no renovar su **comisión de servicios** alegando reducción de plantilla, pero en realidad le sustituyó por otra persona el curso siguiente.  
Esta forma de contratación, amparada por la ley pero excepcional, no se aplica en ningún otro de las 13 aulas de bachillerato de excelencia que se abrieron tras el experimento inicial del San Mateo, ni en iniciativas similares puestas en marcha en otras comunidades, como Murcia o Castilla y León, donde los profesores forman parte de la plantilla previa de los centros.

**16 julio 2018**  

[Informe anual Defensor del Pueblo 2018: Anexos](https://www.defensordelpueblo.es/wp-content/uploads/2019/06/E-1-Recomendaciones.pdf)  
Se cita Recomendación de fecha de salida 16/07/2018  
Con texto asociado a queja nº 16017048  
[Duración máxima de las comisiones de servicio de los funcionarios - defensordelpueblo](https://www.defensordelpueblo.es/resoluciones/duracion-maxima-de-las-comisiones-de-servicio/)  

**18 julio 2018**

[El Defensor del Pueblo recomienda a Educación cambiar el modo de contratación de los docentes del IES San Mateo](https://cadenaser.com/emisora/2018/07/18/radio_madrid/1531934665_802861.html)  
Cuestiona la situación administrativa de las comisiones de servicio, situación en la que se encuentra la plantilla de este centro que es paradigma de un modelo de excelencia impulsado por Esperanza Aguirre en 2011. Considera que la Administración está haciendo una interpretación inadecuada de la ley, que considera estas comisiones como algo excepcional y de duración limitada.

**20 julio 2018**

[Las contrataciones a dedo del IES San Mateo, denunciadas por el Defensor del Pueblo ](https://www.eldiario.es/madrid/somos/malasana/las-contrataciones-a-dedo-del-ies-san-mateo-denunciadas-por-el-defensor-del-pueblo_1_6414938.html)  
 El centro público lleva siete años contratando y despidiendo profesores sin crear plazas fijas, como si fuera una institución privada 

**23 noviembre 2018**

Tras preguntar confirman que no van a ofertar plazas del IES San Mateo en Concurso de Traslados, y comentan la respuesta dada al Defensor del Pueblo mencionando que harán normativa y concurso (la normativa se aprueba en febrero de 2024)  
[2018-11-23 Respuesta RR HH San Mateo 2018 anonimizado](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2018-11-23-Respuesta_RR_HH_San_Mateo_2018-anonimizado.pdf)  
> En respuesta a la recomendación formulada por Defensor del Pueblo en el expediente
de Queja nº 16017048, promovido por el solicitante, y al que alude este en su escrito de solicitud,
esta Dirección General informó a dicha Institución que:  
“Analizada la motivación jurídica expuesta por esta institución, la Administración considera
conveniente atender la Recomendación formulada, por lo que comunica que procederá a iniciar el
trabajo dirigido a la elaboración del concurso correspondiente teniendo en cuenta los requisitos de
capacidad y especialización que debe cumplir el personal docente que presta servicio en el instituto
San Mateo".

**22 diciembre 2021**

[twitter FiQuiPedia/status/1473688195682512909](https://twitter.com/FiQuiPedia/status/1473688195682512909)  
![](https://pbs.twimg.com/media/FHOXmmfWUBAeesn?format=jpg)  
[2021-12-22 Resolución SanMateo.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2021-12-22-ResolucionSanMateo.pdf)

[twitter FiQuiPedia/status/1473689862792429579](https://twitter.com/FiQuiPedia/status/1473689862792429579)  
El problema no es la opacidad de una administración que no pone ni intención ni recursos en la transparencia: el problema es que un ciudadano hace 12 solicitudes en 5 años y consume recursos 🤦‍♂️  
Con un par, @eossoriocrespo

**17 febrero 2022**  
[twitter FiQuiPedia/status/1494353595633971207](https://twitter.com/FiQuiPedia/status/1494353595633971207)  
Alegaciones @educacmadrid a mi reclamación CTyP sobre comisiones servicio IES San Mateo.  
Facilitarme fechas de concesión comisiones actuales supondría colocarme "en una posición de ventaja frente a otros posibles interesados en el IES San Mateo"🤦‍♂️  
Publicadas nadie tendría ventaja  

[2022-02-08-Alegaciones RDACTPCM056-2021 San Mateo.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2022-02-08-AlegacionesRDACTPCM056-2021-SanMateo.pdf)  

**2 febrero 2023**

[Resolucioń RDA007/2023 a reclamación RDACTPCM056/2021 Información reclamada: Fecha de concesión inicial y tipo de las comisiones de servicio de la plantilla de personal del IES San Mateo, así como nombres y apellidos de los docentes que las tienen concedidas.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/algoquedaquedecir/ComisionesServicio/Madrid/2022-12-29-7_ALEGACIONES_ReclamacionCTPCM324_2022_E.G.S.pdf)

**7 marzo 2023**

Recibo [ejecución RDA007/2023 resolución RDACTPCM56_2021 y se ven comisiones desde 2011](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2023-03-07-Ejecuci%C3%B3n_Resoluci%C3%B3n_CTYPC_Reclamaci%C3%B3n+RDACTPCM56_2021.pdf)

Documento 2021 muestra que mintieron
[2021-07-05 Respuesta Madrid Comisiones Servicio Defensor Pueblo 2_20028606.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2021-07-05-RespuestaMadridComisionesServicioDefensorPueblo2_20028606.pdf)
> "podrán ser renovadas…en tanto se consolida la plaza para ser ofertada…máximo 4+2años…conforme se vaya terminando se procederá a su convocatoria"

**8 mayo 2023**

Se convocan comisiones de servicio 2023-2024 y no se cita San Mateo  
[08/05/2023 - CONVOCATORIA PARA DAR PUBLICIDAD A LAS COMISIONES DE SERVICIOS EN PROGRAMAS EDUCATIVOS EN CENTROS PUBLICOS DOCENTES NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID EN EL CURSO 2023-2024](https://www.educa2.madrid.org/web/direcciones-de-area/dat-capital/-/visor/08-05-2023-convocatoria-para-dar-publicidad-a-las-comisiones-de-servicios-en-programas-educativos-en-centros-publicos-docentes-no-universitarios-de-la-comunidad-de-madrid-en-el-curso-2023-2024)


### Proyecto decreto marzo 2023

En marzo 2023 me los envían como enlaces en web CCOO

[1.2. texto proyecto de decreto profesorado bachillerato excelencia.pdf](https://www.ccoo.es/cms/g/adj/readfile.php?v=OTEzNTMucGRmJTdFMS4yLit0ZXh0bytwcm95ZWN0bytkZStkZWNyZXRvK3Byb2Zlc29yYWRvK2JhY2hpbGxlcmF0bytleGNlbGVuY2lhLnBkZg==)

[1.3. main pd profesorado bach excel.pdf](https://www.ccoo.es/cms/g/adj/readfile.php?v=MV85MTM1My5wZGYlN0UxLjMuK21haW4rcGQrcHJvZmVzb3JhZG8rYmFjaCtleGNlbC5wZGY=)

El primer análisis del texto del proyecto, llama la atención:

Hay Artículo 3. Profesorado en la opción de centros de excelencia (que solo aplica al único centro de excelencia actual IES San Mateo)

separado de Artículo 4. Profesorado en la opción de aulas de excelencia (que aplica a todos los demás)

Es llamativo que artículo 3.3 cite expresamente "3. El profesorado que desempeñe sus funciones en los centros de excelencia lo hará en régimen de **comisión de servicios** , con reserva del puesto de origen." mientras que no se citan comisiones de servicios para aulas de excelencia.

Que artículo 3.2 cite "se proveerán mediante concurso de méritos convocado a tal efecto conforme a los principios de igualdad, mérito, capacidad y publicidad." suena bien, pero hay que ver la realidad, porque sin baremo público los méritos se valorarán como hasta ahora, de manera opaca.

Es llamativo que exista una disposición adicional solo para el actual profesorado del IES San Mateo que entró de manera opaca, que les blinda permanecer seis años en total. Eso ya se lo habían respondido al defensor del pueblo, y muestra la importancia de conocer las fechas de provisión de esos puestos que es lo que he conseguido en 2023.

> "Disposición transitoria. Profesorado designado en centros de excelencia con anterioridad a la entrada en vigor del presente Decreto."  
El profesorado designado para impartir docencia en el Programa de Excelencia en el Bachillerato en centros de excelencia antes de la entrada en vigor del presente Decreto,  continuará ocupando su puesto docente mientras no haya cumplido seis cursos continuados de permanencia en él. En el caso de que no se haya alcanzado ese límite, podrá ser renovado de acuerdo con lo establecido en el artículo 3.3 de este decreto. En el caso de que haya permanecido seis o más cursos continuados de docencia en el centro de excelencia, el profesor terminará el curso académico actual, no podrá ser renovado para el siguiente ni podrá tampoco participar en el concurso de méritos para ocupar esa misma plaza en el curso siguiente.

También se añade Disposición final primera. en la que se añade un apartado 3 al artículo 2 de Decreto 63/2012 para los alumnos que realicen el curso equivalente a cuarto de Educación Secundaria Obligatoria en un sistema educativo extranjero y no puedan presentarse a las pruebas de los premios extraordinarios de Educación Secundaria Obligatoria de la Comunidad de Madrid.

Por ver cuándo realizan el trámite de audiencia, a 25 febrero 2023 no está en [Audiencia e información - comunidad.madrid](https://www.comunidad.madrid/transparencia/normativa-planificacion/tramite-audiencia-e-informacion-publica) pero desde luego el comentario inicial es por qué hay provisión distinta para centros de excelencia (realmente IES San Mateo) y aulas de excelencia.

Leyendo por ejemplo artículo 5.3

> 3. El profesorado asignado a docencia en centros y aulas de excelencia podrá permanecer en las correspondientes plazas por un periodo continuado máximo de seis cursos académicos.  
No obstante, **en el caso del profesorado de aulas de excelencia con destino definitivo en el centro, no será de aplicación dicho límite** , aunque la renovación de su docencia en aulas de excelencia requerirá la evaluación positiva de la práctica docente por parte del Servicio de Inspección Educativa.

Esa frase están diciendo que no van a haber profesorado en centros de excelencia con destino definitivo en el centro y que por lo tanto siempre les será de aplicación el límite de 6 años.

Ideas:

- Si el límite es de 6 años ¿puede estar el director del IES San Mateo como docente más de 6 años?  
- Qué sentido tiene poner requisitos si ante una sustitución llega un interino ¿piensan poner requisitos a las sustituciones? 

### Proyecto decreto junio 2023

**14 junio 2023**

[Convocatoria Mesa Sectorial. Lunes 19 junio 2023. Proyecto de decreto del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid](https://feccoo-madrid.org/noticia:667069--Convocatoria_Mesa_Sectorial&opc_id=bd9793dee769dd04e517767e52c5631c)

Incluye [nuevo documento de proyecto de decreto](https://feccoo-madrid.org/42e80eb71dbad434da6c727a57f7aad9000063.pdf)

Llama la atención

> Disposición transitoria. Profesorado designado en comisión de servicios con anterioridad a la entrada en vigor del presente Decreto.  
El profesorado designado para impartir docencia en el Programa de Excelencia en comisión de servicios con anterioridad a la entrada en vigor del presente Decreto podrá continuar ocupando su puesto docente en el Programa, iniciándose el cómputo de permanencia en el Programa a que hace referencia el artículo anterior el curso siguiente a la entrada en vigor del presente decreto.

que supone un indulto a medida para las comisiones del IES San Mateo que incumplen el límite de 6 años.

### Trámite de audiencia julio 2023 (y decreto febrero 2024)

[Proyecto de Decreto, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid.](https://www.comunidad.madrid/transparencia/proyecto-decreto-del-consejo-gobierno-que-se-regula-provision-del-profesorado-del-programa)

Alegaciones de 28 julio a 21 agosto

Cuando empieza a mirarlo veo esto:

En [la página del trámite de audiencia](https://www.comunidad.madrid/transparencia/proyecto-decreto-del-consejo-gobierno-que-se-regula-provision-del-profesorado-del-programa) hay publicados dos documentos distintos de proyecto de decreto:  

[decreto_11_julio_tras_informes.pdf](https://www.comunidad.madrid/transparencia/sites/default/files/decreto_11_julio_tras_informes.pdf) que indica en metadatos 11 julio 2023

[decretoversionpdf.pdf](https://www.comunidad.madrid/transparencia/sites/default/files/decretoversionpdf.pdf) que indica en metadatos 25 julio 2023

Igualmente existen en la página dos MAIN distintos publicados

[main_11_julio_tras_informes_sin_firma.pdf](https://www.comunidad.madrid/transparencia/sites/default/files/main_11_julio_tras_informes_sin_firma.pdf) que indica en portada Versión 03 11 de julio de 2023, 21 páginas

[mainenpdf.pdf](https://www.comunidad.madrid/transparencia/sites/default/files/mainenpdf.pdf) que indica en portada Versión 04 25 de julio de 2023, 22 páginas

Dado que hay dos documentos distintos y un usuario normal no tiene por qué saber consultar los metadatos ni comparar las diferencias, se puede considerar que la publicación del trámite es incorrecta porque no se indica al ciudadano sobre cuál de los dos debe realizar alegaciones, y enlazaría con la posible anulabilidad del trámite según [artículo 48.2 de Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas](https://www.boe.es/buscar/act.php?id=BOE-A-2015-10565#a48 ), ya que se puede considerar que produce “la indefensión de los interesados” 

Finalmente no lo considero en mis alegaciones porque las diferencias son mínimas.

Aunque en principio se puede considerar que se deben realizar alegaciones sobre la versión de proyecto 25 julio 2023, el documento de proyecto no indica internamente fecha, y se puede comprobar que la versión de 11 julio 2023 tiene aspectos más revisados que la versión de 25 julio 2023 (la versión de 11 de julio 2023 indica revisado con mayúsculas “Educación Secundaria” en lugar de “educación secundaria” que aparece en versión de 25 julio 2023, estando esa correción en dictamen Consejo Escolar de 16 marzo 2023)

En respuesta recibida 7 agosto a consulta con código 00267547 plateada el 31 julio se me indicó  
> Buenas tardes En relación a su consulta, le facilitamos respuesta de parte del organismo competente: "El fichero del proyecto de decreto y de Memoria de Análisis de Impacto Normativa para presentar alegaciones se encuentra en el apartado de audiencia. Le facilitamos las url que se utilizan para publicar, pero las versiones que tiene que consultar se encuentran dentro del hito “trámite de audiencia” que es al que tiene que ir para hacer las alegaciones. Tiene que consultar las versiones que salen dentro de ese trámite y no de trámites anteriores. Está de forma intuitiva en el portal" https://www.comunidad.madrid/transparencia/sites/default/files/decretoversionpdf.pdf https://www.comunidad.madrid/transparencia/sites/default/files/mainenpdf.pdf Saludos.”

Guardo una copia de documentos aquí:

[gitlab drive.fiquipedia ProgramaExcelenciaBachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/algoquedaquedecir/ProgramaExcelenciaBachillerato)

También veo que hay un voto particular del dictamen, no enlazado en la web de transparencia

[Dictámenes Consejo Escolar Madrid](https://www.comunidad.madrid/servicios/educacion/dictamenes)

[Voto particular CCOO decreto provisión profesorado programa excelencia comisión permanente 5/2023](https://www.comunidad.madrid/sites/default/files/vp_decreto_provision_profesorado_programa_excelencia_cp_5-2023.pdf)

Veo que documento [INFORME 9/2023 DE COORDINACIÓN Y CALIDAD NORMATIVA DE LA SECRETARÍA GENERAL TÉCNICA DE LA CONSEJERÍA DE PRESIDENCIA, JUSTICIA E INTERIOR SOBRE EL PROYECTO DE DECRETO POR EL QUE SE REGULA LA PROVISIÓN DEL PROFESORADO DEL PROGRAMA DE EXCELENCIA EN BACHILLERATO DE LA COMUNIDAD DE MADRID.](https://www.comunidad.madrid/transparencia/sites/default/files/inf_ofican_sin_firma_dec_peb_.pdf) cita normativa: alguna ya la citaba en el post general sobre comisiones de servicio.

A 17 agosto no veo todavía publicados informe de la Abogacía General de la Comunidad de Madrid ni el dictamen de la Comisión Jurídica Asesora de la Comunidad de Madrid

No consigo encontrar el primero (no hay buscador, uso Google)

[Búsqueda dictámenes 2023 Comisión Jurídica Asesora](https://www.comunidad.madrid/gobierno/informacion-juridica-legislacion/comision-juridica-asesora/dictamenes?search_api_views_fulltext=&field_cja_dic_asunto=All&field_cja_dic_tipo=All&field_cja_dic_year=45730&field_cja_dic_organo_consultante=All&field_cja_dic_tesauro=All)

**20 diciembre 2023**

[ACUERDOS CONSEJO DE GOBIERNO 20 de diciembre de 2023](https://www.comunidad.madrid/sites/default/files/231220_cg.pdf)  
> • Informe relativo a la solicitud de dictamen de la Comisión Jurídica
Asesora de la Comunidad de Madrid sobre el proyecto de decreto del
Consejo de Gobierno por el que se regula la provisión del profesorado del
Programa de Excelencia en Bachillerato de la Comunidad de Madrid.

**27 enero 2024** 

Tras ver que ya hay dictámenes Comisión Jurídica Asesora de 2024 pero no está publicado el de este proyecto, miro despacio y veo que han añadido documentos en [página del trámite de audiencia](https://www.comunidad.madrid/transparencia/proyecto-decreto-del-consejo-gobierno-que-se-regula-provision-del-profesorado-del-programa)  

23 octubre 2023  
* [Informe jurídico](https://www.comunidad.madrid/transparencia/sites/default/files/sj520_23sinfirmar.pdf)   
La abogacía reconoce que no sabe lo que está analizando, pero ven que la disposición transitoria es una barbaridad

> Aún desconociéndose por esta Abogacía la concreta casuística de los puestos
afectados, no podemos sino conminar a reconsiderar el contenido de esta Disposición
en lo que atañe al inciso ”iniciándose en el curso 2024-2025 el cómputo de permanencia
en el Programa a que hace referencia el artículo 5.”  
Habida cuenta de la regulación ex novo que contempla el Proyecto que nos
ocupa, resulta cuestionable aplicar el régimen de permanencia que contempla su
artículo 5 a puestos de trabajo que han sido provistos de acuerdo con otro procedimiento
-el contemplado en el artículo 6.2 de la Orden 11995/2012-. Se advierte, a tal efecto,
que el sistema diseñado en el Decreto proyectado constituye una regulación más
completa y cerrada que la prevista en el régimen actual.  
Todo ello, sin perjuicio de adoptar las cautelas necesarias en orden a garantizar
la continuidad y normal desarrollo del PEB en lo que se refiere a las funciones a
desempeñar por el profesorado destinado a este programa.  

La redacción que cuestiona abogacía 

> El profesorado designado para impartir docencia en el Programa de Excelencia con anterioridad a la entrada en vigor del presente Decreto, podrá continuar ocupando su puesto docente en el Programa, iniciándose en el curso 2024-2025 el cómputo de
permanencia en el Programa a que hace referencia el artículo 5.  

La nueva redacción es elimina "poner el contador a cero" 

> Disposición transitoria única. Profesorado designado con anterioridad a la entrada en vigor del
presente decreto.  
El profesorado designado para impartir docencia en el Programa con anterioridad a la entrada en vigor de este decreto, podrá continuar ocupando su puesto docente **por un curso más, tras el cual podrá solicitar la prórroga a que hace referencia el artículo 5.2.**  



* [ MAIN de 12 de diciembre de 2023 tras audiencia e informe jurídico.](https://www.comunidad.madrid/transparencia/sites/default/files/main_decreto_excelencia_2023-12-12.pdf)  
* [ proyecto de decreto de 5 de diciembre de 2023 posterior a audiencia e informe jurídico.](https://www.comunidad.madrid/transparencia/sites/default/files/decreto_excelencia_2023-12-05.pdf)  

> 6.8. Trámite de participación: audiencia e información pública  
Esta norma ha sido sometida al trámite de audiencia e información pública en cumplimiento de 
lo establecido en el artículo 60.2 de la Ley 10/2019, de 10 de abril, y el artículo 9 del Decreto
52/2021, de 24 de marzo, mediante resolución de 21 de julio de 2023 de la Directora General
de Educación Secundaria, Formación Profesional y Régimen Especial. La publicación en el Portal
de Transparencia de la Comunidad de Madrid ha tenido lugar el 27 de julio y el plazo de
alegaciones ha transcurrido entre el 28 de julio y el 21 de agosto de 2023, ambos días incluidos.
Se han recibido alegaciones de dos personas.  
D. …………………………… firma la recibida por registro de referencia 59/000181.9/23 y fecha 20 de
agosto de 2023. En el escrito, el Sr. ……………….. enumera siete alegaciones.  
> 1. «El proyecto de decreto cita artículo 2.1 de Real Decreto 1364/2010 para indicar que el
"concurso \[de traslados] es el procedimiento normal de provisión de las plazas o puestos
vacantes", pero luego el proyecto de decreto impide la provisión de algunas vacantes
con el procedimiento de concurso de traslados alegando una conveniente
provisionalidad para justificar un procedimiento excepcional que son las comisiones de
servicio» (p. 2). Su argumentación concluye que «se permita la provisión vía concurso
de traslados para los centros de excelencia, no limitando su provisión a comisiones de
servicio, de modo que el concurso de traslados sea el mecanismo normal como indica la
normativa básica» (p. 6).  
Cumple observar, ante esta alegación, lo expresado por la entonces Consejería de
Educación y Juventud, según escrito del Defensor del Pueblo, aportado por el alegante,
de referencia 21027900, de 2 de marzo de 2021 en relación con el expediente 20028606,
a saber, que «la Ley Orgánica 2/2006, de 3 de mayo, de Educación establece en su
Disposición adicional sexta que las Comunidades Autónomas ordenarán su función
pública docente, en el marco de sus respectivas competencias, respetando en todo caso,
las normas básicas contenidas en la misma, así como las bases del régimen estatutario
de los funcionarios públicos docentes.  
»El texto refundido de la Ley del Estatuto Básico del Empleado Público, aprobado por
Real Decreto Legislativo 5/2015, de 30 de octubre, establece en su artículo 81 que cada
Administración Pública en el marco de la planificación general de sus recursos humanos,
y sin perjuicio del derecho de los funcionarios a la movilidad, podrá establecer reglas
para la ordenación de la movilidad voluntaria de los funcionarios públicos cuando
considere que existen sectores prioritarios de la actividad pública con necesidades
específicas de efectivos.  
»Por su parte, el artículo 36 del Reglamento General de ingreso del personal al servicio
de la Administración General del Estado y de provisión de puestos de trabajo y
promoción profesional de los funcionarios civiles de la Administración General del
Estado, aprobado por el Real Decreto 364/1995, de 10 de marzo, establece también el
concurso como sistema normal de provisión de los puestos de trabajo adscritos a
funcionarios, al que se suman la libre designación, de conformidad con lo que
determinen las relaciones de puestos de trabajo en atención a la naturaleza de sus
funciones y, establece la posibilidad de cobertura temporal, en los supuestos previstos
en el Reglamento, mediante comisión de servicios y adscripción provisional.
»El artículo 64.1 dispone que ‘Cuando un puesto de trabajo quede vacante podrá ser
cubierto, en caso de urgente e inaplazable necesidad, en comisión de servicios de
carácter voluntario, con un funcionario que reúna los requisitos establecidos para su
desempeño en la relación de puestos de trabajo’.  
»El artículo 2.1 del Real Decreto 1364/2010, de 29 de octubre, por el que se regula el
concurso de traslados de ámbito estatal entre personal funcionario de los cuerpos
docentes contemplados en la Ley Orgánica 2/2006, de 3 de mayo, de Educación, y otros
procedimientos de provisión de plazas a cubrir por los mismos, establece que ‘el
concurso es el procedimiento normal de provisión de las plazas o puestos vacantes
dependientes de las Administraciones educativas, a cubrir por el personal docente’.  
»La comisión de servicios se ha configurado pues, como un mecanismo extraordinario
de provisión y movilidad de personal que permite cubrir un puesto vacante con un
funcionario que reúna los requisitos para su desempeño. En concreto, en el ámbito
docente las comisiones de servicio se han articulado como el cauce para dar respuesta
a situaciones de diversa índole, como pueden ser razones de salud especialmente grave
del docente, necesidad de formación de equipos directivos, atender a necesidades
específicas o singulares de programas concretos de índole académica o disponer de
personal especializado en puestos de la administración educativa.  
»En este sentido, cabría considerar la concesión de las comisiones de servicio como una
facultad de la administración, de acuerdo con sus atribuciones de autoorganización,
siendo en todo caso imprescindible la concesión motivada que garantice los principios
de mérito y capacidad, mediante la oportuna comprobación y valoración de que
concurren las causas que justifiquen la necesidad de la comisión, así como la
acreditación del cumplimiento por el candidato, de los requisitos legales para el
desempeño del puesto».  
Convendrá insistir, además, en que la adjudicación de los puestos de profesorado en
PEB no se determina en este proyecto de decreto por el procedimiento de libre
designación, sino por riguroso concurso de méritos, cuyo detalle corresponde a normas
de inferior rango.  
> 2. «La regulación de la permanencia en el programa se mezcla con la duración de las
comisiones de servicio, permitiendo el proyecto de orden un máximo de 6 años para las
nuevas comisiones e incluso 19 para las existentes, incumpliendo normativa de rango
superior que fija un máximo de 2 años» (p. 7). En consecuencia, se solicita que «las
comisiones de servicio se limiten a un máximo de 2 años tal y como fija normativa de
rango superior. Que la disposición transitoria única solo aplique a los docentes que no
hayan alcanzado los 2 años para no incumplir normativa de rango superior» (p. 10).
Esta alegación se responde con lo mismo que se ha sobredicho acerca de la primera.
Pueden añadirse convenientemente otras observaciones.  
La sentencia 710/2019 de 28 de mayo de 2019 de la Sala de lo contencioso-
administrativo, Sección cuarta, del Tribunal Supremo (recurso 246/2016) aprueba el
empleo por parte de la administración del «procedimiento de provisión transitoria de
puestos de trabajo mediante el régimen de comisión de servicios». Incluso asienta, en
su fallo tercero, que es doctrina el que la administración pueda emplear este sistema de
provisión de puestos de trabajo.  
Por su parte, en la sentencia 873/2019 de 24 de junio de 2019, de la misma Sala y
Sección del Tribunal Supremo (recurso 1594/2017), en el fundamento de derecho
quinto, 2º, que «la regulación básica se ciñe a una modalidad de comisión de servicios -
la que venga exigida por existencia de plazas vacantes de urgente e inaplazable
necesidad de ser servidas-, su carácter potestativo, que haya convocatoria pública y la
posibilidad de que se fije un plazo para su cobertura transitoria. Queda a la
determinación de la normativa de desarrollo de tal norma básica regular las diferentes
clases de comisiones, su temporalidad, plazo de duración, cobertura de la vacante
mediante los sistemas ordinarios de provisión de destinos, etc.».  
Precisamente lo que aborda este proyecto de decreto es un desarrollo reglamentario
para el caso específico de los profesores de PEB.  
> 3. «El proyecto de decreto pasa a diferenciar la elección de profesorado de centros y aulas
de excelencia cuando no se realizaba antes y la diferenciación no está justificada» (p.
10). Ante ello se pide que la designación del profesorado tanto de centros como de aulas
de excelencia «sea siempre vía concurso entre funcionarios» (p. 10).  
La diferenciación de procedimientos se justifica, básicamente, por la existencia, en el
caso de los centros con aulas de excelencia, de profesorado estable de plantilla, cuya
participación en el PEB ha de ser tenida en consideración. Ha de advertirse, a este
respecto, que en el proyecto de decreto las condiciones de acceso a docencia en PEB de
los profesores de plantilla en los centros con aulas de excelencia son las mismas que
para la docencia en centros de excelencia.  
> 4. «Este proyecto de orden \[sic] tiene su origen en que en 2018 la Consejería de Educación
aceptó la recomendación del Defensor del Pueblo de “Proceder a proveer mediante el
sistema de concurso de traslados los puestos de trabajo del IES San Mateo que lleven
ocupados más de dos años en comisión de servicios”. En 2021 hubo compromiso escrito
de la Consejería con el Defensor del Pueblo en aportar transparencia a la provisión de
los programas en centro como es el caso de excelencia evitando la excepcionalidad de
las comisiones de servicio, pero este proyecto de decreto no lo soluciona, prorrogando
comisiones de servicio existentes sin aportar detalles y remitiendo a un futuro
reglamento» (p. 11). Se estima, en consecuencia, que el decreto debe establecer que se
provea «mediante el sistema de concurso de traslados los puestos de trabajo del IES San
Mateo que lleven ocupados más de dos años en comisión de servicios» (p. 14).  
Véase lo dicho para las alegaciones primera y segunda.  
> 5. «El proyecto de orden \[sic] no regula las sustituciones en el Programa de Excelencia de
Bachillerato buscando un mejor perfil» (p. 14). Propone «crear una lista de profesores
interinos “habilitados/con experiencia previa en docencia excelencia”» (p. 15).  
La creación de listas específicas de profesores interinos para PEB puede dificultar la
cobertura de ausencias de profesores PEB.  
> 6. «El proyecto de decreto al usar comisiones de servicio implica temporalidad que se
pretende reducir» (p. 15). Se pide que se «evite las comisiones de servicio ya que usarlas
aumenta la temporalidad».  
El objetivo de la norma es estabilizar el procedimiento, mantenido una relativa
provisionalidad del profesorado, en beneficio de un PEB que permita el acceso a la
docencia en el programa al profesorado más cualificado en cada momento.  
> 7. «El proyecto de decreto no contempla la especialización del equipo directivo de los
centros de excelencia» (p. 15). Se propone que se «fije para el equipo directivo los
mismos requisitos de mérito y capacidad que al resto del profesorado del Programa de
Excelencia de Bachillerato» (p. 16).  
El procedimiento para la selección y nombramiento de los equipos directivos de los
centros docentes públicos no interfiere en las disposiciones establecidas en este
proyecto de decreto, toda vez que en esta norma nada se dice acerca de aquellos
asuntos.  
D. ……………………………. ha presentado alegaciones mediante documentos presentados por
registro de referencia 59/000445.9/23 y 59/000446.9/23, ambos de fecha de 21 de agosto de 2023. Presenta cinco grupos de alegaciones.  
> 1. En relación con el procedimiento de provisión del profesorado del PEB:  
a. «… del contenido de los […] artículos 3.2 y 3.3 \[del borrador de decreto] se
desprende que la Consejería de Educación, Universidades, Ciencia y Portavocía
no tiene intención de dar cumplimiento a dicha Recomendación del Defensor
del Pueblo (Queja núm. 16017048)» (p. 1). La Recomendación se sustancia,
según el alegante, en «proceder a proveer mediante el sistema de concurso de
traslados los puestos de trabajo del Instituto “San Mateo” que lleven ocupados
más de dos años en comisión de servicios» (p. 1).  
A este respecto, el proponente del proyecto debe reiterar lo reconocido por el
propio Defensor del Pueblo de referencia 21027900, de 2 de marzo de 2021 en
relación con el expediente 20028606 y expresado por la entonces Consejería de
Educación y Juventud, referido poco antes en esta MAIN en la argumentación
ante la alegación primera del Sr. …………………………...  
b. «Además, al hilo de los expuesto en la redacción de los art. 3.2, 3.3, 4.1 y 4.2 se
aprecia un más que absoluto desequilibrio y agravio comparativo entre el
acceso del profesorado al centro de excelencia y a las aulas de excelencia».  
También vale en este caso lo argumentado en relación con la alegación tercera
del Sr. ………………………  
> 2. En relación con los requisitos del profesorado del PEB.  
a. Acerca del artículo 2.1 el alegante observa que «acreditar tres años de
antigüedad en los citados cuerpos de la función pública docente sería un agravio
comparativo para los catedráticos frente a los profesores, ya que uno de los
requisitos para acceder al Cuerpo de Catedráticos de Enseñanza Secundaria
sería contar con una antigüedad mínima de ocho años en el Cuerpo de
Profesores de Enseñanza Secundaria», por lo que propone que dicho artículo se
redacte en los siguientes términos: «Las materias del Programa de Excelencia
en Bachillerato serán impartidas por catedráticos de enseñanza secundaria de
la correspondiente especialidad docente. No obstante, los profesores de
enseñanza secundaria también podrán impartir las materias de su especialidad
en dicho Programa, pero para ello deberán contar con una antigüedad mínima
de ocho años en el correspondiente cuerpo como funcionario de carrera.
Asimismo, catedráticos y profesores deberán acreditar ocho años de
experiencia de docencia directa en el aula».  
En el proyecto de decreto se establece, en el artículo 2.1, la condición esencial
que ha de cumplirse para ser candidato a la docencia en PEB, mientras que en
artículo 2.2 se fijan los criterios de valoración de los candidatos.  
b. Acerca del artículo 2.2, se propone «diseñar un procedimiento de habilitación
específico» para el profesorado PEB, análogo al empleado para el Programa de
Enseñanza Bilingüe.  
Un sistema de habilitación de profesorado PEB constituye una complicación
innecesaria, toda vez que no garantiza en las actuales circunstancias una mejor
selección de profesores para estas enseñanzas que el eventual concurso de
méritos.  
> 3. Permanencia del profesorado PEB: la permanencia «de tres y seis años, a la que hace
mención el […] artículo 5 del Proyecto de Decreto, vulnera lo preceptuado en el
mencionado art. 53.1 de la Ley 1/1986, de 10 de abril, de la Función Pública de la
Comunidad de Madrid».  
De nuevo debe recordarse aquí lo explicado para las alegaciones primera y segunda del
Sr. ……………………  
> 4. Régimen transitorio. El alegante sostiene que la disposición transitoria del proyecto de
decreto vulnera el artículo 53.1 de la Ley 1/1986, de 10 de abril y a la recomendación
del defensor del Pueblo a la queja 16017048. A lo que añade que, «además, y a mi
entender, esta disposición transitoria única vulnera los principios constitucionales de
mérito, capacidad e igualdad, en tanto en cuanto permitiría que el profesorado actual
del bachillerato de Excelencia, que ha sido elegido de manera arbitraria sin mediar
concurso de méritos».  
Se opta por una transición que mantiene la actual situación de profesorado en dicho
centro, para no perjudicar las expectativas –que pueden ser derechos adquiridos- que
dicho profesorado pueda tener.  
La selección de profesorado para el PEB llevada a cabo hasta ahora no ha sido arbitraria,
sino que ha tenido que ajustarse a «criterios objetivos en relación con el fin que se
persigue», según el artículo 6.2 de la orden 11995/2012, de 21 de diciembre.  
> 5. Sustitución de profesores. También el Sr. …………………………….. propone que «las
sustituciones solo se deberían realizar mediante una lista de docentes interinos que
estuviesen en posesión de la habilitación» para el PEB.  
Véase, a este respecto, lo observado a la alegación quinta del Sr. ……………………….., así
como a la alegación 2.b) anterior del Sr. ……………………..  

No se responde jurídicamente (la abogacía dice que no conoce detalles), sino a medida para justificar. Un resumen por ejemplo sería que **la respuesta a que no tiene sentido provisión vía comisión en el San Mateo y que debería ser vía concurso para que haya plantilla estable es ... que como no hay plantilla estable tiene que ser así**

Como no hay dictamente publicado de la Comisión Jurídica Asesora, planteo solicitud vía transparencia.  

---
Solicito copia o enlace al Dictamen de la Comisión Jurídica Asesora asociado al Proyecto de Decreto, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid que tuvo trámite de audiencia de 28/07/2023 hasta 21/08/2023  
Se ha publicado con fecha 5 diciembre 2023 versión de decreto que en preámbulo indica "oída/de conformidad con la Comisión Jurídica Asesora de la Comunidad de Madrid"  
Se ha publicado con fecha 12 diciembre 2023 MAIN (MEMORIA EJECUTIVA DE ANÁLISIS DE IMPACTO NORMATIVO) actualizada recogiendo las alegaciones y en 6.11. Dictamen de la Comisión Jurídica Asesora se indica que se solicitará el correspondiente informe.  
En consejo de gobierno de 20 diciembre 2023 se citó informe relativo a la solicitud de dictamen de la Comisión Jurídica Asesora asociado a este proyecto de decreto, pero a día de hoy no está publicado en la web de la Comisión Jurídica Asesora, donde ya hay publicados dictámenes de 2024  
Por lo anterior considero que es un documento público y existente al que aplica la transparencia.  

---

03/096028.9/24

Los enlaces a las sentencias que citan las pongo en [post general sobre comisiones de servicio](http://algoquedaquedecir.blogspot.com/2017/08/comisiones-de-servicio-en-madrid-y.html) 

**2 febrero 2024**  
Veo en [página trámite de audiencia](https://www.comunidad.madrid/transparencia/proyecto-decreto-del-consejo-gobierno-que-se-regula-provision-del-profesorado-del-programa) (todavía no en buscador CJA) el dictamen  

[Proyecto de decreto sometido a Comisión Jurídica Asesora](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2023-12-05-23.decreto_excelencia_v07_2023-12-05_4583572._tras_ssjj.pdf)  
[Dictamen de la Comisión Jurídica Asesora (18 enero 2024)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2024-01-18-dictamen_cja_18_enero_sin_firma.pdf)  
[MAIN 23 enero 2024 (aunque en web texto enlace indica MAIN de 12-12-2023 enviada a Comisión Jurídica Asesora)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2024-01-23-main_tras_ssjj_enviada_a_cja.pdf)  

La CJA da por buenas las respuestas a las alegaciones (las cita)  
> 17.- Alegaciones presentadas por dos ciudadanos (documentos nº 17 y 18 del expediente).  
> ...  
> Una vez practicado el referido trámite, se han recibido alegaciones de dos ciudadanos.  
> ...  
> En el mismo apartado se establece que el desempeño de los puestos docentes necesarios para atender el Programa de Excelencia en
Bachillerato, en los centros de excelencia, tendrá carácter provisional. Esta previsión que ha sido objeto de crítica en el trámite de información pública, se justifica en la Memoria cuando señala “el decreto que se tramita supone que, para un adecuado cumplimiento de la finalidad del PEB, es conveniente detectar y seleccionar, entre el profesorado
funcionario, el que presente mejor perfil científico, académico e
investigador. Asimismo, es concorde con la finalidad del PEB el que el
profesorado de centros y aulas de excelencia lo sea de manera
provisional. De este modo, todo el profesorado puede participar en el PEB
cuando destaque en el campo científico de su especialidad. Habida
cuenta, por otro lado, de la constante variación y progreso de los
conocimientos –de las ciencias y de las artes, así como de las técnicas
digitales y de comunicación-, importa garantizar para el PEB en lo
posible, un profesorado preocupado por la continua actualización de su
especialidad y por la autosuperación”.

Indica algunos aspectos a cambiar

> la excepción que el proyecto acoge en el apartado 3 para el profesorado de aulas de excelencia con destino definitivo en el centro, exige una justificación en la Memoria.

Tiene una consideración esencial asociada al alumnado extranjero, que luego se ve en MAIN de 23 enero que cuestionan.  (ver página 30 como uno de los argumentos es una carta en 2021 a la Presidenta (Ayuso) y la respuesta de Ayuso a Daniel Cano Villaverde.  

[Nombrado en marzo 2012 presidente de la agencia estatal de meteorología](https://www.lamoncloa.gob.es/consejodeministros/referencias/paginas/2012/refc20120330.aspx) y [cesado en julio 2013](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2013-7728)  

Es curioso el comentario   
> Por lo que hace a los impactos de la norma proyectada, contiene
una referencia al impacto económico y presupuestario para destacar el
nulo impacto en esta materia porque “los centros concernidos disponen
ya de efectivos para estas enseñanzas en sus plantillas orgánicas”, y no
incrementa, ni disminuye el coste de profesorado en las enseñanzas de
excelencia.   

Cuando el IES San Mateo tiene plantilla orgánica 0 desde su creación  

**6 febrero 2024**  
Veo publicado dictamen CJA en web CJA

[0016/24 Proyecto de Reglamento Ejecutivo DICTAMEN del Pleno de la Comisión Jurídica Asesora de la Comunidad de Madrid, aprobado por unanimidad, en su sesión de 18 de enero de 2024, emitido ante la consulta formulada por el vicepresidente, consejero de Educación, Ciencia y Universidades al amparo del artículo 5.3 de la Ley 7/2015, de 28 de diciembre, por la que se somete a dictamen el “proyecto de decreto del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid”.](https://www.comunidad.madrid/sites/default/files/dictamenes/2024/dictamen_16-24_des.pdf)  

**7 febrero 2024**  
Se cita la aprobación en consejo de gobierno, en redes y en web de la comunidad  

[ACUERDOS CONSEJO DE GOBIERNO 7 de febrero de 2024](https://www.comunidad.madrid/sites/default/files/240207_cg.pdf)  
> • Decreto por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid.

[Rueda de prensa de Consejo de Gobierno 07 febrero 2024](https://www.comunidad.madrid/retransmision/2024/02/07/R_07022024)  

[Nota de prensa La Comunidad de Madrid mejora la incorporación del profesorado a sus Bachilleratos de Excelencia](https://www.comunidad.madrid/file/400586/download)  

[240207 nota de prensa consejo de gobierno educación ciencia y universidades decreto profesores bachilerato excelencia pdf](https://www.comunidad.madrid/file/400586/download)  

[twitter educacmadrid/status/1755207940347179070](https://twitter.com/educacmadrid/status/1755207940347179070)  
✍ La @comunidadmadri ha aprobado el Decreto que regula el procedimiento para dotar de docentes a los centros que participan en el programa de Bachillerato de Excelencia.  
🎓 Se imparte en el IES San Mateo y en 16 aulas de Excelencia.  
[La Comunidad de Madrid mejora la incorporación del profesorado a sus Bachilleratos de Excelencia - comunidad.madrid](https://www.comunidad.madrid/noticias/2024/02/07/comunidad-madrid-mejora-incorporacion-profesorado-bachilleratos-excelencia)  

_En tuit citan por error una cuenta parodia de la comunidad, no la oficial_  


[twitter ComunidadMadrid/status/1755236505822413250](https://twitter.com/ComunidadMadrid/status/1755236505822413250)  
📚 La Comunidad de Madrid mejora el procedimiento para dotar de docentes a los centros educativos del programa de Bachillerato de Excelencia.  
👨‍🏫 Se seleccionarán por concurso basado en los principios de igualdad, mérito, capacidad y publicidad.  
+Info: http://c.madrid/cozuc  
(incluye vídeo)  

**9 febrero 2024**

[Decreto 15/2024, de 7 de febrero, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2024/02/09/BOCM-20240209-1.PDF)


### Trámite de audiencia septiembre 2024

[Proyecto de Orden, de la Consejería de Educación, Ciencia y Universidades, por la que se desarrolla el Decreto 15/2024, de 7 de febrero, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato](https://www.comunidad.madrid/transparencia/proyecto-orden-consejeria-educacion-ciencia-y-universidades-que-se-desarrolla-decreto-152024-7)

Alegaciones de 4 septiembre a 12 septiembre

[Proyecto de orden](https://www.comunidad.madrid/transparencia/sites/default/files/01._proy._orden_prof._bach._excelencia_0.pdf)  

Marco algunos detalles en una primera lectura

> Artículo 2. Convocatoria del procedimiento de concurso de méritos.
Las plazas vacantes se cubrirán por el sistema de concurso de méritos  

> Artículo 4. Comisión de valoración.  
>1. Para la valoración de los méritos aportados por los aspirantes se constituirá una comisión
de valoración. Esta comisión estará integrada por los siguientes miembros:  
...  
d) Vocal: un director de un centro de excelencia, designado por la dirección general
competente en las enseñanzas de bachillerato.  
e) Vocal: un director de un centro con aulas de excelencia, designado por la dirección
general competente en las enseñanzas de bachillerato.  
Los vocales a los que se refieren las letras d) y e), actuarán solo cuando se oferten vacantes
correspondientes a la opción del Programa a la que pertenezcan.  

Es peculiar ya que **solo hay un centro de excelencia, el IES San Mateo**, luego el apartado d es poner fija a una persona. 

> Artículo 6. Resolución de prórrogas y renovaciones de docencia en el Programa.  
>1. Antes del 15 de marzo de cada curso escolar, los centros de excelencia comunicarán a las
respectivas direcciones de área territorial de las que dependan, el listado de profesores que
cumplen, con el corriente, tres cursos de docencia. Se empleará para ello el modelo del anexo
I.  

> Artículo 7. Comunicación anual de vacantes.  
Los directores de los centros de excelencia y de los centros con aulas de excelencia
comunicarán a sus respectivas direcciones de área territorial, antes del 30 de abril de cada
curso escolar, las vacantes que se necesite cubrir para el curso escolar siguiente. Las
direcciones de área territorial darán traslado de esa comunicación de vacantes a la dirección
general con competencias en recursos humanos de la consejería competente en materia de
educación, en el plazo de cinco días hábiles.

> Disposición adicional primera. Docencia de los miembros de los equipos directivos.  
Los miembros de los equipos directivos de los centros de excelencia y de centros con aulas
de excelencia pueden impartir docencia del Programa siempre que hayan resultado
seleccionados por los procedimientos previstos.

No aclara qué son "seleccionados por los procedimientos previstos", pero sugiere que es la selección como director, lo que los exime de méritos.  

En [Memoria Análisis Impacto Normativo](https://www.comunidad.madrid/transparencia/sites/default/files/02._main_prof._excelencia_0.pdf)  

> 2.2. Estructura y contenido de la norma  
...  
La disposición adicional primera regula el acceso de los miembros de los equipos directivos a la
docencia en PEB, reiterando para ellos las previsiones generales.  

Según lo indicado ahí, parece que interpretan "seleccionados por los procedimientos previstos" es según los mismos procedimientos que el resto de profesorado de excelencia. Si es así, surge la duda ¿cómo va a actuar la comisión para valorar los méritos del director del único centro de excelencia si él forma parte de la comisión?


Planteo texto de alegaciones

---

Alegación 1: Aclaración de criterios para la docencia en el programa de excelencia por los miembros del equipo directivo.  
En la Disposición adicional primera se indica que "pueden impartir docencia del Programa siempre que hayan resultado
seleccionados por los procedimientos previstos." y la frase es ambigua, ya que no aclara si son procedimientos previstos pra ser seleccionados como equipos directivos o por los procedimientos previstos para ser seleccionados como profesorado del programa de excelencia que trata esta orden.  
En MAIN se indica "La disposición adicional primera regula el acceso de los miembros de los equipos directivos a la
docencia en PEB, reiterando para ellos las previsiones generales." y eso sugiere que se trata de que deben ser seleccionados según los mismos procedimientos que el resto de profesorado de excelencia.   
Debe aclararse la disposición adicional primera para dejar claro que la provisión del profesorado de excelencia aplica a cualquier docente sea o no equipo directivo.  
Se propone la siguiente redacción:  
"Los miembros de los equipos directivos de los centros de excelencia y de centros con aulas de excelencia pueden impartir docencia del Programa siempre que hayan resultado seleccionados por los mismos procedimientos previstos para la selección del resto del profesorado del programa de excelencia".

Alegación 2: Aclaración de artículo 4.1.d mientras exista un único centro de excelencia  
En artículo 4.1.d se indica "d) Vocal: un director de un centro de excelencia, designado por la dirección general
competente en las enseñanzas de bachillerato." pero desde el comienzo del programa de excelencia solo existe un único centro de excelencia, el IES San Mateo.  
La redacción de 4.d no tiene sentido mientras exista un único centro, como ocurre actualmente, ya que:  
- No debe ser designado, dado que solo existe una opción.  
- Choca con la disposición adicional primera y mi alegación 1, ya que que para valorar si puede ejercer la docencia el director del único centro de excelencia un miembro de la comisión debería ser él mismo cuando se tendría que abstener según artículo 23 de Ley 40/2015 tal y como se indica en artículo 4.3 del proyecto, y en ese caso la comisión tendría menos miembros.  
Se propone la siguiente redacción, eliminando el apartado e:  
"d) Vocales: dos directores de centros de excelencia y/o centros con aulas de excelencia, designados por la dirección general competente en las enseñanzas de bachillerato contemplando en las comisiones de valoración de equipos directivos la sustitución de los designados que se abstengan según Ley 40/2015 para que el número de miembros de la comisión siempre sea el mismo."

Alegación 3: Aclaración del control de si se han alcanzado o superado el máximo de 6 cursos académicos que se indican en artículo 5.3 de decreto 15/2024  
En artículo 6,1 se indica para los centros de excelencia (el único que existe es el IES San Mateo) "profesores que cumplen, con el corriente, tres cursos de docencia", pero en artículo 5.3 de Decreto 15/2024 se indica "un período continuado máximo de seis cursos académicos" aclarando que dicho límite aplica a los centros de excelencia (al único que es el IES San Mateo).  
La redacción del artículo 6.1, artículo 7 y anexos I, II y III no contempla dicho límite.  
Como se puede ver en ejecución RDA007/2023 resolución RDACTPCM56_2021 en marzo de 2023 existían comisiones de servicio en el IES San Mateo de más de 6 cursos, sin ser algo puntual.  
https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2023-03-07-Ejecuci%C3%B3n_Resoluci%C3%B3n_CTYPC_Reclamaci%C3%B3n+RDACTPCM56_2021.pdf   
Se propone la siguiente redacción del primer párrafo del artículo 6.1:  
"1. Antes del 15 de marzo de cada curso escolar, los centros de excelencia comunicarán a las respectivas direcciones de área territorial de las que dependan, el listado de profesores que cumplen, con el corriente, tres cursos de docencia y que no han superado el período continuado máximo de seis cursos académicos que indica el artículo 5.3 de Decreto 15/2024. Se empleará para ello el modelo del anexo I."  
Se propone que anexos I, II, III, IV y V se modifiquen para que figure el primer curso académico en el que cada profesor inició su docencia el el centro de excelencia de cara a la comprobación del periodo máximo de seis cursos académicos indicado en el artículo 5.3 de Decreto 15/2024.  
Se propone la siguiente redacción del artículo 7:  
"Los directores de los centros de excelencia y de los centros con aulas de excelencia comunicarán a sus respectivas direcciones de área territorial, antes del 30 de abril de cada curso escolar, las vacantes que se necesite cubrir para el curso escolar siguiente, incluyendo las vacantes asociadas a profesorado de centros de excelencia que ha superado el límite máximo de seis cursos académicos. Las direcciones de área territorial darán traslado de esa comunicación de vacantes a la dirección general con competencias en recursos humanos de la consejería competente en materia de educación, en el plazo de cinco días hábiles."

El objetivo de la alegación 3 es que en 2025 se convoque concurso de méritos para cubrir puestos del Programa de Excelencia de Bachillerato en el centro de excelencia IES San Mateo aplicando artículo 3.2 de decreto 15/2024, aplicando al curso 2025-2026 y a los docentes que ya llevan más de 6 años en comisión de servicio en dicho centro y programa.   

---

43/211416.9/24 

**10 septiembre 2024**

Comentario: en el artículo 3 no se aclara un criterio de desempate, pero quizá eso pueda quedar para concretarse en la convocatoria. De hecho se indica "En los concursos deberán valorarse, como mínimo, los siguientes méritos:" y ese "como mínimo" implica que la convocatoria puede añadir más. 

### Convocatorias de concursos de méritos a partir de 2024 

Tras ver esta noticia de 30 junio 2024  
[La Comunidad de Madrid estrena el Bachillerato de Excelencia en la modalidad de Artes](https://www.comunidad.madrid/noticias/2024/06/30/comunidad-madrid-estrena-bachillerato-excelencia-modalidad-artes)  
> La Comunidad de Madrid estrena el Bachillerato de Excelencia en la modalidad de Artes el próximo curso 2024/25. Así, el instituto público Isabel la Católica de la capital impartirá la rama de Artes Plásticas, Imagen y Diseño, mientras que el IES Isaac Albéniz de Leganés lo hará en la de Música y Artes Escénicas.  

La combino con el decreto 15/2024 y planteo solicitud de transparencia  

El 30 de junio 2024 se anunció que la Comunidad de Madrid estrena el Bachillerato de Excelencia en la modalidad de Artes el próximo curso 2024/2025 https://www.comunidad.madrid/noticias/2024/06/30/comunidad-madrid-estrena-bachillerato-excelencia-modalidad-artes  
Se indican dos centros: IES Isabel la Católica de código 28020909 e IES Isaac Albéniz de código 28020909
Solicito confirmación de que los puestos para las aulas de excelencia en los centros de códigos 28020909 y 28020909 han sido cubiertos con profesorado con destino definitivo en el centro según artículo 4.1 de decreto 15/2024, y en caso contrario, copia o enlace a la convocatoria del concurso de méritos para dotarse del profesorado necesario para las aulas de excelencia en dichos centros según artículo 4.2 de decreto 15/2024.

Presentado 3 agosto 2024 43/082133.9/24

No veo nada en la plantilla orgánica ni funciona de esos centros especial en [Personal docente en centros públicos no universitarios de la Comunidad de Madrid](https://www.comunidad.madrid/servicios/educacion/personal-docente-centros-publicos-no-universitarios-comunidad-madrid)  

**16 septiembre 2024**

Recibo [2024-09-16-ResolucionAccesoProvisionBachExcelenciaArtes.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2024-09-16-ResolucionAccesoProvisionBachExcelenciaArtes.pdf): no dice nada, se limita a decir que cumplen la ley, como si la cumplieran con las comisiones.  

Se limita a decir  
> Se informa que, tal y como se indica en la Disposición final cuarta del Decreto 15/2024, de 7 de febrero,
del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia
en Bachillerato de la Comunidad de Madrid, será de aplicación lo dispuesto en esta norma para la
designación de profesorado del Programa de Excelencia en el Bachillerato para el curso 2024-2025 y
siguientes.




**17 enero 2025**

[Orden 5731/2024, de 27 de diciembre, de la Consejería de Educación, Ciencia y Universidades, por la que se desarrolla el Decreto 15/2024, de 7 de febrero, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid ](https://www.bocm.es/boletin/CM_Orden_BOCM/2025/01/17/BOCM-20250117-12.PDF)  

Busco en [huella normativa](https://www.comunidad.madrid/transparencia/orden-consejeria-educacion-ciencia-y-universidades-que-se-desarrolla-decreto-152024-7-febrero-del)

[Informe abobacía S.J.- 585/2024](https://www.comunidad.madrid/transparencia/orden-consejeria-educacion-ciencia-y-universidades-que-se-desarrolla-decreto-152024-7-febrero-del)
Menciona dos escritos de alegaciones, pero no da detalles ni los rebate  


Menciona dos consideraciones esenciales  
> Por tanto, deberá justificarse o revisarse la competencia para convocar estos procedimientos.  
Esta consideración tiene carácter esencial.  

> Por otra parte, debe computarse como mérito la docencia impartida en programas o cursos
impartidos de similares características que el Programa de Excelencia del Bachillerato, de
acuerdo con el citado apartado 2 del artículo 2 del Decreto 15/2024.  
Esta consideración tiene carácter esencial.  

### Llevar a fiscalía prevaricación

**12 mayo 2023**

Envío escrito a fiscalía
[2023-05-11 Escrito Fiscalía Comisiones IES San Mateo Madrid anonimizado](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2023-05-11-EscritoFiscal%C3%ADaComisionesIESSanMateoMadrid-anonimizado.odt)

**25 mayo 2023**

Recibo [respuesta fiscalía que archiva](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2023-05-19-DIP%20461-2023%20D%20ENRIQUE%20GARCIA%20SIMON-anonimizado.pdf)


### Llevar al Defensor del Pueblo

**27 junio 2023**

Recibo [respuesta Defensor del Pueblo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2023-06-27-RespuestaDefensorPueblo20028606_11-SinFirmasSinDatosPersonales.pdf)

Respondo al Defensor del Pueblo

---

Envío la respuesta recibida en mayo de de fiscalía en la que se archivaba no por cuestionar la ilegalidad de lo denunciado, sino por no demostrar a su juicio que lo que hace la consejería es "injusto y arbitrario" que es requisito para que haya consideración penal asociada a prevaricación e intervengan.  
Considero que una vez que se ha inhibido el poder judicial, archivando y sin emitir una resolución, solo archivando diligencias, sí debe intervenir el Defensor del Pueblo

---  

**27 junio 2024**

Escrito similar, incluyendo lo dicho a inspección, pero citando antecedentes

Las comisiones de servicio indebidamente prorrogadas han sido objeto de intervención del Defensor del Pueblo en varias ocasiones. 
En concreto en 2018 la Consejería de Educación de Madrid aceptó la recomendación del Defensor del Pueblo sobre queja 16017048 
https://www.defensordelpueblo.es/resoluciones/duracion-maxima-de-las-comisiones-de-servicio/

Solicito la ayuda del Defensor del Pueblo ya que aunque Madrid ha publicado por fin en 2024 una orden al respecto, sigue sin convocar comisiones en el IES San Mateo prorrogando indebidamente las existentes que llevan no solo más de 2 años máximo que indica el informe del Defensor del Pueblo de 2021 https://www.defensordelpueblo.es/wp-content/uploads/2022/03/Informe_anual_2021.pdf "Comisiones de servicio ... marca como máximo un plazo de dos años (uno prorrogable por otro)..." y que indica el artículo 53 de la Ley 1/1986 de Madrid https://www.boe.es/buscar/act.php?id=BOE-A-1986-23734#a53, sino incluso más de los 6 que ellos mismos fijan como máximo en esta orden.

Incluyo un documento "expone/solicita" con los detalles




**2 noviembre 2023**

[Respuesta defensor del Pueblo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2023-11-02-DefensorPueblo20028606_12-anonimizado.pdf)

En febrero 2024 creo post separado [Comisiones de servicio. Normativa y jurisprudencia](../comisiones-de-servicio-normativa-y-jurisprudencia) y la idea es argumentar para una nueva reclamación, centrado en el límite temporal, los datos que muestran que hay gente desde 2011, y ejemplos como sentencia STS Galicia 2015

### Reclamar a inspección educativa

La idea viene de la reclamación puesta en [Finalidad evaluación para clasificación centros](../finalidad-evaluacion-para-clasificacion-centros) a inspección Madrid y a alta inspección

Ver [Reclamar a inspección educativa](../reclamar-a-inspeccion-educativa)

**27 junio 2024**  

Planteo escrito: hay limitación en texto "expone", separo parte. Remito a "Dirección de Área Territorial de Madrid-Capital. Inspección educativa", no me planteo https://www.educa2.madrid.org/web/sginspeccioneducativa/organizacion-sgie

EXPONE:  
>Aplicando artículo 3.2 de decreto 15/2024 se debería haber convocado concurso de méritos para cubrir en comisiones de servicio los puestos en el IES San Mateo en el curso 2024-2025 de los docentes que llevan más de 6 años en comisión.  
La administración estará alegando la disposición transitoria única del citado decreto 15/2024 para prorrogar un curso y retrasar esa convocatoria a 2025-2026, pero en este escrito expongo que no aplica a las personas que llevan más de 6 años en comisión de servicio, ya que el límite en centros de excelencia son 6 años incluyendo prórrogas, según artículo 5.3.  
Lo que expongo no es una interpretación personal, ya que la redacción actual de la disposición transitoria se realizó después de que la abogacía de la Comunidad dejase claro que no era válido indicar "iniciándose en el curso 2024-2025 el cómputo de permanencia en el Programa a que hace referencia el artículo 5"  que es lo que realizaban las versiones iniciales del decreto. Adjunto detalle  

Lo que indicó la abogacía fue textualmente lo siguiente https://www.comunidad.madrid/transparencia/sites/default/files/sj520_23sinfirmar.pdf  
"  
Aún desconociéndose por esta Abogacía la concreta casuística de los puestos afectados, no podemos sino conminar a reconsiderar el contenido de esta Disposición en lo que atañe al inciso ”iniciándose en el curso 2024-2025 el cómputo de permanencia en el Programa a que hace referencia el artículo 5.”  
Habida cuenta de la regulación ex novo que contempla el Proyecto que nos ocupa, resulta cuestionable aplicar el régimen de permanencia que contempla su artículo 5 a puestos de trabajo que han sido provistos de acuerdo con otro procedimiento -el contemplado en el artículo 6.2 de la Orden 11995/2012-. Se advierte, a tal efecto, que el sistema diseñado en el Decreto proyectado constituye una regulación más completa y cerrada que la prevista en el régimen actual.  
"  
Si la administración educativa está aceptando en 2024-2025 prorrogar un año más basándose en la disposición transitoria, sin contemplar si ya se llevan más de 6 años, por el mismo motivo estaría aceptando en 2025-2026 solicitar una prórroga de 3 años más sin contemplar si ya se llevan más de 6 años, de modo que no existiría un límite real de 6 cursos para los que ya llevan 6 o más cursos, y la supuesta corrección realizada en el decreto en base a lo indicado por la abogacía no tendría ningún efecto real.
Como se puede ver en ejecución RDA007/2023 resolución RDACTPCM56_2021 en marzo de 2023 existían comisiones de servicio en el IES San Mateo de más de 6 cursos, sin ser algo puntual.  
https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2023-03-07-Ejecuci%C3%B3n_Resoluci%C3%B3n_CTYPC_Reclamaci%C3%B3n+RDACTPCM56_2021.pdf

SOLICITA:  
>Por todo lo expuesto solicito, en junio de 2024, que se convoque concurso de méritos para cubrir puestos del Programa de Excelencia de Bachillerato en el centro de excelencia IES San Mateo aplicando artículo 3.2 de decreto 15/2024, aplicando al curso 2024-2025 y a los docentes que ya llevan más de 6 años en comisión de servicio en dicho centro y programa. 


Registro 27 junio 2024 49/792545.9/24 


