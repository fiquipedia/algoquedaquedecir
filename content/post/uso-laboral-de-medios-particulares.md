+++
author = "Enrique García"
title = "Uso laboral de medios particulares"
date = "2024-01-21"
tags = [
    "normativa", "protección de datos", "Madrid", "Raíces", "laboral"
]
toc = true

description = "Uso laboral de medios particulares"
thumbnail = "https://pbs.twimg.com/media/GEFA-PJW8AAMCu2?format=png"
images = ["https://pbs.twimg.com/media/GEFA-PJW8AAMCu2?format=png"]

# Imagen todada de https://twitter.com/FiQuiPedia/status/1747749770725147131

+++

Revisado 9 diciembre 2024

## Resumen

El 15 enero 2024 en Raíces, la aplicación educativa de Madrid, se indica que se va a implementar doble autenticación y para ello pretenden que sea obligatorio usar el móvil personal. Usando ese ejemplo intento documentar el uso laboral de medios personales.  
Me centro en el caso de docentes para los que el empleador es la administración, aunque hay aspectos generales.  
El confinamiento de 2020 fue una situación excepcional pero puede servir para ilustrar ciertos aspectos, como el que no deja de aplicar la protección de datos. Tiene relación con el teletrabajo, que existe en educación pero en docencia es algo excepcional.  

**Posts relacionados**  

*  [Normativa webs, aplicaciones y protección de datos en educación](https://algoquedaquedecir.blogspot.com/2019/06/normativa-webs-aplicaciones-y-proteccion-datos.html)  
*  [ Raíces: programa gestión educativa Madrid](https://algoquedaquedecir.blogspot.com/2017/11/raices-gestion-educativa-madrid.html)  

## Detalle

En el post [Normativa webs, aplicaciones y protección de datos en educación](https://algoquedaquedecir.blogspot.com/2019/06/normativa-webs-aplicaciones-y-proteccion-datos.html) tenía un apartado "Uso de dispositivos y equipamiento personal, teletrabajo" que muevo aquí  

 
### Cronología

**17 enero 2024**  
Lo comento por primera vez, aunque aparece aviso en raíces con fecha 15 enero 2024  
[twitter FiQuiPedia/status/1747749770725147131](https://twitter.com/FiQuiPedia/status/1747749770725147131)  
Habrá que reclamar a @AEPD_es porque @educacmadrid piensa obligar a que usemos nuestro móvil personal 
[¿Debo usar en la empresa mi móvil personal como segundo factor de autenticación? - cuadernosdeseguridad](https://cuadernosdeseguridad.com/2023/01/movil-personal-segundo-factor-autenticacion/)  
[Expediente N.º: EXP202100091 RESOLUCIÓN DE PROCEDIMIENTO SANCIONADOR - aepd.es](https://www.aepd.es/documento/ps-00480-2021.pdf)  
![](https://pbs.twimg.com/media/GEFAgHMX0AAZsdE?format=png)   
> 15 enero Nuevo acceso a Raíces – Doble factor de autenticación  
> Próximamente se va a cambiar la forma de acceder a Raíces desde Internet para mejorar la seguridad. Además de incluir nuevos métodos de acceso seguro, en caso de acceder con usuario y contraseña va a ser necesario introducir un código que se recibirá por SMS al teléfono móvil que se tenga informado en Raíces. Por tanto, sería necesario comprobar que el número de teléfono esté informado en Raíces y sea correcto o, en caso contrario, introducirlo. Según el perfil de acceso a Raíces, cada usuario puede realizar esta comprobación desde el punto de menú ‘Mis datos -> Datos del usuario/a’ o ‘Utilidades -> Mis datos’.  
En los accesos desde la app Roble, desde los centros educativos o desde cualquier otro equipo conectado a la red de la Comunidad de Madrid, no habrá ningún cambio.  

Me planteo realizar una reclamación a DPD primero y luego a AEPD  
Un tema es si esperar o no a que esté oficialmente puesto en un documento, ya que inicialmente solo está como aviso en Raíces. Parece ser que también se ha enviado correo a algunos centros.  

**12 marzo 2024**
Recibo correo en mi correo personal que es el que tengo asociado a note_public

[Nuevo acceso a FARO desde INTERNET. Sólo para personal que no dispone de acceso a la INTRANET de la Comunidad de Madrid](https://gestiona.comunidad.madrid/comunicacion_mdout/2024/FaroDoblefactor/Doblefactor.html)

Entrando en el enlace https://gestiona7.madrid.org/usug_micuenta/

En la pantalla de login

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-03-12-usug_micuenta-madrid.org-login.png)

En "Condiciones de acceso" sale 

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-03-12-usug_micuenta-madrid.org-Aviso%20de%20seguridad.png)

>Aviso de seguridad  
 La Comunidad de Madrid pone a su disposición los recursos, este ordenador -móvil o tablet-, los sistemas de información y los medios de comunicación electrónicos, necesarios para el desarrollo de su actividad profesional.  
El acceso a dichos recursos solo debe realizarse por las personas autorizadas a los mismos (usuarios) y para la finalidad prevista. El usuario debe hacer, de los recursos y la información que tratan estos, un uso diligente, responsable y conforme a las leyes y el fin para el que se ponen a disposición.  
En particular, no se puede suprimir, eludir o manipular los dispositivos técnicos de protección o cualesquiera otros mecanismos destinados a la identificación y acceso a los recursos. Tampoco es posible desinstalar o utilizar software que no haya sido previamente autorizado. Las actividades o uso contrario a lo previsto por la ley o en este mensaje, puede dar lugar al ejercicio de las acciones que legalmente correspondan. Si se produjera un incidente que cause (o pueda causar) un daño a los recursos o a la información tratada por estos, el usuario deberá ponerlo en conocimiento, a la mayor brevedad, tanto del responsable directo como de Madrid Digital.


Al entrar aparece este mensaje

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-03-12-usug_micuenta-madrid.org-Aviso%20de%20seguridad.png)

>Información sobre protección de datos y Doble Factor de Autenticación  
Le informamos que los datos de carácter personal serán tratados, en su calidad de Responsable del Tratamiento, por la Agencia para la Administración Digital de la Comunidad de Madrid, e incorporados a la actividad de tratamiento IMPLEMENTACIÓN DEL DOBLE FACTOR DE AUTENTICACIÓN (2FA), cuya finalidad es la gestión del acceso e implantación de la medida de seguridad Doble Factor de Autenticación exigida por el Esquema Nacional de Seguridad, cuya aplicación deviene obligatoria en virtud de la disposición adicional primera "Medidas de seguridad en el ámbito del sector público" de la Ley Orgánica 3/2018 de Protección de Datos, siendo el Doble Factor una medida requerida para el acceso seguro a los sistemas de información de la Comunidad de Madrid en situaciones de teletrabajo, trabajo en movilidad **o cualquier otra modalidad de acceso remoto**. Asimismo, el dato de número de teléfono móvil recabado, podrá utilizarse como canal de soporte informático para contactar directamente con usted, en caso de que necesite ayuda para una incidencia en el acceso.  
  Dicha finalidad tiene su base de legitimación en el cumplimiento de una obligación legal artículo 6.1.c) Reglamento 2016/679 General de Protección de Datos (en relación con el art. 10 apartados a) y d) de la Ley 7/2005, de 23 de diciembre, de Medidas Fiscales y Administrativas, por las amplias atribuciones que confiere de forma exclusiva a la Agencia para la Administración Digital de la CAM en materia de seguridad y sistemas de la información); en el cumplimiento de una misión realizada en interés público o en el ejercicio de poderes públicos artículo 6.1.e) RGPD, por referirse a una actividad enmarcada en el contexto de una Administración Pública; y en el consentimiento para el tratamiento de sus datos personales artículo 6.1.a) RGPD, al ser susceptibles de recabarse en orden a la implementación de la medida.  
  Las categorías de datos personales afectadas por este tratamiento son datos identificativos, número de teléfono proporcionado por el interesado y datos recabados por la aplicación de Doble Factor como localización (sólo a los efectos puntuales de identificar el país donde se ubica el interesado y confirmar la viabilidad técnica de garantizar un acceso seguro al sistema) correo electrónico, ID de usuario, direcciones IP, siendo el objetivo verificar la identidad del sujeto que pretende acceder al sistema antes de que este acceso se produzca. Son datos imprescindibles cuyo tratamiento está sujeto al principio de minimización.  
  No está previsto efectuar comunicación de dichos datos a terceros ni efectuar transferencias internacionales con los mismos, limitándose su uso a las labores internas de identificación y operativas antedichas. El plazo de conservación de estos datos será el imprescindible para gestionar, validar y registrar un acceso adecuado, permaneciendo bloqueados tras su supresión, con el fin de afrontar las responsabilidades que pudieran derivar del cumplimiento de requerimientos a instancias de las autoridades judiciales o administrativas.  
  En lo relativo al ejercicio de sus derechos, puede solicitar el acceso, rectificación, supresión, limitación u oposición al tratamiento, así como el derecho a revocar su consentimiento -en el caso de se den los requisitos establecidos en el Reglamento 2016/679 General de Protección de Datos, así como en la Ley Orgánica 3/2018, de Protección de Datos Personal ‐ a través del Registro Electrónico o Registro Presencial o en los lugares y formas previstos en el artículo 16.4 de la Ley 39/2015, preferentemente mediante el formulario de solicitud "Ejercicio de derechos en materia de protección de datos personales". Así mismo, tiene derecho a presentar una reclamación ante la Agencia Española de Protección de Datos.  
  Podrá ampliar información adicional detallada sobre protección de datos en las siguiente dirección web:
www.comunidad.madrid/protecciondedatos.

Tras aceptar aparece esta pantalla donde la única opción es poner el teléfono

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-03-12-usug_micuenta-madrid.org-Datos%20de%20notificaciones%20y%202FA.png)

El texto "»Información sobre el tratamiento de los datos de carácter personal y medidas de seguridad Doble Factor de Autenticación.«" lleva a la misma ventana anterior.

Lo que citan es 
Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales.  
[Disposición adicional primera. Medidas de seguridad en el ámbito del sector público.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-16673#da)  
> 1. El Esquema Nacional de Seguridad incluirá las medidas que deban implantarse en caso de tratamiento de datos personales para evitar su pérdida, alteración o acceso no autorizado, adaptando los criterios de determinación del riesgo en el tratamiento de los datos a lo establecido en el artículo 32 del Reglamento (UE) 2016/679.  
>2. Los responsables enumerados en el artículo 77.1 de esta ley orgánica deberán aplicar a los tratamientos de datos personales las medidas de seguridad que correspondan de las previstas en el Esquema Nacional de Seguridad, así como impulsar un grado de implementación de medidas equivalentes en las empresas o fundaciones vinculadas a los mismos sujetas al Derecho privado.  
En los casos en los que un tercero preste un servicio en régimen de concesión, encomienda de gestión o contrato, las medidas de seguridad se corresponderán con las de la Administración pública de origen y se ajustarán al Esquema Nacional de Seguridad.

El Esquema Nacional de Seguridad está regulado en [Real Decreto 311/2022, de 3 de mayo, por el que se regula el Esquema Nacional de Seguridad.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-7191)

> 4.2.6 Mecanismo de autenticación (usuarios de la organización) [op.acc.6]  
...  
Refuerzo R8-Doble factor para acceso desde o a través de zonas no controladas.  
Se denomina «zona controlada» aquella que no es de acceso público, requiriéndose que el usuario, antes de tener acceso al equipo, se haya autenticado previamente de alguna forma (control de acceso a las instalaciones), diferente del mecanismo de autenticación lógica frente al sistema. Un ejemplo de zona no controlada es Internet.  
– [op.acc.6.r8.1] Para el acceso desde o a través de zonas no controladas se requerirá un doble factor de autenticación: R2, R3 o R4.

Consultando qué es R2, R3 ó R4 

> Refuerzo R2-Contraseña + otro factor de autenticación.  
– [op.acc.6.r2.1] Se requerirá un segundo factor tal como «algo que se tiene», es decir, un dispositivo, una contraseña de un solo uso (OTP, en inglés) como complemento a la contraseña de usuario, o «algo que se es».  
Refuerzo R3-Certificados.  
– [op.acc.6.r3.1] Se emplearán certificados cualificados como mecanismo de autenticación.  
– [op.acc.6.r3.2] El uso del certificado estará protegido por un segundo factor, del tipo PIN o biométrico.  
Refuerzo R4-Certificados en dispositivo físico.  
– [op.acc.6.r4.1] Se emplearán certificados cualificados como mecanismo de autenticación, en soporte físico (tarjeta o similar) usando algoritmos, parámetros y dispositivos autorizados por el CCN.  
– [op.acc.6.r4.2] El uso del certificado estará protegido por un segundo factor, del tipo PIN o biométrico.


Citan DA1 LOPDGDD, ENS y 6.1.c RGPD como legitimación, pero:  
- No hay consentimiento libre si no hay otra opción que el uso laboral de un recurso privado.  
- ENS obliga a R8 que supone R2, R3 ó R4, y fuerzan R2.  

**27 noviembre 2024**  
Nuevo mensaje en Raíces

> Próxima Activación Doble Factor de Autenticación - A partir del 26 de diciembre va a cambiar la forma de acceder a Raíces desde Internet por cuestiones de seguridad. En caso de acceder con usuario y contraseña va a ser necesario introducir un código que se recibirá por SMS o correo electrónico según las preferencias del usuario. Por tanto, sería necesario informar el consentimiento y elegir el mecanismo de envío deseado verificando la información necesaria antes de dicha fecha. Según el perfil de acceso a Raíces, cada usuario puede realizar esta comprobación desde el punto de menú “Mis datos -> Datos del usuario/a” o “Utilidades -> Mis datos”.   
- En los accesos desde la app Roble, desde los centros educativos públicos o desde cualquier otro equipo conectado a la red de la Comunidad de Madrid, no habrá ningún cambio y el acceso se realizará como hasta ahora.   
- En los métodos de acceso seguro (IDentifica, DNIe, certificado electrónico y Cl@ve) tampoco habrá ningún cambio.


En Utilidades >  Mis datos

> Para mejorar la seguridad en sus accesos al sistema, se va a implementar un mecanismo de verificación en dos pasos (autenticación de dos factores-2FA).  
Para ello es necesario disponer de un teléfono móvil y/o un correo electrónico asociados a su cuenta.
Si no informa y autoriza el uso de alguno de estos campos no podrá acceder con usuario y clave al sistema y deberá acceder con otros mecanismos de autenticación seguros (certificado, cl@ve, identifica,etc)  
Autorizo el uso de los siguientes datos para el uso del 2FA  -> un check para marckar
Correo electrónico recuperación clave/2FA  -> campo para rellenar   
Teléfono: -> campo para rellenar   

En principio se debería poder poner el correo de EducaMadrid y poder acceder así desde los centros sin que sean ordenadores de la intranet de la comunidad.  

**2 diciembre 2024**

Se recibe correo dirigido a equipos directivos 

> Asunto: 	Acceso a Raíces - Doble factor de autenticación  
Fecha: 	02-12-2024 08:25  
De: 	Raíces <raices@educa.madrid.org>  
Destinatario:  
Estimado/a director/a:  
A partir del 26 de diciembre se va a introducir el doble factor de autenticación para acceder a Raíces con usuario y contraseña desde Internet. Se ha publicado el siguiente aviso en Raíces:  
A partir del 26 de diciembre va a cambiar la forma de acceder a Raíces desde Internet por cuestiones de seguridad. En caso de acceder con usuario y contraseña va a ser necesario introducir un código que se recibirá por SMS o correo electrónico según las preferencias del usuario. Por tanto, sería necesario informar el consentimiento y elegir el mecanismo de envío deseado verificando la información necesaria antes de dicha fecha. Según el perfil de acceso a Raíces, cada usuario puede realizar esta comprobación desde el punto de menú “Mis datos -> Datos del usuario/a” o “Utilidades -> Mis datos”.  
En los accesos desde la app Roble, desde los centros educativos públicos o desde cualquier otro equipo conectado a la red de la Comunidad de Madrid, no habrá ningún cambio y el acceso se realizará como hasta ahora.  
En los métodos de acceso seguro (IDentifica, DNIe, certificado electrónico y Cl@ve) tampoco habrá ningún cambio.  
Los usuarios que no hayan informado el consentimiento y elegido entre el teléfono o el correo electrónico como mecanismo para la recepción del código, si no disponen de alguno de los métodos de acceso seguro (IDentifica, DNIe, certificado electrónico y Cl@ve) una vez activado el doble factor de autenticación, tendrán que solicitar al centro la generación de una nueva contraseña. En el primer acceso con la nueva contraseña, tendrán la posibilidad de informar el consentimiento y elegir el mecanismo de envío.  
En cualquier caso, para minimizar estos casos, le rogamos que traslade esta información a todos los usuarios de Raíces de su centro, tanto empleados como, especialmente, tutores legales/alumnos emancipados según sea el caso.  
Un saludo y gracias por su colaboración:  
El equipo de Raíces.   

### Reclamación a DPD

En lugar de usar la dirección de correo indicada en [Contacta con nosotros](https://dpd.educa2.madrid.org/contacta-con-nosotros) lo planteo vía registro: si se envía por correo no es oficial y la respuesta no es vinculante, no hay tiempo de repuesta ni deben dedicar recursos a responder lo no oficial si están desbordados.   

**21 enero 2024**  

Planteo texto de reclamación   

Lo presento vía [Formulario de solicitud genérica](https://sede.comunidad.madrid/prestacion-social/formulario-solicitud-generica)  
Asunto Reclamación sobre uso laboral del móvil particular en Consejería de Educación  
No deja mucho texto, así que debo adjuntar documento separado

---
Expone:


El 15 enero 2024 apareció un aviso en Raíces con el siguiente texto, sin que a día 21 enero 2024 haya recibido otro tipo de comunicación oficial:  

15 enero Nuevo acceso a Raíces – Doble factor de autenticación  
Próximamente se va a cambiar la forma de acceder a Raíces desde Internet para mejorar la seguridad. Además de incluir nuevos métodos de acceso seguro, en caso de acceder con usuario y contraseña va a ser necesario introducir un código que se recibirá por SMS al teléfono móvil que se tenga informado en Raíces. Por tanto, sería necesario comprobar que el número de teléfono esté informado en Raíces y sea correcto o, en caso contrario, introducirlo. Según el perfil de acceso a Raíces, cada usuario puede realizar esta comprobación desde el punto de menú ‘Mis datos -> Datos del usuario/a’ o ‘Utilidades -> Mis datos’.  
En los accesos desde la app Roble, desde los centros educativos o desde cualquier otro equipo conectado a la red de la Comunidad de Madrid, no habrá ningún cambio.  

Entiendo que se está pidiendo a los docentes públicos que utilicen su móvil particular (la consejería no les facilita un móvil) para un uso laboral  
El comentario asociado a que "desde los centros educativos o desde cualquier otro equipo conectado a la red de la Comunidad de Madrid, no habrá ningún cambio." entiendo que aplica a los ordenadores en la intranet de la consejería de educación, que en los centros educativos no son numerosos: existen en puestos de secretaría, equipos directivos y puntualmente en sala de profesores alguno, sin que existan en las aulas.  
Según lo indicado sí supondrá un cambio para la mayoría de docentes en la tarea habitual de poner faltas de asistencia que se realiza de forma habitual desde ordenadores del aula, ya que no podrán realizarla si no facilitan su móvil particular.  

Considero que este plantamiento es incorrecto desde una doble perspectiva:  

* Protección de datos. El uso del móvil particular para uso laboral no está legitimado por no cumplirse los requisitos de artículo 6.1  
Procede citar sanción AEPD a Ayuntamiento de Madrid [Expediente N.º: EXP202100091 RESOLUCIÓN DE PROCEDIMIENTO SANCIONADOR - aepd.es](https://www.aepd.es/documento/ps-00480-2021.pdf)  

> Por tanto, habría que señalar que las bases jurídicas que podrían legitimar
supuestamente el tratamiento de datos por parte del Ayuntamiento son principalmente
dos: la necesidad del tratamiento para el cumplimiento de una obligación legal
aplicable al responsable del tratamiento definida en la letra c), o bien, la necesidad del
tratamiento para el cumplimiento de una misión realizada en interés público o en el
ejercicio de poderes públicos conferidos al responsable del tratamiento, definida en la
letra e).

De forma análoga a lo indicado en ese caso  
> ...el tratamiento de datos llevado a cabo no puede ampararse en la
letra c) del artículo 6.1 del RGPD ya exige que este sea necesario para el
cumplimiento de una obligación legal aplicable al responsable del tratamiento, por lo
que a sensu contrario, si el tratamiento no es necesario para el cumplimiento de dicha
obligación el tratamiento deviene ilícito.  
> Y ninguna de las normas señaladas contienen precepto alguno que determine
que dicho tratamiento es necesario para el cumplimiento de las misma...
> .. tal actividad no puede estar legitimada en el cumplimiento de una
obligación legal sino como bien señala en su escrito el reclamado en una adaptación a
las nuevas tecnologías, ...  

Creo que se puede resumir para este caso que la Consejería de Educación está amparándose en "para mejorar la seguridad" pero eso no evita que no exista un consentimiento del trabajador libre, informado y expreso del empleado público para usar su móvil particular. El consentimieno libre supone que debe existir una opción de que no se use su móvil particular.  

En respuesta recibida el 31 julio 2020 el DPD indicó "**Dado que no existe normativa al respecto, el trabajador puede negarse a emplear sus dispositivos privados sin necesidad de argumentación alguna.**"  

* Laboral: en un trabajo por cuenta ajena la administración como empleador está pidiendo al empleado que utilice un medio particular  
Si bien es cierto que la ley no recoge nada explícitamente al respecto, sí existen sentencias asociadas como la de la Audiencia Nacional 2019 sobre Telepizza [CENDOJ Roj: SAN 136/2019 - ECLI:ES:AN:2019:136](https://www.poderjudicial.es/search/AN/openDocument/8ed60e51766c4e3e/20190219)  
Esa sentencia no es sobre empleados públicos y cita el Estatuto de los Trabajadores, pero las ideas generales son aplicables al Estatuto Básico del Empleado público, que cita "dispositivos digitales puestos a su disposición"  

En [Artículo 14 Real Decreto Legislativo 5/2015, de 30 de octubre, por el que se aprueba el texto refundido de la Ley del Estatuto Básico del Empleado Público.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a14)  
> j bis) A la intimidad en el uso de dispositivos digitales puestos a su disposición y frente al uso de dispositivos de videovigilancia y geolocalización, así como a la desconexión digital en los términos establecidos en la legislación vigente en materia de protección de datos personales y garantía de los derechos digitales  

En resumen supone quebrar con la necesaria ajenidad en los medios que caracteriza los contratos por cuenta ajena  

Solicito que se anule la obligatoriedad de facilitar el número de teléfono personal para un uso laboral  

---  

09/121883.9/24

**29 febrero 2024**

Recibo [respuesta](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-02-29-Respuesta+reclamante+doble+autenticaci%C3%B3n+Ra%C3%ADces.pdf) indicando que fue remitida a la Dirección General de Estrategia Digital

**9 abril 2024**

Recibo [respuesta](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-04-08-Contestaci%C3%B3n+Denuncia+2FA+Raices.pdf) de MadridDigital.  

Fechada 8 abril, posterior a mi reclamación a AEPD de 17 marzo. Podría ser usado por ellos como respuesta a AEPD.

Cito párrafos y los comento:

> nos congratula informarle que precisamente la
implantación de esta medida de seguridad tiene por objeto reforzar y garantizar la integridad,
privacidad y confidencialidad de **sus** datos personales.

> la aplicación del doble factor como medida de seguridad para el acceso al sistema de información Raíces, se efectúa en cumplimiento de ...

En esas fraces dicen que están obligados a implimentar 2FA por normativa para **mis** datos personales, pero es para **los** datos personales de la aplicación, no para datos personales no laborales como el móvil: ya comentado a AEPD que el cumplimiento de la obligación legal  no implica que puedan usar móvil personal.  

> Esta Agencia está trabajando en la definición e implantación de la medida legal referida, que
impida la suplantación de identidad en el sistema, y en ese sentido se están llevando a cabo
todas las actuaciones necesarias, para que la recabación del dato cuente con base
legitimadora y responda a los fines y medios definidos por el responsable del tratamiento.

En ese párrafo dicen que van a contar con base legitimadora: ya comentado a AEPD que según el mensaje en la web "micuenta" argumentan como base legitimadora solo la normativa que les obliga a implementar 2FA, sin legitimación para usar un dato no laboral como es el móvil personal.   

> Con mayor o menor acierto, la comunicación emitida es meramente informativa y la medida
de seguridad de doble factor, que protege la seguridad de los datos de todos los usuarios de
Raíces, se implementará en su momento con todas las garantías para el ciudadano, en
cumplimiento de la LOPDGDD y RGPD. Entre tales garantías, cabe mencionar la cláusula
del deber de información y la recabación del consentimiento de forma clara e informada,
siempre que la base legitimadora fuere el consentimiento.

En este párrafo dicen que el mensaje de 15 enero era solo informativo y quizá poco afortunado e indican que recabarán el consentimiento aunque aclaran "siempre que la base legitimadora fuere el consentimiento", luego lo dejan como no seguro.

> No obstante, estando en periodo de definición de implantación de la medida, se trabaja en
medios alternativos de autenticación, donde no sea preciso proporcionar el teléfono móvil
particular para poder acceder a Raíces; como el certificado electrónico, la Clave/Pin o
Identifica.

En este párrafo dicen que están buscando alternativas al móvil particular. 

Certificado electrónico no es válido en ordenadores en el centro de acceso a varias personas pero no en intranet.

Clave/Pin e Identifica no son válidos porque de nuevo dependen de un dispositivo móvil que sería el particular. Raíces dependería de un sistema de auntenticación externo en el que el empleado de nuevo tendría que poner su móvil particular para uso laboral.

> En todo caso, siempre podrá hacerse la conexión a Raíces desde el centro educativo, a
través de la Intranet educativa de la Comunidad de Madrid o a través de la App Roble, si
actúa como tutor/padre/madre de un alumno

En este párrafo insisten en lo que indicaba mensaje 15 enero: comentado a AEPD que no es realista, porque la realidad es que muy pocos ordenadores del centro están en intranet.

### Reclamación a AEPD

En función de lo que responda DPD  
[Sede electrónica AEPD](https://sedeagpd.gob.es/sede-electronica-web/)  
[Reclamaciones AEPD](https://sedeagpd.gob.es/sede-electronica-web/vistas/infoSede/tramitesCiudadanoReclamaciones.jsf)  


Planteo la reclamación:

---

Planteo reclamación porque considero que la Comunidad de Madrid está incumpliendo normativa de protección de datos:

Antecedentes:

El 15 enero 2024 apareció un aviso en la aplicación Raíces que uso como empleado público: el aviso aparece en el documento adjunto a reclamación planteada ante DPD de la Consejería de Educación de Madrid el 21 enero 2024. Adjunto como [DOCUMENTO 1.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-01-21-ExponeSolicitaDPD-Raices.pdf)

El 29 febrero 2024 recibí respuesta de DPD de la Consejería de Educación de Madrid indicando que fue remitida a la Dirección General de Estrategia Digital. Adjunto como [DOCUMENTO 2.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-02-29-Respuesta+reclamante+doble+autenticaci%C3%B3n+Ra%C3%ADces.pdf)
 
El 12 marzo 2024 recibí correo mencionando doble autenticación que enlazaba https://gestiona.comunidad.madrid/comunicacion_mdout/2024/FaroDoblefactor/Doblefactor.html. Adjunto como [DOCUMENTO 3.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-03-12-FARO%20INFORMA%20Acceso%20doble%20factor.pdf)

Accediendo a https://gestiona7.madrid.org/usug_micuenta/ he comprobado que aparece el siguiente mensaje

> Información sobre protección de datos y Doble Factor de Autenticación  
Le informamos que los datos de carácter personal serán tratados, en su calidad de Responsable del Tratamiento, por la Agencia para la Administración Digital de la Comunidad de Madrid, e incorporados a la actividad de tratamiento IMPLEMENTACIÓN DEL DOBLE FACTOR DE AUTENTICACIÓN (2FA), cuya finalidad es la gestión del acceso e implantación de la medida de seguridad Doble Factor de Autenticación exigida por el Esquema Nacional de Seguridad, cuya aplicación deviene obligatoria en virtud de la disposición adicional primera "Medidas de seguridad en el ámbito del sector público" de la Ley Orgánica 3/2018 de Protección de Datos, siendo el Doble Factor una medida requerida para el acceso seguro a los sistemas de información de la Comunidad de Madrid en situaciones de teletrabajo, trabajo en movilidad **o cualquier otra modalidad de acceso remoto**. Asimismo, el dato de número de teléfono móvil recabado, podrá utilizarse como canal de soporte informático para contactar directamente con usted, en caso de que necesite ayuda para una incidencia en el acceso.  
  Dicha finalidad tiene su base de legitimación en el cumplimiento de una obligación legal artículo 6.1.c) Reglamento 2016/679 General de Protección de Datos (en relación con el art. 10 apartados a) y d) de la Ley 7/2005, de 23 de diciembre, de Medidas Fiscales y Administrativas, por las amplias atribuciones que confiere de forma exclusiva a la Agencia para la Administración Digital de la CAM en materia de seguridad y sistemas de la información); en el cumplimiento de una misión realizada en interés público o en el ejercicio de poderes públicos artículo 6.1.e) RGPD, por referirse a una actividad enmarcada en el contexto de una Administración Pública; y en el consentimiento para el tratamiento de sus datos personales artículo 6.1.a) RGPD, al ser susceptibles de recabarse en orden a la implementación de la medida.  
  Las categorías de datos personales afectadas por este tratamiento son datos identificativos, número de teléfono proporcionado por el interesado y datos recabados por la aplicación de Doble Factor como localización (sólo a los efectos puntuales de identificar el país donde se ubica el interesado y confirmar la viabilidad técnica de garantizar un acceso seguro al sistema) correo electrónico, ID de usuario, direcciones IP, siendo el objetivo verificar la identidad del sujeto que pretende acceder al sistema antes de que este acceso se produzca. Son datos imprescindibles cuyo tratamiento está sujeto al principio de minimización.  
  No está previsto efectuar comunicación de dichos datos a terceros ni efectuar transferencias internacionales con los mismos, limitándose su uso a las labores internas de identificación y operativas antedichas. El plazo de conservación de estos datos será el imprescindible para gestionar, validar y registrar un acceso adecuado, permaneciendo bloqueados tras su supresión, con el fin de afrontar las responsabilidades que pudieran derivar del cumplimiento de requerimientos a instancias de las autoridades judiciales o administrativas.  
  En lo relativo al ejercicio de sus derechos, puede solicitar el acceso, rectificación, supresión, limitación u oposición al tratamiento, así como el derecho a revocar su consentimiento -en el caso de se den los requisitos establecidos en el Reglamento 2016/679 General de Protección de Datos, así como en la Ley Orgánica 3/2018, de Protección de Datos Personal ‐ a través del Registro Electrónico o Registro Presencial o en los lugares y formas previstos en el artículo 16.4 de la Ley 39/2015, preferentemente mediante el formulario de solicitud "Ejercicio de derechos en materia de protección de datos personales". Así mismo, tiene derecho a presentar una reclamación ante la Agencia Española de Protección de Datos.  
  Podrá ampliar información adicional detallada sobre protección de datos en las siguiente dirección web:
www.comunidad.madrid/protecciondedatos.

No cuestiono que según Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales. [Disposición adicional primera. Medidas de seguridad en el ámbito del sector público.](https://www.boe.es/buscar/act.php?id=BOE-A-2018-16673#da) se deba implementar el Esquema Nacional de Seguridad y que implementarlo según [Real Decreto 311/2022, de 3 de mayo, por el que se regula el Esquema Nacional de Seguridad.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-7191) 4.2.6 Mecanismo de autenticación (usuarios de la organización) [op.acc.6] suponga que para el acceso desde o a través de zonas no controladas como internet se requerirá un doble factor de autenticación, pero sí considero incorrecto cómo lo pretende implementar la Comunidad de Madrid para sus empleados públicos, como es mi caso en educación. Realizo argumentación a nivel de protección de datos aunque en este caso se entremezcla con argumentación a nivel laboral.

Indican como base de legitimación en el cumplimiento de una obligación legal artículo 6.1 de RGPD [Reglamento (UE) 2016/679](https://www.boe.es/buscar/doc.php?id=DOUE-L-2016-80807) citando 6.1.a (el interesado dio su consentimiento para el tratamiento de sus datos personales para uno o varios fines específicos) y 6.1.c (el tratamiento es necesario para el cumplimiento de una obligación legal aplicable al responsable del tratamiento)  

Incumplimientos RGPD:

1. **Incumplimiento legitimación 6.1.a**

El consentimiento del interesado debe ser libre según considerando 32 RGPD, y no es libre si el empleador no da otra opción al empleado que el uso laboral de un recurso privado, y ese uso laboral de un recurso privado no está amparado por RGPD en 6.1.c como se comenta más adelante.

Procede citar (se cita en escrito enero 2024 a DPD) https://www.poderjudicial.es/search/AN/openDocument/8ed60e51766c4e3e/20190219 JURISPRUDENCIA Roj: SAN 136/2019 - ECLI:ES:AN:2019:136 en la que se indica

>Consideramos que la exigencia de la aportación de un teléfono móvil con conexión de datos para desarrollar el trabajo en los términos efectuados supone un manifiesto abuso de derecho empresarial, ya que además de quebrar con la necesaria ajenidad en los medios que caracteriza la nota de ajenidad del contrato de trabajo

y si bien se cita el Estatuto de Trabajadores la idea esencial aplica a empleados públicos al ser también trabajadores por cuenta ajena.

Procede citar también https://www.poderjudicial.es/search/AN/openDocument/287716d49e2d646f/20210301 JURISPRUDENCIA Roj: STS 518/2021 - ECLI:ES:TS:2021:518 en la que se indica 

> Respecto de las restantes pretensiones, relativas a la anulación del proyecto por otras razones (exigir
aportación de un teléfono móvil con conexión a internet por parte del trabajador y uso en él de aplicaciones
informáticas de la empresa para su geolocalización, y la nulidad también del clausulado del contrato de
trabajo que afecta a dichos extremos, debiendo en otro caso ser la empresa la que proporcione dicha
herramienta y nulidad de las medidas disciplinarias que el proyecto impone y compensaciones económicas
por la aportación del móvil con internet fijadas por la empresa), la Sala de instancia considera que el proyecto
empresarial que se impugna vulnera el derecho de privacidad de los afectados por el conflicto al no superar
el juicio de proporcionalidad cuando puede acudirse a medidas de menor injerencia en aquel derecho como
la implantación de dicho sistema en los vehículos en los que se desplazan los trabajadores para llevar los
pedidos o pulseras con dicho dispositivo que sustituye la aportación de la herramienta por el empleado y no
tiene datos de carácter personal, como seria el numero de teléfono o la dirección email -precisa para descargar
la aplicación-. Además, considera que no se ha dado a los trabajadores la oportuna información del art. 12 y
13 del Reglamento 679/2016 de la Ley entonces vigente, actual art. 11 de la Ley 3/2018.

Procede citar también [Ley 10/2021, de 9 de julio, de trabajo a distancia.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-11472)   
[Artículo 11. Derecho a la dotación suficiente y mantenimiento de medios, equipos y herramientas.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-11472#a1-3)  
> 1. Las personas que trabajan a distancia tendrán derecho a la **dotación y mantenimiento adecuado por parte de la empresa de todos los medios, equipos y herramientas necesarios para el desarrollo de la actividad**, de conformidad con el inventario incorporado en el acuerdo referido en el artículo 7 y con los términos establecidos, en su caso, en el convenio o acuerdo colectivo de aplicación. En el caso de personas con discapacidad trabajadoras, la empresa asegurará que esos medios, equipos y herramientas, incluidos los digitales, sean universalmente accesibles, para evitar cualquier exclusión por esta causa.  
>2. Asimismo, se garantizará la atención precisa en el caso de dificultades técnicas, especialmente en el caso de teletrabajo.  

[Artículo 17. Derecho a la intimidad y a la protección de datos.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-11472#a1-9)  
> 2. La empresa no podrá exigir la instalación de programas o aplicaciones en dispositivos propiedad de la persona trabajadora, ni la utilización de estos dispositivos en el desarrollo del trabajo a distancia.

Aunque se pueda considerar el cambio que pretende realizar la Comunidad de Madrid para los docentes no se trate de un trabajo a distancia, según definición de [Artículo 2. Definiciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-11472#a2) se cita "con carácter regular", y el horario de los docentes tiene una parte de jornada que se realiza de forma regular fuera del centro, según se puede ver en [V. Horarios de los Profesores](https://www.boe.es/buscar/act.php?id=BOE-A-1994-15565#v) donde solamente 30 horas de la jornada laboral son de obligada permanencia en el centro.

Procede citar también Real Decreto Legislativo 5/2015, de 30 de octubre, por el que se aprueba el texto refundido de la Ley del Estatuto Básico del Empleado Público (en adelante EBEP) [Artículo 47 bis. Teletrabajo.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a4-2)  
> 4. La Administración proporcionará y mantendrá a las personas que trabajen en esta modalidad, los medios tecnológicos necesarios para su actividad

Procede citar el [ACUERDO POR EL QUE SE REGULA LA MODALIDAD DE TELETRABAJO EN EL ÁMBITO DEL PERSONAL DOCENTE DE ENSEÑANZA PÚBLICA NO UNIVERSITARIA DE LA COMUNIDAD DE MADRID](https://www.comunidad.madrid/sites/default/files/doc/educacion/rh20/rh20_acuerdo_teletrabajo_personal_docente_sin_firma.pdf) de 2021, en el que se indica que se facilita dispositivo al docente  
> Artículo 7 Derechos y deberes  
>5. El centro docente del que dependa el funcionario docente facilitará los siguientes recursos para el desempeño del puesto en la
modalidad de teletrabajo:  
a) Un ordenador personal de los que cuenta el centro docente para la prestación del servicio en régimen de teletrabajo.

(no entro a valorar que en mi opinión el acuerdo firmado por los sindicatos en artículo 7.4.b contradice artículo 47.bis EBEP en cuanto a conexión, solo lo cito para reforzar la idea de que el docente recibe los dispositivos que necesita, sin aportar dispositivos personales)

Procede citar que el cambio asociado a la obligatoriedad de usar el móvil personal es un cambio laboral que según EBEP [Artículo 33. Negociación colectiva.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a33) y no existe constancia de que se haya negociado con los sindicatos. 

Por último procede citar que el empleado no tiene obligación no solo de aportar su teléfono móvil particular, sino que no tiene obligación de disponer de él.

2. **Incumplimiento legitimación 6.1.c**

El cumplimiento de la obligación legal por la administración de implementar el ENS no implica que el empleado público deba facilitar su número de teléfono personal para un uso laboral, existes otras vías de cumplimiento. Por ejemplo:  
- Que el responsable del tratamiento facilite al empleado público, que es un empleado por cuenta ajena, los recursos necesarios para realizar cumplir su obligación legal, como sería facilitarle un móvil. Está relacionado con 6.1.a, ya que se si no se facilita y se obliga a usar el móvil personal como única opción no es un consentimiento libre.  
- Que el responsable de tratamiento modifique su gestión de red de modo que todos los ordenadores de los centros educativos estén en la intranet y no en internet, lo que evitaría la doble autenticación desde el centro, que es el lugar de trabajo de un empleado por cuenta ajena, y que el responsable de tratamiento permita el acceso vía certificado desde fuera del centro, lo que encaja con lo indicado en el ENS, que obliga a R8 que supone R2, R3 ó R4 (se está forzando R2 cuando también es válido R3 y R4)  

Procede citar (se cita en escrito enero 2024 a DPD) https://www.aepd.es/documento/ps-00480-2021.pdf en la que el reclamado alegaba 6.1.c y se indica   
> el tratamiento de datos llevado a cabo no puede ampararse en la letra c) del artículo 6.1 del RGPD ya exige que este sea necesario para el cumplimiento de una obligación legal aplicable al responsable del tratamiento, por lo que a sensu contrario, si el tratamiento no es necesario para el cumplimiento de dicha obligación el tratamiento deviene ilícito.  
...  
estando acreditado que el reclamado ha tratado el dato del número de móvil de los agentes siendo uno de los elemento o requisito exigido por el software contratado, para el acceso al buzón compartido mediante la introducción de un código que se remite mediante mensaje al móvil del usuario, como medio de verificación de su identidad sin que este contemplado en norma alguna, no puede operar como base legitimadora del tratamiento el cumplimiento de una obligación legal (letra c, del artículo 6.1 del RGPD)

Procede citar Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas. [Artículo 14. Derecho y obligación de relacionarse electrónicamente con las Administraciones Públicas.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-10565#a14)  
> 2. En todo caso, estarán obligados a relacionarse a través de medios electrónicos con las Administraciones Públicas para la realización de cualquier trámite de un procedimiento administrativo, al menos, los siguientes sujetos:  
...  
e) Los empleados de las Administraciones Públicas para los trámites y actuaciones que realicen con ellas por razón de su condición de empleado público, en la forma en que se determine reglamentariamente por cada Administración.  

Por lo tanto todos los empleados públicos tienen certificado digital y pueden utilizar el certificado cumpliendo R8 sin usar su móvil.

Respecto a la afirmación en el mensaje de 15 enero "En los accesos desde la app Roble, desde los centros educativos o desde cualquier Fotro equipo conectado a la red de la Comunidad de Madrid, no habrá ningún cambio.", procede destacar que la mayoría de los ordenadores de los centros educativos no están en la intranet, sino en internet, por lo que si un docente está en el aula o en la sala de profes y quiere realizar una tarea habitual y diaria como pasar lista y poner faltas, se le está obligando a que lleve su móvil personal para pasar lista y poner faltas EN SU CENTRO DE TRABAJO, y eso se debe a que no todos los ordenadores de los centros educativos estén en intranet.

3. Considero que no se ha realizado la EVALUACIÓN DE LA NECESIDAD Y PROPORCIONALIDAD DEL TRATAMIENTO que indica [Gestión del riesgo y evaluación de impacto en tratamientos de datos  personales](https://www.aepd.es/guias/gestion-riesgo-y-evaluacion-impacto-en-tratamientos-datos-personales.pdf) remitiendo a artículo 35.7.b de RGPD.  
> 7. La evaluación deberá incluir como mínimo:  
b) una evaluación de la necesidad y la proporcionalidad de las operaciones de tratamiento con respecto a su finalidad;  

---

Al tramitar obliga a poner texto de descripción de mínimo 300 caracteres

La comunidad de Madrid pretende implantar un sistema de doble autenticación en el que hace obligatorio al empleado público que aporte su móvil personal, lo que considero no que no cumple normativa de protección de datos en cuanto a legitimación 6.1.a y 6.1.c RGPD ni se ha realizado una evaluación de necesidad y proporcionalidad del tratamiento según artículo 35.7.b RGPD

Adjunto [este documento 2024-03-17-ReclamaciónAEPD2FA.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2FAMadrid/2024-03-17-Reclamaci%C3%B3nAEPD2FA.pdf)

Presentado 17 marzo 2024 con Número de registro:REGAGE24e00020389284

### Teletrabajo

Los dispositivos son algo que debe facilitar la empresa, en este caso la administración, y la responsabilidad de protección de datos está asociada a esos dispositivos.  
Si redireccionar el correo no es viable porque los datos no están en dispositivos de la administración, tampoco sería posible usar un dispositivo personal y almacenar el correo en local.  
Intento recopilar ideas en orden cronológico a partir de marzo 2020, inicio de la pandemia   

**28 marzo 2020**  
[twitter FiQuiPedia/status/1243942154638163968](https://twitter.com/FiQuiPedia/status/1243942154638163968)  
Se habla de que los docentes estamos teletrabajando en casa, y creo que procede compartir este texto del contrato de teletrabajo que firmé con una empresa privada el 1 febrero 2005.  
Y sería similar para "telealumnos" en etapas con educación gratuita  
[Artículo 88. Garantías de gratuidad. LOE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a88)  
![](https://pbs.twimg.com/media/EUNeyj5WsAERAwM?format=jpg)  

**26 mayo 2020**  
[twitter FiQuiPedia/status/1265215274103554048](https://twitter.com/FiQuiPedia/status/1265215274103554048)  
Si uso un cliente de correo en un dispositivo que no me facilita @educacmadrid y descargo correos, el responsable de esos datos y de su seguridad paso a ser yo. Mientras no me den dispositivo y almacenamiento seguro, debe ser correo web y dimensionarse como tal.  

**27 mayo 2020**  
[twitter diegoredondo13/status/1265688171557326849](https://twitter.com/diegoredondo13/status/1265688171557326849)  
Atención a la respuesta de la Delegación de Protección de Datos sobre el uso de la Red doméstica de Internet para el teletrabajo.  
![](https://pbs.twimg.com/media/EZCgrVEWsAAQ1Pr?format=jpg)  

Dentro de portalCAU hay un apatado nuevo 2020 sobre trabajo remoto  

[Trabajo remoto - enlace no operativo en 2024](https://gestiona3.madrid.org/portalcau/index.php/conexion-remota/inicio-conexion-remoto)  

Ahí se cita ISLA
[Guía ampliada de conexión ISLA -enlace no operativo en 2024](https://gestiona3.madrid.org/portalcau/index.php/centro-de-documentacion/send/114-conexiones/93-isla-acceso-y-soporte-remoto-ras)  

También VPN
[enlace no operativo en 2024](https://gestiona3.madrid.org/portalcau/index.php/centro-de-documentacion/send/114-conexiones/95-guia-rapida-de-acceso-vpn)  

En 2024 validando enlaces de 2020 encuentro este enlace que cita ISL  
[Descargas soporte remoto Madrid Digital - madrid.org](https://ras.madrid.org/users/main/downloads.html)  

[Acceso Remoto C.M.](https://ar.madrid.org/sslvpn/Login/Login)  

[Guía ampliada de Recomendaciones de seguridad de la información para el trabajo en remoto. Marzo 2020](https://dpd.educa2.madrid.org/web/educamadrid/principal/files/69a6601a-e199-4519-a2eb-192663576204/Recomendaciones/RecomSeguridadTRemoto-ampliada%202.pdf?t=1591203632086)  

Y ahí se oficializa usar equipos personales (asociado a situación excepcional COVID)  
[twitter FiQuiPedia/status/1288469378485424129](https://twitter.com/FiQuiPedia/status/1288469378485424129)  
RECOMENDACIONES DE SEGURIDAD DE LA INFORMACIÓN PARA EL TRABAJO REMOTO @ComunidadMadrid, marzo 2020  
"aquellos empleados ... que hagáis uso de vuestro equipo personal."  
Oficializan teletrabajo con medios personales.  
![](https://pbs.twimg.com/media/EeGQERnXkAEGJY_?format=jpg)  
![](https://pbs.twimg.com/media/EeGQEgzWAAI5pAR?format=jpg)  

**3 junio 2020**  
[Brechas de seguridad: el correo electrónico y las plataformas de productividad online](https://www.aepd.es/es/prensa-y-comunicacion/blog/brechas-seguridad-correo-electronico-plataformas-productividad)  
Establecer políticas restrictivas de acceso a las herramientas de productividad corporativas para uso personal o desde dispositivos no corporativos. En caso de permitirlo, establecer medidas de seguridad apropiadas y mecanismos de compartimentación de la información que **mantengan separados el ámbito personal del profesional**.  


Planteo consulta a DPD [contacta con nosotros](https://dpd.educa2.madrid.org/contacta-con-nosotros)  

---
Quiero trasladar las siguientes consultas sobre privacidad y protección de datos asocidas a uso de medios personales a mi condición empleado público de la Consejería de Educación en la Comunidad de Madrid, donde soy docente de secundaria. La consulta es suficientemente general, y aplica tanto a funcionarios como a contración de interinos.  
Dado el interés general, anticipo que deseo compartir públicamente la respuesta recibida para que oriente a otros docentes, por lo que solicito permiso para compartirla.  

1. ¿La consejería puede exigirme que aporte y use mis medios personales (dispositivos, conexiones) para mi labor profesional como docente en la consejería de educación?
 
2. En caso de respuesta negativa.  
2.1 Si decido aportarlos y usarlos porque la consejería no lo hace, ¿puedo asumir responsabilidades asociadas a protección de datos si los utilizo?  
2.1 Si decido no aportarlos ni usarlos, ¿de qué manera debo argumentar a la organización las razones para abstenerme (no tener dispositivo, no tener conectividad, no saber o garantizar medidas de seguridad en mi dispositivo, no saber o garantizar medidas de seguridad en mi conexioń, o no tener obligación de asumir responsabilidades de protección de datos profesionales en medios personales)?

3. En caso de respuesta positiva.
3.1 ¿Puedo asumir responsabilidades asociadas a protección de datos si los utilizo?
3.2 ¿Puede exigirme la consejería que pague, instale y configure aplicaciones (por ejemplo antivirus) y asumir responsabilidades asociadas a protección de datos si no lo realizo?

Preguntar por si se puede exigir puede parecer forzado, pero durante el periodo de confinamiento muchos docentes de la consejería hemos aportado y usado nuestros medios, por lo que esa exigencia se ha realizado de facto, y si volviera a haber un confinamiento, volvería a ocurrir.

Existe documentación (A) donde se oficializa que se puede teletrabajar con medios personales

Existe documentacioń (B) donde se muestra que la consejería ha valorado en la contratación de docentes "si tiene posibilidades de teletrabajo y si dispone de la competencia digital"


A: [Guía ampliada de Recomendaciones de seguridad de la información para el trabajo en remoto. Marzo 2020](https://dpd.educa2.madrid.org/web/educamadrid/principal/files/69a6601a-e199-4519-a2eb-192663576204/Recomendaciones/RecomSeguridadTRemoto-ampliada%202.pdf?t=1591203632086)  
"aquellos empleados ... que hagáis uso de vuestro equipo personal."  
y hay apartado "3 EQUIPOS" donde se indica  
Si utilizas ordenadores particulares  
Cuando uses equipos particulares, hazlo siempre aplicando las recomendaciones de seguridad.  
-Instala y actualiza regularmente las actualizaciones de seguridad del sistema y un programa antivirus en el ordenador particular.  
-Es  importante  disponer  de  un  programa  antivirus  activo  en  el  ordenador  personal,  así  como, asegúrate que se actualizan de manera frecuente dichos programas. En el mercado existen algunos [antivirus gratuitos](https://www.osi.es/es/herramientas-gratuitas?combine=&herramienta_selec%5B%5D=115).
-No compartas tu equipo particular para trabajo remoto con terceras personas,en lo posible. En  la  medida  de  lo  posible,  no  compartas  el  ordenador  con  terceras  personas,  de  este  modo garantizaras que solo se instala software autorizado y se acceden a sita web de confianza.  

B: [REANUDACIÓN DE ACTIVIDAD EDUCATIVA PRESENCIAL EN CENTROS DOCENTES, SUSTITUCIÓN DE FUNCIONARIOS INTERINOS](http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-disposition&blobheadername2=cadena&blobheadervalue1=filename%3D20200609_reanudacion_actividad_presencial_ares_firmada.pdf&blobheadervalue2=language%3Des%26site%3DPortalEducacionRRHH&blobkey=id&blobtable=MungoBlobs&blobwhere=1353002254894&ssbinary=true
)  
CSV 0908666022483546153315  

----

Tras la respuesta me planteo que podré realizar escrito por registro (DAT, Registro electrónico común, portalCAU ...) adjuntando respuesta y pidiendo conectividad y dispositivo en caso de no haber educación 100% presencial.  
En función de la respuesta citar que sin dispositivo no garantizo que use los míos, y de hacerlo, no asumo responsabilidad alguna.  

**24 julio 2020**  
[Borrador de decreto por el que se regula la modalidad de prestación de servicios en régimen de teletrabajo en la administración de la Comunidad de Madrid - feccoo-madrid.org](http://www.feccoo-madrid.org/c0e5dfa3fed168aeca97ba05f4155259000063.pdf)  


**31 julio 2020**  
Recibo [respuesta de DPD](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2020-07-31-RespuestaDPDMediosPropiosUsoProfesional.pdf)    

La pongo aquí (marco en negrita lo que en original está en rojo)  

1. ¿La consejería puede exigirme que aporte y use mis medios personales (dispositivos, conexiones) para mi labor profesional como docente en la consejería de educación?  
**No. El uso de un dispositivo personal para cuestiones laborales no ha sido regulado por el legislador. En el caso por el que nos pregunta se trata de una posibilidad que la organización ha habilitado para que el trabajador la adopte si lo considera conveniente y de manera voluntaria.**  
2. En caso de respuesta negativa.  
2.1 Si decido aportarlos y usarlos porque la consejería no lo hace, ¿puedo asumir responsabilidades asociadas a protección de datos si los utilizo?  
**Como Vd. indica, en caso de que voluntariamente decida utilizar sus dispositivos privados para acceder a aplicaciones o recursos de información de la Consejería, ésta asume riesgos derivados de que dicho acceso a información corporativa se realiza desde dispositivos de los que no tiene el pleno control. A este respecto le aconsejamos la lectura de la [Guía del INCIBE (Instituto de Ciberseguridad) acerca del uso de dispositivos privados en el ámbito laboral](https://www.incibe.es/sites/default/files/contenidos/guias/doc/guia_dispositivos_moviles_metad.pdf). Como verá en esa guía, por lo general las organizaciones que se encuentran en estos casos elaboran y difunden políticas de uso en las que se recomienda al trabajador que tome medidas para incrementar la seguridad de sus dispositivos (bloqueo automático con contraseña, cifrado del contenido, habilitación del borrado remoto para casos de extravío o robo, no conectar con los sistemas de la organización mediante redes WiFi públicas, no compartir con terceros el dispositivo, etc.). En su mensaje Vd. ha incluido las directrices o políticas en este sentido que ha difundido la Comunidad de Madrid, donde se detallan las medidas que se recomiendan al trabajador, y que no sólo disminuyen los riesgos para la organización, sino también para el propio empleado, y en consecuencia no sólo para la protección de los datos corporativos sino también de los propios.**  
2.1 Si decido no aportarlos ni usarlos, ¿de qué manera debo argumentar a la organización las razones para abstenerme (no tener dispositivo, no tener conectividad, no saber o garantizar medidas de seguridad en mi dispositivo, no saber o garantizar medidas de seguridad en mi conexioń, o no tener obligación de asumir responsabilidades de protección de datos profesionales en medios personales)?  
**Dado que no existe normativa al respecto, el trabajador puede negarse a emplear sus dispositivos privados sin necesidad de argumentación alguna.**  
**Apuntamos a continuación varias referencias normativas que constituyen la Regulación actual del uso de dispositivos tecnológicos en la relación laboral:**  
**La Ley Orgánica, 3/2018, de Protección de Datos Personales y garantía de los derechos digitales, regula esta cuestión en dos artículos:
 Artículo 87. Derecho a la intimidad y uso de dispositivos digitales en el ámbito laboral.**  
**1.  Los trabajadores y los empleados públicos tendrán derecho a la protección de su intimidad en el uso de los dispositivos digitales puestos a su disposición por su empleador.**  
**2.  El empleador podrá acceder a los contenidos derivados del uso de medios digitales facilitados a los trabajadores a los solos efectos de controlar el cumplimiento de las obligaciones laborales o estatutarias y de garantizar la integridad de dichos dispositivos.**  
**3.  Los empleadores deberán establecer criterios de utilización de los dispositivos digitales respetando en todo caso los estándares mínimos de protección de su intimidad de acuerdo con los usos sociales y los derechos reconocidos constitucional y legalmente. En su elaboración deberán participar los representantes de los trabajadores.**  
**Artículo 88. Derecho a la desconexión digital en el ámbito laboral**  
**1.  Los trabajadores y los empleados públicos tendrán derecho a la desconexión digital a fin de garantizar, fuera del tiempo de trabajo legal o convencionalmente establecido, el respeto de su tiempo de descanso, permisos y vacaciones, así como de su intimidad personal y familiar.**  
**2.  Las modalidades de ejercicio de este derecho atenderán a la naturaleza y objeto de la relación laboral, potenciarán el derecho a la conciliación de la actividad laboral y la vida personal y familiar y se sujetarán a lo establecido en la negociación colectiva o, en su defecto, a lo acordado entre la empresa y los representantes de los trabajadores.**  
**3.  El empleador, previa audiencia de los representantes de los trabajadores, elaborará una política interna dirigida a trabajadores, incluidos los que ocupen puestos directivos, en la que definirán las modalidades de ejercicio del derecho a la desconexión y las acciones de formación y de sensibilización del personal sobre un uso razonable de las herramientas tecnológicas que evite el riesgo de fatiga informática. En particular, se preservará el derecho a la desconexión digital en los supuestos de realización total o parcial del trabajo a distancia así como en el domicilio del empleado vinculado al uso con fines laborales de herramientas tecnológicas.**  

**Esta Ley Orgánica introduce modificaciones en el Estatuto de los Trabajadores, incorporando el artículo 20 bis:**  
**Artículo 20 bis. Derechos de los trabajadores a la intimidad en relación con el entorno digital y a la desconexión.**  
**Los trabajadores tienen derecho a la intimidad en el uso de los dispositivos digitales puestos a su disposición por el empleador, a la desconexión digital y a la intimidad frente al uso de dispositivos de videovigilancia y geolocalización en los términos establecidos en la legislación vigente en materia de protección de datos personales y garantía de los derechos digitales.**  
**Cómo en el Estatuto Básico del Empleado Público, incorporando dentro del listado de derechos individuales del empleado público, el siguiente:**  
**j bis) A la intimidad en el uso de dispositivos digitales puestos a su disposición y frente al uso de dispositivos de videovigilancia y geolocalización, así como a la desconexión digital en los términos establecidos en la legislación vigente en materia de protección de datos personales y garantía de los derechos digitales.**  


Se cita

[Dispositivos móviles personalespara uso profesional (BYOD). Una guía de aproximación para el empresario. INCIBE_PTE_AproxEmpresario_008_BYOD-2017-v1](https://www.incibe.es/sites/default/files/contenidos/guias/doc/guia_dispositivos_moviles_metad.pdf)  

**12 agosto 2020**  

Planteo escrito

[twitter FiQuiPedia/status/1293485705071661056](https://twitter.com/FiQuiPedia/status/1293485705071661056)  
Puesto vía registro 49/139010.9/20  
El escenario I presencial en Madrid no es realista; sin #VueltaSegura brotes supondrán confinamiento, quizá intermitente y no global en todos los centros, y volverá el teletrabajo.  
![](https://pbs.twimg.com/media/EfNf6G4WoAEepQx?format=png)  

**17 agosto 2020**  
Lo planteo a miembros de la asamblea
[twitter FiQuiPedia/status/1295301857150210049](https://twitter.com/FiQuiPedia/status/1295301857150210049)  

y también lo pongo vía transparencia 49/148108.9/20. En este caso la respuesta negativa sí aporta información. 

**20 agosto 2020**  
[twitter FiQuiPedia/status/1296463817665282050](https://twitter.com/FiQuiPedia/status/1296463817665282050)  
En @educacyl piden a interinos para su nombramiento declaración disponer de medios informáticos y conexión a internet adecuados para ejercer la actividad docente no presencial.   
Tema de solicitarlos, pago uso propios, o cumplir RGPD, qué tal?  
![](https://pbs.twimg.com/media/Ef3279yX0AYsnVw?format=jpg)  

**15 septiembre 2020**  

Documento de Murcia sobre teletrabajo en educación
[CIRCULAR DE LA DIRECCIÓN GENERAL DE PLANIFICACIÓN EDUCATIVA Y RECURSOS HUMANOS RELATIVA A LA APLICACIÓN DE LA RESOLUCIÓN DE 11 DE SEPTIEMBRE DE 2020, POR LA QUE SE DICTAN INSTRUCCIONES EN RELACIÓN CON LA ACTIVIDAD EDUCATIVA EN ESCENARIOS DE NO PRESENCIALIDAD.](https://sede.carm.es/eAConsultaWeb/consulta/ConsultaController.jpf?csv=CARM-b58e36bb-f74d-ef92-7094-0050569b6280)  
> "Para ello, el profesorado DEBERÁ POSEER  los conocimientos técnicos necesarios, así como EL EQUIPO INFORMÁTICO Y LOS SISTEMAS DE COMUNICACIÓN imprescindibles para garantizar la adecuada prestación del servicio."

**17 septiembre 2020**  
[DECRETO 79/2020, de 16 de septiembre, del Consejo de Gobierno, por el que se regula la modalidad de prestación de servicios en régimen de teletrabajo en la Administración de la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2020/09/17/BOCM-20200917-1.PDF)  
![](https://pbs.twimg.com/media/EiGmzn-XgAApiyc?format=jpg)  
>  Artículo 16. Aportaciones  
> 1. El personal que preste sus servicios en la modalidad de teletrabajo se compromete a:  
> ...  
> b) Aportar conexión a internet que cumpla con las características que defina la Comunidad de Madrid, en su caso.  


**23 septiembre 2020**  
[Real Decreto-ley 28/2020, de 22 de septiembre, de trabajo a distancia.](https://www.boe.es/buscar/act.php?id=BOE-A-2020-11043)  
_derogado por ley 10/2021_  

[Disposición transitoria tercera. Trabajo a distancia como medida de contención sanitaria derivada de la COVID-19.](https://www.boe.es/buscar/act.php?id=BOE-A-2020-11043#dt-3)  
> En todo caso, las empresas estarán obligadas a dotar de los medios, equipos, herramientas y consumibles que exige el desarrollo del trabajo a distancia, así como al mantenimiento que resulte necesario  

**1 octubre 2020**  
[twitter FiQuiPedia/status/1311663954821701633](https://twitter.com/FiQuiPedia/status/1311663954821701633)  
Respuesta @educacmadrid sobre teledocencia que resume su gestión y transparencia:
1. ¿Valoración recursos a destinar? Análisis sin informe oficial, no te lo doy  
2. ¿Qué recursos ha destinado/va a destinar? Inadmitido, lo verás cuando se publique contrato  
[2020-10-01 Respuesta Medios Teledocencia](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Protecci%C3%B3nDatos/2020-10-01-RespuestaMediosTeledocencia.pdf)  
3. ¿Análisis riesgos que Consejería asume derivados acceso información corporativa se realiza desde dispositivos que no tiene pleno control? (es texto de respuesta de su DPD)  
Valoración particular del interesado y no tiene competencia para pronunciarse.  
4. ¿Qué alternativa se ha planteado en caso de inviabilidad de los escenarios  que implican clases online por depender de un uso de recursos personales que la Consejería no puede garantizar?  
No contempla la inviabilidad de ningún escenario presentado.  

**7 octubre 2020**  
@educacyl exigió durante el confinamiento que los interinos docentes aportasen medios propios para teledocencia.  
Excepcional por urgencia y necesidad.  
No hubo sanción ni rechazo, se consideró renuncia justificada.  
@nuriarlopez  
[2020-10-07 Resolucion CyL Medios Interinos](https://drive.google.com/file/d/1vzNxIDF2mwESsmppCn4AqtalrEBfYwQn/view?usp=drive_link)  

[twitter FiQuiPedia/status/1313621215316193280](https://twitter.com/FiQuiPedia/status/1313621215316193280)  
Real Decrelo-Ley 29/2020 ha añadido [art47bis en EBEP](https://boe.es/buscar/act.php?id=BOE-A-2015-11719#a4-2) y ya regula teletrabajo para empleados públicos  
"4. La Administración proporcionará y mantendrá a las personas que trabajen en esta modalidad, los medios tecnológicos necesarios para su actividad."  

**10 julio 2021**  
[Ley 10/2021, de 9 de julio, de trabajo a distancia.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-11472)
[Artículo 11. Derecho a la dotación suficiente y mantenimiento de medios, equipos y herramientas.](https://www.boe.es/buscar/act.php?id=BOE-A-2021-11472#a1-3)  
>1. Las personas que trabajan a distancia tendrán derecho a la **dotación y mantenimiento adecuado por parte de la empresa de todos los medios, equipos y herramientas necesarios para el desarrollo de la actividad**, de conformidad con el inventario incorporado en el acuerdo referido en el artículo 7 y con los términos establecidos, en su caso, en el convenio o acuerdo colectivo de aplicación. En el caso de personas con discapacidad trabajadoras, la empresa asegurará que esos medios, equipos y herramientas, incluidos los digitales, sean universalmente accesibles, para evitar cualquier exclusión por esta causa.  
>2. Asimismo, se garantizará la atención precisa en el caso de dificultades técnicas, especialmente en el caso de teletrabajo.  

**24 septiembre 2021**  
[ACUERDO POR EL QUE SE REGULA LA MODALIDAD DE TELETRABAJO EN EL ÁMBITO DEL PERSONAL DOCENTE DE ENSEÑANZA PÚBLICA NO UNIVERSITARIA DE LA COMUNIDAD DE MADRID](https://www.comunidad.madrid/sites/default/files/doc/educacion/rh20/rh20_acuerdo_teletrabajo_personal_docente_sin_firma.pdf)

### Referencias / normativa / jurisprudencia

Las hay a dos niveles: laboral y de protección de datos. Algunas las cito en reclamación AEPD  

**6 febrero 2019**  
La sentencia (citada por artículos y referencias posteriores) aquí enlace en CENDOJ [CENDOJ Roj: SAN 136/2019 - ECLI:ES:AN:2019:136](https://www.poderjudicial.es/search/AN/openDocument/8ed60e51766c4e3e/20190219)  cita Estatuto de los Trabajadores, en el que **los empleados públicos quedan excluidos por 1.3.a** pero la idea general es aplicable (respuesta DPD julio 2020 citaba ET y EBEP)  

> 1º: - Consideramos que la exigencia de la aportación de un teléfono móvil con conexión de datos para
desarrollar el trabajo en los términos efectuados supone un manifiesto abuso de derecho empresarial, ya que
además de quebrar con la necesaria ajenidad en los medios que caracteriza la nota de ajenidad del contrato
de trabajo ( art. 1.1 ET ) y desplazando el deber empresarial de proporcionar ocupación efectiva del trabajador
( arts. 4.2 a ) y 30 E.T ) a éste al que se responsabiliza de los medios, de forma que cualquier impedimento
en la activación del sistema de geolocalización implica cuando menos la suspensión del contrato de trabajo
y la consiguiente pérdida del salario- ex art. 45.2 E.T -; y, por otro lado, la compensación que se oferta por tal
aportación resulta de todo punto insuficiente, ya que se calcula el valor de un terminal móvil de baja gama (se
ha calculado un precio de 110 euros, y sobre una vida útil de 3 años) y la contratación de unos datos por internet
que únicamente se compensan en función de su utilización en el trabajo, prescindiendo de si tal contratación
era o no deseada por el empleado para el desarrollo de su vida personal.  


En [Artículo 14 Real Decreto Legislativo 5/2015, de 30 de octubre, por el que se aprueba el texto refundido de la Ley del Estatuto Básico del Empleado Público.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11719#a14)  
> j bis) A la intimidad en el uso de dispositivos digitales puestos a su disposición y frente al uso de dispositivos de videovigilancia y geolocalización, así como a la desconexión digital en los términos establecidos en la legislación vigente en materia de protección de datos personales y garantía de los derechos digitales  

**21 febrero 2019**  
[¿Puede obligarse a los trabajadores a aportar un móvil personal para realizar su trabajo? - elderecho.com](https://elderecho.com/puede-obligarse-los-trabajadores-aportar-movil-personal-realizar-trabajo)  
> La AN declara que imponer al trabajador la obligación de aportar un teléfono móvil con conexión de datos para desarrollar su trabajo supone un abuso de derecho empresarial, ya que quiebra con la ajenidad en los medios que caracteriza al contrato de trabajo. Por ello, declara abusivo el nuevo sistema de trabajo según el cual cuando los trabajadores realicen labores de reparto van a ser geolocalizados mediante una app descargada en su teléfono móvil personal.​  

**2 febrero 2021**  
[Uso del móvil personal para fichar ¿Es legal? - inadvisors.es](https://inadvisors.es/movil-personal-fichar-jornada/)
> La sentencia de la Audiencia Nacional Sala de lo Social, sec. 1ª, S 06-02-2019, nº 13/2019, rec. 318/2018 concluye la ILEGALIDAD de tal acción por parte de la empresa.  
> En dicha sentencia se analiza la medida impulsada por la empresa TELEPIZZA, que a través de una aplicación que formaba parte del proyecto “Trucker”, obligaba al repartidor a descargarse en cualquier dispositivo de telefonía móvil con memoria suficiente y de su propiedad, la aplicación que permitía la geolocalización en todo momento.  
> ...
> La sentencia de la Audiencia Nacional concluye que la aportación de un teléfono móvil con conexión de datos para desarrollar el trabajo en los términos efectuados supone un manifiesto abuso de derecho empresarial, ya que quiebra con la necesaria ajenidad en los medios que caracteriza la nota de ajenidad del contrato de trabajo ( art. 1.1 ET (EDL 2015/182832) ) y desplazando el deber empresarial de proporcionar ocupación efectiva del trabajador ( arts. 4.2 a ) y 30 E.T ) a éste al que se responsabiliza de los medios necesarios para prestarlo.  

[Artículo 1. Ámbito de aplicación.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11430#a1)
> 1. Esta ley será de aplicación a los trabajadores que voluntariamente presten sus servicios retribuidos por cuenta ajena y dentro del ámbito de organización y dirección de otra persona, física o jurídica, denominada empleador o empresario  

[Artículo 4. Derechos laborales.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11430#a4)  
> 2. En la relación de trabajo, los trabajadores tienen derecho:  
> a) A la ocupación efectiva.  

[Artículo 30. Imposibilidad de la prestación.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-11430#a30)  
> Si el trabajador no pudiera prestar sus servicios una vez vigente el contrato porque el empresario se retrasare en darle trabajo por impedimentos imputables al mismo y no al trabajador...  

**15 febrero 2022**  
[Mi empresa me obliga a utilizar mi móvil personal en el trabajo, ¿me puedo negar? - pymesyautonomos.com](https://www.pymesyautonomos.com/legalidad/mi-empresa-me-obliga-a-utilizar-mi-movil-personal-trabajo-me-puedo-negar)  
> Este caso vendría a ser similar, el uso de nuestro teléfono personal para registrar la jornada, para tener una aplicación de gestión empresarial, para cualquier otra cuestión, debe ser compensada adecuadamente. Además, el consentimiento del trabajador debe ser libre, informado y expreso.  

**19 agosto 2022**  
[¿Puede una empresa obligar al empleado a utilizar su móvil personal para el trabajo?](https://sevilla.abc.es/economia/puede-empresa-obligar-empleado-utilizar-movil-personal-20220819104533-nts.html)  
> Ahora bien, para saber si es posible esta práctica o no, la ley no recoge nada explícitamente al respecto. Por el contrario, sí que existen algunas sentencias judiciales que abordan el tema y zanjan el asunto de forma clara.  


**2 agosto 2023**  
[La ley que obliga a todas las empresas a dar un móvil a sus empleados  - vozpouli](https://www.vozpopuli.com/espana/ley-obliga-empresas-movil-empleados.html)  
> Aunque sí es cierto que no hay ninguna ley que recoja explícitamente estos asuntos, sí hay sentencias judiciales que los aclaren. Primeramente, una empresa no puede obligar a sus trabajadores a que sean ellos mismo quienes aporten los materiales necesarios para la jornada, especialmente si tuviesen que comprarlo. Esto supondría un abuso del derecho empresarial, chocando con lo establecido en los contratos de trabajo.

Puesto esto, la empresa debería poner a disposición todos los medios disponibles a sus empleados para que pudiesen desempeñar sin problema sus obligaciones laborales. Consecuentemente, si un trabajador no quiere utilizar su teléfono personal para cuestiones laborales está en su derecho y la empresa no podría montar ninguna represalia contra él.

Esto significa que la empresa debe tener disponible un teléfono móvil o un ordenador, si es necesario, para sus empleados. En supuesto de llegar a un acuerdo con el empresario podría usarse el teléfono personal, pero habría que tener en cuenta un par de factores. Debe respetarse el derecho a la intimidad. Esto se traduce en la prohibición de las geolocalizaciones, como se vio en la sentencia del caso Telepizza donde querían geolocalizar a sus trabajadores, y el respeto a la desconexión. Fuera de la jornada laboral no es de carácter obligatorio responder a un mensaje, una llamada o un correo electrónico de motivo de trabajo.

