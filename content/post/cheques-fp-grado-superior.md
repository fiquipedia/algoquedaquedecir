+++
author = "Enrique García"
title = "Cheques FP Grado Superior Madrid"
date = "2022-06-25"
tags = [
    "educación", "Madrid", "Privatización", "post migrado desde blogger", "FP"
]
toc = true

+++

Revisado 9 diciembre 2024

## Resumen

En febrero 2022 desde Madrid se anuncia incremento en "becas" para FP Grado Superior. Intento recopilar información y mostrar que realmente son una manera de privatizar dando más dinero a centros privados, dinero que no solo se da vía conciertos, y que ese incremento es una consecuencia de pactar con la ultraderecha.

En mayo 2022 veo una ilegalidad común en las "becas" de Bachillerato, FP Grado Superior, FP Grado Medio y Educación Infantil: bases no fijan límite de renta y remiten a la convocatoria, lo que incumple normativa básica.

En junio 2022 hago una tabla resumen que incluye "becas" FP Grado Superior [ResumenBecasMadridSoloPrivados.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/ResumenBecasMadridSoloPrivados.pdf)   

**Posts relacionados:**

* [Becas y ayudas educación Madrid: manipulación y conciertos](https://algoquedaquedecir.blogspot.com/2018/11/becas-y-ayudas-educacion-madrid.html) (se ve como gran parte de dinero público va a centros privados pero se esconde esa privatización bajo el nombre de "becas y ayudas") 
* [Gratuidad de la educación](https://algoquedaquedecir.blogspot.com/2021/10/gratuidad-de-la-educacion.html)
* ["Becas" FP Grado Medio Madrid](https://algoquedaquedecir.blogspot.com/2022/02/becas-fp-grado-medio-madrid.html)
* ["Becas" Educación Infantil Madrid](https://algoquedaquedecir.blogspot.com/2022/06/becas-educacion-infantil-madrid.html)
* [No son becas, copón. Son cheques SOLO para privados](https://algoquedaquedecir.blogspot.com/2022/08/no-son-becas-copon-son-cheques-solo-para-privados.html)
* [Datos educación por titularidad](https://algoquedaquedecir.blogspot.com/2020/11/datos-educacion-por-titularidad.html) (se ve evolución de alumnado en FP por titularidad, y el desmantelamiento de la educación pública / potenciamiento de la educación privada)
* [Privatización de la educación: FP](https://algoquedaquedecir.blogspot.com/2021/12/privatizacion-de-la-educacion-fp.html)
* [Cheque Bachillerato Madrid](https://algoquedaquedecir.blogspot.com/2018/10/cheque-bachillerato-madrid.html)
* [Becas para el estudio de Programas de Segunda Oportunidad Madrid](https://algoquedaquedecir.blogspot.com/2021/02/becas-para-el-estudio-de-programas-de-segunda-oportunidad.html) (se ve como lo que según nombre venden como becas realmente da, sin decimales, 100% dinero a privados, con o sin concierto)
* [Privados con concierto en etapas no obligatorias: lo singular como normal](https://algoquedaquedecir.blogspot.com/2017/08/privados-con-concierto-en-etapas-no.html)
* Pendiente post sobre cheque escolar

## Detalles

En este post centralizo ideas de financiación de FP Grado Superior privada que tenía en otros posts, no solo "becas" / cheques.

Se trata de educación no obligatoria, por lo que enlaza con post [Privados con concierto en etapas no obligatorias: lo singular como normal](https://algoquedaquedecir.blogspot.com/2017/08/privados-con-concierto-en-etapas-no.html)

Información general en:

[Becas FP Grado Superior - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/becas-fp-grado-superior)  

### Evolución de FP Grado Superior por titularidad

La evolución de estas "becas y ayudas" que enlazan con la privatización

Ver [Datos educación por titularidad](https://algoquedaquedecir.blogspot.com/2020/11/datos-educacion-por-titularidad.html)

[Privatización de la educación: FP](https://algoquedaquedecir.blogspot.com/2021/12/privatizacion-de-la-educacion-fp.html)

https://twitter.com/FiQuiPedia/status/1332760322315345922

![](https://pbs.twimg.com/media/En7qOjRW8AA7ftY?format=png)  

Cito también la educación de FPGS privada a distancia

(indica % alumnos en la pública: lo que no es pública es privada)

[Educabase - ENSEÑANZAS NO UNIVERSITARIAS / ALUMNADO MATRICULADO. 6. Porcentaje de alumnado de Ciclos de FP matriculado en centros públicos por grado, comunidad autónoma y periodo](http://estadisticas.mecd.gob.es/EducaJaxiPx/Tabla.htm?path=/no-universitaria/alumnado/matriculado/series/gen-porcen-gen//l0/&file=general_06.px&type=pcaxis&L=0)  

![](https://pbs.twimg.com/media/FWFrwtsWIAAzHd0?format=jpg)  

### Información cronológica

**17 mayo 2013**

[La Consejería de Educación de la Comunidad de Madrid prepara el "cheque FP" - magisnet](https://www.magisnet.com/2013/05/la-consejera-de-educacin-de-la-comunidad-de-madrid-prepara-el-cheque-fp/)  
Según ha podido confirmar Magisnet por fuentes sindicales, la Consejería de Educación de Madrid transformará el modelo de conciertos de la FP de Grado Superior en un sistema de becas a los alumnos. El plan, que será presentado públicamente en los próximo días, podría incluir, además, algún tipo de financiación vía convenio a los centros afectados por la retirada del concierto.

**2 septiembre 2013**

[ORDEN 2379/2013, de 22 de julio, de la Consejería de Educación, Juventud y Deporte por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid y se aprueba la convocatoria correspondiente al curso 2013-2014.](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/09/02/BOCM-20130902-10.PDF)

**23 junio 2014**

[Orden 1822/2014, de 2 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Formación Profesional de grado superior en la Comunidad de Madrid y se aprueba la convocatoria abierta correspondiente al curso 2014-2015](https://www.bocm.es/boletin/CM_Orden_BOCM/2014/06/23/BOCM-20140623-14.PDF)

**31 mayo 2016**

[Cierran ciclos de FP en la pública mientras dan becas de 3.000 euros para estudiar en la privada - elplural](https://www.elplural.com/sociedad/cierran-ciclos-de-fp-en-la-publica-mientras-dan-becas-de-3-000-euros-para-estudiar-en-la-privada_81707102)

**1 julio 2016**

## Becas para FPGS (Curso 2016-2017)

[Becas para Formación Profesional de Grado Superior (Curso 2016-2017)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-formacion-profesional)

Fecha inicio: 01/07/2016   Fecha fin: 11/10/2016

**6 junio 2017**

## Becas para FPGS (Curso 2017-2018)

[Becas para Formación Profesional de Grado Superior (Curso 2017-2018)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-formacion-profesional-1)

Fecha inicio: 06/06/2017   Fecha fin: 07/07/2017

[Relación de renuncias aceptadas (05/02/2019)](https://sede.comunidad.madrid/medias/relacion-renuncias-aceptadas-05022019/download)

[Relación de nuevos beneficiarios (02/04/2018). Orden 814/2018 de la Consejería de Educación e Investigación por la que se determinan nuevos beneficiarios de becas de Formación Profesional de Grado Superior para el curso 2017/2018.](https://sede.comunidad.madrid/medias/relacion-nuevos-beneficiarios-02042018-0/download)

**31 marzo 2017**
[ORDEN 792/2017, de 15 de marzo, de la Consejería de Educación, Juventud y Deporte por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Formación Profesional de grado superior en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/03/31/BOCM-20170331-21.PDF)

> Artículo 3  
> Destinatarios de las ayudas  
> Serán destinatarios de las ayudas los alumnos que realicen algún curso de un ciclo formativo de Formación Profesional de Grado Superior, en la modalidad presencial, en **centros privados** autorizados por la Consejería ...  

[Versión "consolidada" de orden 792/2017 que incluye cambios posteriores](http://www.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=9708)

**21 septiembre 2017**

[twitter sanz_ismael/status/910822137866457091](https://twitter.com/sanz_ismael/status/910822137866457091)  
Comunidad de Madrid lanza por 1ª vez Becas FP de Grado Superior públicos y privados.Hasta 27 septiembre #DER17Madrid 

Enlace no operativo en 2021, pero imagen es sobre [Becas para el estudio de Programas de Segunda Oportunidad Madrid](https://algoquedaquedecir.blogspot.com/2021/02/becas-para-el-estudio-de-programas-de-segunda-oportunidad.html) 

[Becas para el estudio de Programas de Segunda Oportunidad - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/becas-estudio-programas-segunda-oportunidad)

**24 abril 2018**

[20 millones para facilitar el acceso a la Formación Profesional el próximo curso - comunidad.madrid](https://www.comunidad.madrid/noticias/2018/04/24/20-millones-facilitar-acceso-formacion-profesional-proximo-curso)  
Destinamos 20 millones para facilitar los estudios de Formación Profesional a alrededor de 10.000 alumnos de la región. El Consejo de Gobierno aprueba esta inversión para las becas de FP de Grado Superior en centros privados autorizados por la Comunidad. Este programa de ayudas directas a los alumnos, puesto en marcha de forma pionera, fomenta la libertad de elección de centro donde cursar los estudios de FP.

## Becas para FPGS (Curso 2018-2019)

[Becas para Formación Profesional de Grado Superior (Curso 2018-2019)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-formacion-profesional-2)

Fecha inicio 17/05/2018    Fecha fin: 06/07/2018

Consultado en febrero 2019, aparece en tramitación el curso 2018-19 sin listados, y en curso 2017-18 aparece cerrado pero con listados publicados en 2019!?

Comparto un tuit asociado a un centro relacionado, misma localidad y nombre que consiguió una cesión de 26000 m^2  de suelo público a 75 años, ver post [Madrid crea centros privados con concierto en diferido](https://algoquedaquedecir.blogspot.com/2018/02/madrid-crea-centros-privados-con-concierto-en-diferido.html)

[twitter CESJuanPabloII/status/1073143962028924930](https://twitter.com/CESJuanPabloII/status/1073143962028924930)  
Los alumnos del [@CESJuanPabloII](https://twitter.com/CESJuanPabloII) ya se están beneficiando de la Beca de la Comunidad de Madrid para centros privados

![](https://pbs.twimg.com/media/DuSSvPEWwAAomXK.jpg)

[twitter BabelRed21/status/1093927685116579842](https://twitter.com/BabelRed21/status/1093927685116579842)  
En [@BabelRed21](https://twitter.com/BabelRed21) nos gustaría saber cómo es posible que la innovación y el bilingüismo de este centro del que usted nos habla, termine con estos resultados:  
[twitter BabelRed21/status/1093931806208995333](https://twitter.com/BabelRed21/status/1093931806208995333)  
A ver si alguien es capaz de decir cómo se hace negocio cuando se coloca el cartel de bilingüismo y luego se consigue que la mayor parte del alumnado termine, con beca, en el ciclo superior de FP. Aquí van los datos de alumnado del centro del que usted nos habla.

![](https://pbs.twimg.com/media/Dy5tKz4W0AAIj9e.jpg)

**21 mayo 2019**  
[La Comunidad aprueba 28,5 millones en becas de FP y Segunda Oportunidad para 13.000 alumnos - comunidad.madrid](http://www.comunidad.madrid/notas-prensa/2019/05/20/comunidad-aprueba-285-millones-becas-fp-segunda-oportunidad-13000-alumnos)  
Un total de 10 millones irán destinados a ayudas para aquellos jóvenes que quieran retomar su formación tras abandonar los estudios
El Gobierno regional invertirá **18,5 millones de euros en financiar becas para el estudio de FP de Grado Superior en centros privados autorizados**  
La cuantía asciende hasta 3.200 euros para FP y con un máximo de 2.800 euros en Segunda Oportunidad

[Nota Prensa CG - Educación Becas 2ª oportunidad y FP superior](http://www.comunidad.madrid/file/160076/download?token=BoSQVuzo) (93.12 KB)

**6 junio 2019**  
[EXTRACTO de la Orden 1649/2019, de 24 de mayo, de la Consejería de Educación e Investigación por la que se aprueba la convocatoria de becas de Formación Profesional de Grado Superior correspondiente al curso 2019-2020.](http://www.bocm.es/boletin/CM_Orden_BOCM/2019/06/06/BOCM-20190606-14.PDF)  
> Beneficiarios  
> Serán destinatarios de las ayudas los alumnos que realicen algún curso de un ciclo formativo de Formación Profesional de Grado Superior, en la modalidad presencial, en **centros privados** autorizados por la Consejería competente en materia de Educación, ...

## Becas para FPGS (Curso 2020-2021)

[Becas para Formación Profesional de Grado Superior (Curso 2020-2021)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-formacion-profesional-3)

Plazo de presentación de solicitudes: Del 13 de agosto al 14 de septiembre de 2020.

**18 junio 2019**  
[https://twitter.com/EnJarras/status/1140924887961395201](https://twitter.com/EnJarras/status/1140924887961395201)  
\#FelizMartes a @Guadalupbragado y al inefable @sanz_ismael Mañana se abre el plazo para solicitar plaza en FP en Madrid. Tranquilos, que el hilo va a ser breve, pero es imprescindible difundirlo: Cada año estos dos peperos dejan en la p*** calle a miles de estudiantes que 
quieren hacer FP, especialmente en el sur (la gente de Pozuelo no suele querer hacer FP).Miles son entre 7 y 10 mil chavales.  
Entonces sale @sanz_ismael presumiendo de becas: tranquilos, el @ppmadrid  te beca para que hagas FP en la privada. Entonces los chicos van a informarse 
a los centros privados: unos 7000 euros al año. 14.000 euros los dos cursos. ¡Pero hay becas!, te dicen.Que ya sería grave si fuera cierto, porque no se entiende que haya que desviar ese dinero público a negocietes privados cuando desde la administración se empeñan tanto en que 
estudies FP pero luego @Guadalupbragado no crea plazas públicas y se dedica a promocionar los centros privados.Pero es que encima, las becas de @sanz_ismael son mentira. No cubren ni la mitad de esos 14.000 €(o 10.000 los ciclos poco experimentales).Y el colmo de los colmos es 
que esas becas solo se las pueden dar a quienes no las necesiten. Me explico.  
Primero tienes que estar ya matriculado en un centro privado, y comprometido -por tanto- a pagar unos 500 euros mensuales (o todo de golpe). Y pagarlos de hecho durante los 4 primeros meses. Y luego 
en enero, si te dan la beca, pues bueno, unos 2.600 euritos cubren una parte. Si no te la dan, sigues pagando esa burrada. O dejas los estudios. Tanto si tienes beca como si no,tienes que tener capacidad para pagar esas cantidades de locura.O sea: tienes que NO necesitar la beca

**19 noviembre 2019**  
[ORDEN 3365/2019, de 8 de noviembre, de la Consejería de Educación y Juventud, por la que se resuelve la convocatoria de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid para el curso 2019-2020](https://www.bocm.es/boletin/CM_Orden_BOCM/2019/11/19/BOCM-20191119-8.PDF) 

En la página [Becas de FP grado superior - comunidad.madrid](http://www.comunidad.madrid/servicios/educacion/becas-fp-grado-superior)  
Solo se permite consulta individual  
[Bienvenido a la Consulta Pública de la Consejería de Educación, Ciencia y Universidades. Introduzca los datos de acceso ... Código de acceso facilitado por la Comunidad de Madrid para su acceso a la consulta de becas y ayudas](https://gestiona2.comunidad.madrid/guay_pub/FormIdentificacion.icm)  

**15 mayo 2020**  
[ORDEN 947/2020, de 5 de mayo, de la Consejería de Educación y Juventud, por la que se aprueba el plan estratégico de subvenciones correspondiente a los cursos 2020-2021, 2021-2022 y 2022-2023, en materia de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2020/05/15/BOCM-20200515-6.PDF)

> VII. Indicadores que permitan verificar el grado de cumplimiento de los objetivos y resultados.  
Los indicadores serán de tipo cuantitativo y cualitativo:  
Indicadores cuantitativos:  
- Un mínimo de 9.000 alumnos matriculados en ciclos de Formación Profesional de Grado Superior, en centros privados, obtiene beca en la convocatoria  
Indicadores cualitativos:  
— El 50 por 100 de los centros privados actualmente autorizados para Formación Profesional inician un programa de transformación de sus enseñanzas de Grado Superior hacia el modelo dual.

**10 julio 2020**  
[ORDEN 1401/2020, de 1 de julio, de la Consejería de Educación y Juventud, por la que se modifican determinados requisitos para la concesión de becas, así como el régimen de compatibilidad establecidos en la Orden 792/2017, de 15 de marzo, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Formación Profesional de grado superior en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2020/07/10/BOCM-20200710-21.PDF) 

Se cambia artículo 19.1 haciendo que no sea compatible con becas de segunda oportunidad.

**29 julio 2020**  
[Fomentamos la libertad de elección en FP con becas de hasta 3.200 euros para 9.000 alumnos - comunidad.madrid](https://www.comunidad.madrid/noticias/2020/07/29/fomentamos-libertad-eleccion-fp-becas-3200-euros-9000-alumnos)  
La Comunidad de Madrid destinará 18,5 millones de euros para facilitar los estudios de Formación Profesional Superior, una medida de la que se beneficiarán alrededor de 9.000 alumnos de la región. El Consejo de Gobierno ha aprobado hoy esta inversión para financiar las becas para estos estudios en centros privados autorizados por la Comunidad de Madrid. Se trata de un programa de ayudas directas que fomenta la libertad de elección a la hora de escoger el centro educativo en el que los madrileños quieren cursar sus estudios.

[ACUERDOS CONSEJO DE GOBIERNO 29 de julio de 2020](https://www.comunidad.madrid/sites/default/files/200729_cg.pdf)  
> • Acuerdo por el que se autoriza un gasto plurianual por importe de 18.500.000 de euros, destinado a financiar la convocatoria de concesión de becas para el estudio de Formación Profesional de grado superior en el curso 2020-2021.

**10 diciembre 2020**  
[ORDEN 3166/2020, de 4 de diciembre, del Consejero de Educación y Juventud,por la que se resuelve la convocatoria de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid para el curso 2020-2021](https://www.bocdm.es/boletin/CM_Orden_BOCM/2020/12/10/BOCM-20201210-13.PDF)  

Los listados de beneficiarios y los de excluidos se hacen públicos de forma simultánea a esta Orden en la página web institucional de la Comunidad de Madrid www.comunidad.madrid. Complementariamente serán expuestos en las Direcciones de Área Territorial,así como en la Oficina de Información de la Consejería de Educación y Juventud  

No hay listados, solo se permite consulta individual

**26 mayo 2021**  
[Destinamos 18,5 millones de euros para las becas de Formación Profesional - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/05/26/destinamos-185-millones-euros-becas-formacion-profesional)  
La Comunidad de Madrid destinará 18,5 millones de euros para facilitar los estudios de Formación Profesional Superior, una medida de la que se beneficiarán alrededor de 9.000 estudiantes de la región. El Consejo de Gobierno en funciones ha aprobado hoy esta inversión para financiar las becas de estos estudios en centros privados autorizados por la Comunidad de Madrid. Con esta medida, el Gobierno regional quiere fomentar la libertad de elección a la hora de decidir el centro educativo en el que los alumnos quieren cursar sus estudios.

**4 junio 2021**  
[Extracto de la Orden 1485/2021, de 27 de mayo, del Consejero de Educación y Juventud, por la que se aprueba la convocatoria de becas de Formación Profesional de grado superior correspondiente al curso 2021-2022](https://bocm.es/boletin/CM_Orden_BOCM/2021/06/04/BOCM-20210604-7.PDF)  

El importe total destinado a esta convocatoria es de 18.500.000 euros. 


## Becas para FPGS (Curso 2021-2022)

[Becas para Formación Profesional de Grado Superior (Curso 2021-2022)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-formacion-profesional-4)


Fecha de inicio: 07/06/2021 Fecha de fin: 30/06/2021 

**2 octubre 2021**  
[El negocio redondo de las FP: así se llena la educación profesional de empresas privadas](https://www.elconfidencial.com/amp/espana/2021-10-02/pelotazo-formacion-profesional-fp-fondos_3298273/)  
Los cambios en los criterios para montar un centro de formación profesional y las campañas de promoción institucional atraen a las empresas ante la falta de plazas públicas

La Comunidad de Madrid abandonó hace una década este sistema para cambiarlo por **becas** , para las que anunció una nueva partida en mayo de 18,5 millones. En el mismo tiempo, 10 años, ha perdido el 22% de sus plazas públicas.

**22 diciembre 2021**  
[RESOLUCIÓN DEL DIRECTOR GENERAL DE EDUCACIÓN CONCERTADA, BECAS Y AYUDAS AL ESTUDIO PARA LA GESTIÓN DEL ABONO DE LAS BECAS PARA EL
ESTUDIO DE FORMACIÓN PROFESIONAL DE GRADO SUPERIOR PARA EL CURSO 2021-2022](https://www.comunidad.madrid/sites/default/files/doc/educacion/vfresolucionabonotarjetasfirmado.pdf)  
CSV 1295326790616377336443  

Se cita la web [Gestión de los Servicios de Becas de la Comunidad de Madrid](https://www.becascm.com/)  

**15 febrero 2022**  
[twitter educacmadrid/status/1493546273269559297](https://twitter.com/educacmadrid/status/1493546273269559297)  
Nuevas ayudas para alumnos que cursan ciclos de FP de Grado Medio en la @ComunidadMadrid.  
Además aumenta:  
29% el presupuesto para becas.  
25% las cuantías de los cheques de Bachillerato, FP de Grado Superior.  
33% las de Educación Infantil.  

Y enlaza esta noticia

[La Comunidad de Madrid crea una beca para alumnos que cursan ciclos de FP de Grado Medio a partir del próximo curso escolar - comunidad.madrid](https://www.comunidad.madrid/noticias/2022/02/15/comunidad-madrid-crea-beca-alumnos-cursan-ciclos-fp-grado-medio-partir-proximo-curso-escolar)

De momento es un anuncio, habrá que ver la implementación real, así que de momento comento y pongo en contexto lo que se afirma en el tuit y artículo, cito literalmente dos párrafos

> "El consejero ha destacado el importante esfuerzo del Gobierno madrileño en lo que se refiere a becas educativas, cuyo presupuesto se va a incrementar de manera notable el próximo curso con un crecimiento de más de 18.000 beneficiarios, es decir, un 68% más. Esta actuación se plasmará en una subida del 33% en la cuantía y los beneficiarios de estas ayudas en Educación Infantil (0-3 años), y un 25% en las de Formación Profesional de Grado Medio, Superior y Bachillerato.  
> En definitiva, la dotación de este programa de becas para 2022/23 crecerá un total de 62,5 millones de euros, lo que supondrá un incremento de más del 29%, hasta alcanzar la cifra de 212 millones de euros. 
>  
> Crecimiento de las ayudas  
>  
> En concreto, las ayudas destinadas a Educación Infantil en centros privados subirán, de media, 378 euros al año, de 1.146 a 1.524 anuales, llegando en algunos casos a más de 580 euros. **En las de FP de Grado Superior, las cantidades anuales a percibir pasarán de 1.770 a 2.212, es decir 442 euros más**, mientras que en las de Bachillerato, llegarán a sumar 750 euros más al año, pasando de 3.000 a 3.750.  
> Con el objetivo de que estas subvenciones lleguen al mayor número posible de alumnos, el Gobierno madrileño también va a incrementar el límite de renta de las familias perceptoras, tomando como referencia el PIB per cápita de la Comunidad de Madrid en 2019, que fue de 35.913 euros. "  

Es un ejemplo de manipulación del uso del término "becas", me autocito el resumen del post [Becas y ayudas educación Madrid: manipulación y conciertos](https://algoquedaquedecir.blogspot.com/2018/11/becas-y-ayudas-educacion-madrid.html), donde también se pueden ver datos

**19 abril 2022**  
[ORDEN 789/2022, de 5 de abril, del Consejero de Educación, Universidades, Ciencia y Portavoz del Gobierno, por la que se modifica la Orden 947/2020, de 5 de mayo, de la Consejería de Educación y Juventud, por la que se aprueba el Plan Estratégico de Subvenciones correspondiente a los cursos 2020-2021, 2021-2022 y 2022-2023, en materia de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid.](https://bocm.es/boletin/CM_Orden_BOCM/2022/04/19/BOCM-20220419-42.PDF)

> II. Objetivos estratégicos  
> Se modifica el objetivo específico para el curso 2022-2023 que es la implantación de una financiación de la formación profesional de grado superior en centros privados mediante un sistema de becas al alumno que beneficie a las rentas más bajas.  
> III. Coste presupuestario previsible  
Para la financiación de estas becas se dispondrá, para el curso escolar 2022-2023, de un total de 30.583.016 euros, con la siguiente distribución:  
> — 15.291.508 euros, de septiembre 2022 a enero 2023.  
> — 15.291.508 euros, de febrero de 2023 a junio de 2023.  

**24 mayo 2022**

Se publica documentación

[Orden de la Consejería de Educación, Universidades, Ciencia y Portavocía por la que se modifican determinados requisitos de la Orden 792/2017, de 15 de marzo, que aprueba las bases reguladoras de concesión de becas de FP de grado superior en la CM](https://www.comunidad.madrid/transparencia/orden-consejeria-educacion-universidades-ciencia-y-portavocia-que-se-modifican-determinados)  

[ABOGACÍA GENERAL DE LA COMUNIDAD DE MADRID. S.J.- 370/2022 INFC. - 2022/777](https://www.comunidad.madrid/transparencia/sites/default/files/s.j.370_22sinforme_jco.pdf)  

> El apartado dos del artículo único del Proyecto modifica los apartados c) y d) del artículo 4, en el que se regulan los requisitos de los solicitantes, modificando los apartados c) y d) e incorporando los apartados e) y f), que quedan redactados en los siguientes términos:
“c) No superar la edad que se determine en la orden de convocatoria.  
d) No superar el límite de renta per cápita familiar que se determine en la orden de convocatoria.  
e) Ser residente en la Comunidad de Madrid, tanto para la modalidad presencial como a distancia.  
Para la modalidad a distancia será necesario haber residido en la Comunidad, como mínimo, los cinco años anteriores a la solicitud de la beca.  
f) No ser alumno repetidor del curso para el que se solicite la beca”.  
Respecto a la modificación de los apartados c) y d), cabe señalar que la determinación de la edad, así como el límite de renta per cápita familiar debe fijarse en las bases, no en la convocatoria, de conformidad con lo dispuesto en el artículo 17.3 b) de la LGS, básico, 6.2.b) de la Ley 2/1995 y 2.1.c) del Decreto 222/1998. No cabe la remisión que hace el Proyecto. La convocatoria no es más que un acto administrativo de aplicación.  
**Esta consideración tiene carácter esencial.**  

**13 junio 2022**  
[ORDEN 1557/2022, de 6 de junio, de la Consejería de Educación, Universidades, Ciencia y Portavocía, por la que se modifican determinados requisitos establecidos en la Orden 792/2017, de 15 de marzo, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Formación Profesional de grado superior en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/06/13/BOCM-20220613-8.PDF)  

No fijan límite de renta en bases, ignorando consideración esencial de la abogacía.  

Se añade el concebido no nacido en el cómputo para renta.

Se cambia artículo 3, por lo que ya no aplica solo a FP presencial, ahora también es a distancia.

Se puede combinar con este texto de [RESOLUCIÓN DEL DIRECTOR GENERAL DE EDUCACIÓN CONCERTADA, BECAS Y AYUDAS AL ESTUDIO PARA LA GESTIÓN DEL ABONO DE LAS BECAS PARA EL ESTUDIO DE FORMACIÓN PROFESIONAL DE GRADO SUPERIOR PARA EL CURSO 2021-2022](https://www.comunidad.madrid/sites/default/files/doc/educacion/vfresolucionabonotarjetasfirmado.pdf)  

> 4.1.6. No deberá justificarse la matriculación y asistencia de los alumnos que
realicen Formación Profesional de Grado Superior en cualquier modalidad que
no sea presencial, aun cuando hayan resultado beneficiarios en la resolución
definitiva.

**29 junio 2022**  
[La Comunidad de Madrid da luz verde a las becas de FP Superior para el próximo curso con un 50% más de beneficiarios - comunidad.madrid](https://www.comunidad.madrid/noticias/2022/06/29/comunidad-madrid-da-luz-verde-becas-fp-superior-proximo-curso-50-beneficiarios)  
La Comunidad de Madrid incrementa un 25% las cuantías de las becas para estudiantes de Formación Profesional de Grado Superior, pasando de un máximo de 2.600 hasta 3.250 euros anuales el próximo curso 2022/23. Los beneficiarios crecerán un 51%, desde los 7.800 actuales a cerca de 12.000.   

**1 julio 2022**  
[El control de las becas a las que optan incluso familias que ganan más de 100.000 euros está en manos de una empresa privada - elpais](https://elpais.com/espana/madrid/2022-07-01/el-control-de-las-becas-a-las-que-optan-incluso-familias-que-ganan-mas-de-100000-euros-esta-en-manos-de-una-empresa-privada.html)  
> La Comunidad de Madrid no va a controlar con medios propios el sistema de becas para estudiar educación infantil, bachillerato y formación profesional (FP) en centros privados, en el que invertirá más dinero que nunca (127 millones), contará previsiblemente con un récord de solicitantes (se prevén 62.000 este curso y 79.500 el próximo) y por primera vez dará acceso a familias con rentas de más de 100.000 euros. ...

## Becas para el estudio de FPGS (2022-2023)

[Becas para el estudio de Formación Profesional de Grado Superior (2022-2023)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-form-prof-grado-superior-2022-2023) 

Fecha de inicio: 07/07/2022 Fecha de fin: 28/07/2022 

**16 agosto 2022**  
Recibo dictámenes de abogacía de bases años anteriores

[drive algoquedaquedecri ChequesFPGS](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/ChequesFPGS) 

**31 agosto 2022**  
Pido datos (es de varias cosas, lo pongo solo aquí)

---

Solicito copia o enlace las MAIN (Memoria del Análisis de Impacto Normativo) definitivas asociadas la regulación de las becas de Educación Infantil, Bachillerato, FP Grado Medio y FP Grado Superior desde el curso 2016 inclusive. 

Hay MAIN de curso 2022-2023 publicadas, no en todas se indica son las definitivas.

Educación Infantil:

https://www.comunidad.madrid/transparencia/orden-conjunta-consejeria-educacion-universidades-ciencia-y-portavocia-y-consejeria-familia-juventud

https://www.comunidad.madrid/transparencia/sites/default/files/01_main_borrador_2022_con_obs_informe_familia.pdf

Indica fecha 4 mayo 2022, posterior a informe abogacía tiene fecha 22 abril 2022

Bachillerato:

https://www.comunidad.madrid/transparencia/orden-consejeria-educacion-universidades-ciencia-y-portavocia-que-se-aprueban-bases-reguladoras

https://www.comunidad.madrid/transparencia/sites/default/files/main_propuesta_bachill_sin_firma.pdf 

Indica fecha 19 abril 2022 e informe abogacía tiene fecha 22 abril 2022, posterior, con consideraciones esenciales, luego debe existir una MAIN posterior.

FPGM

https://www.comunidad.madrid/transparencia/orden-consejeria-educacion-universidades-ciencia-y-portavocia-que-se-aprueban-bases-reguladoras-0

https://www.comunidad.madrid/transparencia/sites/default/files/mainbbrrbecasfpgmedio_30-5-22_sin_firma_.pdf

Indica publicada 14 junio 2022, posterior a informe abogacía tiene fecha 31 mayo 2022  

FPGS:

https://www.comunidad.madrid/transparencia/orden-consejeria-educacion-universidades-ciencia-y-portavocia-que-se-modifican-determinados

https://www.comunidad.madrid/transparencia/sites/default/files/1.-_main_fp_grado_superior_10feb22.pdf

Indica publicada 24 mayo 2022 e informe abogacía tiene misma fecha, con consideraciones esenciales, luego puede existir una MAIN posterior.

--- 

49/785017.9/22

Planteo pedir datos becas (son varias, lo pongo solo aquí)

Me baso en petición desglose Bachillerato realizada por última vez para 2020-21

--- 

1. Copia o enlace a datos en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) de Becas Bachillerato concedidas en cursos 2021-2022 y 2022-2023 desglosado según códigos de centros destinatarios, con número de becas e importe por beca.

Se facilitaron datos de importe por centro para curso 2019-2020 en 09-OPEN-00217.4/2019, y para curso 2020-2021 en 09-OPEN-00009.2/2021.

2. Copia o enlace a datos en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) de Becas FP Grado Medio concedidas en curso 2022-2023 desglosado según códigos de centros destinatarios, con número de becas e importe por beca, código de ciclo cursado e indicación de modalidad presencial o a distancia.

3. Copia o enlace a datos en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) de Becas FP Grado Superior concedidas en curso 2021-2022 y 2022-2023 desglosado según códigos de centros destinatarios, con número de becas e importe por beca, código de ciclo cursado e indicación de modalidad presencial o a distancia.

4. Copia o enlace a datos en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) de Becas Bachillerato, FP Grado Medio y FP Grado Superior concedidas en curso 2022-2023 desglosando código de centro, importe de beca y renta per cápita familiar (o bien renta familiar y número de miembros). Se piden estos datos, anonimizados y desligados de los anteriores), para poder realizar un análisis de distribución de renta de los destinatarios. 

Como justificación de esta petición:

4.1 La propia Comunidad ha usado esos datos para un análisis, limitado a Bachillerato sin dar datos de FP.

https://www.comunidad.madrid/noticias/2022/07/20/comunidad-madrid-concede-942-becas-bachillerato-familias-rentas-debajo-20000-euros

Esos datos son inconsistentes:

63,1+31,1+4,9+0,4=99,5%, falta 0,5%

Se dan datos de porcentajes con un decimal y el error (0,5%) es superior a uno de los valores dados (0,4%)

Días después facilita datos distintos

https://twitter.com/ComunidadMadrid/status/1551878497005600769

El 92,32% de las familias que ha obtenido una beca de Bachillerato tiene una renta per cápita inferior a 20.000 euros.

4.2 La propia normativa asocia cuantías distintas según la renta, luego esos datos se tienen asociados a cada alumno becado.

Orden 1533/2022 Bachillerato, Undécimo.Cuantía de la beca.

Orden 1905/2022 FPGM, Apartado 10. Cuantía de la beca.

Orden 1906/2022 FPGS, Apartado 10. Cuantía de la beca.

5. Puntuación de corte para obtener la beca en curso 2022-23 según artículo 15.1 de orden ORDEN 1142/2022 (Bachillerato),  artículo 16.1 ORDEN 1559/2022 (FPGM), artículo 16.1 ORDEN 792/2017 (FPGS).

---

49/785139.9/22 

**12 septiembre 2022**  
Madrid inadmite indicando que está en curso de elaboración  
[2022-09-12 Resolucion Inadmisión Desglose Cheques.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/2022-09-12-ResolucionInadmisionDesgloseCheques.pdf)  

Ya habían dado datos en julio, y mismo día se usa dato de porcentaje en el debate del estado de la región.

[Ayuso afirma que el 63% de sus becas las recibieron rentas de hasta 10.000 euros - magisnet](https://www.magisnet.com/2022/09/ayuso-afirma-que-el-63-de-sus-becas-las-recibieron-rentas-de-hasta-10-000-euros/)  

**5 diciembre 2022**  
Tras reclamar a  CTPCM el desglose de Bachillerato, y viendo que si pido desglose sería similar, planteo solicitud intermedia para FPGM y Educación Infantil  

---

Solicito copia o enlace a la publicación y fecha de publicación mencionada en resolución de inadmisión de 09-OPEN-00143.4/2022 en la que se indicó "En el momento que esté disponible la información de las convocatorias, se procederá a hacer la publicación correspondiente."

La resolución definitiva de varias de las becas de las que se solicitaba información se ha publicado, así como instrucciones para la gestión del abono

14 noviembre Bachillerato https://www.bocm.es/boletin/CM_Orden_BOCM/2022/11/14/BOCM-20221114-23.PDF

https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_tarjetas_bachillerato_22-23.pdf

30 noviembre FPGM https://www.bocm.es/boletin/CM_Orden_BOCM/2022/11/30/BOCM-20221130-14.PDF

https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_tarjetas_fpgm_22-23.pdf 

2 diciembre Educación Infantil https://www.bocm.es/boletin/CM_Orden_BOCM/2022/12/02/BOCM-20221202-20.PDF 

https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_tarjetas_bepi_22_23_f_1.pdf 

--- 

59/519689.9/22

**19 diciembre 2022**  
Recibo resolución inadmitiendo
[twitter FiQuiPedia/status/1604782019346845696](https://twitter.com/FiQuiPedia/status/1604782019346845696)  
Según @eossoriocrespo "El Gobierno de la @ComunidadMadrid siempre está a favor de la transparencia" eso sí, dentro de la opacidad de datos incómodos que solo dan si yendo a CTPCM les obligan: lo retrasa y hace que datos cheques SOLO para privados no estén antes de elecciones 2023  
[2022-12-19 Resolución Inadmisión Fechas Publicación.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/2022-12-19-ResolucionInadmisionFechasPublicacion.pdf)  

![](https://pbs.twimg.com/media/FkVUvdqXoAIRcXm?format=jpg)

**20 diciembre 2022**  
Se publica la resolución FPGS
[ORDEN 3859/2022, de 16 de diciembre, del Vicepresidente, Consejero de Educación y Universidades, por la que se resuelve la convocatoria de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid para el curso 2022-2023.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/12/20/BOCM-20221220-32.PDF)

**21 diciembre 2022**  
[RESOLUCIÓN DEL DIRECTOR GENERAL DE EDUCACIÓN CONCERTADA, BECAS Y AYUDAS AL ESTUDIO, POR LA QUE SE DICTAN INSTRUCCIONES PARA LA GESTIÓN DEL ABONO DE LAS BECAS PARA EL ESTUDIO DE FP DE GRADO SUPERIOR, EN EL CURSO 2022-2023](https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_firmada_tarjetas.pdf)  
CSV 0890213663204464490027

**27 diciembre 2022**  
Tras publicarse las de Segunda Oportunidad, pongo nueva solicitud

---

Solicito copia o enlace a la publicación y fecha de publicación mencionada en resolución de inadmisión de 09-OPEN-00143.4/2022 en la que se indicó "En el momento que esté disponible la información de las convocatorias, se procederá a hacer la publicación correspondiente." y en resolución de inadmisión 09-OPEN-00224.4/2022 en la que se indicó "aunque se han publicando las resoluciones definitivas de algunas convocatorias, aún queda por publicar la resolución de las becas de Formación Profesional de Grado Superior y la de Segunda Oportunidad y por lo tanto, no ha finalizado el proceso y tramitación de todas las convocatorias.", a pesar de que en solicitudes anteriores no citaba Segunda Oportunidad.

La información que solicito (se me indicó que se procedería a su publicación pero solicitaba copia o enlace) es con el mismo desglose que en solicitud 09-OPEN-00143.4/2022.

La resolución definitiva de las becas de las que se solicitaba información se ha publicado, así como instrucciones para la gestión del abono

14 noviembre Bachillerato https://www.bocm.es/boletin/CM_Orden_BOCM/2022/11/14/BOCM-20221114-23.PDF

https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_tarjetas_bachillerato_22-23.pdf

30 noviembre FPGM https://www.bocm.es/boletin/CM_Orden_BOCM/2022/11/30/BOCM-20221130-14.PDF

https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_tarjetas_fpgm_22-23.pdf 

2 diciembre Educación Infantil https://www.bocm.es/boletin/CM_Orden_BOCM/2022/12/02/BOCM-20221202-20.PDF 

https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_tarjetas_bepi_22_23_f_1.pdf 

20 diciembre FP Grado Superior

https://www.bocm.es/boletin/CM_Orden_BOCM/2022/12/20/BOCM-20221220-32.PDF

https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_firmada_tarjetas.pdf

27 diciembre Segunda Oportunidad

https://www.bocm.es/boletin/CM_Orden_BOCM/2022/12/27/BOCM-20221227-19.PDF

---

59/675503.9/22

## Becas para el estudio de FPGS (2023-2024)

[Becas para el estudio de Formación Profesional de Grado Superior (2023-2024)](https://sede.comunidad.madrid/ayudas-becas-subvenciones/becas-fp-grado-superior-23-24)  

Fecha de inicio: 27/06/2023 Fecha de fin: 17/07/2023 

**14 junio 2023**  
[ACUERDOS CONSEJO DE GOBIERNO 14 de junio de 2023](https://www.comunidad.madrid/sites/default/files/230614_cg.pdf)  
> Acuerdo por el que se autoriza un gasto plurianual por importe de 30.583.016 euros, destinado a financiar la convocatoria de concesión de becas para el estudio de Formación Profesional de Grado Superior en el curso 2023-2024.

[La Comunidad de Madrid aprueba 30,5 millones en becas FP que beneficiarán a más de 12.000 alumnos el próximo curso - comunidad.madrid](https://www.comunidad.madrid/noticias/2023/06/14/comunidad-madrid-aprueba-305-millones-becas-fp-beneficiaran-12000-alumnos-proximo-curso)  
El Consejo de Gobierno en funciones ha aprobado hoy una inversión de 30,5 millones en becas de Formación Profesional destinadas a más de 12.000 alumnos de Grado Superior para el próximo curso escolar 2023/24. Las ayudas beneficiarán a los escolarizados en centros privados de la región de esta etapa educativa no obligatoria y se priorizará para la concesión a los estudiantes con menos recursos económicos.

**24 junio 2023**  
[Ayuso exige buenas notas para tener ayudas para estudiar FP en centros públicos, pese a que no lo ha hecho hasta ahora para las becas en privados - elpais](https://elpais.com/espana/madrid/2023-06-24/ayuso-exige-buenas-notas-para-tener-ayudas-para-estudiar-fp-en-centros-publicos-pese-a-que-no-lo-ha-hecho-hasta-ahora-para-las-becas-en-privados.html)  
El Gobierno prepara 20 millones para que estudiantes con buen currículo afronten la tasa de 400 euros que hay que pagar en los grados superiores del sistema público, pero no aclara si pondrá el mismo requisito para las subvenciones del privado

**26 junio 2023**  
[EXTRACTO de la Orden 2185/2023, de 15 de junio, del Vicepresidente, Consejero de Educación y Universidades, por la que se aprueba la convocatoria de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid correspondiente al curso 2023-2024](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/06/26/BOCM-20230626-36.PDF)  

**18 diciembre 2023**  
Solicitud datos FPGS

Similar a lo realizado para Bachillerato 13 noviembre 2023 a su vez similar a 15 noviembre 2022 para Bachillerato.

---

1. Copia o enlace a datos en formato electrónico si es posible reutilizable CSV, hoja de cálculo de Becas FP Grado Superior concedidas en curso 2023-2024 desglosado según códigos de centros destinatarios, con número de becas e importe por beca, código de ciclo cursado e indicación de modalidad presencial o a distancia.

Para cursos 2021-2022 y 2022-2023 se estimó RDA164/2023, aunque la Comunidad no ha facilitado los datos asociados a FP Grado Superior que indicaba esa resolución.

2. Renta per cápita de corte para obtener la beca según artículo 16 de bases reguladoras ORDEN 792/2017

La resolución definitiva se ha publicado y ha finalizado el plazo de presentación de recursos

Orden 4367/2022, de 13 de noviembre de 2023, del Consejero de Educación, Ciencia y Universidades, por la que se resuelve la convocatoria de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid para el curso 2023-2024.

https://www.bocm.es/boletin/CM_Orden_BOCM/2023/11/15/BOCM-20231115-20.PDF 

---  

43/598630.9/23

**11 enero 2024**  
Recibo [resolución inadmisión a solicitud 18 diciembre 2023 09-OPEN-00204.6/2023](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/ChequesFPGS/2024-01-11-Resoluci%C3%B3nInadmisi%C3%B3nFPGS.pdf)  

Inadmitir la solicitud de acceso a la información solicitada ya que la información correspondiente a las convocatorias está siendo tratada y no estará disponible hasta la finalización de todos los procedimientos que conforman la misma, así como la obtención de los datos solicitados para la elaboración de los informes de las convocatorias 2023/2024.  
En el momento que esté disponible la información de las convocatorias y se disponga de los datos solicitados, se procederá a la elaboración de los informes (como en el curso 2023/2024 (sic, se asume que quieren decir 2022-2023)) y se procederá a hacer la publicación correspondiente de los mismos.  


**22 enero 2024**  
Me planteo reclamación a CTPM similar a la realizada para [Cheque Bachillerato Madrid](https://algoquedaquedecir.blogspot.com/2018/10/cheque-bachillerato-madrid.html) el 27 noviembre 2023, aunque en esa solicitud había 4 puntos y aquí solo 2.  

---

Inadmiten usando artículo 18.a Ley 19/2023 "información que esté en curso de elaboración o de publicación general." pero esta asociada a becas ya resueltas: ha finalizado plazo de alegaciones, el abono de esas becas ya se está gestionado con unos centros e importes por centro ya conocidos. No se indica en qué consiste "la finalización de todos los procedimientos" que suponga no poder facilitar ya la información solicitada, y esta el precedente de RDA164/2023.  
Se comenta que "se procederá a la elaboración de los informes (como en el curso 2023/2024 (sic, se asume que quieren decir 2022-2023)) y se procederá a hacer la publicación correspondiente de los mismos", pero la información publicada en 2023-2024 para FP Grado Superior no responde en absoluto en mi solicitud. 
Hacen referencia a https://www.comunidad.madrid/servicios/educacion/informe-beneficiarios-becas-formacion-profesional-grado-superior-2022-2023 donde se publicó el informe https://www.comunidad.madrid/sites/default/files/2023.08.03_informe_becas_fpgs.pdf pero en dicho informe de 287 páginas la información es:
- Motivos de inadmisión por solicitud anonimizada (páginas 2 a 109)  
- Renta per cápita por solicitud anonimizada (páginas 110 a 287)   
Por lo que se puede comprobar que la información que indican que publicarán no es la asociada a la solicitud inadmitida.  

---

Uso formularios de [Consejo de Transparencia y Participación](https://ctyp.asambleamadrid.es/derechos-del-ciudadano)  

**12 marzo 2024**

Recibo vía consejo [documento de alegaciones](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/ChequesFPGS/2024-03-01-7_OPEN-13_Alegacion.pdf)  

> De acuerdo con lo establecido en los artículos 43 y 44 del Reglamento de Organización y Funcionamiento de este Consejo, le damos traslado de la información recibida por parte de la administración en relación a la reclamación efectuada por usted cuyo número de expediente es RDACTPCM023/2024.  
En caso de que desee efectuar alegaciones a tenor de la información recibida, puede enviarlas a esta misma dirección de correo en el plazo máximo de 10 días a partir de la recepción del presente correo.

Respondo:

Reitero lo indicado en mi reclamación: la información solicitada está asociada a becas ya resueltas, ha finalizado plazo de alegaciones, el abono de esas becas ya se está gestionado con unos centros e importes por centro ya conocidos. No se indica en qué consiste "la finalización de todos los procesos de las convocatorias" que suponga no poder facilitar ya la información solicitada, y está el precedente de RDA164/2023. Si no existe una definición pública y con criterios objetivos y transparentes de "finalización de todos los procesos" la administración puede usar ese argumento indefinidamente.

Vuelven a indicar "como ya se hizo en el curso 2022/2023" por lo que no están contemplando ni lo que indicaba en mi solicitud ni lo que indicaba en mi reclamación, ya que ese informe https://www.comunidad.madrid/sites/default/files/2023.08.03_informe_becas_fpgs.pdf no contempla la información solicitada.

**7 octubre 2024**

Planteo nueva solicitud sin esperar a Consejo de Transparencia


---

1. Copia o enlace a datos en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) de Becas Bachillerato concedidas en cursos 2021-2022, 2022-2023 y 2023-2024 desglosado según códigos de centros destinatarios, con número de becas e importe por beca.

2. Copia o enlace a datos en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) de Becas FP Grado Superior y Becas FP Grado Medio concedidas en cursos 2021-2022, 2022-2023 y 2023-2024 desglosado según códigos de centros destinatarios, con número de becas e importe por beca, código de ciclo cursado e indicación de modalidad presencial o a distancia.

3. Copia o enlace a datos en formato electrónico si es posible reutilizable (CSV, hoja de cálculo) de Becas Bachillerato, Becas FP Grado Superior y Becas FP Gradio Medio concedidas en cursos 2021-2022, 2022-2023 y 2023-2024 desglosando importe de beca y renta per cápita familiar (o bien renta familiar y número de miembros). Se piden estos datos, anonimizados y desligados de los anteriores, para poder realizar un análisis de distribución de renta de los destinatarios. 

Se facilitaron datos de importe por centro para Bachillerato para curso 2019-2020 en 09-OPEN-00217.4/2019, y para curso 2020-2021 en 09-OPEN-00009.2/2021 pero a pesar de RDA164/2023 y 09-OPEN-00177.2/2023 todavía no se han facilitado los datos según lo solicitado en todos los casos, alegando que la información estaba en curso de elaboración y que se publicaría y por eso se realiza una nueva solicitud.

La información solicitada NO es equivalente a la publicada para 2022-2023 en https://www.comunidad.madrid/servicios/educacion/informe-beneficiarios-becas-bachillerato-2022-2023 ni en https://www.comunidad.madrid/servicios/educacion/informe-beneficiarios-becas-formacion-profesional-grado-superior-2022-2023 ni en https://www.comunidad.madrid/servicios/educacion/informe-beneficiarios-becas-formacion-profesional-grado-medio-2022-2023 ya que ahí solo se publican dos datos: número de solicitud y  renta per cápita y (en el caso de Bachillerato) un tercer campo que es si hay concierto.


---

43/358042.9/24

**18 octubre 2024**

09-OPEN-00187.1/2024

Recibo [resolución de inadmisión](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/2024-10-18-Resolucion_de_inadmision.pdf)  

**19 octubre 2024**

Reclamo a consejo de transparencia y protección de datos usando el [formulario web](https://sede.comunidad.madrid/denuncias-reclamaciones-recursos/consejo-transparencia-proteccion-datos)  

---

En la resolución inadmiten indicando "se encuentran en curso de elaboración los informes correspondientes a las becas solicitadas" cuando se solicitan para cursos 2021-2022, 2022-2023 y 2023-2024 y ellos mismos reconocen que tienen informes publicados con datos de 2022-2023 de los que facilitan enlaces, luego su argumentación no es consistente para los datos de 2021-2022, previos a lo que ya han elaborado, ni para 2023-2024, ya que los informes de 2022-2023 los publicaron en agosto de 2023 y de manera análoga los informes de 2023-2024 podrían estar elaborados desde agosto 2024.

En la resolución aportan tres enlaces de 2022-2023 que ya aportaba yo en la solicitud e indicaba explícitamente que la información solicitada NO es equivalente a esos enlaces de 2022-2023.

En la resolución indican "en el momento que estén disponibles los informes" y esa afirmación elude una respuesta de transparencia en plazo vinculándola a una decisión de la administración de disponibilidad de la información solicitada; bajo esa argumentación cualquier resolución de transparencia podría indicar que se facilitará la información en el momento indeterminado en el que la administración decida que está disponible la información, pero eso no es lo que indica la ley de transparencia. La administración no alega que deba elaborar información, de hecho indica que está en curso de elaboración, y no alega que se deba esperar "la finalización de todos los procesos de las convocatorias" como sí alegaron en marzo 2024 para parte de la información, adjunto documento.

Enlazando con las alegaciones de marzo 2024 aportadas, quiero recalcar que esta solicitud de acceso a información pública 09-OPEN-00187.1/2024 que reclamo ahora aglutina información que ha sido objeto de solicitud de acceso a información pública y de posterior reclamación ante el anterior Consejo de Transparencia y Participación de la Comunidad de Madrid, según el siguiente desglose:

Descripción ; Solicitud ; Envío a Consejo ; Acuse recibido ; Admisión trámite ; Reclamación
Datos becas Bachillerato 23-24 y ejecución RDA164/2023 / 09-OPEN-00177.2/2023 / 27/11/23 / 01/12/23 / 11/03/24; RDACTPCM347/2023
Datos becas FPGM 23-24 ; 09-OPEN-00195.4/2023 ; 23/12/23 ; 02/01/24 ; 13/03/24 ; RDACTPCM387/2023
Datos becas FPGS 23-24 ; 09-OPEN-00204.6/2023 ; 22/01/24 ; 24/01/24 ; 28/02/24 ; RDACTPCM023/2024

He realizado una nueva solicitud de acceso pasado un tiempo por varios motivos:  
- Mostrar que, sin que la administración inadmita alegando que deben finalizar procesos al no tener ya sentido, la administración sigue inadmitiendo sin facilitar la información pública con el detalle que se solicita
- Sabiendo qeReclamar directamente al Consejo de Transparencia y Protección de Datos, ya que considero que la gestión de la tramitación de reclamaciones que previamente llevaba el anterior Consejo de Transparencia y Participación de la Comunidad de Madrid no está siendo ágil y es más probable que se resuelva antes esta nueva reclamación.  

---

43/423099.9/24

**20 noviembre 2024**  
Recibo notificación TAUD RECLAMANTE 104/2024 CTPD en la que envían 3 documentos:  
- Remisionalegaciones104_2024_CTPD.pdf (fechada 14 noviembre 2024) con CSV 0907364200717272895588  
- TAUD+RECLAMANTE+104_2024+CTPD.pdf (fechada 20 noviembre 2024) con CSV 1221535032546205187675 que indica 
> De conformidad con lo establecido en el artículo 82 de la Ley 39/2015, de 1 de
octubre, del Procedimiento Administrativo Común de las Administraciones Públicas (LPAC),
se le da traslado de la citada documentación y se le concede un plazo máximo de **QUINCE DÍAS** para que formule las alegaciones que considere oportunas, que podrá presentar en
cualquiera de los Registros de la Comunidad de Madrid, o bien, en alguno de los lugares
previstos en el artículo 16.4 LPAC.  
- alegacionesSGBecasCTPD_104_2024.pdf (fechado 8 noviembre 2024)  

Las alegaciones son 4 páginas, las 2 primeras reproducen mi solicitud y luego realizan alegaciones que separo en párrafos y comento:

> - La modificación de las bases reguladoras de las becas de infantil, bachillerato,
FPGS y la creación de la beca de FPGM en el curso 22/23, supuso un número
de cambios importante y substancial en la aplicación informática de gestión que
impedía e impide hacer una explotación y tratamiento de datos anterior a ese
curso como la que se hizo con los informes que fueron elaborados (solicitando
reelaboración expresa) y publicados en la página de transparencia de la
Consejería para el curso 22/23.  

Esta afirmación no es cierta ya que con posterioridad al cambio normativo de 2022, en noviembre de 2023, la consejería SÍ facilitó los datos de las becas de curso 2023/2024, como se puede comprobar en:   

https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/ChequesBachillerato/2023-11-24-Resoluci%C3%B3n09-OPEN-00177.2-2023.pdf  

https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/ChequesBachillerato/2023-11-24-DatosSolicitadosCentros09-OPEN-00177.2-2023.pdf  

Adjunto correo de 27 noviembre 2023 enviado al anterior consejo en el que se muestra el incumplimiento de la resolución RDA164/2023 cuando el consejo tenía potestad sancionadora.  

> -Es preciso aclarar que con la afirmación de que se facilitará la información
solicitada “en el momento que estén disponibles los informes” no se elude una
respuesta de transparencia, sino que por el contrario la puesta a disposición de
la información por parte de la administración no queda a juicio de oportunidad de
la misma, sino que se facilitará, como se viene haciendo, una vez se haya podido
reelaborar la información solicitada que, como ha sido reiterado en otras
ocasiones, no está disponible su tratamiento a través de la aplicación de gestión
de manera inmediata en su totalidad. En este sentido, se debe aclarar que, en el
ejercicio de su actividad, esta Administración se rige, entre otros, por los
principios de eficiencia en la asignación y utilización de los recursos públicos, así
como el de garantía e igualdad en el ejercicio de los derechos de todos los
ciudadanos en sus relaciones con la Administración. Asimismo, vela porque la
publicación de la informaciones y servicios en su página web se realice de
manera que ésta sea accesible y de uso generalizado por los ciudadanos.

Reitero lo indicado en mi reclamación: bajo esa argumentación cualquier resolución de transparencia podría indicar que se facilitará la información en el momento indeterminado en el que la administración decida que está disponible la información, pero eso no es lo que indica la ley de transparencia.  
Además añado que la ley de transparencia contempla en artículo 20 de ley 19/2013 la posibilidad de ampliación de plazo de un mes a la que la administración no se ha acogido.  

> Además, se hace constar y “alega” la Administración, que toda la reelaboración
que persigue el recurrente y con el grado, claramente abusivo, de desagregación
propuesta, de obligarse a esta Administración a proporcionárselo de manera
inmediata, supondría la paralización de la gestión de las competencias
asignadas y a las que primordialmente está obligada a atender, lo que redundaría
en un perjuicio tal que supondría “paralizar la gestión de las becas que se están
tramitando de la convocatoria 24/25, que aún no está finalizada y de la que los
interesados están a la espera de la resolución definitiva de la misma en alguna
las becas o de los recursos que estén presentando y en la preparación de la del
curso 25/26, en la que ya se está trabajando para que los interesados puedan
disponer de la resolución de su solicitud lo antes posible”.

La afirmación "claramente abusivo" es un juicio de valor: sería un motivo de inadmisión según artículo 18.e de Ley 19/2013 pero no ha sido alegado en la resolución de inadmisión.  

La afirmación "proporcionárselo de manera inmediata" no es consistente cuando la solicitud comenta que se están solicitando datos asociados a otras solicitudes iniciadas hace ya tiempo.  

La afirmación "supondría paralizar la gestión de las becas que se están tramitando de la convocatoria 24/25", refleja que ya no se está trabajando en la gestión de las becas de las convocatorias anteriores y eso supone que sí están finalizadas, y es inconsistente con que la administración sí haya facilitado la información en casos previos como el comentado en noviembre 2023 sin paralizar por ello la gestión de becas 2023/24. 

También se facilitaron datos de importe por centro para curso 2019-2020 en 09-OPEN-00217.4/2019, y para curso 2020-2021 en 09-OPEN-00009.2/2021 y se estimó RDA164/2023.

> Reiteramos nuevamente que la mayoría de la información solicitada por el
interesado no puede ser extraída ni tratada directamente desde la aplicación de
gestión de becas y por este motivo requiere reelaboración. En estos momentos
se está procediendo de la misma forma que el curso anterior; se está
reelaborando y recopilando la información para poder hacer la publicación de
datos del curso 23/24, analizando la que puede ser facilitada por la aplicación de
gestión de becas y la que requiere reelaboración, con la finalidad de avanzar en
la transparencia informativa y el acceso a los datos. Por este motivo se resolvió
la solicitud de manera que, cuando se disponga de la información y los informes
estén finalizados, se procederá a la notificación de los mismos al interesado y a
su publicación en el Portal de Transparencia de manera inmediata. Esto no
significa de ninguna manera, insistimos, que se esté alargando de forma
negligente o desinteresada la reelaboración; se está trabajando en este proceso
para su publicación lo antes posible.

Reitero lo indicado en mi reclamación: bajo esa argumentación cualquier resolución de transparencia podría indicar que se facilitará la información en el momento indeterminado en el que la administración decida que está disponible la información, pero eso no es lo que indica la ley de transparencia. 

También quiero hacer notar que al tiempo que se indica que la información sobre becas no puede ser extraída, en julio de 2022, cuando no estaba el proceso cerrado de las becas 2022/23 y acababa de cambiar la normativa, la propia administración sí pudo extraer información para este artículo en la web de la Comunidad de Madrid en el que aparecían porcentajes con decimales

https://www.comunidad.madrid/noticias/2022/07/20/comunidad-madrid-concede-942-becas-bachillerato-familias-rentas-debajo-20000-euros

> En cuanto a la definición del concepto reelaboración, debemos tener en cuenta
la interpretación que del alcance de la noción de “reelaboración” elaboró el
Consejo de Transparencia y Buen Gobierno en su Criterio Interpretativo
007/2015, de 12 de noviembre. Para ese Consejo la “reelaboración” como causa
de inadmisión de solicitudes de acceso a la información debe entenderse desde
el punto de vista literal del concepto “reelaborar” que es, según define la Real
Academia de la lengua; “volver a elaborar algo”. Es esta circunstancia la que es
exigible para entender que estamos ante un supuesto de reelaboración. De modo
que, “si por reelaboración se aceptara la mera agregación, o suma de datos, o el
mínimo tratamiento de estos, el derecho de acceso a la información se convertirá
en derecho al dato o a la documentación, que no es lo que sanciona el artículo
12 al definir el derecho como “derecho a la información” (CI/007/2015, de 12 de
noviembre). En atención a esta premisa,  
>- “la causa de inadmisión puede entenderse aplicable cuando a la
información que se solicita, perteneciendo al ámbito funcional de
actuación del organismo o entidad que recibe la solicitud, deba: a)
Elaborarse expresamente para dar una respuesta, haciendo uso de
diversas fuentes de información, o b) cuando dicho organismo o entidad
carezca de los medios técnicos que sean necesarios para extraer y
explotar la información (supondría como decimos, la paralización de la
gestión de las becas para atender la petición)”  
En este caso, para justificar y entender la argumentación de la reelaboración,
nos encontramos en la aplicación de ambos supuestos.

El criterio citado https://www.consejodetransparencia.es/dam/jcr:a5c618b6-3a85-4ed8-a06e-b798a687c795/Criterio%25207_2015_Causas%2520inadmisi%25C3%25B3n%2520petici%25C3%25B3n%2520D%25C2%25BAinformaci%25C3%25B3n.pdf

realiza una aclaración en primer lugar  

>2. Reelaboración  
Como en anteriores dictámenes de fijación de criterios es necesario hacer algunas precisiones previas:  
• En primer lugar, es preciso señalar que el artículo 18 de la Ley 19/2013, establece
una serie de causas que permiten declarar la inadmisión de una solicitud de
información que, al tener como consecuencia inmediata la finalización del
procedimiento, habrán de operar, en todo caso, mediante resolución motivada.  
Por tanto, será requisito que la resolución por la que se inadmita la solicitud
especifique las causas que la motivan y la justificación, legal o material aplicables
al caso concreto.  

Según lo anterior la resolución reclamada https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/Privatizaci%C3%B3nEducaci%C3%B3n/BecasMadrid/2024-10-18-Resolucion_de_inadmision.pdf a solicitud 09-OPEN-00187.1/2024 debería citar reelaboración explícitamente artículo 18.e ley 19/2013 y reelaboración, lo que no ocurre.  
La resolución habla de que los informes están en curso de elaboración.  
Que la elaboración en curso indicada en la resolución pase a ser reelaboración en las alegaciones a la reclamación ante el consejo debido a que se incluya la información solicitada es algo distinto. La elaboración en curso supone ejecutar una consulta sobre una base de datos, y la elaboración asociada a la información solicitada, que se la administración considera reelaboración, supone ejecutar una consulta distinta.  

Pongo como ejemplo que considero relevante esta resolución del Ayuntamiento de Madrid

https://drive.google.com/file/d/1SHYVm7Fk-km2WOeeiYVwlvQm3aSnh447/view?usp=drive_link

https://docs.google.com/spreadsheets/d/1pOtRt9UqxG_w90MOUkjcsnPuLsYMOlgi/edit?usp=drive_link&ouid=113978260833777516740&rtpof=true&sd=true

Se puede ver como la administración facilita la información e incluso en la pestaña "SQL Statement" muestra que la "elaboración" es una consulta en una base de datos.  

----

Presentado 24 noviembre 43/602643.9/24 


## Becas para el estudio de Formación Profesional de Grado Superior (2024-2025) 

[RESOLUCIÓN de 3 de junio de 2024, de la Secretaria General Técnica de la Consejería de Educación, Ciencia y Universidades, por la que se dispone la publicación de la convocatoria del contrato denominado “Gestión del pago a los beneficiarios de becas para estudios no universitarios, dividido en cuatro lotes”, a adjudicar por procedimiento abierto con pluralidad de criterio](https://bocm.es/boletin/CM_Orden_BOCM/2024/06/18/BOCM-20240618-37.PDF)  

[ACUERDOS CONSEJO DE GOBIERNO 19 de junio de 2024](https://www.comunidad.madrid/sites/default/files/240619_cg.pdf)  
> Acuerdo por el que se autoriza un gasto plurianual para los años 2024 y
2025, por importe de 30.583.016 euros, destinado a financiar la
convocatoria de concesión de becas para el estudio de Formación
Profesional de grado superior en el curso 2024-2025.

Lote 3 Becas para el estudio de Formación Profesional de Grado Superior

Lote 3: 200.440,72 euros.


**5 diciembre 2024**

[Orden 5677/2024, de 3 de diciembre, de la Consejería de Educación, Ciencia y Universidades, por la que se declaran los expedientes excluidos en la relación con la Orden 5704/2024, del Consejero de Educación, Ciencia y Universidades, por la que se resuelve la convocatoria de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid para el curso 2024-2025 ](https://bocm.es/boletin/CM_Orden_BOCM/2024/12/05/BOCM-20241205-31.PDF)  

[Orden 5704/2024, de 3 de diciembre, del Consejero de Educación, Ciencia y Universidades, por la que se resuelve la convocatoria de becas para el estudio de Formación Profesional de Grado Superior en la Comunidad de Madrid, para el curso 2024-2025 ](https://bocm.es/boletin/CM_Orden_BOCM/2024/12/05/BOCM-20241205-32.PDF)  

## Bajada de precio en FPGS según resultados académicos

Parece limitado a públicos: en cheque solo para privados no hay requisitos académicos

**21 junio 2023**  

[Díaz Ayuso anuncia que la Comunidad de Madrid bajará los precios de FP Superior según los resultados académicos de los alumnos - comunidad.madrid](https://www.comunidad.madrid/noticias/2023/06/21/diaz-ayuso-anuncia-comunidad-madrid-bajara-precios-fp-superior-resultados-academicos-alumnos)  
La Comunidad de Madrid bajará los precios de los estudios de Formación Profesional de Grado Superior en función de los resultados académicos de los alumnos. Así lo ha anunciado hoy la presidenta en funciones, Isabel Díaz Ayuso, durante el Debate de Investidura que se desarrolla en la Asamblea legislativa regional. 

Con el objetivo de continuar avanzando en excelencia y en la gratuidad de las etapas no obligatorias, el Ejecutivo regional bonificará los **precios públicos** de la FP Superior cuya matrícula por curso completo es en la actualidad de 400 euros en **centros públicos.** Esta iniciativa se llevará cabo esta Legislatura en virtud de los resultados de los estudiantes y supondría un esfuerzo presupuestario de alrededor de 20 millones de euros para beneficiar hasta 50.000 alumnos.

## Reclamación en 2022 de la ilegalidad de las "becas" de FP Grado Superior

Se trata de poner recurso de reposición a la Orden convocatoria citando normativa que usa abogacía que obliga a que bases indiquen límite de renta y cuantía.

El tema de no fijar límite de renta en bases se ha cambiado deliberadamente en 2022, porque antes sí estaba.

Me baso en texto de recursos a "becas" Bachillerato

---

Órgano que resolvió: Consejería de Educación, Universidades, Ciencia y Portavocía  
Fecha de resolución: x junio 2022  
Fecha de notificación: x junio 2022 (publicación en BOCM)  
Acto recurrido: Orden convocatoria  
Materia o tipo de expediente: Educación (lo pide obligatorio al tramitar)  
Nº de referencia del expediente: Orden convocatoria (lo pide obligatorio al tramitar)  

---

La Orden convocatoria se basa (la cita en Primero.1.2) en ORDEN 1557/2022, de 6 de junio, de la Consejería de Educación, Universidades, Ciencia y Portavocía, por la que se modifican determinados requisitos establecidos en la Orden 792/2017, de 15 de marzo, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban las bases reguladoras para la concesión de becas para el estudio de Formación Profesional de grado superior en la Comunidad de Madrid,  que  incumple una ley autonómica y un decreto autonómico, todo normativa de rango superior.

- Orden 1557/2022 no fija los requisitos de límite superior de renta (artículo 17.3.b Ley 38/2003, artículo 6.2.b de la Ley 2/1995 y artículo 2.1.c del Decreto 222/1998): remitiéndose a la convocatoria (al modificar artículo 4.1.c de orden 792/2017 indica "“1.c) No superar el límite de renta per cápita familiar que se determine en la orden de convocatoria") y aunque lo fije la convocatoria (orden convocatoria la fija en apartado 2.c), no es legal según la normativa citada y según lo ha indicado la Abogacía de la Comunidad de Madrid.

- Orden 1557/2022 no fija la cuantía (artículo 2.1.g del Decreto 792/1998) remitiéndose a la convocatoria (al no modificar artículo 16 de orden 792/2017 que indica "Las cuantías de las becas se determinarán en cada orden de convocatoria"), y aunque lo fije la convocatoria (orden convocatoria en apartado 10) no es legal según la normativa ciada y según lo ha indicado la Abogacía de la Comunidad de Madrid.

Se cita informe de abogacía donde menciona consideración esencial

https://www.comunidad.madrid/transparencia/sites/default/files/s.j.370_22sinforme_jco.pdf

"Respecto a la modificación de los apartados c) y d), cabe señalar que la determinación de la edad, así como el límite de renta per cápita familiar debe fijarse en las bases, no en la convocatoria, de conformidad con lo dispuesto en el artículo 17.3 b) de la LGS, básico, 6.2.b) de la Ley 2/1995 y 2.1.c) del Decreto 222/1998. No cabe la remisión que hace el Proyecto. La convocatoria no es más que un acto administrativo de aplicación.
Esta consideración tiene carácter esencial."

Se puede ver en orden base 792/2017 publicada originalmente https://www.bocm.es/boletin/CM_Orden_BOCM/2017/03/31/BOCM-20170331-21.PDF que sí se cumple normativa fijando límite de renta en 4.1.e "No superar el límite de renta per cápita familiar de 20.000 euros." si bien no cumplía la determinación de cuantía de becas.

Por lo tanto la  Orden de convocatoria, al igual que orden 1557/2022, es nula de pleno derecho se acuerdo a artículo 47.2 Ley 39/2015 ya que "vulnera ...leyes u otras disposiciones administrativas de rango superior" y es anulable de acuerdo a artículo 48.1 Ley 39/2015 ya que "incurre en infracción del ordenamiento jurídico"

La orden de convocatoria considero que fue notificada el xxx mediante la publicación en BOCM
Extracto de la Orden convocatoria

Se pueden ver detalles de la argumentación legal común en recurso de alzada sobre orden 1142/2022 con registro 49/118552.9/22.

---

[Recurso potestativo de reposición contra actos de la Consejería de Educación, Ciencia y Universidades](https://tramita.comunidad.madrid/denuncias-reclamaciones-recursos/recurso-reposicion-educacion)  

Recurso puesto 25 junio 2022 con referencia registro 49/360529.9/22

