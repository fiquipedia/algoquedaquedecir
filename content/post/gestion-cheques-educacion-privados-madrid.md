+++
author = "Enrique García"
title = "Gestión cheques educación privados Madrid"
date = "2024-12-08"
tags = [
    "educación", "Madrid", "privatización"
]
toc = true

+++

Revisado 9 diciembre 2024

## Resumen

Además de gastar dinero público en cheques **SOLO PARA PRIVADOS** que en Madrid (que llaman becas), hay un gasto adicional, también en empresas privadas: la gestión de esos cheques. Intento centralizar información de manera adicional al post asociado a cada tipo de cheque, donde puede que haya algo de información asociada.  

Posts relacionados:

* [No son becas, copón. Son cheques SOLO para privados](https://algoquedaquedecir.blogspot.com/2022/08/no-son-becas-copon-son-cheques-solo-para-privados.html)  
* [Cheque Bachillerato Madrid](https://algoquedaquedecir.blogspot.com/2018/10/cheque-bachillerato-madrid.html)
* ["Becas" FP Grado Medio Madrid](https://algoquedaquedecir.blogspot.com/2022/02/becas-fp-grado-medio-madrid.html)
* ["Becas" FP Grado Superior Madrid](https://algoquedaquedecir.blogspot.com/2022/06/becas-fp-grado-superior-madrid.html)
* ["Becas" Educación Infantil Madrid](https://algoquedaquedecir.blogspot.com/2022/06/becas-educacion-infantil-madrid.html)
* [Becas para el estudio de Programas de Segunda Oportunidad Madrid](https://algoquedaquedecir.blogspot.com/2021/02/becas-para-el-estudio-de-programas-de-segunda-oportunidad.html)
* [Becas y ayudas educación Madrid: manipulación y conciertos](https://algoquedaquedecir.blogspot.com/2018/11/becas-y-ayudas-educacion-madrid.html)


## Detalle

La gestión tiene varias partes:  
* Gestión de las convocatorias (solicitudes, alegaciones, recursos, baremación)  
* Gestión del abono 

### Gestión de las convocatorias

En este caso parece que se licitan todos los cheques conjuntamente con lotes

**11 diciembre 2020**

[Servicios de colaboración en la gestión de las convocatorias de becas y ayudas para el curso 2021/2022, dividido en cinco lotes ](https://contratos-publicos.comunidad.madrid/contrato-publico/servicios-colaboracion-gestion-convocatorias-becas-ayudas-curso-20212022-dividido)  
Número de expediente A/SER-041769/2020 (C-323M-006-20)  
Presupuesto base licitación. Importe total 249.571,71 euros  
Duración del contrato 11 meses  

Varios adjudicatarios


Nombre o razón social del adjudicatario ADECUACIÓN DE ALTERNATIVAS S.L.  
Nombre o razón social del adjudicatario TOWER CONSULTORES, S.L.  



**20 enero 2022**

[Servicios para la colaboración en la gestión de las convocatorias de becas y ayudas para el curso 2022/2023, dividido en 5 lotes ](https://www.contratos-publicos.comunidad.madrid/contrato-publico/servicios-colaboracion-gestion-convocatorias-becas-ayudas-curso-20222023-dividido)  
Número de expediente A/SER-043029/2021 (C-323M-003-21)  
Presupuesto base licitación. Importe total 249.625,02 euros  
Duración del contrato 9 meses  

Varios adjudicatarios

Nombre o razón social del adjudicatario Randstad Project Services, S.L.U.  
Nombre o razón social del adjudicatario TOWER CONSULTORES, S.L.  

**22 diciembre 2022**

[Colaboración en la gestión de las convocatorias de becas y ayudas para el curso 2023/2024, dividido en 6 lotes ](https://www.contratos-publicos.comunidad.madrid/contrato-publico/colaboracion-gestion-convocatorias-becas-ayudas-curso-20232024-dividido-6-lotes)  
Número de expediente A/SER-046491-2022 (323M-006-2022)  
Presupuesto base licitación. Importe total 336.038,26 euros  
Duración del contrato 12 meses  

Los 6 lotes mismo adjudicatario

Nombre o razón social del adjudicatario TOWER CONSULTORES, S.L.  


**15 enero 2024**

[Colaboración en la gestión de las convocatorias de becas y ayudas para el curso 2024/2025 ](https://contratos-publicos.comunidad.madrid/contrato-publico/colaboracion-gestion-convocatorias-becas-ayudas-curso-20242025-0)  
Número de expediente A/SER-047876/2023 (323M-007/23)  
Presupuesto base licitación. Importe total 993.574,88 euros  
Duración del contrato 17 meses  

Los 6 lotes mismo adjudicatario

Nombre o razón social del adjudicatario TOWER CONSULTORES, S.L.  

Nº del lote Objeto del lote  
1 Becas para la escolarización en el primer ciclo de Educación Infantil en centros de titularidad privada  
Importe de adjudicación (con impuestos) 110.555,23 euros

2 Becas de Formación Profesional de Grado Superior  
Importe de adjudicación (con impuestos) 43.570,99 euros  

3 Becas para el estudio de Programas de Segunda Oportunidad  
Importe de adjudicación (con impuestos) 35.165,24 euros  

4 Becas de Bachillerato  
Importe de adjudicación (con impuestos) 59.633,07 euros  

5 Becas de Formación Profesional de Grado Medio  
Importe de adjudicación (con impuestos) 12.472,84 euros  

6 Becas de Comedor  
Importe de adjudicación (con impuestos) 553.524,22 euros  


**5 diciembre 2024** 

[Servicios para la colaboración en la gestión de las convocatorias de becas y ayudas para el curso 2025/2026, dividido en 5 lotes](https://contratos-publicos.comunidad.madrid/contrato-publico/servicios-colaboracion-gestion-convocatorias-becas-ayudas-curso-20252026-dividido)  
Número de expediente A-SER-042211-2024 (323M-003/2024)  
Presupuesto base licitación. Importe total 1.012.264,90 euros  
Duración del contrato 18 meses  

Nº del lote Objeto del lote  
1 Becas para la escolarización en el primer ciclo de Educación Infantil en centros de titularidad privada  
2 Becas de Formación Profesional  
3 Becas para el estudio de Programas de Segunda Oportunidad.  
4 Becas de Bachillerato  
5 Becas de Comedor  

### Gestión del abono

Parece que la adjudicación siempre es a la misma empresa

NIF del adjudicatario B20158218  
Nombre o razón social del adjudicatario GUREAK MARKETING, S.L.U.  

**13 junio 2018**  
[Gestion de las tarjetas de alumnos beneficiarios de becas para el estudio de formacion profesional de grado superior en la Comunidad de Madrid ](https://contratos-publicos.comunidad.madrid/contrato-publico/gestion-tarjetas-alumnos-beneficiarios-becas-estudio-formacion-profesional-grado-0)  
Número de expediente A/SER-008419/2018 (C-323M/006-18 )  

**12 junio 2019**  
[Gestión de las tarjetas de alumnos beneficiarios de becas para el estudio de bachillerato en la Comunidad de Madrid ](https://contratos-publicos.comunidad.madrid/contrato-publico/gestion-tarjetas-alumnos-beneficiarios-becas-estudio-bachillerato-comunidad-madrid)  
Número de expediente A/SER-009298/2019 (C-323M-002-19)  

**23 julio 2020**  
[Servicios de gestión de las tarjetas de alumnos beneficiarios de becas para el Estudio de Bachillerato en la Comunidad de Madrid ](https://contratos-publicos.comunidad.madrid/contrato-publico/servicios-gestion-tarjetas-alumnos-beneficiarios-becas-estudio-bachillerato)  
Número de expediente A/SER-022517/2020 (C-323M-005-20)  

**10 junio 2022**  
[Gestión del pago a los beneficiarios de becas para estudios no universitarios, dividido en 3 lotes ](https://contratos-publicos.comunidad.madrid/contrato-publico/gestion-pago-beneficiarios-becas-estudios-no-universitarios-dividido-3-lotes)  
Número de expediente A/SER-012081/2022 (C-323M-001-22)  

**30 agosto 2022**  
[Gestión del pago a los beneficiarios de becas para estudios de Formación Profesional de Grado Medio ](https://contratos-publicos.comunidad.madrid/contrato-publico/gestion-pago-beneficiarios-becas-estudios-formacion-profesional-grado-medio)  
Número de expediente A/SER-027106/2022 (C-323M-003-22)  
Presupuesto base licitación. Importe total 41.368,29 euros  
Duración del contrato 12 meses  

**1 agosto 2023**  
[Gestión de las Becas para el estudio de Programas de Segunda Oportunidad ](https://contratos-publicos.comunidad.madrid/contrato-publico/gestion-becas-estudio-programas-segunda-oportunidad)  
Número de expediente A/SER-022294/2023 (323M-005/2023)  
Presupuesto base licitación. Importe total 155.891,50 euros  
Duración del contrato 12 meses  

**7 junio 2024**  
[Gestión del pago a los beneficiarios de becas para estudios no universitarios, dividido en 4 lotes. ](https://contratos-publicos.comunidad.madrid/contrato-publico/gestion-pago-beneficiarios-becas-estudios-no-universitarios-dividido-4-lotes-0)  
Número de expediente A/SER-013225/2024 (323M-001-24)  
Presupuesto base licitación. Importe total 486.218,41 euros  
Duración del contrato 12 meses  



**28 octubre 2024**
[RESOLUCIÓN DEL DIRECTOR GENERAL DE EDUCACIÓN CONCERTADA, BECAS Y AYUDAS AL ESTUDIO, POR LA QUE SE DICTAN INSTRUCCIONES PARA LA GESTIÓN DEL ABONO DE LAS BECAS PARA EL ESTUDIO DE BACHILLERATO, EN EL CURSO 2024-2025.](https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_gestion_tarjetas_bachillerato_24-25_47448157.pdf)  
> Para esta convocatoria la empresa colaboradora será GUREAK MARKETING, S.L.U., que actuará para dichos fines
en nombre y representación de la Consejería de Educación, Ciencia y Universidades.



