+++
author = "Enrique García"
title = "Promoción y titulación en educación"
date = "2024-02-22"
tags = [
    "Madrid", "educación", "normativa"
]
toc = true

+++

Revisado 24 febrero 2024

## Resumen

Con los cambios normativos han ido cambiando las condiciones para la promoción y titulación, en ESO y Bachillerato (incluso obtener el título de ESO sin hacer la ESO, como ocurre en FP Básica)

Se intenta hacer un resumen de normativa, cambios y cómo queda en Madrid. 

Posts relacionados
* [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html)  
* [LOMCE: Abandono escolar temprano](https://algoquedaquedecir.blogspot.com/2018/08/lomce-abandono-escolar-temprano.html)  
* [Calendario escolar Madrid 2017-2018 y convocatoria extraordinaria: detalles de un despropósito](http://algoquedaquedecir.blogspot.com/2017/08/calendario-escolar-madrid-2017-2018-y.html)  
* [Lo básico de normativa básica](https://algoquedaquedecir.blogspot.com/2019/01/lo-basico-de-normativa-basica.html)  

## Detalle

Me centro en las ideas esenciales de las dos últimas leyes, LOMCE y LOMLOE para ver las diferencias. Quizá luego retroceda por comparar con LOE original, LOCE (aunque no se implantó), LOGSE y LGE. 

Me centro en promoción y titulación, no evaluación y calificación, aunque están relacionadas, y por ejemplo en algunas normativas se usan calificaciones para decisiones de promoción y titulación.

### LOMCE

Al ser una modificación de LOE ya derogada se ponen enlaces a la versión correspondiente 

### Titulación ESO LOMCE condicionada a número máximo de suspensos y a superar reválidas 

[(LOMCE) Artículo 28. Evaluación y promoción.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&tn=1&p=20131210#a28)

> Los alumnos y alumnas promocionarán de curso cuando hayan superado todas las materias cursadas o **tengan evaluación negativa en dos materias como máximo**, y repetirán curso cuando tengan evaluación negativa en tres o más materias, o **en dos materias que sean Lengua Castellana y Literatura y Matemáticas de forma simultánea.**  
De forma excepcional, podrá autorizarse la promoción de un alumno o alumna con evaluación negativa en tres materias cuando se den conjuntamente las siguientes condiciones:  
...  

[(LOMCE) Artículo 29. Evaluación final de Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&tn=1&p=20131210#a29)  
> 3. Podrán presentarse a esta evaluación aquellos alumnos y alumnas que hayan obtenido bien evaluación positiva en todas las materias, o bien negativa en un máximo de dos materias siempre que no sean simultáneamente Lengua Castellana y Literatura, y Matemáticas. ...  

[(LOMCE) Articulo 31.  Título de Graduado en Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&tn=1&p=20131210#a31)  
1. **Para obtener el título de Graduado en Educación Secundaria Obligatoria será necesaria la superación de la evaluación final, así como una calificación final de dicha etapa igual o superior a 5 puntos sobre 10.** ...  

El hecho de aprobar el curso pero no la evaluación final y no titular hubiera suponía una situación peculiar

### Titulación ESO LOMCE: programas de mejora del aprendizaje y del rendimiento (PMAR)

En LOMCE se elimina el programa de diversificación curricular LOE, paralelo a 3º y 4º ESO y que permitía titular

LOE  
[Artículo 27. Programas de diversificación curricular.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=43&tn=1&p=20060504#a27)  
> 3. Los programas de diversificación curricular estarán **orientados a la consecución del título de Graduado en Educación Secundaria Obligatoria.**

LOMCE  
[Artículo 27. Programas de mejora del aprendizaje y del rendimiento.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=43&tn=1&p=20131210#a27)  
> 1. El Gobierno definirá las condiciones básicas para establecer los requisitos de los programas de mejora del aprendizaje y del rendimiento que se desarrollarán a partir de 2.º curso de la Educación Secundaria Obligatoria.  
En este supuesto, se utilizará una metodología específica a través de una organización de contenidos, actividades prácticas y, en su caso, de materias diferente a la establecida con carácter general, con la **finalidad de que los alumnos y alumnas puedan cursar el cuarto curso por la vía ordinaria y obtengan el título de Graduado en Educación Secundaria Obligatoria.**

### Titulación Bachillerato LOMCE: ninguna materia suspensa y decisión docente

[(LOMCE) Artículo 36. Evaluación y promoción.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&tn=1&p=20131210#a36)  
> 2. Los alumnos y alumnas promocionarán de primero a segundo de Bachillerato cuando hayan superado las materias cursadas o tengan evaluación negativa en dos materias, como máximo. ...  

[(LOMCE) Artículo 36 bis. Evaluación final de Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&tn=1&p=20131210#a36bis)  
> 2. Sólo podrán presentarse a esta evaluación aquellos alumnos y alumnas que hayan obtenido evaluación positiva en todas las materias

[(LOMCE) Artículo 37. Título de Bachiller.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&tn=1&p=20131210#a37)  
> 1. **Para obtener el título de Bachiller será necesaria la superación de la evaluación final de Bachillerato, así como una calificación final de Bachillerato igual o superior a 5 puntos sobre 10.** ...  

El hecho de aprobar el curso pero no la evaluación final y no titular hubiera suponía una situación peculiar

### Titulación ESO en FP Básica LOMCE

En FP Básica inicialmente no se obtenía el título de ESO, pero cambió durante LOMCE de manera supuestamente transitoria  
Ver [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html)  

### LOMLOE

Se puede ver [evaluación LOMLOE en fiquipedia](https://www.fiquipedia.es/home/legislacion-educativa/lomloe/evaluacion/)

Se cita aquí normativa y jurisprudencia y luego se tratan ideas citando artículos. Se pueden repetir ideas entre página (donde intento centrarme en normativa) y blog.  

[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975)  
Normativa básica por [Disposición final primera. Título competencial.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#df), salvo [anexo III Situaciones de aprendizaje](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#ai-3) 

[Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://boe.es/buscar/act.php?id=BOE-A-2022-5521)  
Normativa básica por [Disposición final segunda. Título competencial.](https://boe.es/buscar/act.php?id=BOE-A-2022-5521#df-2), salvo [anexo III situaciones de aprendizaje](https://boe.es/buscar/act.php?id=BOE-A-2022-5521#ai-3)

[ORDEN 1712/2023, de 19 de mayo, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/31/BOCM-20230531-17.PDF)  
> Artículo 33  
> 1. Obtendrán el título de Graduado en Educación Secundaria Obligatoria los alumnos
que, al finalizar cuarto de la Educación Secundaria Obligatoria y **a juicio del equipo docente, hayan adquirido las competencias clave** establecidas en el perfil de salida y alcanzado los
objetivos de etapa, sin perjuicio de lo dispuesto en el artículo 31.3 del Decreto 65/2022,
de 20 de julio.  
> 2. Las decisiones sobre la obtención del título serán adoptadas de forma colegiada por
el equipo docente del alumno en los términos y condiciones establecidos en el artículo 21.4 del
Decreto 65/2022, de 20 de julio, y de conformidad con los criterios de actuación recogidos
en el proyecto educativo de centro. En todo caso, titularán aquellos alumnos que hayan superado todas las materias y ámbitos cursados, dado que este hecho implica que el alumno
ha adquirido el conjunto de los descriptores del perfil de salida recogidos en el anexo I del
Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria, así como que ha alcanzado los objetivos de etapa.  
> 3. Para facilitar la toma de decisiones por parte de los equipos docentes acerca de la titulación, estos podrán considerar que los alumnos han adquirido las competencias establecidas y alcanzado los objetivos de etapa y, consecuentemente, podrán titular, cuando **el conjunto de la carga lectiva semanal recogida en el anexo I del Decreto 65/2022, de 20 de julio, de las materias no superadas por el alumno en la Educación Secundaria Obligatoria sea inferior a doce horas y el alumno haya obtenido una nota media de la etapa, igual o superior a cinco.**  

El artículo 21.4 mencionado indica  
> Artículo 21  
El proceso de evaluación  
> 4. Las decisiones del equipo docente para la promoción o titulación del alumno atenderán a la consecución de los objetivos, al grado de adquisición de las competencias establecidas y a la valoración de las medidas que favorezcan el progreso del mismo, y **se adoptarán por mayoría cualificada de dos tercios**, previa deliberación, de la que se dejará
constancia en acta. Las demás decisiones serán adoptadas por consenso y, si ello no fuera
posible, se adoptará el criterio de la mayoría absoluta, es decir, más de la mitad de los
miembros que integran el equipo docente del alumno.

El artículo 31.3 mencionado indica   
> Artículo 31  
Alumnos con necesidades educativas especiales  
>3. Se podrán realizar adaptaciones que se aparten significativamente de los criterios
de evaluación y los contenidos del currículo cuando se precise de ellas para facilitar a estos
alumnos la accesibilidad al currículo. Dichas adaptaciones se realizarán buscando el máximo desarrollo posible de las competencias y contendrán los referentes que serán de aplicación en la evaluación de estos alumnos, **sin que este hecho pueda impedirles la promoción o la titulación.**

[Primer golpe a las reglas para pasar de curso en la Comunidad de Madrid tras el fallo del TSJM - elpais](https://elpais.com/espana/madrid/2024-02-04/primer-golpe-a-las-reglas-para-pasar-de-curso-en-la-comunidad-de-madrid-tras-el-fallo-del-tsjm.html)  
El Tribunal tumba las instrucciones de una resolución de 2021 que, según CC OO, afectaba la aplicación de la norma estatal

### Eliminación evaluación extraordinaria

[Artículo 15. Evaluación.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a1-7)  
> 9. Con independencia del seguimiento realizado a lo largo del curso, el equipo docente llevará a cabo la evaluación del alumnado de forma colegiada en una **única sesión que tendrá lugar al finalizar el curso escolar**.

Es algo que ya se había hecho en algunas comunidades durante LOMCE, ver [Calendario escolar Madrid 2017-2018 y convocatoria extraordinaria: detalles de un despropósito](http://algoquedaquedecir.blogspot.com/2017/08/calendario-escolar-madrid-2017-2018-y.html)

### Titulación ESO LOMLOE: sin número de materias suspensas, adquirir competencias y alcanzar objetivos, decisión equipo docente

[Artículo 17. Título de Graduado en Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a1-9)  
> 1. Obtendrán el título de Graduado en Educación Secundaria Obligatoria los alumnos y alumnas que, al terminar la Educación Secundaria Obligatoria, **hayan adquirido, a juicio del equipo docente, las competencias** clave establecidas en el Perfil de salida y alcanzado los objetivos de la etapa, sin perjuicio de lo establecido en el artículo 20.3.  
> 2. Las decisiones sobre la obtención del título serán **adoptadas de forma colegiada por el profesorado** del alumno o la alumna. Las administraciones educativas podrán establecer criterios para orientar la toma de decisiones de los equipos docentes con relación al grado de adquisición de las competencias clave establecidas en el Perfil de salida y en cuanto al logro de los objetivos de la etapa, siempre que dichos criterios **no impliquen la fijación del número ni la tipología de las materias no superadas**.

[Artículo 20. Alumnado con necesidades educativas especiales.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2-2)  
> 3. Con este propósito, las administraciones educativas establecerán los procedimientos oportunos para realizar adaptaciones de los elementos del currículo que se aparten significativamente de los que determina este real decreto cuando se precise de ellas para facilitar a este alumnado la accesibilidad al currículo. Dichas adaptaciones se realizarán buscando el máximo desarrollo posible de las competencias y contendrán los referentes que serán de aplicación en la evaluación de este alumnado, **sin que este hecho pueda impedirles la promoción o la titulación**.

[ORDEN 1712/2023, de 19 de mayo, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/31/BOCM-20230531-17.PDF)  

> Artículo 33  
> 1. Obtendrán el título de Graduado en Educación Secundaria Obligatoria los alumnos
que, al finalizar cuarto de la Educación Secundaria Obligatoria y **a juicio del equipo docente, hayan adquirido las competencias clave** establecidas en el perfil de salida y alcanzado los
objetivos de etapa, sin perjuicio de lo dispuesto en el artículo 31.3 del Decreto 65/2022,
de 20 de julio.  
> 2. Las decisiones sobre la obtención del título serán adoptadas de forma colegiada por
el equipo docente del alumno en los términos y condiciones establecidos en el artículo 21.4 del
Decreto 65/2022, de 20 de julio, y de conformidad con los criterios de actuación recogidos
en el proyecto educativo de centro. En todo caso, titularán aquellos alumnos que hayan superado todas las materias y ámbitos cursados, dado que este hecho implica que el alumno
ha adquirido el conjunto de los descriptores del perfil de salida recogidos en el anexo I del
Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria, así como que ha alcanzado los objetivos de etapa.  

El artículo 21.4 mencionado indica  
> Artículo 21  
El proceso de evaluación  
> 4. Las decisiones del equipo docente para la promoción o titulación del alumno atenderán a la consecución de los objetivos, al grado de adquisición de las competencias establecidas y a la valoración de las medidas que favorezcan el progreso del mismo, y **se adoptarán por mayoría cualificada de dos tercios**, previa deliberación, de la que se dejará
constancia en acta. Las demás decisiones serán adoptadas por consenso y, si ello no fuera
posible, se adoptará el criterio de la mayoría absoluta, es decir, más de la mitad de los
miembros que integran el equipo docente del alumno.

### Titulación ESO LOMLOE: programas de diversificación curricular

Es un programa paralelo a los cursos 3º y 4º ESO. Se hacen grupos con ratios más reducidas y agrupando materias en ámbitos y se consigue atender mejor a alumnos que con ello consiguen titular.

Diversificación curricular se abandonó en LOMCE (donde se sustituyó por PMAR que era paralelo a 2º y 3º ESO y hacía que titular dependiese de cursar un 4º ESO ordinario) y se recupera en LOMLOE. 

[Artículo 27. Programas de diversificación curricular.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a27)  
> 2. Los programas de diversificación curricular estarán **orientados a la consecución del título de Graduado en Educación Secundaria Obligatoria**, por parte de quienes presenten dificultades relevantes de aprendizaje tras haber recibido, en su caso, medidas de apoyo en el primero o segundo curso, o a quienes esta medida de atención a la diversidad les sea favorable para la obtención del título.

[Orden 190/2023, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se desarrolla la organización y el currículo del programa de diversificación curricular de la Educación Secundaria Obligatoria en la Comunidad de Madrid.](https://www.comunidad.madrid/sites/default/files/aud/educacion/2023-02-07_orden_diversificacion.pdf)  
> Artículo 12. Titulación.  
> 1. Obtendrán el título de Graduado en Educación Secundaria Obligatoria los
alumnos que al finalizar el programa de diversificación curricular, y a juicio del equipo
docente, hayan adquirido las competencias clave y hayan alcanzado los objetivos de la
etapa.  
...  
> 3. Las decisiones del equipo docente se adoptarán de conformidad con lo dispuesto
en el artículo 21.4 del Decreto 65/2022, de 20 de julio. Para orientar esta toma de decisiones, el equipo docente podrá considerar que el alumno ha adquirido las competencias clave y ha alcanzado los objetivos de la etapa cuando la nota media del programa de diversificación curricular sea igual o superior a cinco, **siempre que se hayan superado los ámbitos**.
Esta nota media se obtendrá del cálculo de la media ponderada, según la carga horaria de
los diferentes ámbitos y materias cursados en el programa de diversificación curricular

### Titulación Bachillerato en LOMLOE: máximo materia suspensa con condiciones y decisión equipo docente

[Artículo 22. Título de Bachiller.](https://boe.es/buscar/act.php?id=BOE-A-2022-5521#a2-4)  
> 2. Para obtener el título de Bachiller será necesaria la evaluación positiva en todas las materias de los dos cursos de Bachillerato.  
> 3. Excepcionalmente, el equipo docente podrá decidir la obtención del título de Bachiller por un alumno o alumna que haya superado todas las materias salvo una, siempre que se cumplan además todas las condiciones siguientes:  
a) Que el equipo docente considere que el alumno o alumna ha alcanzado los objetivos y competencias vinculados a ese título.  
b) Que no se haya producido una inasistencia continuada y no justificada por parte del alumno o alumna en la materia.  
c) Que el alumno o alumna se haya presentado a las pruebas y realizado las actividades necesarias para su evaluación, incluidas las de la convocatoria extraordinaria.  
d) que la media aritmética de las calificaciones obtenidas en todas las materias de la etapa sea igual o superior a cinco. En este caso, a efectos del cálculo de la calificación final de la etapa, se considerará la nota numérica obtenida en la materia no superada.

### Recursos entre ministerio y CCAA promoción y titulación LOMLOE

**17 noviembre 2021**

[Ayuso con la vieja escuela: “Estamos absolutamente en contra” de suprimir los exámenes de recuperación en la ESO - diario16.com](https://diario16.com/ayuso-con-la-vieja-escuela-estamos-absolutamente-en-contra-de-suprimir-los-examenes-de-recuperacion-en-la-eso/)  
>Es una garantía unos padres ricos y un colegio privado concertado  
>Para la presidenta madrileña “es un agravio enorme entre colegios porque cada claustro de profesores puede decidir ya de manera arbitraria lo que sube y lo que baja”.  
>Cuando Ayuso insinúa que los profesores pueden estar condicionados a aprobar a un alumno o alumna, ¿a qué se refiere? ¿A la enseñanza pública o a la enseñanza concertada?  

Realmente ya se había suprimido por Madrid, ver [Calendario escolar Madrid 2017-2018 y convocatoria extraordinaria: detalles de un despropósito](http://algoquedaquedecir.blogspot.com/2017/08/calendario-escolar-madrid-2017-2018-y.html)

**21 noviembre 2021**

[Madrid denuncia que el decreto de evaluación incumple los plazos de la Ley Celaá y exige su retirada - 20minutos.es](https://www.20minutos.es/noticia/4899025/0/comunidad-madrid-decreto-educacion/)  

**25 mayo 2022**

[La Justicia suspende parte del decreto con el que Madrid quería torpedear la ley de Educación del Gobierno](https://www.eldiario.es/sociedad/justicia-suspende-parte-decreto-madrid-queria-cambiar-sistema-evaluacion-secundaria_1_9024479.html)  
[La Justicia paraliza el decreto de Madrid que regula la evaluación en ESO y Bachillerato](https://elpais.com/espana/madrid/2022-05-25/el-tsjm-paraliza-la-aplicacion-del-decreto-educativo-de-madrid-que-regula-la-evaluacion-del-alumnado-de-eso-y-bachillerato.html)  

Auto descargable de madrid.org/cove con CSV 1259365550890448008897  

**16 febrero 2023**

[El Gobierno defiende en los tribunales el nuevo sistema para pasar de curso - elpais](https//elpais.com/educacion/2023-02-16/el-gobierno-defiende-en-los-tribunales-el-nuevo-sistema-para-pasar-de-curso-y-titular-de-la-lomloe.html)

**4 febrero 2024**

[Primer golpe a las reglas para pasar de curso en la Comunidad de Madrid tras el fallo del TSJM - elpais](https://elpais.com/espana/madrid/2024-02-04/primer-golpe-a-las-reglas-para-pasar-de-curso-en-la-comunidad-de-madrid-tras-el-fallo-del-tsjm.html)  
El Tribunal tumba las instrucciones de una resolución de 2021 que, según CC OO, afectaba la aplicación de la norma estatal

[Varapalo para el Gobierno de Madrid: CCOO tumba en el TSJM las instrucciones que adelantaron la evaluación de la ESO y del Bachillerato en la Comunidad de Madrid arremetiendo contra la LOMLOE](https://feccoo-madrid.org/noticia:684420--Varapalo_para_el_Gobierno_de_Madrid_CCOO_tumba_en_el_TSJM_las_instrucciones_que_adelantaron_la_evaluacion_de_la_ESO_y_del_Bachillerato_en_la_Comunidad_de_Madrid_arremetiendo_contra_la_LOMLOE&opc_id=9f666eca08d47ba26c6a09927d7abbbb)  
    La Comunidad de Madrid hizo un simulacro de adopción de las novedades que introducía a LOMLOE que, en la práctica, suponía anular los aspectos esenciales contenidos en la Ley Orgánica con respecto al proceso de evaluación del alumnado por parte del profesorado, impulsando desigualdades en la evaluación del alumnado madrileño con respecto al alumnado de otras comunidades  
    La sentencia nº 853/2023 por la que la Sección Octava de la Sala de lo Contencioso-Administrativo del Tribunal Superior de Justicia de Madrid estima en su totalidad el recurso contencioso administrativo interpuesto por la Federación de Enseñanza de CCOO de Madrid

[STSJ Madrid, a 19 de diciembre de 2023 - ROJ: STSJ M 14107/2023 ECLI:ES:TSJM:2023:14107 Nº de Resolución: 853/2023](https://www.poderjudicial.es/search/AN/openDocument/30c265bc2f4afceda0a8778d75e36f0d/20240116)  

> CUARTO.- Sobre la falta del oportuno rango normativo de la resolución impugnada.  
> 14. Bajo este título la demanda plantea la nulidad de la resolución impugnada por entender que, aunque
formalmente se ha dictado como un acto administrativo, incorpora una regulación que debería haberse
aprobado como disposición general.
...  
> FALLAMOS  
> 1. ESTIMAR el recurso ... y se declara la nulidad de los siguientes extremos (subrayados y en negrita):  
> "Instrucción Quinta. Características de la evaluación (en ESO)  
> 7. Las decisiones de promoción y titulación en Educación Secundaria Obligatoria, atendiendo a la consecución
de los objetivos, al grado de adquisición de las competencias establecidas y a la valoración de las medidas
que favorezcan el progreso del alumno, habrán de adoptarse por mayoría cualificada de dos tercios, previa
deliberación del equipo docente del alumno, de la que se dejará constancia en acta. Las demás decisiones serán
adoptadas por consenso y, si ello no fuera posible, **se adoptará el criterio de la mayoría absoluta, es decir, más de la mitad de los miembros que integran el equipo docente del alumno.**  
...  
Duodécima. Titulación (en ESO)  
> 2. Las decisiones sobre la obtención del título serán adoptadas de forma colegiada por el equipo docente del
alumno, **conforme a lo establecido en la instrucción quinta**. En todo caso, titularán quienes hayan superado
todas las materias o ámbitos cursados.

### Otras vías de titulación

De momento ideas, algunos temas dan para desarrollar y pueden ser otros posts. En el caso de titulación ESO en FP Básica ya tiene su post.

### Titulación ESO en FP Básica

En FP Básica ya siempre se obtiene el título de ESO, ya no depende del equipo docente  
Ver [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html)  

En LOMCE 2013 

[Artículo 44. Títulos y convalidaciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=66&tn=1&p=20131210#a44)  
> 1. Los alumnos y alumnas que superen un ciclo de Formación Profesional Básica recibirán el título Profesional Básico correspondiente.  
> El título Profesional Básico permitirá el acceso a los ciclos formativos de grado medio de la Formación Profesional del sistema educativo.  
> Los alumnos y alumnas que se encuentren en posesión de un título Profesional Básico **podrán** obtener el título de Graduado en Educación Secundaria Obligatoria por cualquiera de las dos opciones a las que se refiere el artículo 29.1 de esta Ley Orgánica, mediante la **superación de la evaluación final de Educación Secundaria Obligatoria** en relación con las materias del bloque de asignaturas troncales que como mínimo se deban cursar en la opción que escoja el alumno o alumna. La calificación final de Educación Secundaria Obligatoria será la nota obtenida en la evaluación final de Educación Secundaria Obligatoria.

[Real Decreto 1058/2015. Disposición transitoria única. Obtención del título de Graduado en Educación Secundaria Obligatoria por los titulados en Formación Profesional Básica en los cursos 2015/16 o 2016/17.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-12894#dtunica)  
> Los alumnos y alumnas que obtengan un título de Formación Profesional Básica en los cursos 2015/2016 o 2016/2017, en tanto no sea de aplicación la evaluación prevista en el artículo 44.1 de la Ley Orgánica 2/2006, de 3 de mayo, de Educación, **podrán** obtener el título de Educación Secundaria Obligatoria, siempre que, en la evaluación final del ciclo formativo, el **equipo docente considere** que han alcanzado los objetivos de la Educación Secundaria Obligatoria y adquirido las competencias correspondientes

[Real Decreto-ley 5/2016. Artículo 1. Modificación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa. Disposición final quinta. Calendario de implantación](https://www.boe.es/buscar/act.php?id=BOE-A-2016-11733)  
> Hasta la entrada en vigor de la normativa resultante del Pacto de Estado social y político por la educación, los alumnos que obtengan un título de Formación Profesional Básica **podrán** obtener el título de Educación Secundaria Obligatoria, siempre que, en la evaluación final del ciclo formativo, el **equipo docente considere** que han alcanzado los objetivos de la Educación Secundaria Obligatoria y adquirido las competencias correspondientes

En LOMLOE  

[Artículo 44. Títulos y convalidaciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a44)  
> 1. Los alumnos y alumnas que superen un ciclo formativo de grado básico **recibirán** el título de Graduado en Educación Secundaria Obligatoria
 
[Artículo 25. Ciclos Formativos de Grado Básico.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2-7)  
> 7. **La superación de la totalidad de los ámbitos incluidos en un ciclo de grado básico conducirá a la obtención del título de Graduado en Educación Secundaria Obligatoria**. Para favorecer la justificación en el ámbito laboral de las competencias profesionales adquiridas, el alumnado al que se refiere este apartado recibirá asimismo el título de Técnico Básico en la especialidad correspondiente.
 
### Pruebas libres

Existen pruebas libres para obtener el graduado en ESO y de Bachillerato

[Artículo 31. Título de Graduado en Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a31)  
> 4. Las Administraciones educativas, al organizar las pruebas libres para la obtención del título de Graduado en Educación Secundaria Obligatoria, determinarán las partes de la prueba que tiene superadas cada uno de los aspirantes de acuerdo con su historia académica previa.

[Regulación de Pruebas libres - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/regulacion-pruebas-libres#panel-11540)  

### Educación de adultos

[Artículo 5. El aprendizaje a lo largo de la vida.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a5)  
> 4. Asimismo, corresponde a las Administraciones públicas promover, ofertas de aprendizaje flexibles que permitan la adquisición de competencias básicas y, en su caso, las correspondientes titulaciones, a aquellos jóvenes y adultos que **abandonaron el sistema educativo sin ninguna titulación.**  
> 5. El sistema educativo debe facilitar y las Administraciones públicas deben promover que **toda la población llegue a alcanzar una formación de educación secundaria postobligatoria o equivalente.**  

[LOE CAPÍTULO IX Educación de personas adultas](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#cix)

[Real Decreto 217/2022 Disposición adicional tercera. Educación de Personas Adultas.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975#da-3)
> 7. La **superación de todos los ámbitos dará derecho a la obtención del título de Graduado en Educación Secundaria Obligatoria**. Asimismo, el equipo docente podrá proponer para la expedición del título de Graduado en Educación Secundaria Obligatoria a aquellas personas que, aun no habiendo superado alguno de los ámbitos, se considere que han conseguido globalmente los objetivos generales de la formación básica de las personas adultas. En esta decisión se tendrán en cuenta las posibilidades formativas y de integración en la actividad académica y laboral de cada alumno o alumna.  

[Educación de personas adultas - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/educacion-personas-adultas)

### Convalidación de títulos extranjeros

[Homologación de estudios extranjeros no universitarios - mpt.gob.es](https://mpt.gob.es/portal/delegaciones_gobierno/delegaciones/madrid/servicios/educacion/homologaci-n-de-estudios-extranjeros-no-universitarios.html)

[Convalidación y homologación de títulos y estudios extranjeros no universitarios - educacionyfp.gob.es](https://www.educacionyfp.gob.es/mc/convalidacion-homologacion/portada.html)

El caso de Froilán es un ejemplo que muestra el caos normativo

[El 'truco' de Froilán para pasar de 'tripitir' 2º de la ESO a la universidad en apenas dos años](https://www.huffingtonpost.es/entry/froilan-pasa-de-tripitir-2-de-la-eso-a-la-universidad-en-apen_es_5c8af2fce4b0f374fa9b9109.html)  
> ha logrado aprobar los cinco cursos que le quedaban —tres de la la ESO y dos de bachillerato— para acceder a la universidad en apenas dos años gracias al sistema estadounidense   

[twitter FiQuiPedia/status/884792680202502148](https://twitter.com/FiQuiPedia/status/884792680202502148)  
sistema educativo español es Frankenstein con trozos de leyes muertas. Froilán tendrá título COU que no ha cursado  
[Orden de 27 de enero de 1989 por la que se aprueba el régimen de equivalencias de los estudios del sistema educativo de los Estados Unidos de América con los correspondientes españoles de Educación General Básica, Bachillerato Unificado y Polivalente y Curso de Orientación Universitaria.](https://www.boe.es/buscar/doc.php?id=BOE-A-1989-2666)  

### Otras ideas sobre promoción y titulación

### Calificaciones numéricas en promoción y titulación

Evaluar y calificar son cosas distintas, y la promoción y titulación se supone que se centran en evaluar, pero a menudo se toma una decisión en base a una calificación que es numérica, o se cita la calificación numérica en el título.  

La propia normativa LOMCE citaba que el título incluye la calificación numérica  
[(LOMCE) Artículo 31. Título de Graduado en Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899&b=47&tn=1&p=20131210#a31)  
> 3. En el título deberá constar la opción u opciones por las que se realizó la evaluación final, así como la **calificación** final de Educación Secundaria Obligatoria.  
Se hará constar en el título, por diligencia o anexo al mismo, la nueva **calificación** final de Educación Secundaria Obligatoria cuando el alumno o alumna se hubiera presentado de nuevo a evaluación por la misma opción para elevar su calificación final.  
También se hará constar, por diligencia o anexo, la superación por el alumno o alumna de la evaluación final por una opción diferente a la que ya conste en el título, en cuyo caso la **calificación** final será la más alta de las que se obtengan teniendo en cuenta los resultados de ambas opciones.

En LOMLOE mismo [artículo 31](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a31) deja de citar que título incluya la calificación numérica. 

Se puede ver normativa LOMLOE Madrid que combina titulación y calificación numérica 

[ORDEN 1712/2023, de 19 de mayo, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/31/BOCM-20230531-17.PDF)  

> Artículo 33  
...   
> 3. Para facilitar la toma de decisiones por parte de los equipos docentes acerca de la titulación, estos podrán considerar que los alumnos han adquirido las competencias establecidas y alcanzado los objetivos de etapa y, consecuentemente, podrán titular, cuando **el conjunto de la carga lectiva semanal recogida en el anexo I del Decreto 65/2022, de 20 de julio, de las materias no superadas por el alumno en la Educación Secundaria Obligatoria sea inferior a doce horas y el alumno haya obtenido una nota media de la etapa, igual o superior a cinco.**  

### Implicaciones de la promoción y titulación como decisión de equipo docente

Lo comento en [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html) cuando solo era decisión de equipo docente: posible presión alumnos, familias, y condicionamientos al estar decisión de titulación en mano de empleados en caso de centros privados. 

Se puede decir que siempre ha estado en manos cada docente la calificación, pero que el cambio es que se pasa de calificaciones a competencias y objetivos y pasa a ser que decisión de todo el equipo tomar una decisión aunque para un docente no haya superado su materia.

[twitter FiQuiPedia/status/1513119723097104384](https://twitter.com/FiQuiPedia/status/1513119723097104384)  
Interesante hilo al que añadiría que en los privados, con o sin concierto, solo dependerá de docentes elegidos sin transparencia la decisión de promoción y titulación de los hijos de los clientes de su empleador.  
Qué podría salir mal.  
![](https://pbs.twimg.com/media/FP-uXVwXoAgnGp8?format=png)  
![](https://pbs.twimg.com/media/FP-uXVwXoAgnGp8?format=png)  

[twitter FiQuiPedia/status/1513121922086838278](https://twitter.com/FiQuiPedia/status/1513121922086838278)  
Enlaza con esto que ya pasa con la obtención de título ESO vía FPB  
[twitter FiQuiPedia/status/1190207814683283456](https://twitter.com/FiQuiPedia/status/1190207814683283456)  
Hay un factor adicional, no sé su peso en el caso de ACNEEs: en FPB la decisión de titular ESO es de los docentes, luego padres-clientes pueden elegir centro por su publicidad de alto % titulados ESO ya que depende de decisión empresa privada sea así  
[ESO parece pero ESO no es - algoquedaquedecir](http://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html)  

[twitter jrfercuen/status/1513069114948005888](https://twitter.com/jrfercuen/status/1513069114948005888)  
Hilo sobre la LOMLOE y los criterios de promoción y titulación en la ESO.  
Este año, como todos sabemos, entran ya en vigor estos nuevos criterios, recogidos en el Real Decreto 984/2021:   
Mucho se ha hablado ya sobre ellos, destacando los muchos titulares de la prensa que recalcan que el equipo docente será el que, mediante votación, decida qué alumnos con más de dos suspendidas pasan de curso  
Como sabemos, hasta ahora la repetición de curso era automática con 3 o más suspensos (solo en el caso de 3 suspensos podía haber excepciones si se cumplían una serie de condiciones). Ahora, en cambio, el equipo docente podrá decidir la promoción incluso en estos casos.  
Y no sé si lo habéis pensado, pero esto lo vamos a tener que hacer ya en poco más de dos meses. Con un añadido: que este año ya no va a haber evaluación extraordinaria. Más drama para esas evaluaciones finales de junio que se prevén muy largas.  
Creo que no somos conscientes del problema que se nos puede presentar. El hecho de que cada junta de evaluación vote qué alumnos con más de tres suspensos pasan y cuáles no creará muy probablemente agravios comparativos.  
En un mismo centro, habrá juntas de evaluación que darán luz verde a la promoción de alguien que lleve cuatro o cinco suspendidas y juntas que harán repetir a alguien que solo haya suspendido tres. No hablemos ya de las diferencias entre la laxitud de unos y otros centros.  
Podemos entrar en una espiral muy peligrosa de reclamaciones y de decisiones polémicas de las inspecciones educativas. Sobre todo teniendo en cuenta que en el espíritu de la ley está el convertir la repetición en algo excepcional.  
No es casualidad, de hecho, que esa coletilla de la excepcionalidad la hayan repetido tanto los medios de comunicación:  
Pero peor aún es lo que dice este Real Decreto acerca de la titulación en la ESO. Aquí la ambigüedad ya es extrema, ya que ni siquiera hay mención alguna a un número máximo de materias suspensas que asegure la titulación   
No hay que ser muy listo para darse cuenta de que tanta ambigüedad, obviamente, también provocará agravios comparativos. Y en el peor de los casos, teniendo en cuenta que aquí estará en juego nada menos que el título de la ESO, también traerá consigo reclamaciones…  
…y decisiones polémicas de las inspecciones educativas, amparándose por ejemplo en defectos de forma. Y anulando de facto todo el trabajo realizado por los docentes para evaluar de la forma más rigurosa posible a su alumnado. El docente, ninguneado una vez más.  
Por desgracia, creo que todo esto acabará teniendo como consecuencia la devaluación del título de la ESO, como ya pronostican muchos docentes. La atención a la diversidad de nuestro alumnado, la sensibilidad con los casos más complicados…  
…y el problema que hemos tenido históricamente con nuestras tasas de fracaso escolar no puede resolverse así,bajando de esa manera la exigencia para obtener el título.Haciendo eso mejorarán las estadísticas,pero no el nivel de nuestro alumnado,con lo que el problema seguirá ahí.  
A no ser, claro, que por algún motivo a quienes mandan no les interese mejorarlo…  
Estos problemas mencionados en los tuits anteriores deberían resolverse con más recursos, mejores ratios por aula y, en general,más inversión. Se dice muy poco (porque no les conviene decirlo) que somos el 5º país de la UE que menos invierte en educación. Qué podía salir mal…   
En definitiva: mucha suerte, compañeros/as docentes, con las evaluaciones de junio que ya están mucho más cerca de lo que parecen. Y mucha paciencia también. Me temo que nos va a hacer falta…  

### Titulación de alumnos con necesidades y adaptaciones

Los alumnos con adaptaciones significativas tenían "notas con asteriscos" que indicaban una nota sobre la adaptación, no sobre los contenidos el curso. La gestión dependía de cada centro, aunque creo que en general se ponía una nota con asterisco de suspenso al ser significativa, y por separado se daba un boletín de notas con la nota sobre la adaptación, que venía a informar de que al alumno no había superado los objetivos de ese curso, pero se informaba sobre los que había trabajado.

Con el cambio a LOMLOE queda claro que se promociona y titula con lo trabajado en la adaptación  

En LOE

[Artículo 20. Evaluación durante la etapa.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a20)  
> 5. Los referentes de la evaluación en el caso de alumnos y alumnas con necesidades educativas especiales serán los incluidos en las correspondientes adaptaciones del currículo, **sin que este hecho pueda impedirles promocionar de ciclo o etapa.**

En Madrid 

[ORDEN 1712/2023, de 19 de mayo, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/31/BOCM-20230531-17.PDF)  
> Artículo 31  
Alumnos con necesidades educativas especiales  
>3. Se podrán realizar adaptaciones que se aparten significativamente de los criterios
de evaluación y los contenidos del currículo cuando se precise de ellas para facilitar a estos
alumnos la accesibilidad al currículo. Dichas adaptaciones se realizarán buscando el máximo desarrollo posible de las competencias y contendrán los referentes que serán de aplicación en la evaluación de estos alumnos, **sin que este hecho pueda impedirles la promoción o la titulación.**

### Estadísticas, repetición, ley de Goodhart

Se comenta que en España la tasa de repetición es elevada, y eso enlaza con cómo se decide la promoción y titulación. La flexibilización de criterios durante la pandemia hizo que se titulase mucho más. También se puede ver la segregación por titularidad en la promoción y titulación, y la dependencia de factores como el nivel de estudios familiar

Relacionado con lo de titular ESO con FP Básicas, está la idea de arreglar las estadísticas no solucionando el problema de fondo. 

[twitter FiQuiPedia/status/1090600480328335360](https://twitter.com/FiQuiPedia/status/1090600480328335360)  
En diciembre publicaron documento valoración provisional Implantación del nuevo calendario escolar 2017/18
Aparte de truncar gráficas y variaciones anuales pequeñas, llama la atención la diferencia promoción públicos vs concertados  
[18.121 valoración provisional calendario escolar 17-18 pdf](https://www.comunidad.madrid/file/134976/download?token=oZMX1YXd)
![](https://pbs.twimg.com/media/DyKWgklX4AEKdzf?format=jpg)  

