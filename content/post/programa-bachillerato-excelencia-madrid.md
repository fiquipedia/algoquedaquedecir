+++
author = "Enrique García"
title = "Programa Bachillerato Excelencia Madrid"
date = "2024-02-07"
tags = [
    "Madrid", "educación", "comisiones de servicio", "Excelencia bachillerato"
]
toc = true

+++

Revisado 3 agosto 2024

## Resumen

En Madrid existe el programa de Bachillerato de excelencia desde 2012 con dos modalidades: centros de excelencia (solo el IES San Mateo) y aulas de excelencia (con varios centros). 

Se intenta reflejar aquí información sobre el programa separada de las comisiones de servicio: ver post separado [Comisiones de Servicio Madrid (IV): programas en centros. Bachillerato de excelencia](../comisiones-de-servicio-madrid-iv-programas-en-centros-bachillerato-de-excelencia)

Se incluye información del IES San Mateo: la información de lo que pasa en ese centro es parte de lo que pasa en el programa de excelencia.

Relacionado con [Finalidad evaluación para clasificación centros](../finalidad-evaluacion-para-clasificacion-centros)

## Detalle

Información general en [Programa de Excelencia en Bachillerato - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/programa-excelencia-bachillerato)  

Desde 2020 tengo destino, vía concurso de traslados, en un centro de Madrid con aulas de excelencia

[twitter FiQuiPedia/status/1417124931108843520](https://twitter.com/FiQuiPedia/status/1417124931108843520)  
Aparte de ese centro único de Bachillerato de excelencia, hay aulas de Bachillerato de excelencia en otros 15 centros, como el mío actual al que he llegado este curso.

### Información histórica sobre el programa de excelencia de Bachillerato

**2011**

[El Gobierno considera segregador el bachillerato de excelencia - cadenaser.com](https://cadenaser.com/emisora/2011/04/06/radio_madrid/1302047415_850215.html)

[El instituto para los mejores estará en el centro de Madrid - elpais](https://elpais.com/elpais/2011/04/07/actualidad/1302164223_850215.html)

[Dos alumnos abandonan el Bachillerato de Excelencia "por no alcanzar el nivel" - elpais](https://elpais.com/ccaa/2011/12/14/madrid/1323868201_356707.html)

**2012**

[VOTO PARTICULAR PRESENTADO POR LOS CONSEJEROS REPRESENTANTES DE LA FAPA FRANCISCO GINER DE LOS RÍOS CON RELACIÓN A LA ORDEN DE LA CONSEJERÍA DE EDUCACIÓN Y EMPLEO POR LA QUE SE REGULA EL PROGRAMA DE EXCELENCIA EN BACHILLERATO Y SU IMPLANTACIÓN EN INSTITUTOS DE EDUCACIÓN SECUNDARIA DE LA COMUNIDAD DE MADRID.](https://www.fapaginerdelosrios.org/DesktopModules/Bring2mind/DMX/API/Entries/Download?entryid=943&command=core_download&PortalId=0&TabId=186)

[Nota de prensa Bachillerato Excelencia 7 de mayo de 2012 FAPA Giner de los Ríos](https://www.fapaginerdelosrios.org/DesktopModules/Bring2mind/DMX/API/Entries/Download?entryid=95&command=core_download&PortalId=0&TabId=193)

[La Giner de los Ríos tilda de “fracaso” el Bachiller de Excelencia - elpais.com](https://elpais.com/ccaa/2012/05/07/madrid/1336409539_455848.html)

[“Cesta y puntos” o el imaginario de la excelencia educativa - Publicado en Escuela el 14 de junio de 2012 (dropbox de Mariano Martín Gordillo)](https://www.dropbox.com/s/cwvendxr5f2syor/2012-06%20%28Cesta%20y%20puntos%20o%20el%20imaginario%20de%20la%20excelencia%20educativa%29.pdf)

[“Cesta y puntos” o el imaginario de la excelencia educativa - mácula. Blog de Mariano Martín Gordillo](http://maculammg.blogspot.com/2012/10/cesta-y-puntos-o-el-imaginario-de-la.html)

[“Digan a sus hijos que aparquen el amor y se centren en el trabajo” - elpais](https://elpais.com/sociedad/2012/12/21/actualidad/1356120041_305501.html)  
El director del Bachillerato de Excelencia de Madrid, impartido en un centro público, pide a los padres que impidan las relaciones de los alumnos

**2013**

[¿Bachillerato de excelencia? - mácula. Blog de Mariano Martín Gordillo](http://maculammg.blogspot.com/2013/06/bachillerato-de-excelencia.html)  

**2014**

[El fracaso del bachillerato de excelencia. Nota de prensa de la @FAPA_Giner_Rios - recuperarmadrid](http://recuperarmadrid.blogspot.com/2014/02/el-fracaso-del-bachillerato-de.html)  
_Nota no disponible en enlace original FAPA_

[Bachillerato de excelencia. Nota de prensa FAPA Giner Ríos (pdf)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2014-02-10-Bachillerato%20de%20excelencia%20(FAPA%20Giner%20de%20los%20R%C3%ADos).pdf)

[Los alumnos «excelentes» del Instituto San Mateo quieren ser ingenieros - abc](https://www.abc.es/madrid/20140312/abcp-alumnos-excelentes-quieren-ingenieros-20140311.html)

**2015**

[SILVESTRE LANDROBE, H. (2015). El Bachillerato de Excelencia Una breve crónica. Una visión personal. Un balance provisional. Tarbiya, Revista De Investigación E Innovación Educativa, (43).](https://revistas.uam.es/tarbiya/article/view/284)
Núm. 43 (2014): El bachillerato: problemas y perspectivas (aunque indica Publicado 23 febrero 2015)

[El Bachillerato de Excelencia Una breve crónica. Una visión personal. Un balance provisional. Horacio Silvestre Landrobe - unirioja.es](https://dialnet.unirioja.es/servlet/articulo?codigo=5188198)  
> El artículo traza la génesis y desarrollo del “Bachillerato de Excelencia” (BE) en el Instituto San Mateo de Madrid, donde se inició la experiencia didáctica en el curso 2011-2012. El BE pretende resucitar el modelo tradicional de instituto para la formación preuniversitaria que representa, por ejemplo, el Gymnasium en Alemania, modelo que en España se había perdido. Se narra la crónica de la puesta en marcha del proyecto, se analizan los principios teóricos que le sirven de fundamento, se describe su funcionamiento y se hace un primer balance de los resultados obtenidos y los éxitos cosechados, coincidiendo con la salida de la primera promoción de alumnos que han pasado por sus aulas. 

**2016**

[Así es la élite de los institutos madrileños: 858 alumnos elegidos para la 'excelencia' - elconfidencial](https://www.elconfidencial.com/espana/madrid/2016-11-15/bachillerato-excelencia-alumnos-madrid-institutos-elite_1289475/)


**2017**

[Profesores a dedo y presiones para inflar notas: la otra cara del bachillerato ‘excelente’ - elpais](https://elpais.com/politica/2017/09/20/actualidad/1505897225_897958.html)  
El Defensor del Pueblo investiga la contratación de docentes en el modelo público madrileño  

[“Me despidieron del San Mateo por negarme a inflar las notas” - eldiario](https://www.eldiario.es/madrid/somos/malasana/me-despidieron-del-san-mateo-por-negarme-a-inflar-las-notas_1_6422190.html)

**2021**

[Así es ser profesor de un instituto sin suspensos, sin “pellas” y con alumnos que tienen un 9 de media - larazon](https://www.larazon.es/madrid/20210523/yx3bbxvaszfsnfqvtkz3uagw4m.html)

[Horacio Silvestre: "Los alumnos han copiado mucho en los exámenes 'online' y los padres lo sabían" - elmundo.es](https://www.elmundo.es/espana/2021/07/16/60f02d04e4d4d8bd228b4640.html)  
El director del IES de Excelencia San Mateo denuncia "una inflación de notas en el Bachillerato". "Hay más regateo con los suspensos en las juntas de evaluación que en los mercados persas", dice

**2022**

[Una década del Bachillerato de Excelencia en Madrid: "me ha cambiado la vida" - cope.es](https://www.cope.es/actualidad/mas-madrid/noticias/una-decada-del-bachillerato-excelencia-madrid-cambiado-vida-20221021_2356157)

**2024**

[La Comunidad de Madrid estrena el Bachillerato de Excelencia en la modalidad de Artes](https://www.comunidad.madrid/noticias/2024/06/30/comunidad-madrid-estrena-bachillerato-excelencia-modalidad-artes)  
> La Comunidad de Madrid estrena el Bachillerato de Excelencia en la modalidad de Artes el próximo curso 2024/25. Así, el instituto público Isabel la Católica de la capital impartirá la rama de Artes Plásticas, Imagen y Diseño, mientras que el IES Isaac Albéniz de Leganés lo hará en la de Música y Artes Escénicas. 

### Normativa programa excelencia bachillerato

[DECRETO 63/2012, de 7 de junio, del Consejo de Gobierno, por el que se regula el Programa de Excelencia en Bachillerato en institutos de Educación Secundaria de la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2012/06/11/BOCM-20120611-1.PDF)

[ORDEN 11995/2012, de 21 de diciembre, de la Consejería de Educación, Juventud y Deporte, de organización, funcionamiento e incorporación al Programa de Excelencia en Bachillerato en institutos de Educación Secundaria de la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/01/18/BOCM-20130118-5.PDF)

Hay instrucciones anuales: 

[Instrucciones de la Dirección General de Educación Infantil, Primaria y Secundaria sobre organización e incorporación al Programa de Excelencia en bachillerato en institutos de educación secundaria de la Comunidad de Madrid para el curso académico 2016-2017](https://www.educa2.madrid.org/web/educamadrid/principal/files/fbba2a9d-5b42-4daa-9ed8-60879bb7aae3/InstruccionesBachilleratoExcelencia1617.pdf?t=1519303247742)

[Instrucciones de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial sobre organización y desarrollo del Programa de Excelencia en Bachillerato en institutos de educación secundaria de la Comunidad de Madrid para el curso académico 2022-2023.](https://www.comunidad.madrid/sites/default/files/doc/educacion/instrucciones_dgesfpre_progr_excel_bto_2022-2023_30292860.pdf)

[Instrucciones, de 22 de junio, de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial sobre organización y desarrollo del Programa de Excelencia en Bachillerato en institutos de educación secundaria de la Comunidad de Madrid para el curso académico 2023-2024.](https://comunidad.madrid/file/367442/download)

[Decreto 15/2024, de 7 de febrero, del Consejo de Gobierno, por el que se regula la provisión del profesorado del Programa de Excelencia en Bachillerato de la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2024/02/09/BOCM-20240209-1.PDF)

### Diferencias bachillerato excelencia frente al no de excelencia

Al impartir en un centro con aulas de bachillerato de excelencia es una pregunta que he escuchado muchas veces. Lo esencial es:

- En el acceso los alumnos tienen que tener al menos un 8 de nota media en Lengua Castellana y Literatura, Matemáticas, Geografía e Historia e Inglés.
[Instrucción cuarta 2023-2024 Excelencia](https://comunidad.madrid/file/367442/download)
> Los alumnos que deseen incorporarse al Programa deberán acreditar haber concurrido a las
pruebas de los premios extraordinarios de Educación Secundaria Obligatoria de la Comunidad de
Madrid, o bien acreditar haber obtenido en las materias de Lengua Castellana y Literatura,
Primera Lengua Extranjera, Geografía e Historia y Matemáticas de cuarto curso de Educación
Secundaria Obligatoria una nota media igual o superior a 8.

- Los alumnos deben realizar un proyecto de excelencia bianual  
[Artículo 3 DECRETO 63/2012](https://www.bocm.es/boletin/CM_Orden_BOCM/2012/06/11/BOCM-20120611-1.PDF)  
> Los alumnos que cursen el Programa de Excelencia realizarán un proyecto de investigación  

[Artículo 7 ORDEN 11995/2012](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/01/18/BOCM-20130118-5.PDF)  
> 1. Los alumnos que cursen el Programa de Excelencia realizarán un proyecto de investigación que se desarrollará, con carácter general, a lo largo de los dos cursos del Bachillerato.

- La realización del proyecto de excelencia tiene repercusión en las calificaciones  
[Artículo 7 ORDEN 11995/2012](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/01/18/BOCM-20130118-5.PDF)  
>2. La calificación obtenida por los alumnos en el proyecto de investigación podrá ser
tenida en cuenta por los profesores en la evaluación de las distintas materias que cursen en
segundo de Bachillerato.

- Existe horario de tarde para actividades y realización del proyecto excelencia  
[Artículo 3 DECRETO 63/2012](https://www.bocm.es/boletin/CM_Orden_BOCM/2012/06/11/BOCM-20120611-1.PDF)  
>El Programa de Excelencia en Bachillerato deberá contemplar la organización de
actividades, cursos o seminarios de profundización en las distintas materias que componen
el plan de estudios.  

[Artículo 5 ORDEN 11995/2012](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/01/18/BOCM-20130118-5.PDF)  
> 3. En horario vespertino, los alumnos del Programa participarán en las distintas
actividades programadas por el equipo docente destinadas a la elaboración del proyecto de
investigación y a profundizar en su formación 

[Artículo 6 ORDEN 11995/2012](https://www.bocm.es/boletin/CM_Orden_BOCM/2013/01/18/BOCM-20130118-5.PDF)  
> 1. Los profesores que impartan el Programa de Excelencia, en cualquiera de sus opciones, dedicarán, **en horario vespertino, al menos, tres horas a la semana**, a dirigir los proyectos de investigación y a cuantas actividades se hayan programado por el equipo docente para profundizar en la formación de los alumnos, sin que ello suponga que la jornada
exceda de la legalmente establecida.

- Existen algunas horas adicionales de algunas materias  
En LOMCE eran 5 horas en Física y Química en 1º, con LOMLOE pasan a ser 5 hora en Matemáticas en 1º  
[Anexo I ORGANIZACIÓN DEL PRIMER Y SEGUNDO CURSO DEL PROGRAMA DE EXCELENCIA EN BACHILLERATO 2023-2024 Excelencia](https://comunidad.madrid/file/367442/download)

### Repercusión en calificaciones bachillerato del proyecto de excelencia

La normativa es vaga y lo regula realmente cada centro. Hasta donde sé la nota modificada sí se usa para la media de acceso a la universidad, pero no se utiliza para la concesión de matrículas de honor. La repercusión no permite aprobar materias suspensas.

[IES Palas Atenea. Normas proyecto investigación](https://www.educa2.madrid.org/web/educamadrid/principal/files/2b524f08-41d7-4893-96b9-7db456cb38fa/Documentos/Excelencia/Normas%20proyecto%20investigaci%C3%B3n.pdf?t=1571328148401)  
> Podrá aumentar la nota media de 2º hasta 1 punto. Esto supone aumentar hasta 1 punto la nota de
cada materia de 2º de bachillerato, pero sólo una vez que ésta se haya aprobado. Si no se aprueba,
este aumento se producirá en el momento en el que se supere la asignatura.  
⁻ Calificación > 9,5 sube 1 punto en 8 asignaturas  
⁻ Calificación mayor a 8,5 y hasta 9,5 sube 1 punto en 7 asignaturas  
⁻ Calificación mayor a 7,5 y hasta 8,5 sube 1 punto en 6 asignaturas  
⁻ Calificación mayor a 6,5 y hasta 7,5 sube 1 punto en 5 asignaturas  
⁻ Calificación entre 5 y hasta 6,5 sube 1 punto en 4 asignaturas  
 Para que la nota del Proyecto repercuta en la nota media, tiene que tener una calificación mayor o
igual a cinco puntos.

[IES Pinto Antonio López. Bases para los proyectos de investigación curso 2020-2021](https://iespintorantoniolopez.org//wp-content/uploads/2021/09/Bases-para-los-proyectos-del-curso-20-21.pdf)  
> Repercusión en la nota de Bachillerato. La nota del proyecto supondrá un incremento proporcional de
hasta un máximo de un punto sobre la media de segundo de bachillerato. Será la misma calificación la
que fije el porcentaje aplicado: por ejemplo, a un 8 correspondería una subida del 80%, es decir, 0,64
puntos, que se sumarían a la media final de las materias del curso. No se aplicará la subida si la nota del
proyecto es inferior a 7. Si el alumno suspendiera en la convocatoria ordinaria de mayo, solo se aplicará
el incremento a las materias aprobadas. En ningún caso se empleará para obtener el aprobado de una
suspensa.

[IES Prado Santo Domingo. Presentación padres Bachillerato puertas abiertas 2023](https://site.educa.madrid.org/ies.pradodesantodomingo.alcorcon//wp-content/uploads/ies.pradodesantodomingo.alcorcon/2023/03/PRESENTACION-PADRES-BACHILLERATO-PUERTAS-ABIERTAS-2023.pdf)  
> El proyecto de investigación servirá para subir la nota media de 2º de Bachillerato (máx. un punto)

[IES Margarita Salas. Criterios de repersución proyecto excelencia en calificaciones 2º Bachillerato](https://www.educa2.madrid.org/web/educamadrid/principal/files/b749efe3-ca04-40fa-bc4f-09f3ec440494/CRITERIOS%20DE%20REPERCUSI%C3%93N%20PROYECTO%20EXCELENCIA%20EN%20CALIFICACIONES%20SEGUNDO%20BACHILLERATO.pdf?t=1683581373755)  
> La repercusión en las calificaciones se producirá siempre y cuando la
nota final del proyecto, recogida en el acta de evaluación por el tribunal y
basado en las rúbricas de evaluación de cada uno de los miembros del
tribunal, tenga una calificación media total entre 5,5 y 10 puntos. De esta
manera:  
|NOTA DEL PROYECTO | REPERCUSIÓN EN LA NOTA DE 2º BACHILLERATO |
|:-:|:-:|
|5.5-6.4 | 1 PUNTO|
|6.5-7.4 | 2 PUNTO|
|7.5-8.4 | 3 PUNTO|
|8.5-9.4 | 4 PUNTO|
|9.5-10 | 5 PUNTO|

[IES San Mateo. PLAN DE CONVIVENCIA del INSTITUTO SAN MATEO](https://www.educa2.madrid.org/web/educamadrid/principal/files/083bc069-1775-4a64-b63e-26254ecd4de9/Plan_de_Convivencia_Instituto_San_Mateo_APROBADO_19_abril_2021.pdf?t=1620302463660)  
> Artículo 14  
La calificación de cada ‘ensayo’ la realizarán independientemente dos profesores
designados por la Dirección por puntos enteros de 0 a 10. La media aritmética de ambas
calificaciones será la nota final, que en aplicación del artículo 7.2 de la Orden 11995/2012
servirá para incrementar la puntuación obtenida por el alumno en la evaluación final de
2º Bachillerato, de acuerdo con la siguiente escala: una nota final del ‘Ensayo’ de 10 ó 9,5
podrá sumar tres puntos al total de las notas; la calificación de 9 u 8,5, dos puntos; y, si
la nota obtenida es 8, 7,5 ó 7, sólo se sumará un punto. La calificación por debajo del 5 
supondrá incumplir uno de los requisitos para obtener la mención de excelencia en el
historial académico, al igual que no presentar el ‘ensayo’ en los plazos establecidos.

### IES San Mateo

Es el único centro de excelencia (solo tiene Bachillerato y solo de excelencia)

Tiene plantilla orgánica cero desde su creación: todo son comisiones de servicio sin convocatoria. Era algo que se suponía pero se confirma en 2020 tras conseguir que publiquen plantillas docentes  

[Personal docente en centros públicos no universitarios de la Comunidad de Madrid](https://comunidad.madrid/servicios/educacion/personal-docente-centros-publicos-no-universitarios-comunidad-madrid)

Se puede ver detalle claustro en 2021
[2021-07-01 Resolución transparencia claustro San Mateo anonimizada](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2021-07-01-Resoluci%C3%B3nTransparenciaClaustro_san_Mateo-anonimizada.pdf)  
[2021-07-01 Anexo claustro San Mateo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ComisionesServicio/Madrid/2021-07-01-Anexo-ClaustroSanMateo.pdf)  

Excepcionalmente ha habido asignaciones de interinos a jornada parcial  
[ASIGNACIÓN DEFINITIVA POR ESPECIALIDAD Y ORDEN DE INTERINOS CURSO 2021-2022](https://www.comunidad.madrid/sites/default/files/doc/educacion/rh09/rh09_118_2122_0804_asig_def_sec_listado_definitivo_interinos.pdf)  
> Blazquez Serrano, Alicia 1 3 04555 28030939-IES SAN MATEO (MADRID) 0590011 DAT MAD.-CAPITAL Media  
einado Escudero, Miguel Angel 1 3 04216 28030939-IES SAN MATEO (MADRID) 0590017 DAT MAD.-CAPITAL Dos tercios

[twitter FiQuiPedia/status/1343876749365571585](https://twitter.com/FiQuiPedia/status/1343876749365571585)
Segunda idea: IES San Mateo, cupo 18, plantilla orgánica 0, cupo complementario 18, 100% cupo. Permite confirmar situaciones donde las opacas comisiones de servicio de Madrid son escandalosas, además de sin la convocatoria a la que obliga STS 2091/2019

Gracias a forzar en RT0109/2020 publicar las de otros años, @educacmadrid publica, ahora sin solicitarlo, plantillas 2021-22. #ReclamadMalditos  
San Mateo, total 18,6; orgánica 0; complementaria 18,6. Crece (antes 18), sin convocatoria comisiones servicio.  


[PLAN DE CONVIVENCIA del INSTITUTO SAN MATEO](https://www.educa2.madrid.org/web/educamadrid/principal/files/083bc069-1775-4a64-b63e-26254ecd4de9/Plan_de_Convivencia_Instituto_San_Mateo_APROBADO_19_abril_2021.pdf?t=1620302463660)  
> El PC se ha informado en el Claustro de 25 de marzo de 2021 y aprobado en el Consejo Escolar de 19 de abril de 2021.

> Artículo 30  
Los alumnos del Instituto San Mateo deberán vestir de manera adecuada, acorde con las
actividades propias del instituto. Quedan prohibidas aquellas prendas de vestir o
cualquier otro atuendo o tocado que pueda perturbar o entorpecer el normal desarrollo
de las clases. En particular, por razones de seguridad e identificación de los alumnos, no
se puede llevar la cabeza cubierta dentro de las dependencias del instituto ni en las
actividades relacionadas con el desarrollo del curso. El incumplimiento del código de
vestimenta se considerará falta grave y, de acuerdo con el artículo 34.2.b) del Decreto
32/2019, se corregirá con una medida de aplicación inmediata.

[COMPROMISO ÉTICO DE LOS ALUMNOS DEL INSTITUTO SAN MATEO (versión 2018)](https://www.educa2.madrid.org/web/educamadrid/principal/files/bcaa8036-3b99-4971-a51f-21e62aae889f/COMPROMISO_ETICO_versi%C3%B3n_2018.pdf?t=1520593203441)  
> 5. No usaré abalorios, pendientes, peinados, teñidos o cualquier otro tipo de ornato corporal que no fuera
aceptado en cualquier otro centro de enseñanza digno de tal nombre. En otras palabras, declaro expresamente
que no es aceptable ingresar en un centro de enseñanza público para hacer ostentación de indumentarias o
adornos **que no se admitirían en un centro de enseñanza privado** por ser contrarias a las metas formativas
que se persiguen en la educación a estas edades.

[2021 Respuesta inspección educativa IES San Mateo uso de google para admisión](https://drive.google.com/file/d/1jSSvD8uz48U_mOO0DfJ0QRLwWLZ06rXe/view)

[CONVENIO de 9 de septiembre de 2022, entre la Comunidad de Madrid y Radio Televisión Madrid, S. A. U., para impulsar una visión completa y actual de la educación madrileña en los medios de comunicación.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/10/28/BOCM-20221028-27.PDF)  
> 2. La excelencia en el corazón de Madrid:  
El Instituto San Mateo es el centro de referencia de excelencia de la Comunidad de Madrid,
unido a las aulas de excelencia que hay en el resto de la región. Telemadrid.es narrará qué
es, requisitos para optar al centro, lo que ofrece a los estudiantes. Testimonio de exalumnos.  
Además, se añadirá un gráfico interactivo con el número de alumnos que han disfrutado de esta excelencia académica desde que se puso en marcha

[La excelencia en el corazón de Madrid. El Instituto San Mateo es el centro de referencia de excelencia de la Comunidad de Madrid, unido a las aulas de excelencia que hay en el resto de la región - telemadrid](https://www.telemadrid.es/la-semana-de/educacion/La-excelencia-en-el-corazon-de-Madrid-0-2484951494--20220906080000.html)  

### Horacio Silvestre Landrobe

Es del director del IES San Mateo desde su inicio 

[The Intolerance Network - wikileaks](https://wikileaks.org/intolerancenetwork/?q=Silvestre+Landrobe%2C+Salvador&count=50&sort=0#searchresult)  
> Over 17,000 documents from HazteOir and CitizenGO, Spanish right-wing campaigning organizations. Their links to Spain's far-right political party Vox and the Mexican sect El Yunque are well documented. These documents include HazteOir founding CitizenGo in 2013 to expand their reach, as well as their organzing of the 2012 World Congress of Families, an influential American far-right platform.  

Aparece Horacio Silvestre Landrobe 

### Segregación

Enlaza con [Finalidad evaluación para clasificación centros](../finalidad-evaluacion-para-clasificacion-centros)  

Separar a los alumnos por expediente académico se puede ver como segregación, aunque hay que valorar cómo se realiza:  
* En "los centros de excelencia" (único centro es IES San Mateo) solo hay alumnos del programa de bachillerato de excelencia: es segregación intercentros.  
* En las aulas de excelencia solo hay alumnos del programa de bachillerato de excelencia conviviendo con alumnos que no son del programa: es segregación intracentro. Se puede ver como algo similar a segregación intracentro entre sección y programa de centros bilingües. Al igual que en esos casos, la organización del centro puede hacerse con grupos solo programa y solo sección o con grupos mixtos. 

Se pueden ver artículos y opiniones (ver 2014 FAPA Giner de los Ríos y blog de Mariano Martín Gordillo)

En mi experiencia en aulas de excelencia, 4 cursos cuando escribo esto, siempre ha habido grupos mixtos de alumnos de aulas de excelencia con alumnos de fuera del programa en algunas optativas. 

Atender a cada uno según sus capacidades creo es positivo, y con recursos se puede hacer "por arriba y por abajo" con alumnos que pueden más y a los que les cuesta, por lo que se puede hacer la analogía de aula de excelencia (en la que no se reduce la ratio) con un aula de diversificación curricular (en la que sí se reduce la ratio).

En el bachillerato de excelencia, tanto en IES San Mateo como en aulas de excelencia, hay suspensos, repeticiones (saliendo de programa) y abandonos del programa. Realizar estas afirmaciones requieren pruebas, lo que choca con que los datos académicos centro a centro permiten clasificarlos y no se deberían difundir tras el último cambio en LOMLOE [Finalidad evaluación para clasificación centros](../finalidad-evaluacion-para-clasificacion-centros), pero dado que otra persona los solicitó para IES San Mateo y la consejería los facilitó, comparto esa respuesta anonimizada solo para el IES San Mateo, ya que lo veo como una aclaración: ya es público que la nota de acceso a programa es superior a 8 y el propio centro y consejería publicitan que tienen la nota de acceso a la universidad es de las más altas.

[El San Mateo reivindica su «excelencia» en Selectividad - larazon (pdf en web ucm)](https://www.ucm.es/data/cont/media/www/pag-56392/sel%20raz.pdf)  

[Web IES San Mateo](https://www.educa2.madrid.org/web/centro.ies.sanmateo.madrid)  
> RESULTADOS EVAU 2022  
Todos nuestros alumnos de Segundo de Bachillerato se han presentado a la 'Selectividad' obteniendo una nota media de 8,76. Además, como promedio nuestros alumnos añaden 3,598 puntos (sobre 4 puntos posibles) a su nota con los exámenes de la llamada 'fase específica'.  ¡Enhorabuena!

> RESULTADOS EvAU 2021  
Damos la enhorabuena a nuestros 106 alumnos de Segundo de Bachillerato porque se han presentado todos a la 'Selectividad' y todos la han superado. A este dato relevante hay que añadir que la nota media de nuestros alumnos en la calificación de acceso a la Universidad ha sido de 8,85. Es más, en la nota de acceso (con los puntos extra de los exámenes voluntarios) nuestros alumnos promedian un 12,55, estando más del 60% por encima de la nota necesaria para solicitar una 'beca de excelencia'. Además, un 40% supera la exigente barrera de los 13 puntos.

> RESULTADOS EvAU 2020  
Nuestros 128 alumnos de Segundo de Bachillerato se han presentado todos a la 'Selectividad' obteniendo una nota media en la fase general de 9,007. Además, como promedio nuestros alumnos añaden 3,702 puntos (sobre 4 puntos posibles) a su nota de la fase general con los exámenes de la llamada 'fase específica'.  
Cabe destacar que la alumna de Letras Gema González Vaquerizo ha obtenido la segunda mejor nota de acceso de todo Madrid (9,975). Y es que los cuatro primeros de los que se examinan por la UPM son del San Mateo, a saber, la propia Gema, Irene Soto, Pablo Barquín y Pablo de Frutos.  
Enhorabuena a todos por estos resultados, que son el reflejo de la calidad humana y académica de los chicos del San Mateo.

> RESULTADOS EvAU 2019  
Nuestros 117 alumnos de Segundo de Bachillerato se han presentado todos a la 'Selectividad' obteniendo una nota media de 8,83. Ademas, en fase específica suben una media de 3,56 sobre 4 puntos posibles, lo que hace 62 alumnos, más del 50%, estén por encima del 12,5, nota que les posibilita pedir la Beca de Excelencia de la Comunidad de Madrid.  
Este año, además, Álvaro Rodero, obtuvo la mejor nota de Madrid, un 9,975, que alcanzaron también otros cuatro alumnos de otros centros educativos.  
¡Enhorabuena a todos por estos resultados que evidencian el compromiso y buen hacer de nuestros alumnos y profesores!

> RESULTADOS EVAU 2018  
Un año más, nos sentimos muy satisfechos y orgullosos de anunciar los resultados obtenidos por nuestos alumnos en la EVAU. Todos, 133, concurrieron a la prueba: 100% de aprobados con una nota media en la Fase Obligatoria de 8,474 y nota media CAU de 11,769. Además, 53 alumnos obtienen una nota superior a 12,5 con la que previsiblemente podrán optar a la Beca de Excelencia de la Comunidad de Madrid. ¡Enhorabuena a todos!

> RESULTADOS EVAU 2017  
Este año batimos todos los récords: De nuestros 121 alumnos de 2º de Bachillerato, 100% presentados, 100% aprobados, y una nota media en la Fase General de la Evaluación de Acceso a la Universidad de 8,6. Un tercio obtiene más de 9 de media en la Fase General.  
¡Enhorabuena a todos!

[2021-06-15 Notas EVAU San Mateo cursos 2019 y 2020 anonimizado](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2021-06-15-Notas_EVAU_San_Mateo_cursos_2019_y_2020-anonimizado.pdf)

[2021-06-15 Suspensos San Mateo anonimizado](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/algoquedaquedecir/ProgramaExcelenciaBachillerato/2021-06-15-Suspensos_san_Mateo-anonimizado.pdf)




