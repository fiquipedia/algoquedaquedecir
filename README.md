Proyecto para albergar el blog algoquedaquedecir en GitLab pages  
La idea es abandonar Blogger y usar GitLab pages con markdown, de manera similar a lo realizado con http://fiquipedia.gitlab.io/fiquipedia en 2021  

El blog inicial en blogger es https://algoquedaquedecir.blogspot.com en el que no utilizo un dominio propio  

La página estará en http://fiquipedia.gitlab.io/algoquedaquedecir  

Intentaré ir haciendo los nuevos posts aquí y migrar los anteriores: una vez migrado cada post en blogger intentaré dejar solo un enlace a esta página  

Me animo a realizarlo tras la experiencia de la migración de FiQuiPedia:
- No dependo de blogger ni de google
- Los datos quedan más accesibles (necesitaré también migrar muchos datos que enlazo en Google Drive)  

Inicialmente pienso mantener el tema hugo por conocerlo ya.

Subo los primeros posts en enero 2024, después de que blogger me eliminase un post. Uso el tema hugo clarity https://github.com/chipzoller/hugo-clarity 

En febrero 2024 añado un índice flotante según [Adding a floating TOC to the Hugo-Clarity theme](https://www.nodinrogers.com/post/2023-04-06-add-floating-toc-in-hugo-clarity-theme/). Inicialmente solo tiene 2 niveles de índice, pero eso me obliga a pensar bien los títulos de los apartados y crear subniveles: aunque no se lean, permite enlazar cada apartado. 

En marzo 2024 añadir una imagen y texto para "Twitter card" con el front matter en cada post 
